package expressway.ser;

import expressway.common.*;
import java.io.Serializable;


public class ClassificationAttributeSer extends ClassificationElementSer
	implements HierarchyAttributeSer, AttributeSer
{
	public Serializable defaultValue;
	public Serializable value;
	
	public String getNodeKind() { return NodeKindNames.ClassificationAttribute; }
	public Serializable getDefaultValue() { return defaultValue; }
	public void setDefaultValue(Serializable dv) { this.defaultValue = dv; }
	
	public Serializable getValue() { return value; }
	
	public void setValue(Serializable value) { this.value = value; }
}

