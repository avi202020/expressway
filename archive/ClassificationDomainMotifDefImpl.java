/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ClassificationElement.*;
import expressway.server.MotifElement.*;
import expressway.ser.*;
import java.util.*;
import java.io.File;
import java.io.IOException;

	
public class ClassificationDomainMotifDefImpl extends ClassificationDomainImpl
	implements ClassificationDomainMotifDef
{
	/** ownwership. */
	private String[] classNames = null;
		
	
	protected Class getSerClass() { return ClassificationDomainMotifDefSer.class; }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ClassificationDomainMotifDefImpl newInstance = (ClassificationDomainMotifDefImpl)(super.clone(cloneMap, cloneParent));
		newInstance.classNames = new String[classNames.length];
		int i = 0;
		for (String className : classNames) newInstance.classNames[i++] = className;
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		this.classNames = null;
	}
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		ClassificationDomainMotifDefSer ser = (ClassificationDomainMotifDefSer)nodeSer;
		
		//ser.motifJarFileName = (this.jarFile == null? "" :
		//	this.jarFile.getAbsolutePath());
		ser.motifClassNames = this.classNames;

		MenuOwnerHelper.copySerValues(this, (ClassificationDomainMotifDefSer)nodeSer);

		//ser.templateNodeIds = createNodeIdArray(templates);
		
		return super.externalize(nodeSer);
	}


	ClassificationDomainMotifDefImpl(String name)
	throws
		ParameterError
	{
		super(name);
	}
	
	
	protected void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.MotifDefIconImageName);
	}
	
	
	public Set<Template> getTemplates()
	{
		Set<Template> templates = new HashSet<Template>();
		Set<PersistentNode> elts = super.getChildren();
		for (PersistentNode elt : elts)
		{
			if (elt instanceof Template)
				templates.add((Template)elt);
			else throw new RuntimeException(
				"MotifDef contains a ClassificationElement that is not a Template");
		}
		
		return templates;
	}
	
	
	public Template getTemplate(String name)
	throws
		ElementNotFound
	{
		Set<Template> templates = getTemplates();
		for (Template t : templates)
		{
			if (t.getName().equals(name)) return t;
		}
		
		throw new ElementNotFound("Template named '" + name + "'");
	}
	
	
	public Set<Domain> getReferencingDomains()
	{
		List<ClassificationDomain> mds;
		try { mds = getModelEngine().getClassificationDomainPersistentNodes(); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		Set<Domain> refDomains = new TreeSet<Domain>();
		for (ClassificationDomain d : mds)
		{
			if (d.usesMotif(this)) refDomains.add(d);
		}
		
		return refDomains;
	}
	
	
	//public File getJarFile() { return jarFile; }
	
	
	//public void setJarFile(File file) { this.jarFile = file; }
	
		
	public void setMotifClassNames(String[] classNames) {this.classNames = classNames; }
	
	
	public String[] getMotifClassNames() { return this.classNames; }
}

