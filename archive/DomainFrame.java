/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import javax.swing.*;


/**
 * A window (JFrame) for viewing the contents and status of a Domain and
 * the status of the Domain's Scenarios.
 */
 
public class DomainFrame extends JFrame
{
	private String domainName = null;
	private ModelEngineGateway modelEngineGateway = null;
	
	
	DomainFrame(String domainName, ModelEngine modelEngine)
	{
		super("Domain: " + domainName);
		this.domainName = domainName;
		this.modelEngine = modelEngine;
		
		// Set initial position and size.
		setLocation(0, 0);
		setSize(1000, 600);
		
		// Instantiate sub-components:
		domainTabbedPanel = new DomainTabbedPanel();
		getContentPane().add(domainTabbedPanel, BorderLayout.CENTER);
		
		// Re-position sub-components.
		pack();

		// Make visible.
		setVisible(true);
	}
	
	
	synchronized void close()
	{
		
	}
}


/**
 * The top-level tabbed panel that is instantiated into a DomainFrame, and
 * that contains the various views (JPanels) of a Domain and its Scenarios.
 */
 
class DomainTabbedPanel extends JTabbedPane
{
	private String domainName = null;
	private ModelEngine modelEngine = null;
	private DomainPanel domainPanel = null;
	

	DomainTabbedPanel(String domainName, ModelEngine modelEngine)
	{
		this.domainName = domainName;
		this.modelEngine = modelEngine;
		
		Domain domain = modelEngine.getModelDomain(domainName);

		// Instantiate sub-components:
		VisualComponent domainPanel = VisualJComponent.makeVisual(domain);
		add(domainPanel);
		
		// Re-position sub-components.
		pack();

		// Make visible.
		setVisible(true);
	}
}


/**
 * A view (JPanel) of a Scenario, instantiated within a DomainTabbedPanel.
 * The view must correspond to the Domain of the enclosing DomainTabbedPanel.
 */
 
class ScenarioPanel extends JPane
{
	ScenarioPanel()
	{
		// Instantiate sub-components:
		
		
		// Re-position sub-components.

		// Make visible.
		
	}
	
	
	synchronized void close()
	{
		
	}
}

