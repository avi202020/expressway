/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Vector;
import java.io.IOException;
import java.io.Serializable;


/**
 * For writing events to persistent storage for archive, and reading them back.
 * Note: See also the SimulationRunImpl methods writeToArchive and readFromArchive.
 * Note: See also ObjectValueReaderWriter.
 */
 
public class EventListArchiver
{
	/**
	 * Write Generated Events to the specified output stream.
	 */
	 
	public static void writeEvents(String domainName, String scenarioName,
		String simRunName, List<GeneratedEvent>[] eventLists, PrintWriter pw)
	throws
		IOException
	{
		pw.println("Domain/Scenario/Run:" +
			domainName + "/" + scenarioName + "/" + simRunName);
		
		for (List<GeneratedEvent> eventList : eventLists)
			// Each list represents an epoch, and is written as a line
		{
			boolean firstTimeThrough = true;
			
			if (eventList == null) throw new RuntimeException("Null Event list");
			
			for (Event event : eventList)
			{
				if (firstTimeThrough) firstTimeThrough = false;
				else pw.print(";");  // separator between Events on a line.
				
				if (event instanceof StartupEvent)  // don't write these.
				{
				}
				else if (event instanceof GeneratedEvent)
				{
					GeneratedEvent genEvent = (GeneratedEvent)event;
					
					pw.print(genEvent.encodeAsString());
				}
				//else if (event instanceof NoChangeEvent)  // don't write
				//{
				//}
				else
					throw new IOException("Encountered a non-Generated Event: is a "
						+ event.getClass().getName());
			}
			
			pw.println();
		}
		
		pw.println("end events");
	}
	
	
	/**
	 * 
	 */
	 
	public static List<GeneratedEvent>[] readEvents(String clientId, ModelEngineLocal modelEngine,
		String[] domScenRunNameAr, BufferedReader br)
	throws
		IOException
	{
		PersistentNode[] domScenRunAr = new PersistentNode[3];
		readDomScenRun(modelEngine, domScenRunAr, br);
		
		ModelDomain domain = (ModelDomain)(domScenRunAr[0]);
		ModelScenario scenario = (ModelScenario)(domScenRunAr[1]);
		SimulationRun simRun = (SimulationRun)(domScenRunAr[2]);
		
		domScenRunNameAr[0] = domain.getName();
		domScenRunNameAr[1] = scenario.getName();
		domScenRunNameAr[2] = simRun.getName();
		
		return readEventRecords(domain, scenario, simRun, br);
	}
	
	
	/**
	 * 
	 */
	 
	public static List<GeneratedEvent>[] readEventRecords(ModelDomain domain, 
		ModelScenario scenario, SimulationRun simRun, BufferedReader br)
	throws
		IOException
	{
		List<List<GeneratedEvent>> eventListList = new Vector<List<GeneratedEvent>>();
		
		int iteration = 0;
		
		for (;;)
			// each line, representing an epoch, and containing a List of Events.
		try
		{
			iteration++;			
			
			String line = br.readLine();
			if (line == null) break; // done - no more lines.
			if (line.equals("end events")) break;  // done.
			
			String[] eventStrings = line.split(";");
			
			List<GeneratedEvent> eventList = new Vector<GeneratedEvent>();
			
			for (String eventString : eventStrings)  // each Event on the line.
			{
				GeneratedEvent event = GeneratedEventImpl.parseEncodedEvent(eventString);

				eventList.add(event);
			}
			
			eventListList.add(eventList);
		}
		catch (Exception ex) { 
			GlobalConsole.printStackTrace(ex);
			throw new IOException(ex.getMessage()); }
			
			
		List<GeneratedEvent>[] eventListAr = new List[eventListList.size()];

		int listNo = 0;
		for (List<GeneratedEvent> eventList : eventListList)
		{
			eventListAr[listNo++] = eventList;
		}
		
		return eventListAr;
	}
	
	
	/**
	 * 
	 */
	 
	public static void readDomScenRun(ModelEngineLocal modelEngine, 
		PersistentNode[] domScenRunAr, BufferedReader br)
	throws
		IOException
	{
		String[] domScenRunNameAr = new String[3];
		readDomScenRun(domScenRunNameAr, br);
		
		String modelDomainName = domScenRunNameAr[0];
		String scenarioName = domScenRunNameAr[1];
		String simRunName = domScenRunNameAr[2];
		
		ModelDomain domain = null;
		try { domain = modelEngine.getModelDomainPersistentNode(modelDomainName); }
		catch (Exception ex) { throw new IOException("Cannot find domain " +
			modelDomainName); }
		
		ModelScenario scenario = null;
		try { scenario = domain.getModelScenario(scenarioName); }
		catch (Exception ex) { throw new IOException(ex.getMessage()); }
		
		SimulationRun simRun = scenario.getSimulationRun(simRunName);
		
		domScenRunAr[0] = domain;
		domScenRunAr[1] = scenario;
		domScenRunAr[2] = simRun;
	}
	
	
	/**
	 * 
	 */
	 
	public static void readDomScenRun(String[] domScenRunAr, BufferedReader br)
	throws
		IOException
	{
		String firstLine = br.readLine();
		if (firstLine == null) throw new IOException("input file is empty");
		
		String[] parts = firstLine.split(":");
		if (! parts[0].equals("Domain/Scenario/Run")) throw new IOException(
			"On first line, did not find 'Domain/Scenario/Run'");
		
		String allPartsString = parts[1];
		parts = allPartsString.split("/");
		String modelDomainName = parts[0];
		String scenarioName = parts[1];
		String simRunName = parts[2];
		
		domScenRunAr[0] = modelDomainName;
		domScenRunAr[1] = scenarioName;
		domScenRunAr[2] = simRunName;
	}
	
	
	/**
	 * 
	 */
	 
	public static List<GeneratedEvent>[] readEvents(ModelEngineLocal modelEngine,
		PersistentNode[] domScenRunAr, BufferedReader br)
	throws
		IOException
	{
		readDomScenRun(modelEngine, domScenRunAr, br);
		
		ModelDomain domain = (ModelDomain)(domScenRunAr[0]);
		ModelScenario scenario = (ModelScenario)(domScenRunAr[1]);
		SimulationRun simRun = (SimulationRun)(domScenRunAr[2]);
		
		return readEventRecords(domain, scenario, simRun, br);
	}
}




Code from SimulationRunImpl:


	
	public void writeToArchive(PrintWriter writer)
	throws
		IOException
	{
		writer.println("simulation_run:");
		writer.println("duration=" + duration);
		writer.println("iterationLimit=" + iterationLimit);
		writer.println("startingDate=" + 
			(startingDate == null ? "unspecified" : startingDate.getTime()));
		writer.println("finalDate=" + 
			(finalDate == null ? "unspecified" : finalDate.getTime()));
		writer.println("randomSeed=" + randomSeed);
		writer.println("completedNorm=" + completedNorm);
		
		writer.println("simulation_errors");
		for (Exception simError : simTimeErrors)
		{
			writer.print("simulation_error:");
			writer.println(simError.toString());
		}
		
		writer.println("end simulation_errors");
		
		
		// Write external event history
		getModelScenario().getValueHistoryArchiveWriter(writer).
			writeToArchive(this.externalStateValues);
		
		
		writer.println("events:");		
		
		List<GeneratedEvent>[] eventLists = new Vector[iterEventList.size()];
		int i = 0;
		for (Set<SimulationTimeEvent> eventSet : iterEventList)
		{
			List<GeneratedEvent> eventList = new Vector<GeneratedEvent>();
			eventLists[i++] = eventList;
			
			for (SimulationTimeEvent event : eventSet)
				if (event instanceof GeneratedEvent)
					eventList.add((GeneratedEvent)event);
			//eventList.addAll(eventSet);
		}
		
		
		EventListArchiver.writeEvents(this.getModelDomain().getName(), 
			this.getModelScenario().getName(), this.getName(), 
			eventLists, writer);
		
		writer.println("end simulation_run");
	}
	
	
	public static SimulationRun readFromArchive(ModelEngineLocal modelEngine,
		BufferedReader br)
	throws
		CorruptInput,
		ParameterError,
		IOException
	{
		String line = br.readLine();
		if (line == null) throw new IOException("input file is empty");
		
		String nameNew;
		ModelScenario modelScenarioNew;
		ModelDomain modelDomainNew;
		long durationNew;
		int iterationLimitNew;
		Date startingDateNew;
		Date finalDateNew;
		long randomSeedNew;
		boolean completedNormNew;
		List<Exception> simErrorsNew = new Vector<Exception>();


		line = br.readLine();
		if (! (line.equals("simulation_run:"))) throw new CorruptInput(
			"Expected 'simulation_run:'");
		
		
		// Read duration.
		
		line = br.readLine();
		String[] parts = line.split("=");
		if (! (parts[0].equals("duration"))) throw new CorruptInput(
			"Expected 'duration='");
		
		durationNew = Long.parseLong(parts[1]);
		
		
		// Read iterationLimit.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("iterationLimit"))) throw new CorruptInput(
			"Expected 'iterationLimit='");
		
		iterationLimitNew = Integer.parseInt(parts[1]);
		
		
		// Read startingDate.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("startingDate"))) throw new CorruptInput(
			"Expected 'startingDate='");
		
		long startingDateTime = Long.parseLong(parts[1]);
		startingDateNew = new Date(startingDateTime);
		
		
		// Read finalDate.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("finalDate"))) throw new CorruptInput(
			"Expected 'finalDate='");
		
		long finalDateTime = Long.parseLong(parts[1]);
		finalDateNew = new Date(finalDateTime);
		
				
		// Read randomSeed.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("randomSeed"))) throw new CorruptInput(
			"Expected 'randomSeed='");
		
		randomSeedNew = Long.parseLong(parts[1]);
		
						
		// Read completedNorm.
		
		line = br.readLine();
		parts = line.split("=");
		if (! (parts[0].equals("completedNorm"))) throw new CorruptInput(
			"Expected 'completedNorm='");
		
		completedNormNew = Boolean.parseBoolean(parts[1]);
		
						
		// Read SimulationErrors.
		
		for (;;)
		{
			line = br.readLine();
			if (line == null) throw new CorruptInput("Ended early: no Events.");
			
			if (line.equals("end simulation_errors")) break;
			
			if (! (line.startsWith("simulation_error:"))) throw new CorruptInput(
				"Expected 'simulation_error:...'; found " + line);
			
			String errorString = line.substring(17);
			
			SimulationError simError;
			try { simError = SimulationError.parseSimulationErrorString(errorString); }
			catch (IllegalArgumentException ex) { throw new CorruptInput(
				"Simulation Error string is invalid: " + errorString); }
			
			simErrorsNew.add(simError);
		}
		
		
		// Get Model Domain and Model Scenario Nodes.
		
		PersistentNode[] domScenRunAr = new PersistentNode[3];
		EventListArchiver.readDomScenRun(modelEngine, domScenRunAr, br);
		
		nameNew = domScenRunAr[2].getName();
		
		modelScenarioNew = (ModelScenario)(domScenRunAr[1]);
		modelDomainNew = (ModelDomain)(domScenRunAr[0]);
		
		
		// Construct a SimulationRun object.
		
		SimulationRun simRunNew = modelScenarioNew.createSimulationRun(
			nameNew,
			null,
			modelScenarioNew, 
			modelDomainNew, 
			null,  // no callback because we will not be simulating.
			startingDateNew,
			finalDateNew,
			iterationLimitNew,
			durationNew,
			randomSeedNew
			);
		
			
		// Add Simulation Errors to the newly constructed Simulation Run.
		
		((SimulationRunImpl)simRunNew).addSimulationTimeErrors(simErrorsNew);
			
		
		// Read external event history.
		
		((SimulationRunImpl)simRunNew).externalStateValues = 
			modelScenarioNew.getValueHistoryArchiveReader(br).readFromArchive();
		
		
		// Read Events.
		
		List<GeneratedEvent>[] arOfListOfEvent = 
			EventListArchiver.readEventRecords(modelDomainNew, modelScenarioNew, 
				simRunNew, br);
				
			
		// Read final terminator.
		
		line = br.readLine();
		if (line == null) throw new CorruptInput(
			"Ended early: no 'end simulation_run'.");
		
		if (! line.equals("end simulation_run")) throw new CorruptInput(
			"Expected 'end simulation_run'; found: " + line);
		
		
		return simRunNew;
	}


