/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import expressway.common.*;
import expressway.gui.*;
import expressway.common.ModelElement.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.ModelEngineRemote.Warning;
import expressway.common.Constants;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
//import expressway.gui.graph.GraphPanel;
import generalpurpose.ThrowableUtil;
import awt.AWTTools;
import graph.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Collection;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;



/** ****************************************************************************
 * Adds behavior (popup menus and action handlers) to the Base class.
 */
 
public class ModelDomainViewPanel extends ModelDomainViewPanelBase
{
  /* ***************************************************************************
	 ***************************************************************************
	 * Initiatlization permanent state.
	 */
	
	private final ViewFactory viewFactory;
	private final PopulateActionListener populateActionListener;
	private final DepopulateActionListener depopulateActionListener;
	private final MouseListener popupListener;
	private final MouseListener mouseInputListener;
	private final MouseMotionListener mouseMotionListener;
	private SetAttrValueHandler setAttrValueHandler;
	private ViewExpectedValueByScenarioHandler viewExpectedValueByScenarioHandler;
	private AddConduitHandler addConduitHandler;
	//private final JPopupMenu activityPopup;
	
	
	/** ************************************************************************
     * Constructor.
     */
	 
	public ModelDomainViewPanel(JTabbedPane container, String modelDomainName,
		ViewFactory viewFactory, ModelEngineRemote modelEngine)
	{
		super(container, modelDomainName, modelEngine);
		this.viewFactory = viewFactory;
		
		
		// Create GUI action handlers.
		
		this.popupListener = new PopupListener();
		this.populateActionListener = new PopulateActionListener();
		this.depopulateActionListener = new DepopulateActionListener();
		
		VisualDragListener mouseListener = new VisualDragListener();
		this.mouseInputListener = mouseListener;
		this.mouseMotionListener = mouseListener;

		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);

		// Create a popup menu item and add the action "View Evidence" action
		// handler to it.
		
		//JMenuItem menuItem = new JMenuItem("View Evidence");
		//menuItem.addActionListener(viewEvidenceActionListener);

		// Create a popup menu to contain the menu item.
		
		//this.activityPopup = new JPopupMenu();
		//this.activityPopup.add(menuItem);
	}

	
	/*public ViewPanelBase postConstructionSetup()
	throws
		Exception
	{
		return super.postConstructionSetup();
	}*/
	
	
	public void recomputeDisplayAreaLocationAndSize()
	{
		Insets insets = this.getInsets();
		((Component)(getDisplayArea())).setSize(this.getWidth() - insets.left - insets.right,
			this.getHeight() - insets.top - insets.bottom);
		
		((Component)(getDisplayArea())).setLocation(insets.left, insets.top);
	}
	
	
	protected ViewFactory getViewFactory() { return this.viewFactory; }
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * From ModelDomainViewPanelBase.
	 */
	
	
	protected MouseListener getPopupListener() { return this.popupListener; }
	
	
	protected MouseListener getMouseInputListener() { return this.mouseInputListener; }
	
	
	protected MouseMotionListener getMouseMotionListener() { return this.mouseMotionListener; }
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Menu item factories.
	 */
	 
	 
	/** ************************************************************************
	 * Add (or remove) menu items (each with an Action Listener) to the specified
	 * popup menu. The variable visual is the target of the action.
	 */
	 
	protected void addContextMenuItems(JPopupMenu containerPopup, 
		VisualJComponent visual)
	{
		// Check if the Container has already been populated.
		
		JMenuItem menuItem = null;
		
		Set<String> childNodeIds = null;
		if (visual instanceof ContainerVisualJ)
			childNodeIds = ((ContainerVisualJ)visual).getChildNodeIds();
		
		String[] stateNodeIds = null;
		if (visual instanceof PortedContainerVisualJ)
			stateNodeIds = ((PortedContainerSer)(visual.getNodeSer())).stateNodeIds;
		
		if (visual.isPopulated())
		{
			// Create menu item to depopulate it.

			menuItem = new JMenuItem("De-Populate");
			menuItem.addActionListener(
				ModelDomainViewPanel.this.depopulateActionListener);
		}
		else if
			(
				((childNodeIds != null) && (childNodeIds.size() > 0))
				||
				((visual.getNodeSer() instanceof ModelElementSer) && 
					(((ModelElementSer)visual.getNodeSer()).attributeNodeIds.length > 0))
				||
				((stateNodeIds != null) && (stateNodeIds.length > 0))
			)
		{
			// Create a popup menu item and add the action
			// "Populate" action handler to it.
					
			menuItem = new JMenuItem("Populate");
			menuItem.addActionListener(
				ModelDomainViewPanel.this.populateActionListener);
		}

				
		// Add the [De-]Populate menu item.
					
		if (menuItem != null) containerPopup.add(menuItem);
		
		
		// If an Attribute, add item for setting value across all Scenarios.
		
		if (visual instanceof AttributeVisual)
		{
			JMenuItem menuItem4 = new JMenuItem("Set Value in all Scenarios...");
			menuItem4.addActionListener(getSetAttrValueHandler());
			containerPopup.add(menuItem4);
		}
		
		
		// Add menu item for displaying expected value by Scenario.
		
		if (visual instanceof StateVisual)
		{
			JMenuItem menuItem3a = new JMenuItem("View Expected Value By Scenario");
			menuItem3a.addActionListener(getViewExpectedValueByScenarioHandler(
				ModelDomainViewPanel.this.getNodeId(), (StateVisual)visual));
			containerPopup.add(menuItem3a);
		}
		else if (visual instanceof ModelContainerVisualJ)
		{
			JMenuItem menuItem3b = new JMenuItem("View Expected Value By Scenario");
			menuItem3b.addActionListener(getViewExpectedValueByScenarioHandler(
				ModelDomainViewPanel.this.getNodeId(), null));
			containerPopup.add(menuItem3b);
		}
		
		if ((visual instanceof ModelContainerVisual) || (visual instanceof PortVisual))
		{
			Set<VisualComponent> selectedVisuals = getSelected();
			boolean allPorts = true;
			if (selectedVisuals.size() == 0) allPorts = false;
			else for (VisualComponent selVis : selectedVisuals)
			{
				if (! (selVis instanceof PortVisual))
				{
					allPorts = false;
					break;
				}
			}
			
			if (allPorts && (selectedVisuals.size() >= 2))
			{
				JMenuItem addConduitMenuItem = new JMenuItem("Connect Selected Ports");
				addConduitMenuItem.addActionListener(getAddConduitHandler());
				containerPopup.add(addConduitMenuItem);
			}
		}
		
		
		// Add menu items for adding and deleting Visuals.
		
		ActionListener insertNewActionListener = getInsertNewHandler(
			containerPopup.getX(), containerPopup.getY());
			
		if (insertNewActionListener != null)
		{
			JMenuItem menuItem4 = new JMenuItem("Insert New \u2192");  // forward arrow
			menuItem4.addActionListener(insertNewActionListener);
			containerPopup.add(menuItem4);
		}
		
		ActionListener deleteActionListener = getDeleteHandler(visual);
		if (deleteActionListener != null)
		{
			JMenuItem menuItem5 = new JMenuItem("Delete");
			menuItem5.addActionListener(deleteActionListener);
			containerPopup.add(menuItem5);
		}
	}
	
	
	/**
	 * If no menu items are appropriate for the Visual, return null.
	 */
	 
	protected ActionListener getInsertNewHandler(int x, int y)
	{
		return new InsertNewActionListener(x, y);
	}
	
		
	/** ************************************************************************
	 * Respond to a request to instantiate a new Model Element, by popping up
	 * a list of the possible Model Element kinds.
	 */
	 
	class InsertNewActionListener implements ActionListener
	{
		private int x, y;
		
		
		InsertNewActionListener(int x, int y) { this.x = x; this.y = y; }
		
		
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			
			// Create a popup menu that contains a menu item for each of the
			// kinds of ModelElement that can be instantiated within the Visual.
			
			JPopupMenu insertNewPopup = new JPopupMenu();
				
			
			// Add menu item for adding an Attribute.
			
			JMenuItem menuItem = new JMenuItem("Attribute");
			menuItem.addActionListener(new InsertNewNodeHandler(x, y,
				visual.getNodeSer().getNodeId(), "Attribute"));

			insertNewPopup.add(menuItem);
			
			
			// Add menu item for each Container child Element kind.
			
			String[] instantiableKindNames;
			try { instantiableKindNames = getModelEngine().getInstantiableModelElementKinds(
				visual.getNodeSer().getClass()); }
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex), "Error",
					JOptionPane.ERROR_MESSAGE); return;
			}
			
			if (instantiableKindNames == null) return;
			
			for (String name : instantiableKindNames)
			{
				//if (name.startsWith("expressway.ModelElement$"))
				//	name = name.substring(24);
				
				menuItem = new JMenuItem(name);
				menuItem.addActionListener(new InsertNewNodeHandler(x, y,
					visual.getNodeSer().getNodeId(), name));

				insertNewPopup.add(menuItem);
			}
			
			int mx = 0;
			int my = 0;
			Object source = e.getSource();
			if (source instanceof Component)
			{
				mx = ((Component)source).getX() + ((Component)source).getWidth();
				my = ((Component)source).getY();
			}
						
			insertNewPopup.show(visual, mx, my);
		}
	}
	
	
	/** ************************************************************************
	 * Respond to a selection of a Model Element kind by sending a request to the
	 * server to instantiate that Model Element kind.
	 */
	 
	class InsertNewNodeHandler implements ActionListener
	{
		private int x, y;
		private String parentNodeId;
		private String nodeKind;
		
		
		InsertNewNodeHandler(int x, int y, String parentNodeId, String nodeKind)
		{
			this.x = x; this.y = y;
			this.parentNodeId = parentNodeId;
			this.nodeKind = nodeKind;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			double xLogical;
			double yLogical;
			try
			{
				xLogical = ModelDomainViewPanel.this.transformViewXCoordToNode(x, 0);
				yLogical = ModelDomainViewPanel.this.transformViewYCoordToNode(y, 0);
			}
			catch (ParameterError ex)
			{
				ex.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex), "Error",
					JOptionPane.ERROR_MESSAGE);  return;
			}
			
			try
			{
				if (nodeKind.equals("Attribute"))
				{
					try { getModelEngine().createAttribute(false, parentNodeId, xLogical, yLogical); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							getModelEngine().createAttribute(true, parentNodeId, xLogical, yLogical);
					}
				}
				else
				{
					try { getModelEngine().createModelElement(false,
						parentNodeId, nodeKind, xLogical, yLogical); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							getModelEngine().createModelElement(true,
								parentNodeId, nodeKind, xLogical, yLogical);
					}
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex), "Error",
					JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	
	/** ************************************************************************
	 * 
	 */
	 
	class DeleteActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent targetVisual = (VisualJComponent)originator;
			
			Set<VisualComponent> selectedVisuals = ModelDomainViewPanel.this.getSelected();
			selectedVisuals.add(targetVisual);
			
			for (VisualComponent visual : selectedVisuals) try
			{
				try { getModelEngine().deleteModelElement(false, visual.getNodeId()); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(null, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						getModelEngine().deleteModelElement(true, visual.getNodeId());
				}
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex), "Error",
					JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	
	/**
	 * If Delete is not appropriate for the Visual, return null.
	 */
	 
	protected ActionListener getDeleteHandler(VisualJComponent visual)
	{
		return new DeleteActionListener();
	}
	
	
	protected ActionListener getSetAttrValueHandler()
	{
		// Create lazily.
		if (this.setAttrValueHandler == null)
			this.setAttrValueHandler = new SetAttrValueHandler();
		
		return this.setAttrValueHandler;
	}

	
	protected ActionListener getViewExpectedValueByScenarioHandler(String domainId,
		StateVisual sv)
	{
		return new ViewExpectedValueByScenarioHandler(this, getContainer(),
					domainId, sv == null ? null : sv.getNodeId());
	}
	
	
	protected ActionListener getAddConduitHandler()
	{
		// Create lazily.
		if (this.addConduitHandler == null)
			this.addConduitHandler = new AddConduitHandler();
		
		return this.addConduitHandler;
	}
		

	
  /* ***************************************************************************
	 * *************************************************************************
	 * GUI event handlers.
	 */
	 
	 
	/** ************************************************************************
	 * Handler for a Visual drag or selection operation.
	 */
	 
	class VisualDragListener implements MouseListener, MouseMotionListener
	{
		/** The point at which the drag operation began. */
		private java.awt.Point mouseStartingPointInVisual = null;
		
		/** The original location of the Visual, before the drag began. */
		private java.awt.Point originalVisualPositionInParent = null;
		
		
		VisualJComponent startingVisual = null;
		boolean conduitsChecked = false;
		
		
		public synchronized void mousePressed(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			
			Component source = e.getComponent();
			
			if (! (source instanceof VisualJComponent)) return;
			VisualJComponent visual = (VisualJComponent)source;
			
			
			if (source instanceof ConduitVisualJ)
			{
				if (conduitsChecked) conduitsChecked = false;
				else  // Determine if the user clicked near a Conduit segment.
				{
					Conduit closestConduit = null;
					double shortestDistance = 0.0;
					
					Component[] components = ((ConduitVisualJ)source).getParent().getComponents();
					for (int zOrder = components.length-1; zOrder >= 0; zOrder--) // deepest first
					{
						Component component = components[zOrder];
						
						if (component instanceof ConduitVisualJ)
						{
							ConduitVisualJ conduit = (ConduitVisualJ)source;
							double distance = conduit.getDistanceFrom(e.getPoint());
							
							if (distance < SelectionTolerance)
							{
								if ((closestConduit == null) || (distance < shortestDistance))
								{
									closestConduit = conduit;
									shortestDistance = distance;
								}
							}
						}
					}
					
					
					if (closestConduit != null)
					{
						conduitsChecked = true;
						e.setTarget(conduit);
						closestConduit.dispatchEvent(e);  // Forward to conduit.
						return;
					}
					
					
					// No Conduit was selected. Forward to a non-Conduit.
					
					if (forwardToDeepestConduitOfThisContainer(e)) return;
				
					AWTTools.forwardEventToDeepestContainingComponent(e);
				}
				
				
				return;
			}
			
			
			// Process the Event.
			
			this.startingVisual = visual;
			
			try
			{
				// Save the starting point, so that positional translations
				// can be computed.
				
				mouseStartingPointInVisual = e.getPoint();
				originalVisualPositionInParent = visual.getLocation();
				
				
				// If the Ctrl and Alt key are not pressed, then un-select any
				// Visuals that are currently selected.
				
				if // ctrl and alt pressed
					(
					e.isControlDown() ||
					e.isAltDown() ||
					e.isShiftDown() ||
					e.isMetaDown()
					)
					visual.select(! isSelected(visual));
				else
				{
					boolean wasSelected = isSelected(visual);
					selectAll(false);
					visual.select(! wasSelected);
				}
			}
			catch (Exception ex)
			{
				visual.setInconsistent(true);
				ex.printStackTrace();
				
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error",
					JOptionPane.ERROR_MESSAGE); return;
			}
			finally { ModelDomainViewPanel.this.repaint(); }
		}
		
		
		public void mouseDragged(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			
			Component source = e.getComponent();

			/*
			if (source != startingVisual) // Forward to startingVisual.
			{
				AWTTools.forwardEventToContainingComponent(e, startingVisual);
				return;
			}
			*/
			
			
			if (source instanceof ConduitVisualJ)
			{
				if (forwardToDeepestConduitOfThisContainer(e)) return;
				
				AWTTools.forwardEventToDeepestContainingComponent(e);
				
				return;
			}
			
			if (! (source instanceof VisualJComponent)) return;
			
			VisualJComponent visual = (VisualJComponent)source;
			
			if (! visual.getNodeSer().isMovable()) return;
			
			if (visual != startingVisual) return;
			
			try
			{
				// Flush motion Events, and temporarily prevent further Events from
				// being created for the source Component.
				
				////visual.disableEvents();
			
				// Compute the Visual displacement of the drag Event, relative
				// to the original starting point.
				
				int dx = e.getX() - mouseStartingPointInVisual.x;
				int dy = e.getY() - mouseStartingPointInVisual.y;
				
				java.awt.Point location = visual.getLocation();

				int newVisualX = location.x + dx;
				int newVisualY = location.y + dy;
				
				// Tentatively move the Visual to the drag location, to give
				// the user visual feedback. Do not update the position of the
				// corresponding Node.
				
				visual.setLocation(newVisualX, newVisualY);
			}
			catch (Exception ex)
			{
				visual.setInconsistent(true);
				ex.printStackTrace();
				
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error",
					JOptionPane.ERROR_MESSAGE); return;
			}
			finally
			{
				////visual.enableEvents();
				ModelDomainViewPanel.this.repaint();
			}
		}
		
		
		public void mouseReleased(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			
			Component source = e.getComponent();

			if (source instanceof ConduitVisualJ)
			{
				if (forwardToDeepestConduitOfThisContainer(e)) return;
				
				AWTTools.forwardEventToDeepestContainingComponent(e);
				return;
			}
			
			if (! (source instanceof VisualJComponent)) return;
			
			VisualJComponent visual = (VisualJComponent)source;
			
			if (visual != startingVisual) return;

			int id = -1;
			try
			{
				id = logStartDragTime(((VisualJComponent)source).getNodeSer().getNodeId());
				
				
				// Obtain the new location, to which the user has dragged
				// the Visual.
								
				java.awt.Point location = visual.getLocation();

				int newVisualX = location.x;
				int newVisualY = location.y;
				
				
				// Update the Visual with its new location.
				// (Note that the Component location is updated automatically by
				// the refreshRedundantState method, which is called by the
				// updateServer method.)
				
				Container parent = visual.getParent();
				//int yOffset = parent.getHeight() - visual.getHeight();
				
				double nodeNewX = 
					ModelDomainViewPanel.this.transformViewXCoordToNode(
						newVisualX + visual.getInsets().left, parent.getInsets().left);
						
					//newVisualX + visual.getInsets().left - parent.getInsets().left, 0);
					
				int yOriginOffset = parent.getHeight() - parent.getInsets().bottom;
					
					//visual.getParent().getHeight() - visual.getHeight()
					//- visual.getInsets().top;
					
				double nodeNewY =
					ModelDomainViewPanel.this.transformViewYCoordToNode(
						newVisualY + visual.getHeight() - visual.getInsets().bottom,
						yOriginOffset);
					
				
				// If the Visual has a Port that is connected to a Conduit, display
				// the Conduit's visual space location.
				
				visual.getNodeSer().setX(nodeNewX);
				visual.getNodeSer().setY(nodeNewY);
				
				
				// Update the location of the corresponding Node on the server.
				// This is a non-blocking call.
				// This may result in an asynchronous callback to the GUI to
				// update the state of various Visual.
				
				if (visual.getLocation().equals(originalVisualPositionInParent)) return;
				
				
				// Notify View that area that was occupied by the Visual has now
				// changed. This allows the View to repaint any overlays on that
				// area.
				
				java.awt.Point locInDisplayArea = 
					ModelDomainViewPanel.this.getDisplayArea().translateVisualToDisplayArea(
					(Component)(visual.getParent()), originalVisualPositionInParent);
				
				//>>> Do I need this? The server will notify it.
				ModelDomainViewPanel.this.notifyThatDisplayAreaChanged(
					locInDisplayArea.x, locInDisplayArea.y, 
					visual.getWidth(), visual.getHeight());
				
				
				// Attempt to update the server.
				
				try { visual.updateLocationOnServer(); }
				catch (Exception ex2)
				{
					// Restore Visual to the location of the corresponding Node.
					
					if (originalVisualPositionInParent == null) throw new Exception(
						"Original location of component is unknown.", ex2);
					
					visual.setLocation(originalVisualPositionInParent);
					
					throw ex2;
				}
				
				
				// >>>> The server should do this.
				// Notify parent that one of its Components changed. The parent
				// will decide if it is impacted and whether the server should
				// be notified. This notification is necessary for example when
				// a Component is moved and the Component is connected (via its
				// Port) to a Conduit that is owned by the parent Component. In
				// that case the parent Component needs to reroute its Conduit.
				
				if (parent instanceof ContainerVisualJ)
				{
					((ContainerVisualJ)parent).componentChanged(visual);
					
					// this ends up notifying the server a second time,
					// causing a callback to the Conduit Visual.
				}
				
				
				//ModelDomainViewPanel.this.refreshVisuals(visual.getNodeSer());
			}
			catch (Exception ex)
			{
				visual.setInconsistent(true);
				ex.printStackTrace();
				
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error",
					JOptionPane.ERROR_MESSAGE); return;
			}
			finally
			{ 
				ModelDomainViewPanel.this.repaint();
				
				logEndDragTime(id, ((VisualJComponent)source).getNodeSer().getNodeId());
				
				writeDragTimes();
				clearDragEntries();
			}
		}
		

		public void mouseEntered(MouseEvent e) {}

		
		public void mouseExited(MouseEvent e) {}

		
		public void mouseClicked(MouseEvent e) {}

		
		public void mouseMoved(MouseEvent e) {}

	}
	
	
	static class DragEntry
	{
		DragEntry(int id, Kind kind, long time, String compId)
		{ this.id = id; this.kind = kind; this.time = time; this.compId = compId; }
		
		enum Kind { start, finish };
		
		int id;
		Kind kind;
		long time;
		String compId;
	}
	
	static List<DragEntry> dragEntries = new Vector<DragEntry>();
	static int dragId = 0;
	static synchronized int logStartDragTime(String compId)
	{
		dragEntries.add(new DragEntry(dragId, DragEntry.Kind.start, System.nanoTime(), compId));
		int id = dragId;
		dragId++;
		return id;
	}
	
	static synchronized void logEndDragTime(int dragId, String compId)
	{
		dragEntries.add(new DragEntry(dragId, DragEntry.Kind.finish, System.nanoTime(), compId));
	}
	
	static void writeDragTimes()
	{
		for (DragEntry entry : dragEntries)
		{
			System.out.println(entry.id + "\t" + entry.kind + "\t" + entry.time
				+ "\t" + entry.compId);
		}
	}
	
	static void clearDragEntries()
	{
		dragEntries = new Vector<DragEntry>();
	}
	
	
	/**
	 * If the Event source is the deepest Conduit belonging (directly) to its
	 * Container, then return false. Otherwise, identify the deepest such Conduit
	 * and forward the Event to it, and return true. If the source is not
	 * a Conduit, simply return false;
	 */
	 
	protected boolean forwardToDeepestConduitOfThisContainer(MouseEvent e)
	{
		Component source = e.getComponent();
		if (! ((source instanceof ConduitVisualJ)
			|| (source instanceof VisualComponentDisplayArea))
				) return false;
		
		Component[] components = ((ConduitVisualJ)source).getParent().getComponents();
		for (int zOrder = components.length-1; zOrder >= 0; zOrder--) // deepest first
		{
			Component component = components[zOrder];
			
			if (component instanceof ConduitVisualJ)
			{
				if (component == source) return false;

				e.setSource(component);
				component.dispatchEvent(e);
				return true;
			}
		}
		
		throw new RuntimeException(
			"Conduit " + source.getName() + "(" + source.getClass().getName() 
				+ ", of " + source.getParent().getName() + ") not found in Container " 
					+ this.getName() + " (" + this.getClass().getName() + ")");
	}


	 
	/** ************************************************************************
	 * Handler for right-click on a Node.
	 * 
	 * Displays a popup menu that allows the user
	 * to select "Populate", which (when selected) retrieves the Container's child
	 * Nodes and Attributes from the server and displays them as Visuals.
	 * 
	 * For an Activity that has a Transformation Attribute, the popup meno also
	 * allows the user to select "View Evidence", which then
	 * opens an Activity Evidence View.
	 */
	 
	class PopupListener extends MouseInputAdapter
	{
		public void mouseReleased(MouseEvent e)
		{
			if (! ((e.getButton() == MouseEvent.BUTTON2) ||
				(e.getButton() == MouseEvent.BUTTON3) ||
				((e.getButton() == MouseEvent.BUTTON1) && e.isControlDown())
			)) return;
			
			Component source = e.getComponent();
			
			if (! (source instanceof VisualJComponent)) return;
			VisualJComponent visual = (VisualJComponent)source;
			
			if (source instanceof ConduitVisualJ)
			{
				AWTTools.forwardEventToDeepestContainingComponent(e);
				return;
			}
			
			try  // Create a popup menu to contain the menu item.
			{
				JPopupMenu containerPopup = new JPopupMenu();
				
				addContextMenuItems(containerPopup, visual);
							
				containerPopup.show(visual, e.getX(), e.getY());
			}
			catch (Exception ex)
			{
				visual.setInconsistent(true);
				ex.printStackTrace();
				
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error",
					JOptionPane.ERROR_MESSAGE); return;
			}
		}
	}
	
		
	/** ************************************************************************
	 * Retrieves the Container's child Nodes from the server and displays them
	 * as Visuals.
	 */
	 
	class PopulateActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			
			// Populate the Component with its children.
			
			try
			{
				visual.populateElements();
			}
			catch (Exception ex)
			{
				visual.setInconsistent(true);
				
				ex.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error while retrieving Node " + visual.getName(),
					JOptionPane.ERROR_MESSAGE);
			}
			
			notifyVisualJComponentPopulated(visual, true);			
		}
	}
	
	
	/** ************************************************************************
	 * Removes the Container's child Visuals and unregisters them from the server.
	 */
	 
	class DepopulateActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Object source = e.getSource();
			if (! (source instanceof JMenuItem)) return;
			
			Component component = ((JMenuItem)source).getComponent();
			Component parent = component.getParent();
			Component originator = ((JPopupMenu)parent).getInvoker();
			
			if (! (originator instanceof ContainerVisualJ)) return;
			
			ContainerVisualJ containerVisual = (ContainerVisualJ)originator;
			
			try { containerVisual.depopulateElements(PortVisualJ.class); }
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"While removing Visual " + containerVisual.getName(),
					JOptionPane.ERROR_MESSAGE);
			}
			
			notifyVisualJComponentPopulated(containerVisual, false);
		}
	}


	
	
	/**
	 * Create a Conduit between each pair of Ports in the Set of selected Visuals.
	 */
	
	class AddConduitHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			if (! (originator instanceof ModelContainerVisualJ))
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					"To create a Conduit. select Ports and right click on the Container", "Error", 
					JOptionPane.ERROR_MESSAGE); return;
			}
			
			ModelContainerVisualJ containerVisual = (ModelContainerVisualJ)originator;
			
			Set<VisualComponent> selectedVisuals = getSelected();
			
			if (selectedVisuals.size() < 2)
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					"Must select at least two Ports", "Error", 
					JOptionPane.ERROR_MESSAGE); return;
			}
			
			VisualComponent[] visAr = 
				selectedVisuals.toArray(new VisualComponent[selectedVisuals.size()]);
			
			for (int i = 0; i < visAr.length; i++)
			{
				VisualComponent visual = visAr[i];
				
				if (! (visual instanceof PortVisualJ))
				{
					JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
						"May only interconnect Ports", "Error", 
						JOptionPane.ERROR_MESSAGE); return;
				}
				
				PortVisualJ portVisual = (PortVisualJ)visual;
				
				
				// Create a Conduit between the portVisual and each other Port
				// in the selection Set. If two Ports are already connected, do
				// not connect them.
				
				for (int j = i+1; j < visAr.length; j++)
				{
					VisualComponent ov = visAr[j];
					
					if (! (ov instanceof PortVisualJ))
					{
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							"May only interconnect Ports", "Error", 
							JOptionPane.ERROR_MESSAGE); return;
					}
					
					PortVisualJ otherPortVisual = (PortVisualJ)ov;
					
					if (otherPortVisual == portVisual) continue;
					
					
					// Send request to server to create the Conduit. The corresponding
					// Visual will be created when the server sends a NodeAddedNotice.
					
					(new Exception("Creating Conduit")).printStackTrace();
					ConduitSer conduitSer = null;
					try
					{
						try { conduitSer = getModelEngine().createConduit(false,
							containerVisual.getNodeSer().getNodeId(),
							portVisual.getNodeSer().getNodeId(),
							otherPortVisual.getNodeSer().getNodeId()); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							{
								conduitSer = getModelEngine().createConduit(true,
									containerVisual.getNodeSer().getNodeId(),
									portVisual.getNodeSer().getNodeId(),
									otherPortVisual.getNodeSer().getNodeId());
							}
						}
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							ThrowableUtil.getAllMessages(ex), "Error", 
							JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
	}
	
	
	/**
	 * Enable the user to set a value for an Attribute in all Scenarios.
	 */
	 
	class SetAttrValueHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			ScenarioSer[] scenarioSers = null;
			try { scenarioSers = getModelEngine().getScenarios(
				ModelDomainViewPanel.this.getNodeSer().getName()); }
			catch (Exception ex)
			{
				ex.printStackTrace();
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error", JOptionPane.ERROR_MESSAGE); return;
			}
			

			// Identify the selected Attribute.
			
			Component eventSource = AWTTools.getJPopupMenuOriginator(e);
			if (! (eventSource instanceof AttributeVisualJ)) throw new RuntimeException(
				"Event source expected to be an AttributeVisualJ but it is not");
			
			AttributeVisualJ attrVisual = (AttributeVisualJ)eventSource;
			

			// Determine if a value is set for the Attribute in any of the
			// Scenarios.
			
			boolean mightLoseData = false;
			Object[] values = new Object[scenarioSers.length];
			int i = 0;
			for (ScenarioSer scenSer : scenarioSers) try
			{
				Object value = getModelEngine().getAttributeValueById(
					attrVisual.getNodeSer().getNodeId(), scenSer.getNodeId());
				if (value != null) mightLoseData = true;
				values[i++] = value;
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error", JOptionPane.ERROR_MESSAGE); return;
			}
			
			if (mightLoseData)
			{
				// Inform the user via a modal dialog
				if (JOptionPane.showConfirmDialog(null, 
					"A value is set for this Attribute in some Scenarios: \n" +
						"are you sure you want to set it for all Scenarios?",
						"Are you sure?", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
					return;
			}
			
			
			// Display a non-modal value entry panel that allows the user to either
			// enter a String value (that will be parsed if possible), or choose 
			// from a list of tag types.
			
			AttrValueEntryPanel panel = new AttrValueEntryPanel(
				attrVisual, 
				ModelDomainViewPanel.this
				);
				
			int x = 0;
			int y = 0;
			Object source = e.getSource();
			if (source instanceof Component)
			{
				Component comp = (Component)source;
				x = comp.getX();
				y = comp.getY();
			}
				
			panel.setLocation(x, y);
			
			panel.show();
		}
	}
}

