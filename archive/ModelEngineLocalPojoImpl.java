/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.*;
import statistics.HistogramDomainParameters;
import statistics.TimeSeriesParameters;
import generalpurpose.ThrowableUtil;
import java.io.File;
import java.io.Serializable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.FileReader;
import java.io.StringWriter;  // for test only
import java.io.PrintWriter;  // for test only
import java.util.*;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.commons.math.stat.descriptive.SummaryStatisticsImpl;
import org.apache.commons.math.stat.regression.SimpleRegression;
import org.xml.sax.SAXParseException;


/** ****************************************************************************
 * Implements the ModelEngine interface using a Java object without any
 * persistence. Methods are synchronized to provide guarded re-entrancy.
 *
 * This implementation requires the user to identify a 'current' Decision
 * Domain, and that is used as the Decision Domain for all requests until
 * the user specifies another Decision Domain.
 * 
 * The option -load=<file_path> may be specified. This causes the Model Engine
 * to load the specified model definition file at startup. This is useful for
 * testing, since this implementation does not persist models.
 */

public class ModelEngineLocalPojoImpl
	implements ModelEngineLocal, java.io.Serializable
{
	/** Directory into which the database should be written when flushed to disk. */
	transient File databasedir;  // may be null.
	
	/** Directory into which the SimulationRuns should be written when flushed to disk. */
	transient File simrundir;  // may be null.
	
	long priorUniqueId;
	
	// Map of node ID to node reference.
	Map<String, PersistentNode> allNodes;
	
	long eventNumber;
	
	VisualGuidance defaultVisualGuidance;
	
	//java.util.Random random;
	RandomGenerator random;
	
	
	private boolean allowSimulations = true;
		
	private boolean thereAreUnflushedChanges = false;
	
	private Map<String, DecisionDomain> decisionDomains =
		new HashMap<String, DecisionDomain>();

	private Map<String, ModelDomain> modelDomains =
		new HashMap<String, ModelDomain>();


	//private DecisionDomain currentDecisionDomain = null;

	//private ModelDomain currentModelDomain = null;

	//private ModelScenario currentModelScenario = null;

	private transient Map<Integer, ServiceThread> threads;
	
	private transient Set<ListenerRegistrarImpl> listenerRegistrars;
		
	private transient CallbackManager callbackManager;
	

	
	
	/** ************************************************************************
	 * Constructor.
	 */

	public ModelEngineLocalPojoImpl(File dir)
	throws
		RemoteException
	{
		this.databasedir = dir;
		this.simrundir = dir;
		RandomGenerator.reset();
		
		reinitialize();
	}
	
	
	/** ************************************************************************
	 * This constructor is primarily for debugging. Setting the random number
	 * seed provides repeatability.
	 */
	 
	public ModelEngineLocalPojoImpl(File dir, long randomSeed)
	throws
		RemoteException
	{
		this.databasedir = dir;
		this.simrundir = dir;
		RandomGenerator.setRandomSeed(randomSeed);
		
		reinitialize();
	}
	
	
	/**
	 * Initialize any transient state. This is called during construction, and 
	 * also after reconstitution from a serialized state.
	 */
	 
	synchronized void reinitialize()
	{
		threads = new HashMap<Integer, ServiceThread>();
		listenerRegistrars = new HashSet<ListenerRegistrarImpl>();
		callbackManager = new CallbackManager(this);
	}
	
	
	synchronized void setRandomSeed(long seed)
	{
		RandomGenerator.setRandomSeed(seed);
	}
	
	
	synchronized void setDatabaseDirectory(File dbDirFile)
	{
		this.databasedir = dbDirFile;
	}
	
	
	synchronized File getDatabaseDirectory() { return databasedir; }
	
	
	synchronized void setSimulationRunDirectory(File simrundirFile)
	{
		this.simrundir = simrundirFile;
	}
	
	
	synchronized File getSimulationRunDirectory() { return simrundir; }
	
	
	synchronized static File findDatabaseFile(File databasedir)
	throws
		ParameterError  // if database directory or database file not found.
	{
		if (databasedir == null) throw new ParameterError(
			"Database directory is null");
		
		if (! databasedir.exists()) throw new ParameterError(
			"Database directory " + databasedir + " not found.");
		
		if (! databasedir.isDirectory()) throw new ParameterError(
			"Path " + databasedir + " is not a directory");
		
		File dbfile = new File(databasedir, "expresswaydb.ser");
		
		if (! dbfile.exists()) throw new ParameterError(
			"Database file " + dbfile + " not found.");
		
		return dbfile;
	}


	synchronized static ModelEngineLocalPojoImpl loadDatabase(File dbfile, File simrundir)
	throws
		ParameterError,  // if database directory or database file not found.
		IOException
	{
		File databasedir = dbfile.getParentFile();
		
		
		// Reconstitute the ModelEngineLocal instance.
		
		FileInputStream fis = new FileInputStream(dbfile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		ModelEngineLocalPojoImpl modelEngineLocal = null;
		try { modelEngineLocal = (ModelEngineLocalPojoImpl)(ois.readObject()); }
		catch (ClassNotFoundException cnfe) { throw new IOException(cnfe); }
		
		
		// Restore variables that are not persisted.
		
		modelEngineLocal.reinitialize();
		modelEngineLocal.databasedir = databasedir;
		modelEngineLocal.simrundir = databasedir;
		PersistentNodeImpl.priorUniqueId = modelEngineLocal.priorUniqueId;
		PersistentNodeImpl.allNodes = modelEngineLocal.allNodes;
		EventImpl.eventNumber = modelEngineLocal.eventNumber;
		DefaultVisualGuidance.guidance = modelEngineLocal.defaultVisualGuidance;
		RandomGenerator.setRandom(modelEngineLocal.random);
		
		// Reconstitute SimulationRuns.
		
		if (simrundir.exists())
		{
			File[] simRunFiles = simrundir.listFiles(new FilenameFilter()
			{
				public boolean accept(File dir, String name)
				{
					if (name.endsWith(".simrun")) return true;
					return false;
				}
			});
			
			for (File simRunFile : simRunFiles)
			{
				// Check that enough memory remains.
				if (Runtime.getRuntime().freeMemory() < 30000000) throw new IOException(
					"Server memory is running too low; try allocating more Java memory");
				
				try
				{
					BufferedReader br = new BufferedReader(new FileReader(simRunFile));
					SimulationRunImpl.readFromArchive(modelEngineLocal, br);
				}
				catch (Exception ex)
				{
					GlobalConsole.println("For SimRun file " + simRunFile.getName()
						+ ": " + ex.getMessage());
				}
			}
		}
		
		return modelEngineLocal;
	}


	
	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Methods from ModelEngineRemote.
	 *
	 */


	public synchronized String ping(String clientId)
	throws
		IOException
	{
		return (new Date()).toString();
	}
	
	
	public synchronized boolean hasUnflushedChanges()
	{
		return thereAreUnflushedChanges;
	}
	
	
	public synchronized void flush(String clientId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		if (this.databasedir == null) throw new ParameterError(
			"No file path was set by -dbdir option when the database was created.");
		
		File dbfile = new File(this.databasedir, "expresswaydb.ser");
		
		FileOutputStream fos = new FileOutputStream(dbfile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		
		// Copy static values to the ModelEngineLocal instance.
		
		this.random = RandomGenerator.getRandomGenerator();
		this.priorUniqueId = PersistentNodeImpl.priorUniqueId;
		this.allNodes = PersistentNodeImpl.allNodes;
		this.eventNumber = EventImpl.eventNumber;
		this.defaultVisualGuidance = DefaultVisualGuidance.guidance;

		
		// Write the ModelEngine object state to a file.
		
		oos.writeObject(this);
		
		
		// Write unsaved simulations to files in directory simrundir.
		
		File simDir = new File(this.simrundir, "simrundir");
		if (! simDir.exists()) if (! simDir.mkdir()) throw new IOException(
			"Could not create directory " + simDir);
		
		Set<ModelDomain> mds = new HashSet<ModelDomain>(modelDomains.values());
		
		for (ModelDomain domain : mds) try
		{
			Set<ModelScenario> scenarios = domain.getScenarios();
			
			for (ModelScenario scenario : scenarios)
			{
				List<SimulationRun> simRuns = 
					new Vector<SimulationRun>(scenario.getSimulationRuns());
				
				
				for (SimulationRun simRun : simRuns)
				{
					String fileName = domain.getName() + "." + scenario.getName() +
						"." + simRun.getName() + ".simrun";
						
					File file = new File(simDir, fileName);
					if (file.exists()) continue;  // already saved.
					

					PrintWriter pw = new PrintWriter(new FileWriter(file));
					
					try { ((SimulationRunImpl)simRun).writeToArchive(pw); }
					catch (Exception ex)
					{
						GlobalConsole.printStackTrace(ex);
						GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
					}
					finally { pw.close(); }
				}
			}
		}
		catch (Exception ex)
		{
			GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
		}
		
		thereAreUnflushedChanges = false;
	}

	
	public synchronized Integer parseXMLCharAr(String clientId, final char[] charAr, 
		PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			// Execute the parser asychronously so that it does not tie up
			// the ModelEngine interface.
			
			Integer threadHandle = createMonitorThreadHandle();
			XMLMonitorThread thread = 
				new XMLMonitorThread(threadHandle, peerListener, clientId, charAr);
			
			thereAreUnflushedChanges = true;
			
			threads.put(threadHandle, thread);
			
			thread.setDaemon(true);
			thread.start();
			
			return threadHandle;
		}
		catch (Exception ex)
		{
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized ModelDomainSer getModelDomain(String clientId, String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelDomain domain = modelDomains.get(name);
			if (domain == null) throw new ElementNotFound("Domain " + name);
			
			String domainId = domain.getNodeId();
			ModelDomain node = (ModelDomain)(PersistentNodeImpl.getNode(domainId));
			return (ModelDomainSer)(node.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized DecisionDomainSer getDecisionDomain(String clientId, String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			DecisionDomain domain = decisionDomains.get(name);
			if (domain == null) throw new ElementNotFound("Domain " + name);
			
			return (DecisionDomainSer)(domain.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<String> getDomainNames(String clientId)
	throws
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Set<String> decisionDomainNames = decisionDomains.keySet();
			Set<String> modelDomainNames = modelDomains.keySet();
			List<String> domainNames = new Vector<String>();
			domainNames.addAll(decisionDomainNames);
			domainNames.addAll(modelDomainNames);
			return domainNames;
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized List<NodeDomainSer> getDomains(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Vector<NodeSer> domainSers = new Vector<NodeSer>();
			Collection<ModelDomain> mdColl = modelDomains.values();
			Collection<DecisionDomain> ddColl = decisionDomains.values();
			for (ModelDomain md : mdColl) domainSers.add(md.externalize());
			for (DecisionDomain dd : ddColl) domainSers.add(dd.externalize());
			return (List)domainSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<ModelDomainSer> getModelDomains(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Vector<NodeSer> domainSers = new Vector<NodeSer>();
			Collection<ModelDomain> mdColl = modelDomains.values();
			for (ModelDomain md : mdColl) domainSers.add(md.externalize());
			return (List)domainSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
		
	public synchronized List<DecisionDomainSer> getDecisionDomains(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Vector<NodeSer> domainSers = new Vector<NodeSer>();
			Collection<DecisionDomain> ddColl = decisionDomains.values();
			for (DecisionDomain dd : ddColl) domainSers.add(dd.externalize());
			return (List)domainSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String exportNodeAsXML(String clientId, String nodeId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError("Node not found");
			
			XMLWriter writer = new XMLWriter(this, clientId);
			
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			
			writer.writeNode(printWriter, node);
			
			printWriter.flush();
			
			return stringWriter.toString();
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainSer createModelDomain(String clientId, String name, 
		boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelDomainSer domainSer =
				(ModelDomainSer)(createModelDomain_internal(name, useNameExactly)
					.externalize());
			
			thereAreUnflushedChanges = true;
			return domainSer;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainSer createModelDomain(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelDomainSer domainSer =
				(ModelDomainSer)(createModelDomain_internal().externalize());
			
			thereAreUnflushedChanges = true;
			return domainSer;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainSer createIndependentCopy(String clientId, 
		String modelDomainId)
	throws
		ParameterError,
		ModelContainsError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			try
			{
				PersistentNode node = PersistentNodeImpl.getNode(modelDomainId);
				if (node == null) throw new ParameterError("Domain not found");
				
				if (! (node instanceof ModelDomain)) throw new ParameterError(
					"The Node " + modelDomainId + " is not a Model Domain");
				
				ModelDomain domain = (ModelDomain)node;
				
				ModelDomain domainCopy = domain.createIndependentCopy();
				
				
				// Add it to this ModelEngine's Map of Domains.
				
				modelDomains.put(domainCopy.getName(), domainCopy);
				
				
				// Notify clients.
				
				try { callbackManager.notifyAllListeners(
					new PeerNoticeBase.DomainCreatedNotice(domainCopy.getNodeId())); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}			
				
				ModelDomainSer domainSer = (ModelDomainSer)(domainCopy.externalize());
				
				thereAreUnflushedChanges = true;
				return domainSer;
			}
			catch (CloneNotSupportedException ex)
			{
				throw new ModelContainsError(ex);
			}
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized DecisionDomainSer createDecisionDomain(String clientId, 
		String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			DecisionDomainSer domainSer =
				(DecisionDomainSer)(createDecisionDomain_internal(name, useNameExactly)
					.externalize());
			
			thereAreUnflushedChanges = true;
			return domainSer;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSer createModelScenario(String clientId, 
		String modelDomainName, String scenarioName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelScenarioSer scenarioSer =
				(ModelScenarioSer)(createModelScenario_internal(modelDomainName,
					scenarioName).externalize());
					
			thereAreUnflushedChanges = true;
			return scenarioSer;
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized ModelScenarioSer createModelScenario(String clientId, 
		String modelDomainName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelScenarioSer scenarioSer =
				(ModelScenarioSer)(createModelScenario_internal(modelDomainName)
					.externalize());
					
			thereAreUnflushedChanges = true;
			return scenarioSer;
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized ModelScenarioSer copyModelScenario(String clientId, 
		String scenarioId)
	throws
		ModelContainsError,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(scenarioId);
			if (node == null) throw new ParameterError("Could not find Node " + scenarioId);
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node is not a Model Scenario");
			
			ModelScenario scenario = (ModelScenario)node;
			
			ModelDomain domain = scenario.getModelDomain();

			ModelScenario newScenario = null;
			try { newScenario = domain.createScenario(null, scenario); }
			catch (CloneNotSupportedException ex)
			{
				throw new ModelContainsError(ex);
			}
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.ScenarioCreatedNotice(
					newScenario.getDomain().getNodeId(), newScenario.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
			
			return (ModelScenarioSer)(newScenario.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized AttributeSer createAttribute(String clientId, boolean confirm, 
		String parentId, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! (parentNode instanceof ModelElement)) throw new ParameterError(
				"Can only call this method on a Model Element.");
			
			ModelElement parentElement = (ModelElement)parentNode;
			
			if (parentElement.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(parentElement.getFullName());
			
			ModelDomain domain = parentElement.getModelDomain();
			
			if (! confirm)
			{
				if (domain.hasSimulationRuns()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
			}
			
			Attribute attribute = parentElement.createAttribute();
			
			thereAreUnflushedChanges = true;
			
			deleteSimulationRuns(domain);
			
			attribute.setX(x);
			attribute.setY(y);
			
			
			// Notify clients that the Element was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(parentNode);
			try
			{
				peer.notifyAllListeners(new PeerNoticeBase.NodeAddedNotice(
					parentId, attribute.getNodeId()));
				
				peer.notifyAllListeners(new PeerNoticeBase.NodeResizedNotice(parentId));
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (AttributeSer)(attribute.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized AttributeSer createAttribute(String clientId, boolean confirm, 
		String parentId, Serializable value, String scenarioId, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! (parentNode instanceof ModelElement)) throw new ParameterError(
				"Can only call this method on a Model Element.");
			
			ModelElement parentElement = (ModelElement)parentNode;
			
			if (parentElement.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(parentElement.getFullName());
			
			
			ModelDomain domain = parentElement.getModelDomain();
			
			if (! confirm)
			{
				if (domain.hasSimulationRuns()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
			}
			
			Attribute attribute = parentElement.createAttribute();
			
			if (scenarioId == null)
			{
				Set<ModelScenario> scenarios = domain.getScenarios();
				if (scenarios.size() == 0) throw new ParameterError(
					"No Scenarios in which to set the Attribute's value.");
				
				for (ModelScenario scenario : scenarios)
					scenario.setAttributeValue(attribute, value);
			}
			else
			{
				PersistentNode node = PersistentNodeImpl.getNode(scenarioId);
				if (node == null) throw new ParameterError(
					"Cannot identify Node with ID " + scenarioId);
				
				if (! (node instanceof ModelScenario)) throw new ParameterError(
					"Node with ID " + scenarioId + " is not a ModelScenario");
				
				ModelScenario scenario = (ModelScenario)node;
				
				scenario.setAttributeValue(attribute, value);
			}
			
			thereAreUnflushedChanges = true;
			
			if (! (value instanceof Types.Inspect)) deleteSimulationRuns(domain);
			
			attribute.setX(x);
			attribute.setY(y);
			
			
			// Notify clients that the Element was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(parentNode);
			try
			{
				peer.notifyAllListeners(new PeerNoticeBase.NodeAddedNotice(
					parentId, attribute.getNodeId()));
				
				peer.notifyAllListeners(new PeerNoticeBase.NodeResizedNotice(parentId));
					
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (AttributeSer)(attribute.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized String[] getInstantiableModelElementKinds(String clientId, 
		Class nodeSerClass)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			List<String> names;
			
			if (PortedContainerSer.class.isAssignableFrom(nodeSerClass))  // is a PortedContainer
				names = PortedContainerImpl.allInstantiableElementKinds;
			else if (ModelContainerSer.class.isAssignableFrom(nodeSerClass))  // is a ModelContainer
				names = ModelContainerImpl.allInstantiableElementKinds;
			else if (StateSer.class.isAssignableFrom(nodeSerClass))  // is a State
				names = StateImpl.allInstantiableElementKinds;
			else if (ModelElementSer.class.isAssignableFrom(nodeSerClass))  // is a ModelElement
				names = ModelElementImpl.allInstantiableElementKinds;
			else
				return null;
			
			return names.toArray(new String[names.size()]);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized String[] getInstantiableModelElementKinds(String clientId, 
		String parentId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! (parentNode instanceof ModelContainer)) throw new ParameterError(
				"Can only call this method on a Model Container.");
			
			
			List<String> names = 
				((ModelContainer)parentNode).getInstantiableModelElementKinds();
				
			return names.toArray(new String[names.size()]);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String[] getInstantiableModelElementPageNames(String clientId, 
		Class nodeSerClass)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			List<String> names;
			
			if (PortedContainerSer.class.isAssignableFrom(nodeSerClass))  // is a PortedContainer
				names = PortedContainerImpl.allElementPageNames;
			else if (ModelContainerSer.class.isAssignableFrom(nodeSerClass))  // is a ModelContainer
				names = ModelContainerImpl.allElementPageNames;
			else if (StateSer.class.isAssignableFrom(nodeSerClass))  // is a State
				names = StateImpl.allElementPageNames;
			else if (ModelElementSer.class.isAssignableFrom(nodeSerClass))  // is a ModelElement
				names = ModelElementImpl.allElementPageNames;
			else
				return null;
			
			return names.toArray(new String[names.size()]);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized ModelElementSer createModelElement(String clientId, boolean confirm,
		String parentId, String kind, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if ((x < 0) || (y < 0)) throw new ParameterError(
				"x and y must be positive: x=" + x + ", y=" + y);

			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! ( parentNode instanceof ModelElement)) throw new ParameterError(
				"Parent Node is not a Model Element");
			
			ModelElement parentElement = (ModelElement)parentNode;
			
			if (parentElement.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(parentElement.getFullName());
			
			
			if (! confirm)
			{
				if (parentElement.getModelDomain().hasSimulationRuns()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
			}
			
			
			// Capture the parent's current size so that we can determine if it
			// is changed as a side effect of adding the sub-Element to it.
			
			boolean resizedParent = false;
			
			double parentWidth = parentNode.getWidth();
			double parentHeight = parentNode.getHeight();
			
			
			// Create the sub-Element.
			
			ModelElement modelElement = parentElement.createSubElement(kind);
				
			thereAreUnflushedChanges = true;
				
				
			// Determine if the parent's size changed.
			
			if (parentWidth != parentElement.getWidth()) resizedParent = true;
			if (parentHeight != parentElement.getHeight()) resizedParent = true;
			
			
			// Delete simulation runs associated with all Scenarios of the Domain.
			
			deleteSimulationRuns(modelElement.getModelDomain());

			
			// Set the location of the new sub-Element to be where the user clicked.
			
			if (modelElement instanceof Port)
			{
				//..
				//GlobalConsole.println("Positions specified for Port are " + x + "," + y);
			}
			else if (parentNode.getLayoutManager() == null)
			{
				modelElement.setX(x);
				modelElement.setY(y);
			}
			
			
			// Notify clients that the Element was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(parentNode);
			
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(parentId)); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			
			return (ModelElementSer)(modelElement.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void deleteModelElement(String clientId, boolean confirm, 
		String elementId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(elementId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + elementId);
			
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"Can only call this method for a Model Element.");
			
			ModelElement modelElement = (ModelElement)node;

			if (modelElement.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(modelElement.getFullName());
			
			
			boolean canDeleteSimRuns = false;
			
			if (node instanceof Attribute)
			{
				Attribute attr = (Attribute)node;
				ModelDomain domain = attr.getModelDomain();
				Set<ModelScenario> scenarios = domain.getScenarios();
				
				for (ModelScenario scenario : scenarios)
					// each Scenario in which the Attribute has a value
				{
					Serializable value = scenario.getAttributeValue(attr);
					if ((value != null) && (! (value instanceof Types.Inspect)))
					{
						if (attr.getModelDomain().hasSimulationRuns())
							if (confirm)
								canDeleteSimRuns = true;
							else
								throw new Warning(
									"Simulation Runs will be invalid and will be deleted");
						
						break;
					}
				}
			}
			else if (! (node instanceof SimulationRun))
				if (((ModelElement)node).getModelDomain().hasSimulationRuns())
					if (! confirm) throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			
			if (node instanceof ModelDomain)
			{
				ModelDomain domain = (ModelDomain)node;
				
				Set<ModelScenario> scenarios = domain.getScenarios();
				if (scenarios.size() != 0)
				{
					if (! confirm) throw new Warning("Model Scenarios will be deleted.");
					
					for (ModelScenario scenario : scenarios)
						scenario.prepareForDeletion();
				}
				
				scenarios.clear();
				modelDomains.remove(domain.getName());
			}
			else if (node instanceof ModelScenarioSet)
			{
				// The Scenarios in the ScenarioSet should be deleted too, even
				// though they are not "owned by" the ScenarioSet (according to
				// the Ownership pattern). This is ok because a Scenario cannot
				// belong to more than one ScenarioSet, and so there will be no
				// unexpected outcome.
				
				// Delete (from the Domain) each Scenario in the Scenario Set.
				
				ModelDomain domain = ((ModelScenarioSet)node).getModelDomain();
				Set<ModelScenario> scenarios = ((ModelScenarioSet)node).getScenarios();
				for (ModelScenario scenario : scenarios)
				{
					scenario.prepareForDeletion();
					domain.deleteChildElement(scenario);
					
					try { scenario.notifyNodeDeleted(); }
					catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
				}
				
				// Delete the ScenarioSet
				domain.deleteChildElement((ModelScenarioSet)node);
			}
			else if (node instanceof SimRunSet)
			{
				// The SimulationRuns in the SimRunSet should be deleted too, even
				// though they are not "owned by" the SimRunSet (according to
				// the Ownership pattern). This is ok because a SimulationRun cannot
				// belong to more than one SimRunSet, and so there will be no
				// unexpected outcome.
				
				ModelDomain domain = ((SimRunSet)node).getModelDomain();
				Set<SimulationRun> simRuns = ((SimRunSet)node).getSimRuns();
				for (SimulationRun simRun : simRuns)
				{
					simRun.prepareForDeletion();
					domain.deleteChildElement(simRun);
					
					try { simRun.notifyNodeDeleted(); }
					catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
				}
			}
			else
			{
				PersistentNode parentNode = node.getParent();
				if (parentNode == null)
				{
					RuntimeException ex = new RuntimeException("Parent Node is null");
					GlobalConsole.printStackTrace(ex);
					throw ex;
				}
				
				if (node.isPredefined()) throw new ParameterError(
					"Cannot delete a predefined Node");
				
				if (! (parentNode instanceof ModelElement)) throw new ParameterError(
					"Can only delete from a Model Element, not from a " 
						+ parentNode.getClass().getName());
				
				if (node instanceof SimulationRun)
				{
					if (! (parentNode instanceof ModelScenario)) throw new ParameterError(
						"Can only delete a Simulation Run from its parent Model Scenario.");
					
					((ModelScenario)parentNode).deleteSimulationRun((SimulationRun)node);
				}
				else
				{
					ModelElement parent = (ModelElement)parentNode;
					parent.deleteSubElement((ModelElement)node);
		
					if (canDeleteSimRuns)
						deleteSimulationRuns(((ModelElement)node).getModelDomain());
				}
			}
			
			thereAreUnflushedChanges = true;

			
			// Notify clients that the Element was deleted.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeDeletedNotice(elementId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void bindStateAndPort(String clientId, boolean confirm,
		String stateId, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + stateId);
			
			if (! (node instanceof State)) throw new ParameterError(
				"Node with ID " + stateId + " is not a State");

			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
			
			node = PersistentNodeImpl.getNode(portId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + portId);
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Node with ID " + portId + " is not a Port");

			Port port = (Port)node;
			
			if (port.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(port.getFullName());
			

			// Check if the State or Port is pre-defined.
			
			if (state.isPredefined() || port.isPredefined()) throw new ParameterError(
				"Cannot bind to a predefined component");
			

			if (! confirm)
			{
				if (port.getModelDomain().hasSimulationRuns())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			
			state.bindPort(port);

			
			// Notify clients.
			
			PersistentNode container = state.getParent();
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NonStructuralChangeNotice(container.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void unbindPort(String clientId, boolean confirm, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(portId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + portId);
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Node with ID " + portId + " is not a Port");
			
			Port port = (Port)node;
			
			PortedContainer container = (PortedContainer)(port.getParent());
			
			if (container.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(container.getFullName());
			
			if (! confirm)
			{
				if (port.getModelDomain().hasSimulationRuns())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}

			Set<State> states = container.getStates();
			boolean foundBinding = false;
			for (State state : states)
			{
				if (state.getPortBindings().contains(port))
				{
					state.unbindPort(port);
					foundBinding = true;
					break;
				}
			}
			
			if (! foundBinding) throw new ParameterError("Port " + port.getFullName() + 
				" is not bound to a State within " + container.getFullName());

			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NonStructuralChangeNotice(container.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized StateSer getPortBinding(String clientId, String portId)
	throws
		ParameterError,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(portId);
			if (node == null) throw new ElementNotFound(
				"Node with ID " + portId + " not found");
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Node with ID " + portId + " is not a Port");
			
			Port port = (Port)node;
			
			State state = port.getBoundState();
			if (state == null) return null;
			
			return (StateSer)(state.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setPredefEventDefaultTime(String clientId, boolean confirm, 
		String predEventNodeId, Date time)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(predEventNodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + predEventNodeId);
			
			if (! (node instanceof PredefinedEvent)) throw new ParameterError(
				"Node with ID " + predEventNodeId + " is not a PredefinedEvent");
			
			PredefinedEvent predEvent = (PredefinedEvent)node;
			
			if (predEvent.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(predEvent.getFullName());
			
			if (! confirm)
			{
				if (predEvent.getModelDomain().hasSimulationRuns())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			((PredefinedEventImpl)predEvent).setTimeToOccur(time);
			
			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(predEventNodeId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void setPredefEventDefaultValue(String clientId, boolean confirm, 
		String predEventNodeId, Serializable value)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(predEventNodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + predEventNodeId);
			
			if (! (node instanceof PredefinedEvent)) throw new ParameterError(
				"Node with ID " + predEventNodeId + " is not a PredefinedEvent");
			
			PredefinedEvent predEvent = (PredefinedEvent)node;
			
			if (predEvent.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(predEvent.getFullName());
			
			if (! confirm)
			{
				if (predEvent.getModelDomain().hasSimulationRuns())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			((PredefinedEventImpl)predEvent).setNewValue(value);


			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(predEventNodeId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setNodeName(String clientId, boolean confirm, 
		String nodeId, String newName)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (newName == null) throw new ParameterError("Null name argument");
			if (newName.equals("")) throw new ParameterError("Blank name argument");

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + nodeId);
			
			if ((node instanceof ModelElement) && (! (node instanceof ModelDomain)) && (! confirm))
			{
				if (((ModelElement)node).getModelDomain().hasSimulationRuns())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if (node instanceof ModelDomain)
			{
				modelDomains.remove(node);
				((ModelDomainImpl)node).setNameAndNotify(newName);
				modelDomains.put(node.getName(), (ModelDomainImpl)node);
			}
			else if (node instanceof DecisionDomain)
			{
				decisionDomains.remove(node);
				((DecisionDomainImpl)node).setNameAndNotify(newName);
				decisionDomains.put(node.getName(), (DecisionDomainImpl)node);
			}
			else  // Note: the Node might be in some Maps or Sets that map the Node's name.
			{
				PersistentNode parent = node.getParent();
				if (parent == null) throw new ParameterError("Node has no parent");
				
				parent.renameChild(node, newName);
			}
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Conduit was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NameChangedNotice(nodeId, newName)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setPortDirection(String clientId, boolean confirm, 
		String portId, PortDirectionType dir)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ModelContainsError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(portId);
			
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + portId);
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Can only call this method on a Port.");
			
			Port port = (Port)node;
			
			if (port.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(port.getFullName());
			
			if (! confirm)
			{
				if (((ModelElement)node).getModelDomain().hasSimulationRuns()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
			}
			
			
			// Set the Port's direction.
			
			PortedContainer container = port.getContainer();
			if (container == null) throw new ModelContainsError(
				"Port has no parent Container");
			
			container.setPortDirection(port, dir);
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Port was changed.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(port);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(portId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ConduitSer createConduit(String clientId, boolean confirm,
		String parentId, String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Conduit conduit = createConduit_internal(confirm, parentId, portAId, portBId);
				
			thereAreUnflushedChanges = true;
				
			deleteSimulationRuns(conduit.getModelDomain());
			
			
			// Notify clients that the Conduit was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(conduit.getContainer());
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeAddedNotice(parentId, conduit.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (ConduitSer)(conduit.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ConduitSer createConduit(String clientId, boolean confirm,
		String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Conduit conduit = createConduit_internal(confirm, null, portAId, portBId);
				
			thereAreUnflushedChanges = true;
				
			deleteSimulationRuns(conduit.getModelDomain());
			

			// Notify clients that the Conduit was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(conduit.getContainer());
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeAddedNotice(conduit.getContainer().getNodeId(),
					conduit.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (ConduitSer)(conduit.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void rerouteConduit(String clientId, String conduitId)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(conduitId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + conduitId);
			
			if (! (node instanceof Conduit)) throw new ParameterError(
				"Can only call this method on a Conduit.");
			
			Conduit conduit = (Conduit)node;

			if (conduit.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(conduit.getFullName());
			

			conduit.reroute();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Conduit was modified.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(conduit);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(conduitId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void rerouteConduits(String clientId, String containerId,
		String[] conduitIds)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(containerId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + containerId);
			
			if (! (node instanceof ModelContainer)) throw new ParameterError(
				"Can only call this method on a Model Container.");
			
			ModelContainer container = (ModelContainer)node;
			
			if (container.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(container.getFullName());
			
			
			Set<Conduit> conduits = null;
			
			if (conduitIds == null) conduits = container.getConduits();
			else
			{
				conduits = new TreeSet<Conduit>();
				for (String conduitId : conduitIds)
				{
					PersistentNode cNode = PersistentNodeImpl.getNode(conduitId);
					if (cNode == null) throw new ParameterError(
						"Cannot identify the Node with ID " + conduitId);
					
					if (! (cNode instanceof Conduit)) throw new ParameterError(
						"Conduit with ID " + conduitId + " not found");
					
					Conduit conduit = (Conduit)cNode;
					
					conduits.add(conduit);
				}
			}
			
			if (conduits == null) return;
			
			for (Conduit conduit : conduits)  // each Conduit
			{
				if (conduit.getContainer() != container) throw new ParameterError(
					"Conduit " + conduit.getFullName() + " is not owned by " + 
						container.getFullName());
						
				// reroute the Conduit
				
				conduit.reroute();
			}
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Container was modified.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(container);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(containerId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized NodeSer getNode(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (nodeId == null) throw new ParameterError("Node Id is null");
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("For Node Id " + nodeId);
			
			NodeSer nodeSer = node.externalize();
			if (nodeSer == null) throw new RuntimeException("nodeSer is null");
			
			return nodeSer;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer getNode(String clientId, String nodeId, Class nodeSerKind)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (nodeId == null) throw new ParameterError("Node Id is null");
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("For Node Id " + nodeId);
			
			NodeSer nodeSer = node.externalize();
			if (nodeSer == null) throw new RuntimeException("nodeSer is null");
			
			if (! (nodeSerKind.isAssignableFrom(nodeSer.getClass())))
				throw new ParameterError("Node with ID " + nodeId +
					" is not a " + nodeSerKind.getName());
			
			return nodeSer;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	/*public synchronized NodeSer updateNode(String clientId, boolean confirm, NodeSer nodeSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeSer.getNodeId());
			
			if ((node instanceof ModelElement) && (! confirm))
			{
				if (((ModelElement)node).getModelDomain().hasSimulationRuns()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted. Continue?");
			}
			
			if ((node.getVersionNumber() > nodeSer.getVersionNumber()) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + nodeSer.getVersionNumber() +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			node = node.update(nodeSer);
			
			thereAreUnflushedChanges = true;
			
			if (node instanceof ModelScenario)
				deleteSimulationRuns(((ModelScenario)node));
			if (node instanceof ModelElement)
				deleteSimulationRuns(((ModelElement)node).getModelDomain());
			
			NodeSer newNodeSer = node.externalize();

			return newNodeSer;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}*/
	
	
	public synchronized ModelScenarioSer updateModelScenario(String clientId, boolean confirm, 
		ModelScenarioSer scenarioSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(scenarioSer.getNodeId());
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with ID " + scenarioSer.getNodeId() + " is not a ModelScenario");
			
			ModelScenario scenario = (ModelScenario)node;
			
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
			
			if (! confirm)
			{
				if (scenario.hasSimulationRuns()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted. Continue?");
			}
			
			if ((scenario.getVersionNumber() > scenarioSer.getVersionNumber()) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + 
					scenarioSer.getVersionNumber() +
					" (created by client " + clientId +
					") is older than server version " + scenario.getVersionNumber() +
					" (created by client " + scenario.getClientThatLastModified() +
					" at " + (new Date(scenario.getLastUpdated())) +
					") for Node " + scenario.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			scenario.update(scenarioSer);
			
			thereAreUnflushedChanges = true;
			
			deleteSimulationRuns(scenario);
			
			
			// Notify clients.
			
			try { node.notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(node.getNodeId())); }
			catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			
			
			ModelScenarioSer newScenarioSer = (ModelScenarioSer)(scenario.externalize());

			return newScenarioSer;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized NodeSer incrementNodeSize(String clientId,
		int lastRefreshed, String nodeId,
		double dw, double dh, double childShiftX, double childShiftY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			node.setWidth(node.getWidth() + dw);
			node.setHeight(node.getHeight() + dh);
			
			if (node instanceof ModelElement)
				((ModelElement)node).shiftAllSubElements(childShiftX, childShiftY);
			
			if (node instanceof ModelContainer)  // Reroute all Conduits belonging
				// to the Node as well as to the Node's parent (if any).
			{
				ModelContainer container = (ModelContainer)node;
				container.rerouteAllConduits();
				
				PersistentNode containerParent = container.getParent();
				if (containerParent != null)
					if (containerParent instanceof ModelContainer)
						((ModelContainer)containerParent).rerouteAllConduits();
			}
				
			
			node.update();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			try { node.notifyAllListeners(new PeerNoticeBase.NodeResizedNotice(node.getNodeId())); }
			catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			// This will require the client to refresh the entire Node.
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer incrementNodeLocation(String clientId, 
		int lastRefreshed, String nodeId,
		double dx, double dy)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			if (node instanceof Port)  // Ports have special positioning rules.
			{
				Port port = (Port)node;
				
				double newX = node.getX() + dx;
				double newY = node.getY() + dy;
				
				PortedContainer container = port.getContainer();
				
				Position newPos = container.findSideClosestToInternalPoint(newX, newY);
				
				port.setSide(newPos);
			}
			else
			{
				node.getDisplay().setX(node.getX() + dx);
				node.getDisplay().setY(node.getY() + dy);
			}
			
			node.update();
			
			thereAreUnflushedChanges = true;
			
			if (node instanceof Port)
			{
				Port port = (Port)node;
				PortedContainer container = port.getContainer();
				PersistentNode containerContainer = container.getParent();
				try { node.notifyAllListeners(
					new PeerNoticeBase.NodeChangedNotice(containerContainer.getNodeId())); }
				catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			}
			else
				try { node.notifyAllListeners(new PeerNoticeBase.NodeMovedNotice(nodeId)); }
				catch (Exception nex) { GlobalConsole.printStackTrace(nex); }

			
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized NodeSer changeNodeSize(String clientId, int lastRefreshed, 
		String nodeId,
		double newWidth, double newHeight, double childShiftX, double childShiftY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			node.setWidth(newWidth);
			node.setHeight(newHeight);
			
			if (node instanceof ModelElement)
				((ModelElement)node).shiftAllSubElements(childShiftX, childShiftY);
			
			if (node instanceof ModelContainer)  // Reroute all Conduits belonging
				// to the Node as well as to the Node's parent (if any).
			{
				ModelContainer container = (ModelContainer)node;
				container.rerouteAllConduits();
				
				PersistentNode containerParent = container.getParent();
				if (containerParent != null)
					if (containerParent instanceof ModelContainer)
						((ModelContainer)containerParent).rerouteAllConduits();
			}

			node.update();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			try { node.notifyAllListeners(new PeerNoticeBase.NodeResizedNotice(node.getNodeId())); }
			catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			// This will require the client to refresh the entire Node.
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer changeNodeLocation(String clientId,
		int lastRefreshed, String nodeId,
		double newX, double newY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			node.getDisplay().setX(newX);
			node.getDisplay().setY(newY);
			
			node.update();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			try { node.notifyAllListeners(new PeerNoticeBase.NodeMovedNotice(node.getNodeId())); }
			catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized String resolveNodePath(String clientId, String path)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			return resolveNodePath_internal(path);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized Set<NodeSer> getChildren(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			throw new RuntimeException("Not implemented");
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized String[] getChildNodeIds(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			return getChildNodeIds_internal(nodeId);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized AttributeSer getAttribute(String clientId, String modelElementId, 
		String attrName)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(modelElementId);
			if (node == null) throw new ElementNotFound("Node with ID " + modelElementId);
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"The specified Node is not a Model Element, so it has no Attribute.");
			
			Attribute attribute = ((ModelElement)node).getAttribute(attrName);
			if (attribute == null) throw new ElementNotFound("Attribute " + attrName);
			
			return (AttributeSer)(attribute.externalize());
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized AttributeSer[] getAttributes(String clientId, 
		String modelElementId, Class c, String modelScenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(modelElementId);
			if (node == null) throw new ElementNotFound("Node with ID " + modelElementId);
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"The specified Node is not a Model Element, so it has no Attribute.");
			ModelElement element = (ModelElement)node;
			
			node = PersistentNodeImpl.getNode(modelScenarioId);
			if (node == null) throw new ElementNotFound("Node with ID " + modelScenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"The specified Node is not a Model Element, so it has no Attribute.");
			ModelScenario scenario = (ModelScenario)node;
			
			ModelDomain modelDomain = scenario.getModelDomain();
			
			Collection<Attribute> attributes = 
				element.getAttributes().values();
				
			List<AttributeSer> attrSers = new Vector<AttributeSer>();
			
			for (Attribute attr : attributes)
			{
				Serializable value = scenario.getAttributeValue(attr);
				
				if (value == null) continue;
				
				if (c.isAssignableFrom(value.getClass())) attrSers.add(
					(AttributeSer)(attr.externalize()));
			}
		
			return attrSers.toArray(new AttributeSer[attrSers.size()]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized ModelElementSer[] getElementsWithAttributeDeep(String clientId, 
		String modelElementId, Class c, String modelScenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(modelElementId);
			if (node == null) throw new ElementNotFound("Node with ID " + modelElementId);
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"The specified Node is not a Model Element, so it has no Attribute.");
			
			ModelElement element = (ModelElement)node;
			
			node = PersistentNodeImpl.getNode(modelScenarioId);
			if (node == null) throw new ElementNotFound("Node with ID " + modelScenarioId);
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"The specified Model Scenario ID is not a Model Scenario.");
			
			ModelScenario scenario = (ModelScenario)node;
			
			List<ModelElement> elements = new Vector<ModelElement>();
			getElementsWithAttributeRecursive(elements, element, c, scenario);
			
			List<ModelElementSer> elementSers = new Vector<ModelElementSer>();
			for (ModelElement e : elements)
			{
				elementSers.add((ModelElementSer)(e.externalize()));
			}
			
			return elementSers.toArray(new ModelElementSer[elementSers.size()]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized AttributeSer[] getAttributesDeep(String clientId, String modelElementId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(modelElementId);
			if (node == null) throw new ElementNotFound("Node with ID " + modelElementId);
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"The specified Node is not a Model Element, so it has no Attribute.");
			
			ModelElement element = (ModelElement)node;
			
			List<ModelElement> elements = new Vector<ModelElement>();
			getElementsWithAttributeRecursive(elements, element, null, null);
			
			Set<Attribute> attrs = new TreeSet<Attribute>();
			for (ModelElement e : elements)
			{
				attrs.addAll(e.getAttributes().values());
			}
			
			AttributeSer[] attrSers = new AttributeSer[attrs.size()];
			int i = 0;
			for (Attribute attr : attrs)
				attrSers[i++] = (AttributeSer)(attr.externalize());
			
			return attrSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<String> getProgress(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ServiceThread thread = threads.get(new Integer(callbackId));

			if (thread == null)  // not found.
				throw new CallbackNotFound();

			return thread.getProgress();
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<String> getMessages(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ServiceThread thread = threads.get(new Integer(callbackId));

			if (thread == null)  // not found.
				throw new CallbackNotFound();

			return thread.getMessages();
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized boolean isComplete(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			return isComplete(callbackId);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized boolean isAborted(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			return isAborted(callbackId);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void abort(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			abort(callbackId);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized Integer simulate(String clientId,
		String modelDomainName, 
		String scenarioName, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int runs, long duration, 
		PeerListener peerListener, boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (! allowSimulations) throw new CannotObtainLock(
				"Simulations are currently disabled");
			
			if (modelDomainName == null) throw new ParameterError(
				"Null modelDomainName");
		
			ModelDomain modelDomain = modelDomains.get(modelDomainName);
	
			if (modelDomain == null) throw new ElementNotFound();
			
			ModelScenario modelScenario = null;
			if ((scenarioName == null) || scenarioName.equals(""))
				// use current scenario for the domain.
				modelScenario = modelDomain.getCurrentModelScenario();
			else
				modelScenario = modelDomain.getModelScenario(scenarioName);
			
			if (modelScenario == null) throw new ParameterError(
				"Unidentified scenario");
			
			
			if (initialEpoch == null) initialEpoch = new Date();
			
			
			/* Create and start a monitoring thread, to start the simulations in
			 batches and monitor them. The monitoring thread is a ServiceThread
			 and so it automatically inherits the ServiceContext of this thread. */
			
			Set<ModelScenario> modelScenarios = new TreeSet<ModelScenario>();
			modelScenarios.add(modelScenario);
			Integer threadHandle = createMonitorThreadHandle();
			SimulationMonitorThread thread = new SimulationMonitorThread(threadHandle, 
				modelScenarios, initialEpoch, finalEpoch, iterations, runs, duration,
				peerListener, repeatable);
			
			thread.setDaemon(true);
			thread.start();
			
			thereAreUnflushedChanges = true;
			
			threads.put(threadHandle, thread);
			
			return threadHandle;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSetSer createScenariosOverRange(String clientId,
		String baseScenId, String attrId, Serializable[] values)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(baseScenId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify Scenario with Id " + baseScenId);
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + baseScenId + " is not a Model Scenario");
			
			ModelScenario baseScenario = (ModelScenario)node;
			
			
			node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify Attribute with Id " + attrId);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Node with Id " + attrId + " is not an Attribute");
			
			Attribute attr = (Attribute)node;
			
			
			// Create a VariableScenarioSet over the range of the Attribute's allowed value.
			
			ModelScenarioSet scenarioSet = null;
			try { scenarioSet =
				baseScenario.createModelScenarioSet(null /* name */, attr, values);
			} catch (CloneNotSupportedException ex) { throw new ModelContainsError(ex); }
			
			
			// Notify

			PeerNotice notice = new PeerNoticeBase.ScenarioSetCreatedNotice(
				scenarioSet.getModelDomain().getNodeId(), scenarioSet.getNodeId());
					
			try { callbackManager.notifyAllListeners(notice); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			
			return (ModelScenarioSetSer)(scenarioSet.externalize());
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Integer simulateScenarioSet(String clientId,
		String scenSetId, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int noOfRuns, long duration, 
		PeerListener peerListener, boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(scenSetId);
			if (node == null) throw new ElementNotFound(
				"Could not identify Node with Id " + scenSetId);
			
			if (! (node instanceof ModelScenarioSet)) throw new ParameterError(
			"Node with Id " + scenSetId + " is not a Variable Model Scenario Set");
			
			ModelScenarioSet scenSet = (ModelScenarioSet)node;
			
			
			/* Create and start a monitoring thread, to start the simulations in
			 batches and monitor them. The monitoring thread is a ServiceThread
			 and so it automatically inherits the ServiceContext of this thread. */
			
			Integer threadHandle = createMonitorThreadHandle();
			SimulationMonitorThread thread = new SimulationMonitorThread(threadHandle, 
				scenSet.getScenarios(), initialEpoch, finalEpoch, iterations, noOfRuns, duration,
				peerListener, repeatable);
			
			thread.setDaemon(true);
			thread.start();
			
			thereAreUnflushedChanges = true;
			
			threads.put(threadHandle, thread);
			
			return threadHandle;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
		
	public synchronized double[] computeAssurances(String clientId, String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(scenSetId);
			if (node == null) throw new ElementNotFound(
				"Could not identify Node with Id " + scenSetId);
			
			if (! (node instanceof ModelScenarioSet)) throw new ParameterError(
			"Node with Id " + scenSetId + " is not a Variable Model Scenario Set");
			
			ModelScenarioSet scenSet = (ModelScenarioSet)node;
			
			if (scenSet.isLockedForSimulation()) 
				throw new CannotObtainLock(scenSet.getFullName());
			
			return scenSet.computeAssurances();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	/** ************************************************************************
	 * This implementation is synchronous, and returns as soon as the request
	 * has completed.
	 */

	public synchronized Integer[] updateVariable(String clientId, int lastRefreshed,
		String decisionDomainName,
		String scenarioName, String decisionPointName, String variableName,
		Vector<Serializable> valueVector)
	throws
		ClientIsStale,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			/*
			 * Locate the named Variable.
			 */
	
			if (variableName == null) throw new ParameterError(
				"updateVariable: Null Variable name specified");
	
			if (valueVector.size() != 1) throw new ParameterError(
				"Parameter valueVector must contain a single element (it contains " +
				valueVector.size() + ")");
	
			Serializable value = valueVector.get(0);
	
			/*
			GlobalConsole.println(
				"Entered updateVariable(" + decisionDomainName + ", " +
					decisionPointName + ", " + variableName + ", " + value + 
					"(" + value.getClass().getName() + "))...");
			*/
	
			Variable variable = null;
	
			DecisionDomain decisionDomain = decisionDomains.get(decisionDomainName);
	
			/*
			GlobalConsole.println("domainName=" + decisionDomainName);
			for (java.util.Map.Entry<String, DecisionDomain> entry : decisionDomains.entrySet())
				GlobalConsole.println(
					"Key: " + entry.getKey() + ", " +
					"Value name: " + entry.getValue().getName());
			*/
	
			if (decisionDomain == null) throw new ElementNotFound(
				"Cannot locate DecisionDomain " + decisionDomainName);
	
	
			DecisionPoint decisionPoint =
				decisionDomain.getDecisionPoint(decisionPointName);
	
			if (decisionPoint != null)
			{
				Parameter parameter = decisionPoint.getVariable(variableName);
	
				if (parameter != null)
				{
					variable = (Variable)parameter;
				}
			}
	
			if (variable == null) throw new ElementNotFound(
				decisionDomainName + ":" + decisionPointName + "." + variableName);
	
	
			/*
			 * Create a new DecisionScenario to represent (contain) the new
			 * Decisions that result. Create the new scenario by
			 * cloning the specified one, or the current one if none specified,
			 * or create a blank one if neither of these exist. Then, set the
			 * new scenario to be the current one for the DecisionDomain.
			 */
	
			DecisionScenario baseScenario = null;
			if (scenarioName != null)
			{
				// Try to find the specified Scenario.
				baseScenario = decisionDomain.getDecisionScenario(scenarioName);
			}
	
			if (baseScenario == null)
			{
				baseScenario = decisionDomain.getCurrentDecisionScenario();
			}
			
			DecisionScenario decisionScenario = null;
			try
			{
				decisionScenario = decisionDomain.createScenario(
					decisionDomain.getName(), baseScenario);
			}
			catch (ParameterError pe)
			{
				throw new RuntimeException(pe);
			}
			catch (CloneNotSupportedException ex)
			{
				throw new ModelContainsError("Unable to create a DecisionScenario", ex);
			}
			
			thereAreUnflushedChanges = true;
	
			decisionDomain.setCurrentDecisionScenario(decisionScenario);
	
	
			/*
			 * Define the Variable's new value as a Choice within the
			 * DecisionScenario.
			 */
	
			decisionScenario.makeChoice(variable, value);
	
	
			/*
			 * Re-compute any affected decisions in this DecisionDomain.
			 */
			
			Set<Decision> decisions = null;
			SynchronousDecisionCallback syncDecisionCallback =
				new SynchronousDecisionCallback();
	
			try
			{
				decisions = decisionScenario.getDecisionDomain().updateDecisions(
					decisionScenario, syncDecisionCallback,
					new TreeSet<Variable>(Arrays.asList(variable)));
			}
			catch (ModelContainsError ex)
			{
				syncDecisionCallback.showMessage(ex.getMessage());
				throw ex;
			}
			catch (RuntimeException re)
			{
				syncDecisionCallback.showMessage(
					"RuntimeException in decision thread; aborting; " +
						re.getMessage());
	
				GlobalConsole.printStackTrace(re);
				//abortThread();
				throw new InternalEngineError(re);
			}
			finally
			{
				if (decisions != null) syncDecisionCallback.addDecisions(decisions);
			}
	
	
			/*
			 * Determine if any Decisions in this DecisionDomain have changed.
			 * This is done to provide impact analysis to the client.
			 */
	
			if (decisions.isEmpty())
			{
				syncDecisionCallback.showMessage(
					"No decision changes for " + decisionDomain.getName());
			}
			else
			{
				syncDecisionCallback.showMessage(
					"New decisions for these DecisionPoints:");
	
				for (Decision decision : decisions)
				{
					DecisionPoint dp = decision.getDecisionPoint();
					//syncDecisionCallback.showMessage("\t" + dp.getName());
	
					Variable v = decision.getVariable();
					Object val = decision.getValue();
					//syncDecisionCallback.showMessage("\t\t" + v.getName() + "=" +
					//	val.toString() +
					//	"(" + val.getClass().getName() + ")");
				}
			}
	
			
			/*
			 * Determine if propagation to other models is requried.
			 */
			
			Integer[] newThreadHandles = new Integer[0];
			
			
			/*
			 * Identify the set of Model Domains that have Attributes that
			 * are bound to the Variable. These are the "dependent" models.
			 */
	
			Set<ModelDomain> modelDomains = new TreeSet<ModelDomain>();
			Set<VariableAttributeBinding> varAttrBindings = 
				variable.getAttributeBindings();
	
			for (VariableAttributeBinding binding : varAttrBindings)
			{
				Attribute attribute = binding.getAttribute();
				ModelDomain domain = attribute.getModelElement().getModelDomain();
				
				modelDomains.add(domain);
			}
	
	
			/*
			 * Start a simulation for each dependent ModelDomain, and return a set
			 * of handles to these simulations.
			 */
	
	
			// Create the Thread that will compute the new element state and update
			// the DecisionDomains.
	
			newThreadHandles = new Integer[modelDomains.size()];
	
	
			// Start each simulation.
	
			int i = 0;
			for (ModelDomain modelDomain : modelDomains)
			{
				ModelScenario modelScenario = modelDomain.getCurrentModelScenario();
	
	
				//
				// Create a ModelScenario to use for the simulation.
	
				String newName = modelDomain.getName();
	
				ModelScenario newModelScenario = modelScenario;
	
				try
				{
					newModelScenario = modelDomain.createScenario(newName, 
						modelScenario);
	
					modelDomain.setCurrentModelScenario(newModelScenario);
				}
				catch (ParameterError pe)
				{
					throw new ModelContainsError(
						"Context already contains a Scenario for " +
							modelDomain.getName());
				}
				catch (CloneNotSupportedException ex)
				{
					throw new ModelContainsError(ex);
				}
	
	
				//
				// Simulate.
				
				//newThreadHandles[i++] = 
				//	sim(newModelScenario, 
				//		propagate,
				//		decisionScenario);
			}
	
	
			// Return the simulation handles so that the client can monitor
			// and manage the simulations, and request the results when they
			// have completed.
			
			return newThreadHandles;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized List<double[]> getResultStatistics(String clientId, 
		List<String> nodePaths,
		String modelDomainName, String modelScenarioName, String[] optionsStrings)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Parse the options.
						
			List<StatOptionsType> options = parseStatOptionString(optionsStrings);
		
	
			// Find the specified ModelScenario.
			
			ModelDomain domain = modelDomains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			
			ModelScenario scenario = domain.getModelScenario(modelScenarioName);
			if (scenario == null) throw new ElementNotFound(modelScenarioName);
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
			
	
			// Find the elements specified by the nodePaths.
		
			List<PersistentNode> nodes = new Vector<PersistentNode>();
	
			int nodeNo = 0;
			for (String nodePath : nodePaths)
			{
				PersistentNode node = domain.findElement(nodePath);
				if (node == null) throw new ElementNotFound(nodePath + " not found");
				if (! (node instanceof State)) throw new ParameterError(
					"Node must be a State");
	
				nodes.add(node);
				nodeNo++;
			}
			
			
			// Compute the statistics for each State Node.
			
			List<double[]> statsArrayList = new Vector<double[]>();
			for (PersistentNode node : nodes)
			{
				if (! (node instanceof State)) throw new ParameterError(
					"Statistics can only be computed on a State's value");
				
				State state = (State)node;
				
				double[] stats = this.getResultStatistics_internal(state,
					scenario, options);
				
				statsArrayList.add(stats);
			}
				
	
			return statsArrayList;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<double[]> getResultStatisticsById(String clientId, 
		List<String> nodeIds,
		String modelScenarioId, String[] optionsStrings)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Parse the options.
						
			List<StatOptionsType> options = parseStatOptionString(optionsStrings);
		
	
			// Identify the Model Scenario.
			
			PersistentNode node = PersistentNodeImpl.getNode(modelScenarioId);
			if (node == null) throw new ElementNotFound(modelScenarioId + " Id not found");
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + modelScenarioId + " is not a Model Scenario");
			
			ModelScenario scenario = (ModelScenario)node;
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
			

			// Find the elements specified by the nodePaths.
		
			List<State> states = new Vector<State>();
	
			int nodeNo = 0;
			for (String nodeId : nodeIds)
			{
				node = PersistentNodeImpl.getNode(nodeId);
				if (node == null) throw new ElementNotFound("Node Id " + nodeId + " not found");
				if (! (node instanceof State)) throw new ParameterError(
					"Node must be a State");
	
				states.add((State)node);
				nodeNo++;
			}
			
			
			// Compute the statistics for each State Node.
			
			List<double[]> statsArrayList = new Vector<double[]>();
			for (State state : states)
			{
				double[] stats = this.getResultStatistics_internal(state,
					scenario, options);
				
				statsArrayList.add(stats);
			}
				
	
			return statsArrayList;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized double[] getResultStatisticsById(String clientId, String nodeId,
		String modelScenarioId, String[] optionsStrings)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Parse the options.
						
			List<StatOptionsType> options = parseStatOptionString(optionsStrings);
		
	
			// Find the specified ModelScenario.
			
			PersistentNode node = PersistentNodeImpl.getNode(modelScenarioId);
			if (node == null) throw new ElementNotFound(nodeId + " Id not found");
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + modelScenarioId + " is not a Model Scenario");
			
			ModelScenario scenario = (ModelScenario)node;
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
			
			node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound(modelScenarioId + " Id not found");
			if (! (node instanceof State)) throw new ParameterError(
				"Statistics can only be computed on a State's value");
				
			State state = (State)node;
			
			double[] stats = this.getResultStatistics_internal(state,
				scenario, options);
	
			return stats;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<int[]> getHistograms(String clientId, 
		List<String> statePaths,
		String modelDomainName, String modelScenarioName, double bucketSize,
		double bucketStart, int noOfBuckets, boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);


			// Find the specified ModelScenario.
			
			ModelDomain domain = modelDomains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			
			ModelScenario scenario = domain.getModelScenario(modelScenarioName);
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
	
			// Identify the states.
			
			State[] states = new State[statePaths.size()];
			int noOfStates = 0;
			for (String statePath : statePaths)
			{
				ModelElement me = domain.findElement(statePath);
				if (me == null) throw new ElementNotFound(
					"Specified state " + statePath + " not found");
	
				try { states[noOfStates++] = (State)me; }
				catch (ClassCastException cce) { throw new ElementNotFound(
					"Specified state " + statePath + " not found"); }
			}
			
			
			// Tally the histograms.
			
			List<int[]> histograms = new Vector<int[]>();
			
			for (State state : states) // each State
			{
				int[] histogram = getHistogram_internal(state, scenario, bucketSize,
					bucketStart, noOfBuckets, allValues);
				
				histograms.add(histogram);
			}
			
			return histograms;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized int[] getHistogramById(String clientId, String stateId,
		String modelScenarioId, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(modelScenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node " + modelScenarioId + " is not a Model Scenario");
			
			ModelScenario scenario = (ModelScenario)node;
			
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
			node = PersistentNodeImpl.getNode(stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Node " + modelScenarioId + " is not a State");
			
			State state = (State)node;
			
			int[] histogram = getHistogram_internal(state, scenario, bucketSize,
				bucketStart, noOfBuckets, allValues);
				
			return histogram;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized HistogramDomainParameters defineOptimalHistogramById(
		String clientId, String stateId, String modelScenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound("State Id " + stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Id " + stateId + " is not a State");
			
			State state = (State)node;
			
			node = PersistentNodeImpl.getNode(modelScenarioId);
			if (node == null) throw new ElementNotFound("Scenario Id " + modelScenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Id " + modelScenarioId + " is not a Model Scenario");
			
			ModelScenario scenario = (ModelScenario)node;
			
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
			
			// Get the final value for each simulation of the State and Scenario.
			
			Serializable[] values = scenario.getStateFinalValues(state);
			
			
			// Determine the width of the domain of values.
			
			int noOfValues = values.length;
			double minValue = 0.0;
			double maxValue = 0.0;
			for (int i = 0; i < noOfValues; i++)
			{
				Serializable v = values[i];
				if (! (v instanceof Number)) throw new ParameterError(
					"Numeric graph cannot be made of this State, which has value " +
						(v == null ? "null" : v.toString()) + " in simulation " + i + 
							" (from 0) of Scenario " + scenario.getName());
				
				Number value = (Number)v;
				double doubleValue = value.doubleValue();
				
				if (i == 0) { minValue = doubleValue; maxValue = doubleValue; }
				else
				{
					minValue = Math.min(minValue, doubleValue);
					maxValue = Math.max(maxValue, doubleValue);
				}
			}
			
			double domainWidth = maxValue - minValue;
			
			
			// Select a bucket size such that there are at least a few hits in most
			// buckets, and there are at least five buckets.
			
			int noOfBuckets = noOfValues / 3;  // 3 is "a few"
			if (noOfBuckets < 5) // increase the number of buckets so that there
				noOfBuckets = 5;		// are at least 5 buckets.
			
			double bucketSize = domainWidth / noOfBuckets;
			
			
			// Address the case in which the values are clustered such that the
			// bucketSize is zero.
			
			if (bucketSize == 0.0)
			{
				// Arbitrarily choose a bucket size that is equal to the minValue
				// divided by the number of buckets.
				
				bucketSize = minValue / noOfBuckets;
			}
			
			noOfBuckets++;  // because we will shift them all left, to center them
				// and in the process the right-most values are not in a bucket.
			
			
			// Create container for returning results.
			
			HistogramDomainParameters histoParams = new HistogramDomainParameters();
			
			histoParams.bucketSize = bucketSize;
			histoParams.bucketStart = minValue - bucketSize/2.0;
			histoParams.noOfBuckets = noOfBuckets;
	
			return histoParams;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized TimeSeriesParameters defineOptimalValueHistoryById(
		String clientId, String stateId, String modelScenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound("State Id " + stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Id " + stateId + " is not a State");
			
			State state = (State)node;
			
			node = PersistentNodeImpl.getNode(modelScenarioId);
			if (node == null) throw new ElementNotFound("Scenario Id " + modelScenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Id " + modelScenarioId + " is not a Model Scenario");
			
			ModelScenario scenario = (ModelScenario)node;
			
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
			
			// Get the Event history for each simulation of the State and Scenario.
			
			List<SimulationRun> simRuns = 
					new Vector<SimulationRun>(scenario.getSimulationRuns());
			int noOfSimRuns = simRuns.size();
			
			// Values to be determined.
			long minTimeDelta = 0;
			//long maxDuration = 0;
			//long earliestStartTime = 0;
			long latestStartTime = 0;
			long earliestEndTime = 0;
			
			// Working variables.
			int simRunNo = 0;
			long time = 0;
			long priorTime = 0;
			boolean firstTimeThrough = true;
			
			for (SimulationRun simRun : simRuns)
			{
				//maxDuration = Math.max(maxDuration, simRun.getDuration());
				
				latestStartTime = Math.max(latestStartTime, simRun.getActualStartTime());
				
				if (firstTimeThrough) earliestEndTime = simRun.getActualEndTime();
				else earliestEndTime = Math.min(earliestEndTime, simRun.getActualEndTime());
				
				/*GlobalConsole.println("Simulation " + simRun.getName() + " is from " +
					(new Date(simRun.getActualStartTime())) + " to " + (new Date(simRun.getActualEndTime())));
					
					GlobalConsole.println("\tlatestStartTime=" + latestStartTime);
					GlobalConsole.println("\tearliestEndTime=" + earliestEndTime);*/
				
				
				// Determine the minimum time delta between any Events.
				
				List<SimulationTimeEvent> eventsForSimRun = simRun.getEvents(state);
				for (SimulationTimeEvent event : eventsForSimRun)
				{
					/*
					Serializable oValue = event.getNewValue();
					if (! (oValue instanceof Number)) throw new ParameterError(
						"Value '" + oValue + "' must be a Number in order to display it");
					
					double value = ((Number)oValue).doubleValue();
					*/
					
					time = event.getEpoch().getTime();
					
					long timeDelta = time - priorTime;
					if (timeDelta != 0)  // ignore zero deltas
						if (minTimeDelta == 0) minTimeDelta = timeDelta;  // capture first delta
						else if (firstTimeThrough) minTimeDelta = time - priorTime;
							else minTimeDelta = Math.min(minTimeDelta, time - priorTime);
					
					
					//if (firstTimeThrough) earliestStartTime = time;
					//else earliestStartTime = Math.min(earliestStartTime, time);
					
					priorTime = time;
					firstTimeThrough = false;
				}
			}
			
			
			long duration = earliestEndTime - latestStartTime;		
			
			if (duration < 0) throw new ParameterError(
				"No time period could be found that is spanned by every simulation");
			
			if (duration == 0) throw new ParameterError(
				"The time span shared by all simulations is of zero duration");
			
			
			// Determine the granularity of time, based on the shortest interval over
			// which the State changes in any simulation.
			
			if (minTimeDelta == 0) throw new ParameterError(
				"Elapsed time for all simulations is 0; cannot compute time-based averages");
			
			double d = minTimeDelta * DateAndTimeUtils.MsInADay;
			if (d > (double)Long.MAX_VALUE) throw new ParameterError(
				"Time interval of " + d + "ms exceeds allowed range of " + Long.MAX_VALUE);
			
			long shortestChangeInterval = (long)d;
			
			if (shortestChangeInterval == 0) throw new ParameterError(
				"Events do not span any time");
			
			long noOfPeriods = duration / shortestChangeInterval;
			noOfPeriods = Math.min(30, noOfPeriods);  // Prevent an intractable
															// number of periods.
			noOfPeriods = Math.max(5, noOfPeriods);
			long periodDuration = duration / noOfPeriods;
			
			
			// Create container for returning results.
			
			TimeSeriesParameters params = new TimeSeriesParameters();
			params.periodDuration = periodDuration;
			params.startTime = latestStartTime;
			params.noOfPeriods = (int)noOfPeriods;
			
			return params;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized double[][] getExpectedValueHistory(String clientId, String stateId, 
		String modelScenarioId, Date startingEpoch, int noOfPeriods, long periodInMs)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if (periodInMs <= 0) throw new ParameterError("Period must be positive");
			
			PersistentNode node = PersistentNodeImpl.getNode(modelScenarioId);
			if (node == null) throw new ElementNotFound("Node Id " + modelScenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node " + modelScenarioId + " is not a Model Scenario");
			
			ModelScenario modelScenario = (ModelScenario)node;
			
			node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound("Node Id " + stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Node " + stateId + " is not a State");
			
			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
				
			double[][] result = new double[noOfPeriods][];
			
			List<SimulationRun> simRuns = 
					new Vector<SimulationRun>(modelScenario.getSimulationRuns());
			if (simRuns.size() == 0) throw new ParameterError("No simulation runs");
			
			long startTime = startingEpoch.getTime();
			if (periodInMs == Long.MAX_VALUE) throw new ParameterError(
				"Period exceeds maximum that can be supported by the long data type");
			
			for (int p = 0; p < noOfPeriods; p++)
			{
				long endTime = startTime + periodInMs;
				
				double[] timeAveragedValues = new double[simRuns.size()];
				double lowestValue = 0.0;
				double highestValue = 0.0;
				
				double sumOfTimeAveragedValues = 0.0;
				int simRunNo = 0;
				for (SimulationRun simRun : simRuns)
				{
					// Identify any changes to the State's value that occur within the
					// period. Based on these Events, compute the time-averaged value of
					// the State for the period in the Scenario.
					
					double timeAveragedValue;
					try { timeAveragedValue = simRun.computeTimeAveragedValue(state,
						startTime, endTime); }
					catch (SimulationRun.UndefinedValue uv)
					{
						throw new ParameterError(uv);
					}
						
					timeAveragedValues[simRunNo] = timeAveragedValue;
					sumOfTimeAveragedValues += timeAveragedValue;
	
					if (simRunNo == 0)
					{
						lowestValue = timeAveragedValue;
						highestValue = timeAveragedValue;
					}
					else
					{
						lowestValue = Math.min(lowestValue, timeAveragedValue);
						highestValue = Math.max(highestValue, timeAveragedValue);
					}
					
					simRunNo++;
				}
				
				double mean = sumOfTimeAveragedValues / timeAveragedValues.length;
				double median = timeAveragedValues[timeAveragedValues.length/2];
				
				double variance = 0.0;
				for (double timeAveragedValue : timeAveragedValues)
				{
					double delta = timeAveragedValue - mean;
					variance += delta * delta;
				}
				
				variance /= timeAveragedValues.length;
				double sigma = Math.sqrt(variance);
				
				
				// Compute the distribution of the average values.
				
				// Basic approach (tabulate percentiles):
				// Order the simRuns by time-averaged-value, lowest first.
				// Starting with the lowest value, sum the runs that produce that
				// value or lower, until the number of runs is one standard deviation
				// from the median, based on a normal distribution. (This would be
				// where 34.1% of the values are below the median. Two sds would be 
				// where (34.1+13.6)% of the values are below the median. Three sds
				// would be where (2.1+34.1+13.6)% of the values are below the median.)
				// Continue on to find the values for which the samples are one, two,
				// and three sds above the median. (See
				// http://en.wikipedia.org/wiki/File:Standard_deviation_diagram.svg
				// for a diagram.)
				
				int noBelow15pt9Pct = 0;
				int noBelow84pt1Pct = 0;
				double countAt15pt9Percentile = 15.9 * timeAveragedValues.length;
				double countAt84pt1Percentile = 84.1 * timeAveragedValues.length;
				double percentile15pt9 = timeAveragedValues[0];
				double percentile84pt1 = timeAveragedValues[0];
				int count = 0;
				for (double timeAveragedValue : timeAveragedValues)
				{
					count++;
					if (count < countAt15pt9Percentile) percentile15pt9 = timeAveragedValue;
					if (count < countAt84pt1Percentile) percentile84pt1 = timeAveragedValue;
				}
				
				
				// Advanced approach that does not require a normal distribution
				// of mean values and that produces a piece-wise continuous function
				// that can be used in a mathematical optimization algorithm:
				// First create a uniform "clamped" interpolating spline in which 
				// one polynomial is used for each period. (See CubicSpline.html
				// in Sources.)
				/*
				...
				PolynomialSplineFunction spline = ..
				PolynomialFunction[] polynomials = spline.getPolynomials();
				*/
				
				
				// Compute statistics, using the distribution.
				
				double[] stats = new double[7];
				
				stats[0] = percentile15pt9;
				stats[1] = mean;
				stats[2] = percentile84pt1;
				stats[3] = lowestValue;
				stats[4] = highestValue;
				stats[5] = sigma;
				stats[6] = median;
				
	
				result[p] = stats;
				
				startTime = endTime;
			}
			
			return result;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized double[] getCorrelations(String clientId, List<String[]> statePaths,
		String modelDomainName, String modelScenarioName, String optionString)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if ((statePaths == null) || (statePaths.size() == 0)) throw new ParameterError(
				"No state paths");
			
			StatCorrelationType option = StatCorrelationType.cc;
			if (optionString == null) option = StatCorrelationType.cc;
			else if (optionString.equals("cv")) option = StatCorrelationType.cv;
			else if (optionString.equals("cc")) option = StatCorrelationType.cc;
			else throw new ParameterError("Option must be one of cc or cv");
			
			// Find the specified ModelScenario.
			
			ModelDomain domain = modelDomains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			
			ModelScenario scenario = domain.getModelScenario(modelScenarioName);
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
			// Define containers for the States that are identfied by the
			// state paths.
			
			State[] states1 = new State[statePaths.size()];
			State[] states2 = new State[statePaths.size()];
	
			
			// Iterate over each state pair, and compute their correlation.
			
			double[] results = new double[statePaths.size()];
			int i = 0;
			for (String[] pair : statePaths)  // each state path pair i
			{
				if (pair.length != 2) throw new InternalEngineError(
					"The length of a state path pair is " + pair.length);
				
				i++;
				
				ModelElement me0 = domain.findElement(pair[0]);
				if (me0 == null) throw new ElementNotFound("States " + pair[0] + " not found");
	
				ModelElement me1 = domain.findElement(pair[1]);
				if (me1 == null) throw new ElementNotFound("States " + pair[1] + " not found");
	
				try
				{
					states1[i-1] = (State)me0;
					states2[i-1] = (State)me1;
				}
				catch (ClassCastException cce) { throw new ElementNotFound(
					"Specified states not found"); }
				
				SimpleRegression regression = new SimpleRegression();
				
				List<SimulationRun> runs = 
					new Vector<SimulationRun>(scenario.getSimulationRuns());
	
				Serializable[] node1Histories = null;
				Serializable[] node2Histories = null;
				if (option.equals(StatCorrelationType.cv))
				{
					node1Histories = new Serializable[runs.size()];
					node2Histories = new Serializable[runs.size()];
				}
				
				int r = 0;
				for (SimulationRun run : runs)  // each run in the scenario
				{
					r++;
					Serializable path1Value = run.getStateFinalValue(states1[i-1]);
					Serializable path2Value = run.getStateFinalValue(states2[i-1]);
					
					try
					{
						double path1Double = ((Number)path1Value).doubleValue();
						double path2Double = ((Number)path2Value).doubleValue();
						regression.addData(path1Double, path2Double);
					}
					catch (ClassCastException cce) { throw new ParameterError(
						"The specified states have non-numeric values", cce); }
					
					if (option.equals(StatCorrelationType.cv))
					{
						node1Histories[r-1] = path1Value;
						node2Histories[r-1] = path2Value;
					}
				}
					
				if (option.equals(StatCorrelationType.cc))
				{
					results[i-1] = regression.getR();
				}
				else if (option.equals(StatCorrelationType.cv))
				{
					double sigma1 = computeStatistic(StatOptionsType.sd, node1Histories);
					double sigma2 = computeStatistic(StatOptionsType.sd, node2Histories);
					results[i-1] = regression.getR() * sigma1 * sigma2;
				}
			}
			
			return results;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<String> getSimulationRuns(String clientId, String modelDomainName,
		String modelScenarioName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			// Identify the Model Domain.
			ModelDomain modelDomain = modelDomains.get(modelDomainName);
			if (modelDomain == null) throw new ElementNotFound(
				"Model Domain '" + modelDomainName + "' not found");
				
			// Identify the scenario.
			ModelScenario modelScenario =
				modelDomain.getModelScenario(modelScenarioName);
						
			if (modelScenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(modelScenario.getFullName());
				
			List<SimulationRun> runs = 
				new Vector<SimulationRun>(modelScenario.getSimulationRuns());
			List<String> runNames = new Vector<String>();
			
			for (SimulationRun run : runs)
			{
				runNames.add(run.getName());
			}
			
			return runNames;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<SimulationRunSer> getSimulationRuns(String clientId, 
		ModelScenarioSer s)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if (s == null) throw new ParameterError("Scenario is null");
			String domainId = s.modelDomainNodeId;
			if (domainId == null) throw new ParameterError(
				"Scenario domain Id is null");
			
			PersistentNode n = PersistentNodeImpl.getNode(domainId);
			if (n == null) throw new ParameterError(
				"ModelDomain (" + domainId + ") for scenario not found");
			
			if (! (n instanceof ModelDomain)) throw new ParameterError(
				"Id of Scenario's ModelDomain is not a ModelDomain");
			
			ModelDomain modelDomain = (ModelDomain)n;
			
			// Identify the scenario.
			
			String modelScenarioName = s.getName();
			if (s == null) throw new ParameterError("Scenario name is null");
			
			ModelScenario modelScenario =
				modelDomain.getModelScenario(modelScenarioName);
				
			if (modelScenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(modelScenario.getFullName());
				
			List<SimulationRun> runs = 
				new Vector<SimulationRun>(modelScenario.getSimulationRuns());
			Vector<NodeSer> eruns = new Vector<NodeSer>();
			for (SimulationRun r : runs) eruns.add(r.externalize());
			return (List)eruns;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<SimulationRunSer> getSimulationRuns(String clientId, 
		String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(scenarioId);
			if (n == null) throw new ParameterError(
				"ModelScenario (" + scenarioId + ") not found");
			
			if (! (n instanceof ModelScenario)) throw new ParameterError(
				"Id of Scenario is not a ModelScenario");
			
			ModelScenario modelScenario = (ModelScenario)n;
			
			if (modelScenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(modelScenario.getFullName());
				
			List<SimulationRun> runs = 
				new Vector<SimulationRun>(modelScenario.getSimulationRuns());
			Vector<NodeSer> eruns = new Vector<NodeSer>();
			for (SimulationRun r : runs) eruns.add(r.externalize());
			return (List)eruns;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSer[] getScenariosForScenarioSetId(String clientId,
		String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(scenSetId);
			if (n == null) throw new ParameterError(
				"Scenario Set with ID " + scenSetId + " not found");
			
			if (! (n instanceof ModelScenarioSet)) throw new ParameterError(
				"Id of ScenarioSet is not a ModelScenarioSet");
			
			ModelScenarioSet scenSet = (ModelScenarioSet)n;
			
			if (scenSet.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenSet.getFullName());
				
			SortedSet<ModelScenario> scenarios = scenSet.getScenarios();
			
			ModelScenarioSer[] scenSers = new ModelScenarioSer[scenarios.size()];
			int i = 0;
			for (ModelScenario scenario : scenarios)
			{
				scenSers[i++] = (ModelScenarioSer)(scenario.externalize());
			}
			
			return scenSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized SimulationRunSer[] getSimulationRunsForSimRunSetId(String clientId,
		String simRunSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(simRunSetId);
			if (n == null) throw new ParameterError(
				"Simulation Run Set with ID " + simRunSetId + " not found");
			
			if (! (n instanceof SimRunSet)) throw new ParameterError(
				"Id is not a Simulation Run Set");
			
			SimRunSet simRunSet = (SimRunSet)n;
			
			if (simRunSet.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRunSet.getFullName());
				
			SortedSet<SimulationRun> simRuns = simRunSet.getSimRuns();
			
			SimulationRunSer[] simRunSers = new SimulationRunSer[simRuns.size()];
			int i = 0;
			for (SimulationRun simRun : simRuns)
			{
				simRunSers[i++] = (SimulationRunSer)(simRun.externalize());
			}
			
			return simRunSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized SimRunSetSer[] getSimRunSetsForScenarioId(String clientId, String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(scenarioId);
			if (n == null) throw new ParameterError(
				"ModelScenario with ID " + scenarioId + " not found");
			
			if (! (n instanceof ModelScenario)) throw new ParameterError(
				"Id is not a ModelScenario");
			
			ModelScenario scenario = (ModelScenario)n;
			
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
			SortedSet<SimRunSet> simRunSets = scenario.getSimRunSets();
			
			SimRunSetSer[] simRunSetSers = new SimRunSetSer[simRunSets.size()];
			int i = 0;
			for (SimRunSet simRunSet : simRunSets)
			{
				simRunSetSers[i++] = (SimRunSetSer)(simRunSet.externalize());
			}
			
			return simRunSetSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized List<EventSer> getEventsForSimNodeForIteration(
		String clientId, String simRunId, int iteration)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(simRunId);
			if (node == null) throw new ParameterError(
				"Could not identify Node with ID " + simRunId);
			
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with Id " + simRunId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			SortedEventSet<GeneratedEvent> events = 
				simRun.getGeneratedEventsAtIteration(iteration);
			
			
			List<EventSer> eventSers = new Vector<EventSer>();
			for (Event event : events)
				eventSers.add((EventSer)(event.externalize()));
			
			return eventSers;
		}
		catch (ParameterError pe)
		{
			GlobalConsole.printStackTrace(pe);
			throw pe;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<EventSer>[] getEventsByEpoch(String clientId, 
		String modelDomainName,
		String modelScenarioName,
		String simRunName, List<String> paths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			SimulationRun simRun = getSimulationRunByName(modelDomainName,
				modelScenarioName, simRunName);
				
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			int noOfEpochs = simRun.getEpochs().size();
			
			List[][] eventSersHolder = { null };
			
			getEventsByEpoch_internal(modelDomainName, modelScenarioName, simRunName,
				paths, 
				null,  // no filter
				null,  // null if filter is null
				true,
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			return (List<EventSer>[])(eventSersHolder[0]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<EventSer>[] getEventsForSimNodeByEpoch(String clientId, 
		String simNodeId, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simNodeId);
			
			if (node == null) throw new ElementNotFound("Node " + simNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simNodeId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List[][] eventSersHolder = { null };
			
			getEventsByEpoch_internal(simRun.getModelScenario().getModelDomain().getName(),
				simRun.getModelScenario().getName(), simRun.getName(),
				statePaths,
				null,  // no filter
				null,  // null if filter is null
				true,  // return EventSers
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			return (List<EventSer>[])(eventSersHolder[0]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<EventSer>[] getEventsForSimNodeByEpoch(String clientId, 
		String simNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simNodeId);
			
			if (node == null) throw new ElementNotFound("Node " + simNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simNodeId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
						
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List[][] eventSersHolder = { null };
			
			List<State> states = 
				simRun.getModelScenario().getModelDomain().getStatesRecursive();
			
			List<String> statePaths = new Vector<String>();
			for (State state : states) statePaths.add(state.getFullName());
			
			List[] filteredPathsHolder = { null };

			getEventsByEpoch_internal(simRun.getModelScenario().getModelDomain().getName(),
				simRun.getModelScenario().getName(), simRun.getName(),
				statePaths,
				null,  // no filter
				null,  // null if filter is null
				true,  // return EventSers
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			return (List<EventSer>[])(eventSersHolder[0]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized List<EventSer>[] getEventsForSimNodeByEpoch(String clientId, 
		String simNodeId, List<String> statePaths, Class[] stateTags, 
		List<String>[] filteredPathsHolder
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simNodeId);
			
			if (node == null) throw new ElementNotFound("Node " + simNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simNodeId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
						
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List[][] eventSersHolder = { null };
			
			getEventsByEpoch_internal(simRun.getModelScenario().getModelDomain().getName(),
				simRun.getModelScenario().getName(), simRun.getName(),
				statePaths,
				stateTags,  // filter
				filteredPathsHolder,
				true,  // return EventSers
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (filteredPathsHolder[0] == null) throw new RuntimeException(
				"filteredPathsHolder[0] is null");
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			return (List<EventSer>[])(eventSersHolder[0]);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized StateSer[] getStatesRecursive(String clientId, String modelContainerNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(modelContainerNodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + 
				modelContainerNodeId + " not found");
			
			if (! (node instanceof ModelContainer)) throw new ParameterError(
				"Node with ID " + modelContainerNodeId + " is not a Model Container");
			
			ModelContainer container = (ModelContainer)node;
			
			if (container.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(container.getFullName());
				
			List<State> states = container.getStatesRecursive();
			
			StateSer[] stateSers = new StateSer[states.size()];
			int i = 0;
			for (State state : states) stateSers[i++] = (StateSer)(state.externalize());
			
			return stateSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String getEventsByEpochAsHTML(String clientId, String simRunNodeId, 
		List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simRunNodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + simRunNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simRunNodeId + " is not a Simulation Run");
			
			SimulationRun simRun = (SimulationRun)node;

			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List<GeneratedEvent>[] eventListArray = getEventsByEpoch_internal(
				simRun.getModelScenario().getModelDomain().getName(), 
				simRun.getModelScenario().getName(), simRun.getName(), statePaths,
				null, null);
				
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			
			EventWriter.writeEvents(EventWriter.FormatType.html, simRun.getName(), statePaths,
				eventListArray, pw);
			
			pw.flush();
			return sw.toString();
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized String getEventsByEpochAsHTML(String clientId, String simRunNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simRunNodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + simRunNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simRunNodeId + " is not a Simulation Run");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			
			// Identify all of the States in the model. Ignore 
			
			List<State> states = 
				simRun.getModelScenario().getModelDomain().getStatesRecursive();
			
			List<String> statePaths = new Vector<String>();
			for (State state : states) statePaths.add(state.getFullName());
			
			List<GeneratedEvent>[] eventListArray = getEventsByEpoch_internal(
				simRun.getModelScenario().getModelDomain().getName(), 
				simRun.getModelScenario().getName(), simRun.getName(), statePaths,
				null, null);
				
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			
			EventWriter.writeEvents(EventWriter.FormatType.html, simRun.getName(), statePaths,
				eventListArray, pw);
			
			pw.flush();
			return sw.toString();
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Serializable getAttributeValue(String clientId, String attrPath, 
		String scenarioName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(attrPath);
			if (node == null) throw new ParameterError("Not found: " + attrPath);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrPath);
			
			Attribute attribute = (Attribute)node;
			
			ModelDomain modelDomain = attribute.getModelDomain();
			
			ModelScenario modelScenario = null;
			try { modelScenario = modelDomain.getModelScenario(scenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(
				"Not found: " + scenarioName); }
			
			return modelScenario.getAttributeValue(attribute);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized Serializable getAttributeValueForId(String clientId, 
		String attrId, String scenarioName)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ParameterError("Not found: " + attrId);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrId);
			
			Attribute attribute = (Attribute)node;
			
			ModelDomain modelDomain = attribute.getModelDomain();
			
			ModelScenario modelScenario = null;
			try { modelScenario = modelDomain.getModelScenario(scenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(
				"Not found: " + scenarioName); }
			
			return modelScenario.getAttributeValue(attribute);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Serializable getAttributeValueById(String clientId, 
		String attrId, String scenarioId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ParameterError("Not found: " + attrId);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrId);
			
			Attribute attribute = (Attribute)node;
			
			node = PersistentNodeImpl.getNode(scenarioId);
			if (node == null) throw new ParameterError("Not found: " + scenarioId);
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Not a ModelScenario: " + scenarioId);
			
			ModelScenario modelScenario = (ModelScenario)node;
			
			return modelScenario.getAttributeValue(attribute);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized Object[] getStateValuesForIteration(String clientId,
		String simRunId, int iteration)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (iteration < 0) throw new ParameterError(
				"Iteration must be 1 or greater, or 0 to specify initial values");

			PersistentNode node = PersistentNodeImpl.getNode(simRunId);
			if (node == null) throw new ParameterError(
				"Could not identify Node with ID " + simRunId);
			
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with Id " + simRunId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List<State> states = simRun.getModelDomain().getStatesRecursive();
			
			List<String> stateIds = new Vector<String>();
			
			List<Serializable> values = new Vector<Serializable>();
			
			for (State state : states)
			{
				stateIds.add(state.getNodeId());
				
				if (iteration == 0)
					values.add(null);
				else
				{
					Epoch epoch = simRun.getEpochForIteration(iteration);
					Serializable value = simRun.getStateValueAtEpoch(state, epoch);
					values.add(value);
				}
			}
			
			Serializable[] valuesAr = values.toArray(new Serializable[values.size()]);
			
			Object[] result = new Object[2];
			
			result[0] = valuesAr;
			result[1] = stateIds;
			
			return result;
		}
		catch (ParameterError pe)
		{
			GlobalConsole.printStackTrace(pe);
			throw pe;
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized ScenarioSer[] getScenarios(String clientId, String domainName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelDomain modelDomain = modelDomains.get(domainName);
			if (modelDomain != null)
			{
				Set<ModelScenario> scenarios = modelDomain.getScenarios();
				ScenarioSer[] scenarioSers = new ScenarioSer[scenarios.size()];
				int scenarioNo = 0;
				for (Scenario scenario : scenarios)
					scenarioSers[scenarioNo++] = (ScenarioSer)(scenario.externalize());
				
				return scenarioSers;
				//return scenarios.toArray(new ModelScenario[0]);
			}
			
			DecisionDomain decisionDomain = decisionDomains.get(domainName);
			if (decisionDomain != null)
			{
				Set<DecisionScenario> scenarios = decisionDomain.getDecisionScenarios();
				ScenarioSer[] scenarioSers = new ScenarioSer[scenarios.size()];
				int scenarioNo = 0;
				for (Scenario scenario : scenarios)
					scenarioSers[scenarioNo++] = (ScenarioSer)(scenario.externalize());
				
				return scenarioSers;
			}
			
			throw new ParameterError("Domain " + domainName + " not found");
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized ScenarioSer[] getScenariosForDomainId(String clientId, 
		String domainId)
	throws
		ParameterError,  // if the Domain does not exist.
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ParameterError("Node " + domainId + " not found");
			
			if (node instanceof ModelDomain)
			{
				ModelDomain modelDomain = (ModelDomain)node;
				
				Set<ModelScenario> scenarios = modelDomain.getScenarios();
				ScenarioSer[] scenarioSers = new ScenarioSer[scenarios.size()];
				int scenarioNo = 0;
				for (Scenario scenario : scenarios)
					scenarioSers[scenarioNo++] = (ScenarioSer)(scenario.externalize());
				
				if (scenarioSers == null) throw new RuntimeException("scenarioSers is null");
				return scenarioSers;
			}
			else if (node instanceof DecisionDomain)
			{
				DecisionDomain decisionDomain = (DecisionDomain)node;
				
				Set<DecisionScenario> scenarios = decisionDomain.getDecisionScenarios();
				ScenarioSer[] scenarioSers = new ScenarioSer[scenarios.size()];
				int scenarioNo = 0;
				for (Scenario scenario : scenarios)
					scenarioSers[scenarioNo++] = (ScenarioSer)(scenario.externalize());
				
				if (scenarioSers == null) throw new RuntimeException("scenarioSers is null");
				return scenarioSers;
			}
			else throw new ParameterError("Node " + domainId + " is not a Domain");
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSer getModelScenario(String clientId, 
		String modelDomainName, String modelScenarioName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelDomain modelDomain = modelDomains.get(modelDomainName);
			if (modelDomain == null) throw new ParameterError(
				"Model Domain " + modelDomainName + " not found");
			
			ModelScenario scenario;
			try { scenario = modelDomain.getModelScenario(modelScenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(enf); }
			
			return (ModelScenarioSer)(scenario.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized ModelScenarioSetSer[] getScenarioSetsForDomainId(
		String clientId, String domainId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ParameterError("Node " + domainId + " not found");
			
			if (! (node instanceof ModelDomain)) throw new ParameterError(
				"Node with ID " + domainId + " is not a ModelDomain");
			
			ModelDomain domain = (ModelDomain)node;
		
			SortedSet<ModelScenarioSet> scenarioSets = domain.getModelScenarioSets();
			
			ModelScenarioSetSer[] scenSetSers = new ModelScenarioSetSer[scenarioSets.size()];
			int i = 0;
			for (ModelScenarioSet scenSet : scenarioSets)
			{
				scenSetSers[i++] = (ModelScenarioSetSer)(scenSet.externalize());
			}
			
			return scenSetSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized AttributeSer[] getAttributes(String clientId, String elementPath)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(elementPath);
			if (node == null) throw new ParameterError("Not found: " + elementPath);
			
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"Not a ModelElement (and therefore has no Attributes): " + elementPath);
			
			ModelElement modelElement = (ModelElement)node;
			
			Collection<Attribute> attributes = modelElement.getAttributes().values();
			
			AttributeSer[] attributeSers = new AttributeSer[attributes.size()];
			int attributeNo = 0;
			for (Attribute attribute : attributes)
				attributeSers[attributeNo++] = (AttributeSer)(attribute.externalize());
			return attributeSers;
			
			//return attributes.toArray(new Attribute[0]);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized AttributeSer[] getAttributesForId(String clientId, 
		String modelElementId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(modelElementId);
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"Not a ModelElement (and therefore has no Attributes): " + modelElementId);
			
			ModelElement modelElement = (ModelElement)node;
			
			Collection<Attribute> attributes = modelElement.getAttributes().values();
			
			AttributeSer[] attributeSers = new AttributeSer[attributes.size()];
			int attributeNo = 0;
			for (Attribute attribute : attributes)
				attributeSers[attributeNo++] = (AttributeSer)(attribute.externalize());
			return attributeSers;
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized boolean isGeneratorRepeating(String clientId, String genNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(genNodeId);
			if (! (node instanceof Generator)) throw new ParameterError(
				"Not a Generator: " + genNodeId);
			
			Generator gen = (Generator)node;
			
			ModelContainer container = gen.getModelContainer();
			if (container == null) throw new ModelContainsError("Generator has no parent");
			
			return container.isGeneratorRepeating(gen);
		}
		finally
		{
			ServiceContext.clear();
		}
	}	
	
	
	public synchronized void makeGeneratorRepeating(String clientId, 
		String genNodeId, boolean repeating)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(genNodeId);
			if (! (node instanceof Generator)) throw new ParameterError(
				"Not a Generator: " + genNodeId);
			
			Generator gen = (Generator)node;
			
			if (gen.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(gen.getFullName());
				
			ModelContainer container = gen.getModelContainer();
			if (container == null) throw new ModelContainsError("Generator has no parent");
			
			boolean isAlreadyRepeating = container.isGeneratorRepeating(gen);
			if (repeating && isAlreadyRepeating) return;
			if ((! repeating) && (! isAlreadyRepeating)) return;
			
			Conduit repeatingConduit = container.makeGeneratorRepeating(gen, repeating);
			
			thereAreUnflushedChanges = true;
			
			
			// Notify

			PeerNotice notice = new PeerNoticeBase.NonStructuralChangeNotice(genNodeId);
					
			try { callbackManager.notifyAllListeners(notice); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setGeneratorIgnoreStartup(String clientId, 
		String genNodeId, boolean ignore)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(genNodeId);
			if (! (node instanceof Generator)) throw new ParameterError(
				"Not a Generator: " + genNodeId);
			
			Generator gen = (Generator)node;
			
			if (gen.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(gen.getFullName());
				
			gen.setIgnoreStartup(ignore);
			
			thereAreUnflushedChanges = true;
			
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.NonStructuralChangeNotice(genNodeId)); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }			
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized DecisionSer[] getDecisions(String clientId, 
		String decisionPointPath, String scenarioName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(decisionPointPath);
			if (node == null) throw new ParameterError("Not found: " + decisionPointPath);
			
			if (! (node instanceof DecisionPoint)) throw new ParameterError(
				"Not a DecisionPoint (and therefore has no Decisions): " + decisionPointPath);
			
			DecisionPoint decisionPoint = (DecisionPoint)node;
			
			DecisionDomain decisionDomain = decisionPoint.getDecisionDomain();
			
			DecisionScenario decisionScenario = decisionDomain.getDecisionScenario(scenarioName);
			if (decisionScenario == null) throw new ParameterError(
				"Not found: " + scenarioName);
			
			Set<Decision> decisions = decisionScenario.getDecisions(decisionPoint);
			
			DecisionSer[] decisionSers = new DecisionSer[decisions.size()];
			int decisionNo = 0;
			for (Decision decision : decisions)
				decisionSers[decisionNo++] = (DecisionSer)(decision.externalize());
			return decisionSers;
			
			//return decisions.toArray(new Decision[0]);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized Serializable[] getFinalStateValues(String clientId, 
		String statePath, String scenarioName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = findNode(statePath);
			if (node == null) throw new ParameterError("Not found: " + statePath);
			
			if (! (node instanceof State)) throw new ParameterError(
				"Not a State: " + statePath);
			
			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
				
			ModelDomain modelDomain = state.getModelDomain();
			
			ModelScenario modelScenario = null;
			try { modelScenario = modelDomain.getModelScenario(scenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(
				"Not found: " + scenarioName); }
			
			return modelScenario.getStateFinalValues(state);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized Serializable[] getFinalStateValuesForId(String clientId, 
		String stateId, String scenarioId)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ParameterError("Not found: State with Id " + stateId);
			
			if (! (node instanceof State)) throw new ParameterError(
				"Not a State: node with Id " + stateId);
			
			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
				
			ModelDomain modelDomain = state.getModelDomain();
			
			node = PersistentNodeImpl.getNode(scenarioId);
			if (node == null) throw new ParameterError(
				"Not found: Node Id " + scenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + scenarioId + " is not a Model Scenario");
			
			ModelScenario modelScenario = (ModelScenario)node;
			
			return modelScenario.getStateFinalValues(state);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
		
	public synchronized void setAttributeValue(String clientId, boolean confirm, 
		String attrNodeId, String scenarioId, Serializable value)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		if (scenarioId == null) setAttributeValue(clientId, confirm, attrNodeId, value);
		else try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(attrNodeId);
			if (n == null) throw new ParameterError("Attribute with Id " + attrNodeId +
				" not found.");
			if (! (n instanceof Attribute)) throw new ParameterError(
				"Node with Id " + attrNodeId + " is not an Attribute.");
			Attribute attr = (Attribute)n;
			
			if (attr.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(attr.getFullName());
				
			if (! (value instanceof Types.Inspect))
				if (! confirm)
				{
					if (attr.getModelDomain().hasSimulationRuns()) throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
				}
			
			n = PersistentNodeImpl.getNode(scenarioId);
			if (n == null) throw new ParameterError("Scenario with Id " + scenarioId +
				" not found.");
			if (! (n instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + scenarioId + " is not a Model Scenario.");
			ModelScenario scenario = (ModelScenario)n;
			
			scenario.setAttributeValue(attr, value);

			thereAreUnflushedChanges = true;
			
			if (! (value instanceof Types.Inspect)) deleteSimulationRuns(scenario);
			
			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.AttributeValueUpdated(scenario.getNodeId(), 
					attr.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void setAttributeValue(String clientId, boolean confirm, 
		String attrNodeId, Serializable value)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(attrNodeId);
			if (n == null) throw new ParameterError("Attribute with Id " + attrNodeId +
				" not found.");
			if (! (n instanceof Attribute)) throw new ParameterError(
				"Node with Id " + attrNodeId + " is not an Attribute.");
			Attribute attr = (Attribute)n;
			
			if (attr.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(attr.getFullName());
				
			if (! (value instanceof Types.Inspect))
				if (! confirm)
				{
					if (attr.getModelDomain().hasSimulationRuns()) throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
				}
			
			// Get Scenarios.
			
			ModelDomain domain = attr.getModelDomain();
			Set<ModelScenario> scenarios = domain.getScenarios();
			if (scenarios.size() == 0) throw new ParameterError(
				"No Scenarios in which to set the Attribute's value.");


			// Set the Attribute value in each Scenario.
			
			thereAreUnflushedChanges = true;
			for (ModelScenario scenario : scenarios)
			{
				scenario.setAttributeValue(attr, value);

				if (! (value instanceof Types.Inspect)) deleteSimulationRuns(scenario);


				// Notify clients.
				
				Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
				try { peer.notifyAllListeners(
					new PeerNoticeBase.AttributeValueUpdated(scenario.getNodeId(), 
						attr.getNodeId())); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}
			}
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void watchNode(String clientId, boolean enable, String nodePath)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(nodePath);
			if (node == null) throw new ParameterError("Not found: " + nodePath);
			
			node.setWatch(enable);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	
	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Methods from ModelEngineLocal..
	 *
	 */

	
	public synchronized void allowNewSimulations(boolean allow)
	{
		this.allowSimulations = allow;
	}
	
	
	public synchronized boolean newSimulationsAreAllowed()
	{
		return this.allowSimulations;
	}


	public synchronized boolean simulationsAreRunning()
	{
		Set<Integer> threadIds = threads.keySet();
		for (Integer threadId : threadIds)
		{
			ServiceThread thread = threads.get(threadId);
			if (((Thread)thread).isAlive()) return true;
		}
		
		return false;
	}
	
	
	
	public synchronized List<ModelDomain> getModelDomainPersistentNodes()
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
			List<ModelDomain> domainList = new Vector<ModelDomain>();
			Collection<ModelDomain> mdColl = modelDomains.values();
			domainList.addAll(mdColl);
			return domainList;
	}
	
	
	public synchronized ModelDomain getModelDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ModelDomain domain = modelDomains.get(name);
			if (domain == null) throw new ElementNotFound(name);

			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized DecisionDomain getDecisionDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			
			DecisionDomain domain = decisionDomains.get(name);
			if (domain == null) throw new ElementNotFound("Domain " + name);
			
			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized ModelDomain createModelDomainPersistentNode(String name,
		boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ModelDomain domain = createModelDomain_internal(name, useNameExactly);
			thereAreUnflushedChanges = true;
			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	

	public synchronized DecisionDomain createDecisionDomainPersistentNode(String name, 
		boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			DecisionDomain domain = createDecisionDomain_internal(name, useNameExactly);
			thereAreUnflushedChanges = true;
			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized PersistentNode getPersistentNode(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			return node;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}


	public synchronized Set<PersistentNode> getChildPersistentNodes(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node Id " + nodeId);
			
			if (! (node instanceof PersistentNodeContainer))
				new TreeSet<PersistentNode>();
			
			return ((PersistentNodeContainer)node).getChildren();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized ModelElement getModelElementTree(String modelDomainName,
		String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			return getModelElement_internal(modelDomainName, elementName);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}


	public synchronized DecisionElement getDecisionElementTree(String decisionDomainName,
		String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			return getDecisionElement(decisionDomainName, elementName);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}


	public synchronized List<GeneratedEvent>[] getEventNodesByEpoch(
		String modelDomainName, String modelScenarioName,
		String simRunName, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
			return getEventsByEpoch_internal(modelDomainName, modelScenarioName, simRunName,
				statePaths, null, null);
	}


	public synchronized void dump()
	throws
		IOException
	{
		dump(0);
	}


	public synchronized void dump(int indentation)
	throws
		IOException
	{
		for (DecisionDomain decisionDomain : decisionDomains.values())
		{
			decisionDomain.dump(indentation);
		}

		for (ModelDomain modelDomain : modelDomains.values())
		{
			modelDomain.dump(indentation);
		}
	}
	

	public ListenerRegistrar registerPeerListener(String clientId, PeerListener listener)
	throws
		ParameterError,
		IOException
	{
		synchronized (listenerRegistrars)
		{
			ListenerRegistrarImpl registrar = new ListenerRegistrarImpl(listener, 
				clientId, this);
			listenerRegistrars.add(registrar);
			
			return registrar;
		}
	}


	public void unregisterPeerListener(PeerListener listener)
	throws
		ParameterError,
		IOException
	{
		synchronized (listenerRegistrars)
		{
			Set<ListenerRegistrarImpl> registrarsCopy = new HashSet(listenerRegistrars);
			for (ListenerRegistrar registrar : registrarsCopy)
			{
				if (((ListenerRegistrarImpl)registrar).getPeerListener().equals(listener))
					listenerRegistrars.remove(registrar);
			}
		}
	}


	public Set<ListenerRegistrarImpl> getListenerRegistrars()
	{
		synchronized (listenerRegistrars)
		{
			return new HashSet<ListenerRegistrarImpl>(listenerRegistrars);
		}
	}


	public CallbackManager getCallbackManager() { return this.callbackManager; }
	
	

	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Inner classes.
	 *
	 */

		
	/** ************************************************************************
	 * Inner class for handling callback requests when updateDecisions() is
	 * call synchronously (i.e., via a blocking call).
	 */
	
	class SynchronousDecisionCallback implements DecisionCallback
	{
		private List<String> progressMessages = new Vector<String>();
		private List<String> asyncMessages = new Vector<String>();
		private Set<Decision> newDecisions = new TreeSet<Decision>();
		private boolean bSetToAbort = false;
		
		
		void addDecisions(Set<Decision> newDecisions)
		{
			this.newDecisions.addAll(newDecisions);
		}
		
		public synchronized Set<Decision> getNewDecisions()
		{
			return newDecisions;
		}

		public synchronized boolean checkAbort() { return bSetToAbort; }

		public synchronized void showProgress(String msg)
		{
			progressMessages.add(msg);
			GlobalConsole.println(msg);
		}

		public synchronized void showMessage(String msg)
		{
			asyncMessages.add(msg);
			GlobalConsole.println(msg);
		}


		public synchronized void showErrorMessage(String msg)
		{
			asyncMessages.add(msg);
			GlobalConsole.println(msg);
		}
		

		public synchronized void completed()
		{
			//bIsComplete = true;
			showMessage("Service request completed.");
		}

		public synchronized void aborted()
		{
			//abort = true;
			showMessage("Aborted");
		}

		public synchronized void aborted(String msg)
		{
			//abort = true;
			showMessage("Aborted: " + msg);
		}

		public synchronized void aborted(Exception ex)
		{
			//abort = true;
			GlobalConsole.println("Aborted: ");
			GlobalConsole.printStackTrace(ex);

			for (Throwable t = ex; t != null; t = t.getCause())
			{
				showMessage(t.getMessage());
			}
		}

		public synchronized void aborted(String msg, Exception ex)
		{
			//abort = true;
			GlobalConsole.println("Aborted: " + msg);
			GlobalConsole.printStackTrace(ex);

			for (Throwable t = ex; t != null; t = t.getCause())
			{
				showMessage(t.getMessage());
			}
		}

		public synchronized boolean isComplete()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}

		public synchronized List<String> getProgress()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}

		public synchronized List<String> getMessages()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}

		public synchronized boolean isAborted()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}
	}



	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Nested classes.
	 *
	 */

		
	private int monitorThreadNumber = 0;
	
	protected Integer createMonitorThreadHandle()
	{
		return new Integer(++monitorThreadNumber);
	}
	
	
	/** ************************************************************************
	 * A thread for initiating and monitoring a series of simulation runs, each
	 * of which may create its own thread. This implementation is currently
	 * single-threaded, so that at any one time there is only one simulation
	 * run in progress. All simulated Scenarios must belong to the same Domain.
	 */
	
	class SimulationMonitorThread extends ServiceThreadImpl
	{
		private static final long DefaultInitialRandomSeed = 1;
		private Integer requestThreadHandle;
		private Set<ModelScenario> modelScenarios = null;
		//private ModelScenario modelScenario = null;
		private Date initialEpoch = null;
		private Date finalEpoch = null;
		private int iterationLimit = 0;
		private int runs = 0;
		private long duration = 0;
		private PeerListener peerListener = null;
		private boolean repeatable;
		private ModelDomain modelDomain = null;
		
		private boolean proceed = true;

		//private Set<SimulationRun> allSimRuns = new TreeSet<SimulationRun>();
		//private boolean[] simRunCompletedStatus = null;

		
		SimulationMonitorThread(Integer requestThreadHandle,
			Set<ModelScenario> modelScenarios, Date initialEpoch,
			Date finalEpoch, int iterationLimit, int runs, long duration,
			PeerListener peerListener,  // may be null
			boolean repeatable
			)
		{
			super(); // saves reference to ServiceContext of creator thread.
			
			this.requestThreadHandle = requestThreadHandle;
			this.modelScenarios = modelScenarios;
			this.initialEpoch = initialEpoch;
			this.finalEpoch = finalEpoch;
			this.iterationLimit = iterationLimit;
			this.runs = runs;
			this.duration = duration;
			this.peerListener = peerListener;
			this.repeatable = repeatable;
			
			for (ModelScenario scenario : modelScenarios)
			{
				this.modelDomain = scenario.getModelDomain();
				break;
			}
			
			try { modelDomain.lockForSimulation(true); }
			catch (CannotObtainLock col)
			{
				GlobalConsole.printStackTrace(col);
				return;
			}
			
			
			// Identify the ListenerRegistrar for the client and automatically
			// subscribe the client to the Domain being simulated, in the context
			// of each Scenario being simulated.
			
			if (peerListener != null)
			{
				Set<ListenerRegistrarImpl> listenerRegistrars = 
					ModelEngineLocalPojoImpl.this.getListenerRegistrars();
					
				for (ListenerRegistrarImpl registrar : listenerRegistrars)
				{
					if (registrar.getPeerListener().equals(peerListener)) try
					{
						for (ModelScenario modelScenario : modelScenarios)
							registrar.subscribe(modelScenario.getModelDomain().getNodeId(), 
								modelScenario.getName(), true);
					}
					catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
				}
			}
		}
		
		
		public void go()
		{
			try
			{
				// Note: the superclass run method has restored the ServiceContext of the
				// thread that created this thread.
				
				if (isAborted()) { abortThread(); return; }
				
				
				// Establish an initial random seed.
				
				long initialSeed = DefaultInitialRandomSeed;
				if (! this.repeatable)
					initialSeed = System.currentTimeMillis();
				
				RandomGenerator seedGenerationRandomGenerator = new RandomGenerator(initialSeed);
				
				
				/* Schedule the simulations in batches, depending on how many processors
				 the system has. Do not concurrently execute more simulations than
				 the number of processors.*/
				
				int noOfProcessors = Runtime.getRuntime().availableProcessors();
				int batchSize = noOfProcessors;
				
				SimServiceThread[] serviceThreads = new SimServiceThread[batchSize];
				SimCallback[] simCallbacks = new SimCallback[batchSize];
				
				//simRunCompletedStatus = new boolean[simsToRun];
				//for (int i = 0; i < simsToRun; i++) simRunCompletedStatus[i] = false;
			
				int simRunNumber = 0;
				
				for (ModelScenario modelScenario : modelScenarios)
				{
					SimRunSet simRunSet = modelScenario.createSimRunSet();
						
					int simsToRun = runs;
					for (;;) // until all of 'runs' simulations have completed.
					{
						if (simsToRun == 0) break;
						
						// Start at least 'batchSize' more simulations.
						
						//for (int i = 0; i < batchSize; i++)
						for (int i = 0; i < 1; i++)
						{
							if (simsToRun == 0) break;
							
							// Create a callback object to receive notices from
							// the simulation run that we are about to create.
							
							simCallbacks[i] = new SimRunCallbackImpl(this, ++simRunNumber);
							
							
							// Create a RandomGenerator for the Run.
							
							long seed = seedGenerationRandomGenerator.nextLong();
							
						
							// Start an asynchronous simulation run.
							
							try
							{
								//showMessage(
								//	"Simulating model Scenario " + modelScenario.getName() + "...");
								
								serviceThreads[i] = modelScenario.getModelDomain().simulate(
									simRunSet,
									modelScenario, simCallbacks[i], initialEpoch, finalEpoch, 
									iterationLimit, duration, seed);
								
								simsToRun--;
								
								SimulationRun simRun = serviceThreads[i].getSimulationRun();
							}
							catch (Exception ex)
							{
								showMessage(ex.getMessage());
								abortThread();
								return;
							}
					
							if (isAborted()) { abortThread(); return; }
						}
						
						
						// Wait for all of these simulations to finish.
						// Note: improve by implementing a queue so that all processors
						// are always busy.
		
						for (int i = 0; i < batchSize; i++)
						{
							if (serviceThreads[i] != null)
							{
								try { Thread.currentThread().sleep(100); }
								catch (InterruptedException ie) { return; }
								
								serviceThreads[i].waitForFinish();
							}
							
						}
					}
				}
			}
			finally
			{
				// Report to the client that all simulations have completed.
				GlobalConsole.println("All simulations completed.");
				this.completed();
				
				try { this.modelDomain.lockForSimulation(false); }
				catch (CannotObtainLock col)
				{
					GlobalConsole.printStackTrace(col);
				}
			}
		}
	}
	
	
	/** ************************************************************************
	 * Each simulation run is given an instance of this class, to allow the run
	 * to inform the monitor of the run's progress. These callbacks are a logical
	 * extension of the monitor.
	 */
	 
	class SimRunCallbackImpl implements SimCallback
	{
		private SimulationMonitorThread monitorThread = null;
		private int simRunNumber = 0;
		//private boolean bSetToAbort = false;
		private String simRunNodeId = null;
		private int messageNumber = 0;
		
		
		SimRunCallbackImpl(SimulationMonitorThread monitorThread, int simRunNumber)
		{
			this.monitorThread = monitorThread;
			this.simRunNumber = simRunNumber;
		}
		
		
		public void setSimRunNodeId(String id) { this.simRunNodeId = id; }
		
		
		/*
		 * Methods from SimCallback and ProgressCallback. These are to be called
		 * by the simulation thread (for which this is a callback).
		 */
		
		public synchronized boolean checkAbort()
		{
			return monitorThread.checkAbort();
		}
	

		public synchronized void showProgress(String msg)
		{
			showMessage(msg);
		}
		
		
		public synchronized void showProgress(int totalEpochsCompleted,
			long totalTimeElapsed)
		{
			// Notify clients.
			
			try
			{
				SimulationRun simRun = (SimulationRun)(PersistentNodeImpl.getNode(this.simRunNodeId));
				ModelScenario scenario = simRun.getModelScenario();
				
				String id;
				if (scenario.getModelScenarioSet() == null)
					id = scenario.getNodeId();
				else
					id = scenario.getModelScenarioSet().getNodeId();
			
				ModelEngineLocalPojoImpl.this.callbackManager.notifyAllListeners(
					new PeerNoticeBase.ScenarioSimProgressNotice(
						monitorThread.requestThreadHandle,
						id, this.simRunNodeId,
						totalEpochsCompleted, totalTimeElapsed));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		
		
		protected void notifyClientsOfError(String msg)
		{
			try
			{
				SimulationRun simRun = (SimulationRun)(PersistentNodeImpl.getNode(this.simRunNodeId));
				ModelScenario scenario = simRun.getModelScenario();
			
				String id;
				if (scenario.getModelScenarioSet() == null)
					id = scenario.getNodeId();
				else
					id = scenario.getModelScenarioSet().getNodeId();
			
				ModelEngineLocalPojoImpl.this.callbackManager.notifyAllListeners(
					new PeerNoticeBase.ScenarioSimErrorMessageNotice(
						monitorThread.requestThreadHandle,
						id, this.simRunNodeId,
						"Msg No. " + (++messageNumber) + ": " + msg));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	
	
		public synchronized void showMessage(String msg)
		{
			String s = Integer.toString(simRunNumber) + ": " + msg;
			GlobalConsole.println(s);
		}
	
	
		public synchronized void showErrorMessage(String msg)
		{
			String s = "For run " + simRunNumber + ": " + msg;
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
		

		public synchronized void completed()
		{
			GlobalConsole.println("Completed run " + simRunNumber);

			// Notify clients.
			
			try
			{
				SimulationRun simRun = (SimulationRun)(PersistentNodeImpl.getNode(this.simRunNodeId));
				ModelScenario scenario = simRun.getModelScenario();
			
				String id;
				if (scenario.getModelScenarioSet() == null)
					id = scenario.getNodeId();
				else
					id = scenario.getModelScenarioSet().getNodeId();
			
				ModelEngineLocalPojoImpl.this.callbackManager.notifyAllListeners(
					new PeerNoticeBase.ScenarioSimProgressNotice(
						monitorThread.requestThreadHandle,
						id, this.simRunNodeId,
						true /* completed */));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	
	
		public synchronized void aborted()
		{
			String s = "Aborted run " + simRunNumber;
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
	
	
		public synchronized void aborted(String msg)
		{
			String s = "Aborted run " + simRunNumber + ": " + msg;
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
		

		public synchronized void aborted(Exception ex)
		{
			String s = "Aborted run " + simRunNumber + ": " + ex.toString();
			
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
	
	
		public synchronized void aborted(String msg, Exception ex)
		{
			String s = "Aborted run " + simRunNumber + ": " + msg + "; " + 
				ex.toString();
			
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
		
		
		public synchronized boolean getConfirmationToProceed()
		{
			return true;
		}
		
		
		/*
		 * Implementation methods, to be called by the monitor thread to manage
		 * the simulation thread (for which this is a callback).
		 */
		
		synchronized void abortGracefully()
		{
			monitorThread.abort();
		}
	}


	/** ************************************************************************
	 * A thread for initiating and monitoring the processing of an XML file,
	 * which might spawn one or more simulations.
	 */
	 
	class XMLMonitorThread extends ServiceThreadImpl
	{
		private Integer requestThreadHandle;
		private PeerListener peerListener;
		private String clientId;
		private char[] charAr;
		
		XMLMonitorThread(Integer requestThreadHandle,
			PeerListener peerListener,  // may be null
			String clientId,
			char[] charAr
			)
		{
			super(); // saves reference to ServiceContext of creator thread.
			
			this.requestThreadHandle = requestThreadHandle;
			this.peerListener = peerListener;
			this.clientId = clientId;
			this.charAr = charAr;
		}
		
		
		public void go()
		{
			XMLParser xmlParser = null;
				
			try
			{
				//ServiceContext.create(ModelEngineLocalPojoImpl.this, this.clientId);
				
				GlobalConsole.println("Parsing XML document...");
				
				xmlParser = new XMLParser();
				
				xmlParser.setModelEngineLocal(ModelEngineLocalPojoImpl.this);
				
				
				// Set PeerListener for the parser. This will enable the parser
				// to pass PeerListener to the ModelEngine when it simulate, so
				// that simulate() can automatically subscribe the Listener to
				// the SimulationRun.
				
				xmlParser.setPeerListener(peerListener);
				
				xmlParser.setClientId(clientId);
				
			
				xmlParser.parse(charAr);

				GlobalConsole.println("...Interpreting XML document...");
				xmlParser.genDocument();
			}
			catch (SAXParseException spe)
			{
				GlobalConsole.printStackTrace(spe);

				try { ServiceContext.getServiceContext().notifyClient(
					new PeerNoticeBase.ImportErrorNotice(
						"At line " + spe.getLineNumber() +
						", column " + spe.getColumnNumber() + ",<br>" +
						ThrowableUtil.getAllMessagesAsHTML(spe))); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}			
			}
			catch (XMLParser.GenException ge)
			{
				GlobalConsole.printStackTrace(ge);

				// Send notice to clients that the parse failed with
				// the specified error message
				
				try { ServiceContext.getServiceContext().notifyClient(
					new PeerNoticeBase.ImportErrorNotice(
						ThrowableUtil.getAllMessagesAsHTML(ge))); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}			
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				return;
			}
			finally
			{
				List<String> warnings = xmlParser.getWarnings();
				if (warnings.size() > 0) try
				{
					String warningString = "";
					boolean first = true;
					for (String warning : warnings)
					{
						if (first) first = false;
						else warningString += "\n";
						warningString += warning;
					}

					ServiceContext.getServiceContext().notifyClient(
						new PeerNoticeBase.ImportWarningNotice(warningString));
				}
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}
				
				//ServiceContext.clear();
			}
			
			
			// Note: task handles can be obtained by calling
			// xmlParser.getTaskHandles().
			
			GlobalConsole.println("...Completed XML document.");
		}
	}
	


	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Internal methods.
	 *
	 */


	/**
	 * Delete all Simulation Runs for the specified Model Domain, and notify the
	 * client that the deletion has occurred. Delete SimRunSets as well.
	 */
	 
	void deleteSimulationRuns(ModelDomain domain)
	{
		domain.deleteSimulationRuns();
		
		Peer peer = ServiceContext.getServiceContext().getPeer(domain);
		try { peer.notifyAllListeners(
			new PeerNoticeBase.SimulationRunsDeletedNotice(domain.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
	}


	/**
	 * Delete all Simulation Runs for the specified Model Scenario, and notify the
	 * client that the deletion has occurred. Delete SimRunSets as well.
	 */
	 
	void deleteSimulationRuns(ModelScenario scenario)
	{
		scenario.deleteSimulationRuns();
		
		Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
		try { peer.notifyAllListeners(
			new PeerNoticeBase.SimulationRunsDeletedNotice(scenario.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
	}


	ModelDomain getModelDomain_internal(String name)
	{
		return modelDomains.get(name);
	}
	
	
	Set<Decision> getNewDecisions(int callbackId)
	throws
		ParameterError,
		IOException
	{
		
			ServiceThread thread = threads.get(new Integer(callbackId));

			if (thread == null)  // not found.
				throw new CallbackNotFound();
		
			if (thread instanceof DecisionCallback)
			{
				return ((DecisionCallback)thread).getNewDecisions();
			}

			throw new ParameterError("The specified ID does not return Decisions.");
	}
	

	/** ************************************************************************
	 * Create a simulation thread and start it; then return the handle.
	 *

	protected Integer sim(ModelScenario modelScenario, boolean propagate,
		DecisionScenario decisionScenario // the originating DecisionScenario
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		//
		// Create a thread for the simulation.

		UpdateRequestThread thread = new UpdateRequestThread(
			modelScenario,
			propagate,
			decisionScenario,
			null,
			modelScenario.getIterationLimit()
			);

		Integer threadHandle = new Integer(thread.hashCode());

		threads.put(threadHandle, thread);


		//
		// Start the thread.

		GlobalConsole.println(
			"Starting simulation for ModelScenario " + modelScenario.getName() +
			" and DecisionScenario " + 
			(decisionScenario == null ? "null" : decisionScenario.getName()));

		thread.start();
		
		return threadHandle;
	}
	*/

	
	public ModelDomain createModelDomain_internal(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		String domainName = name;
		if (modelDomains.containsKey(name))
			if (useNameExactly) throw new ParameterError(
				"A ModelDomain with the name " + name + " already exists.");
			else
				domainName = createUniqueModelDomainName();

		synchronized (modelDomains)
		{
			final ModelDomain domain = new ModelDomainImpl(domainName);
			modelDomains.put(domainName, domain);
			
			
			// Notify DomainSpaceVisuals that a new Model Domain has been created.
			
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.DomainCreatedNotice(domain.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.println("Unable to notify some listeners");
				GlobalConsole.printStackTrace(ex);
			}
		
			return domain;
		}
	}


	public ModelDomain createModelDomain_internal()
	throws
		ParameterError,
		IOException
	{
		synchronized (modelDomains)
		{
			String name = createUniqueModelDomainName();
			return createModelDomain_internal(name, true);
		}
	}
	
	
	protected String createUniqueModelDomainName()
	{
		synchronized (modelDomains)
		{
			int i = 1;
			String base = "New Domain ";
			for (;;)
			{
				if (i == 1000) throw new RuntimeException(
					"Tried 1000 names and could not find a unique one");
				
				String newName = base + i++;
				if (modelDomains.get(newName) != null) continue;
				
				return newName;
			}
		}
	}
	

	protected String createUniqueDecisionDomainName()
	{
		synchronized (decisionDomains)
		{
			int i = 1;
			String base = "New Domain ";
			for (;;)
			{
				if (i == 1000) throw new RuntimeException(
					"Tried 1000 names and could not find a unique one");
				
				String newName = base + i++;
				if (decisionDomains.get(newName) != null) continue;
				
				return newName;
			}
		}
	}
	

	public DecisionDomain createDecisionDomain_internal(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		String domainName = name;
		if (decisionDomains.containsKey(name))
			if (useNameExactly) throw new ParameterError(
				"A DecisionDomain with the name " + name + " already exists.");
			else
				domainName = createUniqueDecisionDomainName();

		DecisionDomain domain = new DecisionDomainImpl(domainName);
		decisionDomains.put(domainName, domain);
		return domain;
	}
	
	
	public ModelScenario createModelScenario_internal(String modelDomainName,
		String scenarioName)
	throws
		ParameterError,
		IOException
	{
		ModelDomain modelDomain = modelDomains.get(modelDomainName);
		if (modelDomain == null) throw new ParameterError(
			"Unable to identify Model Domain " + modelDomainName);
		
		ModelScenario scenario = modelDomain.createScenario(scenarioName);
		
		
		// Notify clients.
		
		try { callbackManager.notifyAllListeners(
			new PeerNoticeBase.ScenarioCreatedNotice(modelDomain.getNodeId(),
				scenario.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
		
		return scenario;
	}


	public ModelScenario createModelScenario_internal(String modelDomainName)
	throws
		ParameterError,
		IOException
	{
		ModelDomain modelDomain = modelDomains.get(modelDomainName);
		if (modelDomain == null) throw new ParameterError(
			"Unable to identify Model Domain " + modelDomainName);
		
		ModelScenario scenario = modelDomain.createScenario();
		
		
		// Notify clients.
		
		try { callbackManager.notifyAllListeners(
			new PeerNoticeBase.ScenarioCreatedNotice(modelDomain.getNodeId(),
				scenario.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
		
		return scenario;
	}



	/** ************************************************************************
	 * 
	 */
	 
	protected double computeStatistic(StatOptionsType option, Serializable[] nodeHistory)
	throws
		ParameterError
	{
		SummaryStatistics sumStat = new SummaryStatisticsImpl();
		
		for (Serializable o : nodeHistory)
		{
			Number number = null;
			try { number = (Number)o; }
			catch (ClassCastException cce) { throw new ParameterError(
				"An object in the node history is not a Number: cannot calculate stats"); }
			
			if (number == null) continue;  // skip null values.
			double d = number.doubleValue();
			sumStat.addValue(d);
		}
		
		if (option.equals(StatOptionsType.mean)) return sumStat.getMean();
		else if (option.equals(StatOptionsType.geomean)) return sumStat.getGeometricMean();
		else if (option.equals(StatOptionsType.max)) return sumStat.getMax();
		else if (option.equals(StatOptionsType.min)) return sumStat.getMin();
		else if (option.equals(StatOptionsType.sd)) return sumStat.getStandardDeviation();
		//else if (option.equals(StatOptionsType.gamma))
		//	return computeGamma(nodeHistory)
		else
			throw new RuntimeException("Unexpected option");
	}
	
	
	protected List<StatOptionsType> parseStatOptionString(String[] optionsStrings)
	throws
		ParameterError
	{
		int n = optionsStrings.length;
		for (int i = 0; i < n; i++) optionsStrings[i] = 
			optionsStrings[i].trim().toLowerCase();
		
		List<StatOptionsType> options = new Vector<StatOptionsType>();
		for (String s : optionsStrings)
		{
			if (s.equals("mean")) options.add(StatOptionsType.mean);
			else if (s.equals("geomean")) options.add(StatOptionsType.geomean);
			else if (s.equals("max")) options.add(StatOptionsType.max);
			else if (s.equals("min")) options.add(StatOptionsType.min);
			else if (s.equals("sd")) options.add(StatOptionsType.sd);
			else if (s.equals("gamma")) throw new ParameterError(
				"The 'gamma' option is not supported yet");
			else throw new ParameterError("Unrecognized option: " + s);
		}
		
		return options;
	}
	
	
	protected double[] getResultStatistics_internal(State state,
		ModelScenario scenario, List<StatOptionsType> options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		Serializable[] values = scenario.getStateFinalValues(state);
		
		int nStats = options.size();
		
		double[] stats = new double[nStats];
		
		int i = 0;
		for (StatOptionsType option : options)
		{
			double stat = computeStatistic(option, values);
			stats[i++] = stat;
		}
		
		return stats;
	}


	protected int[] getHistogram_internal(State state,
		ModelScenario scenario, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		ParameterError
	{
		// Compute the buckets.
		
		double[] bucketMaxValues = new double[noOfBuckets];
			// A value falls into a bucket B if it is > the max
			// value of the prior bucket, and <= the max value of B.
			
		double maxValue = bucketStart;
		for (int bucketNo = 0; bucketNo < noOfBuckets; bucketNo++)
		{
			maxValue += bucketSize;
			bucketMaxValues[bucketNo] = maxValue;
		}
		
		// Retrieve the Simulation Runs for the specified Model Scenario.
		
		List<SimulationRun> runs = 
				new Vector<SimulationRun>(scenario.getSimulationRuns());
		
		
		int[] histogram = new int[noOfBuckets];
		for (int b = 0; b < noOfBuckets; b++) histogram[b] = 0;  // initialize.
		
		nextRun:
		for (SimulationRun run : runs) // each run
		{
			// Get the final value of the state in this run.
			Serializable value = run.getStateFinalValue(state);
			
			// Convert the value to a double.
			
			double doubleValue = 0.0;
			try { doubleValue = ((Number)value).doubleValue(); }
			catch (ClassCastException cce) { throw new ParameterError(
				"The state " + state.getFullName() +
				" has non-numeric values", cce); }
			
			// Attempt to assign the value to a bucket.
			
			if (doubleValue < bucketStart)  // falls under all buckets.
			{
				if (allValues) throw new ParameterError(
					"Value (" + doubleValue + ") for State " + state.getFullName() +
						" falls under the lowest bucket (" + bucketStart + ")");
				continue;
			}
			
			int bucketNo = 0;
			for (double max : bucketMaxValues) // each bucket
			{
				if (doubleValue <= max)
				{
					histogram[bucketNo]++;
					continue nextRun;
				}
				
				bucketNo++;
			}
			
			// falls above all buckets.
			if (allValues) throw new ParameterError(
				"Value '" + String.valueOf(doubleValue) + "' for State '" + 
					state.getFullName() + "' falls above the highest bucket (" +
						String.valueOf(maxValue) + ")");
		}
		
		return histogram;
	}
	

	/** ************************************************************************
	 * 
	 */
	
	protected String resolveNodePath_internal(String path)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{			
		if (path == null) throw new ParameterError("path is null");
		if (path.equals("")) throw new ParameterError("Path is an empty string");
		
		String domainName = null;
		
		String[] pathParts = path.split("\\.");
		
		if (pathParts.length < 1) throw new RuntimeException();
		domainName = pathParts[0];
		
		String elementName = "";
		for (int partNo = 1; partNo < pathParts.length; partNo++)
			elementName = elementName + pathParts[partNo];
		
		PersistentNode node = null;
		
		try
		{
			node = getModelElement_internal(domainName, elementName);
			return node.getNodeId();
		}
		catch (ElementNotFound enf) {}
		
		node = getDecisionElement(domainName, elementName);
		return node.getNodeId();
	}
	
	
	/** ************************************************************************
	 * 
	 */
	
	String[] getChildNodeIds_internal(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ElementNotFound(nodeId);
		
		if (! (node instanceof PersistentNodeContainer)) return new String[] {};
		
		List<String> childNodeIds = new Vector<String>();
		
		Set<PersistentNode> childNodes = ((PersistentNodeContainer)node).getChildren();
		for (PersistentNode childNode : childNodes)
		{
			childNodeIds.add(childNode.getNodeId());
		}
		
		return childNodeIds.toArray(new String[] {} );
	}
	

	/** ************************************************************************
	 * 
	 */
	
	protected ModelElement getModelElement_internal(String modelDomainName,
		String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		if (modelDomainName == null) throw new ParameterError(
			"Null modelDomainName");

		ModelDomain modelDomain = modelDomains.get(modelDomainName);

		if (modelDomain == null) throw new ElementNotFound();

		ModelElement element = null;
		if ((elementName == null) || elementName.equals("")) element = modelDomain;
		else element = modelDomain.findElement(elementName);

		if (element == null) throw new ElementNotFound();

		return element;
	}


	/** ************************************************************************
	 * Recursively adds to "elements" all elements and child elements that have
	 * Attributes of the specified type. If attrType is null or scenario is null
	 * then do not filter based on the Attribute value type.
	 *
	 * Note: See also the methods ModelScenario.identifyTags and
	 * ModelScenario.identifyTaggedStates.
	 */
	 
	protected void getElementsWithAttributeRecursive(List<ModelElement> elements,
		ModelElement element, Class attrType, ModelScenario scenario)
	throws
		ParameterError
	{
		Collection<Attribute> attributes = element.getAttributes().values();
		
		for (Attribute attr : attributes)
		{
			if ((attrType == null) || (scenario == null))
				elements.add(element);
			else
			{
				Serializable value = scenario.getAttributeValue(attr);
				
				if (value instanceof Class)
				{
					if (attrType.isAssignableFrom((Class)value))
					{
						if (! elements.contains(element))
						{
							elements.add(element);
						}
					}
				}
				else if (attrType.isAssignableFrom(value.getClass()))
				{
					if (! elements.contains(element))
					{
						elements.add(element);
					}
				}
			}
			
			
			// Call recursively on any Attributes of each Attribute, since an
			// Attribute is itself a Model Element that may have Attributes.
			
			//getElementsWithAttributeRecursive(elements, attr, attrType, scenario);
		}
		
		
		// Call recursively.
		
		Set<ModelElement> subElements = element.getSubElements();
		
		for (ModelElement subElement : subElements)
		{
			getElementsWithAttributeRecursive(elements, subElement,
				attrType, scenario);
		}
	}
	
	
	/** ************************************************************************
	 * 
	 */
	
	protected DecisionElement getDecisionElement(String decisionDomainName,
		String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		if (elementName == null) throw new ParameterError(
			"Null decisionDomainName");

		DecisionDomain decisionDomain = decisionDomains.get(decisionDomainName);

		if (decisionDomain == null) throw new ElementNotFound();

		DecisionElement element = null;
		if ((elementName == null) || elementName.equals("")) element = decisionDomain;
		else element = decisionDomain.findElement(elementName);

		if (element == null) throw new ElementNotFound();

		return element;
	}


	/** ************************************************************************
	 * Find the Persistent Node specified by the path, or return null if it
	 * is not found.
	 */

	PersistentNode findNode(String path)
	throws
		ParameterError  // if the path is mal-formed.
	{
		PersistentNode node = null;

		int position = path.indexOf('.');
		if (position < 0)
		{
			// Name is not a compound name: might be a domain name.

			node = decisionDomains.get(path);
			if (node != null) return node;

			node = modelDomains.get(path);
			return node;  // may be null.
		}
		else
		{
			// A compound name. First part might be a domain name.

			String domainName = path.substring(0, position);
			if (domainName.equals("")) return null;

			PersistentNode domain = decisionDomains.get(domainName);
			if (domain == null) domain = modelDomains.get(domainName);

			if (domain == null) return null;

			String remainingPath = path.substring(position+1);
			if (remainingPath.equals("")) return null;

			if (domain instanceof ModelDomain)
			{
				return ((ModelDomain)domain).findElement(remainingPath);
			}

			if (domain instanceof DecisionDomain)
			{
				return ((DecisionDomain)domain).findElement(remainingPath);
			}

			throw new RuntimeException("Unexpected domain type: " + node.getClass().getName());
		}
	}


	/** ************************************************************************
	 * Signal the server application to stop all activity gracefully. If a graceful
	 * stop is not possible, then stop all threads forcefully and return false.
	 */
	 
	boolean stopAllServiceThreads()
	{
		boolean couldStopAllTasksGracefully = true;
		
		Set<Integer> callbackIdSet = threads.keySet();

		for (Integer callbackIdInteger : callbackIdSet)
		{
			try
			{
				int callbackId = callbackIdInteger.intValue();
				
				// Try to gracefully stop the task.
				
				abort(callbackId);
				
				// Wait awhile until it stops.
			
				int noOfTries = 0;
				while (! (isAborted(callbackId) || isComplete(callbackId)))
				{
					noOfTries++;
					if (noOfTries > 10)
					{
						couldStopAllTasksGracefully = false;
						
						ServiceThread thread = threads.get(callbackIdInteger);
						((Thread)thread).stop();
						
						break;
					}
					
					try { Thread.currentThread().sleep(100); }
					catch (InterruptedException ie) { return couldStopAllTasksGracefully; }
				}
			}
			catch (Exception ex)
			{
				GlobalConsole.println(ex.getMessage());
				couldStopAllTasksGracefully = false;
				continue;
			}
		}
		
		return couldStopAllTasksGracefully;
	}
	
	
	void abort(int callbackId)
	throws
		CallbackNotFound
	{
		ServiceThread thread = this.threads.get(new Integer(callbackId));

		if (thread == null)  // not found.
			throw new CallbackNotFound();

		thread.abort();
	}
	
	
	boolean isAborted(int callbackId)
	throws
		CallbackNotFound
	{
		ServiceThread thread = threads.get(new Integer(callbackId));

		if (thread == null)  // not found.
			throw new CallbackNotFound();

		return thread.isAborted();
	}
	
	
	boolean isComplete(int callbackId)
	throws
		CallbackNotFound
	{
		ServiceThread thread = threads.get(new Integer(callbackId));

		if (thread == null)  // not found.
			throw new CallbackNotFound();

		return thread.isComplete();
	}
	
	
	protected ModelScenario getScenarioByName(String modelDomainName, String scenarioName)
	throws
		ElementNotFound
	{
		// Identify the Model Domain.
		ModelDomain modelDomain = modelDomains.get(modelDomainName);
		if (modelDomain == null) throw new ElementNotFound(
			"Model Domain '" + modelDomainName + "' not found");
		
		// Identify the scenario.
		try { return modelDomain.getModelScenario(scenarioName); }
		catch (ParameterError pe) { throw new ElementNotFound(pe); }
	}
	
	
	protected SimulationRun getSimulationRunByName(String modelDomainName,
		String scenarioName, String simRunName)
	throws
		ElementNotFound
	{
		ModelScenario scenario = getScenarioByName(modelDomainName, scenarioName);
		
		SimulationRun simRun = scenario.getSimulationRun(simRunName);
		if (simRun == null) throw new ElementNotFound(
			"Simulation Run '" + simRunName + "' not found within " +
				scenarioName);
				
		return simRun;
	}

	
	/** ************************************************************************
	 * 
	 */
	 
	protected List<GeneratedEvent>[] getEventsByEpoch_internal(
		String modelDomainName, String modelScenarioName,
		String simRunName, List<String> paths, Class[] stateTags,
		List<String>[] filteredStatePathsHolder
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return getEventsByEpoch_internal(modelDomainName, modelScenarioName,
			simRunName, paths, stateTags, filteredStatePathsHolder, false, null);
	}
	
	
	/** ************************************************************************
	 * Note: The first (0th) element in the returned List is for iteration 1.
	 * If epoch 1 was a startup Epoch, then no Events will be listed for it.
	 */
	 
	protected List<GeneratedEvent>[] getEventsByEpoch_internal(
		String modelDomainName, String modelScenarioName,
		String simRunName, List<String> paths, Class[] stateTags,
		List<String>[] filteredStatePathsHolder,
		boolean returnEventSers, List<EventSer>[][] eventSersHolder)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		if (eventSersHolder != null) returnEventSers = true;		
		
		// Identify the Model Domain.
		ModelDomain modelDomain = modelDomains.get(modelDomainName);
		if (modelDomain == null) throw new ElementNotFound(
			"Model Domain '" + modelDomainName + "' not found");
		
		// Identify the scenario.
		ModelScenario modelScenario =
			modelDomain.getModelScenario(modelScenarioName);
		
		// Ientify the Simulation Run.
		SimulationRun simRun = modelScenario.getSimulationRun(simRunName);
		if (simRun == null) throw new ElementNotFound(
			"Simulation Run '" + simRunName + "' not found within " +
				modelScenarioName);
		
		// Identify each Element.
		List<ModelElement> elements = new Vector<ModelElement>();
		List<String> filteredStatePaths = new Vector<String>();
		for (String path : paths)
		{
			ModelElement e = modelDomain.findElement(path);
			if (e == null) throw new ElementNotFound(
				"Path '" + path + "' not found within Model Domain " +
					modelDomain.getName());
					
			if (! (e instanceof State)) throw new ParameterError(
				"History is currently only supported for States.");
			
			
			// Filter.
			boolean keep = false;  // if true, report on this State.
				
			if (stateTags == null) keep = true;  // no filter
			else
			{
				// Find any Attributes that e might have.
				Map<String, Attribute> attrMap = e.getAttributes();
				Collection<Attribute> attrCol = attrMap.values();
				attrsloop: for (Attribute a : attrCol)
					for (Class tag : stateTags)
						if (modelScenario.getAttributeValue(a).getClass() == tag)
						{
							// attrCol.contains one of stateTags
							keep = true;
							break attrsloop;
						}
			}
				
			if (keep)
			{
				elements.add(e);
				filteredStatePaths.add(e.getFullName());
			}
		}
		
		if (stateTags != null) filteredStatePathsHolder[0] = filteredStatePaths;
		
		
		// Retrieve the Event history from the Simulation Run, pertaining
		// to the ModelElements of interest.
		
		List<Epoch> epochs = simRun.getEpochs();
		if (epochs.size() < 1) throw new ModelContainsError("No epochs");
		
		List<GeneratedEvent>[] arrayOfListOfEventsOfInterest = new List[epochs.size()];
		if (returnEventSers) eventSersHolder[0] = new List[epochs.size()];
		List<EventSer>[] eventSers = null;
		if (eventSersHolder != null) eventSers = eventSersHolder[0];
		
		int iteration = 0;
		for (Epoch epoch : epochs)
		{
			iteration++;
			
			SortedEventSet<GeneratedEvent> eventsAtEpoch;
			//SortedEventSet eventsAtEpoch;
			try { eventsAtEpoch = simRun.getGeneratedEventsAtIteration(iteration); }
			catch (ParameterError pe)
			{
				// Most likey the simulation aborted before the final iteration completed.
				eventsAtEpoch = new SortedEventSetImpl();
			}
			
			
			// Create a result row for this epoch.
			
			List<GeneratedEvent> eventsAtEpochOfInterest = new Vector<GeneratedEvent>();
			
			List<EventSer> eventSersAtEpochOfInterest = null;
			if (returnEventSers) eventSersAtEpochOfInterest = new Vector<EventSer>();
			

			// Add one element to the row for each column: one for each
			// caller-specified element.
			
			for (ModelElement element : elements)
			{
				// Check if eventsAtEpoch contains an Event involving element.
				// If so, create a column value; otherwise, write a "No Change" event.
				
				boolean found = false;
				CheckEvents: for (GeneratedEvent event : eventsAtEpoch)
				{
					if (! (event instanceof GeneratedEvent)) continue;  // skip Startup Events.
					
					if ((event.getState() != null) && (event.getState() == element))
					{
						found = true;
						
						// Check if there is already an event for this state in this epoch.
						
						boolean duplicateEvent = false;
						List<Event> eventsCopy = new Vector<Event>(eventsAtEpochOfInterest);
						for (Event otherEvent : eventsCopy)
						{
							if (otherEvent.getState() == event.getState())
							{
								if (otherEvent instanceof EventConflict) continue CheckEvents;
								
								Serializable otherValue = otherEvent.getNewValue();
								if (ObjectValueComparator.compare(event.getNewValue(), otherValue))
								{
									duplicateEvent = true;
								}
								else  // Replace the other Event with a Conflict Event.
								{
									EventConflict conflict = new EventConflict((GeneratedEvent)event);
									eventsAtEpochOfInterest.remove(otherEvent);
									eventsAtEpochOfInterest.add(conflict);
									
									if (returnEventSers)
										eventSersAtEpochOfInterest.add((EventSer)(conflict.externalize()));
								
									continue CheckEvents;
								}
							}
						}
						
						if (! duplicateEvent) eventsAtEpochOfInterest.add(event);
						
						if (returnEventSers)
							eventSersAtEpochOfInterest.add((EventSer)(event.externalize()));
					}
					
					if (eventsAtEpochOfInterest.size() > elements.size())
						throw new InternalEngineError(
							"Mismatched no. of elements in result row");
				}
				
				if (! found) // the state has no event in this epoch.
				{
					// Find the most recent event for this state
					GeneratedEvent priorEvent = null;
					if (iteration > 1) // there is a prior row,
						// Copy the event for this state;
					{
						List<GeneratedEvent> priorRow = 
							arrayOfListOfEventsOfInterest[iteration-2];
							
						for (GeneratedEvent e : priorRow)
						{
							if (e.getState() == element)
							{
								priorEvent = e;
								break;
							}
						}
					}
					
					if (priorEvent == null)
					{
						// Insert a null valued event.
						eventsAtEpochOfInterest.add(
							new NoChangeEvent((State)element, epoch.getDate(), 
							null, simRun, epoch, null, null, false));
							
						if (returnEventSers)
							eventSersAtEpochOfInterest.add((EventSer)
								((new NoChangeEvent(
								(State)element, epoch.getDate(), null, simRun,
								epoch, null, null, false)).externalize()));
					}
					else
					{
						// Insert a "no change" event in the row/column.
						eventsAtEpochOfInterest.add(
							new NoChangeEvent((State)element, epoch.getDate(), 
								priorEvent.getNewValue(), simRun,
								epoch,
								priorEvent.getTriggeringEvents(),
								priorEvent.getWhenScheduled(),
								priorEvent.isCompensation()
								));
							
						if (returnEventSers)
							eventSersAtEpochOfInterest.add((EventSer)
								((new NoChangeEvent(
									(State)element, epoch.getDate(),
									priorEvent.getNewValue(), simRun,
									epoch,
									priorEvent.getTriggeringEvents(),
									priorEvent.getWhenScheduled(),
									priorEvent.isCompensation()
									)).externalize()));
					}
				}
			
				if (eventsAtEpochOfInterest.size() > elements.size())
				{
					GlobalConsole.println("Elements at epoch " + epoch + ":");
					for (ModelElement e : elements) GlobalConsole.println("\t" + e.getFullName());
					
					GlobalConsole.println("Events at epoch " + epoch + ":");
					for (Event ee : eventsAtEpochOfInterest) GlobalConsole.println("\t" + ee);
					
					throw new InternalEngineError("Mismatched number of elements in result row");
				}
			}
			
			if (eventsAtEpochOfInterest.size() != elements.size())
			{
				GlobalConsole.println("Elements at epoch " + epoch + ":");
				for (ModelElement e : elements) GlobalConsole.println("\t" + e.getFullName());

				GlobalConsole.println("Events at epoch " + epoch + ":");
				for (Event ee : eventsAtEpochOfInterest) GlobalConsole.println("\t" + ee);
				
				throw new InternalEngineError("Mismatched number of elements in result: " +
					"found " + eventsAtEpochOfInterest.size() + ", expected " + elements.size());
			}
			
			arrayOfListOfEventsOfInterest[iteration-1] = eventsAtEpochOfInterest;
			if (returnEventSers) eventSers[iteration-1] = eventSersAtEpochOfInterest;
			
			if (eventsAtEpochOfInterest == null) throw new RuntimeException(
				"Event list is null for iteration " + iteration);
		}
		
		return arrayOfListOfEventsOfInterest;
	}	
	
	
	protected Conduit createConduit_internal(boolean confirm, 
		String parentId, // may be null
		String portAId, String portBId)
	throws
		Warning,
		CannotObtainLock,
		ParameterError
	{
		ModelContainer container = null;
		
		PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
		if (parentNode != null)
		{
			container = (ModelContainer)parentNode;
		
			if (! (parentNode instanceof ModelContainer)) throw new ParameterError(
				"Can only call this method on a Model Container.");
		}
		
		PersistentNode na = PersistentNodeImpl.getNode(portAId);
		if (na == null) throw new ParameterError(
			"Cannot identify the Node with ID " + portAId);
		
		if (! (na instanceof Port)) throw new ParameterError(
			"Node " + portAId + " is not a Port");
		
		Port portA = (Port)na;
		
		if (! confirm)
		{
			if (portA.getModelDomain().hasSimulationRuns())
				throw new Warning("Simulation Runs will be invalid and will be deleted");
		}
		
		PersistentNode nb = PersistentNodeImpl.getNode(portBId);
		if (nb == null) throw new ParameterError(
			"Cannot identify the Node with ID " + portBId);
		
		if (! (nb instanceof Port)) throw new ParameterError(
			"Node " + portBId + " is not a Port");
		
		Port portB = (Port)nb;
		
		if (portA.isLockedForSimulation()) 
			throw new CannotModifyDuringSimulation(portA.getFullName());
			
		if (portB.isLockedForSimulation()) 
			throw new CannotModifyDuringSimulation(portB.getFullName());
			
		if (container == null)
			container = portA.chooseContainerForConduit(portB);
		
		return container.createConduit(container.createUniqueSubElementName(
			portA.getName() + "_" + portB.getName()), portA, portB);
	}
	


	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Diagnostic methods.
	 *
	 */

	
	/** ************************************************************************
	 * 
	 */
	
	void assertTrue(String msg, boolean condition)
	{
		if (! condition) GlobalConsole.println("ASSERTION FAILURE: " + msg);
	}
}

