/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;

import expressway.common.ModelElement.*;
import expressway.common.DecisionElement.*;
import expressway.common.ModelAPITypes.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.util.List;
import java.util.Vector;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import org.apache.commons.math.distribution.ContinuousDistribution;


/**
 * Implements the ModelEngine interface using a Java object without any
 * persistence. Methods are synchronized to provide guarded re-entrancy.
 *
 * This implementation requires the user to identify a 'current' Decision
 * Domain, and that is used as the Decision Domain for all requests until
 * the user specifies another Decision Domain.
 */

public class ModelEngineStubImpl implements ModelEngine, Serializable
{
	public List<ModelDomain> modelDomains = new Vector<ModelDomain>();
	public List<DecisionDomain> decisionDomains = new Vector<DecisionDomain>();


	public ModelEngineStubImpl()
	{
		try
		{
			ModelDomainImpl md =
				new ModelDomainImpl("Performance_Domain", 10, 10, 400, 250);

			// x, y, width, height.
			ActivityImpl a1 = new ActivityImpl("a1", 10, 10, 150, 100, md);
			ActivityImpl a2 = new ActivityImpl("a2", 200, 10, 150, 100, md);

			// x, y.
			PortImpl a1p1 = new PortImpl("a1p1", 150, 20, a1);
			PortImpl a2p1 = new PortImpl("a2p1", 0, 40, a2);

			ConduitImpl c1 = new ConduitImpl("c1", a1p1, a2p1, md);

			modelDomains.add(md);

			//System.out.println("ModelEngineStubImpl instantiated.");
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}
	}


	public synchronized String ping()
	{
		return null;
	}


	public void listenToComponent(String nodeId, PeerListener peerListener)
	throws
		ElementNotFound,
		IOException
	{
	}


	public ModelDomain getModelDomain(String name)
	{
		return null;
	}

	
	public DecisionDomain getDecisionDomain(String name)
	{
		return null;
	}
	

	public List<String> getDomainNames()
	{
		return null;
	}
	

	public ModelDomain createModelDomain(String name)
	throws
		ParameterError
	{
		return null;
	}


	public DecisionDomain createDecisionDomain(String name)
	throws
		ParameterError
	{
		return null;
	}


	public List<String> getProgress(int callbackId)
	throws
		ParameterError
	{
		return null;
	}


	public List<String> getMessages(int callbackId)
	throws
		ParameterError
	{
		return null;
	}


	public boolean isComplete(int callbackId)
	throws
		ParameterError
	{
		return false;
	}


	public synchronized boolean isAborted(int callbackId)
	throws
		ParameterError
	{
		return false;
	}
	
	
	public void abort(int callbackId) {}


	public Integer[] simulate(String modelDomainName, String scenarioName, 
		int iterations, boolean step)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		return null;
	}


	public Integer[] updateVariable(String domainName, String scenarioName,
		String decisionPointName,
		String variableName, Vector valueVector, boolean propagate, boolean step)
	throws
		ClientIsStale,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		Integer[] threadHandles = {new Integer(20)};
		return threadHandles;
	}


	ModelElement getModelElement(String fullElementPath)
	throws
		ElementNotFound,
		ParameterError
	{
		....
	}


	public ModelElement getModelElement(String modelDomainName,
		String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		ModelElement element = null;

		try
		{
			element = modelDomains.get(0);

			//System.out.println(
			//	"Calling " + element.getName() + ".externalize()...");
			
			element = (ModelElement)(element.externalize());
			
			//System.out.println(
			//	"...returned from " + element.getName() + ".externalize().");
		}
		catch (RuntimeException ex)
		{
			ex.printStackTrace();
			throw ex;
		}

		return element;
	}


	public DecisionElement getDecisionElementTree(String decisionDomainName,
		String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		DecisionElement element = null;

		try
		{
			element = decisionDomains.get(0);
			element = (DecisionElement)(element.externalize());
		}
		catch (RuntimeException ex)
		{
			ex.printStackTrace();
			throw ex;
		}

		return element;
	}


	public List<Number>[] getResultColumns(Vector elementPaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		return null;
	}


	public synchronized Object getAttributeValue(String attrPath, String scenarioName)
	throws
		ParameterError
	{
		return null;
	}

	
	public synchronized Scenario[] getScenarios(String domainName)
	throws
		ParameterError
	{
		return null;
	}

	
	public synchronized Attribute[] getAttributes(String elementPath)
	throws
		ParameterError
	{
		return null;
	}

	
	public synchronized Decisions[] getDecisions(String decisionPointPath, String scenarioName)
	throws
		ParameterError
	{
		return null;
	}

	
	public synchronized Object getFinalStateValue(String statePath, String scearioName)
	throws
		ParameterError
	{
		return null;
	}

	
	public synchronized void watchNode(boolean enable, String nodePath)
	throws
		ParameterError
	{
	}

	
	public void dump()
	{
		dump(0);
	}


	public void dump(int indentation)
	{
	}


	public abstract class ModelElementImpl implements ModelElement
	{
		public String classname = "expressway.ModelElementImpl";
		public DisplayImpl display = null;
		public String name = null;
		public double width = 0;
		public double height = 0;
		public ModelDomain domain = null;
		ModelElement parent = null;


		public ModelElementImpl(String name, ModelElement parent)
		{
			this.name = name;
			this.display = new DisplayImpl();

			this.parent = parent;
		}


		public PersistentNode externalize()
		{
			//System.out.println("\tExternalizing " + name);
			return this;
		}


		public String getName() { return name; }


		public void setName(String name) { this.name = name; }


		public double getWidth() { return width; }


		public double getHeight() { return height; }


		public void setWidth(double width) { this.width = width; }


		public void setHeight(double height) { this.height = height; }


		public Map<String, Attribute> getAttributes() { return null; }


		public Attribute createAttribute(String name, Object defaultValue)
		{
			return null;
		}

		public ModelDomain getModelDomain() { return domain; }


		public ModelElement getModelElement()
		throws
			CannotObtainLock,
			ElementNotFound,
			ModelContainsError { return this; }


		public String toString() { return getClass().getName(); }


		public void dump() {}


		public void dump(int i) {}


		public Display getDisplay() { return display; }


		public Date getDeletionDate() { return null; }


		public PersistentNode getPriorVersion() { return null; }


		public int compareTo(PersistentNode n) { return 0; }


		public double getX() { return display.getX(); }
		public double getY() { return display.getY(); }
		public double getScale() { return display.getScale(); }
		public double getOrientation() { return display.getOrientation(); }

		public void setX(double x) { display.setX(x); }
		public void setY(double y) { display.setY(y); }
		public void setScale(double scale) { display.setScale(scale); }
		public void setOrientation(double angle) { display.setOrientation(angle); }
	}


	public class ModelDomainImpl extends ModelElementImpl implements ModelDomain
	{
		/** Ownership. */
		Set<Activity> activities = null;

		/** Ownership. */
		Set<Conduit> conduits = null;

		public transient Activity[] activitiesArray = null;
		public transient Conduit[] conduitsArray = null;


		public ModelDomainImpl(String name, double x, double y, double width, double height)
		{
			super(name, null);

			classname = "expressway.ModelDomainImpl";
			display.x = x;
			display.y = y;

			this.width = width;
			this.height = height;

			activities = new HashSet<Activity>();
			conduits = new HashSet<Conduit>();
		}


		public PersistentNode externalize()
		{
			super.externalize();

			activitiesArray = activities.toArray(new Activity[] {});
			conduitsArray = conduits.toArray(new Conduit[] {});

			for (PersistentNode n : activitiesArray) n.externalize();
			for (PersistentNode n : conduitsArray) n.externalize();

			return this;
		}


		public Set<ModelScenario> getScenarios() { return null; }


		public ModelScenario getCurrentModelScenario() { return null; }


		public void setCurrentModelScenario(ModelScenario scenario)
		throws
			ElementNotFound {}


		public Scenario getCurrentScenario()
		{
			return null;
		}
	
	
		public void setCurrentScenario(Scenario scenario)
		throws
			ElementNotFound,
			ParameterError
		{
		}


		public ModelScenario createScenario(String name) { return null; }


		public ModelScenario createScenario(String name, ModelScenario scenario)
		{
			return null;
		}


		public Set<VariableAttributeBinding> getVariableAttributeBindings() { return null; }


		public void addVariableAttributeBinding(VariableAttributeBinding binding) {}


		public Set<AttributeStateBinding> getAttributeStateBindings() { return null; }


		public Set<NativeComponentImplementation> getNativeComponentImplementations()
		{
			return null;
		}


		public SimulationRun simulate(
			ModelScenario modelScenario,
			SimCallback callback,
			Date finalEpoch, int iterationLimit)
		throws
			ParameterError { return null; }


		public SimulationRun simulate(ModelScenario modelScenario,
			SimCallback callback, int iterationLimit)
		throws
			ParameterError { return null; }


		public Set<Function> getFunctions() { return null; }


		public Set<Activity> getActivities() { return null; }


		public Set<Conduit> getConduits() { return null; }


		public Set<Constraint> getConstraints() { return null; }


		public ModelElement getChild(String name)
		throws
			ElementNotFound { return null; }


		public void addComponent(ModelComponent component)
		throws
			ParameterError {}


		public void addConstraint(Constraint constraint)
		throws
			ParameterError {}


		public Function createFunction(String name, Class nativeImplClass)
		throws
			ParameterError { return null; }


		public Terminal createTerminal(String name)
		throws
			ParameterError { return null; }


		public Generator createGenerator(String name, double timeDistShape,
			double timeDistScale, double valueDistShape, double valueDistScale,
			boolean ignoreStartup)
		throws
			ParameterError { return null; }


		public Activity createActivity(String name, Class nativeImplClass)
		throws
			ParameterError { return null; }


		public Conduit createConduit(String name, Port portA, Port portB)
		throws
			ParameterError { return null; }


		public Satisfies createSatisfiesRelation(String name)
		throws
			ParameterError { return null; }


		public Derives createDerivesRelation(String name)
		throws
			ParameterError { return null; }


		public ModelContainer getParent()  { return null; }


		public ModelElement findElement(String qualifiedName)
		throws
			ParameterError { return null; }
	}


	public class ActivityImpl extends ModelElementImpl implements Activity
	{
		/** Ownership. */
		Set<Activity> activities = null;

		/** Ownership. */
		Set<Port> ports = null;

		/** Ownership. */
		Set<Conduit> conduits = null;

		public transient Activity[] activitiesArray = null;
		public transient Port[] portsArray = null;
		public transient Conduit[] conduitsArray = null;


		public ActivityImpl(String name, double x, double y, double width, double height,
			ModelElement parent)
		{
			super(name, parent);
			//super(name, null);

			classname = "expressway.ActivityImpl";
			display.x = x;
			display.y = y;

			this.width = width;
			this.height = height;
			activities = new HashSet<Activity>();
			ports = new HashSet<Port>();
			conduits = new HashSet<Conduit>();

			if (parent == null) return;

			if (parent instanceof ModelDomain)
			{
				((ModelDomainImpl)parent).activities.add(this);
			}
			else if (parent instanceof Activity)
			{
				((ActivityImpl)parent).activities.add(this);
			}
			else throw new RuntimeException(
				"Unexpected type");
		}


		public PersistentNode externalize()
		{
			super.externalize();

			activitiesArray = activities.toArray(new Activity[] {});
			portsArray = ports.toArray(new Port[] {});
			conduitsArray = conduits.toArray(new Conduit[] {});

			for (PersistentNode n : activitiesArray) n.externalize();
			for (PersistentNode n : portsArray) n.externalize();
			for (PersistentNode n : conduitsArray) n.externalize();

			return this;
		}


		public void setNativeImplementationClass(Class nativeImplClass)
		{
		}


		public NativeActivityImplementation getNativeImplementation() { return null; }


		public synchronized Set<GeneratedEvent> respond(SimulationRun simRun, Set<Event> events)
		throws
			ModelContainsError
		{
			return null;
		}


		public Set<Port> getPorts() { return null; }


		public void addPort(Port port)
		throws
			ParameterError {}


		public Port createPort(String name)
		throws
			ParameterError { return null; }


		public Set<State> getStates() { return null; }


		public State createState(String name)
		throws
			ParameterError { return null; }


		public State getState(String name) { return null; }


		public Set<Function> getFunctions() { return null; }


		public Set<Activity> getActivities() { return null; }


		public Set<Conduit> getConduits() { return null; }


		public Set<Constraint> getConstraints() { return null; }


		public ModelElement getChild(String name)
		throws
			ElementNotFound { return null; }


		public void addComponent(ModelComponent component)
		throws
			ParameterError { }


		public void addConstraint(Constraint constraint)
		throws
			ParameterError {}


		public Function createFunction(String name, Class nativeImplClass)
		throws
			ParameterError { return null; }


		public Terminal createTerminal(String name)
		throws
			ParameterError { return null; }


		public Generator createGenerator(String name, double timeDistShape, 
			double timeDistScale, double valueDistShape, double valueDistScale,
			boolean ignoreStartup)
		throws
			ParameterError { return null; }


		public Activity createActivity(String name, Class nativeImplClass)
		throws
			ParameterError { return null; }


		public Conduit createConduit(String name, Port portA, Port portB)
		throws
			ParameterError { return null; }


		public Satisfies createSatisfiesRelation(String name)
		throws
			ParameterError { return null; }


		public Derives createDerivesRelation(String name)
		throws
			ParameterError { return null; }


		public ModelContainer getParent() { return null; }


		public ModelElement findElement(String qualifiedName)
		throws
			ParameterError { return null; }
	}


	public class ConduitImpl extends ModelElementImpl implements Conduit
	{
		public Port p1 = null;
		public Port p2 = null;

		//public double x_OfP1InMc = 0.0;
		//public double y_OfP1InMc = 0.0;
		public double length = 0.0;

		public InflectionPoint[] inflectionPoints = null;


		public ConduitImpl(String name, Port port1, Port port2, ModelElement parent)
		{
			super(name, parent);

			classname = "expressway.ConduitImpl";
			this.p1 = port1;
			this.p2 = port2;

			this.width = 0;
			this.height = 0;

			//this.length = length;

			this.parent = parent;

			if (parent == null) return;

			if (parent instanceof ModelDomain)
			{
				((ModelDomainImpl)parent).conduits.add(this);
			}
			else if (parent instanceof Activity)
			{
				((ActivityImpl)parent).conduits.add(this);
			}
			else throw new RuntimeException(
				"Unexpected type");
		}


		public PersistentNode externalize()
		{
			super.externalize();


			/*
			 * Compute starting point and length.
			 */

			ModelContainer mc = (ModelContainer)(this.parent);

			// Get position of p1 and p2 within their respective parents.

			double x_OfP1InParent = ((PortImpl)p1).display.x;
			double y_OfP1InParent = ((PortImpl)p1).display.y;

			double x_OfP2InParent = ((PortImpl)p2).display.x;
			double y_OfP2InParent = ((PortImpl)p2).display.y;

			// Get position (translation) of p1's parent within its own
			// parent - i.e., within mc.

			double x_OfP1ParentInMc =
				((DisplayImpl)(((PortImpl)p1).parent.getDisplay())).x;

			double y_OfP1ParentInMc =
				((DisplayImpl)(((PortImpl)p1).parent.getDisplay())).y;

			// Transform p1 coordinates from p1's parent reference frame
			// to mc reference frame.

			double p1_theta =
				((DisplayImpl)(((PortImpl)p1).parent.getDisplay())).orientation;

			double x_OfP1InMc =

				x_OfP1ParentInMc
					+ x_OfP1InParent * Math.cos(p1_theta)
						- y_OfP1InParent * Math.sin(p1_theta);

			double y_OfP1InMc =

				y_OfP1ParentInMc
					+ x_OfP1InParent * Math.sin(p1_theta)
						+ y_OfP1InParent * Math.cos(p1_theta);

			// Get position (translation) of p2's parent within its own
			// parent - i.e., within mc.

			double x_OfP2ParentInMc =
				((DisplayImpl)(((PortImpl)p2).parent.getDisplay())).x;

			double y_OfP2ParentInMc =
				((DisplayImpl)(((PortImpl)p2).parent.getDisplay())).y;

			// Transform p2 coordinates from p2's parent reference frame
			// to mc reference frame.

			double p2_theta =
				((DisplayImpl)(((PortImpl)p2).parent.getDisplay())).orientation;

			double x_OfP2InMc =

				x_OfP2ParentInMc
					+ x_OfP2InParent * Math.cos(p2_theta)
						- y_OfP2InParent * Math.sin(p2_theta);

			double y_OfP2InMc =

				y_OfP2ParentInMc
					+ x_OfP2InParent * Math.sin(p2_theta)
						+ y_OfP2InParent * Math.cos(p2_theta);

			this.display.x = x_OfP1InMc;
			this.display.y = y_OfP1InMc;

			// Compute length of this Conduit.


			double dx = x_OfP2InMc - x_OfP1InMc;
			double dy = y_OfP2InMc - y_OfP1InMc;

			this.length = Math.sqrt(dx * dx + dy * dy);

			// Compute angle of this Conduit.

			double angle = Math.atan(dy/dx);
			if (dx < 0) angle += Math.acos(-1);
			((DisplayImpl)(this.getDisplay())).orientation = angle;

			return this;
		}


		public double getLength() { return length; }


		public void connect(Port p1, Port p2)
		{
			this.p1 = p1;
			this.p2 = p2;
		}


		public Port getPort1() { return p1; }


		public Port getPort2() { return p2; }


		public ModelContainer getParent() { return null; }


		public ModelElement findElement(String qualifiedName)
		throws
			ParameterError { return null; }

		public void setInflectionPoints(InflectionPoint[] ipArray)
		{
			this.inflectionPoints = ipArray;
		}

		public InflectionPoint[] getInflectionPoints()
		{
			return inflectionPoints;
		}
	}


	public class PortImpl extends ModelElementImpl implements Port
	{
		public PortImpl(String name, double x, double y, ModelElement parent)
		{
			super(name, parent);

			classname = "expressway.PortImpl";
			display.x = x;
			display.y = y;

			this.width = 5;
			this.height = 5;

			this.parent = parent;
			((ActivityImpl)parent).ports.add(this);
		}


		public PersistentNode externalize()
		{
			super.externalize();
			return this;
		}


		public PortedContainer getContainer() { return (PortedContainer)parent; }


		public void addSourceConnection(Conduit c) { }


		public void addDestinationConnection(Conduit c) { }


		public Set<Conduit> getSourceConnections() { return null; }


		public Set<Conduit> getDestinationConnections() { return null; }
	}


	public class DisplayImpl implements Display
	{
		public double x = 0;
		public double y = 0;
		public double orientation = 0;
		public double scale = 1.0;

		public double getX() { return x; }
		public double getY() { return y; }
		public double getScale() { return scale; }
		public double getOrientation() { return orientation; }  // in radians.

		public void setX(double x) { this.x = x; }
		public void setY(double y) { this.y = y; }
		public void setScale(double scale) { this.scale = scale; }
		public void setOrientation(double angle) { this.orientation = angle; }  // in radians.
	}
}
