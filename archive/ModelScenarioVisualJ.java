/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import java.util.Date;
import expressway.ser.*;
import expressway.common.ModelElement.*;
import expressway.common.VisualComponent.*;
import expressway.common.GraphicView;
import expressway.common.ModelAPITypes.*;
import java.awt.Container;
import java.awt.Color;


class ModelScenarioVisualJ extends ScenarioVisualJ implements ModelScenarioVisual
{
	private Map<Integer, Set<SimulationRunVisual>> simVisuals = 
		new HashMap<Integer, Set<SimulationRunVisual>>();
	
	
	ModelScenarioVisualJ(ModelScenarioSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		setIconImage(NodeIconImageNames.ModelScenarioIconImage);
		setBackground(getNormalBackgroundColor());
	}
	
	
	public void addSimulationRunVisual(Integer id, SimulationRunVisual simPanel)
	{
		Set<SimulationRunVisual> simVisualsForId = simVisuals.get(id);
		if (simVisualsForId == null)
		{
			simVisualsForId = new TreeSet<SimulationRunVisual>();
			simVisuals.put(id, simVisualsForId);
		}
		
		simVisualsForId.add(simPanel);
	}


	public Set<SimulationRunVisual> simulationRunCreated(Integer requestId,
		String simRunNodeId)
	{		
		return null;
	}


	public void refreshAttributeValue(String attrNodeId)
	{
	}
	
	
	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return ScenarioColor; }
	
	
	public Color getHighlightedBackgroundColor() { return ScenarioHLColor; }
}

