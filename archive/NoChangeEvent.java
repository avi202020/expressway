/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.NoChangeEventSer;
import java.util.Date;
import java.io.Serializable;


/**
 * A placeholder to indicate that no (Generated) Event has occurred, but to contain
 * information pertaining to the most recent (driving) Event for a State.
 * A NoChangeEvent should not be written to archive: it is only used as a convenience
 * to assist some user interfaces.
 */
 
public class NoChangeEvent extends EventImpl implements GeneratedEvent
{
	public Epoch epoch; // should reflect the Epoch in which there is no change.
	
	private SortedEventSet<GeneratedEvent> triggeringEvents; // may be null
	
	private Epoch whenScheduled;  // may be null
	
	public boolean compensation;
	
	
	public transient String[] triggeringEventNodeIds;
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this.getDomain())
		{
		// Set all transient externalizable Node fields.
		
		if (triggeringEvents == null)
			triggeringEventNodeIds = new String[] { };
		else
		{
			triggeringEventNodeIds = new String[triggeringEvents.size()];
			int i = 0;
			for (GeneratedEvent e : triggeringEvents) triggeringEventNodeIds[i++] = e.getNodeId();
		}
		
		
		// Set Ser fields.
		
		((NoChangeEventSer)nodeSer).epoch = epoch;
		((NoChangeEventSer)nodeSer).triggeringEvents = triggeringEventNodeIds;
		((NoChangeEventSer)nodeSer).whenScheduled = whenScheduled;
		((NoChangeEventSer)nodeSer).compensation = compensation;
		
		
		return super.externalize(nodeSer);
		}
	}

	protected Class getSerClass() { return NoChangeEventSer.class; }
	
	
	public Object clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		NoChangeEvent newInstance = (NoChangeEvent)(super.clone(cloneMap, cloneParent));
		
		newInstance.triggeringEvents = (SortedEventSet)(triggeringEvents.clone());
		
		return newInstance;
	}
	
	
	/*public PersistentNode update(NodeSer nodeSer)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		NoChangeEventSer ser = (NoChangeEventSer)
			checkInstanceCompatibility(nodeSer, NoChangeEventSer.class);
		
		return super.update(nodeSer);
	}*/
	
	
	public void prepareForDeletion()
	{
		super.prepareForDeletion();
		
		epoch = null;
		triggeringEvents = null;
		

		// Call <type>Deleted(this) on each DeletionListener.
		// (There are none.)
		
		
		// For each owned Node, call prepareForDeletion() and then remove it.
		// (There are none.)
		
		
		// Remove other non-Node owned Objects.
		// (There are none.)
	}


	public NoChangeEvent(State state, Date time, Serializable value, SimulationRun simRun,
		Epoch epoch, SortedEventSet<GeneratedEvent> triggeringEvents, Epoch whenScheduled,
		boolean compensation)
	throws
		ParameterError
	{
		super(state, time, value, simRun);
		
		if (epoch == null) throw new ParameterError("May not have a null epoch");
		
		this.epoch = epoch;
		this.triggeringEvents = triggeringEvents;
		this.whenScheduled = whenScheduled;
		this.compensation = compensation;
	}

	
	/* From SimulationTimeEvent */

	public SimulationRun getSimulationRun() { return (SimulationRun)(this.getParent()); }
	
	
	public Epoch getEpoch() { return epoch; }
	
	
	public void setEpoch(Epoch epoch) { throw new RuntimeException(
		"Attempt to set Epcoh of a NoChangeEvent"); }
	
	
	public int getIteration() { return epoch.getIteration(); }
	
	
	/* From Generated Event */

	/** Return the when-scheduled value of the original Event. */
	public Epoch getWhenScheduled() { return whenScheduled; }
	
	
	/** Return the trigger Set of the original Event. */
	public SortedEventSet<GeneratedEvent> getTriggeringEvents() { return triggeringEvents; }
	
	
	public void setTriggeringEvents(SortedEventSet<GeneratedEvent> events) 
	{ throw new RuntimeException("Attempt to set triggering Events on a NoChangeEvent"); }
	
	
	public void setCompensation(boolean comp) { throw new RuntimeException(
		"Attempt to set compensation flag for a NoChangeEvent"); }
}



