package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;


import java.util.Date;
import java.io.Serializable;

public class NoChangeEventSer extends EventSer
{
	public Epoch epoch;
	public String[] triggeringEvents;
	public Epoch whenScheduled;
	public boolean compensation;
	
	
	public Object deepCopy()
	{
		NoChangeEventSer copy = (NoChangeEventSer)(super.deepCopy());
		
		copy.triggeringEvents = new String[triggeringEvents.length];
		int i = 0;
		for (String s : triggeringEvents) copy.triggeringEvents[i++] = s;
		
		return copy;
	}
}

