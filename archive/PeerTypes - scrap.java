

	interface DomainPeer extends Peer
	{
	}


	interface ModelDomainPeer extends DomainPeer
	{
	}
	
	
	interface DecisionDomainPeer extends DomainPeer
	{
	}
	
	
	interface ScenarioPeer extends Peer
	{
	}
	
	
	interface ModelScenarioPeer extends ScenarioPeer
	{
	}
	
	
	interface DecisionScenarioPeer extends ScenarioPeer
	{
	}
	
	
	interface PortedContainerPeer extends Peer
	{
	}
	

	interface FunctionPeer extends PortedContainerPeer
	{
	}


	interface ActivityPeer extends PortedContainerPeer
	{
	}
	
	
	interface StatePeer extends Peer
	{
	}


	interface DecisionPointPeer extends Peer
	{
	}


	interface ConduitPeer extends Peer
	{
	}


	interface PortPeer extends Peer
	{
	}


	interface VariablePeer extends Peer
	{
	}


	interface AttributePeer extends Peer
	{
	}

