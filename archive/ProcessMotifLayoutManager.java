/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.motifs.process;


import java.util.*;
import expressway.common.GlobalConsole;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement;
import expressway.server.ModelElement.*;



/**
 * LayoutManager for the Process Motif. (See slide "Process Motif".)
 */
 
public class ProcessMotifLayoutManager extends LayoutManager
{
	public ProcessMotifLayoutManager() {}
	
	
	public ProcessMotifLayoutManager(double hContainerSpacing, double vContainerSpacing,
		double hNodeSpacing, double vNodeSpacing)
	{
		this(hContainerSpacing, hContainerSpacing, vContainerSpacing, vContainerSpacing,
			hNodeSpacing, vNodeSpacing);
	}
	
	
	public ProcessMotifLayoutManager(double leftHContainerSpacing, double rightHContainerSpacing,
		double topVContainerSpacing, double bottomVContainerSpacing,
		double hNodeSpacing, double vNodeSpacing)
	{
		super(leftHContainerSpacing, rightHContainerSpacing,
			topVContainerSpacing, bottomVContainerSpacing,
			hNodeSpacing, vNodeSpacing);
	}

	
	public void layout(PersistentNode node, boolean recursive, 
		PersistentNode[] nodesToIgnore)
	{
		Set<PersistentNode> children = node.getChildren();
		
		if (children.size() == 0)
		{
			node.setSizeToStandard();
			return;
		}
		
		SortedSet<PersistentNode> predefinedNodes = node.getPredefinedNodes();
		
		NextChild: for (PersistentNode child : children)
		{
			if (! child.isVisible()) continue;
			if (subNode instanceof Port) continue;
			
			if (recursive)
			{
				LayoutManager lm = child.getLayoutManager();
				if (lm != null) lm.layout(child, true);
			}
			
			if (predefinedNodes.contains(child)) continue;
			if (nodesToIgnore != null)
				for (PersistentNode n : nodesToIgnore)
					if (n == child) continue NextChild;
			
			Set<CrossReferenceable> fromNodes = child.getFromNodes("RASCI.ResponsibleFor");
			if (fromNodes.size() == 0)
			{
				assignToUnassignedRow(child);
			}
			else // More than one Role is assigned to child. We must ensure that
				// there is a Row for this unique set of Roles.
			{
				Set<Role> roles = new TreeSet<Role>();
				for (CrossReferenceable cr : fromNodes)  // each Role assigned to child
				{
					if (! (cr instanceof Role)) throw new RuntimeException(
						"Expected CrossReferenceable to be a Role: it is a " + cr.getClass().getName());
					
					roles.add((Role)cr);
				}
				
				Row row = getRowForRoles(roles);
				
				if (row == null) createRow(roles, getCenterline(child));
				else row.align(child);
			}
		}
		
		positionUnassignedRow();
	}
	
	
	/**
	 * Return the Row that is defined for the specified Role. Return null if
	 * there is no Row defined for the Role.
	 */
	 
	private Set<Row> getRowsForRole(Role role)
	{
		Set<Row> matchingRows = new HashSet<Role>();
		for (Row row : rows)
		{
			if (row.getRoles().contains(role)) matchingRows.add(row);
		}
		
		return matchingRows;
	}
	
	
	private Set<Row> getRowsForPrimaryRole(Role role)
	{
		Set<Row> matchingRows = new HashSet<Role>();
		for (Row row : rows)
		{
			if (row.isPrimaryRole(role)) matchingRows.add(row);
		}
		
		return matchingRows;
	}
	
	
	/**
	 * Find the Row that contains precisely the set of Roles specified, and no
	 * more.
	 */
	 
	private Row getRowForRoles(Set<Role> roles)
	{
		for (Row row : rows)
		{
			List<Role> rowRoles = row.getRoles();
			if (rowRoles.size() != roles.size()) continue;
			
			boolean matchesAll = true;
			for (Role role : roles)
			{
				if (! rowRoles.contains(role))
				{
					matchesAll = false;
					break;
				}
			}
			
			if (matchesAll) return row;
		}
		
		return null;
	}
	
	
	/**
	 * Create a Row as specified.
	 */
	 
	private final Row createRow(Role role, double centerline)
	{
		rows.add(new Row(role, centerline));
	}
	
	
	/**
	 * Create a Row as specified.
	 */
	 
	private final Row createRow(Set<Role> roles, double centerline)
	{
		rows.add(new Row(roles, centerline));
	}
	
	
	/**
	 * Place the specified Node in the Row for Nodes that are not assigned a Role.
	 */
	 
	void assignToUnassignedRow(PersistentNode node)
	{
		if (rowForNodesWithoutRoles == null)
			rowForNodesWithoutRoles = new Row();  // Create lazily if needed.
		
		rowForNodesWithoutRoles.align(node);
	}
	
	
	public static final double DefaultCenterlineSpacing = 100;
	
	void positionUnassignedRow()
	{
		if (rowForNodesWithoutRoles == null) return;
		
		double lowestCenterline = 0;
		double averageCenterlineSpacing = 0;
		double priorCenterline;
		double centerlineSpacing = 0;
		int rowCount = 0;
		for (Row row : rows)
		{
			double centerline = row.getCenterline();
			if (rowCount++ > 0) centerlineSpacing += Max.abs(priorCenterline - centerline);
			lowestCenterline = Math.min(lowestCenterline, centerline);
			priorCenterline = centerline;
		}
		
		if (rowCount == 0) centerlineSpacing = DefaultCenterlineSpacing;
		else centerlineSpacing /= rowCount;
		rowForNodesWithoutRoles.setCenterline(lowestCenterline - centerlineSpacing);
	}
	
	
	/**
	 * Return the y coordinate of the center of the specified Activity.
	 */
	 
	private final double getCenterline(PersistentNode a)
	{
		return a.getY() + a.getHeight()/2;
	}
	
	
	private List<Row> rows = new Vector<Row>();
	private Row rowForNodesWithoutRoles = null;
	
	
	private final class Row
	{
		private List<Role> roles = new Vector<Role>;
		private double centerline;
		
		Row(Role role, double centerline)
		{
			this.roles.add(role);
			this.centerline = centerline;
		}
		
		Row(Set<Role> roles, double centerline)
		{
			this.roles.addAll(roles);
			this.centerline = centerline;
		}
		
		Row() {}
		
		List<Role> getRoles() { return roles; }
		
		void addRole(Role role) { roles.add(role); }
		
		void promoteRoleToPrimary(Role role)
		{
			if (! roles.remove(role)) throw new RuntimeException("Role not found");
			roles.add(0, role);
		}
		
		double getCenterline() { return centerline; }
		
		void setCenterline(double c) { this.centerline = c; }
		
		boolean isPrimaryRole(Role role)
		{
			if ((roles.size() > 0) && (roles.get(0) == role)) return true;
			return false;
		}
		
		
		/** Set the Y location of the Node so that its centerline is aligned 
			(i.e., congruent) with this Row's centerline. */
		
		void align(PersistentNode node)
		{
			node.setY(centerline - node.getHeight()/2);
		}
	}
}

