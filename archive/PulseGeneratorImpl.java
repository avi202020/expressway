/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.common.ModelElement.*;

import java.util.Set;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
//import java.io.ObjectStreamField;
import java.io.Serializable;


public class PulseGeneratorImpl extends ActivityImpl implements PulseGenerator
{
	/** Ownership. */
	public Port inputPort = null;

	/** Ownership. */
	public Port outputPort = null;
	
	/** Ownership. */
	public Port countPort = null;
	
	/** Ownership. */
	public Generator embeddedGenerator = null;
	
	/** Ownership. */
	CompensatorImpl compensator = null;
	
	/** Ownership. */
	Max max = null;


	public transient String inputPortNodeId = null;
	public transient String outputPortNodeId = null;
	public transient String countPortNodeId = null;
	public transient String embeddedGeneratorNodeId = null;
	public transient String compensatorNodeId = null;
	
	public String getInputPortNodeId() { return inputPortNodeId; }
	public String getOutputPortNodeId() { return outputPortNodeId; }
	public String getCountPortNodeId() { return countPortNodeId; }
	public String getEmbeddedGeneratorNodeId() { return embeddedGeneratorNodeId; }
	public String getCompensatorNodeId() { return compensatorNodeId; }
	public String getTimeShapeAttrNodeId() { return embeddedGenerator.getTimeShapeAttrNodeId(); }

	public String getStateNodeId() { return null; }
	public String getCountStateNodeId() { return null; }


	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		if ((child == inputPort) || (child == outputPort) || (child == countPort)
				|| (child == embeddedGenerator) || (child == compensator)
				|| (child == max))
			child.setNameAndNotify(newName);
		else
			super.renameChild(child, newName);
	}


	public NodeSer externalize() throws ParameterError
	{
		super.externalize();
		
		inputPortNodeId = getNodeIdOrNull(inputPort);
		outputPortNodeId = getNodeIdOrNull(outputPort);
		countPortNodeId = getNodeIdOrNull(countPort);
		embeddedGeneratorNodeId = getNodeIdOrNull(embeddedGenerator);
		compensatorNodeId = getNodeIdOrNull(compensator);

		return new PulseGeneratorSer(this);
	}


	public PersistentNode update(NodeSer nodeSer)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		PulseGeneratorSer ser = 
			(PulseGeneratorSer)checkInstanceCompatibility(nodeSer, PulseGeneratorSer.class);
			
		
		// Verify object references.
		// TBD.
		
		
		return super.update(nodeSer);
	}
	
	private static final String[] MaxInputPortNames = { "comp_port", "gen_port" };
	

	/**
	 * Constructor.
	 */

	public PulseGeneratorImpl(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale, double valueDistShape, 
		double valueDistScale, boolean ignoreStartup)
	{
		super(name, parent, domain);
		
		Generator generator = new GeneratorImpl(name, this, this.domain,
			timeDistShape, timeDistScale, valueDistShape, valueDistScale,
			ignoreStartup);
			
		try { generator.setNativeImplementationClass(GeneratorNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		this.embeddedGenerator = generator;

		try
		{
			inputPort = super.createPort("input_port", PortDirectionType.input, Position.left);
			outputPort = super.createPort("output_port", PortDirectionType.output, Position.right);
			countPort = super.createPort("count_port", PortDirectionType.output, Position.bottom);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		

		compensator = new CompensatorImpl("Compensator", this, this.getModelDomain(), 0);
		
		try { compensator.setNativeImplementationClass(
			expressway.CompensatorNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		
		max = new MaxImpl("Max", this, this.getModelDomain(), 0, MaxInputPortNames);
		
		try { max.setNativeImplementationClass(expressway.AbstractMaxNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		Port maxInputPortComp;
		Port maxInputPortGen;
		try {
			maxInputPortComp = max.getPort(MaxInputPortNames[0]);
			maxInputPortGen = max.getPort(MaxInputPortNames[1]);
		}
		catch (ElementNotFound enf) { throw new RuntimeException(
			"Error within PulseGeneratorImpl: cannot find pre-defined Port"); }
		
		//try { addComponent(compensator); }
		//catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		
		// Connect things up.
		try
		{
			createConduit("Start", inputPort, embeddedGenerator.getInputPort());
			createConduit("Count", embeddedGenerator.getCountPort(), countPort);
			createConduit("GenToComp", embeddedGenerator.getOutputPort(), compensator.getInputPort());
			createConduit("GenToMax", embeddedGenerator.getOutputPort(), maxInputPortGen);
			createConduit("CompToMax", compensator.getOutputPort(), maxInputPortComp);
			createConduit("Output", max.getOutputPort(), outputPort);
		}
		catch (ParameterError pe)
		{
			throw new RuntimeException(pe);
		}
	}
	
	
	private static double PreferredWidthInPixels = 50.0;
	private static double PreferredHeightInPixels = 100.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	public void setSizeToStandard()
	{
		super.setSizeToStandard();
		
		//embeddedGenerator.setSizeToStandard();
		//compensator.setSizeToStandard();
		
		inputPort.setSide(Position.left);
		outputPort.setSide(Position.right);
		countPort.setSide(Position.bottom);
		
		//embeddedGenerator.setX(this.getWidth()/2.0 - embeddedGenerator.getWidth()/2.0);
		//embeddedGenerator.setY(2.0*this.getHeight()/4.0 - embeddedGenerator.getHeight()/2.0);
		
		//compensator.setX(this.getWidth()/2.0 - compensator.getWidth()/2.0);
		//compensator.setY(this.getHeight()/4.0 - compensator.getHeight()/2.0);
	}
	
	
	/*public PulseGeneratorImpl(String name, ModelContainer parent, Generator embeddedGenerator)
	{
		super(name, parent, embeddedGenerator.getModelDomain());

		this.embeddedGenerator = embeddedGenerator;
		
		try { addComponent(embeddedGenerator); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		try
		{
			inputPort = super.createPort("input_port");
			outputPort = super.createPort("output_port");
			countPort = super.createPort("count_port");
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		

		CompensatorImpl compensator = new CompensatorImpl("Compensator",
			this, this.getModelDomain(), 0);
			
		try { compensator.setNativeImplementationClass(
			expressway.CompensatorNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		//try { addComponent(compensator); }
		//catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		
		// Connect things up.
		try
		{
			createConduit("Start", inputPort, embeddedGenerator.getInputPort());
			createConduit("Output", embeddedGenerator.getOutputPort(), outputPort);
			createConduit("Count", embeddedGenerator.getCountPort(), countPort);
			
			createConduit("GenToComp", embeddedGenerator.getOutputPort(),
				compensator.getInputPort());
			createConduit("CompToOut", compensator.getOutputPort(), outputPort);
		}
		catch (ParameterError pe)
		{
			throw new RuntimeException(pe);
		}
	}*/


	//public DistributionFactory getTimeDistFactory() { return embeddedGenerator.getTimeDistFactory(); }

	//public DistributionFactory getValueDistFactory() { return embeddedGenerator.getValueDistFactory(); }

	//public double getTimeDistShape() { return embeddedGenerator.getTimeDistShape(); }

	public double getTimeDistScale() { return embeddedGenerator.getTimeDistScale(); }

	public double getValueDistShape() { return embeddedGenerator.getValueDistShape(); }

	public double getValueDistScale() { return embeddedGenerator.getValueDistScale(); }

	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }

	public Port getCountPort() { return countPort; }
	
	public boolean getIgnoreStartup() { return embeddedGenerator.getIgnoreStartup(); }

	public Generator getEmbeddedGenerator() { return embeddedGenerator; }
	
	public Attribute getTimeShapeAttr() { return embeddedGenerator.getTimeShapeAttr(); }
	
	public void dump(int indentation)
	{
		super.dump(indentation);
		
		inputPort.dump(indentation);
		outputPort.dump(indentation);
		countPort.dump(indentation);
	}
}

