package expressway.ser;

import expressway.common.*;
import java.io.Serializable;


public class RequirementAttributeSer extends RequirementElementSer
	implements HierarchyAttributeSer, AttributeSer
{
	public Serializable value;
	
	public Serializable defaultValue;
	
	public String getNodeKind() { return NodeKindNames.RequirementAttribute; }

	public Serializable getDefaultValue() { return defaultValue; }

	public void setDefaultValue(Serializable dv) { this.defaultValue = dv; }
	
	public Serializable getValue() { return value; }
	
	public void setValue(Serializable value) { this.value = value; }
}

