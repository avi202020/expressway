/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.NamedReference.CrossReferenceable;
import expressway.server.HierarchyElement.*;
import java.util.List;
import java.io.Serializable;


/**
 * Nodes that define a hierarchical or requirements.
 */
 
public interface RequirementElement extends HierarchyElement
{
	interface RequirementDomain extends Requirement, HierarchyDomain
	{
	}
	
	
	interface RequirementAttribute extends RequirementElement, HierarchyAttribute
	{
	}
	
	
	interface RequirementDomainMotifDef extends RequirementDomain, HierarchyDomainMotifDef
	{
	}
	
	
	interface RequirementTemplate extends RequirementElement, HierarchyTemplate
	{
	}


	interface Requirement extends RequirementElement, Hierarchy
	{
		/**
		 * Set the position of this Requirement among its parent's sub-Requirements.
		 * Position 0 is the first position. There may be no gaps: that is, if the
		 * last element of the list is at position 10, one may not add an element
		 * at position 12.
		 */
		 
		void changePosition(int pos)
		throws
			ParameterError;
		
		
		int getPosition()
		throws
			ParameterError;
		
		
		int getNoOfRequirements();
		
		
		Requirement createSubRequirement(String name, int position, PersistentNode layoutBound,
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		/** Create a new requirement at the end of this Requirement's List of
			Requirements. */
			
		Requirement createSubRequirement(String name, int position, 
			Scenario scenario, PersistentListNode copiedNode)
		throws
			ParameterError;
		
		
		Requirement createSubRequirement(String name, int position, 
			Scenario scenario, RequirementTemplate template)
		throws
			ParameterError;
		
		
		Requirement createSubRequirement(String name, int position)
		throws
			ParameterError;
			
			
		Requirement createSubRequirement(String name)
		throws
			ParameterError;
		
		
		/*Requirement createSubRequirement(String name, Template template)
		throws
			ParameterError;*/
		
		
		Requirement getParentRequirement();
		
		
		/**
		 * Change the owning Requirement for this Requirement. Note that the outermost
		 * Requirement is always a RequirementDomain.
		 */
		 
		void setParentRequirement(Requirement parent)
		throws
			ParameterError;
		
		
		List<Requirement> getSubRequirements();
		
		
		Requirement getSubRequirement(String name)
		throws
			ElementNotFound;
		
		
		String getHTMLSummary();
		
		
		void setHTMLSummary(String summary);
	}
	
	
	RequirementDomain getRequirementDomain();
	
	
	RequirementAttribute createRequirementAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	RequirementAttribute createRequirementAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;


	RequirementAttribute createRequirementAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
}

