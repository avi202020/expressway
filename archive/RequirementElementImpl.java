/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.RequirementElement.*;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import java.util.TreeSet;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;


public class RequirementElementImpl extends HierarchyElementBase implements RequirementElement
{
  /* Constructors */
  
  
	/**
	 * Used to instantiate new Elements
	 * that will then be attributed by the user. This constructor must provide
	 * reasonable values for any required fields. Parent may not be null.
	 */
	 
	protected RequirementElementImpl(String baseName, RequirementElement parent)
	{
		super(parent);
		this.domain = (RequirementDomain)(parent.getDomain());
		this.name = ((RequirementElementImpl)parent).createUniqueChildName(baseName);
	}


  /* From HierarchyElementBase */
	
	
	protected HierarchyAttributeBase constructAttribute(String name,
		Serializable defaultValue)
	{
		return new RequirementAttributeImpl(name, this, defaultValue);
	}
	
	
	protected HierarchyAttributeBase constructAttribute(String name)
	{
		return new RequirementAttributeImpl(name, this);
	}
	
	
	protected void writeSpecializedXMLAttributes(PrintWriter writer)
	{
	}
	
	
	protected void writeSpecializedElements(PrintWriter writer)
	{
	}
	
	
	protected String getTextToRender()
	{
		return super.getTextToRender();
	}
	
	
  /* From PersistentNodeImpl */
	
	
	protected Class getSerClass() { return RequirementElementSer.class; }
	
	
	protected NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (this.getDomain())
		{
			RequirementElementSer ser = (RequirementElementSer)nodeSer;
			return super.externalize(ser);
		}
	}
	
	
  /* From PersistentNode */
	
	
	public Object clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		RequirementElementImpl newInstance =
			(RequirementElementImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}

	
	public void prepareForDeletion()
	{
		super.prepareForDeletion();

		// Call <type>Deleted(this) on each DeletionListener.
		// (There are none.)
		
		
		// For each owned Node, call prepareForDeletion() and then remove it.
		// (There are none.)
		
		
		// Remove other non-Node owned Objects.
		// (There are none.)
	}
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		return super.getChild(name);
	}
	

	public SortedSet<PersistentNode> getChildren()
	{
		return super.getChildren();
	}


	public int getNoOfHeaderRows() { return super.getNoOfHeaderRows(); }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes() { return super.getPredefinedNodes(); }
	
	
	public double getPreferredWidthInPixels() { return super.getPreferredWidthInPixels(); }
	

	public double getPreferredHeightInPixels() { return super.getPreferredHeightInPixels(); }
	
	
	protected void setIconImageToDefault() throws IOException
	{
		super.setIconImageToDefault();
	}


  /* From RequirementElement */
	
	
	RequirementAttribute createRequirementAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (RequirementAttribute)(createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef));
	}
	
	
	RequirementAttribute createRequirementAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	{
		return (RequirementAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}


	RequirementAttribute createRequirementAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	{
		return (RequirementAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
}

