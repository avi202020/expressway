/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.NamedReference.CrossReferenceable;
import expressway.server.HierarchyElement.*;
import java.util.List;
import java.io.Serializable;


/**
 * Nodes that define a hierarchical or strategys.
 */
 
public interface StrategyElement extends HierarchyElement
{
	interface StrategyDomain extends Strategy, HierarchyDomain
	{
	}
	
	
	interface StrategyAttribute extends StrategyElement, HierarchyAttribute
	{
	}
	
	
	interface StrategyDomainMotifDef extends StrategyDomain, HierarchyDomainMotifDef
	{
	}
	
	
	interface StrategyTemplate extends StrategyElement, HierarchyTemplate
	{
	}


	interface Strategy extends StrategyElement, Hierarchy
	{
		/**
		 * Set the position of this Strategy among its parent's sub-Strategys.
		 * Position 0 is the first position. There may be no gaps: that is, if the
		 * last element of the list is at position 10, one may not add an element
		 * at position 12.
		 */
		 
		void changePosition(int pos)
		throws
			ParameterError;
		
		
		int getPosition()
		throws
			ParameterError;
		
		
		int getNoOfStrategys();
		
		
		Strategy createSubStrategy(String name, int position, PersistentNode layoutBound,
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		/** Create a new strategy at the end of this Strategy's List of
			Strategys. */
			
		Strategy createSubStrategy(String name, int position, 
			Scenario scenario, PersistentListNode copiedNode)
		throws
			ParameterError;
		
		
		Strategy createSubStrategy(String name, int position, 
			Scenario scenario, StrategyTemplate template)
		throws
			ParameterError;
		
		
		Strategy createSubStrategy(String name, int position)
		throws
			ParameterError;
			
			
		Strategy createSubStrategy(String name)
		throws
			ParameterError;
		
		
		/*Strategy createSubStrategy(String name, Template template)
		throws
			ParameterError;*/
		
		
		Strategy getParentStrategy();
		
		
		/**
		 * Change the owning Strategy for this Strategy. Note that the outermost
		 * Strategy is always a StrategyDomain.
		 */
		 
		void setParentStrategy(Strategy parent)
		throws
			ParameterError;
		
		
		List<Strategy> getSubStrategys();
		
		
		Strategy getSubStrategy(String name)
		throws
			ElementNotFound;
		
		
		String getHTMLSummary();
		
		
		void setHTMLSummary(String summary);
	}
	
	
	StrategyDomain getStrategyDomain();
	
	
	StrategyAttribute createStrategyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	StrategyAttribute createStrategyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;


	StrategyAttribute createStrategyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
}

