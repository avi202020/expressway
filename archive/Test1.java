package test;

import expressway.Domain;
import expressway.ModelElement.*;
import expressway.ModelDomainImpl;
import expressway.ModelEngine;
import expressway.ModelEnginePojoImpl;

public class Test1
{
	/**
	 * Test the ModelEngine Pojo implementation.
	 *
	 * Instantiate a ModelEngine, and create a set of models.
	 * Then exercise those models.
	 */

	static ModelEngine modelEngine = null;

	public static void main(String[] args)
	{
		try
		{
			// Instantiate ModelEngine.

			modelEngine = new ModelEnginePojoImpl();


			/*
			// ----------------------------------------------------------------
			// Create a design model.
			//

			System.out.println("Instantiating a Domain...");
			Domain d1 = new DomainImpl();
			modelEngine.addChildElement(null, d1, "d1");

			System.out.println("Instantiating an Activity in the Domain...");
			Activity a11 = new ActivityImpl();
			modelEngine.addChildElement(d1, a11, "a11");

			System.out.println("Adding a Port to the Activity...");
			Port p111 = new PortImpl();
			modelEngine.addChildElement(a11, p111, "p111");

			System.out.println("Instantiating another Activity in the Domain...");
			Activity a12 = new ActivityImpl();
			modelEngine.addChildElement(d1, a12, "a12");

			System.out.println("Adding a Port to the second Activity...");
			Port p121 = new PortImpl();
			modelEngine.addChildElement(a12, p121, "p121");

			System.out.println("Adding a Conduit to the Domain...");
			Conduit c11 = new ConduitImpl();
			modelEngine.addChildElement(d1, c11, "c11");

			System.out.println("Connecting the Port of each Activity via the Conduit...");
			c11.connect(p111, p121);


			// Print out the design structure.

			ModelElementImpl.dump(d1);


			// ----------------------------------------------------------------
			// Test the getModelElementTree(...) method.
			//

			ModelElement e1 = modelEngine.getModelElementTree("d1");
			if (e1 != d1) System.out.println("Error: getModelElementTree did not find d1");


			// ----------------------------------------------------------------
			// Test the updateAttribute(...) method.
			//

			modelEngine.updateAttribute("d1", "Attribute 1", new Integer(10));
			*/


			// ----------------------------------------------------------------
			// Create a performance model that contains at least one arbitrary
			// attribute.
			//

			// Instantiate a Domain to contain the model.
			Domain d2 = new ModelDomainImpl();
			modelEngine.addChildElement(null, d2, "Performance");

			// Instantiate a value-creation Function.


			// Instantiate an Activity to represent an input source.


			// Instantiate an Activity to represent an output sink.


			// Connect the input source to the Function via a Conduit.


			// Connect the Function to the output sink via a Conduit.


			// Add a Double Attribute called "NUsers" to the input source Activity.


			// Add a Double attribute called "Throughput" to the output sink.



			// ----------------------------------------------------------------
			// Create a design model that contains two components, each having an
			// attribute representing the number of instances of that component.
			//

			// Instantiate a Domain to contain the design model.
			Domain d3 = new DomainImpl();
			modelEngine.addChildElement(null, d3, "Design");

			// Instantiate Activity A.

			// Add an isPresent attribte to Activity A.


			// Instantiate Activity B.

			// Add an isPresent attribute to Activity B.


			// ----------------------------------------------------------------
			// Create a decision tree that depends on the business model's arbitrary
			// attribute.
			//

			// Instantiate a Domain to contain the decision model.
			Domain d4 = new DomainImpl();
			modelEngine.addChildElement(null, d4, "Design Decisions");

			// Instantiate a DecisionPoint A to represent the decision about the
			// value of Activity A isPresent.

			// Instantiate a DecisionPoint B to represent the decision about the
			// value of Activity B isPresent.

			// Add an input Variable to DecisionPoint A.

			// Connect the input Variable to the sink Activity's Throughput Attribute.

			// Add an output Variable to DecisionPoint A.

			// Connect the output Variable to Activity A's isPresent Attribute.

			// Insert a Precludes Dependency between two decision points that each
			// impact a structural model parameter.

			//


			// ----------------------------------------------------------------
			// Evaluate the decision domain.
			//

			// Update the NUsers attribute.


			// Verify that the expected Decision node resulted.


			// Veryfy that the design attributes have assumed the expected values.




		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
