import org.junit.*;
import static org.junit.Assert.*;


		public SimulationRunImpl()
		{
			super("Test Simulation Run", test_modelDomain);
		}


		SortedSet<Event> test_futureEvents = null;
		ModelDomain test_modelDomain = null;


		@Before
		public static void setUp()
		{
			test_modelDomain = new ModelDomainImpl("Test Model Domain");
			test_futureEvents = new TreeSet<Event>();
			ModelContainer modelContainer = test_modelDomain;
			EventProducer eventProducer = new ActivityImpl("Test Activity",
				modelContainer, test_modelDomain);
			State state = new StateImpl("Test State", eventProducer);
			Date eventTime = null;
			Object newValue = null;
			Event event = new PredefinedEventImpl(state, time, newValue);
			test_futureEvents.add(event);
		}


		@After
		public static void tearDown()
		{
			test_futureEvents = null;
		}


		@Test
		public void test_getCurrentEvents()
		{
			Date priorEpoch = null;
			Date finalEpoch = null;
			Set<Event> events = getCurrentEvents(priorEpoch,
				test_futureEvents, finalEpoch);
			assertTrue("no current events", (!event.isEmpty()));
		}


