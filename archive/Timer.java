/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;


/**
 * A general purpose timer.
 */
 
class Timer extends Thread
{
	private boolean stopped = false;
	private boolean stopForever = false;
	private Thread threadToInterrupt;
	private long ms = 0;
	
	
	public Timer() { start(); }
	
	
	public synchronized void run()
	{
		for (;;)
		{
			wait();  // wait for this timer to be started (again).
			
			if (stopForever) return;
			
			stopped = false;
			
			try
			{
				wait(this.ms);
			}
			catch (InterruptedException ex)
			{
				System.out.println("Timer interrupted");  // should not happen.
				return;
			}
			
			this.ms = 0;
			if (! stopped) this.threadToInterrupt.interrupt();
		}
	}
	
	
	/**
	 * Start this timer. After the specified number of milliseconds, it will
	 * interrupt its thread, unless it has been stopped first.
	 */
	 
	public synchronized void startTimer(long ms)
	{
		if (! isAlive()) throw new RuntimeException("Timer thread is not alive");
		
		stopped = false;
		threadToInterrupt = Thread.currentThread();
		this.ms = ms;
		notify();  // wake up from a wait state.
	}
	
	
	/**
	 * Stop this timer.
	 */
	 
	public synchronized void stopTimer()
	{
		stopped = true;
		notify();
	}
	
	
	/**
	 * Permanently stops this timer. After calling this, this timer instance cannot
	 * be restarted.
	 */
	 
	public synchronized void closeTimer()
	{
		stopForever = true;
		notify();
	}
}

