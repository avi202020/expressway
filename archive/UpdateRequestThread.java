/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;

import expressway.ModelElement.*;
import expressway.DecisionElement.*;
import expressway.ModelAPITypes.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;


/**
 * Main service thread for handling a client request.
 
 ....This all needs to be re-visited. It will not work with the new thread
 model.
 */

public class UpdateRequestThread extends ServiceThreadImpl
	implements DecisionCallback, SimCallback
{
	private SimControl simControl = null; // assumes one thread per simulation.
	private SimulationRun simRun = null;
	
	private ModelScenario modelScenario = null;
	private boolean propagate = false;
	private DecisionScenario originatingDecisionScenario = null;
	private Date finalEpoch = null;
	private int iterationLimit = 0;
	
	private boolean proceed = false;
	private boolean updateDecisions = false;
	private Set<Decision> newDecisions = new TreeSet<Decision>();
	
	
	UpdateRequestThread(ModelScenario modelScenario, 
		boolean propagate,
		DecisionScenario originatingDecisionScenario, 
			// the originating DecisionScenario; may be null.
		Date finalEpoch, int iterationLimit)
	throws
		ParameterError // If two Scenarios represent the same Domain.
	{
		super();  // establishes the ServiceContext.
		
		if (modelScenario == null) throw new RuntimeException();

		this.modelScenario = modelScenario;
		this.propagate = propagate;
		this.originatingDecisionScenario = originatingDecisionScenario;
		this.finalEpoch = finalEpoch;
		this.iterationLimit = iterationLimit;
		
		if (modelScenario != null) getServiceContext().addScenario(modelScenario);
		if (originatingDecisionScenario != null)
			getServiceContext().addScenario(originatingDecisionScenario);
	}


	/**
	 * The main method for this Thread. This is called after the Thread
	 * is started. This method implements the Cross-Model interaction design
	 * as specified in the Product Architecture.
	 */

	public void go()
	{
		if (isAborted()) { abortThread(); return; }

		//System.out.println(modelScenario.getName() + ": go");
		
		
		/*
		 * Simulate to determine the impact of decision changes.
		 */

		/*
		try
		{
			showMessage(
				"Simulating model Scenario " + modelScenario.getName() + "...");

			...simControl = modelScenario.getModelDomain().simulate(
			....	modelScenario, this, finalEpoch, iterationLimit);

			showMessage("UpdateRequestThread.go: simulation completed.");
		}
		catch (ParameterError ex)
		{
			showMessage(ex.getMessage());
			abortThread();
			return;
		}
		*/

		if (isAborted()) { abortThread(); return; }


		/*
		 * Identify any Decision Variables that might have changed as a
		 * result of simulation.
		 */

		Set<Variable> variables = new TreeSet<Variable>();
		//Set<DecisionDomain> affectedDecisionDomains = new TreeSet<DecisionDomain>();

		for (ModelElement.State state : simRun.getFinalStateValues().keySet())
			// each State that changed as a result of simulation)
		{
			//System.out.println(
			//	"UpdateRequestThrad.go: AttributeStateBindings for State " +
			//	state.getName() + ":");

			for (AttributeStateBinding binding : state.getAttributeBindings())
			{
				Attribute attribute = binding.getAttribute();

				// Identify all Variables that are linked to the changed
				// attribute.

				VariableAttributeBinding vaBinding =
					attribute.getParameterBinding();

				Variable variable = vaBinding.getVariable();

				//System.out.print(", Variable: " + variable.getName());
				
				Object stateVal = null;
				try {
					stateVal = modelScenario.getStateFinalValue(state);
				} catch (ParameterError pe) {
					stateVal="<parameter error>";
				}
				//System.out.print(", final state value=" + 
				//	(stateVal == null ? "null" : stateVal.toString()));
				
				ModelDomain attrDomain = attribute.getModelDomain();
				ServiceContext context = ServiceContext.getServiceContext();
				ModelScenario attrScenario = 
					(ModelScenario)(context.getScenario(attrDomain));
				if (attrScenario == null) throw new RuntimeException(
					"No Scenario found for Attribute " + attribute.getName() + 
					"'s Domain, though the " +
					"Attribute's value was set during this simulation.");
				
				Object attrVal = null;
				try {
					attrVal = attrScenario.getAttributeValue(attribute);
				} catch (ParameterError pe) {
					attrVal = "<parameter error>";
				}
				//System.out.println(", attr value=" +
				//	(attrVal == null? "null" : attrVal.toString()));

				variables.add(variable);
				//affectedDecisionDomains.add(variable.getDecisionDomain());
			}
		}


		/*
		 * Check if propagation has been requested.
		 */
		
		if (! propagate)
			// Do not propagate.
		{
			completed();
			return;
		}
		
		
		/*
		 * Check if re-evaluating the originating DecisionDomain might cause
		 * the re-evaluation of any DecisionPoints during this run.
		 * (This is Propagation Rule 2: see "Cross-Model Interaction" in
		 * the Product Architecture.
		 */
		
		Set<DecisionPoint> affected = null;
		try
		{
			affected = 
				originatingDecisionScenario.getDecisionDomain().
					getAffectedDecisionPoints(variables, true);
					
			//System.out.println();
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException(ex);  // should not happen.
		}
		
		// Determine the intersection between the Variables that have already
		// been impacted by the current request and those that might be
		// re-evaluated if changes are propagated.

		affected.retainAll(variables);
		
		if (affected.size() > 0)
		{
			// Potential re-evaluation. Return now to avoid possible race
			// condition.
			showMessage("Evaluation of Decision Domain will not be performed " +
				"to avoid potential race condition.");
			
			String s = "";
			for (DecisionPoint dp : affected) s += (dp.getName() + " ");
			showMessage(
				"These DecisionPoints would be re-evaluated: " + s);
			return;
		}

		
		/*
		 * Propagation:
		 * (Re-)evaluate any directly connected DecisionScenarios that have
		 * been impacted.
		 */

		List<Variable> remainingVariables = new Vector<Variable>(variables);
		
		for (;;)
		{
			if (remainingVariables.size() == 0) break;
			
			// Create a subset of Variables that all belong to the same
			// DecisionDomain, and remove these Variables from
			// the complete Set of affected Variables.
			
			Set<Variable> decisionVariables = new TreeSet();
			DecisionDomain decisionDomain = 
				remainingVariables.get(0).getDecisionDomain();
			decisionVariables.add(remainingVariables.get(0));
			remainingVariables.remove(0);

			for (Variable v : remainingVariables)
			{
				if (v.getDecisionDomain() == decisionDomain)
					decisionVariables.add(v);
			}
			
			
			// Now update the Decisions for the subset of affected Variables.
			
			Set<Decision> decisions = null;
			try
			{
				decisions = originatingDecisionScenario.getDecisionDomain().
					updateDecisions(
						originatingDecisionScenario, this, decisionVariables);
			}
			catch (ModelContainsError ex)
			{
				showMessage(ex.getMessage());
				abortThread();
			}
			catch (ParameterError pe)
			{
				pe.printStackTrace();
				abortThread();
				showMessage("Internal error");
			}
			catch (RuntimeException re)
			{
				showMessage(
					"RuntimeException in decision thread; aborting; " +
						re.getMessage());

				re.printStackTrace();
				abortThread();
				return;
			}
			finally
			{
				if (decisions != null) newDecisions.addAll(decisions);
			}

			if (isAborted()) { abortThread(); return; }


			/*
			 * Determine if any Decisions have changed.
			 */

			if (decisions.isEmpty())
			{
				showMessage("No decision changes for " + decisionDomain.getName());
			}
			else
			{
				showMessage("New decisions for these DecisionPoints:");
				for (Decision decision : decisions)
				{
					DecisionPoint dp = decision.getDecisionPoint();
					showMessage("\t" + dp.getName());

					Variable v = decision.getVariable();
					Object value = decision.getValue();
					showMessage("\t\t" + v.getName() + "=" +
						value.toString() +
						"(" + value.getClass().getName() + ")");
				}
			}
		}

		completed();
	}


	protected void abortThread()
	{
		super.abortThread();
		if (simRun != null) simRun.abort();
	}

	public synchronized boolean getConfirmationToProceed()
	{
		try
		{
			wait();
		}
		catch (InterruptedException ex)  // someone is trying to stop this thread.
		{
			proceed = false;
		}

		return proceed;
	}

	public void reportSimulationResults(SimulationRun simRun)
	{
		this.simRun = simRun;
	}

	public synchronized void confirmToProceed(boolean proceed)
	{
		this.proceed = proceed;
		notify();
	}


	////public synchronized ActivityPeer getPeer(Activity activity)	
	
	////public synchronized FunctionPeer getPeer(Function function)
	
	////public synchronized ConduitPeer getPeer(Conduit conduit)
	
	////public synchronized PortPeer getPeer(Port port)
	
	////public synchronized AttributePeer getPeer(Attribute attribute)


	public synchronized Set<Decision> getNewDecisions()
	{
		return newDecisions;
	}
}
