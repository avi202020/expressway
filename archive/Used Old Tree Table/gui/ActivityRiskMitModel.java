/*
 */

package expressway.gui;

import java.util.Date;
import treetable.*;


/**
 */
public class ActivityRiskMitModel extends DynamicTreeTableModel
{
    /** Names of the columns. */
    private static final String[] columnNames =
		{ "Name", "Std Evidence", "Criteria", "Act Evidence", "Comment" };

	
	/** Method names used to access the data to display. */
    private static final String[] methodNames =
		{ "getName", "getStdEvidence", "getCriteria","getActEvidence", "getComment" };
	
	
    /** Method names used to set the data. */
    private static final String[] setterMethodNames =
		{ "setName", "setStdEvidence", "setCriteria","setActEvidence", "setComment" };
	
	
    /** Classes presenting the data. */
    private static final Class[] classes =
		{ TreeTableModel.class, String.class, String.class, String.class, String.class };


    public ActivityRiskMitModel(ActivityTreeNode root)
	{
		super(root, columnNames, methodNames, setterMethodNames, classes);
    }

    /**
     * <code>isCellEditable</code> is invoked by the JTreeTable to determine
     * if a particular entry can be added. This is overridden to return true
     * for the first column, assuming the node isn't the root, as well as
     * returning two for the second column if the node is a BookmarkEntry.
     * For all other columns this returns false.
     */
    public boolean isCellEditable(Object node, int column)
	{
		switch (column)
		{
		case 0:
			// Allow editing of the name, as long as not the root.
			return (node != getRoot());
		case 1:
		case 2:
		case 3:
		case 4:
			// Allow editing
			return (node instanceof ActivityTreeNode);
		default:
			return false;
		}
    }
}

