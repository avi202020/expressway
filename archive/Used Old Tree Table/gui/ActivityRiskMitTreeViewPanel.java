/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.*;
import expressway.ModelAPITypes.*;
import expressway.ModelElement.*;
import expressway.DecisionElement.*;
import expressway.ClientModel.*;
import expressway.gui.*;
import expressway.ser.*;
import treetable.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.Container;
import java.awt.Color;
import java.rmi.server.UnicastRemoteObject;
import java.lang.reflect.Field;
import java.awt.Component;
import java.rmi.RemoteException;
import javax.swing.*;
import javax.swing.tree.*;
//import javax.swing.text.*;


/** ****************************************************************************
 * Implements a tabular View of an Activity, depicting the nested Activities as
 * a "tree table": the first column of the table contains a tree (one row per
 * tree node) that breaks down the hierarchy of Activities within this View's
 * root Activity; the other table columns identify "evidence" and "sufficiency
 * criteria" for the row's Activity. This type of View is used for planning
 * project risk mitigation activities.
 */
 
public class ActivityRiskMitTreeViewPanel extends JPanel implements View
{
  /* ***************************************************************************
	 * *************************************************************************
	 * Constants.
	 */
	
	public static final Color BackgroundColor = new Color(200, 200, 255);
	//public static final Color TableColor = new Color(255, 255, 200);
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Initiatlization permanent state.
	 */
	
	/** The tabbed pane that owns this JPanel. */
	private final JTabbedPane container;

	
	/** The root Activity Node representing a project that is composed of a Set
	 of Activities. It is expected that this Activity is a Transformation. */
	private final ActivitySer activity;
	
	
	/** The Model Engine that owns the Nodes that are represented by this View. */
	private final ModelEngine modelEngine;
	
	
	/** The Peer Listener that listens for server messages pertaining to the
	 Visual Components of this View. */
	private final PeerListener peerListener;
	

	/** Listens to the server for changes to the server-side components that are
	 represented in this View. */
	private final ListenerRegistrar listenerRegistrar;
	

	
  /* ***************************************************************************
	 * *************************************************************************
	 * Transient state:
	 * The root node of the tree that is depicted.
	 */
	
	/** The root node of the JTree that is depicted in this View, that represents
	 the server-side Nodes. */
    private ActivityTreeNode root = null;
	
	
	/** Parent node that new entries are added to. */
	private ActivityTreeNode parent;
	

	/** Contains the client-side data that is represented by the View. */
	private TreeTableModel treeTableModel = null;

	
	/** The Tree Table displayed within this JPanel. */
	private JTreeTable treeTable = null;
	
	
	/** The Set of all Visuals owned by this View. */
	private Set<ActivityTreeNode> visuals = new HashSet<ActivityTreeNode>();
	

	/** The x-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelXSize = 1.0;
	
	
	/** The y-dimension size of a display space pixel in the real-valued space
	 of this View. */
	private double pixelYSize = 1.0;
	
	
	/** Display space x coordinate of this JPanel's origin. */
	private int xOriginOffset = 0;
	
	
	/** Display space y coordinate of this JPanel's origin. Note that in the
	 display space, the origin is at the top left of the screen, and the positive
	 y direction is downward. */
	private int yOriginOffset = 0;
	
	 
	 
  /* ***************************************************************************
	 * *************************************************************************
	 * Constructors.
	 */
	 
	 
	/** ************************************************************************
     * Constructor.
     */
	 
    public ActivityRiskMitTreeViewPanel(JTabbedPane container, ActivitySer activity,
		ModelEngine modelEngine)
	throws
		ParameterError,
		IOException
	{
		this.container = container;
		this.activity = activity;
		this.modelEngine = modelEngine;
		
		setBackground(BackgroundColor);
		
		
		// A View owns a PeerListener for all of the Persistent Nodes that it depicts.
		
		this.peerListener = new PeerListenerImpl();
		this.listenerRegistrar = modelEngine.registerPeerListener(peerListener);
	}
	

	/** ************************************************************************
	 * Obtain from the server the top-level Set of PersistentNodes that comprise
	 * the Activity, and add each one as a Visual Component.
	 */
	 
	public void shallowPopulate()
	throws
		Exception
	{
 		// Create a TreeNode as the root.
		
		this.root = new ActivityTreeNode(this, activity);
		this.parent = root;
		

		// Create a Tree Table Model.
		
		this.treeTableModel = new ActivityRiskMitModel(root);

		
		// Create a Tree Table.
		
		this.treeTable = new JTreeTable(treeTableModel);
		
		//this.treeTable.setBackground(TableColor);
		add(this.treeTable);
		show();
		
		
		// Obtain and display the immediate children.
		
		String[] childNodeIds = modelEngine.getChildNodeIds(root.getNodeId());
		System.out.println("There are " + childNodeIds.length + " child nodes.");
		for (String childNodeId : childNodeIds)
		{
			System.out.println("Getting child node " + childNodeId);
			
			VisualComponent visual = addNode(childNodeId, null);
			System.out.println("...added child node " + childNodeId);
		}
		
		int numberOfRows = treeTable.getRowCount();
		System.out.println("Row count = " + numberOfRows);
		System.out.println("...tree leaf count is " + treeTable.getTree().getRowCount());
	}

	
	/** ************************************************************************
	 * Retreive the specified Node from the server and display it as a Visual in
	 * the Tree Table; and subscribe to events pertaining to the Node.
	 * Add to each Visual a mouse handler to allow the user to select the Visual
	 * and choose to perform actions on the Node.
	 */
	 
	public VisualComponent addNode(String nodeId, VisualComponent p)
	throws
		Exception
	{
		// Obtain the specified Node from the server.
		System.out.println("Getting child node " + nodeId);
		NodeSer child = modelEngine.getNode(nodeId);
		System.out.println("Child Node: " + child.getName());
		
		// Validate the Node returned by the server.
		if (! (child instanceof ActivitySer)) throw new Exception(
			"Server returned a " + child.getClass().getName() +
			", expected an ActivitySer.");

		// Create a new Visual Component to represent the child.
		ActivityTreeNode visualTreeNode = new ActivityTreeNode(this, (ActivitySer)child);
		
		
		// Add the new Visual to this View's list of Visuals that it owns.
		
		this.visuals.add(visualTreeNode);
		
		
		// Set fields in the tree node.
		
		AttributeSer transAttr = visualTreeNode.getAttribute("Transformation");
		
		if (transAttr == null)    // display error popup but continue.
		{
			JOptionPane.showMessageDialog(this,
				"Activity does not have a Transformation Attribute.",
				activity.getName(),
				JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			visualTreeNode.setActivityName(transAttr.getName());
			
			Object defValue = transAttr.defaultValue;
			
			if (defValue == null)
			{
				JOptionPane.showMessageDialog(this,
					"Transformation Attribute does not have a default value.",
					activity.getName(),
					JOptionPane.ERROR_MESSAGE);
			}
			else if (! (defValue instanceof Transformation))
			{
				JOptionPane.showMessageDialog(this,
					"Transformation Attribute default value does not implement " +
						Transformation.class.getName(),
					activity.getName(),
					JOptionPane.ERROR_MESSAGE);
			}
			else
			{				
				visualTreeNode.setStdEvidence(((Transformation)defValue).getStdEvidence());
				visualTreeNode.setCriteria(((Transformation)defValue).getCriteria());
				visualTreeNode.setActEvidence(((Transformation)defValue).getActEvidence());
				visualTreeNode.setComment(((Transformation)defValue).getComment());
			}
		}
		

		// Add the Visual to the Tree Table.

		if (p != null)
		{
			if (! (p instanceof ActivityTreeNode)) throw new RuntimeException(
				"Expected an ActivityTreeNode; p is " + p.getClass().getName());
			
			parent = (ActivityTreeNode)p;
		}

		parent.add(visualTreeNode);
		System.out.println("...added node to parent; leaf count is " + parent.getLeafCount());
		
		parent = visualTreeNode;
		
		
		// Subscribe to receive update notifications for the child.
		// Note that this does NOT result in additional server connections.

		listenerRegistrar.subscribe(nodeId);
		
		
		// Here we should add any mouse listeners required to allow the user to
		// manipulate the Visual or initiate actions based on the context of
		// the Visual.
		// ....
		
		
		return visualTreeNode;
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Public methods..
	 */
	 
	 
	/** ************************************************************************
	 * Accessor: return the root Tree Node.
	 */
	 
	public ActivityTreeNode getRoot() { return root; }


	
  /* ***************************************************************************
	 * *************************************************************************
	 * Methods defined in View interface.
	 */
	 
	 
	public ModelEngine getModelEngine()
	{
		return modelEngine;
	}
	
	
	public Set<VisualComponent> identifyVisualComponents(String nodeId)
	{	
		Set<VisualComponent> matchingVisuals = new TreeSet<VisualComponent>();
		
		for (ActivityTreeNode visual : visuals)
		{
			if (visual.getNodeId().equals(nodeId)) matchingVisuals.add(visual);
		}
		
		return matchingVisuals;
	}
	
	
	public String identifyNodeId(VisualComponent visual)
	{
		// Look through all of this Panel's Components. Find the child that matches
		// the specified Visual. There should not be more than one.
		
		for (ActivityTreeNode treeNode : visuals)
		{
			if (treeNode == visual) return treeNode.getNodeId();
		}
		
		return null;  // not found.
	}
	
	
	public int transformNodeXCoordToView(double modelXCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeXCoordToView(modelXCoord,
			pixelXSize, xOriginOffset);
	}
	
	
	public int transformNodeYCoordToView(double modelYCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeYCoordToView(modelYCoord,
			pixelYSize, yOriginOffset);
	}
	
	
	public int transformNodeWidthToView(double nodeWidth)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeWidthToView(nodeWidth,
			pixelXSize);
	}
		
		
	public int transformNodeHeightToView(double nodeHeight)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformNodeWidthToView(nodeHeight,
			pixelYSize);
	}


	public double transformViewXCoordToNode(int viewXCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewXCoordToNode(viewXCoord,
			pixelXSize, xOriginOffset);
	}
	
	
	public double transformViewYCoordToNode(int viewYCoord)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewYCoordToNode(viewYCoord,
			pixelYSize, yOriginOffset);
	}
		
		
	public double transformViewWidthToNode(int viewWidth)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewWidthToNode(viewWidth,
			pixelXSize);
	}
		
		
	public double transformViewHeightToNode(int viewHeight)
	throws
		ParameterError
	{
		return DefaultViewTransformationHelper.transformViewHeightToNode(viewHeight,
			pixelYSize);
	}



  /* ***************************************************************************
	 * *************************************************************************
	 * Inner classes.
	 */
	 
	 
	/** ************************************************************************
	 * The Peer Listener for this View. (See design slide "PeerListener Pattern".)
	 * Listens for notifcation messages from a ModelEngine, and relays those messages
	 * to the visual counterparts of the server components identified in the messages.
	 */
	 
	class PeerListenerImpl extends UnicastRemoteObject implements PeerListener
	{
		protected PeerListenerImpl() throws RemoteException { super(); }
		
		public void notify(PeerNotice notice)
		throws
			InconsistencyError,
			IOException
		{
			String nodeId = notice.getPeerId();
			
			Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
			
			for (VisualComponent visual : visuals) notice.notify(visual);
		}
	}
}

