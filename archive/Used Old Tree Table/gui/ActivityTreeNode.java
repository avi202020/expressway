/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.*;
import expressway.ModelAPITypes.*;
import expressway.ModelElement.*;
import expressway.DecisionElement.*;
import expressway.VisualComponent.ActivityVisual;
import expressway.gui.*;
import expressway.ser.ActivitySer;
import expressway.ser.AttributeSer;
import treetable.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.Color;
import javax.swing.*;
import javax.swing.tree.*;
//import javax.swing.text.*;


/** ************************************************************************
 * Class for the TreeNodes in the TreeTable.
 * These also implement ActivityVisual, in order to be able to receive and
 * handle updates from the server (dispatched to the ActivityVisual via the
 * containing View's Peer Listener).
 */

public class ActivityTreeNode extends DefaultMutableTreeNode implements ActivityVisual
{
  /* ***************************************************************************
	 * *************************************************************************
	 * Initiatlization permanent state.
	 */

	/** . */
	private final String nodeId;
	
	/** . */
	private final View view;
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Transient state:
	 */

	/** . */
	private ActivitySer activity;
	
	/** . */
	private String name;

	/** . */
	private String stdEvidence;

	/** . */
	private String criteria;

	/** . */
	private String actEvidence;

	/** . */
	private String comment;
	

	/** The time (in server-side time) when this Visual was last updated. */
	private long lastUpdated;
	
	/** Indicates whether this Visual has been determined (by the server) to
	 be inconsistent (with respect to the server). */
	private boolean inconsistent;
	
	/** Indicates whether this Visual contains state that has not been
	 propagated to the server. */
	private boolean unpropagated;
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Constructors.
	 */
	 
	
	public ActivityTreeNode(View view, ActivitySer activity)
	{
		this.view = view;
		
		this.activity = activity;
		
		this.nodeId = activity.getNodeId();
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Public accessors.
	 */
	 
	//public String getNodeId() { return nodeId; }
	
	
	public void setActivity(ActivitySer activity) { this.activity = activity; }
	
	public ActivitySer getActivity() { return activity; }
	
	public void setActivityName(String name) { this.name = name; }
	
	public String getActivityName() { return name; }
	
	public void setStdEvidence(String stdEvidence) { this.stdEvidence = stdEvidence; }
	
	public String getStdEvidence() { return stdEvidence; }
	
	public void setCriteria(String criteria) { this.criteria = criteria; }
	
	public String getCriteria() { return criteria; }
	
	public void setActEvidence(String actEvidence) { this.actEvidence = actEvidence; }
	
	public String getActEvidence() { return actEvidence; }
	
	public void setComment(String comment) { this.comment = comment; }
	
	public String getComment() { return comment; }
	
	public String toString() { return getName(); }


	
  /* ***************************************************************************
	 * *************************************************************************
	 * Protected accessors.
	 */
	
	protected void setLastUpdated(long newLastUpdated) { this.lastUpdated = newLastUpdated; }
	
	protected long getLastUpdated() { return lastUpdated; }
	
	protected void setInconsistent(boolean newInconsistent) { this.inconsistent = newInconsistent; }
	
	protected boolean isInconsistent() { return inconsistent; }
	
	protected void setUnpropagated(boolean newUnpropagated) { this.unpropagated = newUnpropagated; }
	
	protected boolean isUnpropagated() { return unpropagated; }
	
	

  /* ***************************************************************************
	 * *************************************************************************
	 * From VisualComponent:
	 */
	 
	 public String getNodeId() { return nodeId; }
	
	 public String getName() { return name; }
		 
	 public View getView() { return view; }



  /* ***************************************************************************
	 * *************************************************************************
	 * From ActivityVisual. These implementations are empty because the intent
	 * of this View is not to depict simulation-time changes.
	 */

	 
	public void showStart()
	{
	}
	
	
	public void showStop()
	{
	}
	
	
	public void showEnteredRespond()
	{
	}



  /* ***************************************************************************
	 * *************************************************************************
	 * From PortedContainerVisual. These implementations are empty because the
	 * intent of this View is not to depict simulation-time changes.
	 */
	 

	public void eventsReceivedInEpoch(Set<Event> events, int epochNo, Date date)
	{
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Non-public methods.
	 */
	 

	/** ************************************************************************
	 * Retrieve the specified Attribute from the Persistent Node depicted by
	 * this Activity Visual.
	 */
	 
	AttributeSer getAttribute(String attrName)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return getView().getModelEngine().getAttribute(this.getNodeId(), attrName);
	}
}

