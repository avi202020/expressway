/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.VisualComponent;
import expressway.VisualComponent.*;
import expressway.ModelEngine;
import expressway.ModelElement;
import expressway.ModelElement.*;
import expressway.DecisionElement.*;
import expressway.Display;
import expressway.ClientModel.*;
import expressway.VisualComponent;
import expressway.VisualComponent.*;
import expressway.NodeSer;
import expressway.NodeContainerSer;
import expressway.View;
import expressway.ModelAPITypes.*;
import expressway.ser.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.Set;
import java.util.TreeSet;
import java.util.Date;
import java.io.IOException;
import java.net.URL;


/** ****************************************************************************
 * Base class for all VisualComponents for Model Views.
 * VisualJComponent is also a factory for VisualComponents.
 */

public class VisualJComponent extends JPanel implements VisualComponent
{
  /* ***************************************************************************
	 ***************************************************************************
	 * Colors used by this class.
	 */
	 
	public static final Color DomainColor = new Color(255, 255, 255);
	public static final Color ScenarioColor = new Color(240, 240, 240);
	public static final Color ActivityColor = new Color(200, 200, 255);
	public static final Color FunctionColor = new Color(255, 200, 200);
	public static final Color ConduitColor = new Color(100, 100, 155);
	public static final Color PortColor = new Color(0, 0, 255);
	public static final Color StateColor = new Color(0, 255, 5);
	public static final Color VariableColor = new Color(255, 200, 255);
	public static final Color AttributeColor = new Color(200, 255, 255);

	
  /* ***************************************************************************
	 ***************************************************************************
	 * Display constants.
	 */
	 
	public static final int IconWidth = 20;
	public static final int IconHeight = 20;
	public static final int IconBorderFontPointSize = 8;
	
	public static final String DefaultImageName = "/DefaultImage.png";
	public static final String DomainIconImageName = "/DomainIconImage.png";
	public static final String ModelDomainIconImageName = "/ModelDomainIconImage.png";
	public static final String DecisionDomainIconImageName = "/DecisionDomainIconImage.png";
	public static final String ScenarioIconImageName = "/ScenarioIconImage.png";
	public static final String ModelScenarioIconImageName = "/ModelScenarioIconImage.png";
	public static final String DecisionScenarioIconImageName = "/DecisionScenarioIconImage.png";
	public static final String PortedContainerIconImageName = "/PortedContainerIconImage.png";
	public static final String ActivityIconImageName = "/ActivityIconImage.png";
	public static final String FunctionIconImageName = "/FunctionIconImage.png";
	public static final String ConduitIconImageName = "/ConduitIconImage.png";
	public static final String PortIconImageName = "/PortIconImage.png";
	public static final String StateIconImageName = "/StateIconImage.png";
	public static final String DecisionPointIconImageName = "/DecisionPointIconImage.png";
	public static final String VariableIconImageName = "/VariableIconImage.png";
	public static final String AttributeIconImageName = "/AttributeIconImage.png";

	
	/** Image displayed as a Visual's icon, unless over-ridden by a specialized
	 Visual type. The image size must match the size of the IconWidth and
	 IconHeight constants. */
	public static final Image DefaultIconImage = getImage(DefaultImageName);


  /* ***************************************************************************
	 ***************************************************************************
	 * Instance state.
	 */

	/** The View that owns this Visual. */
	private View view;
	
	/** Contains the data that identifies and describes the Visual as a Node
	 on the server. */
	private NodeSer nodeSer;
	
	/** If true, this Visual has been populated with its child Visuals, by
	 requesting the corresponding child NodeSers from the server. */
	private boolean populated = false;
	
	/** If true, the Visual is displayed as an icon, with no children. */
	private boolean asIcon;

	/** Border used when this Visual is full-size (i.e., not an icon). */
	private TitledBorder normalBorder;
	
	/** Border used when this Visual is displayed as an icon. */
	private TitledBorder iconBorder;
	
	/** Image used to depict the icon. */
	private Image iconImage;
	
	/** Size of this Visual when displayed full-size (i.e., when not an icon).
	 This size may be out of sync with the embedded NodeSer's size. The intended
	 protocol is that the user can change a Visual's size, and assume that when
	 this change is propagated to the server, the server will accept it; however,
	 if the server does not accept it, the server pushes a new resize operation
	 to the client to set the size back to its prior (or some other) value. */
	private Dimension visualNormalSize;

	/** The time (in server-side time) when this Visual was last updated. */
	private long lastUpdated;
	
	/** Indicates whether this Visual has been determined (by the server) to
	 be inconsistent (with respect to the server). */
	private boolean inconsistent;
	
	/** Indicates whether this Visual contains state that has not been
	 propagated to the server. */
	private boolean unpropagated;

	
  /* ***************************************************************************
	 ***************************************************************************
	 * Static methods.
	 */

	/** ************************************************************************
	 * Factory method for making VisualComponents. If populate is true, then
	 * retrieve child component Ids and display them as icons.
	 */
	 
	public static VisualJComponent makeVisual(NodeSer node, View view,
		Container container, boolean asIcon)
	throws
		ParameterError,  // if the component cannot be made due to an erroneous
						// or out-of-range parameter.
		Exception	// any other error.
	{
		System.out.println("Making Visual for " + node.getName() + "...");
		
		
		// Create a specialized instance of this class to Visually represent the
		// node.
		
		VisualJComponent visual = null;
		
		if (node instanceof ModelDomainSer)
			visual = new ModelDomainVisualJ(node, view);
		else if (node instanceof DecisionDomainSer)
			visual = new DecisionDomainVisualJ(node, view);
		else if (node instanceof ModelScenarioSer)
			visual = new ModelScenarioVisualJ(node, view);
		else if (node instanceof DecisionScenarioSer)
			visual = new DecisionScenarioVisualJ(node, view);
		else if (node instanceof ActivitySer)
			visual = new ActivityVisualJ(node, view);
		else if (node instanceof FunctionSer)
			visual = new FunctionVisualJ(node, view);
		else if (node instanceof ConduitSer)
			visual = new ConduitVisualJ(node, view);
		else if (node instanceof PortSer)
			visual = new PortVisualJ(node, view);
		else if (node instanceof StateSer)
			visual = new StateVisualJ(node, view);
		else if (node instanceof AttributeSer)
			visual = new AttributeVisualJ(node, view);
		else if (node instanceof VariableSer)
			visual = new VariableVisualJ(node, view);
		else throw new RuntimeException(
			"Unrecognized node kind: " + node.getClass().getName());
		
		visual.setView(view);
		
		
		// If requested, make an iconified VisualCompoment for each sub-component.
		// However, do not request each sub-component to be populated.
		
		//if (populate) visual.populateChildren();
		
		
		// Set display attributes and display.
		
		container.add(visual);
		
		visual.setAsIcon(asIcon);
		
		if (asIcon) visual.setBorder(visual.iconBorder);
		else visual.setBorder(visual.normalBorder);
		
		Dimension dimensions = new Dimension(view.transformNodeWidthToView(node.getWidth()),
			view.transformNodeHeightToView(node.getHeight()));
			
		visual.setSize(dimensions);
		
		visual.setLocation(view.transformNodeXCoordToView(visual.getNodeSer().getDisplay().getX()),
			view.transformNodeYCoordToView(visual.getNodeSer().getDisplay().getY()));
			
		visual.validate();
		visual.show();
		
		
		System.out.println("...done making Visual.");
		
		return visual;
	}
	
	
	/** ************************************************************************
	 * Utility method for retrieving an image specified by a resource name.
	 */
	static Image getImage(String imageResourceName)
	{
		URL resource = VisualJComponent.class.getResource(imageResourceName);
		if (resource == null) throw new RuntimeException("Resource for " +
			imageResourceName + " is null.");
		
		System.out.println("Getting resource " + resource.toString());
		
		return Toolkit.getDefaultToolkit().getImage(resource);
	}
	


  /* ***************************************************************************
	 ***************************************************************************
	 * Constructors.
	 */

	/** ************************************************************************
	 * Internal constructor. This is intended to be used only by the specialized
	 * classes. This is the only constructor.
	 */
	 
	protected VisualJComponent(NodeSer node, View view)
	throws
		ParameterError
	{
		this.nodeSer = node;
		this.view = view;
		this.asIcon = asIcon;
		
		visualNormalSize = new Dimension(
			view.transformNodeWidthToView(node.getWidth()),
			view.transformNodeHeightToView(node.getHeight()));
		setPreferredSize(visualNormalSize);
		setSize(visualNormalSize);
		
		setBackground(DomainColor);

		normalBorder = BorderFactory.createTitledBorder(node.getName());
		normalBorder.setTitleJustification(TitledBorder.CENTER);
		
		Font normalBorderFont = normalBorder.getTitleFont();
		Font iconBorderFont = normalBorderFont.deriveFont(IconBorderFontPointSize);
		iconBorder = BorderFactory.createTitledBorder(node.getName());
		//iconBorder = BorderFactory.createTitledBorder(iconBorder, node.getName(),
		//	TitledBorder.CENTER, TitledBorder.TOP, iconBorderFont);
			
		setIconImage(DefaultIconImage);
	}
	
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Defined in View interface.
	 */
	 
	public String getNodeId() { return nodeSer.getNodeId(); }
	
	public String getName() { return nodeSer.getName(); }
	
	public View getView() { return view; }
	

	
  /* ***************************************************************************
	 ***************************************************************************
	 * Public accessors.
	 */
	 
	public boolean getAsIcon() { return asIcon; }
	
	public NodeSer getNodeSer() { return nodeSer; }
	

	
  /* ***************************************************************************
	 ***************************************************************************
	 * Protected accessors.
	 */
	 
	protected void setView(View view) { this.view = view; }
	
	protected void setAsIcon(boolean asIcon) { this.asIcon = asIcon; }
	
	protected Image getIconImage() { return iconImage; }
	
	protected void setIconImage(Image img) { this.iconImage = img; }
	
	protected boolean isPopulated() { return populated; }
	
	protected Dimension getNormalSize() { return visualNormalSize; }
	
	protected void setLastUpdated(long newLastUpdated) { this.lastUpdated = newLastUpdated; }
	
	protected long getLastUpdated() { return lastUpdated; }
	
	protected void setInconsistent(boolean newInconsistent) { this.inconsistent = newInconsistent; }
	
	protected boolean isInconsistent() { return inconsistent; }
	
	protected void setUnpropagated(boolean newUnpropagated) { this.unpropagated = newUnpropagated; }
	
	protected boolean isUnpropagated() { return unpropagated; }
	
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Public methods.
	 */

	/** ************************************************************************
	 * Re-render this Visual as either an icon or as a container, depending on
	 * the value of asIcon.
	 */
	 
	public void iconify(boolean asIcon)
	throws
		ParameterError,
		Exception
	{
		if (asIcon)  // Re-display as an icon.
		{
			if (this.asIcon) return;  // already an icon.
			
			// Recursively make children invisible.
			Component[] components = getComponents();
			for (Component component : components) component.setVisible(false);
			
			// Set size to icon size.
			setSize(IconWidth, IconHeight);
			
			// Set border font size.
			setBorder(iconBorder);
		}
		else  // Re-display in expanded (non-icon) form.
		{
			if (! this.asIcon) return;  // already not an icon.
			
			// If child components have not been retrieved then retrieve them,
			// making them visible.
			if (! isPopulated()) populateChildren();
			
			// Re-size Visual to the NodeSer's size attributes.
			setSize(visualNormalSize);
			
			// Set border font size.
			setBorder(normalBorder);
		}
		
		this.asIcon = asIcon;
		repaint();
	}
	

	/** ************************************************************************
	 * Dispose of all resources owned by the Visual.
	 */
	 
	public synchronized void close()
	{
	}
	

	/** ************************************************************************
	 * Change the size of this Visual. The server is notified of the change.
	 * The server may reject the change immediately, in which case this Visual
	 * should not change its size. If the server later rejects the change, the
	 * server is expected to push a resize command to the client to cause this
	 * Visual to be returned to its prior (or some other) size.
	 */
	 
	public synchronized void changeSize(int newWidth, int newHeight)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		// Notify server.
		view.getModelEngine().changeNodeSize(getLastUpdated(), getNodeId(),
			view.transformViewWidthToNode(newWidth),
			view.transformViewHeightToNode(newHeight));

		// Change size of this Visual.
		this.visualNormalSize = new Dimension(newWidth, newHeight);
		
		// Re-display this Visual.
		repaint();
	}


  /* ***************************************************************************
	 ***************************************************************************
	 * Override painting methods inheritied from JComponent.
	 */

	protected void paintComponent(Graphics g)
	{
		// Display background image, if any; otherwise, background color.
		if (asIcon) g.drawImage(iconImage, 0, 0, new ImageObserver()
		{
			public boolean imageUpdate(Image img, int infoflags, int x, int y,
				int width, int height)
			{
				return (infoflags & ImageObserver.ALLBITS) != ImageObserver.ALLBITS;
			}
		});
		else super.paintComponent(g);
	}
	

  /* ***************************************************************************
	 ***************************************************************************
	 * Package-scope methods.
	 */

	/** ************************************************************************
	 * Add the specified child Node to this Visual.
	 */
	 
	VisualJComponent addChildNode(String nodeId)
	throws
		Exception	// any error.
	{
		return (VisualJComponent)(this.view.addNode(nodeId, this));
	}
	
	
	/** ************************************************************************
	 * Identify all VisualComponents displayed within this VisualJComponent
	 * that represent the specified node Id. This component is included (if it
	 * represents the specified node id).
	 */
	 
	Set<VisualComponent> identifyVisualComponents(String nodeId)
	{
		Set<VisualComponent> comps = new TreeSet<VisualComponent>();
		
		identifyVisualComponents(nodeId, comps);
		
		return comps;
	}
	
	
	/** ************************************************************************
	 * Recursively identfiy child elements that represent the specified node ID.
	 * This component is included if it also represents the node id.
	 */
	 
	void identifyVisualComponents(String nodeId, Set<VisualComponent> matchingComps)
	{
		if (this.getNodeId().equals(nodeId))
			matchingComps.add(this);
			
		Component[] comps = getComponents();
		
		for (Component comp : comps)
		{
			if (! (comp instanceof VisualJComponent)) continue;
			
			VisualJComponent vjComp = (VisualJComponent)comp;
			
			if (vjComp.getNodeId().equals(nodeId)) matchingComps.add(vjComp);
		}
	}
	
	
	/** ************************************************************************
	 * Recursively look for a the specified Visual, beginning with this Visual
	 * and traversing through its sub-components. Return the first one found, or
	 * null if the Visual is not found.
	 */
	 
	public String identifyNodeId(VisualComponent visual)
	{
		if (this == visual) return this.getNodeId();
		
		// Look through all of this Panel's Components. Find the child that matches
		// the specified Visual. There should not be more than one.
		
		Component[] components = getComponents();
		for (Component component : components)
			// each top-level visual component of this panel
		{
			if (! (component instanceof VisualJComponent)) continue;
			
			if (component == visual) return ((VisualJComponent)component).getNodeId();
			
			// Recursively check sub-components.
			
			String id = ((VisualJComponent)component).identifyNodeId(visual);
			if (id != null) return id;
		}
		
		return null;  // not found.
	}
	
	
	/** ************************************************************************
	 * Populate each immediate child component of this Visual.
	 */
	 
	void populateChildren()
	throws
		ParameterError,
		Exception
	{
		populated = true;
		
		NodeSer node = getNodeSer();
		
		if (node instanceof NodeContainerSer)
		{
			Set<String> childNodeIds = ((NodeContainerSer)node).getChildNodeIds();
			for (String childNodeId : childNodeIds)
			{
				addChildNode(childNodeId);
				//NodeSer child = view.getModelEngine().getNode(childNodeId);
				//VisualJComponent childVisual =
				//	makeVisual(child, view, this, false, true);
			}
		}
	}
	
	
	/** ************************************************************************
	 * Query the server to determine if the Node that is depicted by this Visual
	 * has an Attribute with the specified name.
	 */
	 
	boolean hasAttribute(String attrName)
	throws
		Exception
	{
		Object attr = null;
		attr = view.getModelEngine().getAttribute(nodeSer.getNodeId(), attrName);
		return (attr != null);
	}
}



/* *****************************************************************************
 * *****************************************************************************
 * The classes below are concrete implementations of the various VisualComponents.
 */


abstract class DomainVisualJ extends VisualJComponent implements DomainVisual
{
	public static final Image DomainIconImage = getImage(DomainIconImageName);
	
	DomainVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setBackground(DomainColor);
		setIconImage(DomainIconImage);
	}
}


class ModelDomainVisualJ extends DomainVisualJ implements DomainVisual
{
	public static final Image ModelDomainIconImage = getImage(ModelDomainIconImageName);
	
	ModelDomainVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(ModelDomainIconImage);
	}
	
	
	public void showScenarioCreated()
	{
		System.out.println("Scenario created");
	}
}


class DecisionDomainVisualJ extends DomainVisualJ implements DomainVisual
{
	public static final Image DecisionDomainIconImage = getImage(DecisionDomainIconImageName);
	
	DecisionDomainVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(DecisionDomainIconImage);
	}
	
	
	public void showScenarioCreated()
	{
		System.out.println("Scenario created");
	}
}


abstract class ScenarioVisualJ extends VisualJComponent implements ScenarioVisual
{
	public static final Image ScenarioIconImage = getImage(ScenarioIconImageName);
	
	ScenarioVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(ScenarioIconImage);
	}
}


class ModelScenarioVisualJ extends ScenarioVisualJ implements ModelScenarioVisual
{
	public static final Image ModelScenarioIconImage = getImage(ModelScenarioIconImageName);
	
	ModelScenarioVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(ModelScenarioIconImage);
		setBackground(ScenarioColor);
	}


	public void attributesInitialized() {}
	public void simulationRunCreated() {}
	public void attributesUpdated() {}
	public void stateInitEventsCreated() {}
	public void predefinedEventsRetrieved() {}
	public void epochStarted(int epochNo, Date date) {}
	public void epochFinished(int epochNo, Date date) {}
	public void allFunctionsEvaluatedForEpoch(int epochNo, Date date) {}
	public void allActivitiesActivatedForEpoch(int epochNo, Date date) {}
}


class DecisionScenarioVisualJ extends ScenarioVisualJ implements DecisionScenarioVisual
{
	public static final Image DecisionScenarioIconImage = getImage(DecisionScenarioIconImageName);
	
	DecisionScenarioVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(DecisionScenarioIconImage);
		setBackground(ScenarioColor);
	}


	public void choiceCreated() {}
	public void decisionsUpdated() {}
}


abstract class PortedContainerVisualJ extends VisualJComponent implements PortedContainerVisual
{
	public static final Image PortedContainerIconImage = getImage(PortedContainerIconImageName);
	
	PortedContainerVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(PortedContainerIconImage);
	}


	public void eventsReceivedInEpoch(Set<Event> events, int epochNo, Date date) {}
}


class ActivityVisualJ extends PortedContainerVisualJ implements ActivityVisual
{
	public static final Image ActivityIconImage = getImage(ActivityIconImageName);
	
	ActivityVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(ActivityIconImage);
		setBackground(ActivityColor);
	}


	public void showStart()
	{
		System.out.println("ActivityVisualJ: " + getName() + " started.");
	}
	
	
	public void showStop()
	{
		System.out.println("ActivityVisualJ: " + getName() + " stopped.");
	}
	
	
	public void showEnteredRespond()
	{
		System.out.println("ActivityVisualJ: " + getName() + " entered respond().");
	}	
}


class FunctionVisualJ extends PortedContainerVisualJ implements FunctionVisual
{
	public static final Image FunctionIconImage = getImage(FunctionIconImageName);
	
	FunctionVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(FunctionIconImage);
		setBackground(FunctionColor);
	}


	public void showStart()
	{
		System.out.println("FunctionVisualJ: " + getName() + " started.");
	}
	
	
	public void showStop()
	{
		System.out.println("FunctionVisualJ: " + getName() + " stopped.");
	}
}


class ConduitVisualJ extends VisualJComponent implements ConduitVisual
{
	public static final Image ConduitIconImage = getImage(ConduitIconImageName);
	
	ConduitVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(ConduitIconImage);
		setBackground(ConduitColor);
	}
}


class PortVisualJ extends VisualJComponent implements PortVisual
{
	public static final Image PortIconImage = getImage(PortIconImageName);
	
	PortVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(PortIconImage);
		setBackground(PortColor);
	}
}


class StateVisualJ extends VisualJComponent implements StateVisual
{
	public static final Image StateIconImage = getImage(StateIconImageName);
	
	StateVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(StateIconImage);
		setBackground(StateColor);
	}


	public void eventScheduled(Object futureValue, Date date) {}
	public void valueUpdatedInEpoch(int epochNo, Date date) {}
}


class DecisionPointVisualJ extends VisualJComponent implements DecisionPointVisual
{
	public static final Image DecisionPointIconImage = getImage(DecisionPointIconImageName);
	
	DecisionPointVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(DecisionPointIconImage);
		setBackground(VariableColor);
	}


	public void showStart()
	{
		System.out.println("DecisionPointVisualJ: " + getName() + " started.");
	}
	
	
	public void showStop()
	{
		System.out.println("DecisionPointVisualJ: " + getName() + " stopped.");
	}
}


class VariableVisualJ extends VisualJComponent implements VariableVisual
{
	public static final Image VariableIconImage = getImage(VariableIconImageName);
	
	VariableVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(VariableIconImage);
		setBackground(VariableColor);
	}
}


class AttributeVisualJ extends VisualJComponent implements AttributeVisual
{
	public static final Image AttributeIconImage = getImage(AttributeIconImageName);
	
	AttributeVisualJ(NodeSer node, View view)
	throws
		ParameterError
	{
		super(node, view);
		setIconImage(AttributeIconImage);
		setBackground(AttributeColor);
	}
}

