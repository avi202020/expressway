/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.*;
import java.io.*;
import java.util.*;


public class ValueHistoryArchiver
{
	static ValueHistoryArchiveWriter getValueHistoryArchiveWriter(final PrintWriter pw)
	{
		return new ValueHistoryArchiveWriter()
		{
			/**
			 * Syntax:
				'event_history'
					state-id  event ; ... ; event
					...
				'end event_history'
				
				Events are written using the encoded form defined by the method
				GeneratedEventSer.encodeAsString().
			 */
			 
			public void writeToArchive(ValueHistory vh)
			throws
				IOException
			{
				pw.println("event_history");
				
				Set<String> stateIds = history.keySet();
				for (String stateId : stateIds)
				{
					pw.print(stateId + " ");
					GeneratedEventSer[] eventSers = history.get(stateId);
					boolean firstTime = true;
					for (GeneratedEventSer eventSer : eventSers)
					{
						if (firstTime) firstTime = false;
						else pw.print(";");
						
						pw.print(GeneratedEventImpl.encodeAsString(eventSer));
					}
				}
				
				pw.println("end event_history");
			}
		};
	}


	static ValueHistoryArchiveReader getValueHistoryArchiveReader(final BufferedReader br)
	{
		return new ValueHistoryArchiveReader()
		{
			public ValueHistory readFromArchive()
			throws
				IOException
			{
				String line = br.readLine();
				if (! line.equals("event_history")) throw new IOException(
					"Unexpected first line: " + line);
				
				for (;;)
				{
					line = br.readLine();
					if (line == null) break; // done - no more lines.
					if (line.equals("end event_history")) break;  // done.
		
					String[] eventStrings = line.split(";");
					
					List<GeneratedEventSer> eventSerList = new Vector<GeneratedEventSer>();
					
					for (String eventString : eventStrings)  // each Event on the line.
					{
						GeneratedEvent event = GeneratedEventImpl.parseEncodedEvent(eventString);
		
						try { eventSerList.add((GeneratedEventSer)(event.externalize())); }
						catch (ParameterError pe) { throw new IOException(pe); }
					}
					
					GeneratedEventSer[] eventSers = new GeneratedEventSer[eventSerList.size()];
					int i = 0;
					String stateId = null;
					for (GeneratedEventSer ser : eventSerList)
					{
						if (stateId == null) stateId = ser.stateNodeId;
						
						eventSers[i++] = ser;
					}
					
					this.history.put(stateId, eventSers);
				}
				
				return this;
			}
		};
	}
}

