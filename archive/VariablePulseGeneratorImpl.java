/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.common.ModelElement.*;
import expressway.ser.*;

import java.util.Set;
import expressway.common.ModelAPITypes.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
//import java.io.ObjectStreamField;
import java.io.Serializable;


public class VariablePulseGeneratorImpl extends ActivityImpl implements VariableGenerator,
	PulseGenerator
{
	/** Ownership. */
	public Port inputPort = null;

	/** Ownership. */
	public Port outputPort = null;
	
	/** Ownership. */
	public Port countPort = null;

	/** ownership. */
	public Port theta1Port = null;
	
	/** ownership. */
	public Port theta2Port = null;
	
	/** Ownership. */
	public Generator embeddedGenerator = null;
	
	/** Ownership. */
	CompensatorImpl compensator = null;
	
	/** Ownership. */
	Max max = null;

	
	public transient String inputPortNodeId = null;
	public transient String outputPortNodeId = null;
	public transient String countPortNodeId = null;
	public transient String theta1PortNodeId = null;
	public transient String theta2PortNodeId = null;
	public transient String embeddedGeneratorNodeId = null;
	public transient String compensatorNodeId = null;
	
	public String getInputPortNodeId() { return inputPortNodeId; }
	public String getOutputPortNodeId() { return outputPortNodeId; }
	public String getCountPortNodeId() { return countPortNodeId; }
	public String getTheta1PortNodeId() { return theta1PortNodeId; }
	public String getTheta2PortNodeId() { return theta2PortNodeId; }
	public String getEmbeddedGeneratorNodeId() { return embeddedGeneratorNodeId; }
	public String getCompensatorNodeId() { return compensatorNodeId; }
	public String getTimeShapeAttrNodeId() { return embeddedGenerator.getTimeShapeAttrNodeId(); }
	
	public String getStateNodeId() { return null; }
	public String getCountStateNodeId() { return null; }


	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		if ((child == inputPort) || (child == outputPort) || (child == countPort)
				|| (child == theta1Port) || (child == theta2Port) || (child == embeddedGenerator)
				|| (child == compensator) || (child == max))
			child.setNameAndNotify(newName);
		else
			super.renameChild(child, newName);
	}


	public NodeSer externalize() throws ParameterError
	{
		super.externalize();
		
		inputPortNodeId = getNodeIdOrNull(inputPort);
		outputPortNodeId = getNodeIdOrNull(outputPort);
		countPortNodeId = getNodeIdOrNull(countPort);
		theta1PortNodeId = getNodeIdOrNull(theta1Port);
		theta2PortNodeId = getNodeIdOrNull(theta2Port);
		embeddedGeneratorNodeId = getNodeIdOrNull(embeddedGenerator);
		compensatorNodeId = getNodeIdOrNull(compensator);

		return new VariablePulseGeneratorSer(this);
	}


	public PersistentNode update(NodeSer nodeSer)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		VariablePulseGeneratorSer ser = 
			(VariablePulseGeneratorSer)checkInstanceCompatibility(
				nodeSer, VariablePulseGeneratorSer.class);
			
		
		// Verify object references.
		// TBD.
		

		return super.update(nodeSer);
	}
	

	private static final String[] MaxInputPortNames = { "comp_port", "gen_port" };
	
	
	/**
	 * Constructor.
	 */
	public VariablePulseGeneratorImpl(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale, double valueDistShape,
		double valueDistScale, boolean ignoreStartup)
	{
		super(name, parent, domain);
		
		VariableGenerator vGen = new VariableGeneratorImpl(name, this, this.domain,
					timeDistShape, timeDistScale, valueDistShape, valueDistScale,
					ignoreStartup);
		
		this.embeddedGenerator = vGen;
		
		try
		{
			inputPort = super.createPort("input_port", PortDirectionType.input, Position.left);
			outputPort = super.createPort("output_port", PortDirectionType.output, Position.right);
			countPort = super.createPort("count_port", PortDirectionType.output, Position.bottom);

			theta1Port = super.createPort("time_scale", PortDirectionType.input, Position.top);
			theta2Port = super.createPort("value_scale", PortDirectionType.input, Position.top);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
				
		
		compensator = new CompensatorImpl("Compensator",
			this, this.getModelDomain(), 0);
			
		try { compensator.setNativeImplementationClass(
			expressway.CompensatorNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
				
		max = new MaxImpl("Max", this, this.getModelDomain(), 0, MaxInputPortNames);
		
		try { max.setNativeImplementationClass(expressway.AbstractMaxNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		Port maxInputPortComp;
		Port maxInputPortGen;
		try {
			maxInputPortComp = max.getPort(MaxInputPortNames[0]);
			maxInputPortGen = max.getPort(MaxInputPortNames[1]);
		}
		catch (ElementNotFound enf) { throw new RuntimeException(
			"Error within PulseGeneratorImpl: cannot find pre-defined Port"); }
		
		//try { addComponent(compensator); }
		//catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		
		// Connect things up.
		try
		{
			//createConduit("Start", inputPort, vGen.getInputPort());
			//createConduit("Count", countPort, vGen.getCountPort());
			//createConduit("GenToComp", vGen.getOutputPort(), compensator.getInputPort());
			//createConduit("CompToOut", compensator.getOutputPort(), outputPort);
			//createConduit("Output", outputPort, vGen.getOutputPort());

			createConduit("Start", inputPort, vGen.getInputPort(), null);
			createConduit("Count", vGen.getCountPort(), countPort, null);
			createConduit("GenToComp", vGen.getOutputPort(), compensator.getInputPort(), null);
			createConduit("GenToMax", vGen.getOutputPort(), maxInputPortGen, null);
			createConduit("CompToMax", compensator.getOutputPort(), maxInputPortComp, null);
			createConduit("Output", max.getOutputPort(), outputPort, null);

			createConduit("TimeScale", theta1Port, vGen.getTheta1Port(), null);
			createConduit("ValueScale", theta2Port, vGen.getTheta2Port(), null);
		}
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	private static double PreferredWidthInPixels = 50.0;
	private static double PreferredHeightInPixels = 100.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	public void setSizeToStandard()
	{
		super.setSizeToStandard();
		
		inputPort.setSide(Position.left);
		outputPort.setSide(Position.right);
		countPort.setSide(Position.bottom);
		theta1Port.setSide(Position.top);
		theta2Port.setSide(Position.bottom);
	}


	//public DistributionFactory getTimeDistFactory() { return embeddedGenerator.getTimeDistFactory(); }

	//public DistributionFactory getValueDistFactory() { return embeddedGenerator.getValueDistFactory(); }

	//public double getTimeDistShape() { return embeddedGenerator.getTimeDistShape(); }

	public double getTimeDistScale() { return embeddedGenerator.getTimeDistScale(); }

	public double getValueDistShape() { return embeddedGenerator.getValueDistShape(); }

	public double getValueDistScale() { return embeddedGenerator.getValueDistScale(); }
	
	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }

	public Port getCountPort() { return countPort; }

	public Port getTheta1Port() { return theta1Port; }

	public Port getTheta2Port() { return theta2Port; }
	
	public boolean getIgnoreStartup() { return embeddedGenerator.getIgnoreStartup(); }
	
	public Generator getEmbeddedGenerator() { return embeddedGenerator; }
	
	public Attribute getTimeShapeAttr() { return embeddedGenerator.getTimeShapeAttr(); }
	
	public void dump(int indentation)
	{
		super.dump(indentation);
		
		inputPort.dump(indentation);
		outputPort.dump(indentation);
		countPort.dump(indentation);
		theta1Port.dump(indentation);
		theta2Port.dump(indentation);
	}
}

