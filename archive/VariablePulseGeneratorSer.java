package expressway.ser;

import expressway.common.*;
import expressway.common.ModelElement.*;
import expressway.common.DecisionElement.*;

public class VariablePulseGeneratorSer extends GeneratorSer
//public class VariablePulseGeneratorSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String countPortNodeId;
	public String theta1PortNodeId;
	public String theta2PortNodeId;
	public String embeddedGeneratorNodeId;
	public String compensatorNodeId;
	
	
	public VariablePulseGeneratorSer(VariableGenerator d)
	{
		super(d);
		
		if (! (d instanceof PulseGenerator)) throw new RuntimeException(
			"Argument to constructor of VariablrePulseGeneratorSer is not a PulseGenerator");
		
		this.inputPortNodeId = d.getInputPortNodeId();
		this.outputPortNodeId = d.getOutputPortNodeId();
		this.countPortNodeId = d.getCountPortNodeId();
		this.theta1PortNodeId = d.getTheta1PortNodeId();
		this.theta2PortNodeId = d.getTheta2PortNodeId();
		this.embeddedGeneratorNodeId = ((PulseGenerator)d).getEmbeddedGeneratorNodeId();
		this.compensatorNodeId = ((PulseGenerator)d).getCompensatorNodeId();
	}                             
}

