/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.classification;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.gui.TreeTablePanelViewBase.*;
import expressway.help.*;
import java.util.Set;
import java.util.HashSet;
import java.awt.Container;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


public class ClassificationVisualJ extends TabularVisualJ
{
	public ClassificationVisualJ(NodeSer rqmtSer, View view)
	throws
		ParameterError
	{
		super(rqmtSer, (TreeTablePanelViewBase)view, ClassificationSer.fieldNames.length);
		if (! (rqmtSer instanceof ClassificationSer)) throw new RuntimeException(
			"rqmtSer is not a ClassificationSer");
		if (! (view instanceof ClassificationViewPanel)) throw new RuntimeException(
			"view is not a ClassificationViewPanel");
		for (int i = 0; i < ClassificationSer.fieldNames.length; i++)
			setValueAt(((ClassificationSer)rqmtSer).getValueAt(i), i);
	}
	
	
	public String getNodeKind() { return "Classification"; }
	
	
	public String getDemotionNodeKind() { return "Classification"; }
	
	
	public String getDescriptionPageName() { return "Classifications"; }


	public String getScenarioId() { return null; }
	
	
	public void updateServer(int column)
	{
		switch (column)
		{
		case 0: // name
			{
				try
				{
					try { getModelEngine().setNodeName(false, getNodeId(), getNodeSer().getName()); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						{
							getModelEngine().setNodeName(true, getNodeId(), getNodeSer().getName());
						}
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		case 1: // String htmlDescription
			{
				try { getModelEngine().setNodeHTMLDescription(getNodeId(),
					((ClassificationSer)(getNodeSer())).htmlDescription); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		default: throw new RuntimeException("Unexpected column number: " + column);
		}
	}
	
	
	protected void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		/*
		// Find adjacent Strategy Views, if any.
		
		Set<TreeTablePanelViewBase> otherTreeTablePanelViews = 
			getTreeTablePanelViewBase().getAdjacentTreeTablePanelViews();
			
		Set<TreeTablePanelViewBase> adjacentStrategyViews = new HashSet<TreeTablePanelViewBase>();
		for (TreeTablePanelViewBase otherView : otherTreeTablePanelViews)
		{
			if (otherView.getOutermostVisual() instanceof StrategyVisual)
				adjacentStrategyViews.add(otherView);
		}
		
		for (final TreeTablePanelViewBase otherView : adjacentStrategyViews)
			//if (adjacentStrategyViews.size() > 0)
			// this Visual exists in a DualHierarchyPanel and the other
			// View consists of Strategies
		{
			JMenuItem menuItem = new HelpfulMenuItem("Link to...", false,
				"Link, via a " +
				HelpWindow.createHref("Named References", "Named Reference") +
				", the selected row (" +
				HelpWindow.createHref("Classifications", "Classification") + ") to a " +
				"node in another View. The name of " +
				"the Named Reference used is 'ImplementsClassification'.");
			
			menuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// The user should now
					// select a Strategy TreeTableNode from the other View.
					// This is a modal operation.
					
					otherView.selectNode(new TabularSelectionHandler()
					{
						public void tableVisualSelected(TabularVisualJ otherVisual)
						{
							try
							{
								try { getModelEngine().createCrossReference(false,
									ClassificationVisualJ.this.getNodeSer().getDomainId(),
									StandardNamedReferences.ImplementsClassification,
									otherVisual.getNodeId(), ClassificationVisualJ.this.getNodeId()); }
								catch (Warning w)
								{
									if (JOptionPane.showConfirmDialog(null, w.getMessage(),
										"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
									{
										getModelEngine().createCrossReference(true,
											ClassificationVisualJ.this.getNodeSer().getDomainId(),
											StandardNamedReferences.ImplementsClassification,
											otherVisual.getNodeId(), ClassificationVisualJ.this.getNodeId());
									}
								}
							}
							catch (Exception ex)
							{
								ErrorDialog.showReportableDialog(getViewPanel(),
									"While created named reference '" + 
									StandardNamedReferences.ImplementsClassification + 
									"' on the server", "Server Error", ex);
							}
						}
					});
					
				}
			});
			
			containerPopup.add(menuItem);
		}*/
	}
	
	
	public boolean isEditable(int column) { return column >= 0; }
}

