/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.rqmts;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.gui.modeldomain.ModelScenarioViewPanel;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JPopupMenu;
import javax.swing.JComponent;
import javax.swing.ImageIcon;
import javax.swing.tree.*;
import javax.swing.event.*;


/**
 * Provide a hierarchical depiction of a RequirementElements. The View may be
 * rooted at any Requirement - not necessarily a RequirementDomain. Elements are
 * editable and deletable, and can be moved from one place to another within
 * their hierarchy.
 */
 
public class RequirementViewPanel
	extends GenericTreeTablePanelView<RequirementSer>
{
	public RequirementViewPanel(PanelManager panelManager, NodeSer nodeSer, 
		ViewFactory viewFactory, ModelEngineRMI modelEngine)
	{
		super(panelManager, nodeSer, viewFactory, modelEngine, "list");
	}
	
	
	protected Class getTreeNodeVisualType() { return RequirementVisualJ.class; }
	
	
	protected Class getAttributeVisualType() { return RequirementAttributeVisualJ.class; }
}

