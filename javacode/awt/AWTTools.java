/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package awt;

import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import javax.swing.*;

public class AWTTools
{
	/** ************************************************************************
	 * Forward the MouseEvent to the specified Component. The Component must
	 * visually enclose the Point associated with the Event: if it does not, the
	 * Event will not be forwarded and the method will return null. The Event
	 * location will be translated to the Component when it is forwarded.
	 * If e's target is c, then do nothing and return false;
	 */
	 
	public static boolean forwardEventToContainingComponent(MouseEvent e,
		Component c)
	{
		Component originalTarget = (Component)(e.getComponent());
		
		if (c == originalTarget) return false;
		

		// Translate outward/upward from originalTarget to the root Container.
		// This is vector A in the slide "Translating a Mouse Event".
		
		int dxUp = 0;
		int dyUp = 0;
		for (Component comp = originalTarget;;)  // each parent, until originalTarget is found.
		{
			dxUp += comp.getX();
			dyUp += comp.getY();
			
			comp = comp.getParent();
			if (comp == null) break;
		}
		
		
		// Translate outward/upward from c to the root Container.
		// This is vector B in the slide "Translating a Mouse Event".
		
		int dxDown = 0;
		int dyDown = 0;
		for (Component comp = c;;)  // each parent, until c is found.
		{
			dxDown += comp.getX();
			dyDown += comp.getY();
			
			comp = comp.getParent();
			if (comp == null) break;
		}
		
		
		// Subtract the vectors. This is A-B in the slide "Translating a Mouse Event".
		
		int dx = dxUp - dxDown;
		int dy = dyUp - dyDown;
				
		if (! c.contains(e.getX() + dx, e.getY() + dy)) return false;
		
		
		e.translatePoint(dx, dy); // This computes D in the slide "Translating a Mouse Event".
		e.setSource(c);
		c.dispatchEvent(e);
		
		return true;
	}
	

	/** ************************************************************************
	 * Translate and forward the specified Event to the deepest Component that is
	 * visible, and that is underneath the current target of the Event. If such
	 * a Component is not found then forward to the parent Container. If the
	 * Event is forwarded return true; otherwise return false.
	 */
	
	public static boolean forwardEventToDeepestContainingComponent(MouseEvent e)
	{
		Component originalTarget = e.getComponent();
		Container originalTargetParent = originalTarget.getParent();
		if (originalTargetParent == null) return false;
		
		Point eventLocInComp = e.getPoint();
		Point compLoc = originalTarget.getLocation();
		Point eventLocInParent = new Point(eventLocInComp);
		eventLocInParent.translate(compLoc.x, compLoc.y);

		
		// Find the next lower Component within the Event target's Container that
		// contains the Event Point. We want to forward to that Component or to
		// one of its children - whichever is most deeply nested.
		
		Component nextLowerComp = 
			getNextLowerComponent(originalTarget, eventLocInParent);
		
		if (nextLowerComp == null)
		{
			// Forward to Container.
			e.translatePoint(originalTarget.getLocation().x,
				originalTarget.getLocation().y);
			
			//System.out.println("dispatching to " + originalTargetParent.getName());
			//Container p = originalTargetParent.getParent();
			//System.out.println("Parent of new target is " + (p == null ? "null" : p.getName()));
			e.setSource(originalTargetParent);
			originalTargetParent.dispatchEvent(e);
			return true;
		}
			
			
		// Determine Event location in the coordinates of the next Lower Component.
		
		Point nextLowerCompLoc = nextLowerComp.getLocation();
		
		Point eventLocInNextLowerComp = new Point(			
			eventLocInComp.x + compLoc.x - nextLowerCompLoc.x,
			eventLocInComp.y + compLoc.y - nextLowerCompLoc.y
			);
		
			
		// See if there is a sub-Component of the next lower Component that contains
		// the Event location.
		
		Component target = null;
		Point translatedPoint = new Point();
		
		if (nextLowerComp instanceof Container)
		{
			target = findDeepestComponentAt((Container)nextLowerComp, eventLocInNextLowerComp.x,
				eventLocInNextLowerComp.y, translatedPoint);
		}
		
		//System.out.println(
		//	"Deepest comp at " + translatedPoint + " is " +
		//	(target == null ? "null" : target.getName()));
			
		
		if (target == null)  // there was not: did not find a Component nested
			// within nextLowerComp that contains the Point.
		{
			target = nextLowerComp;
			translatedPoint = eventLocInNextLowerComp;
		}
		
		
		// Translate Event location to the coordinates of the new target.
		
		e.translatePoint(translatedPoint.x - eventLocInComp.x,
			translatedPoint.y - eventLocInComp.y);
			
		//System.out.println(e.getPoint());

		
		// Re-dispatch Event to the new target.
		
		//System.out.println("Dispatching to " + target.getName());
		//Container p = target.getParent();
		//System.out.println("Parent of new target is " + (p == null ? "null" : p.getName()));
		e.setSource(target);
		target.dispatchEvent(e);
		
		return true;  // Event was re-dispatched.
	}
	
	
	/** ************************************************************************
	 * Recursively find the deepest sub-Component that is visible at the specified
	 * x, y location within the specified Container. If there is none, return null.
	 * If there is one, return in translatedPoint the location relative to the
	 * Component that is being returned. If null is returned, the value of
	 * translatedPoint is undefined.
	 */
	
	public static Component findDeepestComponentAt(Container container, int x, int y, 
		Point translatedPoint)
	{
		if (translatedPoint == null) throw new RuntimeException(
			"The argument 'translatedPoint' cannot be null.");
		
		Component subComp = container.getComponentAt(x, y);
			
			
		if (subComp == null) return null;
		if (subComp == container) return null;
		
		//System.out.println("\tFound a direct sub-component of 'container' that contains the Point.");
		
		
		// Found a direct sub-component of 'container' that contains the Point.
		
		
		// Translate x and y to the sub-Component's space.
		
		Point subCompLocation = subComp.getLocation();
		int xInSubComp = x - subCompLocation.x;
		int yInSubComp = y - subCompLocation.y;
		translatedPoint.setLocation(xInSubComp, yInSubComp);
		
		
		// If the sub-Component is itself a Container, call recursively.

		if (subComp instanceof Container)
		{
			Component next =
				findDeepestComponentAt((Container)subComp,
					xInSubComp, yInSubComp, translatedPoint);
				//findDeepestComponentAt((Container)subComp,
				//	subCompLocation.x, subCompLocation.y, translatedPoint);
					
			if (next == null)
			{
				translatedPoint.setLocation(xInSubComp, yInSubComp);
				return subComp;
			}
			else
				return next;
		}
		else
			return subComp;
	}
	
	
	/** ************************************************************************
	 * Returns the next lower component in the Z position that contains the
	 * specified Point specified in the reference frame of the parent Container.
	 * Lower means underneath. Only consider direct children of thisComponent's
	 * Container.
	 * If there is no such lower component within thisComponent's Container, or
	 * if thisComponent has no Container, return null.
	 *
	 * **************IMPORTANT***************
	 * Note: this method is not thread-safe. When calling it, ensure that the
	 * children of the parent of thisComponent are not changed or re-ordered.
	 */
	 
	public static Component getNextLowerComponent(Component thisComponent, Point p)
	{
		//System.out.println("\tgetNextLowerComponent(" + thisComponent.getName() +
		//	", " + p + ")");
			
		Container container = thisComponent.getParent();
		if (container == null) return null;
		//System.out.println("\t\tcontainer=" + container.getName());
		
		int zPosition = container.getComponentZOrder(thisComponent);
		//System.out.println("\t\tzPosition=" + zPosition);

		
		// Iterate over each child of the Container that is below thisComponent.
		
		int noOfComponents = container.getComponentCount();
		for (int compNo = container.getComponentZOrder(thisComponent) + 1;
			compNo < noOfComponents; compNo++)
		{
			Component component = container.getComponent(compNo);
			
			
			// Translate to component frame
			
			Point location = component.getLocation();
			Point pComp = new Point(p.x - location.x, p.y - location.y);
			
			
			// See if component contains the Point p.
			
			if (component.contains(pComp))
			{
				return component;
			}
		}
		
		return null;
	}
	
	
	/** ************************************************************************
	 * Identify and return the Component that was selected using the right mouse
	 * button, as part of a context menu selection.
	 */
	 
	public static Component getJPopupMenuOriginator(ActionEvent e)
	{
		Object source = e.getSource();
		if (! (source instanceof JMenuItem)) throw new RuntimeException(
			"The source is not a JMenuItem");
		
		Component component = ((JMenuItem)source).getComponent();
		Component parent = component.getParent();
		Component originator = ((JPopupMenu)parent).getInvoker();
		
		return originator;
	}


	/** ************************************************************************
	 * Utility method for retrieving an image specified by a resource name.
	 */
	 
	public static Image getImage(Class codebase, String imageResourceName)
	{
		return getImage(codebase.getResource(imageResourceName));
	}
	
	
	public static Image getImage(URL url)
	{
		if (url == null) throw new RuntimeException("URL is null.");
		
		return Toolkit.getDefaultToolkit().getImage(url);
	}
	
	
	public static Frame getContainingFrame(Component component)
	{
		for (;;)
		{
			Container parent = component.getParent();
			if (parent == null)
				if (component instanceof Frame)
					return (Frame)component;
				else
					throw new RuntimeException("Containing Frame not found");
			
			component = parent;
		}
	}
}

