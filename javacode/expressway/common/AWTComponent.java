/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;
import java.awt.*;
import java.awt.event.*;


/**
 * Define all of the AWT Component methods that Expressway uses.
 */
 
public interface AWTComponent
{
	/** Return this AWTComponent, cast to an AWT Component. */
	Component getComponent();
	
	void add(PopupMenu popup);
	
	void addComponentListener(ComponentListener l);
	
	void removeComponentListener(ComponentListener l);
	
	void addMouseListener(MouseListener l);
	
	boolean contains(Point p);
	
	Rectangle getBounds();
	
	Component getComponentAt(int x, int y);
	
	Graphics getGraphics();
	
	String getName();
	
	Container getParent();
	
	Dimension getSize();
	
	Toolkit getToolkit();
	
	int getWidth();
	
	int getHeight();
	
	int getX();
	
	int getY();
	
	Point getLocation();
	
	Point getLocation(Point rv);
	
	Point getLocationOnScreen();
	
	boolean isEnabled();
	
	boolean isOpaque();
	
	boolean isShowing();
	
	boolean isVisible();
	
	void repaint();
	
	void setBounds(int x, int y, int width, int height);
	
	void setBounds(Rectangle r);
	
	void setForeground(Color c);
	
	void setLocation(int x, int y);
	
	void setLocation(Point p);
	
	void setName(String name);
	
	void setPreferredSize(Dimension preferredSize);
	
	void setSize(Dimension d);
	
	void setSize(int width, int height);
	
	void setVisible(boolean b);
	
	void validate();
}

