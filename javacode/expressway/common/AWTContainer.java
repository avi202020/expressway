/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import expressway.common.ModelAPITypes.*;
import java.awt.*;


/**
 * Define all of the AWT Container methods that Expressway uses.
 */
 
public interface AWTContainer extends AWTComponent
{
	/** Return this AWTContainer, cast to an AWT Container. */
	Container getContainer();
	
	Component add(Component comp);
	
	Component add(Component comp, int index);
	
	Component add(String name, Component comp);
	
	Component getComponent(int n);
	
	int getComponentCount();
	
	Component[] getComponents();
	
	int getComponentZOrder(Component comp);
	
	Insets getInsets();
	
	boolean isAncestorOf(Component c);
	
	void remove(Component comp);
	
	void remove(int index);
	
	void removeAll();
	
	void setComponentZOrder(Component comp, int index);
	
	void setFont(Font f);
	
	void setLayout(LayoutManager mgr);
}

