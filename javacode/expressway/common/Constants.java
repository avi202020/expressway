/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

public class Constants
{
	public final static String RMIServiceName = "Expressway";
	public static final String ConfigFileName = "expressway_config.xml";
	
}

