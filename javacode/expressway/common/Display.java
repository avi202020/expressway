/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import geometry.Point;

public interface Display extends Point
{
	double getX();
	double getY();
	double getScale();
	double getOrientation();  // in radians.

	void setX(double x);
	void setY(double y);
	void setScale(double scale);
	void setOrientation(double angle);  // in radians.
	
	//Object clone() throws CloneNotSupportedException;
	
	//Object deepCopy();
}



