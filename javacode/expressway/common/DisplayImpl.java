/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import geometry.PointImpl;
import expressway.common.Display;


public class DisplayImpl extends PointImpl implements Display, Cloneable
{
	public double orientation = 0.0;
	public double scale = 1;
	
	
	public DisplayImpl()
	{
		super(0.0, 0.0);
	}
	

	public double getScale() { return scale; }
	public double getOrientation() { return orientation; }  // in radians.

	public void setX(double x) { this.x = x; }
	public void setY(double y) { this.y = y; }
	public void setScale(double scale) { this.scale = scale; }
	public void setOrientation(double angle) { this.orientation = angle; }  // in radians.

	
	public Object clone() throws CloneNotSupportedException
	{
		Display newDisplay = (DisplayImpl)(super.clone());
		return newDisplay;
	}
	
	
	public String toString()
	{
		return "Display: " +
			"x=" + x + ", y=" + y + ", scale=" + scale + ", or=" + orientation + "]";
	}
}



