/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import java.util.Date;


/**
 * Encapsulates all information pertaining to a simulation iteration.
 * The intent is that each Epoch is unique in the course of a simulation,
 * even if they occur at identical time, separated only be causality (and by
 * an infinitesimal and unmeasurable amount of time).
 */
 
public interface Epoch extends java.io.Serializable
{
	long getTime();  // in simulated time. Milliseconds.
	
	Date getDate();  // equivalent to getMilliseconds, but as a Date object.
			// Treat this as a value object. Do not rely on object identity.
			
	int getIteration();  // beginning at 1. However, a value of 0 is allowed
			// to indicate a "starting" epoch prior to the first actual
			// epoch.
			
	Object deepCopy();
}
	


