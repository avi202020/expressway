/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import java.util.Date;


/**
 * Represents a simulation iteration: an indivisible instant in simulated time.
 * Instances of this class are immutable.
 */
 
public class EpochImpl implements Epoch, Cloneable
{
	private long ms;
	private int iteration;
	
	
	public EpochImpl(long ms, int iteration)
	{
		if (iteration <= 0) throw new RuntimeException("iteration=" + iteration);
		
		this.ms = ms;
		this.iteration = iteration;
	}
	
	
	public EpochImpl(Date date, int iteration)
	{
		this(date.getTime(), iteration);
	}
	
	
	public boolean equals(Object otherEpoch)
	{
		if (! (otherEpoch instanceof Epoch)) return false;
		
		return
			(ms == ((Epoch)otherEpoch).getTime()) &&
			(iteration == ((Epoch)otherEpoch).getIteration());
	}
	
	
	public long getTime() { return ms; }
	
	
	public Date getDate() { return new Date(ms); }
	
	
	public int getIteration() { return iteration; }
	
	
	public Object deepCopy()
	{
		try { return this.clone(); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
	}
	
	
	public String toString() { return "(" + ms + " ms, iter " + iteration + ")"; }
}

