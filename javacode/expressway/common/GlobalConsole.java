/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import javax.swing.JTextArea;
import java.io.*;


/**
 * For defining a statically global Text Area that can be used as a console, instead
 * of stdout. This is needed because a reliable method for redirecting stdout and
 * stderr to a TextArea has not been found (Mac platform).
 */
 
public class GlobalConsole
{
	private static JTextArea textArea;
	private static int seqNo = 1;
	private static boolean prependSeqNo = true;
	private static boolean IncludeSeqNo = true;
	
	
	public synchronized static void setTextArea(JTextArea textArea)
	{
		GlobalConsole.textArea = textArea;
	}
	
	
	public synchronized static JTextArea getTextArea()
	{
		return textArea;
	}
	
	
	public synchronized static void print(Object obj)
	{
		if (obj == null) obj = "null";
		
		String seqStr = "";
		if (prependSeqNo)
		{
			prependSeqNo = false;
			if (IncludeSeqNo) seqStr = String.valueOf(++seqNo) + ": ";
		}
		
		if (textArea == null) System.out.print(seqStr + obj.toString());
		else textArea.append(seqStr + obj.toString());
	}
	
	
	public synchronized static void println(Object obj)
	{
		String seqStr = "";
		if (prependSeqNo)
		{
			if (IncludeSeqNo) seqStr = String.valueOf(++seqNo) + ": ";
		}
		prependSeqNo = true;
		
		if (obj == null) obj = "null";
		
		if (textArea == null) System.out.println(seqStr + obj.toString());
		else textArea.append(seqStr + obj.toString() + "\n");
	}
	
	
	public synchronized static void println()
	{
		prependSeqNo = true;
		if (textArea == null) System.out.println();
		else textArea.append("\n");
	}
	
	
	public synchronized static void printStackTrace(Throwable t)
	{
		printStackTrace(t, true);
	}
	
	
	public synchronized static void printStackTrace(Throwable t, boolean withLineNos)
	{
		if (withLineNos)
		{
			if (IncludeSeqNo) println((String.valueOf(++seqNo)) + ": ");
			prependSeqNo = true;
		}
		
		if (textArea == null) printTraceElements(System.out, t);
		else
		{
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			printTraceElements(pw, t);
			pw.flush();
			textArea.append(sw.toString());
		}
		
		Throwable cause = t.getCause();
		if (cause != null)
		{
			if (textArea == null) System.out.print("Caused by: ");
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				pw.print("Caused by: ");
				pw.flush();
				textArea.append(sw.toString());
			}
			
			printStackTrace(cause, false);
		}
	}
	
	
	public synchronized static void flush()
	{
		if (textArea == null) System.out.flush();
	}
	
	
	protected static void printTraceElements(PrintStream ps, Throwable t)
	{
		ps.println(t.getClass().getName() + ": " + t.getMessage());
		StackTraceElement[] elts = t.getStackTrace();
		ps.println("\t" + elts.length + " trace lines:");
		for (StackTraceElement elt : elts)
		{
			ps.println("\t" + elt);
		}
	}
	
	
	protected static void printTraceElements(PrintWriter pw, Throwable t)
	{
		pw.println(t.getClass().getName() + ": " + t.getMessage());
		StackTraceElement[] elts = t.getStackTrace();
		pw.println("\t" + elts.length + " trace lines:");
		for (StackTraceElement elt : elts)
		{
			pw.println("\t" + elt);
		}
	}
}

