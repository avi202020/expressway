/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import expressway.common.*;
import geometry.PointImpl;


public class InflectionPointImpl extends PointImpl implements InflectionPoint
{
	public InflectionPointImpl(double x, double y)
	{
		super(x, y);
	}
	
	
	public String toString()
	{
		return "[ InflectionPoint: x=" + x + ", y=" + y + " ]";
	}
}
