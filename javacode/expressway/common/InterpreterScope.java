/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


/**
 * Base class for any symbol table used by an ExpressionInterpreter.
 */
 
public interface InterpreterScope
{
	public interface ClassScope extends InterpreterScope
	{
		/**
		 * Search the ClassLoader for the specified Java package.
		 */
		 
		Package findJavaPackage(String name);
	}
}

