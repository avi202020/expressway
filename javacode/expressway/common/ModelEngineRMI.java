/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;

import java.rmi.Remote;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import java.io.IOException;
import statistics.HistogramDomainParameters;
import statistics.TimeSeriesParameters;
import java.util.List;
import java.util.Vector;
import java.util.Date;
import java.util.Set;
import java.io.Serializable;
import javax.swing.ImageIcon;


/**
 * Provides the "JavaRPC" interface that is made avaiable to the client.
 *
 * These methods are assumed to be transactional, for implementations
 * that support multiple concurrent requests.
 *
 * Structural changes (other than Attribute values) are disallowed
 * while a simulation is underway.
 */

public interface ModelEngineRMI extends Remote
{
	/** ************************************************************************
	 * Register the specified PeerListener with this ModelEngine. The ModelEngine
	 * will notify the PeerListener when PersistentNodes change.
	 */
	
	ListenerRegistrar registerPeerListener(PeerListener listener)
	throws
		ParameterError,
		IOException;
		
	
	/** ************************************************************************
	 * Unregister the specified Peer Listener from this Model Engine.
	 */
	 
	void unregisterPeerListener(PeerListener listener)
	throws
		ParameterError,
		IOException;



	String ping()
	throws
		IOException;

		
	
	void flush()
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
	
	
	Integer parseXMLCharAr(char[] charAr, PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException;
		
	
	
	Integer processJARByteAr(byte[] byteAr, PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException;
	
	
	
	ModelDomainSer getModelDomain(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	DecisionDomainSer getDecisionDomain(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	List<String> getDomainNames()
	throws
		IOException;
	
		
		
	List<NodeDomainSer> getDomains()
	throws
		ParameterError,
		IOException;
	
		
		
	List<ModelDomainSer> getModelDomains()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
		
	List<ModelDomainMotifDefSer> getModelDomainMotifs()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	List<MotifDefSer> getMotifsUsedByDomain(String domainId)
	throws
		ParameterError,
		IOException;
	
	
	
	boolean domainUsesMotif(String domainId, String motifId)
	throws
		ParameterError,
		IOException;
	

		
	void useMotif(String domainId, String motifId, boolean yes)
	throws
		ParameterError,
		Warning,
		IOException;
	
	
	
	byte[] getMotifResource(String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException;
	
	
	
	byte[] getMotifClass(String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException;
		
		
		
	List<DecisionDomainSer> getDecisionDomains()
	throws
		ParameterError,
		IOException;
		
		
	
	String exportNodeAsXML(String nodeId)
	throws
		ParameterError,
		IOException;
		
		
	
	DomainTypeSer[] getDomainTypes()
	throws
		ParameterError,
		IOException;
		
		
	
	void createDomainForDomainType(String id)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
	
	DomainTypeSer[] getBaseDomainTypes()
	throws
		ParameterError,
		IOException;
		
		
	
	void createDomainType(String baseDomainTypeId, String... motifDefIds)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
	
	void deleteDomainType(String domainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
	
	MotifDefSer[] getMotifsForBaseDomainType(String baseDomainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
	
	MotifDefSer[] getMotifsCompatibleWithDomainId(String domainId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
		
	DomainTypeSer[] getUserDefinedDomainTypes()
	throws
		ParameterError,
		IOException;
	
	
	
	MotifDefSer createMotifForDomainType(String domainBaseTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
	
	
	
	String getViewType(String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
		
		
		
	String getViewClassName(String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException;
	
		
		
	ModelDomainSer createModelDomain(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException;

		

	ModelDomainSer createModelDomain()
	throws
		ParameterError,
		IOException;

		

	ModelDomainMotifDefSer createModelDomainMotif(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException;

		

	ModelDomainMotifDefSer createModelDomainMotif()
	throws
		ParameterError,
		IOException;

		
	ModelDomainSer createIndependentCopy(String modelDomainId)
	throws
		ParameterError,
		ModelContainsError,
		IOException;
		
		
		
	DecisionDomainSer createDecisionDomain(String name, 
		boolean useNameExactly)
	throws
		ParameterError,
		IOException;

		
	
	ScenarioSer createScenario(String domainName, String scenarioName)
	throws
		ParameterError,
		IOException;
	
		
	
	ScenarioSer copyScenario(String scenarioId)
	throws
		ModelContainsError,
		ParameterError,
		IOException;
	
		
	
	ScenarioSer createScenario(String domainName)
	throws
		ParameterError,
		IOException;
	
		
	
	AttributeSer createAttribute(boolean confirm, String parentId, 
		double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	AttributeSer createAttributeBefore(boolean confirm, String nextAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	AttributeSer createAttributeAfter(boolean confirm, String priorAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	int getNoOfAttributes(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		IOException;

	
	AttributeSer createAttribute(boolean confirm, String parentId, 
		Serializable value, String scenarioId, 
		double x, double y)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,
		IOException;

	
	AttributeSer createHierarchyDomainAttributeBefore(boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		
		
	AttributeSer createHierarchyDomainAttributeAfter(boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;

	
	AttributeStateBindingSer bindAttributeToState(boolean confirm, String attrId,
		String stateFullName)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;
		
		

	void deleteAttributeStateBinding(boolean confirm, String bindingNodeId)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	
	void setStateBindingParameters(boolean confirm, String domainOrScenarioId, Boolean type,
		TimePeriodType period)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		


	void bindScenarioToScenarioForDomain(boolean confirm, 
		String bindingScenarioId, String boundScenarioId, String boundDomainId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
	
	
	
	AttributeStateBindingSer getAttributeStateBinding(String attrId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	
	ModelDomainSer[] getBoundDomains(String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	
	ModelDomainSer[] getBindingDomains(String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;
	
	
	
	ModelScenarioSer getBoundScenarioForBoundDomain(String bindingScenarioId, 
		String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException;



	StateSer[] getStatesForModelDomain(String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException;



	StateSer[] getTallyStatesForModelDomain(String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException;



	NodeSer createListNodeBefore(boolean confirm, String parentId,
		String nextNodeId, String scenarioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	NodeSer createListNodeAfter(boolean confirm, String parentId,
		String priorNodeId, String scenarioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;


	
	NodeSer cloneListNodeAfter(boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		InternalEngineError,
		CannotObtainLock,
		IOException;
		
		
		
	String[] getInterfaceNodeKinds()
	throws
		ParameterError,
		IOException;
	
	
		
	String[] getInstantiableNodeKinds(String parentId)
	throws
		ParameterError,
		IOException;
	
	
	
	String[] getInstantiableNodeDescriptions(String parentId)
	throws
		ParameterError,
		IOException;
	
	
	
	ModelElementSer createModelElement(boolean confirm, 
		String parentId, String kind,
		double x, double y)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;


	
	void deleteNode(boolean confirm, String elementId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;



	void demoteListNode(boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		

	
	void promoteListNode(boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;

		

	void createNamedReference(boolean confirm, String domainId,
		String name, String desc, String fromTypeClassName, String toTypeClassName)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		
		
		
	void deleteNamedReference(boolean confirm, String namedRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
		
		

	void createCrossReference(boolean confirm, //String domainId,
		String namedRefName, String fromNodeId, String toNodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;
	
	
	
	void deleteCrossReference(boolean confirm, String crossRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException;

	
	
	NodeSer[] getNamedRefFromNodes(String nodeId, String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	NodeSer[] getNamedRefToNodes(String nodeId, String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	String[] getNamedReferences(String domainId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	NamedReferenceSer getNamedReferenceForDomain(String domainId,
		String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	CrossReferenceSer[] getCrossReferences(String namedRefId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	CrossReferenceSer[] getCrossReferences(String fromId, String toId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	

	void bindStateAndPort(boolean confirm, String stateId, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;



	void unbindPort(boolean confirm, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
		
		
		
	StateSer getPortBinding(String portId)
	throws
		ParameterError,
		ElementNotFound,
		IOException;
		
		
		
	void setPredefEventDefaultTime(boolean confirm, String predEventNodeId, String timeExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
	
	
	
	void setPredefEventDefaultValue(boolean confirm, String predEventNodeId, String valueExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;
		
		
		
	void setPredefEventIsPulse(boolean confirm, String predEventNodeId, boolean pulse)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException;



	void setNodeName(boolean confirm, String nodeId, String newName)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;



	void setNodeHTMLDescription(String nodeId, String newHTMLDesc)
	throws
		ParameterError,
		CannotObtainLock,
		IOException;
		
		
		
	void setPortReflectivity(boolean confirm, String portId, boolean black)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;
		
		
		
	void setPortDirection(boolean confirm, String portId, 
		PortDirectionType dir)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ModelContainsError,
		IOException;
	
	

	ConduitSer createConduit(boolean confirm, String parentId, 
		String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;
	
		
	
	ConduitSer createConduit(boolean confirm, 
		String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException;

		
	
	void rerouteConduit(String conduitId)
	throws
		CannotObtainLock,
		ParameterError,
		IOException;


	
	void rerouteConduits(String containerId, String[] conduitIds)
	throws
		CannotObtainLock,
		ParameterError,
		IOException;
		
		
		
	NodeSer getNode(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;


		
	NodeSer getNode(String nodeId, Class nodeSerKind)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	NodeSer getChild(String parentId, String childName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	/*NodeSer updateNode(boolean confirm, NodeSer nodeSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;*/


		
	String[] getAvailableHTTPFormats(String nodeId, String[] descriptions)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
	
	
	
	String getWebURLString(String nodeId, String format)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		
		
	
	int getHTTPPort()
	throws
		IOException;
		
		
	
	ScenarioSer updateScenario(boolean confirm, ScenarioSer scenarioSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
	
	
		
	void setModelScenarioTimeParams(boolean confirm, String scenarioId,
		Date newFromDate, Date newToDate, long newDuration, int newIterLimit)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
	
		
		
	NodeSer incrementNodeSize(int lastRefreshed, String nodeId, 
		double dw, double dh,
		double childShiftX, double childShiftY, boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		


	NodeSer incrementNodeLocation(int lastRefreshed, String nodeId, 
		double dx, double dy, boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		

		
	NodeSer changeNodeSize(int lastRefreshed, String nodeId, 
		double newWidth, double newHeight, double childShiftX, double childShiftY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		


	NodeSer changeNodeLocation(int lastRefreshed, String nodeId, 
		double newX, double newY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException;
		

		
	String resolveNodePath(String path)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	String[] getChildNodeIds(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
	
	String[] getSubcomponentNodeIds(String modelContainerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	AttributeSer getAttribute(String nodeId, String attrName)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	AttributeSer[] getAttributes(String nodeId, String className, 
		String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	 
	NodeSer[] getNodesWithAttributeDeep(String NodeId, String className,
		String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
		
	
	AttributeSer[] getAttributesDeep(String modelElementId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	AttributeSer[] getAttributes(String elementPath)
	throws
		ParameterError,  // if the Element does not exist.
		IOException;

	
		
	AttributeSer[] getAttributesForId(String modelElementId)
	throws
		ParameterError,  // if the ModelElement does not exist.
		IOException;

	
	
	boolean isGeneratorRepeating(String genNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
	
	
	void makeGeneratorRepeating(boolean confirm, String genNodeId, boolean repeating)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	void setGeneratorIgnoreStartup(boolean confirm, String genNodeId, boolean ignore)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	ScenarioSer[] getScenarios(String domainName)
	throws
		ParameterError,  // if the Domain does not exist.
		IOException;

	
		
	ScenarioSer[] getScenariosForDomainId(String domainId)
	throws
		ParameterError,  // if the Domain does not exist.
		IOException;
		

	
	ModelScenarioSer getModelScenario(String modelDomainName, 
		String modelScenarioName)
	throws
		ParameterError,  // if the ModelDomain or ModelScenario does not exist.
		ElementNotFound,
		IOException;


	 
	ModelScenarioSetSer[] getScenarioSetsForDomainId(String domainId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	DecisionSer[] getDecisions(String decisionPointPath, String scenarioName)
	throws
		ParameterError,  // if the DecisionPoint does not exist or the
						// DecisionScenario does not exist.
		IOException;

	

	ImageIcon getStoredImageIcon(String handle)
	throws
		ParameterError,
		IOException;
		
		
		
	List<String> getProgress(int callbackId)
	throws
		ParameterError,
		IOException;

		

	List<String> getMessages(int callbackId)
	throws
		ParameterError,
		IOException;

		

	boolean isComplete(int callbackId)
	throws
		ParameterError,
		IOException;


		
	boolean isAborted(int callbackId)
	throws
		ParameterError,
		IOException;

		

	void abort(int callbackId)
	throws
		ParameterError,
		IOException;

		

	Integer simulate(String modelDomainName, String scenarioName, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int runs, long duration,
		PeerListener peerListener,  // may be null
		boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
	
	
	ModelScenarioSetSer createScenariosOverRange(String baseScenId,
		String attrId, Serializable[] values)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		

	Integer simulateScenarioSet(
		String scenSetId, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int noOfRuns, long duration, 
		PeerListener peerListener, boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	double[] computeAssurances(String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		

	Integer[] updateVariable(int lastRefreshed, 
		String decisionDomainName, String scenarioName,
		String decisionPointName, String variableName, Vector<Serializable> valueVector)
	throws
		ClientIsStale,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	List<double[]> getResultStatisticsForScenario(List<String> nodePaths, 
		String modelDomainName,
		String modelScenarioName, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	List<double[]> getResultStatisticsById(List<String> nodeIds,
		String distOwnerId, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	double[] getResultStatisticsById(String nodeId,
		String distOwnerId, String[] options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;


		
	List<int[]> getHistograms(List<String> statePaths, String modelDomainName,
		String modelScenarioName, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException;

	
		
	int[] getHistogramById(String stateId,
		String distOwnerId, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException;

		
	
	HistogramDomainParameters defineOptimalHistogramById(String stateId, 
		String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException;
		

		
	TimeSeriesParameters defineOptimalValueHistoryById(
		String stateId, String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	double[][] getExpectedValueHistory(String stateId, String distOwnerId, 
		Date startingEpoch, int noOfPeriods, long periodInMs)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
		
	double[] getCorrelations(List<String[]> statePaths, String modelDomainName,
		String modelScenarioName, String option)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		
	
	List<String> getSimulationRuns(String modelDomainName, String modelScenarioName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	List<SimulationRunSer> getSimulationRuns(ModelScenarioSer s)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	List<SimulationRunSer> getSimulationRuns(String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	ModelScenarioSer[] getScenariosForScenarioSetId(String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	
	SimulationRunSer[] getSimulationRunsForSimRunSetId(String simRunSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	SimRunSetSer[] getSimRunSetsForScenarioId(String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	List<GeneratedEventSer> getEventsForSimNodeForIteration(String simRunId, int iteration)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	List<GeneratedEventSer>[] getEventsByEpoch(String modelDomainName, 
		String modelScenarioName,
		String simRunName, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;

		

	List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String simNodeId, 
		List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;



	List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String simNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	Object[] getEventsForSimNodeByEpoch(String simNodeId,
		List<String> statePaths, Class[] stateTags)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	StateSer[] getStatesRecursive(String modelContainerNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
		
	String getEventsByEpochAsHTML(String simRunNodeId, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		


	String getEventsByEpochAsHTML(String simRunNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		


	Serializable getAttributeValue(String attrPath, String scenarioName)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;
	
		
	
	Serializable getAttributeValueForId(String attrId, String scenarioName)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;
	
	
		
	Serializable getAttributeValueById(String attrId, String scenarioId)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;

		
		
	Serializable[] getAttributeValuesForNode(String nodeId, String scenarioId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException;
	
	
		
	Serializable getAttributeDefaultValueById(String attrId)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException;
		
		
		
	void setAttributeDefaultValue(boolean confirm, String attrId, Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,
		IOException;
	
	
	
	Serializable[] getAttributeDefaultValuesForNode(String nodeId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException;
		
		
		
	Serializable getAttributeDefaultValue(String attrPath)
	throws
		ParameterError,
		IOException;
	
		
	
	Object[] getStateValuesForIteration(String simRunId, int iteration)
	throws
		CannotObtainLock,
		ParameterError,
		IOException;
	
	
	
	Serializable[] getFinalStateValues(String statePath, String scearioName)
	throws
		CannotObtainLock,
		ParameterError,  // if the State does not exist, or the
						// Scenario does not exist.
		IOException;

		
		
	Serializable[] getFinalStateValuesForId(String stateId, String scearioId)
	throws
		CannotObtainLock,
		ParameterError,  // if the State does not exist, or the
						// Scenario does not exist.
		IOException;

		
		
	void setAttributeValue(boolean confirm, String attrNodeId, 
		String scenarioId, Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,  // if the value cannot be set, e.g., if the Attribute
						// is bound to a state.
		IOException;
		
		
	
	/*void setAttributeValue(boolean confirm, String attrNodeId, 
		Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,  // if the value cannot be set, e.g., if the Attribute
						// is bound to a state.
		IOException;*/
		
		
	
	void watchNode(boolean enable, String nodePath)
	throws
		ParameterError,  // if the PersistentNode does not exist.
		IOException;
}
