/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


import awt.AWTTools;
import javax.swing.Icon;
import javax.swing.ImageIcon;


/**
 * The paths on the server (relative to the server's codebase) of icon images
 * are used as handles (names) for those images.
 */
 
public class NodeIconImageNames
{
	public static final String DefaultImageName = "/images/DefaultImage.png";
	
	public static final String NormalDistImageName = "/images/NormalDistImage.png";

	
	//public static final String ContainerIconImageName = "/images/ContainerIconImage.png";
	//public static final String DomainIconImageName = "/images/DomainIconImage.png";
	//public static final String ModelContainerIconImageName = "/images/ModelContainerIconImage.png";
	//public static final String PortedContainerIconImageName = "/images/PortedContainerIconImage.png";
	//public static final String DecisionModelRelationIconImageName = "/images/DecisionModelIcomImage.png";
	public static final String ActionIconImageName = "/images/ActionIconImage.png";
	public static final String ActivityIconImageName = "/images/ActivityIconImage.png";
	public static final String AttributeIconImageName = "/images/AttributeIconImage.png";
	public static final String AttributeStateBindingIconImageName = "/images/AttributeStateBindingIconImage.png";
	public static final String BasicAttributeIconImageName = "/images/AttributeIconImage.png";
	public static final String ChoiceIconImageName = "/images/ChoiceIconImage.png";
	public static final String ClassificationIconImageName = "/images/ClassificationIconImage.png";
	public static final String CoDependencyIconImageName = "/images/CoDependencyIcomImage.png";
	public static final String CompensatorIconImageName = "/images/CompensatorIconImage.png";
	public static final String ConduitIconImageName = "/images/ConduitIconImage.png";
	public static final String ConstantValueIconImageName = "/images/ConstantValueIconImage.png";
	public static final String ConstraintIconImageName = "/images/ConstraintIconImage.png";
	public static final String CrossReferenceImageName = "/images/NamedReferenceIconImage.png";
	public static final String DecisionDomainIconImageName = "/images/DecisionDomainIconImage.png";
	public static final String DecisionAttributeIconImageName = "images/DecisionAttributeIconImage.png";
	public static final String DecisionFunctionIconImageName = "/images/DecisionFunctionIcomImage.png";
	public static final String DecisionIconImageName = "/images/DecisionIcomImage.png";
	public static final String DecisionPointIconImageName = "/images/DecisionPointIconImage.png";
	public static final String DecisionScenarioIconImageName = "/images/DecisionScenarioIconImage.png";
	public static final String DecisionScenarioSetIconImageName = "/images/DecisionScenarioSetIconImage.png";
	public static final String DelayIconImageName = "/images/DelayIconImage.png";
	public static final String DerivesIconImageName = "/images/DerivesIconImage.png";
	public static final String DiscriminatorIconImageName = "/images/DiscriminatorIconImage.png";
	public static final String DomainTypeIconImageName = "/images/DomainTypeIconImage.png";
	public static final String DoubleExpressionIconImageName = "/images/DoubleExpressionIconImage.png";
	public static final String FunctionIconImageName = "/images/FunctionIconImage.png";
	public static final String GeneratorIconImageName = "/images/GeneratorIconImage.png";
	public static final String HierarchyDomainIconImageName = "/images/HierarchyIconImage.png";
	public static final String HierarchyIconImageName = "/images/HierarchyIconImage.png";
	public static final String HierarchyScenarioIconImageName = "/images/HierarchyScenarioIconImage.png";
	public static final String HierarchyScenarioSetIconImageName = "/images/HierarchyScenarioSetIconImage.png";
	public static final String MaxIconImageName = "/images/MaxIconImage.png";
	public static final String MenuTreeIconImageName = "/images/MenuTreeIconImage.png";
	public static final String ModelDomainIconImageName = "/images/ModelDomainIconImage.png";
	public static final String ModelScenarioIconImageName = "/images/ModelScenarioIconImage.png";
	public static final String ModelScenarioSetIconImageName = "/images/ModelScenarioSetIconImage.png";
	public static final String ModifiesIconImageName = "/images/ModifiesIcomImage.png";
	public static final String ModulatorIconImageName = "/images/ModulatorIconImage.png";
	public static final String MotifAttributeIconImageName = "images/MotifAttributeIconImage.png";
	public static final String MotifDefIconImageName = "/images/MotifDefIconImage.png";
	public static final String NamedReferenceIconImageName = "/images/NamedReferenceIconImage.png";
	public static final String NotExpressionIconImageName = "/images/NotExpressionIconImage.png";
	public static final String ObjectiveFunctionIconImageName = "/images/ObjectiveFunctionIconImage.png";
	public static final String OneWayIconImageName = "/images/OneWayIconImage.png";
	public static final String ParameterAliasIconImageName = "/images/ParameterAliasIconImage.png";
	public static final String PortIconImageName = "/images/PortIconImage.png";
	public static final String PrecludesIconImageName = "/images/PrecludesIcomImage.png";
	public static final String PrecursorRelationshipIconImageName = "/images/PrecursorRelationshipIconImage.png";
	public static final String PredefinedEventIconImageName = "/images/PredefinedEventIconImage.png";
	public static final String RequirementIconImageName = "/images/RequirementIconImage.png";
	public static final String RequiresIconImageName = "/images/RequiresIcomImage.png";
	public static final String SatisfiesIconImageName = "/images/SatisfiesIconImage.png";
	public static final String ScenarioIconImageName = "/images/ScenarioIconImage.png";
	public static final String SimRunSetIconImageName = "/images/SimRunSetIconImage.png";
	public static final String SimulationRunIconImageName = "/images/SimulationRunIconImage.png";
	public static final String StateIconImageName = "/images/StateIconImage.png";
	public static final String StrategyIconImageName = "/images/StrategyIconImage.png";
	public static final String SummationIconImageName = "/images/SummationIconImage.png";
	public static final String SustainIconImageName = "/images/SustainIconImage.png";
	public static final String SwitchIconImageName = "/images/SwitchIconImage.png";
	public static final String TallyIconImageName = "/images/TallyIconImage.png";
	public static final String TerminalIconImageName = "/images/TerminalIconImage.png";
	public static final String VariableAttributeBindingIconImageName = "/images/VariableAttributeBindingIconImage.png";
	public static final String VariableIconImageName = "/images/VariableIconImage.png";	
}

