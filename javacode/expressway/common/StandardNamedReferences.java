/**
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.common;


public class StandardNamedReferences
{
	public static final String AddressesRequirement = "AddressesRequirement";
	public static final String AddressesRequirementDesc =
		"Defined automatically by each RequirementDomain. The 'to' Node " +
		"is a Strategy that addresses the Requirement defined by the 'from' Node.";
	
	public static final String AddressesStrategy = "AddressesStrategy";
	public static final String AddressesStrategyDesc =
		"Defined automatically by each StrategyDomain. The 'to' Node " +
		"implements or supports the Strategy defined by the 'from' Node.";
	
	public static final String ImplementsClassification = "ImplementsClassification";
	public static final String ImplementsClassificationDesc =
		"Defined automatically by each ClassificationDomain. The 'to' Node " +
		"implements the Classification defined by the 'from' Node.";
}

