/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.common;


public interface TOGAF
{
	void renderOrgActorCatalog();
	void renderDriverGoalObjCatalog();
	void renderRoleCatalog();
	void renderServiceFuncCatalog();
	void renderProcessEventControlProductCatalog();
	void renderContractMeasureCatalog();
	void renderBusinessInteractionMatrix();
	void renderActorRoleMatrix();
	void renderBusinessFootprintDiagram();
	void renderServiceInformationDiagram();
	void renderFuncDecompDiagram();
	void renderProductLifecycleDiagram();
	void renderGoalObjServiceDiagram();
	void renderBusinessUseCaseDiagram();
	void renderOrgDecompDiagram();
	void renderProcessFlowDiagram();
	void renderEventDiagram();
}

