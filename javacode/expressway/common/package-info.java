/**
 * Interface for Expressway server prototype.
 *
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 *
 * @author Cliff Berg
 * @version 0.3.
 * @see <a href="{@docRoot}/Tool%20Architecture.odg">Tool Architecture (Open Office Draw format)</a>.
 */

package expressway.server;

