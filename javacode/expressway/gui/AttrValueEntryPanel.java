/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.help.*;
import generalpurpose.ThrowableUtil;
import javax.swing.*;
import java.lang.reflect.*;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.*;
import java.text.NumberFormat;
import java.util.*;
import java.io.Serializable;
import javax.swing.border.BevelBorder;


/**
 * A Panel for enabling the user to enter or select a value for an Attribute, either
 * for a particular Scenario or for all Scenarios: there is a separate contstructor
 * for each case.
 */
 
public class AttrValueEntryPanel extends JFrame
{
	private static final int FieldHeight = 15;
	private static final int FieldVerticalMargin = 5;
	
	private static final Font AttrNameFont = new Font("Arial", Font.BOLD, 16);
	private static final FontMetrics AttrNameFontMetrics = 
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(AttrNameFont);

	private static final Font ScenarioNameFont = new Font("Arial", Font.BOLD, 16);
	private static final FontMetrics ScenarioNameFontMetrics = 
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(ScenarioNameFont);

	private NumberFormat numberFormat = NumberFormat.getInstance();
	
	private VisualComponent attributedVisual; // may be null
	private AttributeVisual attrVisual;  // may be null
	private Component originator;
	private View view;
	private ScenarioSer scenarioSer;  // may be null
	private ValueControl valueControl = null;  // may be null
	private Serializable attrCurrentValue;
	private Serializable newSelectedAttrValue;

	private JLabel attrNameLabel;
	private HelpButton attrNameLabelHelpButton;
	private JLabel scenarioNameLabel;
	private HelpButton scenarioNameLabelHelpButton;
	private JButton cancelButton;

	private JPanel entryPanel;
	private JLabel entryLabel;
	private JTextField valueEntryField;
	private HelpButton valueEntryFieldHelpButton;
	private JLabel checkboxLabel;
	private JCheckBox treatAsStringCheckBox;
	private HelpButton treatAsStringCheckBoxHelpButton;
	private JButton entryOkButton;
	
	private JLabel orLabel;

	private JPanel choicePanel;
	private JLabel chooseValueLabel;
	private JComboBox tagTypeComboBox;
	private HelpButton tagTypeComboBoxHelpButton;
	private JButton choiceOkButton;
	private JPanel choiceFieldsPanel;
	private JScrollPane choiceFieldsScrollPane;

	
	/**
	 * Allows one to set the value of an Attribute in the context of a Scenario.
	 */
	 
	public AttrValueEntryPanel(ValueControl valueControl,
		AttributeVisual attrVisual, View view, 
		ScenarioSer scenarioSer, Serializable attrCurValue)
	{
		super("Set Value for " + attrVisual.getName() + " in Scenario " + scenarioSer.getName());
		this.valueControl = valueControl;
		this.setAlwaysOnTop(true);
		this.attrVisual = attrVisual;
		this.view = view;
		this.scenarioSer = scenarioSer;
		this.attrCurrentValue = attrCurValue;
		
		init();
	}
	

	/**
	 * Allows one to set the value of an Attribute for all Scenarios.
	 */
	 
	public AttrValueEntryPanel(ValueControl valueControl,
		AttributeVisual attrVisual, View view)
	{
		super("Set Value for " + attrVisual.getName() + " in each Scenario");
		this.valueControl = valueControl;
		this.setAlwaysOnTop(true);
		this.attrVisual = attrVisual;
		this.view = view;
		
		init();
	}
	

	/**
	 * Allows one to set the value of an Attribute for all Scenarios, when there
	 * is no ValueControl.
	 */
	 
	public AttrValueEntryPanel(AttributeVisual attrVisual, View view)
	{
		this(null, attrVisual, view);
	}
	
	
	/**
	 * Allows one to choose an Attribute value, causing the Attribute to be
	 * created automatically.
	 */
	 
	public AttrValueEntryPanel(VisualComponent attributedVisual, View view,
		ScenarioSer scenarioSer)
	{
		super("Choose Value for Attribute to be created for " + attributedVisual.getName());
		this.setAlwaysOnTop(true);
		this.attributedVisual = attributedVisual;
		this.view = view;
		this.scenarioSer = scenarioSer;
		
		init();
	}
	
	
	void init()
	{
		setLayout(null);
		numberFormat.setMaximumFractionDigits(2);
		
		setBackground(java.awt.Color.white);
		setSize(870, 400);
		setLocation(100, 50);
		
		if (attrVisual == null)
		{
			String label = "Attribute for " + attributedVisual.getName();
			add(attrNameLabel = new JLabel(label));
			add(attrNameLabelHelpButton = new HelpButton(label, HelpButton.Size.Small,
				"Enter or choose a value for a new Attribute to be created" +
				" for " + attributedVisual.getName() + "."));
		}
		else
		{
			add(attrNameLabel = new JLabel(attrVisual.getName()));
			add(attrNameLabelHelpButton = new HelpButton(attrVisual.getName(),
				HelpButton.Size.Small, "Enter or choose a value for" +
				" the selected Attribute."));
		}
		
		attrNameLabel.setFont(AttrNameFont);
		attrNameLabel.setSize(AttrNameFontMetrics.stringWidth(attrNameLabel.getText()), 20);
		attrNameLabel.setLocation(10, 10);
		attrNameLabelHelpButton.setLocation(attrNameLabel.getX() + attrNameLabel.getWidth() + 4,
			attrNameLabel.getY());
		
		if (scenarioSer == null)
		{
			add(scenarioNameLabel = new JLabel("<All Scenarios>"));
			add(scenarioNameLabelHelpButton = new HelpButton("<All Scenarios>", 
				HelpButton.Size.Small,
				"The value is set in all Scenarios for the Domain."));
		}
		else
		{
			add(scenarioNameLabel = new JLabel(scenarioSer.getName()));
			add(scenarioNameLabelHelpButton = new HelpButton(scenarioSer.getName(), 
				HelpButton.Size.Small,
				"The value is set on " + attrNameLabel.getText() +
				" in the Scenario '" + scenarioSer.getName() +
				"'."));
		}
		
		scenarioNameLabel.setFont(AttrNameFont);
		scenarioNameLabel.setSize(ScenarioNameFontMetrics.stringWidth(scenarioNameLabel.getText()), 20);
		scenarioNameLabel.setLocation(10, 30);
		scenarioNameLabelHelpButton.setLocation(scenarioNameLabel.getX() +
			scenarioNameLabel.getWidth() + 4, scenarioNameLabel.getY());
		Font font = attrNameLabel.getFont().deriveFont((float)12.0);

		add(cancelButton = new JButton("Cancel"));
		cancelButton.setSize(100, 15);
		cancelButton.setLocation(750, 10);
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				AttrValueEntryPanel.this.dispose();
			}
		});
		
		
		//
		// Create "entry panel" for entering a value.
		//
		
		add(entryPanel = new JPanel());
		entryPanel.setSize(200, 300);
		entryPanel.setLocation(10, 60);
		entryPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		entryPanel.setLayout(null);
		entryPanel.setBackground(java.awt.Color.white);
		
		// Label in front of the entry field.
		entryPanel.add(entryLabel = new JLabel("Set value"));
		entryLabel.setSize(100, 20);
		entryLabel.setLocation(10, 10);
		
		// Field for entering a value, to be parsed or treated literally.
		String val = "";
		if (attrCurrentValue != null)
			try { val = ObjectValueWriterReader.writeObjectAsString(attrCurrentValue); }
			catch (ParameterError pe) { val = "?????"; }
		entryPanel.add(valueEntryField = new JTextField(val));
		entryPanel.add(valueEntryFieldHelpButton = new HelpButton("Entering an Attribute Value",
			HelpButton.Size.Medium, 
			"The value entered will be used to set the Attribute's value."));
		
		valueEntryField.setLocation(80, 10);
		valueEntryField.setColumns(10);
		valueEntryField.setSize(100, 20);
		valueEntryFieldHelpButton.setLocation(valueEntryField.getX() +
			valueEntryField.getWidth() + 4, valueEntryField.getY());
		
		
		// Fill the field with the Attribute current value, if any.
		
		if (attrCurrentValue != null) valueEntryField.setText(attrCurrentValue.toString());
		
		
		// Label for the 'Treat as String' checkbox.
		entryPanel.add(checkboxLabel = new JLabel("Treat as String"));
		checkboxLabel.setLocation(30, 40);
		checkboxLabel.setSize(100, 20);

		// Checkbox for choosing whether to treat the entered value literally.
		entryPanel.add(treatAsStringCheckBox = new JCheckBox());
		entryPanel.add(treatAsStringCheckBoxHelpButton = new HelpButton(
			"Treat As a String", HelpButton.Size.Medium,
			"If checked, the text entered in the" +
			" 'Set Value' field is not parsed and is used literally (as a string)" +
			" to set the value of the Attribute. If <i>not</i> checked, the text" +
			" is parsed and interpreted if possible: if parsing fails, it is" +
			" treated as a string."));
		treatAsStringCheckBox.setLocation(130, 40);
		treatAsStringCheckBox.setSize(25, 20);
		treatAsStringCheckBoxHelpButton.setLocation(treatAsStringCheckBox.getX() +
			treatAsStringCheckBox.getWidth() + 4, treatAsStringCheckBox.getY());
		
		// Button for submitting the entered value to the server.
		entryPanel.add(entryOkButton = new JButton("OK"));
		entryOkButton.setLocation(70, 70);
		entryOkButton.setSize(60, 15);
		entryOkButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String text = valueEntryField.getText();
				Serializable newValue = text;
				if (! treatAsStringCheckBox.isSelected())
				{
					try { newValue = ObjectValueWriterReader.parseObjectFromString(text); }
					catch (ParameterError pe)
					{
						if (JOptionPane.showConfirmDialog(null, 
							"Could not recognize type of '" + text + "'; treat as a String?",
							"Warning", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
						
							return;
					}
				}
				
				// Try to update server.
				try
				{
					// Create Attribute, if necessary.
					
					AttributeSer attrSer = null;
					
					boolean confirm = false;
					
					// 'Inspect' Attributes don't affect model behavior, so there
					// is no need to delete SimulationRuns.
					if ((newValue != null) && (newValue instanceof Types.Inspect))
						confirm = true;
					
					String scenarioId = (scenarioSer == null? null : scenarioSer.getNodeId());
					
					if (attrVisual == null)  // Create Attribute and set its value.
					{
						try { attrSer = view.getModelEngine().createAttribute(
								confirm, attributedVisual.getNodeId(), newValue,
								scenarioId, 10.0, 10.0); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								
								attrSer = view.getModelEngine().createAttribute(
									true, attributedVisual.getNodeId(), newValue,
									scenarioId, 10.0, 10.0);
						}
					}
					else  // Set value of existing Attribute.
					{
						setValue(newValue);
					}
				}
				catch (Exception ex)
				{
					JOptionPane.showMessageDialog(AttrValueEntryPanel.this, ex); return;
				}
				
				AttrValueEntryPanel.this.dispose();
			}
		});
		
		
		// Create "OR" Label, to be positioned between the two panels.
		
		add(orLabel = new JLabel("OR"));
		orLabel.setLocation(211, 100);
		orLabel.setSize(20, 20);
		
		
		//
		// Create "choice panel" for selecting a value.
		//
		
		add(choicePanel = new JPanel());
		choicePanel.setSize(630, 300);
		choicePanel.setLocation(230, 60);
		choicePanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		choicePanel.setLayout(null);
		choicePanel.setBackground(java.awt.Color.white);
		
		// Label in front of the ComboBox that contains the choices.
		choicePanel.add(chooseValueLabel = new JLabel("Choose a tag"));
		chooseValueLabel.setSize(100, 20);
		chooseValueLabel.setLocation(10, 10);
		
		JLabel fieldsLabel;
		HelpButton fieldsLabelHelpButton;
		choicePanel.add(fieldsLabel = new JLabel("Fields:"));
		choicePanel.add(fieldsLabelHelpButton = new HelpButton(
			"Entering Fields", HelpButton.Size.Medium,
			"Some of the pre-defined Attribute 'tag' types have field values." +
			" You can accept the default values or modify them. See" +
			HelpWindow.createHref("Tag Values") + "."));
		fieldsLabel.setLocation(20, 45);
		fieldsLabel.setSize(60, 15);
		choiceFieldsPanel = new JPanel();
		choiceFieldsPanel.setLayout(new GridLayout(0, 2));
		choiceFieldsScrollPane = new JScrollPane(choiceFieldsPanel);
		choiceFieldsScrollPane.setSize(400, 40);
		choiceFieldsScrollPane.setPreferredSize(new Dimension(100, 40));
		choicePanel.add(choiceFieldsScrollPane);
		choiceFieldsScrollPane.setLocation(fieldsLabel.getWidth() + 20, 40);
		fieldsLabelHelpButton.setLocation(choiceFieldsScrollPane.getX() +
			choiceFieldsScrollPane.getWidth() + 10, fieldsLabel.getY());
		
		
		// ComboBox for presenting the choices, based on the types defined in
		// the expressway.common.Types class.
		List<Class> classes = 
			getNestedClassesRecursive(expressway.common.Types.class, new Vector<Class>());
		choicePanel.add(tagTypeComboBox = 
			new JComboBox(classes.toArray(new Class[classes.size()])));
		tagTypeComboBox.insertItemAt("(none)", 0);		
		tagTypeComboBox.setLocation(110, 10);
		tagTypeComboBox.setSize(500, 20);

		choicePanel.add(tagTypeComboBoxHelpButton = new HelpButton("Choose a tag",
			HelpButton.Size.Medium, "Select a " +
			HelpWindow.createHref("Tag Values", "Tag Value") +
			" for the Attribute's value."));
		tagTypeComboBoxHelpButton.setLocation(tagTypeComboBox.getX() +
			tagTypeComboBox.getWidth() + 4, tagTypeComboBox.getY());
		
		int noOfFields = 0;
		
		tagTypeComboBox.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				newSelectedAttrValue = null;
				
				if (tagTypeComboBox.getSelectedIndex() <= 0) return;

				Object selectedItem = tagTypeComboBox.getSelectedItem();
				if (selectedItem == null) return;
				
				if (! (selectedItem instanceof Class))
				{
					newSelectedAttrValue = null;
					return;
				}
				
				Class selectedClass = (Class)selectedItem;

				try
				{
					if ((attrCurrentValue != null) && selectedItem.equals(attrCurrentValue.getClass()))
						newSelectedAttrValue = attrCurrentValue;
					else
						newSelectedAttrValue = (Serializable)(selectedClass.newInstance());
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
					throw new RuntimeException("Internal error", ex);
				}
				
					
				// Add an edit control for each Field.
		
				removeFieldEditors();
	
				Field[] fields = selectedClass.getFields();
				
				for (Field field : fields)
				{
					Serializable initFieldValue = null;
					
					try
					{
						if (attrCurrentValue == null)
							initFieldValue = (Serializable)(field.get(newSelectedAttrValue));
						else if (selectedClass.isAssignableFrom(attrCurrentValue.getClass()))
							initFieldValue = (Serializable)(field.get(attrCurrentValue));
						else
							initFieldValue = (Serializable)(field.get(newSelectedAttrValue));
					}
					catch (IllegalAccessException ex) { throw new RuntimeException(ex); }
					
					addFieldEditor(field, initFieldValue);
				}
			}
		});
			
			
		// If the Attribute current value is not null, select the appropriate
		// line of the ComboBox.
		
		if (attrCurrentValue != null)
		{
			if (attrCurrentValue instanceof Class)
				tagTypeComboBox.setSelectedItem(attrCurrentValue);
			else
				tagTypeComboBox.setSelectedItem(attrCurrentValue.getClass());
		}

		
		// Button for submitting the user's choice to the server.
		choicePanel.add(choiceOkButton = new JButton("OK"));
		choiceOkButton.setLocation(70, 70 + 30 + choiceFieldsScrollPane.getHeight());
		choiceOkButton.setSize(60, 15);
		choiceOkButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Object selItem = tagTypeComboBox.getSelectedItem();
				if (! (selItem instanceof Class))  // none selected
				{
					JOptionPane.showMessageDialog(AttrValueEntryPanel.this,
						"You must select an item from the drop-down list",
						"Error", JOptionPane.ERROR_MESSAGE); return;
				}
				
				Class c = (Class)selItem;
				
				
				// Set fields of new value.
				
				Field[] fields = c.getFields();
				for (Field field : fields)
				{
					Class fieldType = field.getType();
					
					JTextField textField = getTextField(field.getName());
					if (textField == null) throw new RuntimeException(
						"TextField with name " + field.getName() + " not found");
					
					String text = textField.getText();
					Serializable fieldValue = text;
					try
					{
						fieldValue = ObjectValueWriterReader.parseObjectFromString(text);
					}
					catch (ParameterError pe)
					{
						if (! String.class.isAssignableFrom(fieldType))  // a String is not expected,
							if (JOptionPane.showConfirmDialog(null, 
								"Could not recognized '" + text + "'; treat as a String?",
								"Warning", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
								
								return;
					}
						
					if (AttrValueEntryPanel.this.attrCurrentValue == null)
						if ((fieldValue == null) || fieldValue.equals(""))
							continue;  // allow the default value to be applied.

					try
					{
						Serializable convertedValue = null;
						try
						{
							if (fieldValue != null)
								convertedValue = convert(fieldValue, fieldType);
							
							field.set(newSelectedAttrValue, convertedValue);
						}
						catch (ParameterError pe)
						{
							ErrorDialog.showReportableDialog(AttrValueEntryPanel.this,
								"Cannot convert '" + text + "' to an allowed type",
								"Error Reading Attribute Field", pe);
							return;
						}
					}
					catch (IllegalAccessException iaex) { throw new RuntimeException(iaex); }
				}
				
					
				// Try to update server.
				try
				{
					AttributeSer attrSer = null;
					
					boolean confirm = false;
					
					// 'Inspect' Attributes don't affect model behavior, so there
					// is no need to delete SimulationRuns.
					if (newSelectedAttrValue instanceof Types.Inspect) confirm = true;
					
					String scenarioId = (scenarioSer == null? null : scenarioSer.getNodeId());
					
					if (attrVisual == null)
					{
						try { attrSer = view.getModelEngine().createAttribute(
								confirm, attributedVisual.getNodeId(), newSelectedAttrValue,
									scenarioId, 10.0, 10.0); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								
								attrSer = view.getModelEngine().createAttribute(
									true, attributedVisual.getNodeId(), newSelectedAttrValue,
										scenarioId, 10.0, 10.0);
						}
					}
					else
					{
						setValue(newSelectedAttrValue);
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(AttrValueEntryPanel.this, ex); return;
				}

				AttrValueEntryPanel.this.dispose();
			}
		});
	}
	
	
	protected void setValue(Serializable newValue)
	throws
		Exception
	{
		if (valueControl == null)  // set directly on server
		{
			try
			{
				view.getModelEngine().setAttributeValue(false, 
					attrVisual.getNodeId(), null, newValue);
			}
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(null, w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					view.getModelEngine().setAttributeValue(true, 
						attrVisual.getNodeId(), null, newValue);
			}
		}
		else
			setValueInControl(newValue);
	}
	
	
	protected void setValueInControl(Serializable newValue)
	throws
		Exception
	{
		valueControl.setValue(newValue);
	}
	
	
	protected Serializable convert(Serializable from, Class toType)
	throws
		ParameterError
	{
		if (toType.isInstance(from)) return from;
		else if (String.class.isAssignableFrom(toType)) return from.toString();
		else throw new ParameterError("Value '" + from.toString() + 
			"' is not assignment compatible with type " + toType.getName());
	}
	
	
	private Map<String, JTextField> editFields = new HashMap<String, JTextField>();
	
	
	void addFieldEditor(Field field, Serializable initValue)
	{
		JTextField dataField;
		
		GridLayout layout = (GridLayout)(choiceFieldsPanel.getLayout());
		
		layout.setColumns(2);
		layout.setRows(layout.getRows() + 1);
		
		Dimension size = new Dimension(choiceFieldsScrollPane.getWidth(),
			layout.getRows() * (FieldHeight + FieldVerticalMargin));
			
		choiceFieldsPanel.setSize(size);
		choiceFieldsPanel.setPreferredSize(size);
		choiceFieldsPanel.setMinimumSize(size);
		
		choiceFieldsPanel.add(new JLabel(field.getName()));
		choiceFieldsPanel.add(dataField = new JTextField(
			(initValue == null? "" : initValue.toString())));
		
		editFields.put(field.getName(), dataField);
	}
	
	
	void removeFieldEditors()
	{
		editFields.clear();
		choiceFieldsPanel.removeAll();
		choiceFieldsPanel.setSize(choiceFieldsScrollPane.getWidth(), 
			choiceFieldsScrollPane.getHeight());
		
		GridLayout layout = (GridLayout)(choiceFieldsPanel.getLayout());
		layout.setColumns(2);
		layout.setRows(0);
	}
	
	
	JTextField getTextField(String fieldName)
	{
		return editFields.get(fieldName);
	}
	
	
	/**
	 * Recursively descend the specified Class, adding each nested class to
	 * the List of Classes.
	 */
	 
	static List<Class> getNestedClassesRecursive(Class root, List<Class> classes)
	{
		Class[] childClasses = root.getDeclaredClasses();
		for (Class c : childClasses) classes.add(c);
		for (Class childClass : childClasses)
		{
			getNestedClassesRecursive(childClass, classes);
		}
		
		return classes;
	}
}

