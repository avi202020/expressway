/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import java.io.Serializable;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import javax.swing.event.MouseInputAdapter;


public class AttributeDefaultValueControl extends ValueControl
{
	private static final int AttributeControlWidth = 100;
	private static final int AttributeControlHeight = 20;


	public AttributeDefaultValueControl(ValueControlFactory factory, VisibilityControl visControl,
		AttributeVisual attrVis, Serializable initValue, boolean editable)
	{
		super(factory, visControl, attrVis, initValue, editable);
		addMouseListener(new ChooseTagPopupListener());
	}


	public AttributeDefaultValueControl(ValueControlFactory factory,
		AttributeVisual attrVis, Serializable initValue, boolean editable)
	{
		super(factory, attrVis, initValue, editable);
	}


	public AttributeSer getAttributeSer() { return (AttributeSer)(getVisual().getNodeSer()); }
	
	
	public AttributeVisual getAttributeVisual() { return (AttributeVisual)(getVisual()); }


	public int getValueControlWidth() { return AttributeControlWidth; }
	
	
	public int getValueControlHeight() { return AttributeControlHeight; }
	
	
	protected ModelEngineRMI getModelEngine() { return getAttributeVisual().getModelEngine(); }
	
	
	protected ScenarioSer getScenarioSer() { return null; }
	
	
	public void updateServer()
	{
		try
		{
			try { getModelEngine().setAttributeDefaultValue(false, 
					getAttributeSer().getNodeId(), getValue()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(null, w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
				{
					getModelEngine().setAttributeDefaultValue(true, 
						getAttributeSer().getNodeId(), getValue());
				}
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(null, ex);;
		}
	}


	/** ************************************************************************
	 * Handle a request to display an AttrValueEntryPanel for editing an Attribute
	 * value (in the context of a Scenario).
	 */
	 
	private class ChooseTagPopupListener extends MouseInputAdapter
	{
		public void mouseReleased(MouseEvent e)
		{
			if (! ((e.getButton() == MouseEvent.BUTTON2)
				|| (e.getButton() == MouseEvent.BUTTON3)
				|| ((e.getButton() == MouseEvent.BUTTON1) && e.isControlDown())
			)) return;
			
			Component eventSource = e.getComponent();
			if (! (eventSource instanceof AttributeValueControl))
				throw new RuntimeException(
				"Event source expected to be an AttributeValueControl but it is not");
			
			AttributeValueControl control = (AttributeValueControl)eventSource;
			AttributeVisual attrVisual = control.getAttributeVisual();
			
			Serializable attrValue;
			try { attrValue = control.getValue(); }
			catch (ParameterError pe)
			{
				ErrorDialog.showReportableDialog(AttributeDefaultValueControl.this,
					"Error parsing value", pe);
				return;
			}
			
			AttrValueEntryPanel panel = new AttrValueEntryPanel(
				AttributeDefaultValueControl.this,
				attrVisual, 
				getViewPanel(), 
				getScenarioSer(),
				attrValue
				);
	
			int x = 0;
			int y = 0;
			Object source = e.getSource();
			if (source instanceof Component)
			{
				Component comp = (Component)source;
				x = comp.getX();
				y = comp.getY();
			}
				
			panel.setLocation(x, y);
			panel.show();
		}
	}
}

