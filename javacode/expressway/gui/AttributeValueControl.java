/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import java.io.Serializable;
import java.awt.Container;
import javax.swing.JOptionPane;


public class AttributeValueControl extends AttributeDefaultValueControl
{
	private ScenarioVisual scenarioVisual;
	
	
	public AttributeValueControl(ScenarioVisual scenarioVisual,
		ValueControlFactory factory, VisibilityControl visControl,
		AttributeVisual attrVis, Serializable initValue, boolean editable)
	{
		super(factory, visControl, attrVis, initValue, editable);
		this.scenarioVisual = scenarioVisual;
	}


	public AttributeValueControl(ScenarioVisual scenarioVisual,
		ValueControlFactory factory,
		AttributeVisual attrVis, Serializable initValue, boolean editable)
	{
		super(factory, attrVis, initValue, editable);
		this.scenarioVisual = scenarioVisual;
	}


	protected ScenarioSer getScenarioSer() { return (ScenarioSer)(scenarioVisual.getNodeSer()); }
	
	
	public void updateServer()
	{
		try
		{
			String scenarioId = scenarioVisual.getNodeId();
			try { getModelEngine().setAttributeValue(false, 
					getAttributeSer().getNodeId(), scenarioId, getValue()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog(null, w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
				{
					getModelEngine().setAttributeValue(true, 
						getAttributeSer().getNodeId(), scenarioId, getValue());
				}
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(
				((View)(getVisual().getClientView())).getComponent(), ex);;
		}
	}
}

