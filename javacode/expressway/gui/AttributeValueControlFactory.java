/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.gui.ValueControl.*;
import expressway.ser.*;
import java.io.Serializable;


public class AttributeValueControlFactory implements ValueControlFactory
{
	private ControlPanel controlPanel;
	private ScenarioVisual scenarioVisual;
	
	
	public AttributeValueControlFactory(ControlPanel controlPanel, ScenarioVisual scenVis)
	{
		this.controlPanel = controlPanel;
		this.scenarioVisual = scenVis;
	}
	
	
	public boolean shouldHaveValueControl(NodeSer nodeSer)
	{
		try
		{
			return AttributeVisual.class.isAssignableFrom(getNodeView().getVisualClass(nodeSer));
		}
		catch (CannotBeInstantiated ex) { return false; }
	}
	
	public Class getValueControlClass() { return AttributeValueControl.class; }
	
	
	public ValueControl makeValueControl(VisualComponent vis, 
		VisibilityControl visControl)
	{
		AttributeVisual attrVis = (AttributeVisual)vis;
		AttributeSer attrSer = (AttributeSer)(vis.getNodeSer());
		
		
		// Get the current value of the Attribute in the context of the Scenario.
		Serializable initValue;
		try { initValue = controlPanel.getViewPanel().getModelEngine().getAttributeValueById(
			attrSer.getNodeId(), scenarioVisual.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(controlPanel.getJComponent(), ex);
			initValue = attrSer.getDefaultValue();
		}
		
		return new AttributeValueControl(scenarioVisual, this, visControl, attrVis, initValue, true);
	}
	
	public ValueControl makeValueControl(NodeSer nodeSer, VisibilityControl visControl)
	{
		if (! (nodeSer instanceof AttributeSer))
			throw new RuntimeException("Expected an AttributeSer");
		Serializable initValue = ((AttributeSer)nodeSer).getDefaultValue();
		VisualComponent nodeVis;
		try { nodeVis = getNodeView().identifyVisualComponent(nodeSer.getNodeId()); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		return new AttributeValueControl(
			scenarioVisual,
			this,
			visControl, 
			(AttributeVisual)nodeVis,
			initValue,
			true);
	}
	
	public void unmakeValueControl(ValueControl vc)
	{
	}
		
	
	public ViewPanelBase getViewPanel() { return (ViewPanelBase)(scenarioVisual.getClientView()); }
	
	
	public ControlPanel getControlPanel() { return controlPanel; }
	
	
	protected ClientView getClientView() { return scenarioVisual.getClientView(); }
	
	
	protected NodeView getNodeView() { return (NodeView)(scenarioVisual.getClientView()); }
}

