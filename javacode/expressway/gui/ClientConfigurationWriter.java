/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.VisualComponent.*;
import java.util.*;
import java.io.*;
import javax.swing.JFrame;


/**
 * See slide "Client Configuration File".
 */
 
public class ClientConfigurationWriter
{
	private ClientMainApplication mainApp;
	private PanelManager panelManager;
	private ViewFactory viewFactory;
	
	
	public ClientConfigurationWriter(ClientMainApplication mainApp,
		PanelManager panelManager, ViewFactory viewFactory)
	{
		this.mainApp = mainApp;
		this.panelManager = panelManager;
		this.viewFactory = viewFactory;
	}
	
	
	public static void writeConfiguration(ClientMainApplication mainApp,
		PanelManager panelManager, ViewFactory viewFactory)
	{
		ClientConfigurationWriter ccWriter = new ClientConfigurationWriter(
			mainApp, panelManager, viewFactory);
		
		ccWriter.writeConfiguration();
	}
	
	
	public void writeConfiguration()
	{
		String home = System.getProperty("user.home");
		File configFile = new File(home, Constants.ConfigFileName);
		GlobalConsole.println("Saving configuration to " + configFile.getAbsolutePath());
		
		PrintWriter pw;
		try { pw = new PrintWriter(configFile); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); return; }
		
		pw.println("<expressway_client_configuration server=\"" + 
			mainApp.getModelEngineNetworkPath() + "\">");
		
		List<File> xmlDirs = mainApp.getXMLDirs();
		for (File d : xmlDirs)
		{
			pw.println("\t<xml dir=\"" + d.toString() + "\" />");
		}
		
		List<File> jarDirs = mainApp.getJARDirs();
		for (File d : jarDirs)
		{
			pw.println("\t<jar dir=\"" + d.toString() + "\" />");
		}
		
		JFrame mainFrame = panelManager.getFrame();
		pw.println("\t<main_window width=\"" + mainFrame.getWidth() + "\" " +
			"height=\"" + mainFrame.getHeight() + "\" " +
			"x=\"" + mainFrame.getX() + "\" y=\"" + mainFrame.getY() + "\">");
		
		List<NodeView> views = panelManager.getNodeViews();
		for (NodeView view : views)
		{
			if (! (view instanceof ModelDomainVisual)) continue;
			ModelDomainVisual dVis = (ModelDomainVisual)view;
			
			pw.println("\t\t<model_scenario_view element=\"" + dVis.getName() + "\">");
			
			try
			{
				List<ModelScenarioVisual> modelScenarioVisuals = dVis.getModelScenarioVisuals();
				for (ModelScenarioVisual scenario : modelScenarioVisuals)
				{
					pw.println("\t\t\tcenario name=\"" + scenario.getName() + "\"/>");
				}
			}
			catch (RuntimeException ex) { GlobalConsole.printStackTrace(ex); }
			
			pw.println("\t\t</model_scenario_view>");
		}
		
		pw.println("\t</main_window>");
		
		pw.println("</expressway_client_configuration>");
		pw.flush();
		pw.close();
	}
}

