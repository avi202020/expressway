/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.gui.VisibilityControl.*;
import expressway.gui.ValueControl.*;
import java.util.Set;
import java.io.Serializable;
import java.awt.event.ComponentListener;


/**
 * A transparent Panel that overlays a View and which contains Controls for
 * viewing Attribute values and other aspects of the View without disturbing
 * the natural layout of the View's elements. A ControlPanel is assumed to
 * extend JComponent.
 *
 * See the slide "Viewing Child Attributes".
 * See the slide "Structure of a ModelScenarioViewPanel".
 */
 
public interface ControlPanel
	extends SwingComponent  // thus, implementation must extends JComponent
{
	ViewPanelBase getViewPanel();
	
	
	ControlPanel postConstructionSetup();
	
	
	/**
	 * Show (or hide) all of the VisibilityControls, Visuals and ValueControls 
	 * owned by this ControlPanel pertaining to the specified Control type, and
	 * calls the <VisibilityControl>.showValueControls() method, which shows the
	 * ValueControls if the VisibilityControl's state is set to show its controls.
	 * If a Visual must be added to a ControlPanel, this method calls addNode(String)
	 * on the Control Panel. Hides Controls that are not of the specified type
	 * (and their VisibilityControls as well).
	 */
	 
	void showControls(Class controlType, boolean show);
		
		
	Set<ValueControl> getValueControls(Class valueControlType);
	
	
	ValueControlFactory getValueControlFactory(Class controlType);


	VisibilityControlFactory getVisibilityControlFactory(Class controlType);
	
	
	/**
	 * Create a ValueControl in this ControlPanel to depict the
	 * value of the specified Visual. 'visControl' may be null.
	 */
	 
	ValueControl createValueControl(ValueControlFactory controlFactory,
		VisibilityControl visControl /* may be null */, VisualComponent visual);
	
	
	void uncreateValueControl(ValueControl vc);
	
	
	void unmakeVisibilityControl(VisibilityControl visc);
	
	
	void setComponentListener(ComponentListener listener);
	ComponentListener getComponentListener();

	
	/*
	 * If any Attribute ValueControls are displayed for the specified Node, obtain
	 * the current values of those Attributes from the server and display the
	 * new values.
	 *
	 
	void refreshAttrControlValuesForNode(String nodeId);*/
	
	
	/**
	 * Adds a Visual to the ControlPanel. Calls
	 *	<VisualComponentFactory>.makeGraphicVisual(...)
	 * to create the Visual (because Controls are always graphical, even if they
	 * are overlaid on a Tabular View). Adds any Controls that should be
	 * visible for the Visual.
	 */
	 
	VisualComponent addNode(String nodeId, VisibilityControl vc)
	throws
		Exception;
	
	
	/**
	 * Undoes the action of addNode.
	 */
	 
	void removeVisual(VisualComponent visual);
	
	
	/**
	 * FInds the Visual, if any, in the ControlPanel, that depicts the specified Node.
	 */
	 
	VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError;
	
	
	/**
	 * Save whether this ControlPanel's ValueControls, of the specified type, for
	 * the specified Visual are visible or not.
	 */
	 
	void rememberVisibilityControlStateForVisual(VisualComponent visual, 
		Class valueControlClass, boolean show);
	
	
	void rememberValueControlStateForVisual(VisualComponent visual, Class valueControlClass,
		Serializable value);
	
	
	void rememberValueControlTypeForVisual(VisualComponent visual,
		Class valueControlClass, Class valueType);
	
	
	Class retrieveRememberedTypeForVisual(VisualComponent visual,
		Class valueControlClass);
	
	
	void clearValueControlTypeForVisual(VisualComponent visual,
		Class valueControlClass);
	
	
	/**
	 * Return true if this ControlPanel contains values entered into ValueControls
	 * that have not yet been saved to the server.
	 */
	 
	boolean containsUnsavedFieldValues();
	
	
	/**
	 * Remove all values copied and cached from ValueControls in this ControlPanel.
	 */
	 
	void clearFieldValues();
	
	
	void clearValueFieldTypes();
	
	
	/**
	 * Return whether this ControlPanel's ValueControls, of the specified type, for
	 * the specified Visual should be visible or not.
	 */
	 
	boolean controlsForVisualAreVisible(VisualComponent visual, Class valueControlClass);
		
		
	VisibilityControl getVisibilityControl(VisualComponent visual, Class visControlType);
	
	
	VisibilityControl getVisibilityControl(String visualId, Class visControlType);
	
	
	/** ********************************************************************
	 * Remove all VisibilityControls for the specified Visual.
	 * Also remove any Visuals for the Visual and their
	 * associated ValueControls; any unsaved data in the value fields will be lost. 
	 */
	 
	void removeControls(VisualComponent visual);
	
	
	void removeControls(String nodeId);
	
	
	void removeAllControls();
	
	
	/** Same as method 'setLocation(VisualComponent)' in ScenarioVisual. */
	void setLocation(VisualComponent visual);
	
	
	/** ********************************************************************
	 * Perform any required updates to Components in this ControlPanel in
	 * response to the movement of a Visual in the Domain View underneath.
	 */
	 
	void notifyVisualMoved(VisualComponent visual);
	
	
	void notifyVisualRemoved(VisualComponent visual);
	
	
	void setInfoStrip(InfoStrip infoStrip);
	
	
	InfoStrip getInfoStrip();
	
	
	ScenarioSelector getScenarioSelector();


	boolean attributeControlsAreVisible();
}

