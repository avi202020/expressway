/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.ModelAPITypes.*;
import java.awt.Font;


/**
 * View space constants. These are not used to determine Model Element dimensions.
 */
 
public class DisplayConstants implements ViewConstants
{
	public static final int PixelsPerInch = 92;  // Mac
	public static final int PixelsPerCm = (int)(PixelsPerInch / 2.54);
	public static final int PointsPerInch = 72;
	
	public static final int DefaultNormalBorderThickness = 2;  // pixels
	public static final int DefaultTopBorderThickness = 10;  // pixels
	//public static final int DefaultHighlightedBorderThickness = 0;
	public static final int DefaultIconWidth = 20;  // pixels
	public static final int DefaultIconHeight = 20;  // pixels
	public static final int DefaultIconBorderFontPointSize = 8;  // points
	public static final int DefaultConduitThickness = 2;  // pixels; should be an int multiple of 2.
	public static final int DefaultPortDiameter = 16;  // pixels; should be an int multiple of 2.
	//public static final int DefaultAttributeHeight = 20;

	private int normalBorderThickness = DefaultNormalBorderThickness;
	private int topBorderThickness = DefaultTopBorderThickness;
	private int iconWidth = DefaultIconWidth;
	private int iconHeight = DefaultIconHeight;
	private int iconBorderFontPointSize = DefaultIconBorderFontPointSize;
	private int conduitThickness = DefaultConduitThickness;  // Should be an int multiple of 2.
	private int portDiameter = DefaultPortDiameter;  // Should be an int multiple of 2.
	//private int attributeHeight = DefaultAttributeHeight;
		
	private int borderFontPointSize = PointsPerInch * topBorderThickness / PixelsPerInch;

	/** Font to use for the title on any Component drawn within this View. */
	private Font componentTitleFont = new Font(null, Font.PLAIN, borderFontPointSize);
	
	public int getPixelsPerInch() { return PixelsPerInch; }
	public int getPixelsPerCm() { return PixelsPerCm; }
	public int getNormalBorderThickness() { return normalBorderThickness; }
	public int getTopBorderThickness() { return topBorderThickness; }
	public int getIconWidth() { return iconWidth; }
	public int getIconHeight() { return iconHeight; }
	public int getBorderFontPointSize() { return borderFontPointSize; }
	public int getConduitThickness() { return conduitThickness; }
	public int getPortDiameter() { return portDiameter; }
	//public int getAttributeHeight() { return attributeHeight; }
	
	public void setNormalBorderThickness(int x) { this.normalBorderThickness = x; }
	public void setTopBorderThickness(int x) { this.topBorderThickness = x; }
	public void setIconWidth(int x) { this.iconWidth = x; }
	public void setIconHeight(int x) {  this.iconHeight = x; }
	public void setBorderFontPointSize(int x) { this.borderFontPointSize = x; }
	
	public void setConduitThickness(int x)
	throws
		ParameterError
	{
		if ((x+1)/2 != x/2) throw new ParameterError(
			"Conduit thickness must be a multiple of 2");
		
		this.conduitThickness = x;
	}
		
	public void setPortDiameter(int x)
	throws
		ParameterError
	{
		if ((x+1)/2 != x/2) throw new ParameterError(
			"Port diameter must be a multiple of 2");
		
		this.portDiameter = x;
	}
	
	//public void setAttributeHeight(int h) { this.attributeHeight = h; }
	
	public Font getComponentTitleFont() { return componentTitleFont; }
}

