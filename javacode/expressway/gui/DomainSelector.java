/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.help.*;
import expressway.gui.PanelManager.*;
import generalpurpose.ThrowableUtil;
import expressway.gui.modeldomain.EventHistoryPanel;
import expressway.gui.domainlist.XMLEditPanel;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Vector;
import java.util.Date;
import java.util.Enumeration;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTree;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.MenuElement;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.tree.*;
import javax.swing.event.*;


/**
 * Display a tree of the Domains on the server, and the children of those Domains.
 *
 * The 'enableDisplayOf' and 'disableDisplayOf' methods pertain to whether an Object
 * (typically a VisualComponent) can be added to the tree. These methods operate
 * in an additive manner: the sequence in which they are called affects the result.
 * These methods act on a set of allowable object types to display, so that, for
 * example, calling disableDisplayOfNodes(A.class) and then calling
 * enableDisplayOfNodes(B.class) will allow the display of only those
 * nodes that are not class A or that are of class B. The set of allowable nodes
 * begins with all nodes allowed. The method 'resetAllowableSelections' restores
 * the initial state, which is to allow any node to be displayed.
 *
 * The 'enableSelectionOf' and 'disableSelectionOf' methods pertain to the
 * selectability of tree nodes, and operate in the same manner as the 'enableDisplayOf'
 * and 'disableDisplayOf' methods.
 */
 
public class DomainSelector extends ViewPanelBase
{
  /* ***************************************************************************/
  /* Interface for using this class. */


	public static DomainSelector createDomainSelector(ModelEngineRMI me,
		PanelManager panelManager, ViewFactory viewFactory, String name)
	throws
		Exception
	{
		DomainSelector selector = new DomainSelector(me, panelManager,
			viewFactory, name);
		
		//try { selector.postConstructionSetup(); }
		//catch (Exception ex) { GlobalConsole.printStackTrace(ex); }

		return selector;
	}
	
	
	public void disableDisplayOfNodes(Class c)
	{
		displayRules.add(new ClassNotAllowedRule(c));
	}
	
	
	public void disableDisplayOfNodes(String nodeId)
	{
		displayRules.add(new IdNotAllowedRule(nodeId));
	}


	public void enableDisplayOfNodes(Class c)
	{
		displayRules.add(new ClassAllowedRule(c));
	}
	
	
	public void enableDisplayOfNodes(String nodeId)
	{
		displayRules.add(new IdAllowedRule(nodeId));
	}
	
	
	public void resetAllowableDisplays()
	{
		displayRules.clear();
	}
	
	
	public void disableSelectionOfNodes(Class c)
	{
		selectionRules.add(new ClassNotAllowedRule(c));
	}
	
	
	public void disableSelectionOfNodes(String nodeId)
	{
		selectionRules.add(new IdNotAllowedRule(nodeId));
	}
	
	
	public void enableSelectionOfNodes(Class c)
	{
		selectionRules.add(new ClassAllowedRule(c));
	}
	
	
	public void enableSelectionOfNodes(String nodeId)
	{
		selectionRules.add(new IdAllowedRule(nodeId));
	}
	
	
	public void resetAllowableSelections()
	{
		selectionRules.clear();
	}
	
	
	/**
	 * Set a Listener to be notified whenever a tree node is selected.
	 */
	 
	public void setSelectionListener(NodeSelectionListener listener)
	{
		this.selectionListener = listener;
	}
	
	
	public interface NodeSelectionListener
	{
		void visualSelected(VisualComponent visual);
		
		void hostSelected(MutableTreeNode root);
	}
	
	
  /* ***************************************************************************/
  /* Internal implementation */
	
	
  /* Fields */
	

	private DomainListJTree tree;
	private DefaultTreeModel treeModel;
	private TreeNodeFactory visualFactory;
	private NodeSelectionListener selectionListener;
	private List<Rule> displayRules = new Vector<Rule>();
	private List<Rule> selectionRules = new Vector<Rule>();
	private boolean showNamedRefs = true;
	
	// For reporting of State history for SimulationRuns.
	private static final Class[] StateTagsToReportOn =
	{
		expressway.common.Types.Inspect.ShowHistory.class
	};
	
	
  /* Constructors */

	
	private DomainSelector(ModelEngineRMI me, PanelManager panelManager,
		ViewFactory viewFactory, String n)
	throws
		Exception
	{
		super(me, panelManager, viewFactory, n, true);
		this.setBackground(java.awt.Color.white);
		
		DefaultMutableTreeNode rootNode = new HostTreeNode(
			panelManager.getClientMainApplication().getModelEngineNetworkPath());
			// To do: give this a context menu that provides menu items for creating
			// Domains. See 'createDomainButton' in DomainListPanel.
		
		this.visualFactory = new TreeNodeFactory();
		
		this.tree = new DomainListJTree(treeModel = new DefaultTreeModel(rootNode));
		this.tree.setSelectionModel(new DefaultTreeSelectionModel()
		{
			protected boolean canPathsBeAdded(TreePath[] paths)
			{
				if (paths == null) return true;
				
				boolean canBeSelected = true;
				for (TreePath path : paths)
				{
					Object obj = path.getLastPathComponent();
					for (Rule rule : selectionRules)
					{
						if (canBeSelected)
						{
							if (rule.disallowed(obj)) canBeSelected = false;
						}
						else
						{
							if (rule.allowed(obj)) canBeSelected = true;
						}
					}
					
					// One disallowed path disallows the array of paths.
					if (! canBeSelected) return false;
				}
				
				return canBeSelected;
			}
		});
		
		this.tree.addTreeSelectionListener(new TreeSelectionListener()
		{
			public void valueChanged(TreeSelectionEvent e)
			{
				TreePath path = e.getNewLeadSelectionPath();
				if (path == null) return;
				Object obj = path.getLastPathComponent();
				
				MutableTreeNode root = (MutableTreeNode)(treeModel.getRoot());
				if (obj == root) selectionListener.hostSelected(root);
				if (! (obj instanceof VisualComponent)) return;
				
				VisualComponent visual = (VisualComponent)obj;
				selectionListener.visualSelected(visual);
			}
		});
		
		this.tree.addMouseListener(new MouseAdapter()
		{
			public void mouseReleased(MouseEvent e)
			{
				TreePath path = tree.getPathForLocation(e.getX(), e.getY());
				if (path == null) return;
				
				Object[] pathParts = path.getPath();
				
				if (pathParts.length == 0) return;
				Object o = pathParts[pathParts.length-1];
				if (! (o instanceof SelectorVisualTreeNode)) return;
				SelectorVisualTreeNode visual = (SelectorVisualTreeNode)o;
				
				String nodeId = visual.getNodeSer().getNodeId();
					
				if ( // right-mouse
					(e.getButton() == MouseEvent.BUTTON2) ||
					(e.getButton() == MouseEvent.BUTTON3) ||
					((e.getButton() == MouseEvent.BUTTON1) && e.isControlDown())
				)
				{
					JPopupMenu popup = new JPopupMenu();
					addContextMenuItems(popup, visual);
					popup.show(tree, e.getX(), e.getY());
				}
				else // left mouse; see if double-click
					if ((path != null) && (e.getClickCount() == 2)) try
				{
					// Invoke the "open" operation on the selected Visual.
					
					visual.performOpen();
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				}
			}
		});
		
		treeModel.addTreeModelListener(new TreeModelListener()
		{
			public void treeNodesChanged(TreeModelEvent e) {}
			public void treeNodesInserted(TreeModelEvent e)
			{
				java.awt.Dimension ps = tree.getPreferredSize();
				setPreferredSize(ps);
				tree.revalidate();
			}
			
			public void treeNodesRemoved(TreeModelEvent e)
			{
				java.awt.Dimension ps = tree.getPreferredSize();
				setPreferredSize(ps);
				tree.revalidate();
			}
			
			public void treeStructureChanged(TreeModelEvent e) {}
		});
		
		getDisplayArea().add(tree, "Domain Tree");
		
		
		// Create control for showing/hiding NamedReferences.
		final JButton namedRefControl = new JButton(ButtonIcons.ShowHideNamedReferenceButtonIcon);
		namedRefControl.setToolTipText(
			(namedRefControl.isSelected() ? "Hide" : "Show") + 
				" Named References (if any) under each Domain");
		namedRefControl.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				namedRefControl.setSelected(! namedRefControl.isSelected());
				setShowNamedReferences(namedRefControl.isSelected());
				refreshAll();
			}
		});
		namedRefControl.setSelected(true);
		
		getInfoPanel().add(namedRefControl, "Named Reference Visibility Control");
		
		validate();
	}
	
	
  /* Selection rule classes */

	
	private abstract class Rule
	{
		boolean allowed(Object obj) { return true; }
		boolean disallowed(Object obj) { return false; }
	}
	
	
	private class ClassAllowedRule extends Rule
	{
		Class type;
		ClassAllowedRule(Class c) { this.type = c; }
		boolean allowed(Object obj) { return obj.getClass() == this.type; }
	}
	
	
	private class IdAllowedRule extends Rule
	{
		String id;
		IdAllowedRule(String id) { this.id = id; }
		boolean allowed(Object obj)
		{
			if (obj instanceof VisualComponent)
				return ((VisualComponent)obj).getNodeId().equals(this.id);
			else
				return false;
		}
	}
	
	
	private class ClassNotAllowedRule extends ClassAllowedRule
	{
		ClassNotAllowedRule(Class c) { super(c); }
		boolean allowed(Object obj) { return true; }
		boolean disallowed(Object obj) { return ! super.allowed(obj); }
	}
	
	
	private class IdNotAllowedRule extends IdAllowedRule
	{
		IdNotAllowedRule(String id) { super(id); }
		boolean allowed(Object obj) { return true; }
		boolean disallowed(Object obj) { return ! super.allowed(obj); }
	}
	
	
  /* Implementation-specific methods */


	/**
	 * Refresh the Visuals that depict the specified Node, and its children.
	 */
	
	void refreshViewTreeStructure(NodeSer nodeSer)
	{
		try
		{
			if (nodeSer == null) refreshAll();
			else
			{
				NodeSer newNodeSer = getModelEngine().getNode(nodeSer.getNodeId());
				refreshVisuals(newNodeSer.getNodeId());
				String[] childIds = newNodeSer.getChildNodeIds();
				for (String childId : childIds) refreshViewTreeStructure(childId);
			}
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	void refreshViewTreeStructure(String nodeId)
	{
		try
		{
			if (nodeId == null) refreshAll();
			else
				refreshVisuals(nodeId);
			
			String[] childIds = getModelEngine().getChildNodeIds(nodeId);
			for (String childId : childIds) refreshViewTreeStructure(childId);
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	void addContextMenuItems(JPopupMenu popup, SelectorVisualTreeNode visual)
	{
		addViewContextMenuItems(popup);
		visual.addContextMenuItems(popup);
	}
	
	
	void addViewContextMenuItems(JPopupMenu popup)
	{
	}
	
	
	TreeModel getTreeModel() { return tree.getModel(); }
	
	
	void setShowNamedReferences(boolean show) { this.showNamedRefs = show; }
	
	
	boolean showNamedReferences() { return showNamedRefs; }
	
	
	protected boolean canObjectBeDisplayed(Object obj)
	{
		if (obj == null) return false;
		boolean canBeDisplayed = true;
		for (Rule rule : displayRules)
		{
			if (canBeDisplayed)
			{
				if (rule.disallowed(obj)) canBeDisplayed = false;
			}
			else
			{
				if (rule.allowed(obj)) canBeDisplayed = true;
			}
		}
		
		return canBeDisplayed;
	}
			
			
	void refreshAll()
	{
		List<VisualComponent> visuals = getOutermostVisuals();
		for (VisualComponent visual : visuals) try
		{
			refreshVisuals(visual.getNodeId());
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
  /* From View. Includes methods that are called to identify Visuals. */
	
	
	public List<NodeSer> getOutermostNodeSers()
	{
		List<NodeSer> sers = new Vector<NodeSer>();
		TreeNode root = (TreeNode)(treeModel.getRoot());
		Enumeration e = root.children();
		for (; e.hasMoreElements();)
		{
			Object node = e.nextElement();
			if (node instanceof VisualComponent) sers.add(((VisualComponent)node).getNodeSer());
		}

		return sers;
	}
	
	
	public List<String> getOutermostNodeIds()
	{
		List<String> ids = new Vector<String>();
		TreeNode root = (TreeNode)(treeModel.getRoot());
		Enumeration e = root.children();
		for (; e.hasMoreElements();)
		{
			Object node = e.nextElement();
			if (node instanceof VisualComponent) ids.add(((VisualComponent)node).getNodeId());
		}

		return ids;
	}
	
	
	public List<VisualComponent> getOutermostVisuals()
	{
		List<VisualComponent> visuals = new Vector<VisualComponent>();
		TreeNode root = (TreeNode)(treeModel.getRoot());
		Enumeration e = root.children();
		for (; e.hasMoreElements();)
		{
			Object node = e.nextElement();
			if (node instanceof VisualComponent) visuals.add((VisualComponent)node);
		}
		
		return visuals;
	}
	
	
	public void postConstructionSetup()
	throws
		Exception
	{
		super.postConstructionSetup();
		
		// Populate.
		
		List<NodeDomainSer> domainSers = getModelEngine().getDomains();
		for (NodeDomainSer domainSer : domainSers) addNode(domainSer.getNodeId());
		
		validate();
	}
	
	
	public ScenarioSelector getScenarioSelector()
	{
		return null;
	}
	
	
	public void resizeScenarioSelector()
	{
	}
	
	
	public String getViewType()
	{
		return ViewTypeName;
	}
	
	
	public synchronized Class getVisualClass(NodeSer nodeSer)
	{
		return visualFactory.getVisualClass(nodeSer);
	}
	
	
	public void notifyScenarioNameChanged(ScenarioVisual scenVis, String newName)
	{
	}
	
	
	public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
	{
	}
		
		
	public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId, String crossRefId)
	{
	}
		
		
	public VisualComponent.VisualComponentFactory getVisualFactory()
	{
		return visualFactory;
	}
	
	
	public synchronized VisualComponent makeTabularVisual(NodeSer nodeSer)
	throws
		ParameterError,
		Exception
	{
		return visualFactory.makeTabularVisual(nodeSer);
	}
	
	
	public synchronized Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		/*
			Return all the TreeVisuals that depict the specified Node.
		 */
		
		Set<VisualComponent> visuals = identifyVisualComponents((MutableTreeNode)(treeModel.getRoot()),
			new HashSet<VisualComponent>(), nodeId);
		
		return visuals;
	}
	
	
	public VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
		if (visuals == null) return null;
		if (visuals.size() == 0) return null;
		if (visuals.size() > 1) throw new RuntimeException(
			"Expected only one Visual with Id " + nodeId + " but there are " + visuals.size());
		VisualComponent vis = null;
		for (VisualComponent visual : visuals) vis = visual;
		return vis;
	}


	/**
	 * Recursive implementation used by identifyVisualComponents(String).
	 */
	 
	private Set<VisualComponent> identifyVisualComponents(MutableTreeNode node,
		Set<VisualComponent> visuals, String nodeId)
	throws
		ParameterError
	{
		if (node instanceof SelectorVisualTreeNode)
		{
			SelectorVisualTreeNode visual = (SelectorVisualTreeNode)node;
			if (visual.getNodeId().equals(nodeId))
			{
				visuals.add(visual);
			}
		}
		
		Enumeration<MutableTreeNode> children = node.children();
		for (; children.hasMoreElements();)
		{
			identifyVisualComponents(children.nextElement(), visuals, nodeId);
		}
		
		return visuals;
	}
	
	
	public synchronized Set<VisualComponent> identifyVisualComponents(Class c)
	throws
		ParameterError
	{
		return identifyVisualComponents((MutableTreeNode)(treeModel.getRoot()),
			new HashSet<VisualComponent>(), c);
	}


	/**
	 * Recursive implementation used by identifyVisualComponents(Class).
	 */
	 
	private Set<VisualComponent> identifyVisualComponents(MutableTreeNode node,
		Set<VisualComponent> visuals, Class c)
	throws
		ParameterError
	{
		if ((node instanceof VisualComponent) && (c.isAssignableFrom(node.getClass())))
			visuals.add((VisualComponent)node);
		
		Enumeration<MutableTreeNode> children = node.children();
		for (; children.hasMoreElements();)
		{
			identifyVisualComponents(children.nextElement(), visuals, c);
		}
		
		return visuals;
	}


	public synchronized Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		return identifyVisualComponents(VisualComponent.class);
	}
	
	
	public synchronized Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		TreePath selPath = tree.getSelectionPath();
		
		if (nodeId == null) throw new RuntimeException("nodeId is null");
		Set<VisualComponent> newVisuals = new HashSet<VisualComponent>();
		
		// Check if a Visual already exists for the Node.
		if (identifyVisualComponents(nodeId).size() > 0) return newVisuals;
		
		// Get NodeSer from ModelEngine.
		NodeSer ser = getModelEngine().getNode(nodeId);
		if (ser == null) throw new RuntimeException("Returned NodeSer is null");
		if (ser.getNodeId() == null) throw new RuntimeException(
			"Returned NodeSer has a null Node Id");
		
		// Check if Node is a NamedReference.
		if (ser instanceof NamedReferenceSer)
		{
			if (! showNamedReferences()) return newVisuals;
		}

		// Try to make a Visual for the Node. If unable to, return.
		SelectorVisualTreeNode visual = (SelectorVisualTreeNode)(makeVisual(ser, null, false));
		if (visual == null) return newVisuals;
		if (! canObjectBeDisplayed(visual)) return newVisuals;

		// Add the new Visual to the Set to be returned.
		newVisuals.add(visual);
		
		// Identify the Node Id of the Node that should be depicted as this Node's
		// parent. This is not necessarily the Node's actual parent, because
		// this Node might belong to a non-owning parent, such as a ScenarioSet
		// or SimRunSet. See the slide "Non-Owning Sets".
		
		String parentId = ser.getParentNodeId();
		VisualComponent parentVisual = null;
		NodeSer nodeSer = visual.getNodeSer();
		if (nodeSer instanceof ModelScenarioSer)
		{
			ModelScenarioSer msSer = (ModelScenarioSer)nodeSer;
			if (msSer.modelScenarioSetNodeId != null) parentId = msSer.modelScenarioSetNodeId;
		}
		else if (nodeSer instanceof SimulationRunSer)
		{
			SimulationRunSer srSer = (SimulationRunSer)nodeSer;
			if (srSer.simRunSetNodeId != null) parentId = srSer.simRunSetNodeId;
		}
		
		if (parentId == null) // adding a Domain
		{
			treeModel.insertNodeInto((MutableTreeNode)visual, 
				(MutableTreeNode)(treeModel.getRoot()), 0);
			
			if (showNamedReferences())
			{
				String[] namedReferenceNames = ((NodeDomainSer)ser).getNamedReferenceNames();
				for (String cname :namedReferenceNames)
				{
					NodeSer cser = getModelEngine().getChild(nodeId, cname);
					if (! (cser instanceof NamedReferenceSer)) throw new RuntimeException(
						"Expected a NamedReference but received a " + cser.getClass().getName());
					addNode(cser.getNodeId());
				}
			}
		}
		else
		{
			// Recursively add parent Visuals until a parent Visual is found to
			// be already present.
			
			Set<VisualComponent> viss = identifyVisualComponents(parentId);
			if (viss.size() > 1) throw new RuntimeException(
				"More than one parent Visual for Visual to be added");
			
			if (viss.size() == 0) viss = addNode(parentId);
			
			for (VisualComponent vis : viss) parentVisual = vis;
			if (parentVisual == null) throw new RuntimeException(
				"parent Visual for Node Id " + nodeId + " is null; viss.size=" +
				viss.size() + ".");
			if (! (parentVisual instanceof SelectorVisualTreeNode)) throw new RuntimeException(
				"Parent Visual is not a SelectorVisualTreeNode: it is a " +
				parentVisual.getClass().getName());
			
			// Update the parent Visual's NodeSer, so that fields such as
			// childNodeIds will be refreshed.
			parentVisual.setNodeSer(getModelEngine().getNode(parentVisual.getNodeId()));
			
			treeModel.insertNodeInto((MutableTreeNode)visual, (SelectorVisualTreeNode)parentVisual, 0);
		}
		
		// Make the new Visual visible and select it.
		TreeNode[] nodes = treeModel.getPathToRoot(visual);
		tree.makeVisible(new TreePath(nodes));
		tree.setSelectionPath(selPath); // ensure that the selection does not change.
		
		// Refresh the tree layout.
		revalidate();

		// Subscribe to receive update notifications for the child.
		// Note that this does NOT result in additional server connections.
		if (this.getListenerRegistrar() != null)
			this.getListenerRegistrar().subscribe(nodeId, true);

		return newVisuals;
	}
		
		
	public synchronized void notifyVisualComponentPopulated(VisualComponent visual,
		boolean populated) {}

	
	public synchronized void removeVisual(VisualComponent visual)
	throws
		Exception
	{
		treeModel.removeNodeFromParent((MutableTreeNode)visual);
		revalidate();
	}

		
	public synchronized Set<VisualComponent> refreshVisuals(String nodeId)
	throws
		Exception
	{
		Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
		for (VisualComponent visual : visuals) removeVisual(visual);
		return addNode(nodeId);
	}
	
	
	public synchronized void select(VisualComponent visual, boolean yes)
	{
		if (! (visual instanceof DefaultMutableTreeNode)) return;
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)visual;
		TreePath path = new TreePath(node.getPath());
		TreeSelectionModel model = tree.getSelectionModel();
		if (yes)
			model.addSelectionPath(path);
		else
			model.removeSelectionPath(path);
	}
	

	public void selectNodesWithId(String nodeId, boolean yes)
	throws
		ParameterError
	{
		Set<VisualComponent> visuals = identifyVisualComponents(nodeId);
		for (VisualComponent visual : visuals) select(visual, yes);
	}
	

	public synchronized Set<VisualComponent> getSelected()
	{
		TreeSelectionModel model = tree.getSelectionModel();
		TreePath[] paths = model.getSelectionPaths() ;
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		for (TreePath path : paths)
		{
			Object obj = path.getLastPathComponent();
			if (obj instanceof VisualComponent) visuals.add((VisualComponent)obj);
		}
		
		return visuals;
	}
	
	
	public synchronized boolean isSelected(VisualComponent visual)
	{
		TreeSelectionModel model = tree.getSelectionModel();
		TreePath[] paths = model.getSelectionPaths();
		for (TreePath path : paths)
		{
			Object obj = path.getLastPathComponent();
			if (obj == visual) return true;
		}
		
		return false;
	}
	
	
	public synchronized void selectAll(boolean yes)
	{
		if (yes)
		{
			Set<VisualComponent> visuals;
			try { visuals = identifyVisualComponents(); }
			catch (ParameterError pe)
			{
				ErrorDialog.showReportableDialog(this, pe);
				return;
			}
			
			for (VisualComponent visual : visuals) select(visual, true);
		}
		else
		{
			TreeSelectionModel model = tree.getSelectionModel();
			model.clearSelection();
		}
	}
	
	
  /* From ViewPanelBase. */
	
	
	protected View.ViewHelper createHelper()
	{
		return null;
	}
	
	
	protected ViewHelper getBaseHelper()
	{
		throw new RuntimeException("Should not be called: this class does not use a helper.");
	}
	
	
	public VisualComponent makeVisual(NodeSer nodeSer, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception
	{
		if (nodeSer.getNodeId() == null) throw new RuntimeException("Node Id is null");
		return ((TabularVisualComponentFactory)(getVisualFactory())).makeTabularVisual(nodeSer);
	}
	
	
	private boolean isPopup = false;
	public static final String ViewTypeName = "DomainSelectorView";
	
	
	public boolean isPopup() { return isPopup; }
	
	
	public void setIsPopup(boolean yes)
	{
		isPopup = yes;
	}
	
	
	public ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		throw new RuntimeException("This View does not support Scenarios");
	}
	
	
	public ScenarioVisual getCurrentScenarioVisual() { return null; }


	public ControlPanel getCurrentControlPanel() { return null; }
	
	
	public void saveFocusFieldChanges()
	{
	}
	
	
	public void domainCreated(String domainId)
	{
		try { addNode(domainId); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	public void motifCreated(String motifId)
	{
		try { addNode(motifId); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	

	public void showScenarioCreated(String scenarioId)
	{
		try { addNode(scenarioId); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	public void showScenarioSetCreated(String scenarioSetId)
	{
		try { addNode(scenarioSetId); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
  /* Node types: each is a VisualComponent that extends DefaultMutableTreeNode.
  	Methods in these types are called by the View when handling PeerNotices.
  	Thus, these methods do not notify the server: they merely update this View
  	in response to notifications from the server. */
	
	
  	/**
  	 * Factory for making these Visual TreeNode types.
  	 */

	class TreeNodeFactory implements TabularVisualComponentFactory
	{
		public Class getVisualClass(NodeSer nodeSer)
		{
			if (nodeSer instanceof SimulationRunSer) return SimRunVisualTreeNode.class;
			else if (nodeSer instanceof SimRunSetSer) return SimRunSetVisualTreeNode.class;
			else if (nodeSer instanceof ModelDomainMotifDefSer) return ModelDomainMotifVisualTreeNode.class;
			else if (nodeSer instanceof ModelDomainSer) return ModelDomainVisualTreeNode.class;
			else if (nodeSer instanceof NamedReferenceSer) return NamedReferenceVisualTreeNode.class;
			else if (nodeSer instanceof ModelScenarioSer) return ModelScenarioVisualTreeNode.class;
			else if (nodeSer instanceof ModelScenarioSetSer) return ModelScenarioSetVisualTreeNode.class;
			else if (nodeSer instanceof DecisionDomainMotifDefSer) return DecisionDomainMotifVisualTreeNode.class;
			else if (nodeSer instanceof DecisionDomainSer) return DecisionDomainVisualTreeNode.class;
			else if (nodeSer instanceof DecisionScenarioSer) return DecisionScenarioVisualTreeNode.class;
			else if (nodeSer instanceof HierarchyDomainMotifDefSer) return HierarchyDomainMotifVisualTreeNode.class;
			else if (nodeSer instanceof HierarchyDomainSer) return HierarchyDomainVisualTreeNode.class;
			else if (nodeSer instanceof HierarchyScenarioSer) return HierarchyScenarioVisualTreeNode.class;
			else if (nodeSer instanceof HierarchyScenarioSetSer) return HierarchyScenarioSetVisualTreeNode.class;
			else return null;
		}
		
		
		public VisualComponent makeTabularVisual(NodeSer nodeSer)
		throws
			ParameterError,
			Exception
		{
			Class treeNodeClass = getVisualClass(nodeSer);
			if (treeNodeClass == null) return null;  // Not displayed by this View.
			
			Class nodeSerClass = nodeSer.getClass();
			
			java.lang.reflect.Constructor<SelectorVisualTreeNode> constructor = 
				treeNodeClass.getConstructor(DomainSelector.class, nodeSerClass);
			
			if (nodeSer.getNodeId() == null) throw new RuntimeException("Node Id is null");
			Object instance = constructor.newInstance(DomainSelector.this, nodeSer);
			
			((VisualComponent)instance).setAsIcon(false);
				
			return (VisualComponent)instance;
		}


		public VisualComponent makeGraphicVisual(NodeSer nodeSer, Container container,
			boolean asIcon)
		throws
			CannotBeInstantiated,
			ParameterError,
			Exception
		{
			throw new RuntimeException("Should not be called");
		}
	}

	
  /* ***************************************************************************
  	TreeNode classes.
  	*/

	/**
	 * For displaying the root node of the tree, which should be the host network
	 * path for the ModelEngine.
	 */
	
	public class HostTreeNode extends DefaultMutableTreeNode
	{
		public HostTreeNode(String networkPath)
		{
			super(networkPath);
		}
	}
	
	
	/**
	 * Base type for all VisualComponents used in this DomainSelector. Note that
	 * the root TreeNode is not a VisualComponent.
	 */
	 
	abstract class SelectorVisualTreeNode extends DefaultMutableTreeNode implements VisualComponent
	{
		private ImageIcon imageIcon;
		
		
		public SelectorVisualTreeNode(NodeSer nodeSer)
		{
			if (nodeSer == null) throw new RuntimeException("nodeSer is null");
			if (nodeSer.getNodeId() == null) throw new RuntimeException(
				"Null Node Id for NodeSer '" + nodeSer.getFullName() + "'");
			setUserObject(nodeSer);
		}
		
		
		abstract void performOpen();
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			MenuItemHelper.addMenuOwnerItems(getClientView(), this,
				(MenuOwnerSer)(getNodeSer()), popup, popup.getX(), popup.getY());
		}
		
		
	  /* From VisualComponent */
	
		
		public int getSeqNo()
		{
			if (this == treeModel.getRoot()) return 0;
			return treeModel.getIndexOfChild(this.getParent(), this);
		}
		
		 
		public boolean contains(VisualComponent comp) { return false; }
		
		
		public String[] getChildIds() { return new String[0]; }
		
	
		public final String getNodeId() { return getNodeSer().getNodeId(); }
		
		 
		public final String getName() { return getNodeSer().getName(); }
		
		
		public final String getFullName() { return getNodeSer().getFullName(); }
		
		
		public ImageIcon getImageIcon() { return imageIcon; }
		
		
		public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
		
	
		public abstract String getDescriptionPageName();


		public final boolean isOwnedByMotifDef()
		{
			return getNodeSer().ownedByMotifDef();
		}
		
		
		public final boolean isOwnedByActualTemplateInstance()
		{
			return getNodeSer().ownedByActualTemplateInstance();
		}

	
		public void nameChangedByServer(String newName, String newFullName)
		{
			((NodeSer)(getUserObject())).setName(newName);
			((NodeSer)(getUserObject())).setFullName(newFullName);
			repaint();
		}
		
		
		public void nodeDeleted()
		{
			TreeNode parent = getParent();
			if (parent == treeModel.getRoot())
			{
				treeModel.removeNodeFromParent(this);
			}
			
			if ((parent != null) && (parent instanceof SelectorVisualTreeNode))
			{
				SelectorVisualTreeNode visual = (SelectorVisualTreeNode)parent;
				refreshViewTreeStructure(visual.getNodeSer());
			}
			else
				refreshAll();
		}
		
		
		public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
		{
			NodeSer ser;
			try { ser = getModelEngine().getNode(crossRefId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
			
			if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
				"Node with Id " + crossRefId + " is not a CrossReference");
			CrossReferenceSer crSer = (CrossReferenceSer)ser;
			getClientView().notifyCrossReferenceCreated(crSer);
		}
			
			
		public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
		{
			getClientView().notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
		}
		
		
		public final View getClientView() { return DomainSelector.this; }
		
		
		public ModelEngineRMI getModelEngine() { return DomainSelector.this.getModelEngine(); }
	
	
		public final NodeSer getNodeSer() { return (NodeSer)(getUserObject()); }
		
		
		public final void setNodeSer(NodeSer nodeSer)
		{
			if (nodeSer == null) throw new RuntimeException(
				"Attempt to set NodeSer to null");
			
			this.setUserObject(nodeSer);
		}
		
		 
		public final void update()
		{
			// Get new NodeSer from server.
			try
			{
				setNodeSer(getModelEngine().getNode(getNodeId()));
				refreshRedundantState();
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, 
					"While getting Node form server", ex); return;
			}
			
			refreshViewTreeStructure(getNodeSer());
		}
		
	
		public final void update(NodeSer nodeSer)
		{
			this.setNodeSer(nodeSer);
		}
		
		
		public void redraw()
		{
			repaint();
		}
		
	
		public final void refresh()
		{
			update();
			redraw();
		}
		
		
		/**
		 * Return the location of this Visual within the Graphic of the JTree
		 * that owns it.
		 */
		 
		public java.awt.Point getLocation()
		{
			java.awt.Rectangle rect = getRectangle();
			return new java.awt.Point(rect.x, rect.y);
		}
		
		
		public int getX() { return getRectangle().x; }
		
		
		public int getY() { return getRectangle().y; }
		
		
		protected java.awt.Rectangle getRectangle()
		{
			TreeNode[] nodes = this.getPath();
			TreePath path = new TreePath(nodes);
			int row = tree.getRowForPath(path);
			if (row < 0) throw new RuntimeException(path.toString());
			return tree.getRowBounds(row);
		}
		
	
		public final void refreshLocation()
		{
		}
		
		
		public final void refreshSize()
		{
		}
	
	
		public final void setLocation()
		throws
			Exception
		{
		}
		
		
		public final void setSize()
		throws
			Exception
		{
		}

	
		public void refreshRedundantState()
		throws
			Exception
		{
			if (this == treeModel.getRoot()) setName(this.getName());
		}
		
		
		public final boolean getAsIcon() { return false; }
	
	
		public final void setAsIcon(boolean yes) {}
		
		
		public final boolean useBorderWhenIconified() { return false; }
	
	
		public void populateChildren()
		throws
			ParameterError,
			Exception
		{
			String[] childIds = getNodeSer().getChildNodeIds();
			for (String childId: childIds)
			{
				addNode(childId);
			}
			
			notifyVisualComponentPopulated(this, true);
		}
	
	
		/*public void refreshChildren()
		throws
			ParameterError,
			Exception
		{
		}*/
	
	
		public Set<VisualComponent> getChildVisuals()
		{
			Set<VisualComponent> visuals = new HashSet<VisualComponent>();
			String[] childIds = getNodeSer().getChildNodeIds();
			for (String childId : childIds) try
			{
				visuals.addAll(identifyVisualComponents(childId));
			}
			catch (Exception ex) { throw new RuntimeException(ex); }
			
			return visuals;
		}
		
		
		public final Object getParentObject() { return this.getParent(); }
	
	
		public Container getContainer() { return DomainSelector.this; }
	
	
		public Component getComponent() { return DomainSelector.this; }
	
	
		public void highlight(boolean yes)
		{
			TreeSelectionModel selMod = tree.getSelectionModel();
			TreePath path = new TreePath(this.getPath());
			
			if (yes) selMod.addSelectionPath(path);
			else selMod.removeSelectionPath(path);
		}
		
		
		public boolean isHighlighted()
		{
			TreeSelectionModel selMod = tree.getSelectionModel();
			TreePath path = new TreePath(this.getPath());
			return selMod.isPathSelected(path);
		}
		

		public Color getNormalBackgroundColor()
		{
			DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer)(tree.getCellRenderer());
			return renderer.getBackgroundNonSelectionColor();
		}
	
	
		public Color getHighlightedBackgroundColor()
		{
			DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer)(tree.getCellRenderer());
			return renderer.getBackgroundSelectionColor();
		}


		public int getWidth()
		{
			TreeNode[] nodes = this.getPath();
			TreePath path = new TreePath(nodes);
			int row = tree.getRowForPath(path);
			if (row < 0) return 0;
			java.awt.Rectangle rect = tree.getRowBounds(row);
			return rect.width;
		}
		
		
		public int getHeight()
		{
			TreeNode[] nodes = this.getPath();
			TreePath path = new TreePath(nodes);
			int row = tree.getRowForPath(path);
			if (row < 0) return 0;
			java.awt.Rectangle rect = tree.getRowBounds(row);
			return rect.height;
		}
		
		
		public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
			ScenarioVisual scenarioVisual)
		throws
			IOException
		{
		}
		
		
		public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
			ScenarioVisual scenarioVisual)
		throws
			IOException
		{
		}
	}
	
	
	abstract class ContainerVisualTreeNode extends SelectorVisualTreeNode implements ContainerVisual
	{
		public ContainerVisualTreeNode(NodeSer nodeSer) { super(nodeSer); }
		public void componentResized(VisualComponent child) throws Exception {}  // na
	}
	
	
	abstract class ModelContainerVisualTreeNode extends ContainerVisualTreeNode
		implements ModelContainerVisual
	{
		public ModelContainerVisualTreeNode(NodeSer nodeSer) { super(nodeSer); }
			
		public void resetConduitColors() {}  // na
		public void incrementConduitColor() {}  // na
		public Color getConduitColor() { return null; } // na
	}
	
	
	abstract class PortedContainerVisualTreeNode extends ModelContainerVisualTreeNode
		implements PortedContainerVisual
	{
		public PortedContainerVisualTreeNode(NodeSer nodeSer) { super(nodeSer); }
	}
	
	
	private static final ImageIcon NamedReferenceIcon =
		DomainListJTree.createImageIcon(NodeIconImageNames.NamedReferenceIconImageName);
	

	class NamedReferenceVisualTreeNode extends SelectorVisualTreeNode
	{
		public NamedReferenceVisualTreeNode(NamedReferenceSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(NamedReferenceIcon);
		}
		
		
		public String getDescriptionPageName() { return "Named References"; }


		void performOpen()
		{
			TabularVisualJ parent = (TabularVisualJ)(getParent());
			if (parent == null) return;
			if (! (parent instanceof DomainVisual)) return;
			
			(new NamedReferenceEditor(getClientMainApplication(),
				(NodeDomainSer)(parent.getNodeSer()), (NamedReferenceSer)(getNodeSer()))).show();
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Open", false, 
				"Open a panel to edit the name and properties of this Named Reference.");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e) { performOpen(); }
			});
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Delete", false, "Delete this Named Reference.");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try
					{
						try { getModelEngine().deleteNamedReference(false, getNodeId()); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(DomainSelector.this, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							{
								getModelEngine().deleteNamedReference(true, getNodeId());
							}
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(DomainSelector.this, ex);
					}
				}
			});
			popup.add(mi);
		}
	}
	
	
	class CrossReferenceVisualTreeNode extends SelectorVisualTreeNode
	{
		public CrossReferenceVisualTreeNode(CrossReferenceSer nodeSer) { super(nodeSer); }
		
		
		public String getDescriptionPageName() { return "Named References#Cross_References"; }


		void performOpen()
		{
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Delete",
				false, 
				"Delete this Cross Reference.");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try
					{
						try { getModelEngine().deleteCrossReference(false, getNodeId()); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(DomainSelector.this, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							{
								getModelEngine().deleteCrossReference(true, getNodeId());
							}
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(DomainSelector.this, ex);
					}
				}
			});
			popup.add(mi);
		}
	}
	
	
	class DomainVisualTreeNode<DomainSerType extends NodeDomainSer>
		extends ContainerVisualTreeNode implements DomainVisual
	{
		//private boolean showNamedRefs = false;
		public DomainVisualTreeNode(DomainSerType nodeSer) { super(nodeSer); }
		
		
		public String getDescriptionPageName() { return getNodeSer().getNodeKind() + "s"; }


		void performOpen()
		{
			try
			{
				getViewFactory().createViewPanel(false, getNodeId());
			}
			catch (Exception ex) { ErrorDialog.showReportableDialog(DomainSelector.this, ex); }
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Open", false,
				"Retrieve the " +
				HelpWindow.createHref(getDescriptionPageName(), getNodeSer().getNodeKind()) +
				" from the server and open a graphics view of it.");
			mi.addActionListener(getOpenDomainHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Open Domain without Scenarios", false,
				"Open the selected " +
				HelpWindow.createHref(getDescriptionPageName(), getNodeSer().getNodeKind()) +
				", but not its Model Scenarios (unless " +
				"they are already open).");
			mi.addActionListener(getOpenDomainHandler(this, true));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Make Copy", false,
				"Create and display a new, independent copy of the " +
				HelpWindow.createHref(getDescriptionPageName(), getNodeSer().getNodeKind()) +
				", and assign it a different name.");
			mi.addActionListener(getCopyDomainHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Define new NamedReference...", false,
				"Establish a new type of " +
				HelpWindow.createHref("Named References", "Named Reference") +
				" for this Domain.");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					(new NamedReferenceEditor(getClientMainApplication(),
						(NodeDomainSer)(getNodeSer()))).show();
				}
			});
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Export as XML", false,
				"Generate XML for the " +
				HelpWindow.createHref("Model Domains", "Model Domain") +
				". The XML will include all " +
				HelpWindow.createHref("Model Scenarios") +
				" but will not define or include any " +
				HelpWindow.createHref("Simulation Runs") +
				". You will be able to save the XML to a file, if desired.");
			mi.addActionListener(getExportAsXMLHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Delete", false,
				"Delete (from the database) the " +
				HelpWindow.createHref("Model Domains", "Model Domain") +
				" and all " +
				HelpWindow.createHref("Model Scenarios") + " and " +
				HelpWindow.createHref("Simulation Runs") +
				" associated with it.");
			mi.addActionListener(getDeleteModelElementHandler(this));
			popup.add(mi);
		}
		
		
	  /* From DomainVisual */
		
		
		public void motifAdded(String motifId)
		{
		}
		
		
		public void motifRemoved(String motifId)
		{
		}
		
		
		public ScenarioVisual showScenario(String scenarioId)
		throws
			Exception
		{
			try
			{
				ViewContainer viewPanel = getViewFactory().createViewPanel(
					false, getNodeSer().getDomainId(), scenarioId);
				DomainVisual newView = (DomainVisual)viewPanel;
				return newView.getScenarioVisual(scenarioId);
			}
			catch (Exception ex) { throw new RuntimeException(ex); }
		}
		
		public void showScenarioCreated(String scenarioId)
		{
			try { addNode(scenarioId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}
		
		public void showScenarioDeleted(String scenarioId)
		{
			try
			{
				Set<VisualComponent> viss = identifyVisualComponents(scenarioId);
				for (VisualComponent vis : viss) removeVisual(vis);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}

		public ScenarioVisual getScenarioVisual(String scenarioId)
		{
			Set<VisualComponent> viss;
			try { viss = getClientView().identifyVisualComponents(scenarioId); }
			catch (Exception ex) { throw new RuntimeException(ex); }
			
			if (viss == null) return null;
			for (VisualComponent vis : viss)
			{
				if (! (vis instanceof ScenarioVisual)) throw new RuntimeException(
					"Visual is not a ScenarioVisual");
				if (vis.getNodeSer().getDomainId().equals(this.getNodeId()))
					return (ScenarioVisual)vis;
			}
			
			return null;
		}
	}
	
	
	/**
	 * Find the MenuItem with the specified text in the specified Popup, and replace
	 * the MenuItem with the specified new MenuItem.
	 */
	 
	protected void replaceMenuItem(JPopupMenu popup, String menuItemText, JMenuItem newMenuItem)
	{
		MenuElement[] mes = popup.getSubElements();
		int pos = -1;
		for (MenuElement me : mes)
		{
			pos++;
			if (me instanceof JMenuItem)
			{
				String text = ((JMenuItem)me).getText();
				if (text == null) continue;
				if (text.equals(menuItemText))
				{
					popup.remove(pos);
					popup.insert(newMenuItem, pos);
					return;
				}
			}
		}
	}


	/**
	 * Append MenuItems specific to MotifDefs to the specified Popup Menu, in the
	 * context of the specified Motif VisualTreeNode.
	 */
	 
	protected void addMotifContextMenuItems(DomainVisualTreeNode visual, JPopupMenu popup)
	{
		if (! (visual instanceof MotifDefVisual)) throw new RuntimeException(
			"Expected visual to be a MotifDefVisual: it is a " + visual.getClass().getName());
		
		JMenuItem mi = new HelpfulMenuItem("Open", false,
			"Retrieve the " +
			HelpWindow.createHref("Motifs", "Motif") +
			" from the server and open a graphics view of it.");
		mi.addActionListener(getOpenMotifHandler((MotifDefVisual)visual));
		popup.add(mi);
		
		mi = new HelpfulMenuItem("Make Copy", false,
			"Create and display a new, independent copy of the " +
			HelpWindow.createHref("Motifs", "Motif") +
			", and assign it a different name.");
		mi.addActionListener(getCopyMotifHandler((MotifDefVisual)visual));
		popup.add(mi);
	}
	
	
  /* Types that are specific to ModelDomains. */
	
	
	private static final ImageIcon ModelDomainIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.ModelDomainIconImageName);


	class ModelDomainVisualTreeNode extends DomainVisualTreeNode<ModelDomainSer>
		implements ModelDomainVisual
	{
		public ModelDomainVisualTreeNode(ModelDomainSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(ModelDomainIcon);
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Make Copy", false,
				"Create and display a new, independent copy of the " +
				HelpWindow.createHref("Model Domains", "Model Domain") +
				", and assign it a different name. " +
				HelpWindow.createHref("Simulation Runs") + " are not copied.");
			mi.addActionListener(getCopyDomainHandler(this));
			replaceMenuItem(popup, "Make Copy", mi);

			mi = new HelpfulMenuItem("Create New Scenario", false,
				"Create and display a new " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				" for the " +
				HelpWindow.createHref("Model Domains", "Model Domain") +
				".");
			mi.addActionListener(getDefineNewScenarioHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("View Expected Value by Scenario",
				false, "The " + HelpWindow.createHref("States", "State") +
				" within the " + HelpWindow.createHref("Model Domains", "Model Domain") +
				" (" + this.getName() + ") that is " +
				HelpWindow.createHref("Tag Values", "tagged") +
				" with a " +
				HelpWindow.createHref("Attributes", "Net_Value", "Net Value Attribute") +
				" is identified. The value of this State in each " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				", averaged across all " + 
				HelpWindow.createHref("Simulation Runs", "Simulation Runs") + " for that " +
				"Scenario, is graphed by Scenario, with Total Cost as the X axis. " +
				"The model must also have a State tagged with a Cost.Total Attribute.");
			mi.addActionListener(getViewExpectedValueByScenarioHandler(this));
			popup.add(mi);
		}
		
		
	  /* From ModelDomainVisual */
		
		public List<ModelScenarioVisual> getModelScenarioVisuals()
		{
			ModelDomainSer mdSer = (ModelDomainSer)(getNodeSer());
			String[] scenarioIds = mdSer.scenarioIds;
			List<ModelScenarioVisual> visuals = new Vector<ModelScenarioVisual>();
			for (String scenarioId : scenarioIds)
			{
				ScenarioVisual vis = getScenarioVisual(scenarioId);
				if (vis == null) continue;
				if (! (vis instanceof ModelScenarioVisual)) throw new RuntimeException(
					"Scenario is not a ModelScenario");
				visuals.add((ModelScenarioVisual)vis);
			}
			
			return visuals;
		}
		
		public void simulationRunsDeleted()
		{
			String[] scenIds = ((ModelDomainSer)(getNodeSer())).scenarioIds;
			for (String scenId : scenIds)
			{
				List<SimulationRunSer> simRunSers;
				try { simRunSers = getModelEngine().getSimulationRuns(scenId); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainSelector.this, ex);
					return;
				}
				
				for (SimulationRunSer simRunSer : simRunSers) try
				{
					Set<VisualComponent> viss = identifyVisualComponents(simRunSer.getNodeId());
					for (VisualComponent vis : viss) removeVisual(vis);
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(DomainSelector.this, ex);
					return;
				}
			}
		}

		public void showScenarioSetCreated(String newScenSetId)
		{
			try { addNode(newScenSetId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}

		public void attributeStateBindingChanged(String attrId, String stateId) {}

		public void loadAllScenarios()
		{
			String[] scenarioIds = ((ModelDomainSer)(getNodeSer())).scenarioIds;
			try { getViewFactory().createViewPanel(false, this.getNodeId(), scenarioIds); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}

		public void motifAdded(String motifId)
		{
			try { addNode(motifId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}

		public void motifRemoved(String motifId)
		{
			try
			{
				Set<VisualComponent> viss = identifyVisualComponents(motifId);
				for (VisualComponent vis : viss) removeVisual(vis);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}
		
		
	  /* From ModelContainerVisual */
		
		public void resetConduitColors() {}  // na
		public void incrementConduitColor() {}  // na
		public Color getConduitColor() { return null; } // na
	}


	private static final ImageIcon ModelDomainMotifIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.MotifDefIconImageName);


	class ModelDomainMotifVisualTreeNode extends ModelDomainVisualTreeNode implements MotifDefVisual
	{
		public ModelDomainMotifVisualTreeNode(ModelDomainMotifDefSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(ModelDomainMotifIcon);
		}

		
		public String getDescriptionPageName() { return "Motifs"; }


		void performOpen()
		{
			try { getViewFactory().createViewPanel(false, getNodeId());
				//createMotifViewPanel(
				//(ModelDomainMotifDefSer)(getNodeSer()), true);
			}
			catch (Exception ex) { ErrorDialog.showReportableDialog(DomainSelector.this, ex); }
		}


		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			addMotifContextMenuItems(this, popup);
		}
	}
	
	
	private static final ImageIcon SimulationRunIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.SimulationRunIconImageName);

	
	class SimRunVisualTreeNode extends SelectorVisualTreeNode implements SimulationRunVisual
	{
		public SimRunVisualTreeNode(SimulationRunSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(SimulationRunIcon);
		}
		
		
		public String getDescriptionPageName() { return "Simulation Runs"; }


		void performOpen()
		{
			try { createEventHistoryWindow(); }
			catch (Exception ex) { ErrorDialog.showReportableDialog(DomainSelector.this, ex); }
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("View Value over Time",
				false, 
				"Each " + HelpWindow.createHref("States", "State") +
				" within the " + HelpWindow.createHref("Model Domains", "Model Domain") +
				" that has a Net Value Attribute is identified. " +
				"A graph is displayed showing the simulated history of the State over time.");
			mi.addActionListener(getViewValueOverTimeHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("View Event History", false,
				"A table is displayed showing the full history of all States" +
				" for the " + HelpWindow.createHref("Simulation Runs", "Simulation Run") +
				". If you then click 'Show Events' in the " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				" view, then the table can be used to control display (in the Scenario view) " +
				HelpWindow.createHref("States", "State") + " values for any" +
				" point in simulated time.");
			mi.addActionListener(getViewEventHistoryHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("View Event History for Tagged States",
				false, 
				"A table is displayed showing the full simulated history of all States" +
				" in the " + HelpWindow.createHref("Model Domains", "Model Domain") +
				" that are " + HelpWindow.createHref("Tag Values", "tagged") +
				" with a " + HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
				" Attribute. If you then click 'Show Events' in the " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				" view, then the table can be used to control display (in the Scenario view) " +
				HelpWindow.createHref("States", "State") + " values for any" +
				" point in simulated time.");
			mi.addActionListener(getViewEventHistoryHandler(this,
				StateTagsToReportOn));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Delete", false,
				"Delete the " + HelpWindow.createHref("Simulation Runs", "Simulation Run") +
				".");
			mi.addActionListener(getDeleteModelElementHandler(this));
			popup.add(mi);
		}
		
		
	  /* From SimulationRunVisual */
	
		public void attributesInitialized() {}
		public void attributesUpdated() {}
		public void stateInitEventsCreated() {}
		public void predefinedEventsRetrieved() {}
		public void epochStarted(int epochNo, Date date) {}
		public void epochFinished(int epochNo, Date date) {}
		public void allFunctionsEvaluatedForEpoch(int epochNo, Date date) {}
		public void allActivitiesActivatedForEpoch(int epochNo, Date date) {}
		public void reportProgress(String msg) {}
		public void epochCompleted(Date epoch, int iteration) {}
		public JFrame createEventHistoryWindow(Class... stateTags)
		throws
			Exception
		{
			return EventHistoryPanel.createEventHistoryWindow(DomainSelector.this,
				getNodeId(), stateTags);
		}
	}
	
	
	private static final ImageIcon SimRunSetIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.SimRunSetIconImageName);


	class SimRunSetVisualTreeNode extends SelectorVisualTreeNode
		implements VisualComponent.SimRunSetVisual
	{
		public SimRunSetVisualTreeNode(SimRunSetSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(SimRunSetIcon);
		}
		
		
		public String getDescriptionPageName() { return "Simulation Run Sets"; }


		void performOpen()
		{
			throw new RuntimeException("Should not be able to call Open on a SimRunSet");
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);

			final SimRunSetSer simRunSetSer = (SimRunSetSer)(this.getNodeSer());
			
			JMenuItem mi = new HelpfulMenuItem("View Distribution", false,
				"Display a histogram of the final value of the NetValue " +
				HelpWindow.createHref("States", "State") + " for each " +
				HelpWindow.createHref("Simulation Runs", "Simulation Run") +
				".");
			
			mi.addActionListener(new ViewDistributionHandler(
				DomainSelector.this, simRunSetSer.scenarioId, simRunSetSer.getNodeId(), null /* stateId */,
				simRunSetSer.domainId, getPanelManager()));
			
			popup.add(mi);
			
			mi = new HelpfulMenuItem("View Expected Value Over Time",
				false, "Display the trend of the value of the Net Value " +
				HelpWindow.createHref("States", "State") + " over time, " +
				"averaged over each " + 
				HelpWindow.createHref("Simulation Runs", "Simulation Run") +
				".");
			
			mi.addActionListener(new ViewExpectedValueOverTimeHandler(
				DomainSelector.this, simRunSetSer.scenarioId, simRunSetSer.getNodeId(),
				null /* stateId */, simRunSetSer.domainId, getPanelManager()));

			popup.add(mi);
		}
		
		
	  /* From SimRunSetVisual */
		
		public void simRunSetCreated(String simRunSetId, String modelScenarioId)
		{
			try { addNode(simRunSetId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}
		
		public void refreshStatistics() {}
	}
	
	
	abstract class ScenarioVisualTreeNode extends SelectorVisualTreeNode implements ScenarioVisual
	{
		public ScenarioVisualTreeNode(ScenarioSer nodeSer)
		{
			super(nodeSer);
		}
		
		
		void performOpen()
		{
			try { openScenarioVisual((ScenarioSer)(getNodeSer())); }
			catch (Exception ex) { ErrorDialog.showReportableDialog(DomainSelector.this, ex); }
		}
		
		
	  /* From ScenarioVisual */

		public List<Serializable> getAttributeValues(VisualComponent vis, List<String> attrNames)
		{
			return new Vector<Serializable>();
		}


		public Serializable getAttributeValue(String attrId)
		{
			return null;
		}

		
		public void refreshAttributeValue(String attrNodeId) throws Exception {}

		public void setLocation(VisualComponent visual) {}
		public void addOrRemoveAttributeControls(boolean add, VisualComponent visual)
		throws
			Exception {}

		public void deleteControls(String nodeId) {}

		public boolean controlsForAttrAreVisible(VisualComponent visual) { return false; }
	}
	
	
	private static final ImageIcon ModelScenarioIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.ModelScenarioIconImageName);


	class ModelScenarioVisualTreeNode extends ScenarioVisualTreeNode implements ModelScenarioVisual
	{
		public ModelScenarioVisualTreeNode(ModelScenarioSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(ModelScenarioIcon);
		}
		
		
		public String getDescriptionPageName() { return "Model Scenarios"; }


		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Open", false,
				"Retrieve the " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				" from the server and open a graphics view of it.");
			mi.addActionListener(getOpenScenarioHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Make Copy", false,
				"Create and display a new, independent copy of the " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				", and assign it a different name. " +
				HelpWindow.createHref("Simulation Runs") + " are not copied.");
			mi.addActionListener(getCopyModelScenarioHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Simulate...", false,
				"Simulate the Scenario. A popup dialog allows one to configure the simulation.");
			mi.addActionListener(getSimulateHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("View Distribution", false,
				"Each " + HelpWindow.createHref("States", "State") +
				" within the " + HelpWindow.createHref("Model Domains", "Model Domain") +
				" (" + getNodeSer().getDomainName() + ") " +
				"that has a Net Value Attribute is identified. " +
				"A histogram is created for each of these States of the State's final value in each " +
				HelpWindow.createHref("Simulation Runs", "Simulation Run") + " for the " +
				"Scenario.");
			String domainId = ((ModelScenarioSer)(this.getNodeSer())).domainId;
			mi.addActionListener(getViewDistributionHandler(this, 
				this.getNodeId(), domainId));
			popup.add(mi);

			mi = new HelpfulMenuItem("View Expected Value over Time",
				false, "Display the trend of the value over time of the  " +
				HelpWindow.createHref("States", "States") +
				" that are " + HelpWindow.createHref("Tag Values", "tagged") +
				" with a " + HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") +
				" Attribute, averaged over each " + 
				HelpWindow.createHref("Simulation Runs", "Simulation Run") +
				".");
			mi.addActionListener(getViewExpectedValueOverTimeHandler(
				this, this.getNodeId(), domainId));
			
			popup.add(mi);

			mi = new HelpfulMenuItem("Delete", false,
				"Delete the " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				" and all " +
				HelpWindow.createHref("Simulation Runs") +
				" associated with it.");
			mi.addActionListener(getDeleteModelElementHandler(this));
			popup.add(mi);
		}
		
	  /* From ModelScenarioVisual */
		
		public void addSimulationVisual(Integer id, SimulationVisual simPanel)
		{
			//addSimVisual(id, simPanel);
		}

		
		public Set<SimulationVisual> simulationRunCreated(Integer requestId,
			String simNodeId) // Update View.
		{
			return handleSimulationRunCreated(this, simNodeId);
		}
		
		public void simulationRunsDeleted()
		{
			List<SimulationRunSer> simRunSers;
			try { simRunSers = getModelEngine().getSimulationRuns(this.getNodeId()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
			
			for (SimulationRunSer simRunSer : simRunSers) try
			{
				Set<VisualComponent> viss = identifyVisualComponents(simRunSer.getNodeId());
				for (VisualComponent vis : viss) removeVisual(vis);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}

		public void setShowEventsMode(boolean showEventsMode) {}
		public boolean getShowEventsMode() { return false; }
		public void showEventsForIteration(String simRunId, int iteration) {}
		public void refreshStatistics() {}
		public void simTimeParamsChanged(Date newFromDate, Date newToDate, long newDuration,
			int newIterLimit) {}
		public void addOrRemoveStateControls(boolean add, VisualComponent visual)
		throws
			Exception {}
		public boolean controlsForStateAreVisible(PortedContainerVisual visual) { return false; }
		public JFrame createEventHistoryWindow(String simRunId, Class... stateTags)
		throws
			Exception { return EventHistoryPanel.createEventHistoryWindow(DomainSelector.this, 
				simRunId, stateTags); }
	}
	
	
	private static final ImageIcon ModelScenarioSetIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.ModelScenarioSetIconImageName);


	class ModelScenarioSetVisualTreeNode extends SelectorVisualTreeNode implements ModelScenarioSetVisual
	{
		public ModelScenarioSetVisualTreeNode(ModelScenarioSetSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(ModelScenarioSetIcon);
		}
		
		
		public String getDescriptionPageName() { return "Model Scenario Sets"; }


		void performOpen()
		{
			String[] scenIds = ((ScenarioSetSer)(getNodeSer())).getScenarioIds();
			for (String scenId : scenIds)  try // Open each Scenario in the ScenarioSet.
			{
				getViewFactory().createViewPanel(false, getNodeSer().getDomainId(),
					scenId);
			}
			catch (Exception ex) { ErrorDialog.showReportableDialog(DomainSelector.this, ex); }
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Simulate Scenarios...", false,
				"Open a window to allow you to start the simulation of each Scenario" +
				" in the Scenario Set.");
			final SimulationVisualMonitor simMonitor = new SimulationVisualMonitor()
			{
				public void simulationVisualClosing(SimulationVisual simVis)
				{
					//DomainSelector.this.removeSimVisual(simVis.getCallbackId());
				}
			};
			
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					String domainId = 
						((ModelScenarioSetSer)(getNodeSer())).domainId;
					
					ModelDomainSer domainSer = null;
					try { domainSer = 
							(ModelDomainSer)(getModelEngine().getNode(
								domainId, ModelDomainSer.class)); }
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(DomainSelector.this, ex);
						return;
					}
					
					(new SimulatePanel(
						DomainSelector.this, 
						ModelScenarioSetVisualTreeNode.this,
						simMonitor,
						(ModelScenarioSetSer)(getNodeSer()),
						domainSer,
						getViewFactory(), 
						getModelEngine())).show();
				}
			});
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Graph Tradeoffs...", false,
				"Open a window to allow you to define a graph of a selected result" +
				" statistic (a selected State's mean or uncertainty) or Attribute value" +
				" versus an other results statistic (mean or uncertainty for another State)" +
				" or Attribute value.");
			mi.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					(new ScenarioSetGraphDefPanel(getPanelManager(),
						ModelScenarioSetVisualTreeNode.this)).show();
				}
			});
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Delete", false,
				"Delete (from the database) the " +
				HelpWindow.createHref("Model Scenario Sets", "Model Scenario Set") +
				" and all " +
				HelpWindow.createHref("Model Scenarios") + " and " +
				HelpWindow.createHref("Simulation Runs") +
				" derived from it.");
			mi.addActionListener(getDeleteModelElementHandler(this));
			popup.add(mi);
		}
		
		
	  /* From ModelScenarioSetVisual */
		
		public void addSimulationVisual(Integer id, SimulationVisual simPanel)
		{
			//addSimVisual(id, simPanel);
		}

		
		public Set<SimulationVisual> simulationRunCreated(Integer requestId,
			String simNodeId)
		{
			return handleSimulationRunCreated(this, simNodeId);
		}
		
		public void simulationRunsDeleted()
		{
			String[] simRunIds = ((SimRunSetSer)(getNodeSer())).simRunIds;
			for (String simRunId : simRunIds) try
			{
				Set<VisualComponent> viss = identifyVisualComponents(simRunId);
				for (VisualComponent vis : viss) removeVisual(vis);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}

		public void refreshStatistics() {}
	}


	protected Set<SimulationVisual> handleSimulationRunCreated(
		SelectorVisualTreeNode treeNodeTarget,
		String simNodeId) // Update View.
	{
		try { addNode(simNodeId); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		return null;
	}
	
	
  /* Types that are specific to DecisionDomains */
	
	
	private static final ImageIcon DecisionDomainIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.DecisionDomainIconImageName);


	class DecisionDomainVisualTreeNode extends DomainVisualTreeNode<DecisionDomainSer>
		implements DecisionDomainVisual
	{
		public DecisionDomainVisualTreeNode(DecisionDomainSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(DecisionDomainIcon);
		}

		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
		}
		
		
	  /* From ContainerVisual */
	
		public void componentResized(VisualComponent child) throws Exception {} // na
	}


	class DecisionDomainMotifVisualTreeNode extends DecisionDomainVisualTreeNode
		implements MotifDefVisual
	{
		public DecisionDomainMotifVisualTreeNode(DecisionDomainMotifDefSer nodeSer)
		{
			super(nodeSer);
		}

		
		public String getDescriptionPageName() { return "Motifs"; }


		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			addMotifContextMenuItems(this, popup);
		}
	}
	
	
	private static final ImageIcon DecisionScenarioIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.DecisionScenarioIconImageName);


	class DecisionScenarioVisualTreeNode extends SelectorVisualTreeNode
		implements DecisionScenarioVisual
	{
		public DecisionScenarioVisualTreeNode(DecisionScenarioSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(DecisionScenarioIcon);
		}
		
		
		public String getDescriptionPageName() { return "Decision Scenarios"; }


		void performOpen()
		{
			try { getViewFactory().createViewPanel(false, getNodeSer().getDomainId(),
				getNodeSer().getNodeId()); }
			catch (Exception ex) { ErrorDialog.showReportableDialog(DomainSelector.this, ex); }
		}
		
		
	  /* From DecisionScenarioVisual */
		
		public void choiceCreated(String choiceId)
		{
			try { addNode(choiceId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}

		public void decisionsUpdated() {}
	}
	
	
	
  /* Types that are specific to HierarchyDomains */
	
	
	private static final ImageIcon HierarchyDomainIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.HierarchyIconImageName);


	class HierarchyDomainVisualTreeNode extends DomainVisualTreeNode<HierarchyDomainSer>
		implements HierarchyDomainVisual
	{
		public HierarchyDomainVisualTreeNode(HierarchyDomainImplSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(HierarchyDomainIcon);
		}
	}

	
	class HierarchyDomainMotifVisualTreeNode extends HierarchyDomainVisualTreeNode
		implements MotifDefVisual
	{
		public HierarchyDomainMotifVisualTreeNode(HierarchyDomainMotifDefSer nodeSer)
		{
			super(nodeSer);
		}

		
		public String getDescriptionPageName() { return "Motifs"; }


		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			addMotifContextMenuItems(this, popup);
		}
	}
	
	
	private static final ImageIcon HierarchyScenarioIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.HierarchyScenarioIconImageName);


	class HierarchyScenarioVisualTreeNode extends ScenarioVisualTreeNode
		implements HierarchyScenarioVisual
	{
		public HierarchyScenarioVisualTreeNode(HierarchyScenarioImplSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(HierarchyScenarioIcon);
		}
		
		
		public String getDescriptionPageName() { return "Hierarchy Scenarios"; }


		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Open", false,
				"Retrieve the " +
				HelpWindow.createHref("Hierarchy Scenarios", "Hierarchy Scenario") +
				" from the server and open a graphics view of it.");
			mi.addActionListener(getOpenScenarioHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Make Copy", false,
				"Create and display a new, independent copy of the " +
				HelpWindow.createHref("Hierarchy Scenarios", "Hierarchy Scenario") +
				", and assign it a different name. " +
				HelpWindow.createHref("Simulation Runs") + " are not copied.");
			mi.addActionListener(getCopyHierarchyScenarioHandler(this));
			popup.add(mi);
			
			mi = new HelpfulMenuItem("Delete", false,
				"Delete the " +
				HelpWindow.createHref("Hierarchy Scenarios", "Hierarchy Scenario") +
				".");
			mi.addActionListener(getDeleteHierarchyElementHandler(this));
			popup.add(mi);
		}
	}
	
	
	private static final ImageIcon HierarchyScenarioSetIcon = 
		DomainListJTree.createImageIcon(NodeIconImageNames.HierarchyScenarioSetIconImageName);

	class HierarchyScenarioSetVisualTreeNode extends SelectorVisualTreeNode
		implements HierarchyScenarioSetVisual
	{
		public HierarchyScenarioSetVisualTreeNode(HierarchyScenarioSetSer nodeSer)
		{
			super(nodeSer);
			setImageIcon(HierarchyScenarioSetIcon);
		}
		
		
		public String getDescriptionPageName() { return "Hierarchy Scenario Sets"; }


		void performOpen()
		{
			String[] scenIds = ((ScenarioSetSer)(getNodeSer())).getScenarioIds();
			for (String scenId : scenIds)  try // Open each Scenario in the ScenarioSet.
			{
				getViewFactory().createViewPanel(false, getNodeSer().getDomainId(),
					scenId);
			}
			catch (Exception ex) { ErrorDialog.showReportableDialog(DomainSelector.this, ex); }
		}
		
		
		void addContextMenuItems(JPopupMenu popup)
		{
			super.addContextMenuItems(popup);
			
			JMenuItem mi = new HelpfulMenuItem("Delete", false,
				"Delete (from the database) the " +
				HelpWindow.createHref("Hierarchy Scenario Sets", "Hierarchy Scenario Set") +
				" and all " +
				HelpWindow.createHref("Hierarchy Scenarios") + " and " +
				HelpWindow.createHref("Simulation Runs") +
				" derived from it.");
			mi.addActionListener(getDeleteHierarchyElementHandler(this));
			popup.add(mi);
		}
		
		
	  /* From ScenarioSetVisual */
	}
	


  /* ***************************************************************************/
  /* Methods for retrieving menu action handlers */


	protected ActionListener getOpenDomainHandler(DomainVisualTreeNode mdtVisual)
	{
		return new OpenDomainHandler(mdtVisual.getNodeId());
	}
	
	
	protected ActionListener getOpenDomainHandler(DomainVisualTreeNode mdtVisual,
		boolean domainOnly)
	{
		return new OpenDomainHandler(mdtVisual.getNodeId(), domainOnly);
	}
	
	
	protected ActionListener getOpenMotifHandler(MotifDefVisual mdtVisual)
	{
		return new OpenMotifHandler(mdtVisual.getNodeId());
	}
	
	
	protected ActionListener getDeleteModelElementHandler(SelectorVisualTreeNode mdtVisual)
	{
		return new DeleteModelElementHandler(mdtVisual.getNodeId());
	}
	
	
	protected ActionListener getDeleteHierarchyElementHandler(SelectorVisualTreeNode mdtVisual)
	{
		return new DeleteHierarchyElementHandler(mdtVisual.getNodeId());
	}

	
	protected ActionListener getDeleteMotifHandler(MotifDefVisual mtVisual)
	{
		return new DeleteMotifHandler(mtVisual);
	}
	
	
	protected ActionListener getCopyDomainHandler(DomainVisualTreeNode mdtVisual)
	{
		return new CopyDomainHandler(mdtVisual.getNodeId());
	}
	
	
	protected ActionListener getCopyMotifHandler(MotifDefVisual mdtVisual)
	{
		return new CopyMotifHandler(mdtVisual.getNodeId());
	}
	
	
	protected ActionListener getDefineNewScenarioHandler(ModelDomainVisualTreeNode mdtVisual)
	{
		return new DefineNewScenarioHandler(mdtVisual);
	}
	
	
	protected ActionListener getViewExpectedValueByScenarioHandler(ModelDomainVisualTreeNode mdtVisual)
	{
		return new ViewExpectedValueByScenarioHandler(
				this, getPanelManager(), mdtVisual.getNodeId(), null);
	}
	
	
	protected ActionListener getExportAsXMLHandler(VisualComponent mdtVisual)
	{
		return new ExportAsXMLHandler(mdtVisual.getNodeId(), mdtVisual.getName());
	}
	
	
	protected ActionListener getOpenScenarioHandler(ScenarioVisualTreeNode mdtVisual)
	{
		return new OpenScenarioHandler((ScenarioSer)(mdtVisual.getNodeSer()));
	}
	
	
	protected ActionListener getCopyModelScenarioHandler(ModelScenarioVisualTreeNode mstVisual)
	{
		return new CopyModelScenarioHandler(mstVisual.getNodeId());
	}
	
	
	protected ActionListener getCopyHierarchyScenarioHandler(HierarchyScenarioVisualTreeNode mstVisual)
	{
		return new CopyHierarchyScenarioHandler(mstVisual.getNodeId());
	}
	
	
	protected ActionListener getSimulateHandler(ModelScenarioVisualTreeNode mstVisual)
	{
		return new SimulateHandler(mstVisual);
	}
	
	
	protected ActionListener getViewDistributionHandler(ModelScenarioVisualTreeNode mstVisual,
		String distOwnerNodeId, String modelDomainNodeId)
	{
		return new ViewDistributionHandler(this, mstVisual.getNodeId(), distOwnerNodeId, null, 
			modelDomainNodeId, getPanelManager());
	}
	
	
	protected ActionListener getViewExpectedValueOverTimeHandler(
		ModelScenarioVisualTreeNode mstVisual, String distOwnerNodeId, 
		String modelDomainNodeId)
	{
		return new ViewExpectedValueOverTimeHandler(
				this, mstVisual.getNodeId(), distOwnerNodeId, null, modelDomainNodeId, getPanelManager());
	}
	
	
	protected ActionListener getViewValueOverTimeHandler(SimRunVisualTreeNode srtVisual)
	{
		return new ViewValueOverTimeHandler(srtVisual.getNodeId());
	}
	
	
	protected ActionListener getViewEventHistoryHandler(SimRunVisualTreeNode srtVisual)
	{
		return new ViewEventHistoryHandler(srtVisual);
	}
	

	protected ActionListener getViewEventHistoryHandler(SimRunVisualTreeNode srtVisual,
		Class[] stateTags)
	{
		return new ViewEventHistoryHandler(srtVisual, stateTags);
	}


  /* ***************************************************************************/
  /* Menu action handlers */

	
	class OpenDomainHandler implements ActionListener
	{
		private String domainNodeId;
		boolean domainOnly;
		
		
		public OpenDomainHandler(String domainNodeId)
		{
			this(domainNodeId, false);
		}
		
		
		public OpenDomainHandler(String domainNodeId, boolean domainOnly)
		{
			this.domainNodeId = domainNodeId;
			this.domainOnly = domainOnly;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				NodeSer nodeSer = getModelEngine().getNode(domainNodeId);
				getViewFactory().createViewPanel(false, domainNodeId, ! domainOnly);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}
	}
	
	
	class OpenMotifHandler implements ActionListener
	{
		private String motifNodeId;
		
		
		public OpenMotifHandler(String motifNodeId)
		{
			this.motifNodeId = motifNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				//NodeSer nodeSer = getModelEngine().getNode(motifNodeId);
				//MotifDefSer motifDefSer = (MotifDefSer)nodeSer;
				getViewFactory().createViewPanel(false, motifNodeId);
				//getViewFactory().createMotifViewPanel(nodeSer, true);
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}
	}
	
	
	class DeleteModelElementHandler implements ActionListener
	{
		private String modelElementNodeId;
		
		
		public DeleteModelElementHandler(String modelElementNodeId)
		{
			this.modelElementNodeId = modelElementNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				try { getModelEngine().deleteNode(false, modelElementNodeId); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(DomainSelector.this, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					{
						getModelEngine().deleteNode(true, modelElementNodeId);
					}
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
			}
		}
	}
	
	
	class DeleteHierarchyElementHandler implements ActionListener
	{
		private String hierElementNodeId;
		
		
		public DeleteHierarchyElementHandler(String hierElementNodeId)
		{
			this.hierElementNodeId = hierElementNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				try { getModelEngine().deleteNode(false, hierElementNodeId); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(DomainSelector.this, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					{
						getModelEngine().deleteNode(true, hierElementNodeId);
					}
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
			}
		}
	}
	
	
	class DeleteMotifHandler implements ActionListener
	{
		private MotifDefVisual mtVisual;
		
		
		public DeleteMotifHandler(MotifDefVisual mtVisual)
		{
			this.mtVisual = mtVisual;
		}


		public void actionPerformed(ActionEvent e)
		{
			try
			{
				try { getModelEngine().deleteNode(false, mtVisual.getNodeId()); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(DomainSelector.this,
						w.getMessage(), "Warning",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					{
						getModelEngine().deleteNode(true, mtVisual.getNodeId());
					}
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
			}
		}
	}
	
	
	class CopyDomainHandler implements ActionListener
	{
		private String domainNodeId;
		
		
		public CopyDomainHandler(String domainNodeId)
		{
			this.domainNodeId = domainNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try { getModelEngine().createIndependentCopy(domainNodeId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex); return;
			}
		}
	}
	

	class CopyMotifHandler implements ActionListener
	{
		private String motifNodeId;
		
		
		public CopyMotifHandler(String motifNodeId)
		{
			this.motifNodeId = motifNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try { getModelEngine().createIndependentCopy(motifNodeId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex); return;
			}
		}
	}
	
	
	class DefineNewScenarioHandler implements ActionListener
	{
		private ModelDomainVisualTreeNode mdtVisual;
		
		
		public DefineNewScenarioHandler(ModelDomainVisualTreeNode mdtVisual)
		{
			this.mdtVisual = mdtVisual;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try { getModelEngine().createScenario(mdtVisual.getNodeSer().getName()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex); return;
			}
		}
	}
	
	
	class ExportAsXMLHandler implements ActionListener
	{
		private String nodeId;
		private String name;
		
		
		public ExportAsXMLHandler(String nodeId, String name)
		{
			this.nodeId = nodeId;
			this.name = name;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			// Get the XML from the server.
			
			String xml;
			try { xml = getModelEngine().exportNodeAsXML(nodeId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex); return;
			}
			
			
			// Write to an edit panel.
			
			XMLEditPanel editPanel = new XMLEditPanel(this.name);
			editPanel.setSize(800, 400);
			editPanel.setText(xml);
			editPanel.show();
		}
	}


	class OpenScenarioHandler implements ActionListener
	{
		private ScenarioSer scenarioSer;
		
		
		public OpenScenarioHandler(ScenarioSer scenarioSer)
		{
			this.scenarioSer = scenarioSer;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try { openScenarioVisual(scenarioSer); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, 
					"Cannot open view of Domain for Scenario " + 
						scenarioSer.getName(), ex);
				return;
			}
		}
	}


	/**
	 * Open a View of the specified Scenario. If there is already a View
	 * of that Scenario, bring it to the foreground. If the Scenario's ModelDomain is not open,
	 * then open it.
	 */
	 
	ScenarioVisual openScenarioVisual(ScenarioSer scenarioSer)
	throws
		Exception
	{
		ViewContainer viewPanel = getViewFactory().createViewPanel(
			false, scenarioSer.getDomainId(), scenarioSer.getNodeId());
		
		if (viewPanel == null) return null;
		return ((View)viewPanel).showScenario(scenarioSer.getNodeId());
	}
	
	
	class CopyModelScenarioHandler implements ActionListener
	{
		private String modelScenarioNodeId;
		
		
		public CopyModelScenarioHandler(String modelScenarioNodeId)
		{
			this.modelScenarioNodeId = modelScenarioNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try { getModelEngine().copyScenario(modelScenarioNodeId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex); return;
			}
		}
	}

	
	class CopyHierarchyScenarioHandler implements ActionListener
	{
		private String hierScenarioNodeId;
		
		
		public CopyHierarchyScenarioHandler(String hierScenarioNodeId)
		{
			this.hierScenarioNodeId = hierScenarioNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try { getModelEngine().copyScenario(hierScenarioNodeId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex); return;
			}
		}
	}

	
	class SimulateHandler implements ActionListener
	{
		private ModelScenarioVisual modelScenarioVisual;
		
		public SimulateHandler(ModelScenarioVisual scenVis)
		{
			this.modelScenarioVisual = scenVis;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				final SimulationVisualMonitor simMonitor = new SimulationVisualMonitor()
				{
					public void simulationVisualClosing(SimulationVisual simVis)
					{
						//DomainSelector.this.removeSimVisual(simVis.getCallbackId());
					}
				};
	
				String domainId = 
					((ModelScenarioSer)(modelScenarioVisual.getNodeSer())).domainId;
				
				ModelDomainSer domainSer = (ModelDomainSer)(getModelEngine().getNode(
					domainId, ModelDomainSer.class));
				
				(new SimulatePanel(
					DomainSelector.this, 
					this.modelScenarioVisual,
					simMonitor,
					(ModelScenarioSer)(this.modelScenarioVisual.getNodeSer()),
					domainSer,
					DomainSelector.this.getViewFactory(),
					DomainSelector.this.getModelEngine()
					)
				).show();
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}		
		}
	}
	
	
	class ViewValueOverTimeHandler implements ActionListener
	{
		private String simNodeId;
		
		
		public ViewValueOverTimeHandler(String simNodeId)
		{
			this.simNodeId = simNodeId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			JOptionPane.showMessageDialog(DomainSelector.this,
					"Not implemented yet", "Error",
					JOptionPane.ERROR_MESSAGE); return;
		}
	}
	

	class ViewEventHistoryHandler implements ActionListener
	{
		private SimRunVisualTreeNode simVisual;
		private Class[] stateTags;
		
		
		public ViewEventHistoryHandler(SimRunVisualTreeNode simVisual)
		{
			this.simVisual = simVisual;
		}
		
		
		public ViewEventHistoryHandler(SimRunVisualTreeNode simVisual,
			Class[] stateTags)
		{
			this.simVisual = simVisual;
			this.stateTags = stateTags;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try { simVisual.createEventHistoryWindow(stateTags); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(DomainSelector.this, ex);
				return;
			}
		}
	}

	

  /* ***************************************************************************/
  /* Extend JTree in order to override the CellRenderer. */


	static class DomainListJTree extends JTree
	{
		public DomainListJTree(TreeModel model)
		{
			super(model);
			this.setBackground(java.awt.Color.white);
			setRootVisible(true);
			
			this.setCellRenderer(new DefaultTreeCellRenderer()
			{
				public Component getTreeCellRendererComponent(JTree tree, Object value,
					boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
				{
					Component comp = super.getTreeCellRendererComponent(
						tree, value, sel, expanded, leaf, row, hasFocus);
					
					if (value instanceof SelectorVisualTreeNode)
						this.setIcon(((SelectorVisualTreeNode)value).getImageIcon());
					return comp;
				}
			});
		}
		
		
		public Set<TreePath> getAllVisiblePaths()
		{
			Set<TreePath> paths = new HashSet<TreePath>();
			int count = this.getRowCount();
			
			for (int rowNo = 0; rowNo < count; rowNo++) try
			{
				TreePath path = getPathForRow(rowNo);
				paths.add(path);
			}
			catch (ArrayIndexOutOfBoundsException ex) {}  // ignore
			
			return paths;
		}
		
		
		public Dimension getMinimumSize() { return new Dimension(200, 100); }
		
		
		static ImageIcon createImageIcon(String path)
		{
			java.net.URL imgURL = DomainListJTree.class.getResource(path);
			if (imgURL != null)
				return new ImageIcon(imgURL);
			else
			{
				System.err.println("Couldn't find file: " + path);
				return null;
			}
		}
	}
}

