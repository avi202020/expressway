/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.gui.InfoStrip.*;


public class GraphicDomainInfoStripImpl extends DomainInfoStripImpl
	implements GraphicDomainInfoStrip
{
	private GraphicInfoStripHelper helper;
	
	
	public GraphicDomainInfoStripImpl(AbstractGraphicViewPanel viewPanel)
	{
		super((NodeViewPanelBase)viewPanel);
		helper = new GraphicInfoStripHelper(this);
	}
}

