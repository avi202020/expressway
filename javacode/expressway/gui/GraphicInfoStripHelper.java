/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import geometry.PointImpl;
import java.awt.Color;import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.Icon;
import javax.swing.ImageIcon;


/** ************************************************************************
 * Add controls for zooming to an InfoStrip. Merely instantiating this helper
 * is all that is required.
 */
 
public class GraphicInfoStripHelper
{
	public static Icon MagnifyingGlassIcon = null;
	private InfoStrip infoStrip;
	
	
	static
	{
		String path = "/images/MagnifyingGlass.png";
		java.net.URL imgURL = GraphicInfoStripHelper.class.getResource(path);
		if (imgURL != null)
			MagnifyingGlassIcon = new ImageIcon(imgURL);
		else
			throw new Error("Cound not find image file " + path);
	}
	
	
	public GraphicInfoStripHelper(final InfoStrip infoStrip)
	{
		this.infoStrip = infoStrip;
		
		final JPanel zoomBox = new JPanel()
		{
			{
				java.awt.Dimension size = new java.awt.Dimension(20, 20);
				setPreferredSize(size);
				setMinimumSize(size);
				setBackground(Color.white);
				
				JLabel label;
				add(label = new JLabel(MagnifyingGlassIcon));
			}
		};
		
		class ZoomHandler implements ActionListener
		{
			private double pixelSize;
			
			ZoomHandler(int scalePercent)
			{
				this.pixelSize = 100.0 / (double)scalePercent;
			}
			
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					// Determine the Visual center point P.
					
					VisualComponentDisplayAreaImpl displayArea = 
						(VisualComponentDisplayAreaImpl)(getNodeView().getDisplayArea());
					
					int centerX = displayArea.getWidth()/2;
					int centerY = displayArea.getHeight()/2;

					
					// Determine the location Pdomain of P in Model Space.
					
					double domainX = getNodeView().transformViewXCoordToNode(centerX, 0);
					double domainY = getNodeView().transformViewYCoordToNode(centerY, 0);
					
					
					// Change View scale.
					
					getNodeView().setPixelXSize(pixelSize);
					getNodeView().setPixelYSize(pixelSize);
					
					
					// Determine the location Pview of Pdomain in the View space.
					
					int viewX = getNodeView().transformNodeXCoordToView(domainX, 0);
					int viewY = getNodeView().transformNodeYCoordToView(domainY, 0);

					
					// Center the View on that location.
					
					//((JPanel)(getDisplayArea())).removeAll();
					getNodeView().centerOnViewLocation(viewX, viewY);
					
					if (getNodeView() instanceof NodeScenarioView)
						((NodeScenarioView)(getNodeView())).setShowScenarios(false);
					((VisualComponentDisplayAreaImpl)(getNodeView().getDisplayArea())).repaint();
					if (getNodeView() instanceof NodeScenarioView)
						((NodeScenarioView)(getNodeView())).setShowScenarios(true);

					
					//setCurrentScenarioPanel(avp);
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(infoStrip.getComponent(), ex);
				}
				
				((JPanel)(getNodeView().getDisplayArea())).repaint();
			}
		}
		
		zoomBox.addMouseListener(new MouseListener()
		{
			java.awt.Color HighlightColor = java.awt.Color.blue;
			
			public void mousePressed(MouseEvent e)
			{
				int curScalePct = (int)(100.0 / getNodeView().getPixelXSize());
				int newScalePct = curScalePct;
				
				JPopupMenu popup = new JPopupMenu();
				JMenuItem menuItem;
				
				newScalePct = 1000;
				menuItem = new JMenuItem("1000%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 500;
				menuItem = new JMenuItem("500%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 250;
				menuItem = new JMenuItem("250%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 150;
				menuItem = new JMenuItem("150%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 125;
				menuItem = new JMenuItem("125%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 100;
				menuItem = new JMenuItem("100%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 90;
				menuItem = new JMenuItem("90%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 80;
				menuItem = new JMenuItem("80%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 70;
				menuItem = new JMenuItem("70%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 50;
				menuItem = new JMenuItem("50%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 30;
				menuItem = new JMenuItem("30%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				newScalePct = 10;
				menuItem = new JMenuItem("10%");
				menuItem.addActionListener(new ZoomHandler(newScalePct));
				if (PointImpl.areAlmostEqual(newScalePct, curScalePct))
					menuItem.setBackground(HighlightColor);
				popup.add(menuItem);
				
				popup.show(zoomBox, e.getX(), e.getY());
			}

			public void mouseClicked(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseReleased(MouseEvent e)  {}
		});

		
		infoStrip.add(zoomBox);
	}
	
	
	protected GraphicView getNodeView() { return (GraphicView)(infoStrip.getNodeView()); }
}

