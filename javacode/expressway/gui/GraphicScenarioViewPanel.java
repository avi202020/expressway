/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;


public interface GraphicScenarioViewPanel extends NodeScenarioView,
	GraphicDomainViewPanel  //, GraphicView
{
}

