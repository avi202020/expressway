/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import javax.swing.*;
import java.util.Set;
import java.awt.Font;
import javax.swing.JPanel;
import java.awt.Component;
import java.awt.Container;


/**
 * A View that displays Visuals based on the X,Y locations of the Nodes that
 * they depict.
 */
 
public interface GraphicView extends NodeView, GraphicVisualComponentFactory
{
	void computeDisplayAreaLocationAndSize(boolean repositionComponents);
	
	
	/** Adjust the static offset so that the outermost Visual is located at the bottom
		left corner of this View. */
	void positionOutermostVisual();
	
	
	/** ************************************************************************
	 * Create a Visual to represent the specified Node Ser, as a child of the
	 * specified Container. If this View already has a Visual for the Node, do not
	 * make another. Do not populate the Visual with Visuals for the Node's
	 * sub-Elements.
	 */  //Why do I need this? Isn't this method already defined by the Factory interface?
	 
	VisualComponent makeGraphicVisual(NodeSer node, Container container, boolean asIcon)
	throws
		CannotBeInstantiated,
		ParameterError,  // if the component cannot be made due to an erroneous
						// or out-of-range parameter.
		Exception;	// any other error.
	
	
	/* ***************************************************************************
	 ***************************************************************************
	 * Visual transformation from Node to View.
	 */

	/** ************************************************************************
	 * Transorm the specified X Node coordinate to the display space of this View.
	 * 
	 * The offset is the location of the Node's origin in the Visual's display
	 * space, assuming that the Display space offsets are 0.
	 * For the x coordinate this is typically:
	 
	 		visual_left_border_thickness
	 
	 * Any borders should be drawn AROUND the Node space of a Visual: the relative
	 * positions and distances between Node space origins should be preserved by
	 * a Visual. It is as if the Nodes were first drawn without borders, their
	 * positions fixed, and then decorated with borders afterwards. However, since
	 * the Visual's location must account for the border, this means that the 
	 * border's size must be included in the offset.
	 */
	 
	int transformNodeXCoordToView(double modelXCoord, int xOriginOffset)
	throws
		ParameterError;  // if the conversion cannot be performed.
	
	
	/** ************************************************************************
	 * Transorm the specified Y Node coordinate to the display space of this View.
	 *
	 * The offset is the location of the Node's origin in the Visual's display
	 * space, assuming that the Display space offsets are 0. 
	 * For the y coordinate this is typically:
	 
	 		visual_height - visual_bottom_border_thickness
	 
	 * Any borders should be drawn AROUND the Node space of a Visual: the relative
	 * positions and distances between Node space origins should be preserved by
	 * a Visual. It is as if the Nodes were first drawn without borders, their
	 * positions fixed, and then decorated with borders afterwards. However, since
	 * the Visual's location must account for the border, this means that the 
	 * border's size must be included in the offset.
	 */
	 
	int transformNodeYCoordToView(double modelYCoord, int yOriginOffset)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** ************************************************************************
	 * Transform the specified Node width to a width value in this View's
	 * coordinate system.
	 */
	
	int transformNodeDXToView(double nodeWidth)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** ************************************************************************
	 * Transform the specified Node height to a height value in this View's
	 * coordinate system.
	 */
	
	int transformNodeDYToView(double nodeHeight)
	throws
		ParameterError;  // if the conversion cannot be performed.
	
	
	
	
  /* ***************************************************************************
	 ***************************************************************************
	 * Visual transformation from View to Node.
	 */

	/** ************************************************************************
	 * Transform the specified X View display coordinate to the display space
	 * of the Node. Visual component borders are accounted for.
	 */
	 
	double transformViewXCoordToNode(int viewXCoord, int xOriginOffset)
	throws
		ParameterError;  // if the conversion cannot be performed.
	
	
	/** ************************************************************************
	 * Transorm the specified Y View display coordinate to the display space
	 * of the Node. Visual component borders are accounted for.
	 */
	 
	double transformViewYCoordToNode(int viewYCoord, int yOriginOffset)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** ************************************************************************
	 * Transform the specified Visual width to a width value in the Model's
	 * coordinate system.
	 */
	
	double transformViewDXToNode(int viewWidth)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** ************************************************************************
	 * Transform the specified Visual height to a height value in the Model's
	 * coordinate system.
	 */
	
	double transformViewDYToNode(int viewHeight)
	throws
		ParameterError;  // if the conversion cannot be performed.
		
		
	/** ************************************************************************
	 * Modify the size of the Node that is depicted by the specified GraphicVisual.
	 * The server is updated. The Visual is later updated via a Notice sent from
	 * the server. This method normally delegates to the GraphicVisualComponent
	 * method of the same name.
	 */
	 
	void resizeNode(GraphicVisualComponent visual, double dw, double dh, double childShiftX,
		double childShiftY, boolean notify)
	throws
		Exception;
		
		
	/** ************************************************************************
	 * Modify the position of the Node that is depicted by the specified GraphicVisual.
	 * The server is updated. The Visual is later updated via a Notice sent from
	 * the server. This method normally delegates to the GraphicVisualComponent
	 * method of the same name.
	 */
	 
	void moveNode(GraphicVisualComponent visual, double dx, double dy, boolean notify)
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Should be called on a Visual whenever it is moved relative to the View,
	 * allowing the View to be reliably notified of Visual movement so that it
	 * can adjust the location of overlaid elements.
	 */
	 
	void notifyVisualMoved(VisualComponent visual);
	
	
	/** ************************************************************************
	 * Should be called by a Visual whenever it has been resized.
	 */
	 
	void notifyVisualResized(VisualComponent visual);
	
	
	/** ************************************************************************
	 * Should be called by a Visual whenever it is removed from a View. The intention
	 * is that this method should update any non-owning references (aliases) for 
	 * the Visual. The assumption is that the Visual object is still available (has
	 * not been garbage collected) and that it still contains a NodeSer containing
	 * a Node Id identifying the Node whose Visual has been removed. If there
	 * is any kind of error, this method returns silently.
	 */
	 
	void notifyVisualRemoved(VisualComponent visual);
	
	
	/** ************************************************************************
	 * Should be called by a Visual whenever the DisplayArea area occupied by the
	 * Visual has changed. This allows the View to repaint any overlays that it might
	 * have, for the area affected. The coordinates are expressed in the system
	 * of the DisplayArea.
	 */
	 
	void notifyThatDisplayAreaChanged(int x, int y, int width, int height);	

	
	/** ************************************************************************
	 * Shift the View's display offsets so that the specified point is in the middle
	 * of the View's DisplayArea.
	 */
	
	public void centerOnViewLocation(int xView, int yView)
	throws
		Exception;
	
	
	double getPixelXSize();
	
	
	double getPixelYSize();
	
	
	void setPixelXSize(double s);
	
	
	void setPixelYSize(double s);
	
	
	
  /* ***************************************************************************
	 * Display space offsets.
	 */
	
	/** Display space x coordinate of this View's logical (Node space) origin,
	 in Display Area size units. This is the sum of the static and dynamic offsets. */
	
	int getXOriginOffset();
	
	
	/** Display space y coordinate of this View's logical (Node space) origin,
	 in Display Area size units. This is the sum of the static and dynamic offsets. */
		
	int getYOriginOffset();
	
	
	/** Display space x coordinate of this View's logical origin, when this
	 View is created or resized, based on the dimensions of the Display Area.
	 This only changes when the Display Area is resized. */
	
	int getXOriginStaticOffset();


	/** Display space y coordinate of this View's logical origin, when this
	 View is created or resized, based on the dimensions of the Display Area.
	 This only changes when the Display Area is resized.
	 Normally, the static Y offset is the height of the Display Area. */
	
	int getYOriginStaticOffset();
	
	
	/** Offset resulting from scrolling or centering. This gets added to the static
	 offset. */
	
	int getXOriginDynamicOffset();


	/** Offset resulting from scrolling or centering. This gets added to the static
	 offset. */
	
	int getYOriginDynamicOffset();
}

