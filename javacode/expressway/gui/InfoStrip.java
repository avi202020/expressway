/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import javax.swing.JTextField;
import javax.swing.JLabel;


public interface InfoStrip
	extends SwingComponent  // so that type compatibility can be checked
{
	NodeView getNodeView();
	
	
	JTextField setNameField(JTextField field);
	JTextField getNameField();
	
	
	JLabel setModifiedLabel(JLabel label);
	JLabel getModifiedLabel();
	
	
	void update();
	
	
	interface DomainInfoStrip extends InfoStrip
	{
	}
	
	
	interface GraphicDomainInfoStrip extends DomainInfoStrip
	{
	}
	
	
	interface ScenarioInfoStrip extends InfoStrip
	{
		NodeScenarioPanel getScenarioPanel();
	}
	
	
	interface GraphicScenarioInfoStrip extends ScenarioInfoStrip
	{
	}
}

