/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.gui.View.*;
import expressway.gui.GraphicView;
import expressway.common.VisualComponent.*;
import expressway.help.*;


public interface MenuItemContext
{
	View getClientView();
	VisualComponentDisplayArea getDisplayArea();
	ScenarioVisual getCurrentScenarioVisual();
	VisualComponent getVisualComponent();
	HelpfulMenuItem getMenuItem();
	ModelEngineRMI getModelEngine();
}

