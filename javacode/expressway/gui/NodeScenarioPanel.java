/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.gui.InfoStrip.*;


public interface NodeScenarioPanel extends ControlPanel, ScenarioVisual
{
	NodeView getNodeView();


	void setScenarioInfoStrip(ScenarioInfoStrip infoStrip);
	
	
	ScenarioInfoStrip getScenarioInfoStrip();
}

