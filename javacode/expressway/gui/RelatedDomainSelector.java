/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.gui.TreeTablePanelViewBase.CenterPanel;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent;
import expressway.common.VisualComponent.*;
import expressway.help.HelpWindow;
import expressway.help.HelpfulText.*;
import awt.AWTTools;
import swing.MultiHierarchyPanel;
import java.awt.Frame;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Set;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.tree.MutableTreeNode;


/**
 * A popup to enable the user to select a Domain and a NamedReference for mapping
 * to and from another Domain via a CrossMapPanel.
 */
 
public class RelatedDomainSelector extends JDialog
{
	private DomainSelector domainSelector;
	private JComboBox relationSelector;
	private JButton okButton;
	
	
	public RelatedDomainSelector(final TreeTablePanelViewBase currentView)
	throws
		Exception
	{
		super(AWTTools.getContainingFrame(currentView),
			"Select other Domain and Relationship", false);
		
		
		// Create a hierarchical table of the Domains that are available. Gray
		// out those that cannot be selected (such as the Domain being depicted
		// by 'currentView').
		
		domainSelector = DomainSelector.createDomainSelector(currentView.getModelEngine(),
			currentView.getPanelManager(), currentView.getViewFactory(), currentView.getName());
		domainSelector.disableDisplayOfNodes(VisualComponent.class);
		domainSelector.enableDisplayOfNodes(DomainVisual.class);
		domainSelector.disableSelectionOfNodes(currentView.getOutermostNodeId());
		domainSelector.setSelectionListener(new DomainSelector.NodeSelectionListener()
		{
			public void visualSelected(VisualComponent visual)
			{
				// List all the NamedReferences owned by the current View's Domain
				// and the selected Domain.
				
				try
				{
					String[] curViewNrs = 
						currentView.getModelEngine().getNamedReferences(
							currentView.getOutermostNodeSer().getDomainId());
					
					String[] selViewNrs =
						currentView.getModelEngine().getNamedReferences(
							visual.getNodeId());
					
					for (String relation : curViewNrs) relationSelector.addItem(relation);
					for (String relation : selViewNrs) relationSelector.addItem(relation);
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(RelatedDomainSelector.this,
						"While accessing server to obtain Named Relations",
						"Server Error", ex);
				}
			}

			public void hostSelected(MutableTreeNode root)
			{
			}
		});
		
		
		relationSelector = new JComboBox();
		relationSelector.setEditable(false);
		
		okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// Add a new CrossMapPanel and View to the enclosing MultiHierarchyPanel.
				
				MultiHierarchyPanel hierarchyPanel = currentView.getMultiHierarchyPanel();
				
				Set<VisualComponent> viss = domainSelector.getSelected();
				if (viss.size() == 0) return;
				if (viss.size() > 1)
				{
					ErrorDialog.showErrorDialog(RelatedDomainSelector.this,
						"Only select one item at a time in the Domain List");
					return;
				}
				
				VisualComponent vis = null;
				for (VisualComponent v : viss) vis = v;  // will execute once.
				
				String relationName = (String)(relationSelector.getSelectedItem());
				
				TreeTablePanelViewBase leftPanel = currentView;
				View newView;
				try { newView = currentView.getViewFactory().createView(vis.getNodeId()); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(RelatedDomainSelector.this,
						"Unable to create View", ex);
					return;
				}
				
				TreeTablePanelViewBase rightPanel = (TreeTablePanelViewBase)newView;
				
				hierarchyPanel.addCrossMapPanel(new CenterPanel(leftPanel, rightPanel,
					relationName));
				
				hierarchyPanel.addTreeTableComponent(rightPanel);
				
				try { rightPanel.postConstructionSetup(); }
				catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
				
				RelatedDomainSelector.this.dispose();
			}
		});
		
		
		// Arrange the Components.
		
		setLayout(new GridLayout(1, 2));
		
		add(domainSelector);
		
		JPanel rightPanel = new JPanel();
		rightPanel.setBackground(Color.white);
		
		add(rightPanel);
		rightPanel.setLayout(new GridLayout(2, 1));
		
		JPanel selectorPanel = new JPanel();
		selectorPanel.setBackground(Color.white);
		selectorPanel.add(new StatusText(false, "Named References",
			"These are all of the " + HelpWindow.createHref("Named References") +
			" referenced by the View's Domain and the Domain selected in the left panel."));
		selectorPanel.add(relationSelector);
		rightPanel.add(selectorPanel);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.white);
		buttonPanel.add(okButton);
		rightPanel.add(buttonPanel);
		
		setSize(400, 300);
	}
}

