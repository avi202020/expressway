/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.gui.InfoStrip.*;
import expressway.ser.*;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.util.HashSet;
import java.awt.Component;
import java.awt.Container;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JLabel;



/**
 * A ControlJPanel that is also a ScenarioVisual.
 */

public class ScenarioControlJPanel extends ControlJPanel implements NodeScenarioPanel
{
	public static final Color ScenarioColor = new Color(240, 240, 240);
	public static final Color ScenarioHLColor = new Color(240, 200, 200);

	/** The Scenario that is depicted by this Visual. May be null if
		the Scenario is a blank Scenario being constructed by the user. */
	private ScenarioSer scenarioSer;

	private final AttributeValueControlFactory attrValueControlFactory = 
		new AttributeValueControlFactory(this, this);
	
	private final AttributeVisibilityControlFactory attrVisControlFactory = 
		new AttributeVisibilityControlFactory(attrValueControlFactory);
	
	private ImageIcon imageIcon;
	
	
	public ScenarioControlJPanel(NodeViewPanelBase viewPanel, ScenarioSer scenarioSer,
		ScenarioSelector controlPanelSelector)
	{
		super(viewPanel, controlPanelSelector);
		this.scenarioSer = scenarioSer;
	}
	
	
	Serializable getAttributeValue(AttributeVisual visual)
	{
		String[] ids = getScenarioSer().getAttributeNodeIds();
		Serializable[] values = getScenarioSer().getAttributeValues();
		String visId = visual.getNodeId();
		int i = 0;
		for (String id : ids)
		{
			if (id.equals(visId)) return values[i];
			i++;
		}
		
		return null;
	}
	
	
	public ScenarioSer setScenarioSer(ScenarioSer ser) { return this.scenarioSer = ser; }
	public ScenarioSer getScenarioSer() { return this.scenarioSer; }
	
	public JTextField setNameField(JTextField field)
	{
		if (getInfoStrip() == null) return null;
		return getInfoStrip().setNameField(field);
	}
	
	
	public JTextField getNameField()
	{
		if (getInfoStrip() == null) return null;
		return getInfoStrip().getNameField();
	}
	
	
	public JLabel setModifiedLabel(JLabel label)
	{
		if (getInfoStrip() == null) return null;
		return getInfoStrip().setModifiedLabel(label);
	}
	
	
	public JLabel getModifiedLabel()
	{
		if (getInfoStrip() == null) return null;
		return getInfoStrip().getModifiedLabel();
	}

	
	/** ********************************************************************
	 * Update all elements in the Info Strip.
	 */
	 
	protected void updateInfoStrip()
	{
		getInfoStrip().update();
	}
	
	
	
  /* ***********************************************************************
   * From ControlPanel.
   */
	
	public void uncreateValueControl(ValueControl vc)
	{
		if (vc.getControlPanel() == null) return;
		
		// Save the state of the ValueControl in this ControlPanel.
		try
		{
			// Only do this if the value differs from the current db value.
			Serializable fieldValue = vc.getValue();
			Serializable dbValue = getAttributeValue(vc.getVisual().getNodeId());
			if (fieldValue != dbValue)
			{
				if ((fieldValue != null) && (! fieldValue.equals(dbValue)))
					rememberValueControlStateForVisual(vc.getVisual(), vc.getClass(),
						fieldValue);
			}
		}
		catch (ParameterError pe)
		{
			GlobalConsole.println("Error parsing ValueControl value: " + pe.getMessage());
		}
		
		super.uncreateValueControl(vc);
	}

	
	
  /* ***********************************************************************
   * From VisualComponent.
   */
	
	public String getNodeId() { return scenarioSer.getNodeId(); }
	
	
	public String getName() { return super.getName(); }
	//public String getName() { return scenarioSer.getName(); }
	
	
	public String getFullName() { return scenarioSer.getFullName(); }
	
	
	public ImageIcon getImageIcon() { return imageIcon; }
	
	
	public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
	
	
	public String getDescriptionPageName() { return "Model Scenarios"; }


	public int getSeqNo() { return 0; }

	 
	public boolean contains(VisualComponent comp)
	{
		if (comp instanceof Component)
			return ((Component)comp).getParent() == this;
		else
			return false;
	}
	

	public boolean isOwnedByMotifDef()
	{
		return getNodeSer().ownedByMotifDef();
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return getNodeSer().ownedByActualTemplateInstance();
	}

	
	public String[] getChildIds()
	{
		return new String[0];
	}
	

	public void nameChangedByServer(String newName, String newFullName)
	{
		this.scenarioSer.setName(newName);
		this.scenarioSer.setFullName(newFullName);
		this.setName(newName);
		
		getClientView().notifyScenarioNameChanged(this, newName);
	}
	
	
	public void nodeDeleted()  // the Scenario was deleted on the server.
	{
		// Remove this NodeScenarioPanel from the ScenarioSelectorImpl.
		getScenarioViewPanel().removeScenarioVisual(this);
	}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
		NodeSer ser;
		try { ser = getModelEngine().getNode(crossRefId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
		
		if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
			"Node with Id " + crossRefId + " is not a CrossReference");
		CrossReferenceSer crSer = (CrossReferenceSer)ser;
		getClientView().notifyCrossReferenceCreated(crSer);
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
		getClientView().notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
	}
		
		
	public View getClientView() { return getViewPanel(); }
	
	
	public ModelEngineRMI getModelEngine() { return getClientView().getModelEngine(); }
	
	
	public NodeSer getNodeSer() { return this.scenarioSer; }
	
	
	public void setNodeSer(NodeSer nodeSer) { this.scenarioSer = (ModelScenarioSer)nodeSer; }
	
	
	public void update()
	{
		NodeSer nodeSer = null;
		try { nodeSer = getModelEngine().getNode(scenarioSer.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this,  ex);
			return;
		}
			
		this.scenarioSer = (ModelScenarioSer)nodeSer;
		
		update(nodeSer);
	}
	
	
	public void update(NodeSer nodeSer)
	{
		this.scenarioSer = (ModelScenarioSer)nodeSer;

		try { this.refreshRedundantState(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
		}

		this.updateInfoStrip();
	}
	

	public void redraw() { this.repaint(); }		
	

	public void refresh()
	{
		update();
		redraw();
	}
	
	
	public java.awt.Point getLocation()
	{
		return new java.awt.Point(0, 0);
	}

	
	public int getX() { return 0; }
	
	
	public int getY() { return 0; }
	

	public void refreshLocation() {}

	
	public void refreshSize() {}
	
	
	public void setLocation() throws Exception {}
	
	
	public void setSize() throws Exception {}

	
	public void refreshRedundantState()
	throws
		Exception
	{
		if (this.getNameField() != null)
			this.getNameField().setText(this.scenarioSer.getName());
		
		/*if (this.getModifiedLabel() != null)
			this.getModifiedLabel().setText("Version: " +
				this.scenarioSer.getLastUpdated());*/
				
		getClientView().notifyScenarioNameChanged(this, this.scenarioSer.getName());
	}

	
	public boolean getAsIcon() { return false; }  // never display as an icon.
	
	
	public void setAsIcon(boolean yes) {}
	
	
	public boolean useBorderWhenIconified() { return false; }


	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
	}
	
	
	/*public void refreshChildren()
	throws
		ParameterError,
		Exception
	{
	}*/
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		Component[] children = getComponents();
		for (Component child : children)
			if (child instanceof VisualComponent)
				visuals.add((VisualComponent)child);
		
		return visuals;
	}
	
	
	public Object getParentObject() { return this.getParent(); }


	public Container getContainer() { return this; }
	
	
	public Component getComponent() { return this; }
	
	
	public void highlight(boolean yes) {}
	
	
	public boolean isHighlighted() { return false; }
	

	public Color getNormalBackgroundColor() { return ScenarioColor; }


	public Color getHighlightedBackgroundColor() { return ScenarioHLColor; }
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{ // OK
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{ // OK
	}


  /* ***********************************************************************
   * From ScenarioVisual.
   */

	public List<Serializable> getAttributeValues(VisualComponent vis, List<String> attrNames)
	{
		List<Serializable> values = new Vector<Serializable>();
		Set<AttributeVisual> attrViss = getAttributeVisualsFor(vis);
		for (AttributeVisual attrVis : attrViss)
		{
			Set<ValueControl> attrValueControls = getValueControls(attrVis);
			for (ValueControl control : attrValueControls)
			{
				attrNames.add(attrVis.getName());
				Serializable value;
				try { value = control.getValue(); }
				catch (ParameterError pe) { value = control.getValueAsString(); }
				values.add(value);
			}
		}
		
		return values;
	}


	public Serializable getAttributeValue(String attrId)
	{
		ScenarioSer scenSer = (ScenarioSer)(getNodeSer());
		String[] attrIds = scenSer.getAttributeNodeIds();
		Serializable[] attrValues = scenSer.getAttributeValues();
		int i = 0;
		for (String id : attrIds)
			if (id.equals(attrId)) return attrValues[i];
			else i++;
		return null;
	}
	
	
	public void setLocation(VisualComponent visual)
	{
		super.setLocation(visual);
	}


	public void refreshAttributeValue(String attrNodeId)
	throws
		Exception
	{
		AttributeValueControl control = getAttributeControl(attrNodeId);
		if (control == null) return;
		
		if (scenarioSer != null) try // Get the Attribute's current value.
		{
			Serializable currentValue =
				getModelEngine().getAttributeValueById(attrNodeId,
					scenarioSer.getNodeId());
			
			control.setValue(currentValue);
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this,  ex);
		}
	}
	
	
  /* ***********************************************************************
   * From NodeScenarioPanel
   */


	public NodeView getNodeView() { return (NodeView)(getViewPanel()); }


	public void setScenarioInfoStrip(ScenarioInfoStrip infoStrip)
	{
		setInfoStrip(infoStrip);
	}
	
	
	public ScenarioInfoStrip getScenarioInfoStrip()
	{
		return (ScenarioInfoStrip)(getInfoStrip());
	}
	
	
  /* ***********************************************************************
	* From ControlJPanel
	*/
  
	protected AttributeValueControlFactory getAttributeValueControlFactory()
	{
		return attrValueControlFactory;
	}
	
	
	protected AttributeVisibilityControlFactory getAttrVisControlFactory()
	{
		return attrVisControlFactory;
	}
	
	
  /* ***********************************************************************
	* Implementation
	*/


	public NodeScenarioView getScenarioViewPanel() { return (NodeScenarioView)(getViewPanel()); }
}

