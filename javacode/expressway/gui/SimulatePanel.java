/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.Date;
import javax.swing.*;
import expressway.ser.*;
import expressway.gui.ViewFactory;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.help.*;
import generalpurpose.ThrowableUtil;
import generalpurpose.DateAndTimeUtils;
import statistics.FitGammaDistribution;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.GammaDistribution;
import org.apache.commons.math.distribution.GammaDistributionImpl;


/**
 * For initiating Simulation Runs.
 */
 
public class SimulatePanel extends JFrame
{
	public static final Color TranlucentWhite = new Color(255, 255, 255, 127);
	
	private ModelDomainSer mdSer = null;
	private ModelScenarioVisual scenarioVisual = null;
	private ModelScenarioSetVisual scenSetVisual = null;
	
	private ModelScenarioSer modelScenarioSer = null;
	private ModelScenarioSetSer scenSetSer = null;
	
	private View view;
	
	private SimulationVisualMonitor container;
	
	private ViewFactory viewFactory;
	private ModelEngineRMI modelEngine;
	                     
	private JTextField maxSimTimeField;
	private JLabel maxSimTimeLabel;
	private HelpButton maxSimTimeHelpButton;
            
	private JTextField maxIterationsField;
	private JLabel maxIterationsLabel;
	private HelpButton maxIterationsHelpButton;
            
	private JTextField initialEpochField;
	private JLabel initialEpochLabel;
	private HelpButton initialEpochHelpButton;
            
	private JTextField finalEpochField;
	private JLabel finalEpochLabel;
	private HelpButton finalEpochHelpButton;

	private JTextField runsField;
	private JLabel runsLabel;
	private HelpButton runsHelpButton;
            
	private JButton simButton;
	private HelpButton simHelpButton;
            
	private JButton cancelButton;
	        
	private JCheckBox repeatableCheckbox;
	private HelpButton repeatableHelpButton;
	
	private String modelDomainName;
	private String scenarioName;
	private Date initialEpoch;
	private Date finalEpoch;
	private int iterations;
	private int runs;
	private long duration;  // in ms
	private boolean repeatable = true;
	
	private Integer callbackId = null;
	private final NumberFormat numberFormat = NumberFormat.getInstance();
	
	
	/**
	 * For initiating Simulation Runs for a Set of Scenarios (ScenarioSet).
	 */
	 
	public SimulatePanel(
		final View view, 
		final ModelScenarioSetVisual scenSetVisual,
		final SimulationVisualMonitor container,
		final ModelScenarioSetSer scenSetSer,
		final ModelDomainSer mdSer,
		final ViewFactory viewFactory,
		final ModelEngineRMI modelEngine)
	{
		super("Simulate Scenario Set '" + scenSetSer.getName() + "' of '" + mdSer.getName() + "'");
		this.setAlwaysOnTop(true);
		this.mdSer = mdSer;
		this.scenSetVisual = scenSetVisual;
		this.scenSetSer = scenSetSer;
		
		init(view, container, viewFactory, modelEngine);
	}
	
	
	/**
	 * For initiating Simulation Runs for a single Scenario.
	 */
	 
	public SimulatePanel(
		final View view, 
		final ModelScenarioVisual scenarioVisual,
		final SimulationVisualMonitor container,
		final ModelScenarioSer scenarioSer,
		final ModelDomainSer mdSer,
		final ViewFactory viewFactory, 
		final ModelEngineRMI modelEngine)
	{
		super("Simulate Scenario '" + scenarioSer.getName() + "' of '" + mdSer.getName() + "'");
		this.setAlwaysOnTop(true);
		this.mdSer = mdSer;
		this.scenarioVisual = scenarioVisual;
		this.modelScenarioSer = scenarioSer;

		init(view, container, viewFactory, modelEngine);
	}
	
	
	void init(
		final View view, 
		final SimulationVisualMonitor container,
		final ViewFactory viewFactory, 
		final ModelEngineRMI modelEngine)
	{
		this.view = view;
		this.container = container;
		this.viewFactory = viewFactory;
		this.modelEngine = modelEngine;
		
		setLayout(null);
		numberFormat.setMaximumFractionDigits(2);
		
		setBackground(TranlucentWhite);
		setSize(400, 300);
		setLocation(100, 50);
		
		add(maxSimTimeField = new JTextField(""));
		add(maxSimTimeLabel = new JLabel("Max Sim Time (days)"));
		add(maxSimTimeHelpButton = new HelpButton("Max Simulation Time (in days)", 
			HelpButton.Size.Medium,
			"The upper limit of elapsed time permitted for the simulation." +
			" The simulation will terminate if this limit is reached."));

		add(maxIterationsField = new JTextField(""));
		add(maxIterationsLabel = new JLabel("Max Iterations"));
		add(maxIterationsHelpButton = new HelpButton("Max Number of Iterations", 
			HelpButton.Size.Medium,
			"The upper limit of the number of iterations permitted for the simulation." +
			" The simulation will terminate if this limit is reached."));

		add(runsField = new JTextField(""));
		add(runsLabel = new JLabel("Runs"));
		add(runsHelpButton = new HelpButton("Number of Simulations to Perform", 
			HelpButton.Size.Medium,
			"The Scenario is simulated this many times. Each simulation" +
			" produces a Simulation Run."));
		
		add(repeatableCheckbox = new JCheckBox("Repeatable", true));
		add(repeatableHelpButton = new HelpButton("Repeatable", 
			HelpButton.Size.Medium,
			"If checked, the same random key will be used each time simulations" +
			" are re-run. Recommended when testing a model."));

		add(initialEpochField = new JTextField(""));
		add(initialEpochLabel = new JLabel("Start date (opt)"));
		add(initialEpochHelpButton = new HelpButton("Simulation Start Time",
			HelpButton.Size.Medium,
			"The simulated time at which simulation begins. " +
			"Syntax is: Jun 10, 2011 10:00:00 PM EDT. " +
			"If unspecified, the Scenario's start time is used. If that is " +
			"unspecified, the current real time is used."));
		
		add(finalEpochField = new JTextField(""));
		add(finalEpochLabel = new JLabel("End date (opt)"));
		add(finalEpochHelpButton = new HelpButton("Simulation End Time",
			HelpButton.Size.Medium,
			"The simulated time past which simulation may not progress. " +
			"Syntax is: Jun 10, 2011 10:00:00 PM EDT. " +
			"If unspecified, the Scenario's end time is used. If that is " +
			"unspecified, then simulation ends after the specified max sim time,." +
			"or when the maximum iterations have occurred."));
		
		add(simButton = new JButton("Simulate"));
		add(simHelpButton = new HelpButton("Simulate", 
			HelpButton.Size.Medium,
			"Perform the simulations. This is executed on the server." +
			" The progress of the simulations will be displayed in a window," +
			" and simulation can be aborted at any time. When completed, the" +
			" Simulation Runs will be listed in the Domain List panel."));

		add(cancelButton = new JButton("Cancel"));
		
		Font font = maxSimTimeLabel.getFont().deriveFont((float)12.0);
		
		maxSimTimeField.setColumns(10);
		maxSimTimeField.setFont(font);		
		maxSimTimeField.setSize(100, 25);
		maxSimTimeLabel.setSize(150, 15);

		maxIterationsField.setColumns(10);
		maxIterationsField.setFont(font);		
		maxIterationsField.setSize(100, 25);
		maxIterationsLabel.setSize(150, 15);

		runsField.setColumns(10);
		runsField.setFont(font);		
		runsField.setSize(100, 25);
		runsLabel.setSize(150, 15);
		repeatableCheckbox.setSize(100, 15);
		
		initialEpochField.setColumns(15);
		initialEpochField.setFont(font);		
		initialEpochField.setSize(150, 25);
		initialEpochLabel.setSize(150, 15);
		
		finalEpochField.setColumns(15);
		finalEpochField.setFont(font);		
		finalEpochField.setSize(150, 25);
		finalEpochLabel.setSize(150, 15);
		
		simButton.setSize(100, 20);
		cancelButton.setSize(100, 20);

		int rowY = 20;
		int rowHeight = 30;
		
		maxSimTimeField.setLocation(200, rowY);
		maxSimTimeHelpButton.setLocation(maxSimTimeField.getX() + 
			maxSimTimeField.getWidth() + 4, maxSimTimeField.getY());
		maxSimTimeLabel.setLocation(20, rowY);
		rowY += rowHeight;

		maxIterationsField.setLocation(200, rowY);
		maxIterationsHelpButton.setLocation(maxIterationsField.getX() + 
			maxIterationsField.getWidth() + 4, maxIterationsField.getY());
		maxIterationsLabel.setLocation(20, rowY);
		rowY += rowHeight;

		runsField.setLocation(200, rowY);
		runsHelpButton.setLocation(runsField.getX() + 
			runsField.getWidth() + 4, runsField.getY());
		runsLabel.setLocation(20, rowY);
		rowY += rowHeight;

		repeatableCheckbox.setLocation(200, rowY);
		repeatableHelpButton.setLocation(repeatableCheckbox.getX() + 
			repeatableCheckbox.getWidth() + 4, repeatableCheckbox.getY());
		rowY += rowHeight;
		rowY += 10;

		initialEpochField.setLocation(200, rowY);
		initialEpochLabel.setLocation(20, rowY);
		initialEpochHelpButton.setLocation(initialEpochField.getX() + 
			initialEpochField.getWidth() + 4, initialEpochField.getY());
		rowY += rowHeight;

		finalEpochField.setLocation(200, rowY);
		finalEpochLabel.setLocation(20, rowY);
		finalEpochHelpButton.setLocation(finalEpochField.getX() + 
			finalEpochField.getWidth() + 4, finalEpochField.getY());
		rowY += rowHeight;		
		rowY += rowHeight;		
		
		simButton.setLocation(20, rowY);
		simHelpButton.setLocation(simButton.getX() + 
			simButton.getWidth() + 4, simButton.getY());

		cancelButton.setLocation(250, rowY);
		
		
		repeatableCheckbox.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
					SimulatePanel.this.repeatable = true;
				else if (e.getStateChange() == ItemEvent.DESELECTED)
					SimulatePanel.this.repeatable = false;
				else throw new RuntimeException("Unexpected value for item state change");
			}
		});
		
		
		simButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try { getValues(); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(SimulatePanel.this, ex);
					return; // leave the panel up.
				}
				
				NodeSer nodeSer = (modelScenarioSer == null ? scenSetSer : modelScenarioSer);
				
				try { SimulatePanel.this.view.getListenerRegistrar().subscribe(
					nodeSer.getNodeId(), true); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(SimulatePanel.this, ex); return;
				}
				
				Integer callbackId = null;
				
				try
				{
					if (scenarioVisual == null)
						callbackId = getModelEngine().simulateScenarioSet(
							scenSetSer.getNodeId(),
							initialEpoch, finalEpoch, 
							iterations, false, runs, duration, view.getPeerListener(), 
							SimulatePanel.this.repeatable);
					else
						callbackId = getModelEngine().simulate(
							modelDomainName, scenarioName, 
							initialEpoch, finalEpoch,
							iterations, false, runs, duration, view.getPeerListener(),
							SimulatePanel.this.repeatable);
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(SimulatePanel.this, ex);
					return; // leave the panel up.
				}
				
				SimulatePanel.this.callbackId = callbackId;
				
				SimulationControlPanel simControlPanel = null;
				
				if (scenarioVisual == null)
				{
					simControlPanel = new SimulationControlPanel(view,
						container,
						scenSetSer,  //
						mdSer, viewFactory, modelEngine,
						SimulatePanel.this.callbackId, SimulatePanel.this.duration,
						SimulatePanel.this.iterations, SimulatePanel.this.runs);
					
					SimulatePanel.this.scenSetVisual.addSimulationVisual(
						callbackId, simControlPanel);
				}
				else
				{
					simControlPanel = new SimulationControlPanel(view,
						container,
						modelScenarioSer,  //
						mdSer, viewFactory, modelEngine,
						SimulatePanel.this.callbackId, SimulatePanel.this.duration,
						SimulatePanel.this.iterations, SimulatePanel.this.runs);
					
					SimulatePanel.this.scenarioVisual.addSimulationVisual(
						callbackId, simControlPanel);
				}

				simControlPanel.show();
				
				
				dispose();
			}
		});
		
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// If simulation is running, attempt to cancel it.
				
				if (callbackId != null)
				try
				{
					getModelEngine().abort(callbackId.intValue());
					callbackId = null;
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(SimulatePanel.this, ex);
					return; // leave the panel up.
				}
				
				
				dispose();
			}
		});
	}
	
	
	ModelEngineRMI getModelEngine() { return this.modelEngine; }
	
	
	void getValues()
	throws
		Exception
	{
		String modelDomainNodeId;
		if (modelScenarioSer == null)
		{
			modelDomainNodeId = scenSetSer.domainId;
		}
		else
		{
			modelDomainNodeId = modelScenarioSer.domainId;
			scenarioName = modelScenarioSer.getName();
		}
		
		NodeSer modelDomainNodeSer = getModelEngine().getNode(modelDomainNodeId);
		modelDomainName = modelDomainNodeSer.getName();
		
		
		// Parse start date and end date.
		
		String newFromDateStr = initialEpochField.getText().trim();
		String newToDateStr = finalEpochField.getText().trim();
		
		DateFormat df = DateAndTimeUtils.DateTimeFormat;
		
		Date newFromDate = initialEpoch;
		if (newFromDateStr.equals(""))
			newFromDate = null;
		else
			newFromDate = df.parse(newFromDateStr);
		
		Date newToDate = finalEpoch;
		if (newToDateStr.equals(""))
			newToDate = null;
		else
			newToDate = df.parse(newToDateStr);
		
		initialEpoch = newFromDate;
		finalEpoch = newToDate;
		
		iterations = (numberFormat.parse(maxIterationsField.getText())).intValue();
		runs = (numberFormat.parse(runsField.getText())).intValue();
		duration = (numberFormat.parse(maxSimTimeField.getText())).longValue()
			* DateAndTimeUtils.MsInADay;  // convert from days to ms.
	}
}

