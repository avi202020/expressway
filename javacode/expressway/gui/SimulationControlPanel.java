/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.gui.ViewFactory;
import expressway.help.*;
import generalpurpose.ThrowableUtil;
import generalpurpose.DateAndTimeUtils;
import statistics.FitGammaDistribution;
import java.awt.*;
import java.awt.event.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.*;
import javax.swing.*;
import javax.swing.WindowConstants.*;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.GammaDistribution;
import org.apache.commons.math.distribution.GammaDistributionImpl;


/**
 * A Panel for displaying the progress of a set of concurrent simulations (a
 * SimRunSet) of a ModelScenario.
 */
 
public class SimulationControlPanel extends JFrame implements SimulationVisual
{
	public static final Color TranlucentWhite = new Color(255, 255, 255, 127);
	
	private View view;
	private SimulationVisualMonitor container;
	//private ModelScenarioSer modelScenarioSer;
	private ViewFactory viewFactory;
	private ModelEngineRMI modelEngine;
	
	private ModelScenarioSer scenarioSer = null;
	private ModelScenarioSetSer scenSetSer = null;
	private JLabel simTimeLabel;
	private JLabel simTimeLabelLabel;
	private JLabel iterationsLabel;
	private JLabel iterationsLabelLabel;
	private JLabel runsLabel;
	private JLabel runsLabelLabel;
	private JLabel avgProgLabel;
	private JLabel avgProgLabel2;
	private JLabel completedLabel;
	
	private JButton stopButton;
	private HelpButton stopButtonHelpButton;
	
	private JProgressBar timeProgressBar;
	private HelpButton timeProgressBarHelpButton;
	
	private JProgressBar iterProgressBar;
	private HelpButton iterProgressBarHelpButton;
	
	private JTextArea msgArea;
	private JScrollPane msgScrollPane;
	private JLabel msgAreaLabel;
		
	
	private String modelDomainName;
	private String scenarioName;

	private long duration;  // in ms.
	private int iterations;
	private int runs;
	private int runsCompleted = 0;
	
	private Integer callbackId;
	private NumberFormat numberFormat = NumberFormat.getInstance();
	
	/** Maps a SimulationRun's Node ID to its name. */
	private Map<String, String> simNames = new HashMap<String, String>();
	
	/** Maps a SimulationRun's Node ID to its most recently reported number of
		iterations. */
	private Map<String, Integer> iterProgressMap = new HashMap<String, Integer>();
	
	/** Maps a SimulationRun's Node ID to its most recently reported elapsed
		simulation time. */
	private Map<String, Long> timeProgressMap = new HashMap<String, Long>();
	
	private ImageIcon imageIcon;
	
	
	public SimulationControlPanel(View view,
		SimulationVisualMonitor container, 
		ModelScenarioSetSer scenSetSer,
		ModelDomainSer mdSer, ViewFactory viewFactory, ModelEngineRMI modelEngine,
		final Integer callbackId, long duration, int iterations, int runs)
	{
		super("Simulating '" + scenSetSer.getName() + "' of '" + mdSer.getName() + "'");
		
		this.scenSetSer = scenSetSer;
		this.setAlwaysOnTop(true);
		init(view, container, viewFactory, modelEngine, callbackId, duration, iterations, runs);
	}
	
	
	public SimulationControlPanel(View view,
		SimulationVisualMonitor container, 
		ModelScenarioSer scenarioSer,
		ModelDomainSer mdSer, ViewFactory viewFactory, ModelEngineRMI modelEngine,
		final Integer callbackId, long duration, int iterations, int runs)
	{
		super("Simulating '" + scenarioSer.getName() + "' of '" + mdSer.getName() + "'");
		
		this.scenarioSer = scenarioSer;
		this.setAlwaysOnTop(true);
		init(view, container, viewFactory, modelEngine, callbackId, duration, iterations, runs);
	}
		
	
	void init(
		View view,
		SimulationVisualMonitor container, 
		ViewFactory viewFactory, 
		ModelEngineRMI modelEngine,
		final Integer callbackId, long duration, int iterations, int runs)
	{
		this.view = view;
		this.container = container;
		this.viewFactory = viewFactory;
		this.modelEngine = modelEngine;
		this.callbackId = callbackId;
		this.duration = duration;
		this.iterations = iterations;
		this.runs = runs;
		
		setLayout(null);
		numberFormat.setMaximumFractionDigits(2);
		
		setBackground(Color.white);
		//setBackground(TranlucentWhite);
		setSize(400, 450);
		setLocation(100, 50);
		
		add(simTimeLabel = new JLabel(String.valueOf(duration/DateAndTimeUtils.MsInADay)));
		add(simTimeLabelLabel = new JLabel("Max days"));

		add(iterationsLabel = new JLabel(String.valueOf(iterations)));
		add(iterationsLabelLabel = new JLabel("Max iterations"));

		add(runsLabel = new JLabel(String.valueOf(runs)));
		add(runsLabelLabel = new JLabel("Runs"));
		
		add(avgProgLabel = new JLabel("Avg Progress"));
		add(avgProgLabel2 = new JLabel("for All Runs"));
		
		add(completedLabel = new JLabel(""));

		add(stopButton = new JButton("Stop All Runs"));
		add(stopButtonHelpButton = new HelpButton("Stop All Runs", HelpButton.Size.Medium,
			"Gracefully abort all simulations that are being monitored" +
			" by this Simulation Control Panel."));
		
		add(msgScrollPane = new JScrollPane(msgArea = new JTextArea(5, 50)));
		msgArea.setEditable(false);
		add(msgAreaLabel = new JLabel("Messages:"));

		
		Font font = simTimeLabelLabel.getFont().deriveFont((float)12.0);
		
		simTimeLabel.setSize(100, 25);
		simTimeLabelLabel.setSize(150, 15);

		iterationsLabel.setSize(100, 25);
		iterationsLabelLabel.setSize(150, 15);

		runsLabel.setSize(100, 25);
		runsLabelLabel.setSize(150, 15);
		
		avgProgLabel.setSize(100, 15);
		avgProgLabel2.setSize(100, 15);
		
		completedLabel.setSize(200, 15);
		
		stopButton.setSize(100, 20);

		
		simTimeLabel.setLocation(150, 75);
		simTimeLabelLabel.setLocation(20, 80);

		iterationsLabel.setLocation(150, 105);
		iterationsLabelLabel.setLocation(20, 110);

		runsLabel.setLocation(150, 135);
		runsLabelLabel.setLocation(20, 140);
		
		avgProgLabel.setHorizontalAlignment(SwingConstants.CENTER);
		avgProgLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		
		avgProgLabel.setLocation(250, 30);
		avgProgLabel2.setLocation(250, 50);
		
		completedLabel.setLocation(150, 200);
		
		stopButton.setLocation(250, 230);
		stopButtonHelpButton.setLocation(stopButton.getX() + stopButton.getWidth()
			+ 4, stopButton.getY());


		//setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowListener()
		{
			public void windowActivated(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			
			public void windowClosing(WindowEvent e)
			{
				SimulationControlPanel.this.container.simulationVisualClosing(
					SimulationControlPanel.this);
			}
			
			public void windowDeactivated(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowOpened(WindowEvent e) {}
		});
		
		
		stopButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				// If simulation is running, attempt to cancel it.
				
				try
				{
					getModelEngine().abort(callbackId.intValue());
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(SimulationControlPanel.this, ex);
					return; // leave the panel up.
				}
			}
		});
		
		
		int timeMin = 0;
		int timeMax = (int)(duration / DateAndTimeUtils.MsInADay);
		timeProgressBar = new JProgressBar(javax.swing.SwingConstants.HORIZONTAL,
			timeMin, timeMax);
		timeProgressBarHelpButton = new HelpButton("Average Progress: Simulation Time",
			HelpButton.Size.Medium, "Elapsed simulated time as a fraction of" +
			" the maximum allowed time, averaged across all simulation runs.");
		
		timeProgressBar.setValue(timeMin);
		
		int iterMin = 0;
		int iterMax = 0;
		if (scenarioSer == null) iterMax = iterations * scenSetSer.scenarioIds.length;
		else iterMax = iterations;

		iterProgressBar = new JProgressBar(javax.swing.SwingConstants.HORIZONTAL,
			iterMin, iterMax);
		iterProgressBarHelpButton = new HelpButton("Average Progress: Iterations",
			HelpButton.Size.Medium, "Number of iterations simulated so far," +
			" as a fraction of the iteration limit, averaged across all simulation runs.");
			
		iterProgressBar.setValue(iterMin);
		
		timeProgressBar.setSize(100, 20);
		iterProgressBar.setSize(100, 20);
		
		add(timeProgressBar);
		add(timeProgressBarHelpButton);
		
		add(iterProgressBar);
		add(iterProgressBarHelpButton);
		
		timeProgressBar.setLocation(250, 80);
		timeProgressBarHelpButton.setLocation(timeProgressBar.getX() + timeProgressBar.getWidth()
			+ 4, timeProgressBar.getY());

		iterProgressBar.setLocation(250, 110);
		iterProgressBarHelpButton.setLocation(iterProgressBar.getX() + iterProgressBar.getWidth()
			+ 4, iterProgressBar.getY());
		
		msgScrollPane.setSize(350, 100);
		msgScrollPane.setLocation(20, 300);
		
		msgAreaLabel.setSize(100, 25);
		msgAreaLabel.setLocation(20, 275);
	}
	
	
	public ModelEngineRMI getModelEngine() { return this.modelEngine; }



  /*
   * From SimRunSetVisual:
   */

	public void simRunSetCreated(String simRunSetId, String modelScenarioId)
	{
	}
	
	
	public void refreshStatistics() {}
	
	

  /*
   * From SimulationVisual:
   */

	public Integer getCallbackId() { return callbackId; }
	
	
	public void simulationRunCreatedForSimulation(String simNodeId, String simRunName)
	{
		simNames.put(simNodeId, simRunName);
	}
	
	
	public void totalEpochsCompletedForSim(String simNodeId, int noOfEpochs)
	{
		// Record the new progress of the specified Simulation Run.
		
		iterProgressMap.put(simNodeId, new Integer(noOfEpochs));
		

		// Overall progress = sum(progress of each run)/(no. of runs)
		
		int total = 0;
		Collection<Integer> iters = iterProgressMap.values();
		for (Integer iter : iters) total += iter;
		if (scenarioSer == null) total /= runs * scenSetSer.scenarioIds.length;
		else total /= runs;
		
		iterProgressBar.setValue(total);
	}
	
	
	public void totalTimeElapsedForSim(String simNodeId, long ms)
	{
		// Record the new progress of the specified Simulation Run.
		
		timeProgressMap.put(simNodeId, new Long(ms));
		
		
		// Overall progress = sum(progress of each run)/(no. of runs)
		
		long total = 0;
		Collection<Long> times = timeProgressMap.values();
		for (Long time : times) total += time;
		
		if (scenarioSer == null) total /= (DateAndTimeUtils.MsInADay * runs * scenSetSer.scenarioIds.length);
		else total /= (DateAndTimeUtils.MsInADay * runs);
		
		timeProgressBar.setValue((int)total);
	}
	
	
	public void simulationCompleted(String simNodeId)
	{
		String simRunName = simNames.get(simNodeId);
		if (simRunName == null) simRunName = "(SimRun Node " + simNodeId + ")";
		msgArea.append(simRunName + " completed.\n");
		
		runsCompleted++;
		
		if (runsCompleted == runs)
		{
			completedLabel.setText("All Simulations Completed");
		}
	}
	

	public void errorReportedForSim(String simNodeId, String msg)
	{
		String simRunName = simNames.get(simNodeId);
		if (simRunName == null) simRunName = "(SimRun Node " + simNodeId + ")\n";
		msgArea.append(simRunName + ": " + msg + "\n");
	}


  /*
   * From VisualComponent:
   */

	public String getNodeId()
	{
		throw new RuntimeException("Should not be called");
	}

	
	public String getName()
	{
		return super.getName();
	}
	
	
	public String getFullName() { throw new RuntimeException("Should not be called"); }
	

	public ImageIcon getImageIcon()
	{
		return this.imageIcon;
	}
	
	
	public void setImageIcon(ImageIcon imgIcon)
	{
		this.imageIcon = imageIcon;
	}
	
	
	public String getDescriptionPageName()
	{
		return "Simulation Runs";
	}
	
	
	public int getSeqNo() { return 0; }
	
		 
	public boolean contains(VisualComponent comp)
	{
		return false;
	}
	

	public boolean isOwnedByMotifDef() { return false; }
	
	
	public boolean isOwnedByActualTemplateInstance() { return false; }


	public String[] getChildIds()
	{
		return new String[0];
	}
	
	
	public void nameChangedByServer(String newName, String newFullName) {}
	
	
	public void nodeDeleted() {}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
	}
		
		
	public View getClientView()
	{
		return this.view;
	}
	
	
	public NodeSer getNodeSer()
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public void setNodeSer(NodeSer nodeSer)
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public void update()
	{
		throw new RuntimeException("Should not be called");
	}
	

	public void update(NodeSer nodeSer)
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public void redraw()
	{
		throw new RuntimeException("Should not be called");
	}
	

	public void refresh()
	{
		throw new RuntimeException("Should not be called");
	}

	
	public void refreshLocation()
	{
		throw new RuntimeException("Should not be called");
	}

	
	public void refreshSize()
	{
		throw new RuntimeException("Should not be called");
	}

	
	public void setLocation()
	throws
		Exception
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public void setSize()
	throws
		Exception
	{
		throw new RuntimeException("Should not be called");
	}


	public void refreshRedundantState()
	throws
		Exception
	{
		throw new RuntimeException("Should not be called");
	}

	
	public boolean getAsIcon()
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public void setAsIcon(boolean yes)
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public boolean useBorderWhenIconified() { return true; }
	
	
	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	/*public void refreshChildren()
	throws
		ParameterError,
		Exception
	{
		throw new RuntimeException("Should not be called");
	}*/
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public Object getParentObject()
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public Container getContainer() { return this; }
	
	
	public Component getComponent() { return this; }
	
	
	public void highlight(boolean yes)
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public boolean isHighlighted() { throw new RuntimeException("Should not be called"); }
	
	
	public Color getNormalBackgroundColor()
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public Color getHighlightedBackgroundColor()
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
	}
}

