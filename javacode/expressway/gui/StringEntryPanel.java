/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;


/**
 * A Panel for enabling the user to enter or select a value for an Attribute, either
 * for a particular Scenario or for all Scenarios: there is a separate contstructor
 * for each case.
 */
 
public class StringEntryPanel extends JFrame
{
	private ValueReturner valueReturner = null;
	private JTextField inputField;
	private JButton okButton;
	private JButton cancelButton;
	
	
	/*public StringEntryPanel(String title, final ValueReturner valueReturner)
	{
		setValueReturner(valueReturner);
	
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String text = inputField.getText();
				valueReturner.returnStringValue(text);
			}
		});
	}*/
	
	
	public StringEntryPanel(String title)
	{
		this(title, true);
	}
	
	
	public StringEntryPanel(String title, boolean show)
	{
		super(title);
		
		this.setLayout(new FlowLayout());
		this.setAlwaysOnTop(true);
		this.setSize(400, 80);
		
		add(inputField = new JTextField());
		add(okButton = new JButton("OK"));
		add(cancelButton = new JButton("Cancel"));
		
		inputField.setColumns(15);
		inputField.setSize(40, 10);
		
		if (show) this.show();
	}
	
	
	public void setText(String text)
	{
		inputField.setText(text);
	}
	
	
	public void setValueReturner(final ValueReturner vr)
	{
		this.valueReturner = vr;
	
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String text = inputField.getText();
				vr.returnStringValue(text);
			}
		});
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				StringEntryPanel.this.dispose();
			}
		});
	}

	
	public interface ValueReturner
	{
		void returnStringValue(String value);
	}
}

