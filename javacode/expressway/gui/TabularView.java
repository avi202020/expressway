/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;


import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import java.awt.Color;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;


/**
 * A View that displays Visuals in a Tree-Table layout, based on the parent-child
 * hierarchy of the corresponding Nodes.
 */
 
public interface TabularView extends NodeView, TabularVisualComponentFactory
{
	NodeSer getRootNodeSer();
	
	
	VisualComponent makeTabularVisual(NodeSer nodeSer)
	throws
		CannotBeInstantiated,
		ParameterError,  // if the component cannot be made due to an erroneous
						// or out-of-range parameter.
		Exception;	// any other error.
	
	
	void removeVisual(VisualComponent visual)
	throws
		Exception;
	
	
	FieldEditor getEditor(String columnName, int row);
	
	
	JXTreeTable getTreeTable();
	
	
	Color getBackgroundSelectionColor();
	
	
	Color getBackgroundNonSelectionColor();
	
	
	DefaultTreeTableModel getTreeTableModel();
	
	
	void repaintVisual(VisualComponent visual);
	
	
	void setName(String name);
	
	
	interface FieldEditor extends TableCellEditor, TreeCellEditor
	{
		void clear() throws Warning, Exception;
		
		void show() throws NotVisible;
	}
}

