/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.SimulationVisual;
import expressway.common.VisualComponent.ScenarioVisual;
import expressway.ser.CrossReferenceSer;
import expressway.gui.ControlPanel;
import javax.swing.*;
import java.util.Set;
import java.util.List;
import java.io.IOException;
import java.awt.Container;
import java.awt.Component;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.MouseListener;


/**
 * A View is a client-side container for VisualComponents that represent server-side
 * components. A View may subscribe to receive information about updates to the
 * server-side components, by registering a PeerListener with the server.
 */
 
public interface View
	extends
		ClientView,
		SwingComponent  // requires that every implementation extends a JComponent
{
	/**
	 * Adjust layout of Components so that they are properly sized and placed.
	 * Perform initialization that depends on the instantiation of this View.
	 * Return this Panel. This method should always be called after instantiating
	 * this View Panel, once the View Panel has been added to its Container.
	 * However, new Components should not be added by this method.
	 */
	 
	void postConstructionSetup()
	throws
		Exception;


	VisualComponentDisplayArea getDisplayArea();
	
	
	/** A panel containing information about the View. The info panel is normally
		situated above the display area in the View. It normally contains
		a ScenarioSelector. */
	JPanel getInfoPanel();
	
	
	/** For setting the dimensions and location of the Display Area. */
	void computeDisplayAreaLocationAndSize();
	
	
	/** ************************************************************************
	 * Return the ClientMainApplication that this and the other Views of the current
	 * user interface Java application.
	 */
	 
	ClientMainApplication getClientMainApplication();
	
	
	void registerPeerListener()
	throws
		Exception;
	
	
	void unregisterPeerListener()
	throws
		Exception;
	
	
	/** ************************************************************************
	 * Return true if this View is a popup View (i.e., is not part of the main
	 * Window). */
	 
	boolean isPopup();
	
	
	/** ************************************************************************
	 * Return the ControlPanel that is currently displayed by this View. If none
	 * is currently displayed, return null.
	 */
	 
	ControlPanel getCurrentControlPanel();

	
	/** ************************************************************************
	 * Return the display constants that are defined for this View. Each
	 * View may have its own display constants to define the size of standard
	 * visual elements such as a border's thickness.
	 */
	 
	ViewConstants getDisplayConstants();
	

	/** ************************************************************************
	 * Find the Image identified by the specified handle. If the Image is stored
	 * or cached locally, return the local copy. Otherwise, request the Image
	 * from the ModelEngine. If the Image is not found, return null.
	 */
	 
	ImageIcon getStoredImage(String handle)
	throws
		IOException;  // if there is an error contacting the ModelEngine.
	
	
	/** ************************************************************************
	 * Return the ViewFactory that was used to create this View.
	 */
	 
	ViewFactory getViewFactory();
	
	
	/** ************************************************************************
	 * Return the factory to be used for creating instances of VisualComponents
	 * for this View.
	 */
	 
	VisualComponent.VisualComponentFactory getVisualFactory();
	
	
	/** ************************************************************************
	 * Attempt to save changes to fields that save when they lose focus.
	 */
	 
	void saveFocusFieldChanges();
	
	
	/** ************************************************************************
	 * Identify the Visual Class that should be used by this View to depict the
	 * specified Node Ser.
	 */
	
	Class getVisualClass(NodeSer node)
	throws
		CannotBeInstantiated;
	
	
	/** ************************************************************************
	 * If yes is true, add the specified Visual to this View's Set of selected
	 * Visuals. If yes is false, remove it.
	 */
	
	void select(VisualComponent visual, boolean yes);
	
	
	/** ************************************************************************
	 * If 'yes' is true, select all of the Visuals in this View that depict the
	 * Node with the specified Id. Otherwise, de-select those same Visuals.
	 */
	 
	void selectNodesWithId(String nodeId, boolean yes)
	throws
		ParameterError;
	

	/** ************************************************************************
	 * Return a copy of the Set of currently selected Visuals.
	 */
	 
	Set<VisualComponent> getSelected();
	
	
	/** ************************************************************************
	 * Return true if the specified Visual is selected.
	 */
	 
	boolean isSelected(VisualComponent visual);
	
	
	/** ************************************************************************
	 * If yes is true, add all of this View's Visuals to the Set of selected
	 * Visuals. Otherwise, remove all, so that the Set is empty.
	 */
	 
	void selectAll(boolean yes);


	PeerListener getPeerListener();
	
	
	ListenerRegistrar getListenerRegistrar();
	
	
	/** ************************************************************************
	 * Return the JTabbedPanel Container that owns the Component that implements
	 * this View.
	 */
	 
	PanelManager getPanelManager();
	
	
	/** ************************************************************************
	 * Return the AWT Window that contains this View.
	 */
	 
	Window getWindow();
	
	
	/** Pass all mouse click events to this handler. Once the handler is invoked,
		remove it. If this is called multiple times, only the most recent handler
		is used. */
	void addTemporaryMouseListener(MouseListener listener);
	
	
	/** Remove the temporary mouse click handler, if any. */
	void removeTemporaryMouseListener();
	
	
	void reportBug();
	
	
	
  /* ***************************************************************************
	 * Nested types.
	 */
	
	/**
	 * Classes that implement reusable functionality for View.
	 */
	 
	public interface ViewHelper
	{
	}
	
	
	/**
	 * The region within which Visual Components are to be displayed.
	 * Has no borders or insets. This Panel should be instantiated within a
	 * View, and all Visual Components should be instantiated within
	 * the View's VisualComponentDisplayArea.
	 */
	 
	public interface VisualComponentDisplayArea extends SwingComponent
	{
		void clear();
		
		View getView();
		
		
		/**
		 * Return the location in this Display Area of the specified location, as 
		 * expressed in the space of the specified Component
		 */
		 
		java.awt.Point translateVisualToDisplayArea(VisualComponent c,
			java.awt.Point visualLocation);
	
	
		/**
		 * Return the location, relative to this Display Area, of the specified
		 * Visual. If the Visual is not found, return null.
		 */
		 
		java.awt.Point getLocationOfVisual(VisualComponent visual);
		
		
		int getWidth();
		
		int getHeight();
		
		void add(Component comp, Object constraints);
		
		Component[] getComponents();
	}
}

