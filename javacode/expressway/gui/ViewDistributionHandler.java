/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.ser.StateSer;
import expressway.ser.ModelDomainSer;
import expressway.ser.ModelElementSer;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.gui.graph.GraphPanel;
import graph.*;
import statistics.HistogramDomainParameters;
import generalpurpose.ThrowableUtil;
import awt.AWTTools;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import java.awt.Component;
import java.util.Vector;
import java.util.List;


/**
 * Create a Graph that depicts the distribution of expected value of the
 * specified State(s), based on all simulations of a Scenario.
 */

public class ViewDistributionHandler implements ActionListener
{
	private View view;
	private String scenarioId;
	private String distOwnerId;
	private String stateId;  // may be null
	private String modelDomainId;  // only used if stateId is null.
	private PanelManager container;
	
	
	public ViewDistributionHandler(View view, String scenarioId, String distOwnerId,
		String stateId,
		String modelDomainId, PanelManager container)
	{
		this.view = view;
		this.scenarioId = scenarioId;
		this.distOwnerId = distOwnerId;
		this.stateId = stateId;  // may be null: if null, then modelDomainId is used.
		this.modelDomainId = modelDomainId;
		this.container = container;
		
		if (scenarioId == null) throw new RuntimeException("Scenario Id may not be null");
		if (distOwnerId == null) throw new RuntimeException("Dist Owner Id may not be null");
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
		// Identify the States.
		
		List<String> stateIds = new Vector<String>();
		if (stateId != null)
		{
			stateIds.add(stateId);
		}
		else if (scenarioId != null)
		{
			// Identify the States with the Attribute "NetValue", within the
			// Model Container.
			
			NodeSer[] nodeSers = null;
			try { nodeSers = view.getModelEngine().getNodesWithAttributeDeep(
				modelDomainId, Types.State.Financial.NetValue.class.getName(),
				scenarioId); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
							
			for (NodeSer nodeSer : nodeSers)
			{
				if (nodeSer instanceof StateSer)
					stateIds.add(nodeSer.getNodeId());
			}
		}
		else
			throw new RuntimeException(
				"ViewDistributionHandler must be called on a State or Scenario");
		
		
		// Create a Graph that depicts each State's computed distribution.
		
		for (String stateId : stateIds)
		{
			// *****************
			// Define a histogram of the final values of the State. The 
			// increments for the State value should be chosen so that the 
			// number of values in each bucket are about the same.
			// 
			// The number of buckets should be chosen so there can be at least 
			// a few points in the buckets that surround the most likely State 
			// value. 
			// 
			// The buckets should begin at the lowest value obtained through 
			// simulation and end with the highest value.
			
			// Determine the optimal histogram parameters, based on the number
			// of values, their clustering, and their ranges.

			HistogramDomainParameters histoParams;
			
			double[] bucketSizeAr = new double[1];
			double[] bucketStartAr = new double[1];
			int[] noOfBucketsAr = new int[1];
			
			StateSer stateSer = null;
			ModelDomainSer domainSer = null;
			try
			{
				stateSer = (StateSer)(view.getModelEngine().getNode(stateId));
				domainSer = (ModelDomainSer)(view.getModelEngine().getNode(stateSer.domainId));
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			String stateName = stateSer.getName();
			
			try { histoParams = view.getModelEngine().defineOptimalHistogramById(
				stateId,
				distOwnerId
				); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			
			// Compute the histogram.
			
			int[] counts = null;
			try { counts = view.getModelEngine().getHistogramById(
				stateId,
				distOwnerId,
				histoParams.bucketSize, histoParams.bucketStart,
				histoParams.noOfBuckets, true
				); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			if (histoParams.noOfBuckets != counts.length)
			{
				ErrorDialog.showReportableDialog(null, "Internal error", 
					new RuntimeException(
					"The number of buckets does not match the number of array elements")); return;
			}
			
			
			// Compute statistics.
			
			String[] options = { "mean", "geomean", "max", "min", "sd" };
			
			double[] statistics = null;
			try { statistics = view.getModelEngine().getResultStatisticsById(
				stateId,
				distOwnerId, options); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			
			// *****************
			// Create a Graph of the resulting distribution and allow the user 
			// to interactively adjust the number of buckets, the position of 
			// each bucket, and the size of each bucket. When the user is 
			// satisfied, the user can save the Graph.
			// 
			// The Graph should consist of one Graph showing the mode for each
			// bucket, overlaid with a smooth Graph of the computed curve.
			
			// Format values into the structures required for the Graph methods.
			
			GraphInput graphInput = new GraphInput();
			graphInput.formatInput(histoParams, counts);
			
			
			// Determine the scale and dimensions of the Graph, based on the
			// range of values and the size of the display.
			
			int graphDisplayWidth = container.getRecommendedPanelAreaWidth();
			int graphDisplayHeight = container.getRecommendedPanelAreaHeight();
			GraphDimensions graphDimensions = new GraphDimensions();
			
			
			graphDimensions.getOptimalDimensions(histoParams, graphInput,
				graphDisplayWidth, graphDisplayHeight);
							
			
			// Create a Bar Graph.
			
			final BarGraph modeGraph = new BarGraph();
			modeGraph.setSummaryStatistics(new double[] { statistics[0], statistics[4] });
			
			modeGraph.setXValues(graphInput.xValues);
			modeGraph.setYValues(graphInput.yValues);
			modeGraph.setXAxisLabel(stateName);
			modeGraph.setYAxisLabel("Frequency");

			modeGraph.setPixelXSize(graphDimensions.pixelXSize);
			modeGraph.setPixelYSize(graphDimensions.pixelYSize);
			modeGraph.setXStartLogical(graphDimensions.xStartLogical);
			modeGraph.setYStartLogical(graphDimensions.yStartLogical);
			modeGraph.setXEndLogical(graphDimensions.xEndLogical);
			modeGraph.setYEndLogical(graphDimensions.yEndLogical);
			modeGraph.setXDeltaLogical(graphDimensions.xDeltaLogical);
			modeGraph.setYDeltaLogical(graphDimensions.yDeltaLogical);
			
			
			// Create a Graph of the curve, and overlay it on the Bar Graph.
			
			final SmoothGraph curveGraph = new SmoothGraph();
			
			curveGraph.copyDimensionsFrom(modeGraph);
			
			curveGraph.setXValues(graphInput.xValues);
			curveGraph.setYValues(graphInput.yValues);
			curveGraph.setXAxisLabel("");
			curveGraph.setYAxisLabel("");
			

			// Create Graph Panel
			
			GraphPanel graphPanel = new GraphPanel(container, modeGraph);
			graphPanel.setName(stateName + " dist.");
			graphPanel.setTitle("Distribution of Expected Value for State " +
				stateSer.getFullName() + " of Domain " + domainSer.getName());

			//GraphPanel graphPanel = new GraphPanel(curveGraph);
			//graphPanel.layerGraph(curveGraph);

			
			// Instantiate the graph in a Panel with controls for changing
			// the gGraph's parameters and refreshing the Graph.
			
			// ...tbd
			
			
			// Add the Graph Panel to the Main Panel's tabbed Pane.
			
			container.addPanel(graphPanel.getName(), graphPanel, false);
		}
			
		container.recomputeLayout();
	}
}

