/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import java.awt.Component;
import awt.AWTTools;
import java.util.Vector;
import java.util.List;
import java.util.Date;
import graph.*;
import statistics.TimeSeriesParameters;
import generalpurpose.ThrowableUtil;
import generalpurpose.DateAndTimeUtils;
import expressway.ser.*;
import expressway.ser.ModelDomainSer;
import expressway.ser.ModelElementSer;
import expressway.ser.ModelScenarioSer;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.Types;
import expressway.gui.graph.GraphPanel;
import expressway.help.*;


/**
 * Create a Graph that depicts the expected value over time of the
 * specified State(s), based on all simulations of the DistributionOwner.
 */

public class ViewExpectedValueOverTimeHandler implements ActionListener
{
	private View view;
	private String scenarioId;
	private String distOwnerId;
	private String stateId; // may be null
	private String scopeNodeId;
	private PanelManager container;
	
	
	public ViewExpectedValueOverTimeHandler(View view, String scenarioId, String distOwnerId,
		String stateId, String scopeNodeId, PanelManager container)
	{
		this.view = view;
		this.scenarioId = scenarioId;
		this.distOwnerId = distOwnerId;
		this.stateId = stateId;  // may be null
		this.scopeNodeId = scopeNodeId;
		this.container = container;
		
		if (scenarioId == null) throw new RuntimeException("Scenario Id is null");
		if (distOwnerId == null) throw new RuntimeException("Distrubution Onwer is null");
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
		// Identify the Scenario.
		
		if (scenarioId == null)
		{
			JOptionPane.showMessageDialog(null,
				"Can only compute expected value on a saved Scenario", 
				"Error", JOptionPane.ERROR_MESSAGE); return;
		}
		
		
		// Identify the States.
		
		List<String> stateIds = new Vector<String>();
		if (stateId != null)
		{
			stateIds.add(stateId);
		}
		else if (scenarioId != null)
		{
			// Identify the States with the Attribute "NetValue", within the
			// scope.
			
			NodeSer[] nodeSers = null;
			try { nodeSers = view.getModelEngine().getNodesWithAttributeDeep(
				scopeNodeId, Types.State.Financial.NetValue.class.getName(),
				scenarioId); }
			catch (Exception ex)
			{
				ErrorDialog.showThrowableDialog(null, ex); return;
			}
			
			for (NodeSer nodeSer : nodeSers)
			{
				if (nodeSer instanceof StateSer)
					stateIds.add(nodeSer.getNodeId());
			}
		}
		else
			throw new RuntimeException(
				"ViewExpectedValueOverTimeHandler may only be called on a State or Scenario");
		
		if (stateIds.size() == 0)
		{
			ErrorDialog.showErrorDialog(null,
				"There must be a State tagged with a NetValue Attribute.\n" +
				"See Online Help, Tag Values for more information."
				); return;
		}
		
		
		// Create a Graph that depicts each State's expected value over time.
		
		for (String stateId : stateIds)
		{
			StateSer stateSer = null;
			ModelDomainSer domainSer = null;
			ModelScenarioSer scenarioSer = null;
			String stateName;
			try
			{
				stateSer = (StateSer)(view.getModelEngine().getNode(stateId));
				scenarioSer = (ModelScenarioSer)(view.getModelEngine().getNode(scenarioId));
				domainSer = (ModelDomainSer)(view.getModelEngine().getNode(stateSer.domainId));
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			stateName = stateSer.getName();
			
			TimeSeriesParameters timeSeriesParams;
			try { timeSeriesParams = 
				view.getModelEngine().defineOptimalValueHistoryById(
					stateId, distOwnerId); }
			catch (Exception ex)
			{
				ErrorDialog.showThrowableDialog(null, ex); return;
			}
			
			double[][] stats;
			try { stats = view.getModelEngine().getExpectedValueHistory(
					stateId, distOwnerId, 
					new Date(timeSeriesParams.startTime), 
					timeSeriesParams.noOfPeriods,
					timeSeriesParams.periodDuration); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(null, ex); return;
			}
			
			GraphInput graphInput = new GraphInput();
			
			// reset X axis to a zero base so that the X axis values will be
			// labeled starting with zero instead of with the absolute number of
			// days since the start of Java time.
			timeSeriesParams.startTime = 0;
			
			// Determining ranges so that the data can be graphed.
			graphInput.formatInput(timeSeriesParams, stats, 1);
			
			
			// Determine the scale and dimensions of the Graph, based on the
			// range of values and the size of the display.
			
			int graphDisplayWidth = container.getRecommendedPanelAreaWidth();
			int graphDisplayHeight = container.getRecommendedPanelAreaHeight();
			GraphDimensions graphDimensions = new GraphDimensions();
			
			graphDimensions.getOptimalDimensions(timeSeriesParams, graphInput,
				graphDisplayWidth, graphDisplayHeight);
			
			if (graphDimensions.pixelXSize == 0.0) throw new RuntimeException(
				"pixel X size is 0");
							
			if (graphDimensions.pixelYSize == 0.0) throw new RuntimeException(
				"pixel Y size is 0");
							
			
			// Create a curve Graph.
			
			final SmoothGraph smoothGraph = new SmoothGraph();
			smoothGraph.setDisplaySlopes(true);
			
			smoothGraph.setXScaleFactor(1.0/DateAndTimeUtils.MsInADay);
			smoothGraph.setXValues(graphInput.xValues);
			smoothGraph.setYValues(graphInput.yValues);
			smoothGraph.setXAxisLabel("Days");
			smoothGraph.setYAxisLabel("E(" + stateName + ")");

			smoothGraph.setPixelXSize(graphDimensions.pixelXSize);
			smoothGraph.setPixelYSize(graphDimensions.pixelYSize);
			smoothGraph.setXStartLogical(graphDimensions.xStartLogical);
			smoothGraph.setYStartLogical(graphDimensions.yStartLogical);
			smoothGraph.setXEndLogical(graphDimensions.xEndLogical);
			smoothGraph.setYEndLogical(graphDimensions.yEndLogical);
			smoothGraph.setXDeltaLogical(graphDimensions.xDeltaLogical);
			smoothGraph.setYDeltaLogical(graphDimensions.yDeltaLogical);


			// Create Graph Panel
			
			GraphPanel graphPanel = new GraphPanel(container, smoothGraph);
			graphPanel.setName("E(" + stateName + ")");
			graphPanel.setTitle("Expected Value Over Time for State " +
				stateSer.getFullName() + " of Scenario " + scenarioSer.getName() +
					" of Domain " + domainSer.getName());

			
			// Add the Graph Panel to the Main Panel's tabbed Pane.
			
			container.addPanel(graphPanel.getName(), graphPanel, false);
		}
	}
}

