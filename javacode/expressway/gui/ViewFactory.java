/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.ser.ActivitySer;
import expressway.ser.ModelDomainSer;
import expressway.common.ModelEngineRMI;
import expressway.ser.ModelElementSer;
import expressway.gui.PanelManager.*;
import javax.swing.JTabbedPane;
import javax.swing.JComponent;


public interface ViewFactory
{
	/*
	 * Deprecated methods. Calls to these should be replaced with calls to the
	 * createView and createViewPanel methods.
	 */
	
	
	View createRiskMitigationView(ActivitySer actSer, String modelScenarioNodeId,
		boolean bringToFore)
	throws
		Exception;
	
	
	View createModelDomainViewPanel(ModelElementSer modelElementSer, boolean openScenarios,
		boolean bringToFore)
	throws
		Exception;
	
	
	View createModelDomainViewPanel(String elementId, boolean openScenarios,
		boolean bringToFore)
	throws
		Exception;
	
	
	/*
	View createMotifViewPanel(NodeSer nodeSer, boolean bringToFore)
	throws
		Exception;
	
	
	View createMotifViewPanel(String nodeId, boolean bringToFore)
	throws
		Exception;
	*/


	View createPopupModelDomainViewPanel(String title,
		ModelElementSer nodeSer)
	throws
		Exception;


	/*
	 * The methods below utilize the server method getViewClassName(String nodeId).
	 * This method returns the name of the View class that the client should create.
	 * Each Domain should call setViewClassName when it is constructed.
	 */


	/**
	 * This method should not instantiate the View, attempt to set its size,
	 * or called postConstructionSetup. The PanelManager does that.
	 */
	 
	NodeView createView(String nodeId, String... scenarioIds)
	throws
		Exception;


	NodeView createView(String nodeId, boolean loadScenarios)
	throws
		Exception;


	/**
	 * Create and intantiate a View. If a View already exists for the specified
	 * Node, return it instead of creating another.
	 */
	 
	ViewContainer createViewPanel(boolean asPopup, String nodeId, String... scenarioIds)
	throws
		Exception;


	ViewContainer createViewPanel(boolean asPopup, String nodeId, boolean loadScenarios)
	throws
		Exception;
	
	
	/**
	 * Create two Views, instantiate them in a dual-View Panel, and connect them
	 * via the specified NamedReference type.
	 */
	 
	ViewContainer createDualViewPanel(boolean asPopup, String namedRefName, String nodeId,
		String otherNodeId)
	throws
		Exception;
}

