/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent;
import expressway.common.ModelAPITypes.*;
import expressway.help.*;
import generalpurpose.*;
import java.awt.Component;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Window;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.*;
import java.util.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.HttpURLConnection;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.io.IOException;
import java.io.OutputStreamWriter;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;



/** ****************************************************************************
 * A common Panel base class for all GUI Views that may contain Visuals.
 * Contains a PeerListener implementation.
 */
 
public abstract class ViewPanelBase extends JPanel implements View, DomainSpaceVisual
{
	/** Cache of locally stored Images. Used by getStoredImage(String). */
	private static Map<String, ImageIcon> storedImageIcons = new HashMap<String, ImageIcon>();
	

	private View.ViewHelper helper;
	
	
	private ModelEngineRMI modelEngine;
	
	
	/** All VisualComponents are displayed within the display area. It has no
		borders or insets. It resizes dynamically based on the size of this View Panel,
		but its position from the top-left is fixed. */
	private final VisualComponentDisplayAreaImpl displayArea;
	
	
	private JPanel infoPanel;


	/** The ViewFactory used to create this View. */
	private ViewFactory viewFactory;
	
	
	/** Listener for server-side events. */
	private PeerListener peerListener;
	
	
	/** Only non-null if the Model Engine support RMI callbacks. */
	private ListenerRegistrar listenerRegistrar = null;  // may be null.
	
	
	private PanelManager panelManager;

	
	/** Sizes (in View space) of standard elements such as border thickness. */
	private DisplayConstants displayConstants;
	
	
	private MouseListener tempMouseListener;

	
	
	public ViewPanelBase(ModelEngineRMI modelEngine, PanelManager panelManager,
		ViewFactory viewFactory, String n, boolean maximizeChildSize)
	throws
		Exception
	{
		this.modelEngine = modelEngine;
		setName(n);
		this.panelManager = panelManager;
		this.viewFactory = viewFactory;
		
		this.setLayout(new BorderLayout());
		this.infoPanel = new JPanel();
		
		this.infoPanel.setBackground(Color.white);
		add(infoPanel, BorderLayout.NORTH);
		this.displayArea = new VisualComponentDisplayAreaImpl(this, maximizeChildSize);
		add(displayArea, BorderLayout.CENTER);
		helper = createHelper();
		
		// A View owns a PeerListener for all of the Persistent Nodes that it depicts.
		if (getModelEngine() instanceof ModelEngineRMI)
		{
			if (this.peerListener == null) registerPeerListener();
		}
	}
	
	
	public ViewPanelBase(ModelEngineRMI modelEngine, PanelManager panelManager,
		String n, boolean maximizeChildSize)
	throws
		Exception
	{
		this(modelEngine, panelManager, null, n, maximizeChildSize);
		this.viewFactory = (ViewFactory)this;
	}
	
	
	protected abstract ViewHelper createHelper();
	
	
	protected ViewHelper getBaseHelper() { return this.helper; }
	
	
	public Component getComponent() { return this; }
	
	
	public Container getContainer() { return this; }
	
	
	public JComponent getJComponent() { return this; }
	
	
	/**
	 * Perform any intialization that associates this View with particular
	 * server-side Nodes.
	 */
	 
	public synchronized void postConstructionSetup()
	throws
		Exception
	{
		if (getParent() == null) throw new RuntimeException(
			"Called postConstructionSetup on a View that does not belong to a Container");
		validate();
	}
	
	
	public synchronized void registerPeerListener()
	throws
		Exception
	{
		this.peerListener = new PeerListenerImpl(this, this.getName());
		this.listenerRegistrar = 
			((ModelEngineRMI)(getModelEngine())).
				registerPeerListener(getPeerListener());
	}
	
	
	public synchronized void unregisterPeerListener()
	throws
		Exception
	{
		((ModelEngineRMI)(getModelEngine())).unregisterPeerListener(getPeerListener());
		listenerRegistrar = null;
	}
	
	
	public synchronized ModelEngineRMI getModelEngine() { return this.modelEngine; }


	public synchronized VisualComponentDisplayArea getDisplayArea() { return this.displayArea; }
	
	
	public synchronized JPanel getInfoPanel() { return infoPanel; }
	
	
	public synchronized void computeDisplayAreaLocationAndSize()
	{
		Insets insets = this.getInsets();
		VisualComponentDisplayArea da = getDisplayArea();
		
		int left = insets.left;
		int top = insets.top;
		int right = insets.right;
		int bottom = insets.bottom;
		
		int w = this.getWidth();
		int h = this.getHeight();
		
		int newW = w - left - right;
		int newH = h - top - bottom;
		
		int dw = newW - w;
		int dh = newH - h;
		
		VisualComponentDisplayAreaImpl displayArea = (VisualComponentDisplayAreaImpl)da;
		
		displayArea.setBounds(left, top, newW, newH);
	}
	
	
	public ClientMainApplication getClientMainApplication() { return getPanelManager().getClientMainApplication(); }
	
	
	public boolean isPopup() { return false; }
	
	
	public abstract void setIsPopup(boolean yes);
	
	
	public abstract ScenarioVisual showScenario(String scenarioId)
	throws
		Exception;
	
	
	public ViewConstants getDisplayConstants()
	{ 
		if (displayConstants == null) 
			displayConstants = new DisplayConstants();
		
		return displayConstants;
	}
	

	public ImageIcon getStoredImage(String handle)
	throws
		IOException
	{
		synchronized (storedImageIcons)
		{
			ImageIcon imageIcon = storedImageIcons.get(handle);
			
			if (imageIcon != null) return imageIcon;
			
			try { imageIcon = getModelEngine().getStoredImageIcon(handle); }
			catch (ParameterError pe) { throw new IOException(pe); }
			
			if (imageIcon != null)
			{
				storedImageIcons.put(handle, imageIcon);
				return imageIcon;
			}
		}
		
		return null;
	}
	
	
	public ViewFactory getViewFactory() { return this.viewFactory; }
	
	
	public abstract ScenarioSelector getScenarioSelector();
	
	
	public abstract void resizeScenarioSelector();
	
	
	public abstract void saveFocusFieldChanges();
	
	
	public abstract Class getVisualClass(NodeSer node) throws CannotBeInstantiated;
	
	
	/**
	 * Create a Visual to depict the specified Node in this View. If 'container'
	 * is not equal to null, add the new Visual to this View, under that Component.
	 */
	 
	public abstract VisualComponent makeVisual(NodeSer node, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception;
	
	
	public abstract Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError;
		
		
	public abstract VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError;
	
	
	public abstract Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError;
	
	
	/**
	 * If the specified Node is already represented in this View, do nothing
	 * and return null. Otherwise, create appropriate Visuals for the Node and
	 * return a Set of the Visuals created.
	 */
	 
	public abstract Set<VisualComponent> addNode(String nodeId)
	throws
		Exception;
		
		
	public abstract void removeVisual(VisualComponent visual)
	throws
		Exception;

		
	public abstract Set<VisualComponent> refreshVisuals(String nodeId)
	throws
		Exception;
		
		
	public synchronized void refreshAttributeStateBinding(String domainId,
		String attrId, String stateId)
	throws
		Exception
	{
		Set<VisualComponent> visuals = this.identifyVisualComponents(domainId);
		for (VisualComponent visual : visuals)
		{
			((ModelDomainVisual)visual).attributeStateBindingChanged(attrId, stateId);
		}
	}

	
	public abstract void select(VisualComponent visual, boolean yes);
	
	
	public abstract void selectNodesWithId(String nodeId, boolean yes)
	throws
		ParameterError;
	

	public abstract Set<VisualComponent> getSelected();
	
	
	public abstract boolean isSelected(VisualComponent visual);


	public abstract void selectAll(boolean yes);
	
	
	public abstract void domainCreated(String domainId);
	
	
	public abstract void motifCreated(String motifId);
	
		
	public abstract void showScenarioCreated(String scenarioId);
	
	
	public abstract void showScenarioSetCreated(String scenarioSetId);
	
	
	public synchronized PeerListener getPeerListener() { return this.peerListener; }
	
	
	public synchronized ListenerRegistrar getListenerRegistrar() { return this.listenerRegistrar; }
	
	
	public synchronized PanelManager getPanelManager() { return panelManager; }
	
	
	public synchronized Window getWindow()
	{
		Container container = this.getTopLevelAncestor();
		if (! (container instanceof Window)) throw new RuntimeException(
			"Outermost Container of View is not a Window");
		
		return (Window)container;
	}
	
	
	public synchronized void addTemporaryMouseListener(MouseListener listener)
	{
		/* Pass all mouse click events to this handler. Once the handler is invoked,
		remove it. If this is called multiple times, only the most recent handler
		is used. */
		removeTemporaryMouseListener();
		this.tempMouseListener = listener;
		addMouseListener(listener);
	}
	
	
	public synchronized void removeTemporaryMouseListener()
	{
		removeMouseListener(this.tempMouseListener);
		this.tempMouseListener = null;
	}
	
	
	/**
	 * Display a Window that allows the user to enter information about a problem
	 * with Expressway and then send that information to Expressway Solutions.
	 */
	 
	public void reportBug()
	{
		(new BugReportWindow()).show();
	}
}

