/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.ser.NodeSer;
import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.common.VisualComponent.ScenarioVisual;
import expressway.gui.ValueControl.*;
import java.util.List;
import java.util.Vector;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;



/** ********************************************************************
 * A VisibilityControl manages the visibility of a set of ValueControls.
 * It is a button that appears in a ControlPanel, visually linked to a.
 * VisualComponent. Clicking on the button causes the ValueControls for the
 * VisualComponent to disappear (but the VisibilityControl does not disappear).
 * Clicking again on the VisibilityControl causes the Visual's ValueControls to
 * re-appear.
 */
 
public abstract class VisibilityControl extends JButton
{
	public interface VisibilityControlFactory
	{
		Class getVisibilityControlClass();
		
		/** Create a VisibilityControl for the specified Visual.
			//and add it to the
			//ControlPanel and any Lists for managing it. 
			'vis' is the Visual 
			to which the Control pertains. 'visControl' may
			be null. */
		VisibilityControl makeVisibilityControl(VisualComponent visual, boolean showInitially);
		
		/** Undoes the operation of makeVisibilityControl. */
		void unmakeVisibilityControl(VisibilityControl vc);
		
		ValueControlFactory getValueControlFactory();
		
		ViewPanelBase getViewPanel();
		
		ControlPanel getControlPanel();
	}
	
	
	/**
	 * The 'showInitially' argument indicates whether the ValueControls
	 * that are managed by this VisibilityControl should be created
	 * in a visible state. Subclass constructors determine whether this
	 * VisibilityControl is initially visible.
	 */
		
	public VisibilityControl(VisibilityControlFactory factory,
		final ViewPanelBase viewPanel, ControlPanel controlPanel,
		VisualComponent visual, ValueControlFactory valueControlFactory, boolean showInitially)
	{
		super(ButtonIcons.ShowValueControlsButtonIcon);
		this.factory = factory;
		this.viewPanel = viewPanel;
		this.controlPanel = controlPanel;
		this.visual = visual;
		this.valueControlFactory = valueControlFactory;
		this.show = showInitially;
		
		java.awt.Dimension d = new Dimension(
			ButtonIcons.ShowValueControlsButtonIcon.getIconWidth(),
			ButtonIcons.ShowValueControlsButtonIcon.getIconHeight());
		
		this.setSize(d);
		this.setPreferredSize(d);
		
		showValueControls();
		addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				showValueControls(! show, true);
				viewPanel.repaint();
			}
		});
	}
	
	
	public VisibilityControlFactory getFactory() { return factory; }
	
	
	public ValueControlFactory getValueControlFactory() { return valueControlFactory; }
	
	
	/**
	 * Position the Visibility Control at the top left corner of the Visual.
	 */
	
	public void setLocation()
	{
		java.awt.Point visualLocationInScenarioPanel = 
			viewPanel.getDisplayArea().getLocationOfVisual(getVisual());
		
		this.setLocation(visualLocationInScenarioPanel);
	}
	
	
	/**
	 * Non-redundantly add the specified ValueControls to this VisibilityControls
	 * List of ValueControls.
	 */
	 
	public void addValueControl(ValueControl vc)
	{
		if (valueControls.contains(vc)) return;
		valueControls.add(vc);
	}
	
	
	/**
	 * Remove the specified ValueControl from this VisibilityControls List of
	 * ValueControls.
	 */
	 
	public void removeValueControl(ValueControl vc)
	{
		valueControls.remove(vc);
	}
	
	
	/**
	 * Instantiate a Visual for each Node that should have a ValueControl,
	 * and intantiate each ValueControl.
	 */
	
	public void createValueControls()
	{
		createValueControl(getVisual().getNodeSer().getAttributeNodeIds());
	}
	
	
	/**
	 * Update the server values that are depicted by the ValueControls that are
	 * owned by this VisibilityControl.
	 */
	 
	public void updateServer()
	{
		ValueControl[] vcs = getValueControls();
		for (ValueControl vc : vcs) vc.updateServer();
	}
	
	
	/**
	 * Sets the show/hide state of the VisibilityControl, and then calls
	 * showValueControls(). If 'updateValueControlVisibilityState' is true,
	 * then update the remembered state of this VisibilityControls's ValueControls.
	 */

	public void showValueControls(boolean show, boolean updateValueControlVisibilityState)
	{
		if (updateValueControlVisibilityState)
			getControlPanel().rememberVisibilityControlStateForVisual(visual, 
				valueControlFactory.getValueControlClass(), show);
		this.show = show;
		showValueControls();
	}
	
	
	/**
	 * If the VisibilityControl is set to show its Value Controls, then 
	 * for each child Visual of the type this VisibiltyControl is connerned with,
	 * call addNode on the Control Panel to add that Visual: the new Visual's
	 * ValueControl (if any) will be created by addNode.
	 * Otherwise, remove each ValueControl referenced by this VisibilityControl
	 * and any Visuals they reference that are owned by the ControlPanel.
	 */
	 
	public void showValueControls()
	{
		showValueControls(this.show);
	}
	
	
	/**
	 * Accessor.
	 * Return true if the ValueControls for this VisibilityControl should
	 * be made visible when this VisibilityControl is visible.
	 */
	
	public final boolean getShow() { return show; }
	
	
	public final ValueControl[] getValueControls()
	{
		return valueControls.toArray(new ValueControl[valueControls.size()]);
	}
	
	
	public final ViewPanelBase getViewPanel() { return viewPanel; }
	
	
	public final ControlPanel getControlPanel() { return controlPanel; }
	
	
	public final ScenarioVisual getScenario() { return (ScenarioVisual)(controlPanel); }
	
	
	public final VisualComponent getVisual() { return visual; }
	
	
  /* Internal */
	
	
	private VisibilityControlFactory factory;
	private ViewPanelBase viewPanel;
	private ControlPanel controlPanel;
	private VisualComponent visual;  // not in this ScenarioPanel.
	private ValueControlFactory valueControlFactory;
	private boolean show;
	private List<ValueControl> valueControls = new Vector<ValueControl>();
	
	
	/**
	 * Create a ValueControl that is controlled by this VisibilityControl.
	 * Should delegate to ControlPanel.createValueControl.
	 */
	 
	protected void createValueControl(String... nodeIds)
	{
		for (String nodeId : nodeIds) try
		{
			NodeSer nodeSer = viewPanel.getModelEngine().getNode(nodeId);
			
			addValueControl(getControlPanel().createValueControl(
				valueControlFactory, this, visual));
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
	
	
	/**
	 * Call <ControlPanel>.removeVisual for the Visual of each ValueControl
	 * referenced by this VisibilityControl.
	 */
	 
	protected void removeValueControlVisuals()
	{
		ValueControl[] valcs = valueControls.toArray(new ValueControl[valueControls.size()]);
		for (ValueControl valControl : valcs)
		{
			getControlPanel().removeVisual(valControl.getVisual());
		}
	}
	
	
	protected void showValueControls(boolean show)
	{
		if (show)
		{
			if (valueControlFactory.shouldHaveValueControl(visual.getNodeSer()))
			{
				createValueControl(visual.getNodeId());
			}
			
			String[] children = visual.getNodeSer().getChildNodeIds();
			for (String childId : children) try
			{
				NodeSer childSer = viewPanel.getModelEngine().getNode(childId);
				if (valueControlFactory.shouldHaveValueControl(childSer))
				{
					getControlPanel().addNode(childId, this);
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, "While contacing server", ex);
				return;
			}
		}
		else
		{
			// Remove ValueControls.
			ValueControl[] valcAr = valueControls.toArray(new ValueControl[valueControls.size()]);
			for (ValueControl valControl : valcAr)
			{
				getControlPanel().uncreateValueControl(valControl);
			}
			
			removeValueControlVisuals();
		}
	}
}
	
	

