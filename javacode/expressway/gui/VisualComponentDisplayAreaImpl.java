/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import javax.swing.JPanel;
import expressway.common.VisualComponent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import javax.swing.JComponent;

	
/**
 * The region within which Visual Components are to be displayed.
 * Has no borders or insets. This Panel should be instantiated within a
 * View, and all Visual Components should be instantiated within
 * the View's VisualComponentDisplayArea.
 */
 
public class VisualComponentDisplayAreaImpl extends JPanel
	implements View.VisualComponentDisplayArea
{
	private View view;
	private boolean maximizeComponentSize;
	
	
	public VisualComponentDisplayAreaImpl(View view, boolean maximizeComponentSize)
	{
		this.view = view;
		this.maximizeComponentSize = maximizeComponentSize;
		this.setLayout(new DisplayAreaLayoutManager());
		this.setBackground(java.awt.Color.white);
	}
	
	
	public Component getComponent() { return this; }
	
	
	public Container getContainer() { return this; }
	
	
	public JComponent getJComponent() { return this; }
	
	
	public void clear()
	{
		Graphics g = this.getGraphics();
		if (g == null) return;
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
	}
	
	
	public View getView() { return view; }
		
		
	public void repaint()
	{
		clear();
		super.repaint();
	}
	
	
	/**
	 * Return the location in this Display Area of the specified location, as 
	 * expressed in the space of the specified Component
	 */
	 
	public java.awt.Point translateVisualToDisplayArea(VisualComponent c,
		java.awt.Point visualLocation)
	{
		Object obj = c;
		
		java.awt.Point p = new java.awt.Point(visualLocation);
		for (;;)
		{
			if (obj == null) throw new RuntimeException(
					"Visual Component " + c.getName() +
					" (" + c.getClass().getName() + ")" +
					" is null or is not contained by this Display Area");
			
			if (obj == this) return p;
			
			if (! (obj instanceof Component)) throw new RuntimeException(
				"Visual is not an AWT Component");
			
			Component comp = (Component)obj;
			int x = p.x;
			int y = p.y;
			(p = comp.getLocation(p)).translate(x, y);
			obj = comp.getParent();
		}
	}
	
	
	/**
	 * Return the location, relative to this Display Area, of the specified
	 * Visual. If the Visual is not found, return null.
	 */
	 
	public java.awt.Point getLocationOfVisual(VisualComponent visual)
	{
		Container parentContainer = (Container)(visual.getParentObject());
		if (parentContainer instanceof ControlPanel)
		{
			// the location in the DisplayArea of the Visual's parent Node must be
			// used to calculate the result.
			String parentId = visual.getNodeSer().getParentNodeId();
			View view = getView();
			if (! (view instanceof NodeView)) throw new RuntimeException(
				"View is not a NodeView, but it must be to call getLocationOfVisual");
			VisualComponent parentVisual;
			try { parentVisual = ((NodeView)view).identifyVisualComponent(parentId); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
			java.awt.Point parentLocation = getLocationOfVisual(parentVisual);
			return translateVisualToDisplayArea(parentVisual, BaseOffsetPoint);
		}
		else
		{
			// The Visual is not owned by the ControlPanel
			return translateVisualToDisplayArea((VisualComponent)parentContainer, 
				visual.getLocation());
		}
	}
	
	
	private java.awt.Point BaseOffsetPoint = new java.awt.Point(10, 10);
	
	
	public void add(Component comp, Object constraints)
	{
		if (constraints == null) throw new RuntimeException(
			"Layout constraints must not be null");
		super.add(comp, constraints);
	}
	
	
	public Dimension getMinimumSize()
	{
		return getLayout().minimumLayoutSize(this);
	}
	
	
	public Dimension getPreferredSize()
	{
		return getLayout().preferredLayoutSize(this);
	}


	/**
	 * LayoutManager that ensures that all Components are the same size as
	 * their the DisplayArea and that they are located at 0,0, and that the ControlPanel
	 * (if any) remains on top.
	 */
	 
	protected class DisplayAreaLayoutManager implements LayoutManager
	{
		public void addLayoutComponent(String name, Component comp)
		{
			Container parent = comp.getParent();
			if (comp instanceof ControlPanel)
			{
				parent.setComponentZOrder(comp, 0);
				if (! maximizeComponentSize)
				{
					comp.setSize(minimumLayoutSize(VisualComponentDisplayAreaImpl.this));
					comp.setLocation(0, 0);
				}
			}
			else
			{
				if (parent.getComponentCount() == 0) throw new RuntimeException(
					"Should not happen: 0 Components after a Component added");
				
				if (parent.getComponentCount() == 1)
					parent.setComponentZOrder(comp, 0);
				else
					parent.setComponentZOrder(comp, 1);
			}
				
			if (comp instanceof Container)
			{
				LayoutManager lm = ((Container)comp).getLayout();
				if (lm != null) lm.layoutContainer((Container)comp);
			}
			
			if (maximizeComponentSize)
			{
				comp.setSize(minimumLayoutSize(VisualComponentDisplayAreaImpl.this));
				comp.setLocation(0, 0);
			}
			else
				comp.setSize(comp.getPreferredSize());
		}
		
		
		public void layoutContainer(Container parent)
		{
			Dimension d = VisualComponentDisplayAreaImpl.this.getSize();
			Component[] children = parent.getComponents();
			for (Component child : children)
			{
				if (child instanceof ControlPanel)
				{
					parent.setComponentZOrder(child, 0);
					if (! maximizeComponentSize)
					{
						child.setLocation(0, 0);
						child.setSize(d);
					}
				}
				
				if (maximizeComponentSize)
				{
					child.setLocation(0, 0);
					child.setSize(d);
				}
				else
					child.setSize(child.getPreferredSize());
				
				if (child instanceof Container)
				{
					LayoutManager lm = ((Container)child).getLayout();
					if (lm != null) lm.layoutContainer((Container)child);
				}
			}
		}
		
		
		public Dimension minimumLayoutSize(Container parent)
		{
			Dimension mind = new Dimension(0, 0);
			Component[] children = parent.getComponents();
			for (Component child : children)
			{
				Dimension cd = child.getSize();
				//Dimension cd = child.getMinimumSize();
				cd.width = Math.max(cd.width, mind.width);
				cd.height = Math.max(cd.height, mind.height);
			}
			
			return mind;
		}
		
		
		public Dimension preferredLayoutSize(Container parent)
		{
			return minimumLayoutSize(parent);
		}
		
		
		public void removeLayoutComponent(Component comp)
		{
		}
	}
}

