/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.activityrisk;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.common.Types.Activity.Transformation;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.PanelManager.*;
import expressway.gui.modeldomain.EventHistoryPanel;
import expressway.gui.modeldomain.ModelScenarioViewPanel;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.gui.InfoStrip.*;
import generalpurpose.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.Date;
import java.awt.Component;
import java.io.Serializable;
import java.io.IOException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.File;
import java.awt.Container;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JComponent;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;
import javax.swing.JOptionPane;
import javax.swing.event.*;
import javax.swing.tree.TreePath;


/** ****************************************************************************
 * Implements a tabular View of an Activity, in the context of a Scenario,
 * depicting the nested Activities as
 * a "tree table": the first column of the table contains a tree (one row per
 * tree node) that breaks down the hierarchy of Activities within this View's
 * root Activity; the other table columns identify various Attribute values,
 * specifically "evidence" and "sufficiency criteria" for the row's Activity.
 * This type of View is used for planning project risk mitigation activities.
 */
 
public class ActivityRiskMitTreeViewPanel extends TreeTablePanelViewBase
	implements TabularVisualComponentFactory, ModelScenarioVisual,
		PanelManager.Single_NodeView_Container
{
  /* Constants. */
	
	public static final String ViewTypeName = "RiskMitigationActivityView";
	public static final Color ScenarioColor = new Color(240, 240, 240);
	public static final Color ScenarioHLColor = new Color(240, 200, 200);

		
  /* Instance state. */
	
	/** The Model Scenario that provides context for the Activity, so that its
	 Attribute values can be determined. */
	private ModelScenarioSer scenarioSer;
	
	private NodeScenarioPanel scenarioPanel;
	private ScenarioSelector controlPanelSelector;
	private AttributeValueControlFactory attrValueControlFactory;
	private AttributeVisibilityControlFactory attrVisibilityControlFactory;
	
	private ImageIcon imageIcon;
	
	
  /* Constructor. */
	
	public ActivityRiskMitTreeViewPanel(PanelManager panelManager,
		ScenarioSelector controlPanelSelector,
		ViewFactory viewFactory, ActivitySer rootActivitySer, ModelScenarioSer scenarioSer,
		ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(panelManager, viewFactory, rootActivitySer, modelEngine);
		this.scenarioSer = scenarioSer;
		this.controlPanelSelector = controlPanelSelector;
		scenarioPanel = new ScenarioControlJPanel(this, scenarioSer, controlPanelSelector);
		attrValueControlFactory =
			new AttributeValueControlFactory(scenarioPanel, scenarioPanel);
		attrVisibilityControlFactory = 
			new AttributeVisibilityControlFactory(attrValueControlFactory);
	}
	
	
	public synchronized String getViewType() { return ViewTypeName; }
	
	
	public ScenarioVisual getCurrentScenarioVisual() { return this; }
	
	
	public ControlPanel getCurrentControlPanel() { return scenarioPanel; }
	
	
	public VisualComponent.VisualComponentFactory getVisualFactory() { return this; }
	
	
	public DomainInfoStrip constructDomainInfoStrip()
	{
		return new DomainInfoStripImpl(this);
	}

	
	
  /* From PanelManager nested types. */


	public NodeView getNodeView()
	{
		return this;
	}


	public Set<NodeView> getNodeViews(String nodeId)
	{
		Set<NodeView> nodeViews = new HashSet<NodeView>();
		nodeViews.add(this);
		return nodeViews;
	}


	public Set<View> getViews(String nodeId)
	{
		Set<View> views = new HashSet<View>();
		views.add(this);
		return views;
	}
	
	
	public Set<View> getViews()
	{
		Set<View> views = new HashSet<View>();
		views.add(this);
		return views;
	}
	
	
	public int getNoOfViews() { return 1; }



  /* From ViewPanelBase */
	
	
	public ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		ViewContainer comp = getViewFactory().createViewPanel(
			true, getOutermostVisual().getNodeId(), scenarioId);
		return ((ModelScenarioViewPanel)comp).getScenarioVisual(scenarioId);
	}
	
	
	public void showScenarioCreated(String scenarioId) {}
	
	
	public void showScenarioSetCreated(String scenarioSetId) {}
	
	
	public void showScenarioDeleted(String scenarioId) {}
	
	
	public ScenarioVisual getScenarioVisual(String scenarioId)
	{
		return this;
	}
	
		
	protected View.ViewHelper createHelper()
	{
		return null;
	}
	
	
	protected ViewHelper getBaseHelper()
	{
		throw new RuntimeException("Should not be called: this class does not use a helper.");
	}
	
	
  /* From TreeTablePanelViewBase */
	
	
	public List<String> getFieldNames()
	{
		String[] fnames = RiskMitActivityVisualJ.FieldNames;
		ArrayList<String> alist = new ArrayList<String>(fnames.length);
		for (String fname : fnames) alist.add(fname);
		return alist;
	}
	
	
	public void notifyScenarioNameChanged(ScenarioVisual scenVis, String newName)
	{ // OK
	}
	
	
	public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
	{
	}
		
		
	public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId, String crossRefId)
	{
	}
		
		
	protected boolean nodeIsPermitted(NodeSer nodeSer)
	throws
		Exception
	{
		if (! (nodeSer instanceof ActivitySer)) return false;
		
		AttributeSer[] attrSer = getModelEngine().getAttributes(
			nodeSer.getNodeId(), Transformation.class.getName(), scenarioSer.getNodeId());
		
		return (attrSer.length > 0);
	}
	
	
	public VisualComponent makeTabularVisual(NodeSer nodeSer)
	throws
		ParameterError,
		Exception
	{
		if (! (nodeSer instanceof ActivitySer)) throw new ParameterError(
			"Only Activities are allowed in a " + ViewTypeName + " View");
		
		VisualComponent visual = super.makeTabularVisual(nodeSer);
		if (! (visual instanceof RiskMitActivityVisualJ)) throw new RuntimeException(
			"Expected a RiskMitActivityVisualJ");
		
		RiskMitActivityVisualJ rvisual = (RiskMitActivityVisualJ)visual;
		
		AttributeSer[] attrSers = getModelEngine().getAttributes(
			nodeSer.getNodeId(), Transformation.class.getName(), scenarioSer.getNodeId());
		
		if (attrSers.length == 0) throw new Exception(
			"No Transformation Attributes for '" + nodeSer.getFullName() + "'");
		
		if (attrSers.length > 1) throw new ParameterError(
			"More than one Transformation Attribute for '" + nodeSer.getFullName() + "'");
		
		AttributeSer attrSer = attrSers[0];
		rvisual.setTransformationAttrId(attrSer.getNodeId());

		Serializable value = getModelEngine().getAttributeValueById(
			attrSer.getNodeId(), scenarioSer.getNodeId());
		
		if (! (value instanceof Transformation)) throw new Exception(
			"Unexpected: Attribute value is not a Transformation object");
		
		rvisual.setTransformation((Transformation)value);
		return visual;
	}	
	
	
	public synchronized void postConstructionSetup()
	throws
		Exception
	{
		super.postConstructionSetup();
		
		this.setNodeSer(getModelEngine().getNode(this.getNodeId()));
		
		
		// Adjust size and position of Display Area, and install handler that
		// will re-adjust it when this Panel is resized.
		
		this.computeDisplayAreaLocationAndSize();
		
		
		// Refresh the View.
				
		VisualComponent outermostVisual = getOutermostVisual();
		outermostVisual.refresh();


		// Add handler for when the Display Area changes size.
		
		this.addComponentListener(new ComponentAdapter()
		{
			public void componentResized(ComponentEvent e)
			{
				//int priorHeight = displayAreaHeight;
				ActivityRiskMitTreeViewPanel.this.computeDisplayAreaLocationAndSize();
				
				//int dh = displayAreaHeight - priorHeight;
				//setYOriginDynamicOffset(getYOriginDynamicOffset() - dh);
			}
		});
		

		// Subscribe this View, because it is also a Visual.
		
		if (this.getListenerRegistrar() != null)
		{
			this.getListenerRegistrar().subscribe(getRootNodeId(), true);
		}
		
		repaint();
		// did not cause a repaint.
	}


	protected void addViewContextMenuItems(JPopupMenu containerPopup,
		final TabularVisualJ visual, int x, int y)
	{
		super.addViewContextMenuItems(containerPopup, visual, x, y);
	}
	
	
	protected void keyPressed(EditActionType keyAction, TreePath[] selectedPaths, int[] columns)
	{
		super.keyPressed(keyAction, selectedPaths, columns);
	}


	public Class getVisualClass(NodeSer nodeSer)
	{
		if (nodeSer instanceof ActivitySer)
			return RiskMitActivityVisualJ.class;
		else if (nodeSer instanceof AttributeSer)
			return expressway.gui.modeldomain.AttributeVisualJ.class;
		else
			throw new RuntimeException("Unrecognized NodeSer type");
	}
	
	
	protected void treeNodesChanged(TreeModelEvent e) {}
	
	protected void treeNodesInserted(TreeModelEvent e) {}
	
	protected void treeNodesRemoved(TreeModelEvent e) {}
	
	protected void treeStructureChanged(TreeModelEvent e) {}
	
	public void notifyVisualComponentPopulated(VisualComponent visual,
		boolean populated) {}
	
	public void notifyVisualRemoved(VisualComponent visual) {}
	
	
	public void resizeScenarioSelector()
	{
	}
	
	
	public ScenarioSelector getScenarioSelector() { return controlPanelSelector; }
	
	
	public ScenarioVisual makeScenarioVisual(ScenarioSer scenarioSer)
	{
		return new ScenarioControlJPanel(this, scenarioSer, controlPanelSelector);
	}
	
	
  /* From ScenarioVisual */


	public List<Serializable> getAttributeValues(VisualComponent vis, List<String> attrNames)
	{
		if (! (vis instanceof RiskMitActivityVisualJ)) throw new RuntimeException(
			"Expected vis to be a RiskMitActivityVisualJ");
		RiskMitActivityVisualJ rvis = (RiskMitActivityVisualJ)vis;
		
		Transformation transformation = rvis.getTransformation();
		
		List<Serializable> values = new Vector<Serializable>();
		values.add(transformation.getStdEvidence());
		values.add(transformation.getCriteria());
		values.add(transformation.getActEvidence());
		values.add(transformation.getComment());
		
		attrNames.add("Std Evidence");
		attrNames.add("Criteria");
		attrNames.add("Actual Evidence");
		attrNames.add("Comment");
		
		return values;
	}


	public Serializable getAttributeValue(String attrId)
	{
		return null;
	}
	
		
  /* From VisualComponent */
	
	
	public String getNodeId() { return scenarioSer.getNodeId(); }
	
	
	public String getFullName() { return scenarioSer.getFullName(); }
	

	public ImageIcon getImageIcon() { return imageIcon; }
	
	
	public void setImageIcon(ImageIcon imgIcon) { this.imageIcon = imgIcon; }
	
	
	public String getDescriptionPageName() { return "Risk Mitigation View"; }
		

	public int getSeqNo() { return 0; }
	
	
	public boolean contains(VisualComponent comp)
	throws
		NoNodeException
	{
		if (comp instanceof RiskMitActivityVisualJ)
			return ((RiskMitActivityVisualJ)comp).getParent() == this;
		else
			return false;
	}
	
	
	public boolean isOwnedByMotifDef()
	{
		return getNodeSer().ownedByMotifDef();
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return getNodeSer().ownedByActualTemplateInstance();
	}
	

	public String[] getChildIds()
	{
		Component[] children = getDisplayArea().getComponents();
		Set<String> childIds = new TreeSetNullDisallowed<String>();
		for (Component child : children)
			if (child instanceof VisualComponent)
				childIds.add(((VisualComponent)child).getNodeId());
		
		return childIds.toArray(new String[childIds.size()]);
	}
	
	
	public void nameChangedByServer(String newName, String newFullName)
	{
		this.scenarioSer.setName(newName);
		this.scenarioSer.setFullName(newFullName);
		this.setName(newName);
		
		getClientView().notifyScenarioNameChanged(this, newName);
	}
	
	
	public void nodeDeleted()  // the Scenario was deleted on the server.
	{
		if (JOptionPane.showOptionDialog(this, 
			"The Scenario '" + scenarioSer.getFullName() +
			" has been deleted on the server. This View must therefore close. " +
			"Do you wish to save its data to a local file?",
			"Scenario Deleted",
			JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, 
			null, null) == JOptionPane.YES_OPTION)
		{
			for (;;)
			{
				try
				{
					File file = FileChooserUtils.getFileToSave(this, FileChooserUtils.FileType.xml);
					if (file == null) return;  // user gave up.

					if (file.exists())
					{
						if (JOptionPane.showConfirmDialog(this,
							"File exists; overwrite?", "Please Confirm",
							JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
						{
							continue;  // try again.
						}
					}
					
					writeDataAsXML(file);
					
					return;  // Sucessful write.
				}
				catch (Throwable t)
				{
					ErrorDialog.showReportableDialog(this, t);
				}
			}
		}
	}
	
	
	public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
	{
		NodeSer ser;
		try { ser = getModelEngine().getNode(crossRefId); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
			return;
		}
		
		if (! (ser instanceof CrossReferenceSer)) throw new RuntimeException(
			"Node with Id " + crossRefId + " is not a CrossReference");
		CrossReferenceSer crSer = (CrossReferenceSer)ser;
		notifyCrossReferenceCreated(crSer);
	}
		
		
	public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
	{
		notifyCrossReferenceDeleted(this.getNodeId(), toNodeId, crossRefId);
	}
		
		
	protected void writeDataAsXML(File file)
	throws
		IOException
	{
		PrintWriter pwriter = null;
		try
		{
			pwriter = new PrintWriter(new FileWriter(file));
			getOutermostVisual().writeScenarioDataAsXMLRecursive(pwriter, 0, this);
		}
		finally
		{
			try { if (pwriter != null) pwriter.close(); }
			catch (Throwable t) { t.printStackTrace(); }
		}
	}
	
	
	public View getClientView() { return this; }
	
	
	public ModelEngineRMI getModelEngine() { return getClientView().getModelEngine(); }
	
	
	public NodeSer getNodeSer() { return this.scenarioSer; }
	
	
	public void setNodeSer(NodeSer nodeSer) { this.scenarioSer = (ModelScenarioSer)nodeSer; }
	
	
	public void update()
	{
		NodeSer nodeSer = null;
		try { nodeSer = getModelEngine().getNode(scenarioSer.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this,  ex);
			return;
		}
			
		this.scenarioSer = (ModelScenarioSer)nodeSer;
		
		update(nodeSer);
	}
	

	public void update(NodeSer nodeSer)
	{
		this.scenarioSer = (ModelScenarioSer)nodeSer;

		try { this.refreshRedundantState(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
		}
	}
	
	
	public void redraw() { this.repaint(); }		
	

	public void refresh()
	{
		update();
		redraw();
	}
	
	
	public java.awt.Point getLocation()
	{
		return new java.awt.Point(0, 0);
	}

	
	public int getX() { return 0; }
	
	
	public int getY() { return 0; }

	
	public void refreshLocation() {}

	
	public void refreshSize() {}
	
	
	public void setLocation() throws Exception {}
	
	
	public void setSize() throws Exception {}

	
	public void refreshRedundantState()
	throws
		Exception
	{
		this.setName(this.scenarioSer.getFullName());
		getClientView().notifyScenarioNameChanged(this, this.scenarioSer.getName());
	}

	
	public boolean getAsIcon() { return false; }  // never display as an icon.
	
	
	public void setAsIcon(boolean yes) {}
	
	
	public boolean useBorderWhenIconified() { return false; }
	
	
	public void populateChildren()
	{
	}
	
	
	/*public void refreshChildren()
	throws
		ParameterError,
		Exception
	{
	}*/
	
	
	public Set<VisualComponent> getChildVisuals()
	{
		Set<VisualComponent> visuals = new HashSet<VisualComponent>();
		//visuals.add(getOutermostVisual());
		return visuals;
	}
	
	
	public Object getParentObject() { return this.getParent(); }
	
	
	public Container getContainer() { return this; }
	
	
	public Component getComponent() { return this; }
	
	
	public void highlight(boolean yes) {}
	
	
	public boolean isHighlighted() { return false; }
	

	public Color getNormalBackgroundColor() { return ScenarioColor; }


	public Color getHighlightedBackgroundColor() { return ScenarioHLColor; }
	
	
	public int getWidth() { return super.getWidth(); }

	
	public int getHeight() { return super.getHeight(); }
	
	
	public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writeScenarioDataAsXML(writer, indentation, scenarioVisual);
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writer.print(StringUtils.tabs[indentation]);
		writer.println("<" + ViewTypeName + " full_name=\"" + this.getFullName() + "\">");
		writer.println("</" + ViewTypeName + ">");
	}

	
  /* ***********************************************************************
   * Methods defined in ModelScenarioVisual.
   */

	public void addSimulationVisual(Integer id, SimulationVisual simPanel)
	{
	}
	
	
	public Set<SimulationVisual> simulationRunCreated(Integer requestId,
		String simRunNodeId)
	{
		return new HashSet<SimulationVisual>();
	}
	
	
	public void simulationRunsDeleted()
	{
	}
	
	
	public void refreshAttributeValue(String attrNodeId)
	throws
		Exception
	{
		Set<VisualComponent> visuals = identifyVisualComponents();
		for (VisualComponent visual : visuals)
		{
			if (! (visual instanceof RiskMitActivityVisualJ)) continue;
			RiskMitActivityVisualJ rVisual = (RiskMitActivityVisualJ)visual;
			
			if (rVisual.getTransformationAttributeId().equals(attrNodeId))
			{
				Serializable currentValue =
					getModelEngine().getAttributeValueById(attrNodeId,
						scenarioSer.getNodeId());
				
				if (! (currentValue instanceof Transformation))
					throw new RuntimeException("Value is not a Transformation");
				
				Transformation trans = (Transformation)currentValue;
				
				// Update visual column values.
				rVisual.refreshRedundantState();
			}
		}
	}
	
	
	public void setShowEventsMode(boolean showEventsMode)
	{
	}
	
	
	public boolean getShowEventsMode() { return false; }
	

	public void showEventsForIteration(String simRunId, int iteration)
	{
	}
			
	
	public void refreshStatistics()
	{
	}
	
	
	public void simTimeParamsChanged(Date newFromDate, Date newToDate,
		long newDuration, int newIterLimit)
	{
		this.scenarioSer.startingDate = newFromDate;
		this.scenarioSer.finalDate = newToDate;
		this.scenarioSer.duration = newDuration;
		this.scenarioSer.iterationLimit = newIterLimit;
	}
	
	
	public void setLocation(VisualComponent visual)
	{
	}
	
	
	public void addOrRemoveAttributeControls(boolean add, VisualComponent visual) // Done
	throws
		Exception
	{
		//addOrRemoveControls(add, visual, attrValueControlFactory, AttributeVisual.class);
		//addOrRemoveControls(add, visual, attrVisibilityControlFactory);
	}


	public void addOrRemoveStateControls(boolean add, VisualComponent visual)
	throws
		Exception
	{
	}
	
	
	public void deleteControls(String nodeId)
	{
		//removeControls(nodeId);
	}
	

	public boolean controlsForAttrAreVisible(VisualComponent visual)
	{
		if (getCurrentControlPanel() == null) return false;
		return getCurrentControlPanel().controlsForVisualAreVisible(
			visual, AttributeValueControl.class);
	}
	
	
	public boolean controlsForStateAreVisible(PortedContainerVisual visual)
	{
		return false;
	}
	
	
	public JFrame createEventHistoryWindow(String simRunId, Class... stateTags)
	throws
		Exception
	{
		return EventHistoryPanel.createEventHistoryWindow(this, 
			simRunId, stateTags);
	}
}

