/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.activityrisk;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.common.Types.Activity.Transformation;
import expressway.ser.*;
import expressway.gui.*;
import generalpurpose.StringUtils;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import javax.swing.JPopupMenu;
import javax.swing.JOptionPane;


public class RiskMitActivityVisualJ extends TabularVisualJ
{
	private String transformationAttrId;
	
	private Transformation transformation;
	
	public static final String[] FieldNames =
	{
		"Name",
		"Standard Evidence",
		"Criteria",
		"Actual Evidence",
		"Comment"
	};
	
	
	RiskMitActivityVisualJ(ActivitySer activitySer, ActivityRiskMitTreeViewPanel view)
	throws
		ParameterError
	{
		super(activitySer, view, FieldNames.length);
	}
	
	
	public void setTransformationAttrId(String id) { this.transformationAttrId = id; }

	
	public void setTransformation(Transformation t)
	{
		this.transformation = t;
		refreshRedundantState();
	}
	
	
	public String getNodeKind() { return "Activity"; }
	
	
	public String getDemotionNodeKind() { return "Activity"; }
	
	
	public String getDescriptionPageName() { return "Risk Mitigation View"; }
	
	
	public String getTransformationAttributeId() { return transformationAttrId; }
	
	
	public Transformation getTransformation() { return transformation; }
	
	
	public String getScenarioId()
	{
		return ((ActivityRiskMitTreeViewPanel)(getClientView())).getCurrentScenarioVisual().getNodeId();
	}


	public void updateServer(int column)
	{
		switch (column)
		{
		case 0: // String name
			{
				try
				{
					try { getModelEngine().setNodeName(false, getNodeSer().getNodeId(),
						getNodeSer().getName()); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(null, w.getMessage(),
							"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							getModelEngine().setNodeName(true, getNodeSer().getNodeId(),
								getNodeSer().getName());
					}
				}
				catch (Exception ex) { ErrorDialog.showReportableDialog(getTreeTable(), ex); }
				break;
			}
		case 1: // String stdEvidence
		case 2: // String criteria
		case 3: // String actEvidence
		case 4: // String comment
			{
				NodeSerBase nodeSer = (NodeSerBase)(getNodeSer());
				
				try
				{
					try { getModelEngine().setAttributeValue(false, transformationAttrId, 
						getScenarioId(), transformation); }
					catch (Warning w)
					{
						if (JOptionPane.showConfirmDialog(getTreeTable(), w.getMessage(),
							"Warning: Simultions will be deleted",
							JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) 
							== JOptionPane.YES_OPTION)
							getModelEngine().setAttributeValue(true, transformationAttrId, 
								getScenarioId(), transformation);
					}
				}
				catch (Exception ex) { ErrorDialog.showReportableDialog(getTreeTable(), ex); }
				break;
			}
		default: throw new RuntimeException("Unexpected column number: " + column);
		}
	}
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writer.println(StringUtils.tabs[indentation] + 
			"Name=\"" + getName() + "\"");
		writer.println(StringUtils.tabs[indentation] + 
			"Standard Evidence=\"" + transformation.getStdEvidence() + "\"");
		writer.println(StringUtils.tabs[indentation] + 
			"Criteria=\"" + transformation.getCriteria() + "\"");
		writer.println(StringUtils.tabs[indentation] + 
			"Actual Evidence=\"" + transformation.getActEvidence() + "\"");
		writer.println(StringUtils.tabs[indentation] + 
			"Comment=\"" + transformation.getComment() + "\"");
	}
	
	
	protected void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		MenuItemHelper.addMenuOwnerItems(getClientView(), this,
			(MenuOwnerSer)(getNodeSer()), containerPopup, x, y);
	}

	
	public void refreshRedundantState()
	{
		setValueAt(getName(), 0);
		setValueAt(transformation.getStdEvidence(), 1);
		setValueAt(transformation.getCriteria(), 2);
		setValueAt(transformation.getActEvidence(), 3);
		setValueAt(transformation.getComment(), 4);
	}
	
	

  /* From DefaultMutableTreeTableNode */
	
	
	public boolean isEditable(int column) { return column >= 0; }
	
	
	/**
	 * Override to map Visual's columns to the View's columns.
	 */
	 
	public void setValueAt(Object aValue, int column)
	{
		switch(column)
		{
		case 0: getNodeSer().setName(aValue.toString()); break;
		case 1: transformation.setStdEvidence(aValue.toString()); break;
		case 2: transformation.setCriteria(aValue.toString()); break;
		case 3: transformation.setActEvidence(aValue.toString()); break;
		case 4: transformation.setComment(aValue.toString()); break;
		default: throw new RuntimeException("Unexpected column number: " + column);
		}
		
		super.setValueAt(aValue, column);
	}
}

