/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.decision;

import java.awt.Image;
import expressway.gui.*;
import expressway.ser.*;
import expressway.common.VisualComponent.*;
import expressway.common.NodeIconImageNames;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.awt.Container;
import java.awt.Color;


class DecisionDomainVisualJ extends DecisionPointVisualJ implements DecisionDomainVisual
{
	DecisionDomainVisualJ(DecisionDomainSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		setBackground(getNormalBackgroundColor());
		setImageIcon(NodeIconImageNames.DecisionDomainIconImageName);
	}
	
	
	public String getDescriptionPageName() { return "Decision Domains"; }


	public ScenarioVisual showScenario(String scenarioId) throws Exception
	{
		throw new RuntimeException("Not implemented yet");
	}
	
	
	public void showScenarioCreated(String scenarioId)
	{
		GlobalConsole.println("Scenario created");
	}


	public void showScenarioDeleted(String scenarioId)
	{
		GlobalConsole.println("Scenario deleted");
	}
	
	
	public ScenarioVisual getScenarioVisual(String scenarioId)
	{
		throw new RuntimeException("Not implemented yet");
	}
	
	
	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return DomainColor; }
	
	
	public Color getHighlightedBackgroundColor() { return DomainHLColor; }
}

