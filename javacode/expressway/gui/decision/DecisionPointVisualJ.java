/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.decision;

import java.awt.Image;
import expressway.ser.*;
import expressway.gui.*;
import expressway.common.VisualComponent.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.awt.Container;
import java.awt.Color;


class DecisionPointVisualJ extends JPanel implements GraphicVisualComponent,
	DecisionPointVisual
{
	DecisionPointVisualJ(DecisionPointSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setImageIcon(NodeIconImageNames.DecisionPointIconImageName);
		//setBackground(getNormalBackgroundColor());
	}


	public String getDescriptionPageName() { return "Decision Points"; }


	public void showStart()
	{
		GlobalConsole.println("DecisionPointVisualJ: " + getName() + " started.");
	}
	
	
	public void showStop()
	{
		GlobalConsole.println("DecisionPointVisualJ: " + getName() + " stopped.");
	}


	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return VariableColor; }
	
	
	public Color getHighlightedBackgroundColor() { return VariableHLColor; }
}

