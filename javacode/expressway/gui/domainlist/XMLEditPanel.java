/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.domainlist;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import generalpurpose.ThrowableUtil;
import expressway.common.*;
import expressway.gui.FileChooserUtils;
import expressway.gui.FileChooserUtils.*;


/**
 * A Panel for viewing and editing XML.
 */
 
public class XMLEditPanel extends JFrame
{
	private JPanel controlPanel;
	private JEditorPane editPanel;
	private JButton saveAsButton;
	
	
	public XMLEditPanel(String name)
	{
		super(name);
		setLayout(new BorderLayout());
		add(controlPanel = new JPanel(), BorderLayout.NORTH);
		editPanel = new JEditorPane();
		
		//Put the editor pane in a scroll pane.
		JScrollPane editorScrollPane = new JScrollPane(editPanel);
		editorScrollPane.setVerticalScrollBarPolicy(
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		editorScrollPane.setPreferredSize(new Dimension(800, 400));
		editorScrollPane.setMinimumSize(new Dimension(10, 10));
		add(editorScrollPane, BorderLayout.CENTER);
		
		controlPanel.add(saveAsButton = new JButton("Save As"));
		saveAsButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				FileWriter fwriter = null;
				try
				{
					File file = FileChooserUtils.getFileToSave(XMLEditPanel.this,
						FileType.xml);
					if (file == null) return;

					if (file.exists())
					{
						if (JOptionPane.showConfirmDialog(XMLEditPanel.this,
							"File exists; overwrite?", "Please Confirm",
							JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
						{
							return;
						}
					}
					
					fwriter = new FileWriter(file);
					String text = editPanel.getText();
					fwriter.write(text, 0, text.length());
				}
				catch (IOException ex)
				{
					ErrorDialog.showReportableDialog(XMLEditPanel.this, ex);
				}
				finally
				{
					try { if (fwriter != null) fwriter.close(); }
					catch (Throwable t) { t.printStackTrace(); }
				}
				
				//XMLEditPanel.this.dispose();
			}
		});
		
		validate();
	}
	
	
	public void setText(String text)
	{
		editPanel.setText(text);
	}
}

