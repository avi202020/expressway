/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.hier;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.help.*;
import expressway.ser.*;
import expressway.gui.*;
import java.awt.Insets;
import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.net.URL;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * For depicting Views of a HierarchyDef.
 */
 
public class HierarchyMotifViewPanel extends HierarchyViewPanel implements MotifView
{
  /* ***************************************************************************/
  /* Static values. */
	
	public static final String ViewTypeName = "HierarchyMotifView";
	
	
	
	public HierarchyMotifViewPanel(PanelManager container, 
		NodeSer nodeSer, ViewFactory viewFactory, ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(container, nodeSer, viewFactory, modelEngine);
	}
	
	
	protected View.ViewHelper createHelper()
	{
		return null;
	}
	
	
	protected ViewHelper getBaseHelper()
	{
		throw new RuntimeException("Should not be called: this class does not use a helper.");
	}
	
	
	public synchronized void postConstructionSetup()
	throws
		Exception
	{
		// Add a control strip for managing this Panel's functions.
		
		//this.createDomainInfoStrip();
		//this.resizeDomainInfoStrip();
		computeDisplayAreaLocationAndSize();
		//computeDisplayAreaLocationAndSize(false);
		
		
		// Instantiate the Visuals.
		
		super.postConstructionSetup();
	}
	
	
	public ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public String getViewType() { return ViewTypeName; }
	
	
	public ScenarioSelector getScenarioSelector()
	{
		return null;
	}
	
	
	public void showScenarioSetCreated(String scenarioSetId)
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	public void resizeScenarioSelector()
	{
	}
	
	
	public synchronized void refreshRedundantState()
	throws
		Exception
	{
		//super.refreshRedundantState();
	}
	
	/*
	public synchronized void computeDisplayAreaLocationAndSize(boolean repositionComponents)
	{
		if (getDomainInfoStrip() == null)
		{
			super.computeDisplayAreaLocationAndSize(repositionComponents);
			return;
		}
		
		super.computeDisplayAreaLocationAndSize(false);
		
		this.resizeDomainInfoStrip();
		
		Insets insets = this.getInsets();
		
		int left = insets.left;
		int top = insets.top;
		int right = insets.right;
		int bottom = insets.bottom;
		
		int w = this.getWidth();
		int h = this.getHeight();
		int sssHeight = getDomainInfoStrip().getHeight();		

		int daW = w - left - right;
		int daH = h - top - bottom - sssHeight;

		VisualComponentDisplayAreaImpl displayArea = 
			(VisualComponentDisplayAreaImpl)(this.getDisplayArea());

		displayArea.setSize(daW, daH);
		
		displayArea.setLocation(left, top + sssHeight);
		
		try
		{
			String id = getOutermostNodeId();
			VisualComponent outermostVisual = identifyVisualComponent(id);
			outermostVisual.setLocation();
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}*/


	protected void setShowScenarios(boolean show)
	throws
		Exception
	{
	}


	public void loadAllScenarios()
	{
	}
	
	
	public ScenarioVisual makeScenarioVisual(ScenarioSer ser)
	{
		throw new RuntimeException("Cannot make a Scenario in a Motif View");
	}
	
	
	public ScenarioVisual getScenarioVisual(String scenarioId)
	{
		throw new RuntimeException("This View does not support Scenarios");
	}
	
	
	public synchronized void notifyScenarioNameChanged(ScenarioVisual scenVis,
		String newName)
	{
		// ok
	}
	
	
	public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
	{
	}
		
		
	public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId, String crossRefId)
	{
	}
		
		
	public ControlPanel getCurrentControlPanel()
	{
		return null;
	}

	
	public ScenarioVisual getCurrentScenarioVisual() { return null; }
	
	
	public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
	}
}

