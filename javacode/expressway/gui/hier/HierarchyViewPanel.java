/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.hier;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.InfoStrip.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import java.io.Serializable;
import java.util.List;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JPopupMenu;
import javax.swing.JComponent;
import javax.swing.ImageIcon;
import javax.swing.tree.*;
import javax.swing.event.*;


/**
 * Provide a hierarchical depiction of a HierarchyElements. The View may be
 * rooted at any Hierarchy - not necessarily a HierarchyDomain. Elements are
 * editable and deletable, and can be moved from one place to another within
 * their hierarchy.
 */
 
public abstract class HierarchyViewPanel
	extends TreeTablePanelViewBase
{
	public HierarchyViewPanel(PanelManager panelManager, NodeSer nodeSer, 
		ViewFactory viewFactory, ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(panelManager, viewFactory, nodeSer, modelEngine);
		
		VisualComponent root = getOutermostVisual();
		
		ImageIcon li = root.getImageIcon();
		ImageIcon oi = li;
		ImageIcon ci = li;
		setLeafIcon(li);
		setOpenIcon(oi);
		setClosedIcon(ci);
	}
	
	
	//private ScenarioViewPanelHelper helper;
	
	
	protected Class getTreeNodeVisualType() { return HierarchyVisualJ.class; }
	
	
	protected Class getAttributeVisualType() { return HierarchyAttributeVisualJ.class; }

	
	public VisualComponentFactory getVisualFactory() { return this; }
	
	
	public String getDescriptionPageName() { return "Hierarchy View"; }
	
	
  /* From TreeTablePanelViewBase */
	
	
	public synchronized void postConstructionSetup()
	throws
		Exception
	{
		super.postConstructionSetup();
		
		
		// Adjust size and position of Display Area, and install handler that
		// will re-adjust it when this Panel is resized.
		
		this.computeDisplayAreaLocationAndSize();
		
		
		// Refresh the View.
				
		VisualComponent outermostVisual = getOutermostVisual();
		outermostVisual.refresh();


		// Add handler for when the Display Area changes size.
		
		this.addComponentListener(new ComponentAdapter()
		{
			public void componentResized(ComponentEvent e)
			{
				computeDisplayAreaLocationAndSize();
			}
		});
		
		repaint(); // did not cause a repaint.
	}


	protected void addViewContextMenuItems(JPopupMenu containerPopup,
		final TabularVisualJ visual, int x, int y)
	{
		super.addViewContextMenuItems(containerPopup, visual, x, y);
	}
	
	
	public Class getVisualClass(NodeSer nodeSer)
	{
		if (nodeSer instanceof HierarchySer)
			return getTreeNodeVisualType();
		else if (nodeSer instanceof AttributeSer)
			return getAttributeVisualType();
		else
			throw new RuntimeException("Unrecognized NodeSer type");
	}
	
	
	protected void keyPressed(EditActionType keyAction, TreePath[] selectedPaths, int[] columns)
	{
		super.keyPressed(keyAction, selectedPaths, columns);
	}


	public VisualComponent makeTabularVisual(NodeSer nodeSer)
	throws
		ParameterError,
		Exception
	{
		return super.makeTabularVisual(nodeSer);
	}
	
	
	public DomainInfoStrip constructDomainInfoStrip()
	{
		return new DomainInfoStripImpl(this);
	}

		
  /* Internal methods */


	protected void treeNodesChanged(TreeModelEvent e) {}
	
	protected void treeNodesInserted(TreeModelEvent e) {}
	
	protected void treeNodesRemoved(TreeModelEvent e) {}
	
	protected void treeStructureChanged(TreeModelEvent e) {}
	
	
	public void notifyVisualComponentPopulated(VisualComponent visual,
		boolean populated) {}
	
	public void notifyVisualMoved(VisualComponent visual) {}
	
	public void notifyVisualRemoved(VisualComponent visual) {}
}

