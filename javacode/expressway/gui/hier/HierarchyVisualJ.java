/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.hier;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.gui.TreeTablePanelViewBase.*;
import expressway.help.*;
import awt.AWTTools;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.awt.Container;
import java.awt.Component;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


public class HierarchyVisualJ extends TabularVisualJ
{
	public HierarchyVisualJ(NodeSer hierSer, View view, Serializable[] values)
	throws
		ParameterError
	{
		super(hierSer, (TreeTablePanelViewBase)view, ((HierarchySer)hierSer).getFieldNames().length);
		if (! (hierSer instanceof HierarchySer)) throw new RuntimeException(
			"hierSer is not a HierarchySer");
		if (! (view instanceof HierarchyViewPanel)) throw new RuntimeException(
			"view is not a HierarchyViewPanel");
	}
	
	
	public String getNodeKind() { return "Hierarchy"; }
	
	
	public String getDemotionNodeKind() { return "Hierarchy"; }
	
	
	public String getDescriptionPageName() { return "Hierarchies"; }


	public String getScenarioId() { return null; }
	
	
	public void updateServer(int column)
	{
		switch (column)
		{
		case 0: // name
			{
				try { getModelEngine().setNodeName(true, getNodeId(), getValueAt(0).toString()); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				break;
			}
		default:
			{
				if (column >= getColumnCount()) throw new RuntimeException(
					"Unexpected column number: " + column);
				
				try
				{
					Object value = getValueAt(column);
					if (! (value instanceof Serializable)) throw new Exception(
						"Value in column " + column + " is not Serializable");
					
					String attrNodeId = getAttributeIdForColumn(column);
					ScenarioVisual scenarioVisual = 
						getTreeTablePanelViewBase().getCurrentScenarioVisual();
						
					if (scenarioVisual == null)  // update the default value
					{
						try { getModelEngine().setAttributeDefaultValue(false, attrNodeId,
							(Serializable)value); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								getModelEngine().setAttributeDefaultValue(true, attrNodeId,
									(Serializable)value);
						}
					}
					else  // update the Scenario value
					{
						String scenarioId = scenarioVisual.getNodeId();
					
						try { getModelEngine().setAttributeValue(false, attrNodeId, 
							scenarioId, (Serializable)value); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								getModelEngine().setAttributeValue(true, attrNodeId, 
									scenarioId, (Serializable)value);
						}
					}
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				break;
			}
		}
	}
	
	
	protected void addContextMenuItems(JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);


		final HelpfulMenuItem menuItem1 = new HelpfulMenuItem("Motifs \u2192", false,
			"List the motifs that are available from the server, and those that " +
			"are available to this Domain. " +
			"See " + HelpWindow.createHref("Motifs") + ".");
		
		menuItem1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e1)
			{
				Component originator = AWTTools.getJPopupMenuOriginator(e1);
				
				MotifDefSer[] motifDefSers = null;
				try
				{
					String domainId = ((NodeView)(getClientView())).getOutermostNodeSer().getDomainId();
					motifDefSers = getModelEngine().getMotifsCompatibleWithDomainId(domainId);
				}
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				List<MotifDefSer> motifsUsedSers = null;
				try { motifsUsedSers =
					getModelEngine().getMotifsUsedByDomain(getNodeId()); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(getViewPanel(), ex);
				}
				
				
				// Create a popup menu that contains a menu item for each of the
				// kinds of motif that is available..
				
				JPopupMenu popup = new JPopupMenu();
				
				popup.add(new HelpfulMenuItem("Domain uses these Motifs:", false,
					"To use a " + HelpWindow.createHref("Motifs", "motif") +
					" is to assert that the " +
					HelpWindow.createHref("Model Domains", "Model Domain") +
					" will be used. " +
					"Thus, usage expresses <i>intent</i>. The Domain might " +
					"not actually reference any elements of a motif that it 'uses'. " +
					"To 'use' a motif, select the Motif in the list presented " +
					"by this menu."));
				
				popup.addSeparator();
					
				int i = 0;
				for (final MotifDefSer motifDefSer : motifDefSers)
				{
					String title = motifDefSer.getName();
					
					for (MotifDefSer motifUsedSer : motifsUsedSers)
					{
						if (motifUsedSer.getNodeId().equals(motifDefSer.getNodeId()))
						{
							title += " \u2714";  // heavy check mark
							break;
						}
					}
					
					HelpfulMenuItem menuItem2 = 
						new HelpfulMenuItem(title, false, motifDefSer.getHtmlDescription());
						
					menuItem2.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e2)
						{
							// Toggle use of the Motif.
							try
							{
								String domainId = HierarchyVisualJ.this.getNodeId();
								String motifId = motifDefSer.getNodeId();
								
								boolean usesMotif = 
									getModelEngine().domainUsesMotif(domainId, motifId);
								
								getModelEngine().useMotif(domainId, motifId, ! usesMotif);
							}
							catch (Exception ex)
							{
								ErrorDialog.showThrowableDialog(getViewPanel(), ex);
							}
						}
					});
	
					popup.add(menuItem2);
				}
				
				popup.show(originator, x, y);
			}
		});
		
		containerPopup.add(menuItem1);
		
		MenuItemHelper.addMenuOwnerItems(getViewPanel(), this,
			(MenuOwnerSer)(getNodeSer()), containerPopup, x, y);
	}


	protected Serializable[] getAttributeValues()
	throws
		Exception
	{
		return getModelEngine().getAttributeValuesForNode(getNodeId(), getScenarioId());
	}
	
	
	public boolean isEditable(int column) { return column >= 0; }
}

