/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import expressway.ser.*;
import expressway.gui.*;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.NodeIconImageNames;
import expressway.common.ModelAPITypes.*;
import java.awt.Container;
import java.awt.Color;
import javax.swing.JPopupMenu;


class ActivityVisualJ extends PortedContainerVisualJ implements ActivityVisual
{
	ActivityVisualJ(ActivitySer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		//setImageIcon(NodeIconImageNames.ActivityIconImageName);
		//setBackground(getNormalBackgroundColor());
	}
	
	
	public String getDescriptionPageName() { return "Activities"; }


	public void showStart()
	{
		GlobalConsole.println("ActivityVisualJ: " + getName() + " started.");
	}
	
	
	public void showStop()
	{
		GlobalConsole.println("ActivityVisualJ: " + getName() + " stopped.");
	}
	
	
	public void showEnteredRespond()
	{
		GlobalConsole.println("ActivityVisualJ: " + getName() + " entered respond().");
	}


	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return ActivityColor; }
	
	
	public Color getHighlightedBackgroundColor() { return ActivityHLColor; }
	
	
	void addContextMenuItems(JPopupMenu containerPopup, int x, int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		MenuItemHelper.addMenuOwnerItems(getGraphicView(), this,
			(MenuOwnerSer)(getNodeSer()), containerPopup, x, y);
	}
}

