/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.common.NodeIconImageNames;
import expressway.gui.*;
import expressway.ser.*;
import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics;


public class AttributeStateBindingVisualJ extends VisualJComponent
	implements AttributeStateBindingVisual
{
	public static final int DefaultWidth = 4;
	
	
	AttributeStateBindingVisualJ(AttributeStateBindingSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view, false);
		//setImageIcon(NodeIconImageNames.AttributeStateBindingIconImageName);
		//setBackground(getNormalBackgroundColor());
	}
	
	
	public String getDescriptionPageName() { return "Binding an Attribute to a State"; }
	
	
	//int getNormalBorderThickness() { return 0; }
	

	public Color getNormalBackgroundColor() { return AttributeStateBindingColor; }
	
	
	public Color getHighlightedBackgroundColor() { return AttributeStateBindingHLColor; }
	
	
	void highlightBackground(boolean yes)
	{
		super.highlightBackground(yes);
		
		repaint();
	}
	
	
	public void setLocation()
	throws
		Exception
	{
		if (getParent() == null) return;
		this.setLocation(getParent().getWidth() - this.getWidth(), 0);
	}
	
	
	public void setSize()
	throws
		Exception
	{
		if (getParent() == null) return;
		this.setSize(DefaultWidth, getParent().getHeight());
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		ChevronSymbol.drawChevronSymbol(g, 0, 0, DefaultWidth, this.getHeight());
	}
}

