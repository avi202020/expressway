/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import expressway.ser.*;
import expressway.common.VisualComponent.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.common.NodeIconImageNames;
import expressway.gui.*;
import expressway.help.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.util.*;
import java.io.Serializable;


public class AttributeVisualJ extends VisualJComponent implements AttributeVisual
{
	static AttributeValueParser DefaultValueParser = new AttributeValueParser()
	{
		public Serializable parseValue(String text)
		throws
			ParameterError
		{
			return ObjectValueWriterReader.parseObjectFromString(text);
		}
	};
	
	
	public String getDescriptionPageName() { return "Attributes"; }
	
	
	private AttributeValueParser valueParser = DefaultValueParser;
	
	
	//private JLabel label;
	
	
	public AttributeVisualJ(AttributeSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		
		//setImageIcon(NodeIconImageNames.AttributeIconImageName);
		//setBackground(getNormalBackgroundColor());
		
		//setBorder(null);
		//this.setLayout(null);
		//this.label = new JLabel(node.getName());
		//this.label.setFont(this.getGraphicView().getComponentTitleFont());
		//this.label.setSize(50, 10);
		//this.label.setLocation(0, 0);
		//this.add(label);
	}


	public AttributeValueParser getValueParser() { return valueParser; }
	
	
	void setValueParser(AttributeValueParser parser) { this.valueParser = parser; }
	
	
	public Serializable parseValue(String text)
	throws
		ParameterError
	{
		return valueParser.parseValue(text);
	}
	
	
	//int getNormalBorderThickness() { return 0; }
	

	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return AttributeColor; }
	
	
	public Color getHighlightedBackgroundColor() { return AttributeHLColor; }


	public void populateChildren()
	throws
		ParameterError,
		Exception
	{
		ModelAttributeSer attrSer = (ModelAttributeSer)(this.getNodeSer());
		if (attrSer.attrStateBindingNodeId != null)
		{
			this.getClientView().addNode(attrSer.attrStateBindingNodeId);
		}
		
		super.populateChildren();
	}
	
	
	/*public void refreshChildren()
	throws
		ParameterError,
		Exception
	{
	}*/
	
	
	public int getSeqNo() { return ((ModelAttributeSer)(getNodeSer())).attributeNo; }


	void addContextMenuItems(JPopupMenu containerPopup, int x, int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		final AttributeStateBindingSer bindingSer;
		try { bindingSer = getModelEngine().getAttributeStateBinding(
			AttributeVisualJ.this.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(AttributeVisualJ.this, ex);
			return;
		}

		if (bindingSer == null)
		{
			if ((! this.isOwnedByMotifDef()) && (! this.isOwnedByActualTemplateInstance()))
			{
				JMenuItem menuItem1 = new HelpfulMenuItem("Bind to State...", false,
					"Link the selected Attribute to a State in another Model Domain. " +
					"When simulation is performed, the value of the Attribute is determined. " +
					"See " + HelpWindow.createHref("Binding an Attribute to a State") + ".");
				
				menuItem1.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						new AttributeBindingControl();
					}
				});
				
				containerPopup.add(menuItem1);
			}
		}
		else
		{
			JMenuItem menuItem1 = new HelpfulMenuItem("Modify State binding...", false,
				"Change the State to which this Attribute is bound. See " +
				HelpWindow.createHref("Binding an Attribute to a State") + ".");
			
			menuItem1.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new AttributeBindingControl(bindingSer);
				}
			});
			
			containerPopup.add(menuItem1);
			
			JMenuItem menuItem2 = new HelpfulMenuItem("Unbind from State", false,
				"Remove the Attribute's binding to State '" + bindingSer.stateFullName +
				"'.");
			
			menuItem2.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try
					{
						try { getModelEngine().deleteAttributeStateBinding(false,
							bindingSer.getNodeId()); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(AttributeVisualJ.this,
								w.getMessage(), "Warning", JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION)
								getModelEngine().deleteAttributeStateBinding(true,
									bindingSer.getNodeId());
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(AttributeVisualJ.this, ex);
					}
				}
			});
			
			containerPopup.add(menuItem2);
		}
	}
	
	
	class AttributeBindingControl extends JFrame
	{
		final AttributeStateBindingSer bindingSer;
		
		
		AttributeBindingControl()
		{
			this(null);
		}
		
		
		AttributeBindingControl(AttributeStateBindingSer bindingSer)
		{
			super("Bind Attribute to a State in Another Domain");
			this.setAlwaysOnTop(true);
			this.bindingSer = bindingSer;
			
			
			// Display two lists, side-by-side: (1) a list of Model Domains, and
			// a list of the States in the Model Domain that the user selects
			// in the first list.
			
			JSplitPane splitPane = new JSplitPane();
			JScrollPane topPanel = new JScrollPane(splitPane);
			JPanel centerPanel = new JPanel();
			JPanel bottomPanel = new JPanel();
			
			final JList domainList = new JList();
			
			final StateList stateList = new StateList();
			
			splitPane.setLeftComponent(domainList);
			splitPane.setRightComponent(stateList);
			
			List<ModelDomainSer> modelDomainSers = null;
			try { modelDomainSers = getModelEngine().getModelDomains(); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(this, ex);
				return;
			}
			
			String[] domainNames = new String[modelDomainSers.size()];
			int i = 0;
			for (ModelDomainSer ser : modelDomainSers)
			{
				// Skip Attribute's Domain, since one cannot bind to a State
				// in the Attribute's own Domain.
				if (bindingSer != null)
					if (ser.getNodeId().equals(bindingSer.domainId)) continue;
				
				// Skip Motif Domains.
				if (ser instanceof ModelDomainMotifDefSer) continue;
				
				domainNames[i++] = ser.getName();
			}
			
			domainList.setListData(domainNames);
			
			if (bindingSer != null)
			{
				for (ModelDomainSer ser : modelDomainSers)
				{
					if (ser.getNodeId().equals(bindingSer.stateDomainId))
					{
						domainList.setSelectedValue(ser.getName(), true);
						
						// Show the Domain's States in the StateList.
						stateList.refresh(ser.getName());
						
						break;
					}
				}
			}
			
			domainList.addListSelectionListener(new ListSelectionListener()
			{
				public void valueChanged(ListSelectionEvent e)
				{
					Object value = domainList.getSelectedValue();
					if (value == null) return;
					
					if (! (value instanceof String)) throw new RuntimeException(
						"Value in List Selection expected to be a String but is a " +
						value.getClass().getName());
						
					String domainName = (String)value;
					stateList.refresh(domainName);
				}
			});
			
			JButton bindButton = new JButton("Bind");
			bindButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					String stateName = stateList.getSelectedStateName();
					if (stateName == null)
					{
						ErrorDialog.showErrorDialog(AttributeBindingControl.this,
							"No State selected");
						return;
					}
					
					// Create a new AttributeStateBinding.
					try
					{ 
						try { getModelEngine().bindAttributeToState(false, 
							AttributeVisualJ.this.getNodeId(), stateName); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(AttributeBindingControl.this,
								"Simuations will be deleted. Continue?", "Please confirm",
								JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
							{
								getModelEngine().bindAttributeToState(true, 
									AttributeVisualJ.this.getNodeId(), stateName);
							}
							else
								return;
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(AttributeBindingControl.this, ex);
						return;
					}
					
					AttributeBindingControl.this.dispose();
				}
			});
			
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					AttributeBindingControl.this.dispose();
				}
			});
			
			bottomPanel.add(bindButton);
			bottomPanel.add(cancelButton);
			
			//centerPanel.setLayout(null);
			centerPanel.add(new HelpButton("What is this?", true, HelpButton.Size.Large,
				"Select from the Model Domains on the left. Once selected, a Domain will " +
				"display a list of the States in that Domain. Select a State to bind the " +
				"Attribute (that you clicked on) to that State. See " +
				HelpWindow.createHref("Binding an Attribute to a State") + "."));
			
			add(topPanel, BorderLayout.NORTH);
			add(centerPanel, BorderLayout.CENTER);
			add(bottomPanel, BorderLayout.SOUTH);

			this.setSize(500, 300);
			this.show();
		}
		
		class StateList extends JList
		{
			/** Refresh the dropdown list of State names. */
			
			void refresh(String modelDomainName)
			{
				if (modelDomainName == null) return;
				
				StateSer[] stateSers = null;
				try { stateSers =
					getModelEngine().getStatesForModelDomain(modelDomainName); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(this, ex);
					return;
				}
				
				String[] stateNames = new String[stateSers.length];
				int i = 0;
				String currentStateFullName = null;
				for (StateSer ser : stateSers)
				{
					stateNames[i++] = ser.getFullName();
					if (bindingSer != null)
						if (ser.getNodeId().equals(bindingSer.stateNodeId))
							currentStateFullName = ser.getFullName();
					
					// Skip States that belong to a Motif.
					if (ser.isOwnedByMotifDef) continue;
					
					
					// Skip States that belong to a TemplateInstance that
					// references a ModelTemplate.
					if (ser.isOwnedByActualTemplateInstance) continue;
				}
				
				setListData(stateNames);
				
				// Select the State to which the Attribute is currently bound.
				
				if (currentStateFullName != null)
					this.setSelectedValue(currentStateFullName, true);
			}
			
			
			String getSelectedStateName()
			{
				Object value = this.getSelectedValue();
				if (value == null) return null;
				
				if (! (value instanceof String)) throw new RuntimeException(
					"Value in List Selection expected to be a String but is a " +
					value.getClass().getName());
					
				return (String)value;
			}
		}
	}
}

