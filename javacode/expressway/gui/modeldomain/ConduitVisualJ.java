/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.gui.*;
import geometry.Point;
import geometry.PointImpl;
import geometry.LineSegment;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.IOException;
import java.net.URL;
import java.awt.Insets;


/** ****************************************************************************
 * Implemented as a JPanel that is transparent. Line segments are added as
 * opaque Components on the transparent JPanel.
 */
 
class ConduitVisualJ extends VisualJComponent implements ConduitVisual
{
	private List<ConduitSegment> segments = new Vector<ConduitSegment>();
	
	private boolean doubleThickness = false;
	
	
	ConduitVisualJ(ConduitSer conduitSer, GraphicView view)
	throws
		Exception
	{
		super(conduitSer, view, false);
		
		setBackground(Transparent);
		setOpaque(false);
		
		setContainerVisualJRedundantState();
	}
	
	
	public String getDescriptionPageName() { return "Conduits"; }
	
	
	/**
	 * See also PortVisualJ.setLocation() and ConduitVisualJ.setLocation().
	 */

	public void setLocation()
	{
		if (getParent() == null) return;
		Insets parentInsets = getParent().getInsets();
		
		this.setLocation(parentInsets.left, parentInsets.top);
	}
	
	
	private final void setContainerVisualJRedundantState()
	throws
		Exception
	{
		VisualJComponent conduitParent = (VisualJComponent)(this.getParent());
		if (conduitParent == null) return;  // can't compute visual attributes yet.
		
		setBorder(null);
		Insets parentInsets = conduitParent.getInsets();
		
		int newWidth = conduitParent.getWidth() - parentInsets.left - parentInsets.right;
		int newHeight = conduitParent.getHeight() - parentInsets.top - parentInsets.bottom;
			
		setBounds(parentInsets.left, parentInsets.top, newWidth, newHeight);
		
		setVisible(false);
		setBackground(Transparent);		
		
		ConduitSer conduitSer = (ConduitSer)(this.getNodeSer());
		
		//String beginPortOwnerId = conduitSer.beginPortOwnerId;
		//String endPortOwnerId = conduitSer.endPortOwnerId;
		geometry.Point p1Location = conduitSer.p1Location;
		geometry.Point p2Location = conduitSer.p2Location;
		//geometry.Point p1LocationFromOutsideIconified = conduitSer.p1LocationFromOutsideIconified;
		//geometry.Point p2LocationFromOutsideIconified = conduitSer.p2LocationFromOutsideIconified;
		InflectionPoint[] inflectionPoints = conduitSer.inflectionPoints;
		
		// Transform starting location into the Visual space.
		
		Point beginPoint = p1Location;
		Point endPoint = p2Location;
		
		/*
		Set<VisualComponent> visuals = getClientView().identifyVisualComponents(beginPortOwnerId);
		if (visuals.size() == 0) throw new Exception("Cannot idenify Visual for Port's owner");
		if (visuals.size() > 1) throw new Exception("More than one Visual depicting Port");
		
		VisualComponent beginPortOwnerVisual;
		for (VisualComponent v : visuals) beginPortOwnerVisual = v;
		if // Port owner is iconified in the View
			(beginPortOwnerVisual.getAsIcon())
		{
			beginPoint = p1LocationFromOutsideIconified;
		}
		else
		{
			beginPoint = p1Location;
		}
		
		visuals = getClientView().identifyVisualComponents(beginPortOwnerId);
		if (visuals.size() == 0) throw new Exception("Cannot idenify Visual for Port's owner");
		if (visuals.size() > 1) throw new Exception("More than one Visual depicting Port");
		
		VisualComponent endPortOwnerVisual;
		for (VisualComponent v : visuals) endPortOwnerVisual = v;
		if // Port owner is iconified in the View
			(endPortOwnerVisual.getAsIcon())
		{
			endPoint = p2LocationFromOutsideIconified;
		}
		else
		{
			endPoint = p2Location;
		}*/
		
		
		// Iterate over each segment, creating a ConduitSegment for each.
		
		int noOfInflectionPoints = 
			inflectionPoints == null ? 0 : inflectionPoints.length;
		
		int noOfSegments = noOfInflectionPoints + 1;
		
		Point segBegin = beginPoint;
		Point segEnd = null;
		
		
		// Adjust beginning Point to be at the center of the Port.
		
		int conduitThickness = 
			getGraphicView().getDisplayConstants().getConduitThickness();
		
		synchronized (segments)
		{
			segments = new Vector<ConduitSegment>();
			
			for (int segmentNo = 1; segmentNo <= noOfSegments; segmentNo++)
			{
				if (segmentNo < noOfSegments)
				{
					segEnd = getConduitSer().inflectionPoints[segmentNo-1];
					
					// Adjust for dimensions of line. Remember that an Inflection Point
					// occurs along the centerline of the line.
					
					segEnd = new PointImpl(
						segEnd.getX() - conduitThickness/2.0,
						segEnd.getY() - conduitThickness/2.0);
				}
				else
				{
					// Handle the last Segment, which ends at a Port instead
					// of an Inflection Point.
				
					segEnd = endPoint;
					
					// Adjust to be at the center of the Port.
					
					segEnd = new PointImpl(segEnd.getX() 
						//+ radius + p2Node.getWidth()/2.0 - conduitThickness/2.0
						, 
						segEnd.getY()
						//+ radius + p2Node.getHeight()/2.0 - conduitThickness/2.0
						);
				}
				
				
				/*
				if (PointImpl.areAlmostEqual(segBegin.getX(), segEnd.getX())) 
					// vertical
					;
				else if (PointImpl.areAlmostEqual(segBegin.getY(), segEnd.getY()))
					// horizontal
					;
				else // segment is on an angle
				
					System.out.println(">>>>>>>>>Warning: Conduit segment is on an angle.\n" +
					//throw new ModelContainsError("Cannot presently handle Conduits on an angle; " +
					"segment endpoints are: " +
					"(" + segBegin.getX() + ", " + segBegin.getY() + "), " +
					"(" + segEnd.getX() + ", " + segEnd.getY() + ")"
					);
					*/
					
				int x0 = // origin of Segment in coord space of the Conduit Visual.
					this.transformNodeXCoordToView(segBegin.getX());
					
				int y0 = // origin of Segment in coord space of the Conduit Visual.
					this.transformNodeYCoordToView(segBegin.getY());
					
				int x1 = // end of Segment in coord space of the Conduit Visual.
					this.transformNodeXCoordToView(segEnd.getX());
					
				int y1 = // end of Segment in coord space of the Conduit Visual.
					this.transformNodeYCoordToView(segEnd.getY());
				
				
				// Create Segment visual Component, and add it to this JComponent.
				// The Segment will draw itself. It is a transparent rectangle that draws
				// the segment as an opaque line within that rectangle.
				
				ConduitSegment segment = 
					new ConduitSegment(new java.awt.Point(x0, y0), new java.awt.Point(x1, y1));
				
				segments.add(segment);
	
				
				// Iterate again.
				
				segBegin = segEnd;
			}
		}
	}
	
	
	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
		setContainerVisualJRedundantState();
	}
	
	
	public boolean getAsIcon() { return false; }
	
	
	public void setAsIcon(boolean yes) {}
	
	
	ConduitSer getConduitSer() { return (ConduitSer)(this.getNodeSer()); }
	
	
	protected ModelContainerVisual getContainerVisual()
	{
		return (ModelContainerVisual)(this.getParent());
	}
	
	
	public void highlight(boolean yes)
	{
		doubleThickness = yes;
		setHighlighted(yes);
	}
	
	
	public boolean isHighlighted() { return doubleThickness; }
	
	
	public void refresh()
	{
		super.refresh();
		//getParent().repaint();
	}
	
	
	void highlightBackground(boolean yes) {}


	void addContextMenuItems(JPopupMenu containerPopup, int x, int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		MenuItemHelper.addMenuOwnerItems(getGraphicView(), this,
			(MenuOwnerSer)(getNodeSer()), containerPopup, x, y);
	}
	
	
	protected synchronized void paintComponent(Graphics g)
	{
		if (! getNodeSer().isVisible()) return;

		synchronized (segments)
		{
			for (ConduitSegment segment : segments)
			{
				try { segment.paintComponent(g); }
				catch (RuntimeException re)  // for debug only.
				{
					ConduitSer ser = (ConduitSer)(this.getNodeSer());
					
					re.printStackTrace();
				}
			}
		}

		getContainerVisual().incrementConduitColor();
	}


	//protected boolean clearBeforeDraw() { return false; }
	
	
	
	/** ************************************************************************
	 * Return the shortest distance between any of the Segments of this Conduit
	 * and the specified Point. The Point is in the space of the Conduit.
	 */
	 
	protected double getDistanceFrom(java.awt.Point p)
	{
		geometry.PointImpl dPoint = new geometry.PointImpl(p.getX(), p.getY());
		
		double shortestDistance = 0.0;
		boolean firstSegment = true;
		for (ConduitSegment cSeg : segments)
		{
			geometry.Point cSegBegin = 
				new geometry.PointImpl(cSeg.getSegBegin().x, cSeg.getSegBegin().y);
				
			geometry.Point cSegEnd = 
				new geometry.PointImpl(cSeg.getSegEnd().x, cSeg.getSegEnd().y);
				
			LineSegment lSeg = new LineSegment(cSegBegin, cSegEnd);
			
			double distance = lSeg.distanceTo(dPoint);
			
			if (firstSegment)
			{
				shortestDistance = distance;
				firstSegment = false;
			}
			else shortestDistance = Math.min(distance, shortestDistance);
			
		}
		
		return shortestDistance;
	}


	/** ************************************************************************
	 * Represents a visual segment of a Conduit.
	 */
	 
	class ConduitSegment
	{
		private java.awt.Point segBegin;  // Starting point, in coord space of Conduit..
		private java.awt.Point segEnd;  // Endpoint, in coord space of Conduit.
		
		public ConduitSegment(java.awt.Point segBegin, java.awt.Point segEnd)
		{
			this.segBegin = segBegin;
			this.segEnd = segEnd;
		}
		
		public java.awt.Point getSegBegin() { return segBegin; }
		public java.awt.Point getSegEnd() { return segEnd; }
	
		void setSegBegin(java.awt.Point segBegin) { this.segBegin = segBegin; }
		void setSegEnd(java.awt.Point segEnd) { this.segEnd = segEnd; }
		
		
		public void paintComponent(Graphics g)
		{
			// Draw a line from segBegin to segEnd.
			
			g.setColor(getContainerVisual().getConduitColor());
			g.setPaintMode();
			
			java.awt.Stroke stroke = 
				(ConduitVisualJ.this.doubleThickness ? thickStroke : normalStroke);
			
			if (g instanceof java.awt.Graphics2D)
			{
				((java.awt.Graphics2D)g).setStroke(stroke);
			}
			
			g.drawLine(segBegin.x, segBegin.y, segEnd.x, segEnd.y);
		}
	}
	
	private static final java.awt.Stroke normalStroke = new java.awt.BasicStroke((float)2.0);
	private static final java.awt.Stroke thickStroke = new java.awt.BasicStroke((float)4.0);
}

