/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.*;
import expressway.ser.*;
import generalpurpose.DateAndTimeUtils;
import java.util.List;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Set;
import java.text.NumberFormat;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.*;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class EventHistoryPanel extends JScrollPane
{
	private static final int MaxFractionDigits = 2;
	private final ModelScenarioVisual scenarioVisual;
	private final SimulationRunSer simRunSer;
	//private String simRunId;
	private int noOfNodes;  // columns
	private int noOfEpochs; // rows
	private NumberFormat numberFormat = NumberFormat.getInstance();
	
	
	public static JFrame createEventHistoryWindow(View view,
		String simRunId, Class... stateTags)
	throws
		Exception
	{
		NodeSer nodeSer = view.getModelEngine().getNode(simRunId);
		if (! (nodeSer instanceof SimulationRunSer)) throw new Exception(
			"Node with ID " + simRunId + " is not a SimulationRun");
		
		SimulationRunSer simRunSer = (SimulationRunSer)nodeSer;
		String domainId = simRunSer.domainId;
		if (domainId == null) throw new RuntimeException("Domain Id is null");
		
		StateSer[] stateSers = null;
		List<String> stateNames = null;
		List<String> stateIds = null;
		List<GeneratedEventSer>[] eventListArray = null;
		
		stateSers = view.getModelEngine().getStatesRecursive(domainId);
		
		stateNames = new Vector<String>();
		stateIds = new Vector<String>();
		for (StateSer stateSer : stateSers)
		{
			stateNames.add(stateSer.getFullName());
			stateIds.add(stateSer.getNodeId());
		}
		
		if ((stateTags == null) || (stateTags.length == 0))
		{
			eventListArray = 
				view.getModelEngine().getEventsForSimNodeByEpoch(simRunSer.getNodeId(), 
					stateNames);
		}
		else
		{
			Object[] result =
				view.getModelEngine().getEventsForSimNodeByEpoch(
					simRunSer.getNodeId(),
					stateNames,
					stateTags);
					
			eventListArray = (List<GeneratedEventSer>[])(result[0]);
			stateNames = (List<String>)(result[1]);  // filtered list
			stateIds = (List<String>)(result[2]);  // filtered list
		}
		
		if (stateNames == null) throw new RuntimeException(
			"The List of State names to report on is null");
		
		if (stateNames.size() == 0) throw new Exception(
			"There must be at least one State tagged as Inspect$ShowHistory.\n" +
			"See Online Help, Tag Values for more information.");
		
		String scenarioId = simRunSer.modelScenarioNodeId;
		

		// Determine if a ModelScenarioVisual is open for the Scenario.
		// If not, create one. Regardless, make it visible.
		
		Set<NodeView> domainPanels = view.getPanelManager().getNodeViewsForDomain(domainId);
		
		if (domainPanels.size() == 0)  // Create a ViewPanel for the Domain.
		{
			NodeView domainView = (NodeView)
				(view.getViewFactory().createModelDomainViewPanel(domainId, false, true));
			domainPanels.add(domainView);
		}
		
		ModelScenarioVisual sv = null;
		for (NodeView viewPanel : domainPanels)
		{
			if (viewPanel instanceof ModelScenarioViewPanel)
			{
				ModelScenarioViewPanel scenarioViewPanel = 
					(ModelScenarioViewPanel)viewPanel;
				
				sv = (ModelScenarioVisual)(scenarioViewPanel.showScenario(scenarioId));
				break;
			}
		}
		
		if (sv == null) throw new RuntimeException(
			"Unable to find or create a Visual for Scenario with ID " + scenarioId);
		
		
		// Create an Event History Window for showing the Event history
		// and for controlling the simulation replay.
		
		EventHistoryPanel historyPanel = 
			new EventHistoryPanel(sv, simRunSer,
				stateNames, stateIds, eventListArray);
	
		JFrame historyFrame = new JFrame("History for simulation " + simRunSer.getFullName());
		historyFrame.setLayout(new BorderLayout());
		historyFrame.setSize(800, 400);
		//historyFrame.add(historyPanel);
		historyFrame.add(historyPanel, BorderLayout.CENTER);
		historyPanel.doLayout();
		historyPanel.validate();
		
		historyFrame.pack();
		
		historyFrame.setVisible(true);
		
		return historyFrame;
	}
	
	
	protected EventHistoryPanel(ModelScenarioVisual scenarioVisual, 
		final SimulationRunSer simRunSer,
		final List<String> stateNames, 
		final List<String> stateIds,
		final List<GeneratedEventSer>[] eventListArray)
	throws
		ParameterError
	{
		super(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		this.scenarioVisual = scenarioVisual;
		this.simRunSer = simRunSer;
		final String simRunId = simRunSer.getNodeId();
		final long startingMs = simRunSer.actualStartTime.getTime();
		
		if (eventListArray.length == 0) throw new ParameterError(
			"No epochs in Event List array");
		
		if (stateNames.size() != stateIds.size()) throw new ParameterError(
			"The number of State names must be the same as the number of State Ids");
		
		
		// Format State names so that each name part displays on its own line.
		
		final List<String> formattedStateNames = new Vector<String>();
		for (String unformattedName : stateNames)
		{
			String formattedName = "<html>";
			String[] parts = unformattedName.split("\\.");
			boolean skippedDomainName = false;
			boolean firstPart = true;
			for (String part : parts)
			{
				if (! skippedDomainName)
				{
					skippedDomainName = true;
					continue;
				}
				
				if (firstPart) firstPart = false;
				else formattedName += "<br>";
				
				formattedName += part;
			}
			
			formattedName += "</html>";
			formattedStateNames.add(formattedName);
		}
		
		
		this.noOfEpochs = eventListArray.length;
		this.noOfNodes = stateNames.size();
		
		numberFormat.setMaximumFractionDigits(MaxFractionDigits);
		
		TableModel dataModel = new AbstractTableModel()
		{
			public int getColumnCount() { return noOfNodes+2; }
			
			public int getRowCount() { return noOfEpochs;}
			
			public String getColumnName(int column)
			{
				if (column == 0) return "<html>Iteration<br>Number</html>";
				else if (column == 1) return "<html>Elapsed Days</html>";
				else return formattedStateNames.get(column-2);
			}
			
			public Class getColumnClass(int column)
			{
				if (column == 0) return Integer.class;
				else if (column == 1) return String.class;
				else return String.class;
			}
			
			public Object getValueAt(int row, int col)
			{
				if (col == 0)
				{
					return new Integer(row+1);
				}
				else if (col == 1)
				{
					GeneratedEventSer eventSer = null;
					if (eventListArray[row].size() > 0) eventSer = eventListArray[row].get(0);
					if (eventSer == null) return "?";
					long elapsedMs = 
						eventSer.timeToOccur.getTime() - startingMs;
					double elapsedDays = 
						((double)elapsedMs) / ((double)(DateAndTimeUtils.MsInADay));
					return new Double(elapsedDays).toString();
				}
				else if (col > stateNames.size() + 1)
					throw new RuntimeException("Invalid column no: " + col);
				else
				{
					// Get the event for column [col-2]
					// Read through the EventSers until the column ID is found.
					
					List<GeneratedEventSer> eventSerList = eventListArray[row];
					String columnStateId = stateIds.get(col-2);
					
					boolean found = false;
					GeneratedEventSer newEventSer = null;
					for (GeneratedEventSer eventSer : eventSerList)
					{
						if (eventSer.stateNodeId.equals(columnStateId))
						{
							found = true;
							newEventSer = eventSer;
							break;
						}
					}
					
					if (!found)
					{
						for (int priorRow = row-1; priorRow >= 0; priorRow--)
							// each prior row priorRow, starting with the immediately prior,
						{
							List<GeneratedEventSer> priorEventSers = eventListArray[priorRow];
							boolean foundPrior = false;
							for (GeneratedEventSer priorEventSer: priorEventSers)
							{
								if (priorEventSer.stateNodeId.equals(columnStateId))
								{
									foundPrior = true;
									newEventSer = priorEventSer;
									break;
								}
							}
							
							if (foundPrior) break; // the column has a value in priorRow
						}
					}
					
					Serializable value;
					if (newEventSer == null) value = null;
					else value = newEventSer.newValue;
					
					String formattedValue = formatObjectValue(value);
					
					if (found)
						return "<html><b>" + formattedValue + "</b></html>";
					else if (newEventSer instanceof EventConflictSer)
						return "<html><font color=\"red\">" + formattedValue + "</font></html>";
					else
						return "<html><font color=\"gray\">" + formattedValue + "</font></html>";
				}
			}
		};
		
		final JTable table = new JTable(dataModel);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			public void valueChanged(ListSelectionEvent event)
			{
				if (event.getValueIsAdjusting()) return;
				
				int rowIndex = table.getSelectedRow();
				if (rowIndex < 0) return;
				
				int modelIndex = table.convertRowIndexToModel(rowIndex) + 1;
				
				// Inform the ScenarioVisual that the Scenario is interested in the
				// iteration indicated by 'modelIndex'.
				
				EventHistoryPanel.this.scenarioVisual.showEventsForIteration(
					simRunId, modelIndex);
			}
		});
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumnModel columnModel = table.getColumnModel();
		for (Enumeration<TableColumn> e = columnModel.getColumns(); e.hasMoreElements();)
			e.nextElement().setMinWidth(100);
		
		// The first column is the iteration number: it doesn't need to be as wide.
		columnModel.getColumn(0).setMinWidth(20);
		
		this.setViewportView(table);
		
		scenarioVisual.showEventsForIteration(simRunId, 1);
	}
	
	
	protected String formatObjectValue(Serializable value)
	{
		if (value == null) return("(null)");
		else if (value instanceof Number) try {
			return numberFormat.format(value); }
		catch (IllegalArgumentException iae)
		{
			System.err.println("Error formatting Number");
			return "(error)";
		}
		else if (value instanceof Boolean) {
			if (value.equals(Boolean.TRUE)) return "true";
			else return "false"; }
		else {
			System.err.println("Unable to format object of type " +
				value.getClass().getName());
			return "(" + value.toString() + ")"; }
	}
}

