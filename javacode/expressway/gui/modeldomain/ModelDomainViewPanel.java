/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import expressway.common.*;
import expressway.help.*;
import expressway.gui.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.Constants;
import expressway.common.VisualComponent.*;
import expressway.ser.*;
import expressway.help.*;
import geometry.PointImpl;
import generalpurpose.ThrowableUtil;
import generalpurpose.DateAndTimeUtils;
import awt.AWTTools;
import graph.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.Vector;
import java.util.List;
import java.util.Set;
import java.util.Date;
import generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.Collection;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;



/** ****************************************************************************
 * Adds behavior (popup menus and action handlers) to the Base class.
 */
 
public abstract class ModelDomainViewPanel extends ModelDomainViewPanelBase
{
	public static final String ViewTypeName = "ModelDomainView";
	
	public static final int PixelsPerMouseWheelClick = 10;
	
	/** The mouse must click within this number of pixels to select a border. */
	public static final double BorderSelectionTolerance = 5.0;
	
	/** The mouse must click within this number of pixels to select a Conduit. */
	public static final double ConduitSelectionTolerance = 3.0;
	
	/** Color used to draw Visual Component outline in XOR mode when a Visual is resized. */
	public Color OutlineXORColor = Color.white;

	
  /* ***************************************************************************
	 ***************************************************************************
	 * Initiatlization permanent state.
	 */
	
	private final PopulateActionListener populateActionListener;
	//private final DepopulateActionListener depopulateActionListener;
	private final MouseListener popupListener;
	private final MouseListener mouseInputListener;
	private final MouseMotionListener mouseMotionListener;
	private SetAttrValueHandler setAttrValueHandler;
	private ViewExpectedValueByScenarioHandler viewExpectedValueByScenarioHandler;
	private AddConduitHandler addConduitHandler;
	//private final JPopupMenu activityPopup;
	
	
	/** ************************************************************************
     * Constructor.
     */
	 
	public ModelDomainViewPanel(PanelManager container, NodeSer nodeSer,
		ViewFactory viewFactory, ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(container, nodeSer, nodeSer.getFullName(), viewFactory, modelEngine);

		if (! (nodeSer instanceof ModelElementSer)) throw new RuntimeException(
			"nodeSer is not a ModelElementSer");
		ModelElementSer modelElementSer = (ModelElementSer)nodeSer;
		
		// Create GUI action handlers.
		
		this.popupListener = new PopupListener();
		this.populateActionListener = new PopulateActionListener();
		//this.depopulateActionListener = new DepopulateActionListener();
		
		VisualDragListener mouseListener = new VisualDragListener();
		this.mouseInputListener = mouseListener;
		this.mouseMotionListener = mouseListener;

		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);
		this.addMouseWheelListener(mouseListener);
		
		// Create a popup menu item and add the action "View Evidence" action
		// handler to it.
		
		//JMenuItem menuItem = new JMenuItem("View Evidence");
		//menuItem.addActionListener(viewEvidenceActionListener);

		// Create a popup menu to contain the menu item.
		
		//this.activityPopup = new JPopupMenu();
		//this.activityPopup.add(menuItem);
	}
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * From ModelDomainViewPanelBase.
	 */
	
	
	protected MouseListener getPopupListener() { return this.popupListener; }
	
	
	protected MouseListener getMouseInputListener()
	{
		if (this.mouseInputListener == null) throw new RuntimeException(
			"Mouse input listener is null");
		
		return this.mouseInputListener;
	}
	
	
	protected MouseMotionListener getMouseMotionListener()
	{
		if (this.mouseMotionListener == null) throw new RuntimeException(
			"Mouse motion listener is null");
		
		return this.mouseMotionListener;
	}


	public synchronized String getViewType() { return ViewTypeName; }
	
	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Menu item factories.
	 */
	 
	 
	/** ************************************************************************
	 * Add (or remove) menu items (each with an Action Listener) to the specified
	 * popup menu. The variable visual is the target of the action. The x and y
	 * coordinates are the mouse location within the Component that is the original
	 * source of the Event.
	 */
	 
	protected void addContextMenuItems(JPopupMenu containerPopup, 
		final VisualJComponent visual, int x, int y)
	{
		// Check if the Container has already been populated.
		
		JMenuItem menuItem = null;
		
		//String[] childNodeIds = null;
		//if (visual instanceof ContainerVisualJ)
		//	childNodeIds = ((ContainerVisualJ)visual).getChildIds();
		
		String[] stateNodeIds = null;
		if (visual instanceof PortedContainerVisualJ)
			stateNodeIds = ((PortedContainerSer)(visual.getNodeSer())).stateNodeIds;
		

		if (visual instanceof PortVisual)
		{
			JMenuItem nameMenuItem = new JMenuItem("Port '" + visual.getName() + "'");
			//JMenuItem nameMenuItem = new JMenuItem("Port '" + visual.getName() + "'");
			containerPopup.add(nameMenuItem);
			containerPopup.addSeparator();
			
			JMenuItem connectMenuItem = new HelpfulMenuItem("Connect to other Port...",
				false, "Creates a " + HelpWindow.createHref("Conduits", "Conduit") + 
				" to connect this " + HelpWindow.createHref("Ports", "Port") +
				" to another Port," +
				" which you specify by selecting it.");
			connectMenuItem.addActionListener(new ConnectPortsActionListener(x, y));
			containerPopup.add(connectMenuItem);
			
			JMenuItem portDirMenuItem = new HelpfulMenuItem("Change Direction \u2192",
				false, "Change the direction of the " +
				HelpWindow.createHref("Ports", "Port") + " to one of in, out, or bidirectional.");
			portDirMenuItem.addActionListener(new ChangePortDirectionActionListener(x, y));
			containerPopup.add(portDirMenuItem);
			
			Container parent = visual.getParent();
			if ((((PortedContainerSer)(((VisualJComponent)parent).getNodeSer()))
				.stateNodeIds.length > 0) && (! visual.getNodeSer().isPredefined()))
				// Port''s container directly owns a State
			{
				JMenuItem bindToStateMenuItem = new HelpfulMenuItem("Bind to State...", false,
					"When a " + HelpWindow.createHref("Ports", "Port") +
					" is bound to a " + HelpWindow.createHref("States", "State") +
					", changes in the State's value propagate to the Port.");
					
				bindToStateMenuItem.addActionListener(new BindPortToStateActionListener(x, y));
				containerPopup.add(bindToStateMenuItem);
			}
			
			StateSer stateSer = null;
			try { stateSer = getModelEngine().getPortBinding(visual.getNodeId()); }
			catch (Exception ex) { ex.printStackTrace(); }
			
			if (stateSer != null) // Port is bound,
			{
				JMenuItem unbindMenuItem = new HelpfulMenuItem("Unbind from State", false,
					"When a " + HelpWindow.createHref("Ports", "Port") +
					" is <i><b>not</b></i> bound to a " + HelpWindow.createHref("States", "State") +
					", changes in the State's value do automatically <i><b>not</b></i> propagate" +
					" to the Port.");
				unbindMenuItem.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						try
						{
							try { getModelEngine().unbindPort(false, visual.getNodeId()); }
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog(null, w.getMessage(),
									"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
									getModelEngine().unbindPort(true, visual.getNodeId());
							}
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex); return;
						}
					}
				});
				
				containerPopup.add(unbindMenuItem);
			}
		}


		JMenuItem menuItem0 = new HelpfulMenuItem("Rename...", false,
			"Change the name of the Component.");
			
		menuItem0.addActionListener(getRenameHandler(visual.getName()));
		containerPopup.add(menuItem0);
		
		
		// Add menu item to display a popup View containing an expansion of the
		// selected Node.
		
		if (visual instanceof ContainerVisual)
		{
			menuItem = new HelpfulMenuItem("Expand in new window", false,
				"A new window will be opened, showing the contents of the component.");
			menuItem.addActionListener(ModelDomainViewPanel.this.populateActionListener);
			containerPopup.add(menuItem);
		}
		
		
		// If an Attribute, add item for setting value across all Scenarios.
		
		if (visual instanceof AttributeVisual)
		{
			JMenuItem menuItem4 = new HelpfulMenuItem("Set Value in all Scenarios...", false,
				"The value you enter will be set for the " +
				HelpWindow.createHref("Attributes", "Attribute") + " in each " +
				HelpWindow.createHref("Model Scenarios", "Scenario") + " of the '" +
				this.getName() + "' " + 
				HelpWindow.createHref("Model Domains", "Model Domain") + ".");
				
			menuItem4.addActionListener(getSetAttrValueHandler());
			containerPopup.add(menuItem4);
		}

		
		// Add menu item for displaying expected value by Scenario.
		
		if (visual instanceof StateVisual)
		{
			JMenuItem menuItem3a = new HelpfulMenuItem("View Expected Value By Scenario",
				false, "The value of the State in each " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				", averaged across all " + 
				HelpWindow.createHref("Simulation Runs", "Simulation Runs") + " for that " +
				"Scenario, is graphed by Scenario.");
				
			menuItem3a.addActionListener(getViewExpectedValueByScenarioHandler(
				ModelDomainViewPanel.this.getNodeId(), (StateVisual)visual));
			containerPopup.add(menuItem3a);
			
			Container parent = visual.getParent();
			if ((((PortedContainerSer)(((VisualJComponent)parent).getNodeSer()))
				.portNodeIds.length > 0) && (! visual.getNodeSer().isPredefined()))
				// State''s container directly owns a Port
			{
				JMenuItem bindToPortMenuItem = new HelpfulMenuItem("Bind to Port...", false,
					"When a " + HelpWindow.createHref("States", "State") +
					" is bound to a " + HelpWindow.createHref("Ports", "Port") +
					", changes in the State's value propagate to the Port.");
				
				bindToPortMenuItem.addActionListener(new BindStateToPortActionListener(x, y));
				containerPopup.add(bindToPortMenuItem);
			}
			
			JMenuItem menuItem3c = new HelpfulMenuItem("Tag as...", false,
				"Adds an Attribute to the Component and allows you to set " +
				"the value that the " + HelpWindow.createHref("Attributes", "Attribute") +
				" will have in <i>all</i> " + HelpWindow.createHref("Model Scenarios") +
				". This is useful for setting " + HelpWindow.createHref("Tag Values") +
				" such as " +
				HelpWindow.createHref("Tag Values", "Net_Value", "Net Value") + " and " +
				HelpWindow.createHref("Tag Values", "Show_History", "Show History") + "."
				);
				
			menuItem3c.addActionListener(getTagModelElementHandler());
			containerPopup.add(menuItem3c);
		}
		else if (visual instanceof ModelContainerVisualJ)
		{
			JMenuItem menuItem3b = new HelpfulMenuItem("View Expected Value By Scenario",
				false, 
				"Each " + HelpWindow.createHref("States", "State") +
				" within the Component (" + visual.getName() + ") " +
				"that has a Net Value Attribute is identified. " +
				"The value of each of these States in each " +
				HelpWindow.createHref("Model Scenarios", "Model Scenario") +
				", averaged across all " + 
				HelpWindow.createHref("Simulation Runs", "Simulation Runs") + " for that " +
				"Scenario, is graphed by Scenario.");
				
			menuItem3b.addActionListener(getViewExpectedValueByScenarioHandler(
				ModelDomainViewPanel.this.getNodeId(), null));
			containerPopup.add(menuItem3b);
		}
		
		
		if (visual instanceof PredefinedEventVisual)
		{
			PredefinedEventSer predEvSer = (PredefinedEventSer)(visual.getNodeSer());
			
			JMenuItem menuItemDefValue = new HelpfulMenuItem("Change Default Value", false,
				"A " + HelpWindow.createHref("Predefined Events", "Predefined Event") +
				" can have a default value which is used if no value is set for the " +
				"Predefined Event's value Attribute."
				);
				
			menuItemDefValue.addActionListener(new SetPredefEventDefaultValueHandler());
			
			containerPopup.add(menuItemDefValue);
			
			JMenuItem menuItemDefTime = new HelpfulMenuItem("Change Default Time", false,
				"A " + HelpWindow.createHref("Predefined Events", "Predefined Event") +
				" can have a default time (in days, from start of simulation) which " +
				"is used if no value is set for the Predefined Event's time Attribute."
				);
				
			menuItemDefTime.addActionListener(new SetPredefEventDefaultTimeHandler());
			
			containerPopup.add(menuItemDefTime);
			
			if (predEvSer.pulse) // a pulse Event,
			{ // Display menu item to make it a flow Event.
				JMenuItem menuItemFlow = new HelpfulMenuItem("Make a 'flow' Event", false,
					"A " + HelpWindow.createHref("Predefined Events", "Predefined Event") +
					" can be either a 'pulse' Event or a 'flow' Event. See " +
					HelpWindow.createHref("Events", "Pulse_Events_Versus_Flow_Events",
						"Pulse Events versus Flow Events") + "."
					);
				
				menuItemFlow.addActionListener(new SetPredefPulseHandler(
					predEvSer.getNodeId(), false));
				
				containerPopup.add(menuItemFlow);
			}
			else // Display menu item to make it a pulse Event
			{
				JMenuItem menuItemPulse = new HelpfulMenuItem("Make a 'pulse' Event", false,
					"A " + HelpWindow.createHref("Predefined Events", "Predefined Event") +
					" can be either a 'pulse' Event or a 'flow' Event. See " +
					HelpWindow.createHref("Events", "Pulse_Events_Versus_Flow_Events",
						"Pulse Events versus Flow Events") + "."
					);
				
				menuItemPulse.addActionListener(new SetPredefPulseHandler(
					predEvSer.getNodeId(), true));
				
				containerPopup.add(menuItemPulse);
			}
		}
		
		if ((visual instanceof ModelContainerVisual) || (visual instanceof PortVisual))
		{
			Set<VisualComponent> selectedVisuals = getSelected();
			boolean allPorts = true;
			if (selectedVisuals.size() == 0) allPorts = false;
			else for (VisualComponent selVis : selectedVisuals)
			{
				if (! (selVis instanceof PortVisual))
				{
					allPorts = false;
					break;
				}
			}
			
			if (allPorts && (selectedVisuals.size() >= 2))
			{
				JMenuItem addConduitMenuItem = new HelpfulMenuItem("Connect Selected Ports",
					false, "Create one or more " +
					HelpWindow.createHref("Conduits") +
					" within " + visual.getName() + " connecting the selected " +
					HelpWindow.createHref("Ports") + "");
				
				addConduitMenuItem.addActionListener(getAddConduitHandler());
				containerPopup.add(addConduitMenuItem);
			}
		}
		
		if (visual instanceof GeneratorVisualJ)
		{
			/*
			Make Self-Starting / Make Non-Self-Starting
			//Make Variable / Make Non-Variable
			Make Repeating / Make Non-Repeating
			*/
			
			final AbstractGeneratorSer genSer = (AbstractGeneratorSer)(visual.getNodeSer());
			
			JMenuItem ignoreStartupMenuItem;
			JMenuItem makeRepeatingMenuItem = null;
			
			
			if (genSer.ignoreStartup)
			{
				ignoreStartupMenuItem = new HelpfulMenuItem("Make Self-Starting", false,
					"If a " + HelpWindow.createHref("Generators", "Generator") +
					" is " + HelpWindow.createHref("Generators", "Self_Starting", "Self Starting") +
					" it starts its operation when simulation starts, without needing" +
					" to be triggered by an incoming " + 
					HelpWindow.createHref("Events", "Event") + ".");
			}
			else
			{
				ignoreStartupMenuItem = new HelpfulMenuItem("Make Non-Self-Starting", false,
					"If a " + HelpWindow.createHref("Generators", "Generator") +
					" is <i>not</i> " + HelpWindow.createHref("Generators", "Self_Starting", "Self Starting") +
					" it will not start its operation until it receives an " +
					HelpWindow.createHref("Events", "Event") + " on its input " +
					HelpWindow.createHref("Ports", "Port") + ".");
			}
			
			
			boolean rep = false;
			try
			{
				rep = getModelEngine().isGeneratorRepeating(genSer.getNodeId());
				if (rep)
				{
					makeRepeatingMenuItem = new HelpfulMenuItem("Make Non-Repeating", false,
						"If a " + HelpWindow.createHref("Generators", "Generator") +
						" is " + HelpWindow.createHref("Generators", "Repeating", "Repeating") +
						" it will continue to operate indefinitely, generating " +
						HelpWindow.createHref("Events") + " according to its distribution," +
						" without needing to be re-triggered."
						);
				}
				else
				{
					makeRepeatingMenuItem = new HelpfulMenuItem("Make Repeating", false,
						"If a " + HelpWindow.createHref("Generators", "Generator") +
						" is <i>not</i> " + HelpWindow.createHref("Generators", "Repeating", "Repeating") +
						" it will not generate another " +
						HelpWindow.createHref("Events", "Event") + " until it is re-triggered" +
						" by an " + HelpWindow.createHref("Events", "Event") +
						" on its input " + HelpWindow.createHref("Ports", "Port") + "."
						);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			
			final boolean repeating = rep;


			ignoreStartupMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try
					{
						try { getModelEngine().setGeneratorIgnoreStartup(false, genSer.getNodeId(),
							! (genSer.ignoreStartup)); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog((Component)visual,
								w.getMessage(), "Please confirm",
							JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null)
								== JOptionPane.YES_OPTION)
								getModelEngine().setGeneratorIgnoreStartup(true, genSer.getNodeId(),
									! (genSer.ignoreStartup));
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
					}
				}
			});

			containerPopup.add(ignoreStartupMenuItem);
			
			
			if (makeRepeatingMenuItem != null)
			{
				makeRepeatingMenuItem.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						try
						{
							try { getModelEngine().makeGeneratorRepeating(
								false, genSer.getNodeId(), !repeating); }
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog((Component)visual,
									w.getMessage(), "Please confirm",
									JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null)
									== JOptionPane.YES_OPTION)
									getModelEngine().makeGeneratorRepeating(
										true, genSer.getNodeId(), !repeating);
							}
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
						}
					}
				});
			
				containerPopup.add(makeRepeatingMenuItem);
			}
		}
		
		
		// Add menu items for adding and deleting Visuals.
		
		if (! (visual instanceof AttributeVisual))  // remove when nested Attrs are supported.
		{
			ActionListener insertNewActionListener = getInsertNewHandler(x, y);
			
			if (insertNewActionListener != null)
			{
				JMenuItem menuItem4 = new HelpfulMenuItem("Insert New \u2192",  // forward arrow
					false,
					"Create a new Component, to be owned by '" + visual.getName() +
					"'."
					);
					
				menuItem4.addActionListener(insertNewActionListener);
				containerPopup.add(menuItem4);
			}
		}
		
		ActionListener deleteActionListener = getDeleteHandler(visual);
		if ((deleteActionListener != null) && (! visual.getNodeSer().isPredefined()))
		{
			JMenuItem menuItem5 = new HelpfulMenuItem("Delete", false,
				"Delete the selected Components");
			menuItem5.addActionListener(deleteActionListener);
			containerPopup.add(menuItem5);
		}
		
		
		visual.addContextMenuItems(containerPopup, x, y);
	}
	
	
	class SetPredefEventDefaultValueHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			final VisualJComponent targetVisual = (VisualJComponent)originator;


			// Ask the user for the new value.
			
			final StringEntryPanel entryPanel = new StringEntryPanel("Enter new value...", false);
			entryPanel.setValueReturner(
				new StringEntryPanel.ValueReturner()
				{
					public void returnStringValue(String newValueString)
					{
						try
						{
							try
							{
								getModelEngine().setPredefEventDefaultValue(false,
									targetVisual.getNodeId(), newValueString);
									
								entryPanel.dispose();
							}
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog(null, w.getMessage(),
									"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								{
									getModelEngine().setPredefEventDefaultValue(true,
										targetVisual.getNodeId(), newValueString);
									
									entryPanel.dispose();
								}
							}
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
						}
					}
				});
			
			java.awt.Point targetLocation = targetVisual.getLocationOnScreen();
			
			entryPanel.setLocation(targetLocation.x + targetVisual.getWidth(),
				targetLocation.y);
				
			entryPanel.show();
		}


		public Serializable parseValue(String text)
		throws
			ParameterError
		{
			return ObjectValueWriterReader.parseObjectFromString(text);
		}
	}
	
	
	private static final long MaxDays = Long.MAX_VALUE / DateAndTimeUtils.MsInADay;
	
	
	class SetPredefEventDefaultTimeHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			final VisualJComponent targetVisual = (VisualJComponent)originator;


			// Ask the user for the new value.
			
			final StringEntryPanel entryPanel = new StringEntryPanel("Enter new time (days)...", false);
			entryPanel.setValueReturner(
				new StringEntryPanel.ValueReturner()
				{
					public void returnStringValue(String newTimeString)
					{
						try
						{
							try  // if a number, check its range
							{
								double newTimeDouble = 
									Double.parseDouble(newTimeString) * DateAndTimeUtils.MsInADay;
								
								if (newTimeDouble < 0.0) throw new Exception(
									"For predefined Event " + targetVisual.getNodeSer().getFullName() +
									": 'time' must have a positive time");
								
								if (newTimeDouble > Long.MAX_VALUE) throw new Exception(
									"For predefined Event " + targetVisual.getNodeSer().getFullName() +
									": 'time' " + newTimeString + " exceeds allowed range of " + 
									MaxDays + " days");
							}
							catch (NumberFormatException nfe) {}  // ignore
							
							try
							{
								getModelEngine().setPredefEventDefaultTime(false,
									targetVisual.getNodeId(), newTimeString);
									
								entryPanel.dispose();
							}
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog(null, w.getMessage(),
									"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								{
									getModelEngine().setPredefEventDefaultTime(true,
										targetVisual.getNodeId(), newTimeString);
									
									entryPanel.dispose();
								}
							}
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
						}
					}
				});
			
			java.awt.Point targetLocation = targetVisual.getLocationOnScreen();
			
			entryPanel.setLocation(targetLocation.x + targetVisual.getWidth(),
				targetLocation.y);
			
			entryPanel.show();
		}
	}


	class SetPredefPulseHandler implements ActionListener
	{
		private String predefEventId;
		private boolean pulse;
		
		SetPredefPulseHandler(String predefEventId, boolean pulse)
		{
			this.predefEventId = predefEventId;
			this.pulse = pulse;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				try { getModelEngine().setPredefEventIsPulse(false, 
					this.predefEventId, this.pulse); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(null, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					{
						getModelEngine().setPredefEventIsPulse(true, 
							this.predefEventId, this.pulse);
					}
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
			}
		}
	}


	class ConnectPortsActionListener implements ActionListener
	{
		/** Location at which popup was created within the Containing Component. */
		int mx, my;
		
		
		ConnectPortsActionListener(int x, int y) { this.mx = x; this.my = y; }
		
		
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			if (! (visual instanceof PortVisual)) throw new RuntimeException(
				"Expected a PortVisual: action was on a " + visual.getClass().getName());
			
			PortVisual portVisual = (PortVisual)visual;
			
			(new ConnectPortToPortWindow(mx, my, portVisual)).show();
		}
	}
	
	
	class ConnectPortToPortWindow extends JFrame
	{
		private JButton cancelButton;
		PortVisual portVisual;
		PortedContainerVisual container;
		ModelContainerVisual containerContainer;
		
		
		ConnectPortToPortWindow(int x, int y, PortVisual portVisual)
		{
			super("Bind " + portVisual.getName() + " to a State");
			this.setAlwaysOnTop(true);
			this.portVisual = portVisual;
			this.setSize(400, 200);
			this.setLocation(x, y);
			this.setAlwaysOnTop(true);
			this.setBackground(ModelAPITypes.TranslucentWhite);
			
			
			addWindowListener(new WindowListener()
			{
				public void windowActivated(WindowEvent e) {}
				public void windowClosed(WindowEvent e) {}
				
				public void windowClosing(WindowEvent e) { cancelConnectPortToPortMode(); }
				
				public void windowDeactivated(WindowEvent e) {}
				public void windowDeiconified(WindowEvent e) {}
				public void windowIconified(WindowEvent e) {}
				public void windowOpened(WindowEvent e) {}
			});
			
			
			// Put View into "Connect Port to Port" mode.
			
			setConnectPortToPortMode();
			
			
			// Add controls.
			
			setLayout(new java.awt.FlowLayout());
			
			this.container = (PortedContainerVisual)(((VisualJComponent)portVisual).getParent());
				
			this.containerContainer = (ModelContainerVisual)(((VisualJComponent)container).getParent());
				
			add(new JLabel("Select a Port within " + container.getName() +
				" or " + containerContainer.getName()));
			
			add(cancelButton = new JButton("Cancel"));
			
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					cancelConnectPortToPortMode();
					ConnectPortToPortWindow.this.dispose();
				}
			});
			
			this.show();
		}
		
		
		protected void setConnectPortToPortMode()
		{
			ModelDomainViewPanel.this.setSelectMode(new VisualSelectionHandler()
			{
				public void visualSelected(VisualComponent selectedVisual)
				{
					if (! (selectedVisual instanceof PortVisual))
					{
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							"Must select a Port", "Error",
							JOptionPane.ERROR_MESSAGE); return;
					}
					
					Container selectedVisualContainer = ((VisualJComponent)selectedVisual).getParent();
					//if (! ((selectedVisualContainer == ConnectPortToPortWindow.this.container) ||
					//		(selectedVisualContainer == ConnectPortToPortWindow.this.containerContainer)))
					//{
					//	JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					//		"Selected Port must belong to either " + container.getName() +
					//			" or " + containerContainer.getName(),
					//		"Error", JOptionPane.ERROR_MESSAGE); return;
					//}
					
					
					// Create Conduit connecting portVisual to selectedVisual.
					
					try
					{
						try { getModelEngine().createConduit(
							false, //((VisualJComponent)selectedVisualContainer).getNodeId(),
							portVisual.getNodeId(), selectedVisual.getNodeId()); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								getModelEngine().createConduit(true, 
									//((VisualJComponent)selectedVisualContainer).getNodeId(),
									portVisual.getNodeId(), selectedVisual.getNodeId());
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex); return;
					}
					
					cancelConnectPortToPortMode();
					ConnectPortToPortWindow.this.dispose();
				}
			});
		}
		
		
		protected void cancelConnectPortToPortMode()
		{
			ModelDomainViewPanel.this.cancelSelectMode();
		}
	}
	
	
	class BindPortToStateActionListener implements ActionListener
	{
		/** Location at which popup was created within the Containing Component. */
		int mx, my;
		
		
		BindPortToStateActionListener(int x, int y) { this.mx = x; this.my = y; }
		
		
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			if (! (visual instanceof PortVisual)) throw new RuntimeException(
				"Expected a PortVisual: action was on a " + visual.getClass().getName());
			
			PortVisual portVisual = (PortVisual)visual;
			
			// Create a non-model stay-on-top popup and the GUI is put into 
			// "Bind Port To State" mode that can only be escaped by selecting 
			// a State or by closing the popup.
			
			(new BindPortToStateWindow(mx, my, portVisual)).show();
		}
	}
	
	
	class BindPortToStateWindow extends JFrame
	{
		private JButton cancelButton;
		PortVisual portVisual;
		
		
		BindPortToStateWindow(int x, int y, PortVisual portVisual)
		{
			super("Bind " + portVisual.getName() + " to a State");
			this.setAlwaysOnTop(true);
			this.portVisual = portVisual;
			this.setSize(500, 200);
			this.setLocation(x, y);
			this.setAlwaysOnTop(true);
			this.setBackground(ModelAPITypes.TranslucentWhite);
			
			
			addWindowListener(new WindowListener()
			{
				public void windowActivated(WindowEvent e) {}
				public void windowClosed(WindowEvent e) {}
				
				public void windowClosing(WindowEvent e) { cancelBindToPortMode(); }
				
				public void windowDeactivated(WindowEvent e) {}
				public void windowDeiconified(WindowEvent e) {}
				public void windowIconified(WindowEvent e) {}
				public void windowOpened(WindowEvent e) {}
			});
			
			
			// Put View into "Bind Port to State" mode.
			
			setBindToPortMode();
			
			
			// Add controls.
			
			setLayout(new java.awt.FlowLayout());
			
			PortedContainerVisual container = 
				(PortedContainerVisual)(((VisualJComponent)portVisual).getParent());
				
			add(new JLabel("Select a State within " + container.getName() +
				"; any existing binding for " + portVisual.getName() +
				" will be removed."));
			
			add(cancelButton = new JButton("Cancel"));
			
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					cancelBindToPortMode();
					BindPortToStateWindow.this.dispose();
				}
			});
			
			this.show();
		}
		
		
		protected void setBindToPortMode()
		{
			ModelDomainViewPanel.this.setSelectMode(new VisualSelectionHandler()
			{
				public void visualSelected(VisualComponent selectedVisual)
				{
					if (! (selectedVisual instanceof StateVisual))
					{
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							"Must select a State", "Error",
							JOptionPane.ERROR_MESSAGE); return;
					}
					
					Container selectedVisualContainer = ((VisualJComponent)selectedVisual).getParent();
					if (selectedVisualContainer != ((VisualJComponent)portVisual).getParent())
					{
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							"Selected State must belong to " + ((VisualJComponent)portVisual).getParent().getName(),
							"Error", JOptionPane.ERROR_MESSAGE); return;
					}
					
					String stateId = null;
					try { stateId = selectedVisual.getNodeId(); }
					catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
					
					String portId = null;
					try { portId = BindPortToStateWindow.this.portVisual.getNodeId(); }
					catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
					
					try
					{
						try { getModelEngine().bindStateAndPort(false, stateId, portId); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								getModelEngine().bindStateAndPort(true, stateId, portId);
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex); return;
					}

					
					cancelBindToPortMode();
					BindPortToStateWindow.this.dispose();
				}
			});
		}
		
		
		protected void cancelBindToPortMode()
		{
			ModelDomainViewPanel.this.cancelSelectMode();
		}
	}
	
	
	/**
	 * Put this View into a mode such that it will only process mouse press events.
	 * For each mouse press event, identify the Visualthat was clicked on and pass
	 * that to the specified VisualSelectionHandler.
	 */
	 
	void setSelectMode(VisualSelectionHandler selectionHandler)
	{
		this.selectionHandler = selectionHandler;
	}
	
	
	/**
	 * Remove this View from the mode set by setSelectMode().
	 */
	 
	void cancelSelectMode()
	{
		selectionHandler = null;
	}
	
	
	boolean isSelectMode() { return (selectionHandler != null); }
	
	
	private VisualSelectionHandler selectionHandler;
	
	
	VisualSelectionHandler getSelectionHandler() { return selectionHandler; }
	
	
	/**
	 * Given to the View by Action Handlers that want the user to modally select a
	 * Visual Component.
	 */
	 
	interface VisualSelectionHandler
	{
		void visualSelected(VisualComponent selectedVisual);
	}
	
	
	class BindStateToPortActionListener implements ActionListener
	{
		/** Location at which popup was created within the Containing Component. */
		int mx, my;
		
		
		BindStateToPortActionListener(int x, int y) { this.mx = x; this.my = y; }
		
		
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			if (! (visual instanceof StateVisual)) throw new RuntimeException(
				"Expected a StateVisual: action was on a " + visual.getClass().getName());
			
			StateVisual stateVisual = (StateVisual)visual;
			
			// a non-model stay-on-top popup to display that tells the user to 
			// select a Port. Put the GUI is put into a "Connect Ports" mode that
			// can only be escaped by closing the popup or selecting a Port: any
			// other action displays a modal error Dialog.
			
			(new BindStateToPortWindow(mx, my, stateVisual)).show();
		}
	}
	
	
	class BindStateToPortWindow extends JFrame
	{
		private JButton cancelButton;
		StateVisual stateVisual;
		
		
		BindStateToPortWindow(int x, int y, StateVisual stateVisual)
		{
			super("Bind " + stateVisual.getName() + " to a Port");
			this.setAlwaysOnTop(true);
			this.stateVisual = stateVisual;
			this.setSize(300, 200);
			this.setLocation(x, y);
			this.setAlwaysOnTop(true);
			this.setBackground(ModelAPITypes.TranslucentWhite);
			
			
			addWindowListener(new WindowListener()
			{
				public void windowActivated(WindowEvent e) {}
				public void windowClosed(WindowEvent e) {}
				
				public void windowClosing(WindowEvent e) { cancelBindToStateMode(); }
				
				public void windowDeactivated(WindowEvent e) {}
				public void windowDeiconified(WindowEvent e) {}
				public void windowIconified(WindowEvent e) {}
				public void windowOpened(WindowEvent e) {}
			});
			
			
			// Put View into "Bind Port to State" mode.
			
			setBindToStateMode();
			
			
			// Add controls.
			
			setLayout(new java.awt.FlowLayout());
			
			PortedContainerVisual container = 
				(PortedContainerVisual)(((VisualJComponent)stateVisual).getParent());
				
			add(new JLabel("Select a Port within " + container.getName()));
			
			add(cancelButton = new JButton("Cancel"));
			
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					cancelBindToStateMode();
					BindStateToPortWindow.this.dispose();
				}
			});
			
			this.show();
		}
		
		
		protected void setBindToStateMode()
		{
			ModelDomainViewPanel.this.setSelectMode(new VisualSelectionHandler()
			{
				public void visualSelected(VisualComponent selectedVisual)
				{
					if (! (selectedVisual instanceof PortVisual))
					{
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							"Must select a Port", "Error",
							JOptionPane.ERROR_MESSAGE); return;
					}
					
					Container selectedVisualContainer = ((VisualJComponent)selectedVisual).getParent();
					if (selectedVisualContainer != ((VisualJComponent)stateVisual).getParent())
					{
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							"Selected Port must belong to " + ((VisualJComponent)stateVisual).getParent().getName(),
							"Error", JOptionPane.ERROR_MESSAGE); return;
					}
					
					String stateId = null;
					try { stateId = BindStateToPortWindow.this.stateVisual.getNodeId(); }
					catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }

					String portId = null;
					try { portId = selectedVisual.getNodeId(); }
					catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
					
					try
					{
						try { getModelEngine().bindStateAndPort(false, stateId, portId); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								getModelEngine().bindStateAndPort(true, stateId, portId);
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex); return;
					}
					
					cancelBindToStateMode();
					BindStateToPortWindow.this.dispose();
				}
			});
		}
		
		
		protected void cancelBindToStateMode()
		{
			ModelDomainViewPanel.this.cancelSelectMode();
		}
	}
	
	
	class ChangePortDirectionActionListener implements ActionListener
	{
		/** Location at which popup was created within the Containing Component. */
		int mx, my;
		
		
		ChangePortDirectionActionListener(int x, int y) { this.mx = x; this.my = y; }
		
		
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			if (! (visual instanceof PortVisual)) throw new RuntimeException(
				"Expected a PortVisual: action was on a " + visual.getClass().getName());
			
			PortVisual portVisual = (PortVisual)visual;
			
			PortDirectionType direction = null;
			try { direction = ((PortSer)(portVisual.getNodeSer())).direction; }
			catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
			
			
			// Create a popup menu that contains a menu item for each of the
			// possible directions except the current direction of the Port.
			
			JPopupMenu changePortDirPopup = new JPopupMenu();
				
			
			for (PortDirectionType dir : PortDirectionType.values())
			{
				if (dir.equals(direction)) continue;  // skip the current direction.
				
				JMenuItem menuItem = null;
				switch (dir)
				{
					case input:
						menuItem = new HelpfulMenuItem(String.valueOf(dir), false,
							"An " + HelpWindow.createHref("Ports", "Input", "Input Port") +
							" can only pass " + HelpWindow.createHref("Events") +
							" from outside the Component to the inside."
							);
						break;
						
					case output:
						menuItem = new HelpfulMenuItem(String.valueOf(dir), false,
							"An " + HelpWindow.createHref("Ports", "Output", "Output Port") +
							" can only pass " + HelpWindow.createHref("Events") +
							" from inside the Component to the outside."
							);
						break;
						
					case bi:
						menuItem = new HelpfulMenuItem(String.valueOf(dir), false,
							"A " + HelpWindow.createHref("Ports", "Bi_Directional", "Bi-Directional Port") +
							" can pass " + HelpWindow.createHref("Events") +
							" in any direction."
							);
						break;
						
					default: throw new RuntimeException("Unexpected Port direction");
				}
				
					
				try { menuItem.addActionListener(new SetPortDirectionHandler(
					portVisual.getNodeId(), dir)); }
				catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }


				changePortDirPopup.add(menuItem);
			}
			
			changePortDirPopup.show(visual, mx, my);
		}
	}
	
	
	class SetPortDirectionHandler implements ActionListener
	{
		private final String portNodeId;
		private final PortDirectionType dir;
		
		
		SetPortDirectionHandler(String portNodeId, PortDirectionType dir)
		{
			this.portNodeId = portNodeId;
			this.dir = dir;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				try { getModelEngine().setPortDirection(false, portNodeId, dir); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(null, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						getModelEngine().setPortDirection(true, portNodeId, dir);
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
			}
		}
	}
	
	
	/**
	 * If no menu items are appropriate for the Visual, return null.
	 */
	 
	protected ActionListener getInsertNewHandler(int x, int y)
	{
		return new InsertNewActionListener(x, y);
	}
	
		
	/** ************************************************************************
	 * Respond to a request to instantiate a new Model Element, by popping up
	 * a list of the possible Model Element kinds.
	 */
	 
	class InsertNewActionListener implements ActionListener
	{
		/** Location at which popup was created within the Containing Component. */
		int mx, my;
		
		
		InsertNewActionListener(int x, int y) { this.mx = x; this.my = y; }
		
		
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			
			// Create a popup menu that contains a menu item for each of the
			// kinds of ModelElement that can be instantiated within the Visual.
			
			JPopupMenu insertNewPopup = new JPopupMenu();
			
			
			// Add menu item for each Container child Element kind.
			
			String[] instantiableKindNames;
			String[] descriptions;
			try
			{
				instantiableKindNames = 
					getModelEngine().getInstantiableNodeKinds(
						visual.getNodeSer().getNodeId());
					
				descriptions =
					getModelEngine().getInstantiableNodeDescriptions(
						visual.getNodeSer().getNodeId());
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex); return;
			}
			
			if (instantiableKindNames == null) return;
			
			JMenuItem menuItem;
			int i = 0;
			for (String name : instantiableKindNames)
			{
				String description = descriptions[i++];
				
				if (description.startsWith("@"))  // description is a Web page name.
					menuItem = new HelpfulMenuItem(name, true, description.substring(1));
				else
					menuItem = new HelpfulMenuItem(name, false, description);
				
				menuItem.addActionListener(new InsertNewNodeHandler(mx, my,
					visual.getNodeSer().getNodeId(), name, visual));

				insertNewPopup.add(menuItem);
			}
			
			insertNewPopup.show(visual, mx, my);
		}
	}
	
	
	/** ************************************************************************
	 * Respond to a selection of a Model Element kind by sending a request to the
	 * server to instantiate that Model Element kind.
	 */
	 
	class InsertNewNodeHandler implements ActionListener
	{
		private int x, y;
		private String parentNodeId;
		private String nodeKind;  // motif types are qualified names.
		private VisualJComponent visual;
		
		
		InsertNewNodeHandler(int x, int y, String parentNodeId, String nodeKind,
			VisualJComponent visual)
		{
			this.x = x; this.y = y;
			this.parentNodeId = parentNodeId;
			this.nodeKind = nodeKind;
			this.visual = visual;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			double xLogical = (x - visual.getInsets().left) * getPixelXSize();
			double yLogical = (visual.getHeight() - visual.getInsets().top -
				visual.getInsets().bottom - y) * getPixelYSize();
				
			double nodeWidth = visual.getNodeSer().getWidth();
			double nodeHeight = visual.getNodeSer().getHeight();
			
			xLogical = Math.min(nodeWidth, xLogical);
			xLogical = Math.max(0.0, xLogical);
			
			yLogical = Math.min(nodeHeight, yLogical);
			yLogical = Math.max(0.0, yLogical);

			try
			{
				try { getModelEngine().createModelElement(false,
					parentNodeId, nodeKind, xLogical, yLogical); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(null, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						getModelEngine().createModelElement(true,
							parentNodeId, nodeKind, xLogical, yLogical);
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
			}
		}
	}
	
	
	/** ************************************************************************
	 * 
	 */
	 
	class RenameHandler implements ActionListener
	{
		String currentName;
		
		
		RenameHandler(String currentName)
		{
			this.currentName = currentName;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			final VisualJComponent targetVisual = (VisualJComponent)originator;


			// Ask the user for the new name.
			
			final StringEntryPanel entryPanel = new StringEntryPanel("Enter new name...", false);
			entryPanel.setText(this.currentName);
			entryPanel.setValueReturner(
				new StringEntryPanel.ValueReturner()
				{
					public void returnStringValue(String newName)
					{
						try
						{
							try
							{
								getModelEngine().setNodeName(false, targetVisual.getNodeId(),
									newName);
									
								entryPanel.dispose();
							}
							catch (Warning w)
							{
								if (JOptionPane.showConfirmDialog(null, w.getMessage(),
									"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
								{
									getModelEngine().setNodeName(true, targetVisual.getNodeId(),
										newName);
									
									entryPanel.dispose();
								}
							}
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
						}
					}
				});
			
			java.awt.Point targetLocation = targetVisual.getLocationOnScreen();
			
			entryPanel.setLocation(targetLocation.x + targetVisual.getWidth(),
				targetLocation.y);
			
			entryPanel.show();
		}
	}
	
	
	/** ************************************************************************
	 * 
	 */
	 
	class DeleteActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			if (! (originator instanceof VisualJComponent)) return;
			
			VisualJComponent targetVisual = (VisualJComponent)originator;
			
			//Set<VisualComponent> selectedVisuals = ModelDomainViewPanel.this.getSelected();
			//selectedVisuals.add(targetVisual);
			
			try
			//for (VisualComponent visual : selectedVisuals) try
			{
				try { getModelEngine().deleteNode(false, targetVisual.getNodeId()); }
				catch (Warning w)
				{
					if (JOptionPane.showConfirmDialog(null, w.getMessage(),
						"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						getModelEngine().deleteNode(true, targetVisual.getNodeId());
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
			}
		}
	}
	
	
	protected ActionListener getRenameHandler(String currentName)
	{
		return new RenameHandler(currentName);
	}
	
	
	/**
	 * If Delete is not appropriate for the Visual, return null.
	 */
	 
	protected ActionListener getDeleteHandler(VisualJComponent visual)
	{
		return new DeleteActionListener();
	}
	
	
	protected ActionListener getSetAttrValueHandler()
	{
		// Create lazily.
		if (this.setAttrValueHandler == null)
			this.setAttrValueHandler = new SetAttrValueHandler();
		
		return this.setAttrValueHandler;
	}

	
	protected ActionListener getViewExpectedValueByScenarioHandler(String domainId,
		StateVisual sv)
	{
		try { return new ViewExpectedValueByScenarioHandler(this, getPanelManager(),
			domainId, sv == null ? null : sv.getNodeId()); }
		catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
	}
	
	
	protected ActionListener getTagModelElementHandler()
	{
		return new TagModelElementHandler();
	}
	

	protected ActionListener getAddConduitHandler()
	{
		// Create lazily.
		if (this.addConduitHandler == null)
			this.addConduitHandler = new AddConduitHandler();
		
		return this.addConduitHandler;
	}
		

	/**
	 * Find the Conduit Visual that is closest to the specified Point, and that
	 * is within ConduitSelectionTolerance. The Conduit must be an immediate child of
	 * the specified Container. If no Conduit meets these conditions
	 * then return null. The Point is expressed in the coordinates of
	 * the specified Container.
	 */
	 
	protected ConduitVisualJ findCloseConduit(ContainerVisualJ container,
		java.awt.Point locInContainer)
	{
		ConduitVisualJ closestConduit = null;
		double shortestDistance = 0.0;
		
		Insets insets = container.getInsets();
		int xInCond = locInContainer.x - insets.left;
		int yInCond = locInContainer.y - insets.top;
		java.awt.Point locInConduit = new java.awt.Point(xInCond, yInCond);
		
		Component[] components = container.getComponents();
		
		for (int zOrder = components.length-1; zOrder >= 0; zOrder--) // deepest first
		{
			Component component = components[zOrder];
			
			if (component instanceof ConduitVisualJ)
			{
				ConduitVisualJ conduit = (ConduitVisualJ)component;
				double distance = conduit.getDistanceFrom(locInConduit);
				
				if (distance < ConduitSelectionTolerance)
				{
					if ((closestConduit == null) || (distance < shortestDistance))
					{
						closestConduit = conduit;
						shortestDistance = distance;
					}
				}
			}
		}
		
		return closestConduit;
	}
	
	
	/**
	 * Determine whether the specified Point is within the area designated as a
	 * tab for resizing the specified Visual. Such a tab is normally depicted
	 * visually by a shaded or hashed area on the corner of the Visual.
	 */
	 
	protected boolean pointIsWithinDragTabOfVisual(java.awt.Point locInComp,
		VisualJComponent visual)
	{
		int x = locInComp.x;
		int y = locInComp.y;
		int w = visual.getWidth();
		int h = visual.getHeight();
		
		if (x > w) return false;
		if (x < 0) return false;
		if (y > h) return false;
		if (y < 0) return false;
		
		return (w-x + h-y) <= VisualJComponent.ResizingTabDiam;
	}
	
	
	/**
	 * Find the border of the specified Visual Component that is closest to the 
	 * specified Point, and that is within BorderSelectionTolerance. The Point
	 * need not be within the bounds of the Component. If no Visual Component
	 * border meets these conditions return -1. Otherwise, return the border (of
	 * the Visual), where 0 is the bottom, 1 is the right, 2 is the top, and 3
	 * is the left.
	 *
	 
	protected int findClosestBorder(VisualJComponent visual, java.awt.Point locInComp)
	{
		int x = locInComp.x;
		int y = locInComp.y;
		int w = visual.getWidth();
		int h = visual.getHeight();
		
		int dist[] = new int[4];
		dist[0] = Math.abs(h-y);
		dist[1] = Math.abs(w-x);
		dist[2] = Math.abs(y);
		dist[3] = Math.abs(x);
		
		int closestBorder = -1;
		double shortestDistance = 0.0;
		for (int border = 0; border < 4; border++)
		{
			double distance = dist[border];

			if (distance < BorderSelectionTolerance)
			{
				if ((closestBorder == -1) || (distance < shortestDistance))
				{
					closestBorder = border;
					shortestDistance = distance;
				}
			}
		}
		
		return closestBorder;
	}*/
					

	
  /* ***************************************************************************
	 * *************************************************************************
	 * GUI event handlers.
	 */
	 
	 
	/** ************************************************************************
	 * Handler for a Visual drag or selection operation.
	 * See the slides "MousePressed Handling in ModelDomainViewPanel",
	 * "MouseDrag Handling in ModelDomainViewPanel", and
	 * "MouseRelease Handling in ModelDomainViewPanel".
	 */
	 
	class VisualDragListener implements MouseListener, MouseMotionListener,
		MouseWheelListener
	{
		/** The Visual receiving the Mouse Down event. This Visual is then the
			recipient of all subsequent events until after MouseUp. */
		private VisualJComponent startingVisual = null;
		
		/** The point at which the drag or resize operation began. */
		private java.awt.Point mouseStartingPointInVisual = null;
		
		/** The original location of the Visual, before the drag or resize began. */
		private java.awt.Point originalVisualPositionInParent = null;
		
		/** The absolute screen location of the mouse-press event. */
		private java.awt.Point originalAbsoluteMouseStartingPoint = null;
		
		/** The original size of the Visual, before resize begain. */
		private java.awt.Dimension originalSize = null;
		
		///** The border of the startingVisual that is being dragged to resize the Visual,
		//	where the value of borderBeingMoved is from 0-3 representng the
		//	bottom, right, top, and left borders, respectively. If -1, then the
		//	Visual is being dragged as a whole rather than being resized. */
			
		/** If true, the border is being dragged to resize the Visual; otherwise,
			the entire Visual is being dragged. */
		private boolean borderBeingMoved = false;
		//private int borderBeingMoved = -1;
		
		/** See the slide "MousePressed Handling in ModelDomainViewPanel". */
		//private boolean conduitsChecked = false;
		private boolean conduitSelected = false;
		
		int newVisualX;
		int newVisualY;
		int newVisualWidth;
		int newVisualHeight;
		boolean selectedOutermostVisual;
		
		
		public synchronized void mousePressed(MouseEvent e)
		{
			if (e.getButton() != MouseEvent.BUTTON1) return;
			Component source = e.getComponent();
			
			if (! (source instanceof VisualJComponent))
			{
				startingVisual = null;
				return;
			}
			
			VisualJComponent visual = (VisualJComponent)source;
			
			
			// If source is a Conduit, forward to the deepest containing non-Conduit.
			
			if (forwardToDeepestConduitOfThisContainer(e)) return;
			if (visual instanceof ConduitVisualJ)
			{
				AWTTools.forwardEventToDeepestContainingComponent(e);
				return;
			}
			
			if (visual instanceof ModelContainerVisualJ)
			{
				// Determine if the user selected a Conduit.
				
				ConduitVisualJ closestConduit = findCloseConduit(
					(ModelContainerVisualJ)visual, e.getPoint());
					
				if (closestConduit != null)
				{
					//conduitsChecked = true;
					conduitSelected = true;
					closestConduit.highlight(! closestConduit.isHighlighted());
					
					//e.setSource(closestConduit);
					//closestConduit.dispatchEvent(e);  // Forward to conduit.
					source.repaint();
					notifyComponentRepainted(source);
					return;
				}
			}
			
			if (isSelectMode())
			{
				VisualSelectionHandler selectionHandler = getSelectionHandler();
				if (selectionHandler != null)
					selectionHandler.visualSelected(visual);
				
				return;
			}
			
			
			// Set state variables for the drag operation.
			
			this.startingVisual = visual;
			this.mouseStartingPointInVisual = e.getPoint();
			this.originalVisualPositionInParent = visual.getLocation();
			this.originalAbsoluteMouseStartingPoint = e.getLocationOnScreen();
			this.originalSize = visual.getSize();
			this.selectedOutermostVisual = visual.isOutermostInView();
			

			// Check if the user is trying to resize the Visual.
			
			if (visual.getAsIcon())
				borderBeingMoved = false;
			else
				if (visual.getNodeSer().isResizable())  // resizable?
					borderBeingMoved = pointIsWithinDragTabOfVisual(e.getPoint(), visual);
				else
					borderBeingMoved = false;
			
			if (borderBeingMoved)
			{
				newVisualX = visual.getX();
				newVisualY = visual.getY();
				newVisualWidth = visual.getWidth();
				newVisualHeight = visual.getHeight();
				
				//Graphics g = visual.getParent().getGraphics();
				
				
				//g.setXORMode(OutlineXORColor);
				//g.drawRect(newVisualX, newVisualY, newVisualWidth, newVisualHeight);
			}
			else  // border is not being moved.
			{
				// Selection logic.
				
				if (source instanceof ModelDomainVisual) selectAll(false);
				else
				{
					boolean wasSelected = isSelected(visual);
		
					if (!  // ctrl and alt pressed not pressed
						(
						e.isControlDown() ||
						e.isAltDown() ||
						e.isShiftDown() ||
						e.isMetaDown()
						)
					   )
						selectAll(false);  // Unslect any Visuals that were selected.
		
					visual.select(! wasSelected);
				}
			}

			ModelDomainViewPanel.this.repaint();
		}
		
		
		public synchronized void mouseDragged(MouseEvent e)
		{
			if (conduitSelected) return;
			
			if (isSelectMode()) return;
			
			if (
				(e.getButton() != MouseEvent.BUTTON1)
				&& (e.getButton() != 0)  // needed for Windows bug
				)
				return;
			
			if (startingVisual == null) return;
			
			Component source = e.getComponent();

			if (source != startingVisual) // Forward to startingVisual.
			{
				AWTTools.forwardEventToContainingComponent(e, startingVisual);
				return;
			}
			
			
			// Determine if event is outside the target's Container.
			
			Component eventTarget = e.getComponent();
			if (! eventTarget.getParent().contains(e.getX() + eventTarget.getX(),
				e.getY() + eventTarget.getY())) return;
			
			
			VisualJComponent visual = startingVisual;
			
			if (visual instanceof ConduitVisualJ) return;
			
			
			// Compute the Visual displacement of the drag Event, relative
			// to the prior mouse location.
			
			int dx = e.getX() - mouseStartingPointInVisual.x;
			int dy = e.getY() - mouseStartingPointInVisual.y;
			
			
			// Initialize starting point and size.
			
			newVisualX = visual.getX();
			newVisualY = visual.getY();
			newVisualWidth = originalSize.width;
			newVisualHeight = originalSize.height;
			
			Container parent = visual.getParent();
			
			if (borderBeingMoved)  // Resizing.
			{
				newVisualWidth += dx;
				newVisualHeight += dy;
				
				visual.setSize(newVisualWidth, newVisualHeight);
				parent.repaint();
			}
			else  // Dragging the entire Visual.
			{
				if (! visual.getNodeSer().isMovable()) return;
				
				
				// Update the Visual's location, causing the Visual to move on the screen.
				
				newVisualX += dx;
				newVisualY += dy;
				
				
				// Tentatively move the Visual to the drag location, to give
				// the user visual feedback. Do not update the position of the
				// corresponding Node.
				
				visual.setLocation(newVisualX, newVisualY);
				parent.repaint();
			}
		}
		
		
		public synchronized void mouseReleased(MouseEvent e)
		{
			if (conduitSelected)
			{
				conduitSelected = false;
				return;
			}
			
			if (isSelectMode()) return;

			if (e.getButton() != MouseEvent.BUTTON1) return;
			
			if (startingVisual == null) return;
			
			Component source = e.getComponent();

			if (source != startingVisual) // Forward to startingVisual.
			{
				AWTTools.forwardEventToContainingComponent(e, startingVisual);
				return;
			}
			
			
			VisualJComponent visual = startingVisual;
			
			if (visual instanceof ConduitVisualJ) return;
			
			
			/*if (borderBeingMoved >= 0)
			{
				Graphics g = visual.getParent().getGraphics();
				g.setXORMode(OutlineXORColor);
				g.drawRect(newVisualX, newVisualY, newVisualWidth, newVisualHeight);
			}*/
			
			
			int dx = e.getXOnScreen() - originalAbsoluteMouseStartingPoint.x;
			int dy = e.getYOnScreen() - originalAbsoluteMouseStartingPoint.y;

			double nodeDx, nodeDy;
			Container parent = visual.getParent();
			
			try
			{
				if (borderBeingMoved)  // Resizing. See slide "Resizing a Visual".
				{
					nodeDy = transformViewDYToNode(dy);
					nodeDx = transformViewDXToNode(dx);
					
					if (parent instanceof ContainerVisual)
					{
						try
						{
							resizeNode(visual, nodeDx, nodeDy, 0.0, nodeDy, false);
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
							return;
						}
					}
					else if (parent instanceof GraphicView.VisualComponentDisplayArea)
					{
						try
						{
							// In Node space, the Node's lower boundary has been moved
							// downward by nodeDy, away from its children.
							visual.incrementSizeOnServer(nodeDx, nodeDy, 0.0, nodeDy, false);

							// Increase offset by dy.
							setYOriginDynamicOffset(getYOriginDynamicOffset() + dy);
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
							return;
						}
						
						visual.refresh();
					}
					else
					{
						parent.repaint();
					}
					
					//shiftViewInDisplayArea(0, dy);
				}	
				else // Border not being moved: Visual is being moved as a whole.
				{
					if (parent instanceof ContainerVisualJ)
					{
						try  // move the Node.
						{
							moveNode(visual, transformViewDXToNode(dx),
								-transformViewDYToNode(dy), false);
						}
						catch (Exception ex)
						{
							ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
							return;
						}
					}
					else  // an outermost Visual: only adjust the View dynamic offset.
					{
						shiftViewInDisplayArea(dx, dy);
						visual.refresh();
					}
				}
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
					
				visual.refresh();
				return;
			}

			
			// Notify Scenario overlays that the Visual has moved, so that they
			// can update the locations of their Controls.
			
			ModelDomainViewPanel.this.notifyVisualMoved(visual);
			ModelDomainViewPanel.this.repaint();
		}
		

		public void mouseEntered(MouseEvent e) {}

		
		public void mouseExited(MouseEvent e) {}

		
		public void mouseClicked(MouseEvent e) {}

		
		public void mouseMoved(MouseEvent e) {}
		
		
		/** Handle scrolling. */
		
		public void mouseWheelMoved(MouseWheelEvent e)
		{
			boolean moveSideways = false;
			
			if (e.isControlDown() || e.isAltDown()) moveSideways = true;
			
			int noOfClicks = e.getWheelRotation();
			
			// For each click, move ten pixels.
			
			int delta = noOfClicks * PixelsPerMouseWheelClick;
			
			int deltaX = 0;
			int deltaY = 0;
			
			if (moveSideways) deltaX = -delta;
			else deltaY = -delta;
			
			shiftViewInDisplayArea(deltaX, deltaY);
			getDisplayArea().repaint();
		}
	}
	
	
	public void resizeNode(GraphicVisualComponent visual, double dw, double dh, double childShiftX,
		double childShiftY, boolean notify)
	throws
		Exception
	{
		visual.resizeNode(dw, dh, childShiftX, childShiftY, notify);
	}
		
		
	public void moveNode(GraphicVisualComponent visual, double dx, double dy, boolean notify)
	throws
		Exception
	{
		visual.moveNode(dx, dy, notify);
	}
	
	
	protected void shiftViewInDisplayArea(int toTheRight, int downward)
	{
		setXOriginDynamicOffset(getXOriginDynamicOffset() + toTheRight);
			
		setYOriginDynamicOffset(getYOriginDynamicOffset() + downward);
		
		
		// Move the outermost Visual(s) by delta amount.
		
		VisualComponent ov = getOutermostVisual();
		if (outermostVisual != null)
		{
			((Component)ov).setLocation(ov.getX() + toTheRight, ov.getY() + downward);
			
			// Update Attribute value controls in Scenario Visuals.
			if (ov instanceof GraphicVisualComponent)
				((GraphicVisualComponent)ov).getGraphicView().
					notifyVisualMoved(ov);
		}
	}
	
	
	protected void notifyComponentRepainted(Component comp) {}
	
	
	/*
	static class DragEntry
	{
		DragEntry(int id, Kind kind, long time, String compId)
		{ this.id = id; this.kind = kind; this.time = time; this.compId = compId; }
		
		enum Kind { start, finish };
		
		int id;
		Kind kind;
		long time;
		String compId;
	}
	
	static List<DragEntry> dragEntries = new Vector<DragEntry>();
	static int dragId = 0;
	static synchronized int logStartDragTime(String compId)
	{
		dragEntries.add(new DragEntry(dragId, DragEntry.Kind.start, System.nanoTime(), compId));
		int id = dragId;
		dragId++;
		return id;
	}
	
	static synchronized void logEndDragTime(int dragId, String compId)
	{
		dragEntries.add(new DragEntry(dragId, DragEntry.Kind.finish, System.nanoTime(), compId));
	}
	
	static void writeDragTimes()
	{
		for (DragEntry entry : dragEntries)
		{
			System.out.println(entry.id + "\t" + entry.kind + "\t" + entry.time
				+ "\t" + entry.compId);
		}
	}
	
	static void clearDragEntries()
	{
		dragEntries = new Vector<DragEntry>();
	}
	*/
	
	
	/** ************************************************************************
	 * Toggle whether or not to display the current Scenario. If false, only
	 * the Model Domain is displayed. If true, the current Scenario is overlayed
	 * on the Model Domain. It is intended that this method be called by a menu
	 * selection or other visual control.
	 */
	 
	protected abstract void setShowScenarios(boolean show)
	throws
		Exception;
	
	
	/**
	 * If the Event source is the deepest Conduit belonging (directly) to its
	 * Container, then return false. Otherwise, identify the deepest such Conduit
	 * and forward the Event to it, and return true. If the source is not
	 * a Conduit, simply return false;
	 */
	 
	protected boolean forwardToDeepestConduitOfThisContainer(MouseEvent e)
	{
		Component source = e.getComponent();
		if (! ((source instanceof ConduitVisualJ)
			|| (source instanceof VisualComponentDisplayArea))
				) return false;
		
		Component[] components = ((ConduitVisualJ)source).getParent().getComponents();
		for (int zOrder = components.length-1; zOrder >= 0; zOrder--) // deepest first
		{
			Component component = components[zOrder];
			
			if (component instanceof ConduitVisualJ)
			{
				if (component == source) return false;

				e.setSource(component);
				component.dispatchEvent(e);
				return true;
			}
		}
		
		throw new RuntimeException(
			"Conduit " + source.getName() + "(" + source.getClass().getName() 
				+ ", of " + source.getParent().getName() + ") not found in Container " 
					+ this.getName() + " (" + this.getClass().getName() + ")");
	}


	 
	/** ************************************************************************
	 * Handler for right-click on a Node.
	 */
	 
	class PopupListener extends MouseInputAdapter
	{
		//boolean conduitsChecked = false;
		
		
		public void mouseReleased(MouseEvent e)
		{
			if (! ((e.getButton() == MouseEvent.BUTTON2)
				|| (e.getButton() == MouseEvent.BUTTON3)
				|| ((e.getButton() == MouseEvent.BUTTON1) && e.isControlDown())
			)) return;
			
			Component source = e.getComponent();
			
			if (! (source instanceof VisualJComponent)) return;
			VisualJComponent visual = (VisualJComponent)source;
			
			if (visual instanceof ConduitVisual)
			{
				if (forwardToDeepestConduitOfThisContainer(e)) return;
				AWTTools.forwardEventToDeepestContainingComponent(e);
				return;
			}
			else if (visual instanceof ModelContainerVisual)
			//if (visual instanceof ConduitVisual)
			{
				//if (conduitsChecked) conduitsChecked = false;
				//else  // Determine if the user clicked near a Conduit segment.
				//{
				ConduitVisualJ closestConduit = findCloseConduit(
					(ModelContainerVisualJ)source, e.getPoint());
				
				if (closestConduit != null)
				{
					//conduitsChecked = true;
					
					//if (closestConduit != visual)
					//{
					//	e.setSource(closestConduit);
					//	closestConduit.dispatchEvent(e);  // Forward to conduit.
					//	return;
					//}
					
					JPopupMenu containerPopup = new JPopupMenu();
					containerPopup.setLocation(e.getX(), e.getY());
					addContextMenuItems(containerPopup, closestConduit, e.getX(), e.getY());
					containerPopup.show(visual, e.getX(), e.getY());
					return;
				}
				
				
				// No Conduit was selected. Forward to a non-Conduit. Fall through.
				
				//if (forwardToDeepestConduitOfThisContainer(e)) return;
			
				//AWTTools.forwardEventToDeepestContainingComponent(e);
				//return;
				//}
			}
			
			try  // Create a popup menu to contain the menu item.
			{
				JPopupMenu containerPopup = new JPopupMenu();
				
				addContextMenuItems(containerPopup, visual, e.getX(), e.getY());
							
				containerPopup.show(visual, e.getX(), e.getY());
			}
			catch (Exception ex)
			{
				visual.setInconsistent(true);
				
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex), "Error", ex); return;
			}
		}
	}
	
		
	/** ************************************************************************
	 * Retrieves the Container's child Nodes from the server and displays them
	 * in a new View.
	 */
	 
	class PopulateActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			VisualJComponent visual = (VisualJComponent)originator;
			
			
			// Create a popup View for the chosen Visual.
			
			try { ModelDomainViewPanel.this.createPopupModelDomainViewPanel(
				visual.getFullName(), (ModelElementSer)(visual.getNodeSer())); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"Error while creating View for Node " + visual.getFullName(),
					ex);
			}
			
			//notifyVisualComponentPopulated(visual, true);			
		}
	}
	
	
	/** ************************************************************************
	 * Removes the Container's child Visuals and unregisters them from the server.
	 *
	 
	class DepopulateActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Object source = e.getSource();
			if (! (source instanceof JMenuItem)) return;
			
			Component component = ((JMenuItem)source).getComponent();
			Component parent = component.getParent();
			Component originator = ((JPopupMenu)parent).getInvoker();
			
			if (! (originator instanceof ContainerVisualJ)) return;
			
			ContainerVisualJ containerVisual = (ContainerVisualJ)originator;
			
			try { containerVisual.depopulateChildren(PortVisualJ.class); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"While removing Visual " + containerVisual.getName(),
					ex);
			}
			
			notifyVisualComponentPopulated(containerVisual, false);
		}
	}*/


	
	
	/**
	 * Create a Conduit between each pair of Ports in the Set of selected Visuals.
	 */
	
	class AddConduitHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Component originator = AWTTools.getJPopupMenuOriginator(e);
			if (! (originator instanceof ModelContainerVisualJ))
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					"To create a Conduit. select Ports and right click on the Container", "Error", 
					JOptionPane.ERROR_MESSAGE); return;
			}
			
			ModelContainerVisualJ containerVisual = (ModelContainerVisualJ)originator;
			
			Set<VisualComponent> selectedVisuals = getSelected();
			
			if (selectedVisuals.size() < 2)
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
					"Must select at least two Ports", "Error", 
					JOptionPane.ERROR_MESSAGE); return;
			}
			
			VisualComponent[] visAr = 
				selectedVisuals.toArray(new VisualComponent[selectedVisuals.size()]);
			
			for (int i = 0; i < visAr.length; i++)
			{
				VisualComponent visual = visAr[i];
				
				if (! (visual instanceof PortVisualJ))
				{
					JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
						"May only interconnect Ports", "Error", 
						JOptionPane.ERROR_MESSAGE); return;
				}
				
				PortVisualJ portVisual = (PortVisualJ)visual;
				
				
				// Create a Conduit between the portVisual and each other Port
				// in the selection Set. If two Ports are already connected, do
				// not connect them.
				
				for (int j = i+1; j < visAr.length; j++)
				{
					VisualComponent ov = visAr[j];
					
					if (! (ov instanceof PortVisualJ))
					{
						JOptionPane.showMessageDialog(ModelDomainViewPanel.this,
							"May only interconnect Ports", "Error", 
							JOptionPane.ERROR_MESSAGE); return;
					}
					
					PortVisualJ otherPortVisual = (PortVisualJ)ov;
					
					if (otherPortVisual == portVisual) continue;
					
					
					// Send request to server to create the Conduit. The corresponding
					// Visual will be created when the server sends a NodeAddedNotice.
					
					(new Exception("Creating Conduit")).printStackTrace();
					ConduitSer conduitSer = null;
					try
					{
						try { conduitSer = getModelEngine().createConduit(false,
							containerVisual.getNodeSer().getNodeId(),
							portVisual.getNodeSer().getNodeId(),
							otherPortVisual.getNodeSer().getNodeId()); }
						catch (Warning w)
						{
							if (JOptionPane.showConfirmDialog(null, w.getMessage(),
								"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
							{
								conduitSer = getModelEngine().createConduit(true,
									containerVisual.getNodeSer().getNodeId(),
									portVisual.getNodeSer().getNodeId(),
									otherPortVisual.getNodeSer().getNodeId());
							}
						}
					}
					catch (Exception ex)
					{
						ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex);
					}
				}
			}
		}
	}
	
	
	/**
	 * Enable the user to set a value for an Attribute in all Scenarios.
	 */
	 
	class SetAttrValueHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			ScenarioSer[] scenarioSers = null;
			try { scenarioSers = getModelEngine().getScenarios(
				ModelDomainViewPanel.this.getNodeSer().getName()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex); return;
			}
			

			// Identify the selected Attribute.
			
			Component eventSource = AWTTools.getJPopupMenuOriginator(e);
			if (! (eventSource instanceof AttributeVisualJ)) throw new RuntimeException(
				"Event source expected to be an AttributeVisualJ but it is not");
			
			AttributeVisualJ attrVisual = (AttributeVisualJ)eventSource;
			

			// Determine if a value is set for the Attribute in any of the
			// Scenarios.
			
			boolean mightLoseData = false;
			Serializable[] values = new Serializable[scenarioSers.length];
			int i = 0;
			for (ScenarioSer scenSer : scenarioSers) try
			{
				Serializable value = getModelEngine().getAttributeValueById(
					attrVisual.getNodeSer().getNodeId(), scenSer.getNodeId());
				if (value != null) mightLoseData = true;
				values[i++] = value;
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(ModelDomainViewPanel.this, ex); return;
			}
			
			if (mightLoseData)
			{
				// Inform the user via a modal dialog
				if (JOptionPane.showConfirmDialog(null, 
					"A value is set for this Attribute in some Scenarios: \n" +
						"are you sure you want to set it for all Scenarios?",
						"Are you sure?", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
					return;
			}
			
			
			// Display a non-modal value entry panel that allows the user to either
			// enter a String value (that will be parsed if possible), or choose 
			// from a list of tag types.
			
			AttrValueEntryPanel panel = new AttrValueEntryPanel(attrVisual, 
				ModelDomainViewPanel.this);
				
			int x = 0;
			int y = 0;
			Object source = e.getSource();
			if (source instanceof Component)
			{
				Component comp = (Component)source;
				x = comp.getX();
				y = comp.getY();
			}
				
			panel.setLocation(x, y);
			
			panel.show();
		}
	}
	
	
	/**
	 * Allow the user to choose an Attribute value from a list of the types defined
	 * in Types, and then create an Attribute with that value.
	 */
	 
	class TagModelElementHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			ScenarioSer[] scenarioSers = null;
			try { scenarioSers = getModelEngine().getScenarios(
				ModelDomainViewPanel.this.getNodeSer().getName()); }
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelDomainViewPanel.this, ex); return;
			}
			

			// Identify the selected Visual.
			
			Component eventSource = AWTTools.getJPopupMenuOriginator(e);
			if (! (eventSource instanceof VisualJComponent)) throw new RuntimeException(
				"Event source expected to be a Visual but it is not");
			
			VisualJComponent visual = (VisualJComponent)eventSource;
			
			
			// Display a non-modal value entry panel that allows the user to either
			// enter a String value (that will be parsed if possible), or choose 
			// from a list of tag types.
			
			AttrValueEntryPanel panel = new AttrValueEntryPanel(
				visual, 
				ModelDomainViewPanel.this,
				null // all Scenarios
				);
				
			int x = 0;
			int y = 0;
			Object source = e.getSource();
			if (source instanceof Component)
			{
				Component comp = (Component)source;
				x = comp.getX();
				y = comp.getY();
			}
				
			panel.setLocation(x, y);
			
			panel.show();
			

		}
	}
}

