/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import geometry.Point;
import geometry.PointImpl;
import awt.AWTTools;
import expressway.common.*;
import expressway.common.VisualComponent.*;
import expressway.common.NodeIconImageNames;
import expressway.common.ClientModel.*;
import expressway.common.VisualComponent.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.gui.*;
import expressway.help.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.Set;
import java.util.List;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Date;
import java.util.Vector;
import java.io.IOException;
import java.net.URL;


class ModelDomainVisualJ extends ModelContainerVisualJ implements ModelDomainVisual
{
	int getNormalBorderThickness() { return 4; }
	
	
	ModelDomainVisualJ(ModelDomainSer nodeSer, GraphicView view)
	throws
		Exception
	{
		super(nodeSer, view);
		setBackground(DomainColor);
	}
	
	
	public String getDescriptionPageName() { return "Model Domains"; }


	public ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		((ModelDomainSer)(this.getNodeSer())).addScenario(scenarioId);
		
		Set<VisualComponent> scenarioVisuals = null;
		try { scenarioVisuals = getClientView().identifyVisualComponents(scenarioId); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		ModelScenarioVisual scenarioVisual = null;
		for (VisualComponent vis : scenarioVisuals)
		{
			scenarioVisual = (ModelScenarioVisual)vis;
			break;
		}
		
		if (scenarioVisual == null) throw new RuntimeException(
			"Could not display Model Scenario with ID " + scenarioId);
		
		return scenarioVisual;
	}
	
	
	public void showScenarioCreated(String scenarioId)
	{
		// Add the new Scenario to the Domain's list of Scenarios.
		
		((ModelDomainSer)(this.getNodeSer())).addScenario(scenarioId);
	}
	
	
	public void showScenarioDeleted(String scenarioId)
	{
		((ModelDomainSer)(this.getNodeSer())).removeScenario(scenarioId);
	}
	
	
	public ScenarioVisual getScenarioVisual(String scenarioId)
	{
		return null; // ScenarioPanels are associated with the DomainView, not
			// with the DomainVisualJ.
	}
		

	public List<ModelScenarioVisual> getModelScenarioVisuals()
	{
		return new Vector<ModelScenarioVisual>();
	}


	public void simulationRunsDeleted()
	{
	}


	public void showScenarioSetCreated(String newScenSetId)
	{
	}
	
	
	public void loadAllScenarios()
	{
	}


	public void motifAdded(String motifId)
	{
	}
	
	
	public void motifRemoved(String motifId)
	{
	}


	public void attributeStateBindingChanged(String attrId, String stateId)
	{
		Set<VisualComponent> visuals = null;
		try
		{
			visuals = identifyVisualComponents(attrId);
			for (VisualComponent visual : visuals) visual.refresh();
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		try
		{
			visuals = identifyVisualComponents(stateId);
			for (VisualComponent visual : visuals) visual.refresh();
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}


	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}
	
	
	public Color getNormalBackgroundColor() { return DomainColor; }
	
	
	public Color getHighlightedBackgroundColor() { return DomainHLColor; }


	void addContextMenuItems(final JPopupMenu containerPopup, final int x, final int y)
	{
		super.addContextMenuItems(containerPopup, x, y);
		
		final HelpfulMenuItem menuItem1 = new HelpfulMenuItem("Motifs \u2192", false,
			"List the motifs that are available from the server, and those that " +
			"are available to this Model Domain. " +
			"See " + HelpWindow.createHref("Motifs") + ".");
		
		menuItem1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e1)
			{
				Component originator = AWTTools.getJPopupMenuOriginator(e1);
				VisualJComponent visual = (VisualJComponent)originator;
				
				List<ModelDomainMotifDefSer> motifDefSers = null;
				try { motifDefSers = getModelEngine().getModelDomainMotifs(); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(ModelDomainVisualJ.this, ex);
				}
				
				List<MotifDefSer> motifsUsedSers = null;
				try { motifsUsedSers =
					getModelEngine().getMotifsUsedByDomain(getNodeId()); }
				catch (Exception ex)
				{
					ErrorDialog.showReportableDialog(ModelDomainVisualJ.this, ex);
				}
				
				
				// Create a popup menu that contains a menu item for each of the
				// kinds of motif that is available..
				
				JPopupMenu popup = new JPopupMenu();
				
				popup.add(new HelpfulMenuItem("Domain uses these Motifs:", false,
					"To use a " + HelpWindow.createHref("Motifs", "motif") +
					" is to assert that the " +
					HelpWindow.createHref("Model Domains", "Model Domain") +
					" will be used. " +
					"Thus, usage expresses <i>intent</i>. The Model Domain might " +
					"not actually reference any elements of a motif that it 'uses'. " +
					"To 'use' a motif, select the Motif in the list presented " +
					"by this menu."));
				
				popup.addSeparator();
					
				int i = 0;
				for (final ModelDomainMotifDefSer motifDefSer : motifDefSers)
				{
					String title = motifDefSer.getName();
					
					for (MotifDefSer motifUsedSer : motifsUsedSers)
					{
						if (motifUsedSer.getNodeId().equals(motifDefSer.getNodeId()))
						{
							title += " \u2714";  // heavy check mark
							break;
						}
					}
					
					HelpfulMenuItem menuItem2 = 
						new HelpfulMenuItem(title, false, motifDefSer.htmlDescription);
						
					menuItem2.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e2)
						{
							// Toggle use of the Motif.
							try
							{
								String domainId = ModelDomainVisualJ.this.getNodeId();
								String motifId = motifDefSer.getNodeId();
								
								boolean usesMotif = 
									getModelEngine().domainUsesMotif(domainId, motifId);
								
								getModelEngine().useMotif(domainId, motifId, ! usesMotif);
							}
							catch (Exception ex)
							{
								ErrorDialog.showThrowableDialog(ModelDomainVisualJ.this, ex);
							}
						}
					});
	
					popup.add(menuItem2);
				}
				
				popup.show(originator, x, y);
			}
		});
		
		containerPopup.add(menuItem1);
		
		MenuItemHelper.addMenuOwnerItems(getGraphicView(), this,
			(MenuOwnerSer)(getNodeSer()), containerPopup, x, y);
	}
}

