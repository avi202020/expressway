/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import expressway.ser.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.InfoStrip;
import expressway.gui.ValueControl.*;
import expressway.gui.VisibilityControl.*;
import expressway.gui.*;
import expressway.gui.graph.*;
import expressway.help.*;
import statistics.HistogramDomainParameters;
import statistics.TimeSeriesParameters;
import statistics.FitGammaDistribution;
import graph.*;
import awt.AWTTools;
import generalpurpose.ThrowableUtil;
import geometry.PointImpl;
import generalpurpose.DateAndTimeUtils;
import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.io.IOException;
import java.io.Serializable;
import java.io.PrintWriter;
import java.net.URL;
import java.net.MalformedURLException;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JFrame;
import java.util.*;
import java.text.*;
import javax.swing.*;
import javax.swing.event.*;


/** ************************************************************************
 * A translucent panel for containing editable Attribute values for a
 * Model Scenario. This panel is overlaid on a ModelDomain View.
 */
 
public class ModelScenarioPanel extends ScenarioControlJPanel
	implements ModelScenarioVisual, GraphicNodeScenarioPanel
{
	private static final Font StateControlTextFont = new Font(null, Font.PLAIN, 10);
	private static final NumberFormat StateControlNumberFormat = NumberFormat.getInstance();
	private static final int StateControlWidth = 100;
	private static final int StateControlHeight = 20;
	
	static
	{
		StateControlNumberFormat.setMaximumFractionDigits(2);
	}

	private final StateValueControlFactory stateValueControlFactory = new StateValueControlFactory();

	private final StateVisControlFactory stateVisControlFactory = new StateVisControlFactory();
	
	/** Determines whether the NodeScenarioPanel displays Event values. */
	private boolean showEventsMode = false;
	
	/** Set to true when State controls are to be shown. */
	private boolean showStateControls = false;
	
	/** A JFrame showing the Event history of a particular SimulationRun of
		this ModelScenario. */
	private JFrame eventHistoryFrame = null;

	
	
	/** ********************************************************************
	 * Constructor for an existing Scenario.
	 */
	 
	ModelScenarioPanel(ModelScenarioViewPanel viewPanel, ModelScenarioSer scenarioSer)
	{
		super(viewPanel, scenarioSer, viewPanel.getScenarioSelector());
		
		this.setName(scenarioSer.getName());

		try { this.refreshRedundantState(); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(this, ex);
		}
	}
	
	
	ModelScenarioViewPanel getModelScenarioViewPanel()
	{
		return (ModelScenarioViewPanel)(getViewPanel());
	}
	
	
	void setEventHistoryFrame(JFrame frame) { this.eventHistoryFrame = frame; }
	
	
	void clearEventHistoryFrame()
	{
		if (this.eventHistoryFrame == null) return;
		this.eventHistoryFrame.dispose();
		this.eventHistoryFrame = null;
	}
	
	
	/** ********************************************************************
	 * For creating a "blank" Scenario to allow the user to define
	 * it and then save it, at which time it will obtain a Node Id.
	 *
	 
	ModelScenarioPanel copyScenarioPanel()
	{
		//
		//	Clone the ModelScenarioSer that is embedded in the Panel. The
		//	clone should be:
		//		1. A deep (value-based) copy.
		//		2. Anonymous: the name and Node Id of the Scenario should
		//			be nullified.
		//		3. Any fields that must be set by the server (such as
		//			modification date) should be nullified.
		//
		
		ModelScenarioSer newScenarioSer = 
			(ModelScenarioSer)(this.scenarioSer.deepCopy());
		
		newScenarioSer.anonymize();
			
		return new ModelScenarioPanel(getModelScenarioViewPanel(), newScenarioSer);
	}*/
	
	
	public ModelScenarioPanel postConstructionSetup()
	{
		// Adjust locations and sizes of this Scenario and its static Components.
		
		//this.layoutControlPanel();
		
		
		// Add VisibilityControls for each outermost Visuals AND immediate
		// children of the outermost Visuals.
		
		setShowEventsMode(false);
		
		getViewPanel().repaint();
		// did not cause a repaint.
		
		return this;
	}
	
	
	public NodeView getNodeView() { return (NodeView)(getViewPanel()); }
	
	
  /* ***********************************************************************/
  /* From ControlPanel */


	public void showControls(Class controlType, boolean show)
	{
		// Show (or hide) all of the VisibilityControls, Visuals and ValueControls
		// owned by this ControlPanel pertaining to the specified Control type,
		// and calls the <VisibilityControl>.showValueControls() method, which
		// shows the ValueControls if the VisibilityControl's state is set to
		// show its controls. If a Visual must be added to the ControlPanel,
		// showValueControls calls addNode(String) on the Control Panel.
		// Hides Controls that are not of the specified type.
		
		removeAllControls();
		
		if (StateControl.class.isAssignableFrom(controlType))
		{
			this.showStateControls = show;
			if (! show) return;
			
			showControls(stateValueControlFactory, StateVisual.class,
				stateVisControlFactory);
		}
		else
			super.showControls(controlType, show);
	}
	
	
	public ValueControlFactory getValueControlFactory(Class controlType)
	{
		ValueControlFactory factory = super.getValueControlFactory(controlType);
		if (factory != null) return factory;
		
		if (StateControl.class.isAssignableFrom(controlType)) return stateValueControlFactory;
		return null;
	}


	public VisibilityControlFactory getVisibilityControlFactory(Class controlType)
	{
		VisibilityControlFactory factory = super.getVisibilityControlFactory(controlType);
		if (factory != null) return factory;

		if (StateControl.class.isAssignableFrom(controlType)) return stateVisControlFactory;
		return null;
	}
	
	
	public VisualComponent addNode(NodeSer nodeSer, VisibilityControl visControl)
	throws
		Exception
	{
		VisualComponent visual = super.addNode(nodeSer, visControl);
		
		if (nodeSer instanceof StateSer)
		{
			ValueControl vc = createStateControl(visControl, visual);
			if (visControl != null) visControl.addValueControl(vc);
		}
		
		return visual;
	}
	
	
	public void removeVisual(VisualComponent visual)
	{
		if (visual == null) return;
		
		// Remove the ValueControls that are associated with the Visual.
		// It is assumed that the Visual is instantiated in this ControlPanel;
		// if not, no action.
		
		Set<ValueControl> valcs = getValueControls(visual);
		ValueControl[] valcAr = valcs.toArray(new ValueControl[valcs.size()]);
		for (ValueControl valc : valcAr) uncreateValueControl(valc);
		
		this.remove((Component)visual);
	}


  /* ***********************************************************************
   * Internal implementation.
   */
	

	/** If 'changed' is true, highlight Visuals for the specified State in
		such a manner as to indicate that the State has changed. This is
		currently used by the NodeScenarioPanel when replaying a simulation. */
	
	void showStateChanged(String stateVisualId, boolean changed)
	{
		StateControl stateControl = getStateControl(stateVisualId);
		if (stateControl == null) return;  // no Visual for the State

		stateControl.highlight(changed);
	}
	
	
	void setStateValue(String stateVisualId, Serializable value)
	{
		StateControl control = getStateControl(stateVisualId);
		if (control == null) return;  // no Visual that depicts the State
		
		control.setValue(value);
	}
	
	
	/** ********************************************************************
	 * Create a new StateControl in this NodeScenarioPanel to depict the
	 * value of the specified StateVisual for some Simulation Run.
	 * 'visControl' may be null.
	 */
	 
	protected StateControl createStateControl(
		VisibilityControl visControl /* may be null */, VisualComponent stateVis)
	{
		return (StateControl)
			(createValueControl(stateValueControlFactory, visControl, stateVis));
	}


	protected void deleteStateControl(StateControl sc)
	{
		uncreateValueControl(sc);
	}
	
	
	protected StateControl getStateControl(StateVisual visual)
	{
		return (StateControl)(getValueControl(visual, StateControl.class));
	}
	
	
	protected StateControl getStateControl(String nodeId)
	{
		return (StateControl)(getValueControl(nodeId, StateControl.class));
	}
	
	
	/** ********************************************************************
	 * Create a new StateVisibilityControl for the specified Visual.
	 */
	 
	protected StateVisibilityControl createStateVisibilityControl(PortedContainerVisualJ vj,
		boolean showInitially)
	{
		StateVisibilityControl svc = (StateVisibilityControl)
			(stateVisControlFactory.makeVisibilityControl(vj, showInitially));
		svc.setLocation();
		add(svc);
		return svc;
	}
	
	
	protected void deleteStateVisibilityControl(StateVisibilityControl vc)
	{
		stateVisControlFactory.unmakeVisibilityControl(vc);
	}
	
	
	protected StateVisibilityControl getStateVisibilityControl(VisualComponent visual)
	{
		return (StateVisibilityControl)(getVisibilityControl(visual, StateVisibilityControl.class));
	}
	
	
	protected StateVisibilityControl getStateVisibilityControl(String visualId)
	{
		return (StateVisibilityControl)(getVisibilityControl(visualId, StateVisibilityControl.class));
	}
	
	
	/** ********************************************************************
	 * Manages the visibility of a set of StateControls. Other types of
	 * ValueControls are not affected.
	 */
	 
	class StateVisibilityControl extends VisibilityControl
	{
		StateVisibilityControl(VisibilityControlFactory factory,
			PortedContainerVisualJ visual, boolean showInitially)
		{
			super(factory, getModelScenarioViewPanel(), ModelScenarioPanel.this,
				visual, stateValueControlFactory, showInitially);
				
			setToolTipText("Click to show or hide the simulated State values for this object");
		}
		

		public void createValueControls()
		{
			PortedContainerSer meSer = (PortedContainerSer)(getVisual().getNodeSer());
			for (String stateId : meSer.stateNodeIds)
			{
				createValueControl(stateId);
			}
		}
	}
		
		
	class StateValueControlFactory implements ValueControlFactory
	{
		public boolean shouldHaveValueControl(NodeSer nodeSer)
		{
			try { return StateVisualJ.class.isAssignableFrom(
				getClientView().getVisualFactory().getVisualClass(nodeSer)); }
			catch (CannotBeInstantiated ex) { return false; }
		}
		
		public Class getValueControlClass() { return StateControl.class; }
		
		public ValueControl makeValueControl(VisualComponent vis, 
			VisibilityControl visControl)
		{
			StateVisualJ stateVis = (StateVisualJ)vis;
			StateSer attrSer = (StateSer)(vis.getNodeSer());
			return new StateControl(this, visControl, stateVis);
		}

		public ValueControl makeValueControl(NodeSer nodeSer, VisibilityControl visControl)
		{
			VisualComponent stateVis;
			try { stateVis = identifyVisualComponent(nodeSer.getNodeId()); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
			if (stateVis == null) throw new RuntimeException("Could not find Visual");
			return new StateControl(this, visControl, (StateVisualJ)stateVis);
		}
		
		public void unmakeValueControl(ValueControl vc)
		{
		}

		
		public ViewPanelBase getViewPanel() { return ModelScenarioPanel.this.getViewPanel(); }
		
		public ControlPanel getControlPanel() { return ModelScenarioPanel.this; }
	}
		
		
	class StateVisControlFactory implements VisibilityControlFactory
	{
		public Class getVisibilityControlClass() { return StateVisibilityControl.class; }
		
		public VisibilityControl makeVisibilityControl(VisualComponent visual,
			boolean showInitially)
		{
			return new StateVisibilityControl(this, (PortedContainerVisualJ)visual, showInitially);
		}
		
		public void unmakeVisibilityControl(VisibilityControl vc)
		{
			ValueControl[] valcs = vc.getValueControls();
			for (ValueControl valc : valcs) deleteStateControl((StateControl)valc);
			remove(vc);  // Remove the VisibilityControl from the ControlPanel.
		}
		
		public ValueControlFactory getValueControlFactory() { return stateValueControlFactory; }
		
		public ViewPanelBase getViewPanel() { return ModelScenarioPanel.this.getViewPanel(); }
		
		public ControlPanel getControlPanel() { return ModelScenarioPanel.this; }
	}
	
	
	/** ********************************************************************
	 * For indication of a State's value when replaying Events.
	 */
	 
	class StateControl extends ValueControl
	{
		Color NormalBackground = Color.gray;
		Color HighlightBackgroundColor = Color.yellow;
		Color ConflictBackgroundColor = Color.red;
		
		private boolean highlight = false;
		private boolean conflict = false;

		
		StateControl(ValueControlFactory factory, VisibilityControl visControl,
			StateVisualJ stateVis)
		{
			super(factory, visControl, stateVis, null, false);
			this.setSize(StateControlWidth, StateControlHeight);
			
			setValueFont(StateControlTextFont);
			setValueNumberFormat(StateControlNumberFormat);

			highlight(false);
		}
		
		
		public void updateServer() { throw new RuntimeException("Server update attempted"); }
		
		
		StateVisualJ getStateVisual() { return (StateVisualJ)(getVisual()); }
		
		
		void setConflict(boolean conflict)
		{
			if (conflict) getValueComponent().setBackground(ConflictBackgroundColor);
			else highlight(this.highlight);
		}
	}
	
	
	
  /* ***********************************************************************
   * Accessors.
   */
	
	public boolean stateControlsAreVisible() { return showStateControls; }
	

	
  /* ***********************************************************************
   * Methods defined in ScenarioVisual.
   */

	public void addSimulationVisual(Integer id, SimulationVisual simPanel)
	{
		getModelScenarioViewPanel().addSimVisual(id, simPanel);
	}
	
	
	public Set<SimulationVisual> simulationRunCreated(Integer requestId,
		String simRunNodeId)
	{
		return getModelScenarioViewPanel().getSimulationVisuals(requestId);
	}
	
	
	public void simulationRunsDeleted()
	{
	}
	
	
	public void setShowEventsMode(boolean showEventsMode)
	{
		this.showEventsMode = showEventsMode;
		
		if (showEventsMode)
			showControls(StateControl.class, true);
		else
			showControls(AttributeValueControl.class, true);
	}
	
	
	public boolean getShowEventsMode() { return this.showEventsMode; }
	

	public void showEventsForIteration(String simRunId, int iteration)
	{
		if (! (this.showEventsMode)) return;
		
		
		// Get State values and Events that are needed.
		
		Serializable[] priorValues = null;
		java.util.List<String> stateIds = null;
		java.util.List<GeneratedEventSer> eventSers = null;
		try
		{
			if (iteration > 0)
			{
				// Get prior values.
				Object[] result = getModelEngine().getStateValuesForIteration(
					simRunId, iteration);
				
				priorValues = (Serializable[])(result[0]);
				stateIds = (java.util.List<String>)(result[1]);
				
				// Get Events for specified iteration.
				eventSers = getModelEngine().getEventsForSimNodeForIteration(
					simRunId, iteration);
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog(ModelScenarioPanel.this, ex); return;
		}
		
		
		// Clear highighting on all StateControls.
		
		Set<ValueControl> controls = getValueControls(StateControl.class);
		for (ValueControl control : controls) control.highlight(false);
		
		
		// Set State prior value in each Control.
		
		if (priorValues != null)  // Set prior values.
		{
			int stateSeqNo = 0;
			for (String stateId : stateIds)
				setStateValue(stateId, priorValues[stateSeqNo++]);
		}
		
		
		// Set any values that change in the iteration, and highlight those
		// StateControls.
		
		if (eventSers != null) for (GeneratedEventSer eventSer : eventSers)
		{
			String stateId = eventSer.stateNodeId;
			Serializable newValue = eventSer.newValue;
			
			int valuesIndex = 0;
			for (String id : stateIds)
				if (id.equals(stateId)) break;
				else valuesIndex++;
			
			if (valuesIndex >= stateIds.size()) throw new RuntimeException(
				"Unable to identify State in the State Ids returned by " +
				"getStateValuesForIteration");
			
			Serializable priorValue = priorValues[valuesIndex];
			
			if (
				(priorValue == null) && (newValue != null)
				||
				((priorValue != null) && (! priorValue.equals(newValue)))
				)
			{
				showStateChanged(eventSer.stateNodeId, true);
				setStateValue(eventSer.stateNodeId, newValue);
			}
		}	
	}
			
	
	public void refreshStatistics()
	{
	}
	
	
	public void simTimeParamsChanged(Date newFromDate, Date newToDate,
		long newDuration, int newIterLimit)
	{
		this.getModelScenarioSer().setStartingDate(newFromDate);
		this.getModelScenarioSer().setFinalDate(newToDate);
		this.getModelScenarioSer().setDuration(newDuration);
		this.getModelScenarioSer().setIterationLimit(newIterLimit);
	}
	
	
	public void addOrRemoveStateControls(boolean add, VisualComponent visual)
	throws
		Exception
	{
		//addOrRemoveControls(add, visual, stateValueControlFactory, StateVisual.class);
		addOrRemoveControls(add, visual, stateVisControlFactory);
	}
	
	
	public boolean controlsForStateAreVisible(PortedContainerVisual visual)
	{
		return controlsForVisualAreVisible(visual, StateControl.class);
	}
	
	
	public JFrame createEventHistoryWindow(String simRunId, Class... stateTags)
	throws
		Exception
	{
		return EventHistoryPanel.createEventHistoryWindow(getClientView(), 
			simRunId, stateTags);
	}
	
	
  /* Protected methods */
	
	
	protected ModelScenarioSer getModelScenarioSer() { return (ModelScenarioSer)(getNodeSer()); }
}

