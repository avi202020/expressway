/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import expressway.ser.*;
import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.gui.*;
import expressway.gui.graph.*;
import expressway.gui.InfoStrip.*;
import expressway.common.ModelAPITypes.*;
import expressway.help.*;
import statistics.HistogramDomainParameters;
import statistics.TimeSeriesParameters;
import statistics.FitGammaDistribution;
import graph.*;
import awt.AWTTools;
import generalpurpose.ThrowableUtil;
import generalpurpose.DateAndTimeUtils;
import generalpurpose.StringUtils;
import geometry.PointImpl;
import java.awt.event.*;
import java.awt.Component;
import java.awt.Container;
import java.awt.geom.AffineTransform;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.io.IOException;
import java.io.Serializable;
import java.io.PrintWriter;
import java.net.URL;
import java.net.MalformedURLException;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JFrame;
import java.util.*;
import java.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.PopupMenu;
import java.awt.LayoutManager;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Window;



/** ****************************************************************************
 * Implements a Graphic View of a Model Scenario, in which Scenario-specific
 * Attribute values may be displayed and can be edited.
 *
 * Displays a tabbed Panel listing the current Scenarios for the Mode Domain,
 * and allows the user to select which one to display. The user can choose to
 * hide or show the tabbed list of Scenarios. If the Scenarios are hidden 
 * the user can edit the ModelDomain. If the Scenarios are shown, the user
 * can edit the Attribute values for the current Scenarion (selected via its
 * tab).
 */
 
public class ModelScenarioViewPanel extends ModelDomainViewPanel 
	implements
		NodeScenarioView,
		GraphicScenarioViewPanel,
		VisualComponent.SimulationVisualMonitor //, EventsView
{
  /* ***************************************************************************/
  /* Static values. */
	
	public static final String ViewTypeName = "ModelScenarioView";
	
	
  /* ***************************************************************************/
  /* Instance variables. */

	
	/** Implements the standard functionality of a ScenarioViewPanel. This class
		delegates most of its functions to the Helper class. */
	//private ScenarioViewPanelHelper helper; Don't need this: uses the superclass helper.
	
	/** Mouse action handler for performing the ViewDistribution function. */
	private ViewDistributionHandler viewDistributionHandler;
	
	/** Mouse action handler for performing the ViewExpectedValueOverTime function. */
	private ViewExpectedValueOverTimeHandler viewExpectedValueOverTimeHandler;
	
	/** Mouse handler for creating an Activity Risk Mitigation Panel. */
	private final ViewEvidenceActionListener viewEvidenceActionListener;
	
	/** Indicates whether this View Panel exists in its own popup View Window, or
		is part of the main Window. */
	private boolean isInPopupWindow = false;
		

  /* ***************************************************************************/
  /* Constructor. */

	
	public ModelScenarioViewPanel(PanelManager container, 
		NodeSer nodeSer, ViewFactory viewFactory, ModelEngineRMI modelEngine)
	throws
		Exception
	{
		super(container, nodeSer, viewFactory, modelEngine);
		if (! (nodeSer instanceof ModelElementSer)) throw new RuntimeException(
			"nodeSer is not a ModelElementSer");
		
		this.viewEvidenceActionListener = new ViewEvidenceActionListener();
		createScenarioSelector();
	}
	
	
  /* ***************************************************************************
   * Methods from NodeScenarioView.
   */
	
	
	/** ************************************************************************
	 * Toggle whether or not to display the current Scenario. If false, only
	 * the Model Domain is displayed. If true, the current Scenario is overlayed
	 * on the Model Domain. It is intended that this method be called by a menu
	 * selection or other visual control.
	 */
	 
	public void setShowScenarios(boolean show)
	{
		getHelper().setShowScenarios(show);
	}
	
	
	/** ************************************************************************
	 * Return the current state of the toggle set by the setShowScenarios method.
	 */
	 
	public boolean getShowScenarios()
	{
		return getHelper().getShowScenarios();
	}
	
	
	/** ************************************************************************
	 * Add the specified NodeScenarioPanel as an InfoStrip in the Scenario selection
	 * strip tabbed pane; also add the NodeScenarioPanel to this Model Scenario
	 * View Panel and make it not visible.
	 * Do not change the current visible tab unless the new tab is the only tab.
	 */
	 
	public void addScenarioVisual(final NodeScenarioPanel panel)
	{
		getHelper().addScenarioVisual(panel);
	}
	
	
	/** ************************************************************************
	 * Remove the specified Scenario as a tab in the Scenario selection strip. Do not
	 * change the current visible tab unless the tab is the current tab.
	 */
	 
	public void removeScenarioVisual(NodeScenarioPanel panel)
	{
		getHelper().removeScenarioVisual(panel);
	}
	
	
	public Set<NodeScenarioPanel> getScenarioPanels()
	{
		return getHelper().getScenarioPanels();
	}
	
	
	public void setCurrentScenarioPanel(String scenarioId)
	{
		getHelper().setCurrentScenarioPanel(scenarioId);
	}
	
	
	public void setCurrentScenarioPanel(ScenarioSer scenarioSer)
	{
		getHelper().setCurrentScenarioPanel(scenarioSer);
	}
	
	
	/** ************************************************************************
	 * Make the specified panel the currently selected Scenario Panel, and
	 * make it visible if this.showScenarios is true. If panel is null, then
	 * make no panel the current one.
	 */
	 
	public void setCurrentScenarioPanel(NodeScenarioPanel panel)
	{
		getHelper().setCurrentScenarioPanel(panel);
	}
	
	
	public NodeScenarioPanel getCurrentScenarioPanel()
	{
		return getHelper().getCurrentScenarioPanel();
	}
	
	
	/** ************************************************************************
	 * Return the Attribute Value Panel that depicts the specified Scenario, or
	 * null if none depicts it.
	 */
	 
	public NodeScenarioPanel getScenarioPanel(String scenarioNodeId)
	{
		return getHelper().getScenarioPanel(scenarioNodeId);
	}
	

	public Container getContainer() { return this; }
	
	
	/** ************************************************************************
	 * Verify that the Scenario name is legitimate, including from a security
	 * perspective. Intended for screening user input Scenario names.
	 */
	 
	public void validateScenarioName(String name)
	throws
		ParameterError
	{
		getHelper().validateScenarioName(name);
	}
	
	
	/** ************************************************************************
	 * Should be called immediately after the dimensions of this Panel are
	 * set or are changed, so that the dimensions of the Strip can be adjusted.
	 */
	 
	public void resizeScenarioSelectorImpl()
	{
		getHelper().resizeScenarioSelectorImpl();
	}
	
	
	public ScenarioInfoStrip createScenarioInfoStrip(NodeScenarioPanel panel)
	{
		if (! (panel instanceof GraphicNodeScenarioPanel)) throw new RuntimeException(
			"Scenario Panel is not a GraphicNodeScenarioPanel");
		return getScenarioSelector().createScenarioInfoStrip(panel);
	}
	
	
	public ScenarioInfoStrip constructScenarioInfoStrip(NodeScenarioPanel panel)
	{
		return new GraphicScenarioInfoStripImpl((GraphicNodeScenarioPanel)panel);
	}
	
	
  /* ***************************************************************************
   * Methods from ViewPanelBase.
   */
	
	
	protected View.ViewHelper createHelper()
	{
		return new ScenarioViewPanelHelper(this, new SuperNodeScenarioView());
	}
	
	
	protected ScenarioViewPanelHelper getHelper()
	{
		return (ScenarioViewPanelHelper)(getBaseHelper());
	}
	
	
	public synchronized void postConstructionSetup()
	throws
		Exception
	{
		getHelper().postConstructionSetup();
	}
	
	
	public ScenarioSelector getScenarioSelector()
	{
		return getHelper().getScenarioSelector();
	}
	
	
	public void resizeScenarioSelector()
	{
		getHelper().resizeScenarioSelectorImpl();
	}
	
	
	public synchronized String getViewType() { return ViewTypeName; }
	
	
	
  /* ***************************************************************************
   * Methods from NodeViewPanelBase.
   */
	
	
	public DomainInfoStrip createDomainInfoStrip()
	{
		return getScenarioSelector().createDomainInfoStrip();
	}
	
	
  /* ***************************************************************************
   * Methods from ModelDomainViewPanelBase.
   */
	
	
	public synchronized Set<VisualComponent> addNode(String nodeId)
	throws
		Exception
	{
		Set<VisualComponent> visuals = getHelper().addNode(nodeId);
		if (visuals == null) return visuals;
			
		// Update the ModelScenarioPanels.
		for (VisualComponent visual : visuals)
		{
			addOrRemoveStateControls(visual);
		}
				
		return visuals;
	}
	
	
	public synchronized void removeVisual(VisualComponent visual)
	throws
		Exception
	{
		getHelper().removeVisual(visual);
	}
	
	
	public synchronized void notifyScenarioNameChanged(ScenarioVisual scenVis,
		String newName)
	{
		getHelper().notifyScenarioNameChanged(scenVis, newName);
	}

	
	public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
	{
	}
		
		
	public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId, String crossRefId)
	{
	}
		
		
	public synchronized VisualComponent makeVisual(NodeSer nodeSer, Container container,
		boolean asIcon)
	throws
		ParameterError,
		Exception
	{
		return super.makeVisual(nodeSer, container, asIcon);
		// calls newVisual.setLocation().
		
		// The new Visual's setLocation() method should observe that the Container is
		// this ModelScenarioPanel, and position the new Visual based on the location
		// of the VisualJComponent that depicts the Node's parent.
	}
	

	public synchronized Set<VisualComponent> identifyVisualComponents(String nodeId)
	throws
		ParameterError
	{
		return super.identifyVisualComponents(nodeId);
		
		// The implementation in AbstractGraphicViewPanel will
		// automatically include the Scenario Panels and their Visuals.
	}
	
	
	public synchronized VisualComponent identifyVisualComponent(String nodeId)
	throws
		ParameterError
	{
		return getHelper().identifyVisualComponent(nodeId);
	}


	public synchronized Set<VisualComponent> identifyVisualComponents()
	throws
		ParameterError
	{
		Set<VisualComponent> visualComponents = getHelper().identifyVisualComponents();
		
		Set<NodeScenarioPanel> scenarioPanels = getScenarioPanels();
		for (NodeScenarioPanel panel : scenarioPanels)
		{
			Set<ValueControl> scs = 
				panel.getValueControls(ModelScenarioPanel.StateControl.class);
			for (ValueControl vc : scs)
			{
				ModelScenarioPanel.StateControl sc = (ModelScenarioPanel.StateControl)vc;
				StateVisual sv = sc.getStateVisual();
				visualComponents.add(sv);
			}
		}
		
		return visualComponents;
	}
	
	
	public synchronized void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
	}


	public ScenarioVisual getCurrentScenario()
	{
		return getHelper().getCurrentScenarioPanel();
	}
	
	
	ModelDomainViewPanelBase createPopupModelDomainViewPanel(String title, ModelElementSer meSer)
	throws
		Exception
	{
		return (ModelDomainViewPanelBase)(getViewFactory().createViewPanel(true, meSer.getNodeId()));
	}


	public synchronized ScenarioVisual showScenario(String scenarioId)
	throws
		Exception
	{
		return getHelper().showScenario(scenarioId);
	}
	
	
	public ScenarioVisual makeScenarioVisual(ScenarioSer scenarioSer)
	{
		if (! (scenarioSer instanceof ModelScenarioSer)) throw new RuntimeException(
			"scenarioSer is not a ModelScenarioSer");
		return new ModelScenarioPanel(this, (ModelScenarioSer)scenarioSer);
	}
	

	public ScenarioVisual getCurrentScenarioVisual()
	{
		return getHelper().getCurrentScenario();
	}
	
	
	public ControlPanel getCurrentControlPanel()
	{
		return getHelper().getScenarioSelector().getCurrentScenarioPanel();
	}
	
	
	public synchronized void showScenarioCreated(String scenarioId)
	{
		getHelper().showScenarioCreated(scenarioId);
	}
	
	
	public synchronized void showScenarioDeleted(String scenarioId)
	{
		getHelper().showScenarioDeleted(scenarioId);
	}
	
	
	public synchronized ScenarioVisual getScenarioVisual(String scenarioId)
	{
		return getHelper().getScenarioVisual(scenarioId);
	}

	
	public synchronized void notifyVisualMoved(VisualComponent visual)
	{
		super.notifyVisualMoved(visual);
		Set<NodeScenarioPanel> panels = getScenarioPanels();
		for (NodeScenarioPanel panel : panels)
		{
			panel.notifyVisualMoved(visual);
		}
	}
	
	
	public synchronized void notifyVisualRemoved(VisualComponent visual)
	{
		super.notifyVisualRemoved(visual);
		Set<NodeScenarioPanel> panels = getScenarioPanels();
		for (NodeScenarioPanel panel : panels)
		{
			panel.notifyVisualRemoved(visual);
		}
	}
	
	
	public synchronized void notifyThatDisplayAreaChanged(int x, int y, int width, int height)
	{
		getHelper().notifyThatDisplayAreaChanged(x, y, width, height);
	}
	
	
	public synchronized void update()
	{
		super.update();
	}
	
	

  /* ***************************************************************************
   * Methods from ScenarioView.
   */

	
	public void deleteControls(String nodeId)
	{
		getHelper().deleteControls(nodeId);
	}
	
	
	
  /* ***************************************************************************
   * Methods from MotifViewPanel.
   */
	
	
	public synchronized void computeDisplayAreaLocationAndSize(boolean repositionComponents)
	{
		super.computeDisplayAreaLocationAndSize(repositionComponents);
	}
	
	
	
  /* ***************************************************************************
   * Methods from java.awt.Component.
   */
	
	public synchronized void repaint()
	{
		if (getHelper() == null) super.repaint();
		else getHelper().repaint();
	}
	
	
	
  /* ***************************************************************************
   * Methods from VisualComponent.SimulationVisualMonitor.
   */
	
	public void simulationVisualClosing(SimulationVisual simVis)
	{
		removeSimVisual(simVis.getCallbackId());
	}
	
	

  /* ***************************************************************************
   * Methods from VisualComponent.
   */
	
	
	public synchronized void writeScenarioDataAsXML(PrintWriter writer, int indentation,
		ScenarioVisual scenarioVisual)
	throws
		IOException
	{
		writer.print(StringUtils.tabs[indentation]);
		writer.println("<ModelScenario full_name=\"" + scenarioVisual.getFullName() + "\">");
		writer.println("</ModelScenario>");
	}

	
  /* *******************************************************************
   * Methods defined in ModelDomainVisual.
   */
	 
	
	public void showScenarioSetCreated(String newScenSetId)
	{
		getHelper().showScenarioSetCreated(newScenSetId);
	}


	public void loadAllScenarios()
	{
		getHelper().loadAllScenarios();
	}


	public java.util.List<ModelScenarioVisual> getModelScenarioVisuals()
	{
		List<ScenarioVisual> svs = getHelper().getScenarioVisuals();
		List<ModelScenarioVisual> msvs = new Vector<ModelScenarioVisual>();
		for (ScenarioVisual sv : svs) msvs.add((ModelScenarioVisual)sv);
		return msvs;
	}
	
	

  /* ***************************************************************************
   * Methods from AbstractGraphicViewPanel.
   */
	
	
	protected Set<VisualComponent> refreshVisuals(NodeSer node)
	throws
		Exception
	{
		return getHelper().refreshVisuals(node);
	}
	
	
	
  /* ***************************************************************************
   * Protected methods.
   */
	
	
	protected void addOrRemoveAttributeControls(VisualComponent visual)
	{
		getHelper().addOrRemoveAttributeControls(visual);
	}
	
	
	protected void addOrRemoveStateControls(VisualComponent visual)
	{
		Set<NodeScenarioPanel> panels = this.getScenarioPanels();
		for (NodeScenarioPanel panel : panels)
		{
			ModelScenarioPanel msp = (ModelScenarioPanel)panel;
			try { msp.addOrRemoveStateControls(msp.stateControlsAreVisible(), visual); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	}
	
	protected void notifyComponentRepainted(Component comp)
	{
		getHelper().notifyComponentRepainted(comp);
	}
	
	
	/**
	 * If there is a current Scenario, return its Node Id. Otherwise return
	 * null.
	 */
	
	protected String getCurrentScenarioId()
	{
		return getHelper().getCurrentScenarioId();
	}
	
	
	void dumpAttrPanels(String msg)  // for debug only
	{
		getHelper().dumpAttrPanels(msg);
	}
	
	
	/** ************************************************************************
	 * Create and instantiate a Scenario Selection Strip within this Panel.
	 * This method is intended to be called only once, or when the contents of
	 * this Panel need to be completely refreshed. This method does not
	 * set the size of the Strip: for that, call resizeScenarioSelectorImpl.
	 *
	 * Should be called after Panel construction because this method makes calls
	 * to the server to obtain information about each Scenario.
	 */
	
	protected void createScenarioSelector()
	{
		getHelper().createScenarioSelector();
	}
	
	
	/** ************************************************************************
	 * Find and return the index of the tab that contains a Visual for the
	 * specified Scenario. There should not be more than one, but if there is, 
	 * simply return the first one. If not found, return -1.
	 */
	 
	protected int getScenarioTab(NodeScenarioPanel panel)
	{
		return getHelper().getScenarioSelector().getScenarioTab(panel);
	}
	
	
	protected void addContextMenuItems(JPopupMenu containerPopup, 
		final VisualJComponent visual, int x, int y)
	{
		super.addContextMenuItems(containerPopup, visual, x, y);
		
		// Check if the Scenarios are hidden or not.
		
		if (! getShowScenarios()) return;

		
		/*
			Add menu items that pertain to Scenario-specific function:
			<Scenario><State>.viewDistribution
			<Scenario><State>.viewExpectedValueOverTime
			<Scenario>.viewDistribution
			<Scenario>.viewExpectedValueOverTime
			<Scenario>.defineNewSimulation
		*/
		
		if ((visual instanceof StateVisual) || (visual instanceof ModelContainerVisualJ))
		{
			if (getCurrentScenarioId() != null)
			{
				JMenuItem menuItem1 = new HelpfulMenuItem("View Distribution", false,
					"Display a histogram of the final value of the selected " +
					HelpWindow.createHref("States", "States") + " for each " +
					HelpWindow.createHref("Simulation Runs", "Simulation Run") +
					".");
					
				menuItem1.addActionListener(getViewDistributionHandler(
					getCurrentScenarioId(), visual.getNodeId()));
				containerPopup.add(menuItem1);
				
				String scopeNodeId = visual.getNodeId();
				StateVisual state = null;
				if (visual instanceof StateVisual) state = (StateVisual)visual;
				
				JMenuItem menuItem2 = new HelpfulMenuItem("View Expected Value Over Time",
					false, "Display the trend of the value of the selected " +
					HelpWindow.createHref("States", "States") + " over time, " +
					"averaged over each " + 
					HelpWindow.createHref("Simulation Runs", "Simulation Run") +
					".");
					
					try { menuItem2.addActionListener(getViewExpectedValueOverTimeHandler(
						getCurrentScenarioId(), scopeNodeId, 
						(state == null ? null : state.getNodeId()))); }
					catch (VisualComponent.NoNodeException ex) { throw new RuntimeException(ex); }
				
				containerPopup.add(menuItem2);
			}
		}
		
		
		// Add menu item for creating a Risk Mitigation View.
		
		if (visual instanceof ActivityVisual)
		{
			if (! (visual instanceof ActivityVisualJ))
				throw new RuntimeException(
				"Incompatible implementation of an ActivityVisual: " +
				"must be an ActivityVisualJ");
			
			ActivityVisualJ activity = (ActivityVisualJ)visual;
			
			String scenarioId = getCurrentScenarioId();
			if (scenarioId != null) try
			{
				if (activity.hasAttributeType(expressway.common.Types.Activity.Transformation.class, 
					scenarioId))
				{
					// Create a popup menu item and add the action
					// "View Evidence" action handler to it.
					
					JMenuItem menuItem3 = new HelpfulMenuItem("View Evidence", false,
						"If the " + HelpWindow.createHref("Activities", "Activity") +
						" has a " + HelpWindow.createHref("Tag Values", "Transformation", "Transformation") +
						" Attribute, open a tabular view of the Activity in which" +
						" one can enter " + HelpWindow.createHref("Controls") +
						" or other kinds of supporting evidence.");
						
					menuItem3.addActionListener(
						ModelScenarioViewPanel.this.viewEvidenceActionListener);
			
					// Add the menu item to the popup menu created for
					// the Container.
					
					containerPopup.add(menuItem3);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		if (visual instanceof GeneratorVisualJ)
		{
			((GeneratorVisualJ)visual).addScenarioContextMenuItems(containerPopup,
				getCurrentScenarioId());
		}
		
		if (visual instanceof AttributeVisual)
		{
			final String modelScenarioId = getCurrentScenarioId();
			if (modelScenarioId != null)
			{
				JMenuItem menuItem4 = new HelpfulMenuItem("Create Delta Scenarios for Attribute",
					false,
					"Opens a window that allows the user to define two new " +
					"Scenarios, different from this one only in the value of the " +
					"selected Attribute. The window allows the user to choose the " +
					"value of the Attribute for each Scenario, as a percent change " +
					"from its value in this Scenario. This is useful for " +
					"<a href=\"http://www.investopedia.com/terms/s/sensitivityanalysis.asp\">" +
					"sensitivity analysis</a>.</html");
				
				menuItem4.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						// Display panel that allows user to choose the deltas to create
						// two new Scenarios.
						
						(new DeltaScenariosPanel(modelScenarioId, (AttributeVisual)visual)).show();
					}
				});
				
				containerPopup.add(menuItem4);
				
				JMenuItem menuItem5 = new HelpfulMenuItem("Create Scenarios to Vary Over Range...",
					false,
					"The user is prompted for a range over which to vary the selected Attribute. " +
					"A Scenario Set is then created containing one Scenario for values over the " +
					"range provided by the user. The user can then simulate the entire Scenario Set " +
					"from the Domain List View.");
				
				menuItem5.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						// Display a panel that allows the user to choose the range
						// over which the Attribute's value should be varied.
						
						(new DefineScenarioSetPanel(modelScenarioId, (AttributeVisual)visual)).show();
					}
				});
				
				containerPopup.add(menuItem5);
			}
		}
	}

	
	/*class CreateDeltaScenariosHandler implements ActionListener
	{
		private AttributeVisual attrVisual;
		private String modelScenarioId;
		
		
		public CreateDeltaScenariosHandler(AttributeVisual attrVisual,
			String modelScenarioId)
		{
			this.attrVisual = attrVisual;
			this.modelScenarioId = modelScenarioId;
		}
		
		
		public void actionPerformed(ActionEvent e)
		{
			// Display panel that allows user to choose the deltas to create
			// two new Scenarios.
			
			(new DeltaScenariosPanel(modelScenarioId, attrVisual)).show();
		}
	}*/
	

	protected ActionListener getViewDistributionHandler(String scenarioId, String stateId)
	{
		return new ViewDistributionHandler(this, scenarioId, scenarioId, stateId, this.getNodeId(),
			getPanelManager());
	}
	

	protected ActionListener getViewExpectedValueOverTimeHandler(String scenarioId,
		String scopeNodeId, String stateId)
	{
		return new ViewExpectedValueOverTimeHandler(this, scenarioId, scenarioId, stateId, 
			scopeNodeId, getPanelManager());
	}
	

	public void notifyVisualComponentPopulated(VisualComponent visual,
		boolean populated)
	{
		super.notifyVisualComponentPopulated(visual, populated);
	}
	
	
		
  /* ***************************************************************************
   * ***************************************************************************
   * Nested classes.
   */
		

	/** ************************************************************************
	 * Opens a new Activity Evidence View Panel. This handler is invoked after
	 * a GUI action has been interpreted to mean that the user wants an
	 * Activity Evidence View Panel to open for a user-selected Activity,
	 * and that the selected Activity is a Transformation Activity.
	 * This class is stateless and can be used in a re-entrant manner,
	 * since it exits before it exposes the state of the parent class to any
	 * other threads.
	 */
	 
	class ViewEvidenceActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Identify the Activity that the user selected.
			
			Object source = e.getSource();
			if (! (source instanceof JMenuItem)) return;
			
			Component component = ((JMenuItem)source).getComponent();
			Component parent = component.getParent();
			Component originator = ((JPopupMenu)parent).getInvoker();
			
			if (! (originator instanceof ActivityVisualJ)) return;
			
			
			ActivityVisualJ activityVisual = (ActivityVisualJ)originator;
			NodeSer nodeSer = activityVisual.getNodeSer();
			ActivitySer activitySer = (ActivitySer)nodeSer;
			
			
			// Create a new Activity Evidence View Panel for the specified Activity.
			
			ViewPanelBase panel = null;
			try
			{
				String scenarioId = getCurrentScenarioId();
				if (scenarioId == null)
				{
					JOptionPane.showMessageDialog(ModelScenarioViewPanel.this,
					"Can only view Transformation Activity evidence in the context of a Scenario",
					"No current Sceanario",
					JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				panel = (ViewPanelBase)(
					getViewFactory().createRiskMitigationView(activitySer, scenarioId, true));
					
				panel.setPreferredSize(new java.awt.Dimension(800, 400));
			}
			catch (Exception ex)
			{
				ErrorDialog.showReportableDialog(ModelScenarioViewPanel.this,
					ThrowableUtil.getAllMessages(ex),
					"While trying to create a View for Activity " + activitySer.getName(),
					ex);
				return;
			}
			
			
			// Add the new Panel to the Frame's PanelManager.
			
			getPanelManager().addPanel("Activity Evidence", panel, false);
		}
	}
	
	
  /* Proxy classes needed by ScenarioViewPanelHelper */
	
	
	private class SuperNodeScenarioView extends SuperNodeView
	{
	}
	
	
	private class SuperNodeView extends SuperView implements NodeView
	{
		public ScenarioVisual showScenario(String scenarioId) throws Exception
		{ return ModelScenarioViewPanel.super.showScenario(scenarioId); }
		
		public void showScenarioCreated(String scenarioId)
		{ ModelScenarioViewPanel.super.showScenarioCreated(scenarioId); }
		
		public void showScenarioDeleted(String scenarioId)
		{ ModelScenarioViewPanel.super.showScenarioDeleted(scenarioId); }
		
		public ScenarioVisual getScenarioVisual(String scenarioId)
		{ throw new RuntimeException("Should not be called"); }
		
		public void setOutermostNodeSer(NodeSer nodeSer) throws Exception
		{ ModelScenarioViewPanel.super.setOutermostNodeSer(nodeSer); }
		
		public NodeSer getOutermostNodeSer() { return ModelScenarioViewPanel.super.getOutermostNodeSer(); }
		
		public String getOutermostNodeId() { return ModelScenarioViewPanel.super.getOutermostNodeId(); }
		
		public VisualComponent getOutermostVisual() { return ModelScenarioViewPanel.super.getOutermostVisual(); }
		
		public void initializeNodeSer(NodeSer nodeSer) { ModelScenarioViewPanel.super.initializeNodeSer(nodeSer); }
	
		public void postConstructionSetup()
		throws
			Exception { ModelScenarioViewPanel.super.postConstructionSetup(); }
		
		public VisualComponent identifyVisualComponent(String nodeId)
		throws
			ParameterError { return ModelScenarioViewPanel.super.identifyVisualComponent(nodeId); }
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		throws
			Exception { return ModelScenarioViewPanel.super.getAvailableHTTPFormats(descriptions); }
		
		public String getWebURLString(String format)
		throws
			Exception { return ModelScenarioViewPanel.super.getWebURLString(format); }
		
		public String getExpresswayURLString()
		throws
			ParameterError { return ModelScenarioViewPanel.super.getExpresswayURLString(); }
		
		public String getViewType() { return ModelScenarioViewPanel.super.getViewType(); }
	
		public ScenarioVisual makeScenarioVisual(ScenarioSer scenSer) { throw new RuntimeException("Should not be called"); }
		
		public DomainInfoStrip createDomainInfoStrip() { return ModelScenarioViewPanel.super.createDomainInfoStrip(); }
		
		public DomainInfoStrip constructDomainInfoStrip() { return ModelScenarioViewPanel.super.constructDomainInfoStrip(); }
		
		public DomainInfoStrip getDomainInfoStrip() { return ModelScenarioViewPanel.super.getDomainInfoStrip(); }

		public void resizeDomainInfoStrip() { ModelScenarioViewPanel.super.resizeDomainInfoStrip(); }
		
		
		/* From VisualComponent */
		
		public String getNodeId() throws NoNodeException
		{ return ModelScenarioViewPanel.super.getNodeId(); }
		
		public String getName()
		{ return ModelScenarioViewPanel.super.getName(); }
		
		public String getFullName()
		{ return ModelScenarioViewPanel.super.getFullName(); }
	
		public ImageIcon getImageIcon()
		{ return ModelScenarioViewPanel.super.getImageIcon(); }
		
		public void setImageIcon(ImageIcon imgIcon)
		{ ModelScenarioViewPanel.super.setImageIcon(imgIcon); }
		
		public String getDescriptionPageName()
		{ return ModelScenarioViewPanel.super.getDescriptionPageName(); }
			
		public int getSeqNo()
		{ return ModelScenarioViewPanel.super.getSeqNo(); }
		
		public boolean contains(VisualComponent comp) throws NoNodeException
		{ return ModelScenarioViewPanel.super.contains(comp); }
		
		public boolean isOwnedByMotifDef()
		{ return ModelScenarioViewPanel.super.isOwnedByMotifDef(); }
		
		public boolean isOwnedByActualTemplateInstance()
		{ return ModelScenarioViewPanel.super.isOwnedByActualTemplateInstance(); }
		
		public String[] getChildIds()
		{ return ModelScenarioViewPanel.super.getChildIds(); }
		
		public void nameChangedByServer(String newName, String newFullName)
		{ ModelScenarioViewPanel.super.nameChangedByServer(newName, newFullName); }
		
		public void nodeDeleted()
		{ ModelScenarioViewPanel.super.nodeDeleted(); }
		
		public void notifyCrossReferenceCreated(String toNodeId, String crossRefId)
		{ ModelScenarioViewPanel.super.notifyCrossReferenceCreated(toNodeId, crossRefId); }
			
		public void notifyCrossReferenceDeleted(String toNodeId, String crossRefId)
		{ ModelScenarioViewPanel.super.notifyCrossReferenceDeleted(toNodeId, crossRefId); }
			
		public ClientView getClientView()
		{ return ModelScenarioViewPanel.super.getClientView(); }
		
		public ModelEngineRMI getModelEngine()
		{ return ModelScenarioViewPanel.super.getModelEngine(); }
		
		public NodeSer getNodeSer()
		{ return ModelScenarioViewPanel.super.getNodeSer(); }
		
		public void setNodeSer(NodeSer nodeSer) throws NoNodeException
		{ ModelScenarioViewPanel.super.setNodeSer(nodeSer); }
		
		public void update()
		{ ModelScenarioViewPanel.super.update(); }
		
		public void update(NodeSer nodeSer)
		{ ModelScenarioViewPanel.super.update(nodeSer); }
		
		public void redraw()
		{ ModelScenarioViewPanel.super.redraw(); }
		
		public void refresh()
		{ ModelScenarioViewPanel.super.refresh(); }
		
		public java.awt.Point getLocation()
		{ return ModelScenarioViewPanel.super.getLocation(); }
		
		public int getX()
		{ return ModelScenarioViewPanel.super.getX(); }
		
		public int getY()
		{ return ModelScenarioViewPanel.super.getY(); }
	
		public void refreshLocation()
		{ ModelScenarioViewPanel.super.refreshLocation(); }
	
		public void refreshSize()
		{ ModelScenarioViewPanel.super.refreshSize(); }
		
		public void setLocation() throws Exception
		{ ModelScenarioViewPanel.super.setLocation(); }
		
		public void setSize() throws Exception
		{ ModelScenarioViewPanel.super.setSize(); }
	
		public void refreshRedundantState() throws Exception
		{ ModelScenarioViewPanel.super.refreshRedundantState(); }
	
		public boolean getAsIcon()
		{ return ModelScenarioViewPanel.super.getAsIcon(); }
		
		public void setAsIcon(boolean yes)
		{ ModelScenarioViewPanel.super.setAsIcon(yes); }
		
		public boolean useBorderWhenIconified()
		{ return ModelScenarioViewPanel.super.useBorderWhenIconified(); }
		
		public void populateChildren() throws ParameterError, Exception
		{ ModelScenarioViewPanel.super.populateChildren(); }
			
		public Set<VisualComponent> getChildVisuals()
		{ return ModelScenarioViewPanel.super.getChildVisuals(); }
		
		public Object getParentObject()
		{ return ModelScenarioViewPanel.super.getParentObject(); }
		
		public Container getContainer()
		{ return ModelScenarioViewPanel.super.getContainer(); }
		
		public void highlight(boolean yes)
		{ ModelScenarioViewPanel.super.highlight(yes); }
		
		public boolean isHighlighted()
		{ return ModelScenarioViewPanel.super.isHighlighted(); }
		
		public Color getNormalBackgroundColor()
		{ return ModelScenarioViewPanel.super.getNormalBackgroundColor(); }
		
		public Color getHighlightedBackgroundColor()
		{ return ModelScenarioViewPanel.super.getHighlightedBackgroundColor(); }
		
		public int getWidth()
		{ return ModelScenarioViewPanel.super.getWidth(); }
		
		public int getHeight()
		{ return ModelScenarioViewPanel.super.getHeight(); }
		
		public void writeScenarioDataAsXMLRecursive(PrintWriter writer, int indentation,
			ScenarioVisual scenarioVisual) throws IOException
		{ ModelScenarioViewPanel.super.writeScenarioDataAsXMLRecursive(writer,
			indentation, scenarioVisual); }
		
		public void writeScenarioDataAsXML(PrintWriter writer, int indentation,
			ScenarioVisual scenarioVisual) throws IOException
		{ throw new RuntimeException("Should not be called"); }
	}
	
	
	private class SuperView extends SuperClientView implements View
	{
		public void postConstructionSetup() throws Exception { ModelScenarioViewPanel.super.postConstructionSetup(); }
		
		public VisualComponentDisplayArea getDisplayArea() { return ModelScenarioViewPanel.super.getDisplayArea(); }
		
		public JPanel getInfoPanel() { return ModelScenarioViewPanel.super.getInfoPanel(); }
		
		public void computeDisplayAreaLocationAndSize() { ModelScenarioViewPanel.super.computeDisplayAreaLocationAndSize(); }
		
		public ClientMainApplication getClientMainApplication() { return ModelScenarioViewPanel.super.getClientMainApplication(); }
		
		public void registerPeerListener() throws Exception { ModelScenarioViewPanel.super.registerPeerListener(); }
	
		public void unregisterPeerListener() throws Exception { ModelScenarioViewPanel.super.unregisterPeerListener(); }
	
		public boolean isPopup() { return ModelScenarioViewPanel.super.isPopup(); }
		
		public ControlPanel getCurrentControlPanel() { throw new RuntimeException("Should not be called"); }
		
		public ViewConstants getDisplayConstants() { return ModelScenarioViewPanel.super.getDisplayConstants(); }
	
		public ImageIcon getStoredImage(String handle)
		throws
			IOException { return ModelScenarioViewPanel.super.getStoredImage(handle); }
		
		public ViewFactory getViewFactory() { return ModelScenarioViewPanel.super.getViewFactory(); }
		
		public VisualComponent.VisualComponentFactory getVisualFactory() { return ModelScenarioViewPanel.super.getVisualFactory(); }
		
		public void saveFocusFieldChanges() { ModelScenarioViewPanel.super.saveFocusFieldChanges(); }
		
		public Class getVisualClass(NodeSer node)
		throws CannotBeInstantiated { return ModelScenarioViewPanel.super.getVisualClass(node); }
		
		public void select(VisualComponent visual, boolean yes) { ModelScenarioViewPanel.super.select(visual, yes); }
		
		public void selectNodesWithId(String nodeId, boolean yes)
		throws
			ParameterError { ModelScenarioViewPanel.super.selectNodesWithId(nodeId, yes); }
	
		public Set<VisualComponent> getSelected() { return ModelScenarioViewPanel.super.getSelected(); }
		
		public boolean isSelected(VisualComponent visual) { return ModelScenarioViewPanel.super.isSelected(visual); }
		
		public void selectAll(boolean yes) { ModelScenarioViewPanel.super.selectAll(yes); }
	
		public PeerListener getPeerListener() { return ModelScenarioViewPanel.super.getPeerListener(); }
		
		public ListenerRegistrar getListenerRegistrar() { return ModelScenarioViewPanel.super.getListenerRegistrar(); }
		
		public PanelManager getPanelManager() { return ModelScenarioViewPanel.super.getPanelManager(); }
		
		public Window getWindow() { return ModelScenarioViewPanel.super.getWindow(); }
		
		public void addTemporaryMouseListener(MouseListener listener) { ModelScenarioViewPanel.super.addTemporaryMouseListener(listener); }
		
		public void removeTemporaryMouseListener() { ModelScenarioViewPanel.super.removeTemporaryMouseListener(); }
		
		public void reportBug() { ModelScenarioViewPanel.super.reportBug(); }
	}
	
	
	private class SuperClientView extends SuperSwingComponent implements ClientView
	{
		public ModelEngineRMI getModelEngine()
		{ return ModelScenarioViewPanel.super.getModelEngine(); }
	
		public List<NodeSer> getOutermostNodeSers()
		{ return ModelScenarioViewPanel.super.getOutermostNodeSers(); }
		
		public List<String> getOutermostNodeIds()
		{ return ModelScenarioViewPanel.super.getOutermostNodeIds(); }
		
		public List<VisualComponent> getOutermostVisuals()
		{ return ModelScenarioViewPanel.super.getOutermostVisuals(); }
		
		public String getName() { return ModelScenarioViewPanel.super.getName(); }
	
		public ScenarioVisual showScenario(String scenarioId)
		throws
			Exception { return ModelScenarioViewPanel.super.showScenario(scenarioId); }
			
		public ScenarioVisual getCurrentScenarioVisual()
		{ throw new RuntimeException("Should not be called"); }
	
		public Set<VisualComponent> identifyVisualComponents(String nodeId)
		throws
			ParameterError { return ModelScenarioViewPanel.super.identifyVisualComponents(nodeId); }
	
		public Set<VisualComponent> identifyVisualComponents(Class c)
		throws
			ParameterError { return ModelScenarioViewPanel.super.identifyVisualComponents(c); }
		
		public Set<VisualComponent> identifyVisualComponents()	throws
			ParameterError { return ModelScenarioViewPanel.super.identifyVisualComponents(); }
		
		public Set<VisualComponent> refreshVisuals(String nodeId)
		throws
			Exception { return ModelScenarioViewPanel.super.refreshVisuals(nodeId); }
	
		public void refreshAttributeStateBinding(String domainId, String attrId, String stateId)
		throws
			Exception { ModelScenarioViewPanel.super.
				refreshAttributeStateBinding(domainId, attrId, stateId); }
		
		public Set<VisualComponent> addNode(String nodeId)
		throws
			Exception { return ModelScenarioViewPanel.super.addNode(nodeId); }

		public void removeVisual(VisualComponent visual)
		throws
			Exception { ModelScenarioViewPanel.super.removeVisual(visual); }
	
		public void notifyVisualComponentPopulated(VisualComponent visual, boolean populated)
		{ ModelScenarioViewPanel.super.notifyVisualComponentPopulated(visual, populated); }
		
		public void notifyScenarioNameChanged(ScenarioVisual scenVis, String newName)
		{ throw new RuntimeException("Should not be called"); }
		
		public void notifyCrossReferenceCreated(CrossReferenceSer crSer)
		{ throw new RuntimeException("Should not be called"); }
			
		public void notifyCrossReferenceDeleted(String fromNodeId, String toNodeId,
			String crossRefId)
		{ ModelScenarioViewPanel.super.notifyCrossReferenceDeleted(fromNodeId, toNodeId); }
	}
	
	
	private class SuperSwingComponent extends SuperAWTContainer implements SwingComponent
	{
		public JComponent getJComponent() { throw new RuntimeException("Should not be called"); }
	}
	
	
	private class SuperAWTContainer extends SuperAWTComponent implements AWTContainer
	{
		public Container getContainer() { throw new RuntimeException("Should not be called"); }
		
		public Component add(Component comp) { return ModelScenarioViewPanel.super.add(comp); }
		
		public Component add(Component comp, int index) { return ModelScenarioViewPanel.super.add(comp, index); }
		
		public Component add(String name, Component comp) { return ModelScenarioViewPanel.super.add(name, comp); }
		
		public Component getComponent(int n) { return ModelScenarioViewPanel.super.getComponent(n); }
		
		public int getComponentCount() { return ModelScenarioViewPanel.super.getComponentCount(); }
		
		public Component[] getComponents() { return ModelScenarioViewPanel.super.getComponents(); }
		
		public int getComponentZOrder(Component comp) { return ModelScenarioViewPanel.super.getComponentZOrder(comp); }
		
		public Insets getInsets() { return ModelScenarioViewPanel.super.getInsets(); }
		
		public boolean isAncestorOf(Component c) { return ModelScenarioViewPanel.super.isAncestorOf(c); }
		
		public void remove(Component comp) { ModelScenarioViewPanel.super.remove(comp); }
		
		public void remove(int index) { ModelScenarioViewPanel.super.remove(index); }
		
		public void removeAll() { ModelScenarioViewPanel.super.removeAll(); }
		
		public void setComponentZOrder(Component comp, int index) { ModelScenarioViewPanel.super.setComponentZOrder(comp, index); }
		
		public void setFont(Font f) { ModelScenarioViewPanel.super.setFont(f); }
		
		public void setLayout(LayoutManager mgr) { ModelScenarioViewPanel.super.setLayout(mgr); }
	}
	
	
	private class SuperAWTComponent implements AWTComponent
	{
		public Component getComponent() { throw new RuntimeException("Should not be called"); }
		
		public void add(PopupMenu popup) { ModelScenarioViewPanel.super.add(popup); }
		
		public void addComponentListener(ComponentListener l) { ModelScenarioViewPanel.super.addComponentListener(l); }
		
		public void removeComponentListener(ComponentListener l) { ModelScenarioViewPanel.super.removeComponentListener(l); }
		
		public void addMouseListener(MouseListener l) { ModelScenarioViewPanel.super.addMouseListener(l); }
		
		public boolean contains(Point p) { return ModelScenarioViewPanel.super.contains(p); }
		
		public Rectangle getBounds() { return ModelScenarioViewPanel.super.getBounds(); }
		
		public Component getComponentAt(int x, int y) { return ModelScenarioViewPanel.super.getComponentAt(x, y); }
		
		public Graphics getGraphics() { return ModelScenarioViewPanel.super.getGraphics(); }
		
		public String getName() { return ModelScenarioViewPanel.super.getName(); }
		
		public Container getParent() { return ModelScenarioViewPanel.super.getParent(); }
		
		public Dimension getSize() { return ModelScenarioViewPanel.super.getSize(); }
		
		public Toolkit getToolkit() { return ModelScenarioViewPanel.super.getToolkit(); }
		
		public int getWidth() { return ModelScenarioViewPanel.super.getWidth(); }
		
		public int getHeight() { return ModelScenarioViewPanel.super.getHeight(); }
		
		public int getX() { return ModelScenarioViewPanel.super.getX(); }
		
		public int getY() { return ModelScenarioViewPanel.super.getY(); }
		
		public Point getLocation() { return ModelScenarioViewPanel.super.getLocation(); }
		
		public Point getLocation(Point rv) { return ModelScenarioViewPanel.super.getLocation(rv); }
		
		public Point getLocationOnScreen() { return ModelScenarioViewPanel.super.getLocationOnScreen(); }
		
		public boolean isEnabled() { return ModelScenarioViewPanel.super.isEnabled(); }
		
		public boolean isOpaque() { return ModelScenarioViewPanel.super.isOpaque(); }
		
		public boolean isShowing() { return ModelScenarioViewPanel.super.isShowing(); }
		
		public boolean isVisible() { return ModelScenarioViewPanel.super.isVisible(); }
		
		public void repaint() { ModelScenarioViewPanel.super.repaint(); }
		
		public void setBounds(int x, int y, int width, int height) { ModelScenarioViewPanel.super.setBounds(x, y, width, height); }
		
		public void setBounds(Rectangle r) { ModelScenarioViewPanel.super.setBounds(r); }
		
		public void setForeground(Color c) { ModelScenarioViewPanel.super.setForeground(c); }
		
		public void setLocation(int x, int y) { ModelScenarioViewPanel.super.setLocation(x, y); }
		
		public void setLocation(Point p) { ModelScenarioViewPanel.super.setLocation(p); }
		
		public void setName(String name) { ModelScenarioViewPanel.super.setName(name); }
		
		public void setPreferredSize(Dimension preferredSize) { ModelScenarioViewPanel.super.setPreferredSize(preferredSize); }
		
		public void setSize(Dimension d) { ModelScenarioViewPanel.super.setSize(d); }
		
		public void setSize(int width, int height) { ModelScenarioViewPanel.super.setSize(width, height); }
		
		public void setVisible(boolean b) { ModelScenarioViewPanel.super.setVisible(b); }
		
		public void validate() { ModelScenarioViewPanel.super.validate(); }
	}
}

