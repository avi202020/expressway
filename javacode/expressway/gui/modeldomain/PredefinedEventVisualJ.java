/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;

import java.awt.Image;
import expressway.ser.*;
import expressway.gui.*;
import expressway.common.VisualComponent.*;
import expressway.common.NodeIconImageNames;
import expressway.common.ModelAPITypes.*;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class PredefinedEventVisualJ extends VisualJComponent implements PredefinedEventVisual
{
	private static final int SideLabelMargin = 3;
	private static final int TopBottomLabelMargin = 0;
	
	private static final Font LabelFont = new Font("Arial", Font.PLAIN, 9);
	
	
	private JLabel defaultTimeLabel;
	private JLabel valueLabel;
	private JLabel defaultValueLabel;
	
	
	PredefinedEventVisualJ(PredefinedEventSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view, true);
		setImageIcon(NodeIconImageNames.PredefinedEventIconImageName);
		setBackground(getNormalBackgroundColor());
		
		add(defaultTimeLabel = new JLabel());
		add(valueLabel = new JLabel("Value: "));
		add(defaultValueLabel = new JLabel());
		
		defaultTimeLabel.setFont(LabelFont);
		valueLabel.setFont(LabelFont);
		defaultValueLabel.setFont(LabelFont);
		
		
		//this.setToolTipText(this.getName());
	}
	
	
	public String getDescriptionPageName() { return "Predefined Events"; }


	protected void setDefaultLabels()
	{
		PredefinedEventSer nodeSer = (PredefinedEventSer)(getNodeSer());
		
		String days = 
			(nodeSer.timeExprString == null? "" : nodeSer.timeExprString);
		String timeStr = (days + " days");
		defaultTimeLabel.setText("Default time: " + timeStr);
		defaultTimeLabel.setToolTipText(timeStr);
		
		String valueStr = nodeSer.valueExprString == null? "" : nodeSer.valueExprString;
		defaultValueLabel.setText(valueStr);
		defaultValueLabel.setToolTipText(valueStr);
		valueLabel.setToolTipText(valueStr);
	}


	//int getNormalBorderThickness() { return 0; }
	

	public void refreshRedundantState()
	throws
		Exception
	{
		super.refreshRedundantState();
		
		Insets insets = this.getInsets();

		int labelWidth = this.getWidth() - insets.left - insets.right - SideLabelMargin * 2;
		
		valueLabel.setSize(30, 12);
		defaultValueLabel.setSize(80, 14);
		defaultTimeLabel.setSize(labelWidth, 12);
		
		valueLabel.setLocation(SideLabelMargin + insets.left, 
			TopBottomLabelMargin + insets.top
			);
		
		defaultValueLabel.setLocation(SideLabelMargin + insets.left + 30,
			TopBottomLabelMargin + insets.top
			);
		
		defaultTimeLabel.setLocation(SideLabelMargin + insets.left, 
			TopBottomLabelMargin + 14 + insets.top
			);
		
		setDefaultLabels();
	}
	
	
	public Color getNormalBackgroundColor() { return PredefinedEventColor; }
	
	
	public Color getHighlightedBackgroundColor() { return PredefinedEventHLColor; }


	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		if (((PredefinedEventSer)(this.getNodeSer())).pulse) drawEventSymbol(g);
	}
	

	protected void drawEventSymbol(Graphics g)
	{
		int len = EventSymbol.DefaultEventSymbolLength;
		int x = this.getWidth() - len;
		int y = this.getHeight() / 2;
		
		EventSymbol.drawEventSymbol(g, x, y, len, Color.white);
	}
}

