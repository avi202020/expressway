/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import expressway.common.NodeIconImageNames;
import expressway.ser.*;
import expressway.gui.*;
import java.awt.image.ImageObserver;


public class PulseGeneratorVisualJ extends GeneratorVisualJ
{
	public static final Image GeneratorIconImage = getImage(NodeIconImageNames.GeneratorIconImageName);
	public static final Image NormalDistImage = getImage(NodeIconImageNames.NormalDistImageName);
	
	
	public PulseGeneratorVisualJ(PulseGeneratorSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
	}


	public String getDescriptionPageName() { return "Pulse Generators"; }
}

