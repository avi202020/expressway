/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.gui.modeldomain;


import java.awt.Graphics;
import java.awt.Color;
import expressway.ser.*;
import expressway.gui.*;
import java.awt.Image;
import expressway.common.NodeIconImageNames;


public class TerminalVisualJ extends FunctionVisualJ
{
	private Color repaintColor;
	

	public TerminalVisualJ(TerminalSer node, GraphicView view)
	throws
		Exception
	{
		super(node, view);
		setImageIcon(NodeIconImageNames.TerminalIconImageName);
		setBackground(TransparentWhite);
		repaintColor = getNormalBackgroundColor();
	}
	

	public String getDescriptionPageName() { return "Terminals"; }


	//int getNormalBorderThickness() { return 0; }
	

	void highlightBackground(boolean yes)
	{
		if (yes) this.repaintColor = getHighlightedBackgroundColor();
		else this.repaintColor = getNormalBackgroundColor();
		repaint();
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int w = this.getWidth()/2;
		int h = w;
		
		// Draw a filled circle in the background.
		
		int x = w/2;
		int y = x + this.getInsets().top;
	
		g.setColor(repaintColor);		
		g.fillOval(x, y, w, h) ;
	}
	
	
	private static final java.awt.Stroke normalStroke = new java.awt.BasicStroke((float)2.0);


	public Color getNormalBackgroundColor() { return TerminalColor; }
	
	
	public Color getHighlightedBackgroundColor() { return TerminalHLColor; }
}

