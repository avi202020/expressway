/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.gui.value;


public interface ValueElementVisual extends VisualComponent
{
	interface FundingSourceVisual extends 
	{
	}
	
	
	interface AccountVisual
	{
	}
	
	
	interface OpportunityVisual
	{
	}
	
	
	interface MarketVisual
	{
	}
	
	
	interface FulfillmentVisual
	{
	}
	
	
	interface SummationVisual
	{
	}
	
	
	interface MetricVisual
	{
	}
	
	
	/**
	 * Depict a Ramp, and provide the ability to fit the curves
	 * to data or estimates.
	 */
	 
	interface RampVisual
	{
	}
}


