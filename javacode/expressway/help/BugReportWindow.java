/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.help;


import expressway.common.*;
import generalpurpose.HTTPPoster;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;


public class BugReportWindow extends JFrame
{
	final JTextArea textArea;
	final JButton cancelButton;
	final JButton submitButton;
	
	
	public BugReportWindow()
	{
		super("Report a Problem about Expressway");
		
		this.setAlwaysOnTop(true);
		this.setSize(500, 400);
		this.setLocation(100, 100);
		
		textArea = new JTextArea(5, 30);
		JScrollPane scrollPane = new JScrollPane(textArea);
		
		this.add(scrollPane, BorderLayout.CENTER);
		
		JPanel buttonArea;
		this.add(buttonArea = new JPanel(), BorderLayout.SOUTH);
		
		buttonArea.add(cancelButton = new JButton("Cancel"));
		buttonArea.add(submitButton = new JButton("Submit"));
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				BugReportWindow.this.dispose();
			}
		});
		
		submitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				HTTPPoster.postAsync(ReleaseInfo.BugReportURL,
					"FirstName", "Expressway",
					"LastName", "",
					"formmail_mail_email", "bugs@expresswaysolutions.com",
					"Country", "",
					"BugInfo", textArea.getText());
				
				BugReportWindow.this.dispose();
			}
		});
	}
}
	
	

