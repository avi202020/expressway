/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.help;

import expressway.common.ReleaseInfo;
import java.io.IOException;
import java.net.URLEncoder;
import generalpurpose.HTMLWindow;
import generalpurpose.ThrowableUtil;


/**
 * A Frame in which HTML-formatted Help content is displayed.
 */
 
public class HelpWindow extends HTMLWindow
{
	private static final java.awt.Dimension DefaultSize = new java.awt.Dimension(300, 300);
	
	
	public HelpWindow(String title)
	throws
		IOException
	{
		super(title, false /* don't include Save button */, 
			true /* include forward and back buttons */);
		
		this.setSize(DefaultSize);
		this.setEditable(false);
		this.setAlwaysOnTop(true);
	}


	public static String createURL(String pageName)
	{
		pageName = pageName.replace(" ", "%20");
		//pageName = URLEncoder.encode(pageName, "UTF-8");  replaces spaces with
			// plus signs, but the HTMLWindow doesn't translate those into spaces.
			// See http://www.w3schools.com/tags/ref_urlencode.asp
		
		try {
		return ReleaseInfo.HelpBaseURLString + pageName + ".html";
		} catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public static String createURL(String pageName, String sectionName)
	{
		sectionName = sectionName.replace(" ", "%20");
		//sectionName = URLEncoder.encode(sectionName, "UTF-8");
				
		try {
		return createURL(pageName) + "#" + sectionName;
		} catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public static String createHref(String pageName, String linkText)
	{
		return "<a href=\"" + createURL(pageName) + "\">" + linkText + "</a>";
	}
	
	
	public static String createHref(String pageName, String sectionName, 
		String linkText)
	{
		return "<a href=\"" + createURL(pageName, sectionName) + "\">" + 
			linkText + "</a>";
	}
	
	
	public static String createHref(String pageName)
	{
		return "<a href=\"" + createURL(pageName) + "\">" + pageName + "</a>";
	}
}

