/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.help;

import expressway.common.ReleaseInfo;
import java.io.IOException;
import java.net.URLEncoder;
import generalpurpose.HTMLWindow;
import generalpurpose.ThrowableUtil;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Color;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;


/**
 * A Label or Text Field with a help Button next to it.
 */
 
public class HelpfulText
{
	final static Font TitleFont = new Font(Font.SANS_SERIF, Font.BOLD, 16);
	final static Font NormalFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
	
	final static FontMetrics TitleFontMetrics =
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(TitleFont);
	
	final static FontMetrics NormalFontMetrics =
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(NormalFont);
		
	
	/**
	 * Display a text Label with a Help Button next to it.
	 */
	 
	public static class StatusText extends AbstractStatusText
	{
		public StatusText(boolean isTitle, String text, String htmlHelpText)
		{
			super(new JLabel(), isTitle, text, htmlHelpText);
		}
		
		
		public StatusText(final boolean isTitle, 
			final String text, final int indentationLevel, final String htmlHelpText)
		{
			super(new JLabel(), isTitle, text, indentationLevel, htmlHelpText);
		}
	}
	
	
	/**
	 * Display a Text Field with a Help Button next to it.
	 */
	 
	public static class StatusTextField extends AbstractStatusText implements FocusListener
	{
		private String originalText = "";
		private TextChangedHandler handler;
		
		
		public StatusTextField(boolean isTitle, String text, String htmlHelpText)
		{
			super(new JTextField(), isTitle, text, htmlHelpText);
			this.originalText = text;
			getComponent().addFocusListener(this);
		}
		
		
		public StatusTextField(final boolean isTitle, 
			final String text, final int indentationLevel, final String htmlHelpText)
		{
			super(new JTextField(), isTitle, text, indentationLevel, htmlHelpText);
			this.originalText = text;
			getComponent().addFocusListener(this);
		}
		
		
		public StatusTextField setTextChangedHandler(TextChangedHandler handler)
		{
			this.handler = handler;
			return this;  // return this so that this method can be called when
				// the StatusTextField is created and one can still obtain a
				// reference to the StatusTextField.
		}
		
		
		public String getOriginalText() { return originalText; }
		
		
		protected void setOriginalText(String text)
		{
			this.originalText = (text == null? "" : text);
		}
		
		
		public void refreshText(String text)
		{
			setOriginalText(text);
			this.setText(text);
		}
		
		
		public void focusGained(FocusEvent e) {}
		
		
		public void focusLost(FocusEvent e)
		{
			if (this.handler == null) return;
			
			if (! this.getOriginalText().equals(this.getText()))
			{
				// Invoke TextChangedHandler.
				
				this.handler.textChanged(this);
			}
		}
	}
	
	
	public interface TextChangedHandler
	{
		void textChanged(StatusTextField statusTextField);
	}

		
	/**
	 * Display a text Label with a Help Button next to it.
	 */
	 
	public static abstract class AbstractStatusText extends JPanel
	{
		private Font font;
		private Component comp;
		private FontMetrics fontMetrics;
		private HelpButton helpButton = null;
		

		public AbstractStatusText(Component comp, boolean isTitle, String text, String htmlHelpText)
		{
			this(comp, isTitle, text, 0, htmlHelpText);
		}
		
		
		public AbstractStatusText(final Component comp, final boolean isTitle, 
			final String text, final int indentationLevel, final String htmlHelpText)
		{
			this.comp = comp;
			
			if (isTitle)
			{
				this.font = TitleFont;
				this.fontMetrics = TitleFontMetrics;
			}
			else
			{
				this.font = NormalFont;
				this.fontMetrics = NormalFontMetrics;
				this.setBackground(Color.white);
			}

			this.setFont(font);
			if (comp instanceof javax.swing.JLabel)
				((javax.swing.JLabel)(this.comp)).setText(text);
			else if (comp instanceof javax.swing.JTextField)
				((javax.swing.JTextField)(this.comp)).setText(text);
			else throw new RuntimeException("Unexpected component type");
			
			this.add(comp);
			this.comp.setFont(font);
			
			if (htmlHelpText != null)
			{
				this.add(this.helpButton = 
					new HelpButton(text, HelpButton.Size.Medium, htmlHelpText));
			}

			setLayout(new LayoutManager()
			{
				public void layoutContainer(Container parent)
				{
					comp.setLocation(2 + 4*indentationLevel, 2);

					comp.setSize(Math.max(100, comp.getX() + getTextWidth() + 10),
						HelpButton.LargeSize.height + 2);
					if (helpButton != null)
						helpButton.setLocation(comp.getX() + comp.getWidth(), 2);
				}
				
				public Dimension minimumLayoutSize(Container parent)
				{
					return new Dimension(Math.max(100, getTextWidth() + comp.getX() + 
						(helpButton == null? 0 : helpButton.getWidth())) + 20, 20);
				}
				
				public Dimension preferredLayoutSize(Container parent) 
				{
					return new Dimension(Math.max(100, getTextWidth() + comp.getX() + 
						(helpButton == null? 0 : helpButton.getWidth()) + 20), 20);
				}
				
				public void addLayoutComponent(String name, Component comp) { layoutContainer(AbstractStatusText.this); }
				public void removeLayoutComponent(Component comp)  { layoutContainer(AbstractStatusText.this); }
				
				int getTextWidth()
				{
					return fontMetrics.stringWidth(getText());
				}
			});
			
			validate();
		}
		
		
		public String getText()
		{
			if (comp instanceof javax.swing.JTextField)
				return ((javax.swing.JTextField)(this.comp)).getText();
			else if (comp instanceof javax.swing.JLabel)
				return ((javax.swing.JLabel)(this.comp)).getText();
			else throw new RuntimeException("Unexpected component type");
		}
		
		
		public void setText(String text)
		{
			String t = text == null? "" : text;

			if (comp instanceof javax.swing.JTextField)
				((javax.swing.JTextField)(this.comp)).setText(t);
			else if (comp instanceof javax.swing.JLabel)
				((javax.swing.JLabel)(this.comp)).setText(t);
			else throw new RuntimeException("Unexpected component type");
		}
		
		
		protected Component getComponent() { return this.comp; }
		
		
		protected void validateTree() { this.getLayout().layoutContainer(this); }
	}
}


