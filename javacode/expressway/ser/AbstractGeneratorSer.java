package expressway.ser;

import expressway.common.*;



public abstract class AbstractGeneratorSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String countPortNodeId;
	public String stateNodeId;
	public String countStateNodeId;
	
	
	public String timeShapeAttrNodeId;
	public String timeScaleAttrNodeId;
	public String valueShapeAttrNodeId;
	public String valueScaleAttrNodeId;
	public double defaultTimeDistShape;
	public double defaultTimeDistScale;
	public double defaultValueDistShape;
	public double defaultValueDistScale;
	
	public boolean ignoreStartup;
	public boolean repeating;
	
	
	/*public AbstractGeneratorSer(Generator d)
	{
		super(d);
		
		this.inputPortNodeId = d.getInputPortNodeId();
		this.outputPortNodeId = d.getOutputPortNodeId();
		this.countPortNodeId = d.getCountPortNodeId();
		this.stateNodeId = d.getStateNodeId();
		this.countStateNodeId = d.getCountStateNodeId();
		
		this.timeShapeAttrNodeId = d.getTimeShapeAttrNodeId();
		this.timeScaleAttrNodeId = d.getTimeScaleAttrNodeId();
		this.valueShapeAttrNodeId = d.getValueShapeAttrNodeId();
		this.valueScaleAttrNodeId = d.getValueScaleAttrNodeId();
		
		this.defaultTimeDistShape = d.getDefaultTimeDistShape();
		this.defaultTimeDistScale = d.getDefaultTimeDistScale();
		this.defaultValueDistShape = d.getDefaultValueDistShape();
		this.defaultValueDistScale = d.getDefaultValueDistScale();
		
		this.ignoreStartup = d.getIgnoreStartup();
		
		
		//System.out.println("Checking if gen " + d.getNodeId() + " is repeating"
		//	+ ", parent ModelContainer=" + d.getModelContainer().getNodeId());
			
		try { this.repeating = d.getModelContainer().isGeneratorRepeating(d); }
		catch (Exception ex) { throw new RuntimeException(ex); }
	}*/
}

