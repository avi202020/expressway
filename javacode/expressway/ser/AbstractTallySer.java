package expressway.ser;

import expressway.common.*;



public abstract class AbstractTallySer extends ActivitySer
{
	public String[] inputPortNodeIds;
	public String outputPortNodeId;
	public String stateNodeId;
	public String initValueAttrNodeId;
	public String doubleCountAttrNodeId;

	public Number defaultInitValue;
	public boolean defaultDoubleCount;
	
	
	public Object deepCopy()
	{
		AbstractTallySer copy = (AbstractTallySer)(super.deepCopy());
		
		copy.inputPortNodeIds = new String[this.inputPortNodeIds.length];
		for (int i = 0; i < copy.inputPortNodeIds.length; i++)
			copy.inputPortNodeIds[i] = this.inputPortNodeIds[i];
		
		return copy;
	}
}

