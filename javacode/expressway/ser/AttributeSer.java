package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;


import java.io.Serializable;

public interface AttributeSer extends NodeSer
{
	Serializable getDefaultValue();
	void setDefaultValue(Serializable dv);
}

