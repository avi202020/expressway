/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.ser;



public interface CrossRefableSer
{
	/**
	 * Returns the CrossReferences that the Node participates in.
	 * The returned array contains one element for each CrossReference; each
	 * element of the array consists of a three-element String array, where the
	 * three elements are:
	 *	<NameReference name>
	 *	<From-NodeId>
	 *	<To-NodeId>
	 * The Node represented by this NodeSer is either the From Node or the To Node.
	 * To determine which, one should compare the <From-NodeId> and the <To-NodeId>
	 * with the Node Id of this NodeSer.
	 */
	 
	String[][] getCrossRefs();
}

