package expressway.ser;

import expressway.common.*;


public class CrossReferenceSer extends NodeSerBase
{
	public String namedRefName;
	public String fromNodeId;
	public String toNodeId;

	public String getNodeKind() { return NodeKindNames.CrossReference; }
}

