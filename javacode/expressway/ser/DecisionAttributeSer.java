package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.io.Serializable;

public class DecisionAttributeSer extends DecisionElementSer implements AttributeSer
{
	public Serializable defaultValue;
	
	public String getNodeKind() { return NodeKindNames.DecisionAttribute; }
	
	public Serializable getDefaultValue() { return defaultValue; }
	public void setDefaultValue(Serializable dv) { this.defaultValue = dv; }
}

