package expressway.ser;


import expressway.common.*;


public class DecisionDomainMotifDefSer extends DecisionDomainSer implements MenuOwnerSer
{
	public String[] menuItemNodeIds;
	public String[] menuTreeNodeIds;

	public String[] motifClassNames;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	public String[] javaMethodNames;
	
	public String getNodeKind() { return NodeKindNames.DecisionDomainMotifDef; }


	
	/* From MenuOwnerSer. */
	 
	public void setMenuItemNodeIds(String[] ids) { this.menuItemNodeIds = ids; }
	public void setMenuItemTextAr(String[] textAr) { this.menuItemTextAr = textAr; }
	public void setMenuItemDescAr(String[] descAr) { this.menuItemDescAr = descAr; }
	public void setJavaMethodNames(String[] names) { this.javaMethodNames = names; }
	public void setMenuTreeNodeIds(String[] ids) { this.menuTreeNodeIds = ids; }
	
	public String[] getMenuItemNodeIds() { return menuItemNodeIds; }
	public String[] getMenuItemTextAr() { return menuItemTextAr; }
	public String[] getMenuItemDescAr() { return menuItemDescAr; }
	public String[] getJavaMethodNames() { return javaMethodNames; }
	public String[] getMenuTreeNodeIds() { return menuTreeNodeIds; }
}

