package expressway.ser;

import expressway.common.*;
import java.util.Date;
import java.io.Serializable;


public class DecisionScenarioSer extends DecisionElementSer implements ScenarioSer
{
	public String name;
	public Date date;
	public String[] decisionNodeIds;


	public String[] attributeNodeIds;
	public Serializable[] attributeValues;
	
	public String scenarioThatWasCopiedNodeId;
	public String scenarioSetNodeId;

	
	public String getNodeKind() { return NodeKindNames.DecisionScenario; }

	public void setDate(Date d) { this.date = d; }
	public Date getDate() { return date; }
	
	public void setScenarioThatWasCopiedNodeId(String id) { this.scenarioThatWasCopiedNodeId = id; }
	public String getScenarioThatWasCopiedNodeId() { return scenarioThatWasCopiedNodeId; }
	
	public void setScenarioSetNodeId(String id) { this.scenarioSetNodeId = id; }
	public String getScenarioSetNodeId() { return scenarioSetNodeId; }
	
	public void setAttributeNodeIds(String[] ids) { this.attributeNodeIds = ids; }
	public String[] getAttributeNodeIds() { return attributeNodeIds; }
	
	public void setAttributeValues(Serializable[] values) { this.attributeValues = values; }
	public Serializable[] getAttributeValues() { return attributeValues; }
	
	public void setDecisionNodeIds(String[] ids) { this.decisionNodeIds = ids; }
	public String[] getDecisionNodeIds() { return decisionNodeIds; }

	public Object deepCopy()
	{
		DecisionScenarioSer copy = (DecisionScenarioSer)(super.deepCopy());
		
		copy.decisionNodeIds = new String[this.decisionNodeIds.length];
		for (int i = 0; i < copy.decisionNodeIds.length; i++)
			copy.decisionNodeIds[i] = this.decisionNodeIds[i];
		
		return copy;
	}
}

