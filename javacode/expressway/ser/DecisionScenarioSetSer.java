package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import java.util.Date;


public class DecisionScenarioSetSer extends DecisionElementSer implements ScenarioSetSer
{
	public String attributeId;
	public String scenarioThatWasCopiedNodeId;
	public String[] decisionScenarioIds;

	public String getNodeKind() { return NodeKindNames.DecisionScenarioSet; }
	
	public void setScenarioIds(String[] ids) { decisionScenarioIds = ids; }
	public String[] getScenarioIds() { return decisionScenarioIds; }
	
	public void setScenarioThatWasCopiedNodeId(String id) { scenarioThatWasCopiedNodeId = id; }
	public String getScenarioThatWasCopiedNodeId() { return scenarioThatWasCopiedNodeId; }

	public void setVariableAttributeId(String id) { this.attributeId = id; }
	public String getVariableAttributeId() { return attributeId; }
}

