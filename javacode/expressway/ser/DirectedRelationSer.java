package expressway.ser;

import expressway.common.*;



public class DirectedRelationSer extends ConstraintSer
{
	public String[] subjectNodeIds;
	public String[] directObjectNodeIds;
	
	public String getNodeKind() { return NodeKindNames.DirectedRelation; }
	
	public Object deepCopy()
	{
		DirectedRelationSer copy = (DirectedRelationSer)(super.deepCopy());
		
		copy.subjectNodeIds = new String[this.subjectNodeIds.length];
		for (int i = 0; i < copy.subjectNodeIds.length; i++)
			copy.subjectNodeIds[i] = this.subjectNodeIds[i];
		
		copy.directObjectNodeIds = new String[this.directObjectNodeIds.length];
		for (int i = 0; i < copy.directObjectNodeIds.length; i++)
			copy.directObjectNodeIds[i] = this.directObjectNodeIds[i];
		
		return copy;
	}
}

