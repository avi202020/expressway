package expressway.ser;

import expressway.common.*;



public class DiscriminatorSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String controlPortNodeId;
	public String stateNodeId;
	public String thresholdAttrNodeId;
	public double defaultThreshold;
	
	public String getNodeKind() { return NodeKindNames.Discriminator; }
}

