package expressway.ser;

import expressway.common.*;

public class DomainTypeSer extends NodeSerBase
{          
	public boolean builtin;
	public String domainTypeName;
	public String[] defaultMotifIds;
	
	
	public DomainTypeSer() {}
	
	
	public DomainTypeSer(boolean builtin, String domainTypeName, String[] defaultMotifIds)
	{
		this.builtin = builtin;
		this.domainTypeName = domainTypeName;
		this.defaultMotifIds = defaultMotifIds;
	}
	
	
	public String getNodeKind() { return NodeKindNames.DomainType; }
}

