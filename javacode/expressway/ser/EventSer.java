package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import generalpurpose.SerializableUtils;
import java.io.Serializable;


import java.util.Date;
import java.io.Serializable;

public class EventSer implements Serializable, Cloneable
{
	public String stateNodeId;
	public Date timeToOccur;
	public Serializable newValue;
	public boolean pulse;
	
	
	public EventSer()
	{
	}
	
	
	public EventSer(String stateNodeId, Date timeToOccur, Serializable newValue, boolean pulse)
	{
		this.stateNodeId = stateNodeId;
		this.timeToOccur = timeToOccur;
		this.newValue = newValue;
		this.pulse = pulse;
	}
	
	
	public EventSer(String stateNodeId, Date timeToOccur, Serializable newValue)
	{
		this(stateNodeId, timeToOccur, newValue, false);
	}
	
	
	public String getEventClassName() { return "expressway.server.EventImpl"; }
	
	
	EventSer makeCopy()
	{
		return (EventSer)(clone());
	}
	
	
	protected Object clone()
	{
		try
		{
			EventSer eventSerCopy = (EventSer)(super.clone());
			eventSerCopy.newValue = SerializableUtils.makeCopy(newValue);
			return eventSerCopy;
		}
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public String toString()
	{
		return
			this.getClass().getName() +
			": State " + 
			(stateNodeId == null? "null" : stateNodeId) + 
			" -> " + 
			(newValue == null? "null" : newValue.toString()) + 
			" @ " + 
			(timeToOccur == null? "null" : timeToOccur)
			;
	}
}

