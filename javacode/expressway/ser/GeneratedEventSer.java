package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.util.Date;
import java.io.Serializable;



public class GeneratedEventSer extends SimulationTimeEventSer
{
	public Epoch whenScheduled;
	public boolean compensation;
	public long[] triggeringEvents;
	
	
	public GeneratedEventSer()
	{
	}
	

	public GeneratedEventSer(long id, String simRunNodeId, String stateNodeId,
		Date timeToOccur, Serializable newValue,
		Epoch epoch, Epoch whenScheduled, long[] triggeringEventIds, boolean pulse,
		boolean compensation)
	{
		super(id, simRunNodeId, stateNodeId, timeToOccur, newValue, pulse, epoch);
		this.whenScheduled = whenScheduled;
		this.compensation = compensation;
		this.triggeringEvents = triggeringEvents;
	}
	
	
	public String getEventClassName() { return "expressway.server.GeneratedEventImpl"; }


	//public Object deepCopy()
	//{
	//	GeneratedEventSer copy = (GeneratedEventSer)(super.deepCopy());
	//	
	//	copy.triggeringEvents = new long[triggeringEvents.length];
	//	int i = 0;
	//	for (long id : triggeringEvents) copy.triggeringEvents[i++] = id;
	//	
	//	return copy;
	//}
}

