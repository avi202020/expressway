package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import java.net.URL;


public interface HierarchyDomainSer extends HierarchySer, NodeDomainSer
{
	void setSourceName(String sourceName);
	void setSourceURL(URL sourceURL);
	void setDeclaredName(String declaredName);
	void setDeclaredVersion(String declaredVersion);
	
	String getSourceName();
	URL getSourceURL();
	String getDeclaredName();
	String getDeclaredVersion();
}

