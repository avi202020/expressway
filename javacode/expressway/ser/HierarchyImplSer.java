package expressway.ser;

import expressway.common.*;
import java.io.Serializable;
import generalpurpose.StringUtils;


public class HierarchyImplSer extends HierarchyElementImplSer
	implements HierarchySer
{
	public String[] fieldNames;
	public String[] subHierarchyNodeIds;
	public int seqNo;

	public String getNodeKind() { return NodeKindNames.Requirement; }
	
	
  /* From HierarchySer */

	
	public void setSubHierarchyNodeIds(String[] ids) { this.subHierarchyNodeIds = ids; }
	
	
	public String[] getSubHierarchyNodeIds() { return subHierarchyNodeIds; }
	
	
	public void setFieldNames(String[] names)
	{
		this.fieldNames = names;
	}
	
	
	/** Does not include the first column, which is the 'Name' field. */
	public String[] getFieldNames() { return fieldNames; }
	
	
	public void setSeqNo(int n) { this.seqNo = n; }
	public int getSeqNo() { return seqNo; }
}

