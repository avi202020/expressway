package expressway.ser;

public abstract class MenuOwnerBaseSer extends MotifElementSer implements MenuOwnerSer
{
	/*
	The goal of this implementation is to contain all of the information about
	the MenuItems that are directly owned by the MenuOwner, so as to avoid
	a separate call to the server to retrieve each MenuItem.
	 */
	
	public String[] menuItemNodeIds;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	
	
	
	/**
	 * Contains one entry for each menuItemTextAr element. If one of those elements
	 * is a MenuTree, then the corresponding entry in javaMethodNames is null.
	 */
	 
	public String[] javaMethodNames;
	
	
	/**
	 * Contains one entry for each menuItemTextAr element. If one of those elements
	 * is an Action, then the corresponding entry in javaMethodNames is null.
	 */
	 
	public String[] menuTreeNodeIds;


	/*
	 * From MenuOwnerSer.
	 */
	 
	public void setMenuItemNodeIds(String[] ids) { this.menuItemNodeIds = ids; }
	public void setMenuItemTextAr(String[] textAr) { this.menuItemTextAr = textAr; }
	public void setMenuItemDescAr(String[] descAr) { this.menuItemDescAr = descAr; }
	public void setJavaMethodNames(String[] names) { this.javaMethodNames = names; }
	public void setMenuTreeNodeIds(String[] ids) { this.menuTreeNodeIds = ids; }
	
	public String[] getMenuItemNodeIds() { return menuItemNodeIds; }
	public String[] getMenuItemTextAr() { return menuItemTextAr; }
	public String[] getMenuItemDescAr() { return menuItemDescAr; }
	public String[] getJavaMethodNames() { return javaMethodNames; }
	public String[] getMenuTreeNodeIds() { return menuTreeNodeIds; }
}

