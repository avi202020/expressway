package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;


import java.io.Serializable;

public class ModelAttributeSer extends ModelElementSer implements AttributeSer
{
	public String varAttrBindingNodeId;
	public Serializable defaultValue;
	public String attrStateBindingNodeId;
	public int attributeNo;
	public String modelTemplateNodeId;
	public String modelTemplateFullName;
	public String[] menuItemNodeIds;
	public String[] menuItemTextAr;
	public String[] menuItemDescAr;
	
	public String getNodeKind() { return NodeKindNames.ModelAttribute; }
	
	public Serializable getDefaultValue() { return defaultValue; }
	
	public void setDefaultValue(Serializable dv) { this.defaultValue = dv; }
	

	public Object deepCopy()
	{
		ModelAttributeSer copy = (ModelAttributeSer)(super.deepCopy());
		
		copy.defaultValue = null;
		
		//copy.attrStateBindingNodeIds = new String[attrStateBindingNodeIds.length];
		//for (int i=0; i < attrStateBindingNodeIds.length; i++)
		//	copy.attrStateBindingNodeIds[i] = this.attrStateBindingNodeIds[i];
		
		return copy;
	}
}

