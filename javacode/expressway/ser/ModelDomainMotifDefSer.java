package expressway.ser;


import expressway.common.*;


public class ModelDomainMotifDefSer extends ModelDomainSer implements MotifDefSer, MenuOwnerSer
{
	public String[] motifClassNames;
	public String getNodeKind() { return NodeKindNames.ModelDomainMotifDef; }

	/* From MotifDefSer. */
	
	public String[] getMotifClassNames() { return motifClassNames; }
}

