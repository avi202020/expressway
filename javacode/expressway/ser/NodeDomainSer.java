/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.ser;


import expressway.common.*;
import java.net.URL;


public interface NodeDomainSer extends NodeSer, MenuOwnerSer
{
	void setSourceName(String name);
	String getSourceName();
	
	void setSourceURL(URL url);
	URL getSourceURL();
	
	void setDeclaredName(String name);
	String getDeclaredName();
	
	void setDeclaredVersion(String version);
	String getDeclaredVersion();
	
	void setNamedReferenceNames(String[] names);
	String[] getNamedReferenceNames();
	
	void setScenarioSetNodeIds(String[] ids);
	String[] getScenarioSetNodeIds();
	
	void setCurrentScenarioNodeId(String id);
	String getCurrentScenarioNodeId();
	
	void setScenarioIds(String[] ids);
	String[] getScenarioIds();
	
	void setMotifDefNodeIds(String[] ids);
	String[] getMotifDefNodeIds();
}

