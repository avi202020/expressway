package expressway.ser;

import expressway.common.*;



public class NotExpressionSer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String stateNodeId;
	public String getNodeKind() { return NodeKindNames.NotExpression; }
}

