package expressway.ser;

import expressway.common.*;



public class OneWaySer extends ActivitySer
{
	public String inputPortNodeId;
	public String outputPortNodeId;
	public String stateNodeId;
	
	public String getNodeKind() { return NodeKindNames.OneWay; }
}

