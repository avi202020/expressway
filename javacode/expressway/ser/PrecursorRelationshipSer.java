package expressway.ser;

import expressway.common.*;



public class PrecursorRelationshipSer extends DecisionElementSer
{
	public String precursorDecisionPointNodeId;
	public String subordinateDecisionPointNodeId;
	
	public String getNodeKind() { return NodeKindNames.PrecursorRelationship; }
}

