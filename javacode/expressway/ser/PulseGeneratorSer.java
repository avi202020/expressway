package expressway.ser;

import expressway.common.*;



public class PulseGeneratorSer extends GeneratorSer
//public class PulseGeneratorSer extends ActivitySer
{
	public String riseStateNodeId;
	public String riseInNodeId;
	public String riseOutNodeId;
	
	public String getNodeKind() { return NodeKindNames.PulseGenerator; }
}

