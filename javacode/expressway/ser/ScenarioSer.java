/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.ser;

import expressway.common.*;
import java.util.Date;
import java.io.Serializable;


public interface ScenarioSer extends NodeSer
{
	void setDate(Date d);
	Date getDate();
	
	void setScenarioThatWasCopiedNodeId(String id);
	String getScenarioThatWasCopiedNodeId();
	
	void setScenarioSetNodeId(String id);
	String getScenarioSetNodeId();
	
	void setAttributeNodeIds(String[] ids);
	String[] getAttributeNodeIds();
	
	void setAttributeValues(Serializable[] values);
	Serializable[] getAttributeValues();
}

