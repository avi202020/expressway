package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.io.Serializable;


import java.util.Date;
import java.io.Serializable;

public class SimulationTimeEventSer extends EventSer
{
	public long id;
	public String simRunNodeId;
	public Epoch epoch;
	
	
	public SimulationTimeEventSer()
	{
	}
	
	
	public SimulationTimeEventSer(long id, String simRunNodeId, String stateNodeId,
		Date timeToOccur, Serializable newValue, boolean pulse, Epoch epoch)
	{
		super(stateNodeId, timeToOccur, newValue, pulse);
		
		this.id = id;
		this.simRunNodeId = simRunNodeId;
		this.epoch = epoch;
	}
	
	
	public SimulationTimeEventSer(long id, String simRunNodeId, String stateNodeId,
		Date timeToOccur, Serializable newValue, Epoch epoch)
	{
		this(id, simRunNodeId, stateNodeId, timeToOccur, newValue, false, epoch);
	}
	
	
	public String getEventClassName() { return "expressway.server.SimulationTimeEventImpl"; }


	//public Object deepCopy()
	//{
	//	SimulationTimeEventSer copy = (SimulationTimeEventSer)(super.deepCopy());
	//	
	//	copy.newValue = null;
	//	
	//	return copy;
	//}
}

