package expressway.ser;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;


/**
 * Note: This class should not be used, since Startup Events are an internal
 * mechanism and should not be archived or exposed to clients. Thus, the
 * externalize(...) method should never be called on a StartupEvent.
 */
 
public class StartupEventSer extends EventSer
{
	public String componentNodeId;
	
	public Epoch epoch;
}

