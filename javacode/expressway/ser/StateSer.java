package expressway.ser;

import expressway.common.*;
import generalpurpose.TreeSetNullDisallowed;
import java.util.*;


public class StateSer extends ModelComponentSer
{
	public String eventProducerNodeId;
	public int stateNo;
	public String[] preEventNodeIds;
	public String[] attributeBindingNodeIds;
	public String[] boundAttrNodeIds;
	public String[] portBindingNodeIds;
	
	public String getNodeKind() { return NodeKindNames.State; }
	
	public Set<String> getPredefinedEventNodeIds()
	{
		Set<String> preEventNodeIdSet = new TreeSetNullDisallowed<String>();
		preEventNodeIdSet.addAll(Arrays.asList(this.preEventNodeIds));
		
		return preEventNodeIdSet;
	}
	
	
	public Object deepCopy()
	{
		StateSer copy = (StateSer)(super.deepCopy());
		
		copy.preEventNodeIds = new String[this.preEventNodeIds.length];
		for (int i = 0; i < copy.preEventNodeIds.length; i++)
			copy.preEventNodeIds[i] = this.preEventNodeIds[i];
		
		copy.attributeBindingNodeIds = new String[this.attributeBindingNodeIds.length];
		for (int i = 0; i < copy.attributeBindingNodeIds.length; i++)
			copy.attributeBindingNodeIds[i] = this.attributeBindingNodeIds[i];
		
		copy.portBindingNodeIds = new String[this.portBindingNodeIds.length];
		for (int i = 0; i < copy.portBindingNodeIds.length; i++)
			copy.portBindingNodeIds[i] = this.portBindingNodeIds[i];
		
		return copy;
	}
}

