package expressway.ser;

import expressway.common.*;
import java.io.Serializable;


public interface TabularSer extends NodeSer
{
	/** Names of Attributes. Does NOT include the name field (because it is
		not an Attribute). */
	void setFieldNames(String[] fieldNames);
	String[] getFieldNames();
}

