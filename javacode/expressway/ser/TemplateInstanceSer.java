package expressway.ser;


import expressway.ser.NodeSer;


public interface TemplateInstanceSer extends NodeSer
{
	String getTemplateNodeId();
	String getTemplateFullName();
	
	void setTemplateNodeId(String id);
	void setTemplateFullName(String s);
}

