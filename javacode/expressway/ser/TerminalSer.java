package expressway.ser;

import expressway.common.*;



public class TerminalSer extends FunctionSer
{
	public String portNodeId;
	public String stateNodeId;
	public int color;
	
	public String getNodeKind() { return NodeKindNames.Terminal; }
}

