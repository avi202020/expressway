package expressway.ser;

import expressway.common.*;



public class VariablePulseGeneratorSer extends PulseGeneratorSer
{
	public String theta1PortNodeId;
	public String theta2PortNodeId;
	
	public String getNodeKind() { return NodeKindNames.VariablePulseGenerator; }
}

