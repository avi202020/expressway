/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import generalpurpose.DateAndTimeUtils;
import statistics.DeterministicDistribution;
import statistics.NormalizedGammaDistribution;
import java.util.Set;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public abstract class AbstractSustain extends ActivityBase implements Sustain
{
	public static final double DefaultTimeDistShape = 2.0;
	public static final double DefaultTimeDistScale = 1.0;
	

	public static final AttributeValidator ShapeParamValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof Number)) throw new ParameterError(
				"Sustain shape parameter value must be a Number");
			
			Double value = ((Number)newValue).doubleValue();
			
			if (value < 0.0) throw new ParameterError("Sustain shape must be non-negative");
		}
	};


	public static final AttributeValidator ScaleParamValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof Number)) throw new ParameterError(
				"Sustain scale parameter value must be a Number");
			
			Double value = ((Number)newValue).doubleValue();
			
			if (value < 0.0) throw new ParameterError("Sustain scale must be non-negative");
		}
	};


	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public Port periodPort = null;

	/** reference only. */
	public State state = null;
	
	public double defaultTimeDistShape;
	
	public double defaultTimeDistScale;

	/** reference only. */
	public ModelAttribute timeDistShapeAttr = null;
	
	/** reference only. */
	public ModelAttribute timeDistScaleAttr = null;


	public transient String inputPortNodeId = null;
	public transient String outputPortNodeId = null;
	public transient String periodPortNodeId = null;
	public transient String stateNodeId = null;
	public transient String timeDistShapeAttrNodeId = null;
	public transient String timeDistScaleAttrNodeId = null;

	public String getInputPortNodeId() { return inputPortNodeId; }
	public String getOutputPortNodeId() { return outputPortNodeId; }
	public String getPeriodPortNodeId() { return periodPortNodeId; }
	public String getStateNodeId() { return stateNodeId; }
	public String getTimeDistShapeAttrNodeId() { return timeDistShapeAttrNodeId; }
	public String getTimeDistScaleAttrNodeId() { return timeDistScaleAttrNodeId; }
	

	public Class getSerClass() { return AbstractSustainSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		inputPortNodeId = getNodeIdOrNull(inputPort);
		outputPortNodeId = getNodeIdOrNull(outputPort);
		periodPortNodeId = getNodeIdOrNull(periodPort);
		stateNodeId = getNodeIdOrNull(state);
		timeDistShapeAttrNodeId = getNodeIdOrNull(timeDistShapeAttr);
		timeDistScaleAttrNodeId = getNodeIdOrNull(timeDistScaleAttr);
		
		
		// Set Ser fields.
		
		((AbstractSustainSer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((AbstractSustainSer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((AbstractSustainSer)nodeSer).periodPortNodeId = this.getPeriodPortNodeId();
		((AbstractSustainSer)nodeSer).stateNodeId = this.getStateNodeId();
		((AbstractSustainSer)nodeSer).defaultTimeDistShape = this.getDefaultTimeDistShape();
		((AbstractSustainSer)nodeSer).defaultTimeDistScale = this.getDefaultTimeDistScale();
		((AbstractSustainSer)nodeSer).timeDistShapeAttrNodeId = this.getTimeDistShapeAttrNodeId();
		((AbstractSustainSer)nodeSer).timeDistScaleAttrNodeId = this.getTimeDistScaleAttrNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		AbstractSustain newInstance = (AbstractSustain)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.periodPort = cloneMap.getClonedNode(periodPort);
		newInstance.state = cloneMap.getClonedNode(state);
		newInstance.timeDistShapeAttr = cloneMap.getClonedNode(timeDistShapeAttr);
		newInstance.timeDistScaleAttr = cloneMap.getClonedNode(timeDistScaleAttr);
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deletePort(periodPort, null);
		deleteState(state, null);	
		deleteAttribute(timeDistShapeAttr, null);
		deleteAttribute(timeDistScaleAttr, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		periodPort = null;
		state = null;	
		timeDistShapeAttr = null;
		timeDistScaleAttr = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public AbstractSustain(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale)
	{
		super(name, parent, domain);
		init(timeDistShape, timeDistScale);
	}


	public AbstractSustain(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultTimeDistShape, DefaultTimeDistScale);
	}
	
	
	public String getTagName() { return "sustain"; }
	
	
	private void init(double timeDistShape, double timeDistScale)
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		this.defaultTimeDistShape = timeDistShape;
		this.defaultTimeDistScale = timeDistScale;
		
		try
		{
			this.timeDistShapeAttr = createModelAttribute("time_dist_shape", new Double(timeDistShape), this, null);
			this.timeDistScaleAttr = createModelAttribute("time_dist_scale", new Double(timeDistScale), this, null);

			inputPort = super.createPort("input_port", PortDirectionType.input, Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, Position.right, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		this.timeDistShapeAttr.setValidator(ShapeParamValidator);
		this.timeDistScaleAttr.setValidator(ScaleParamValidator);

		
		//inputPort.setSide(Position.left);
		//outputPort.setSide(Position.right);

		
		// Automatically bind the output Port to the internal State.

		try { state = super.createState("SustainState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}


		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		state.setMovable(false);
		timeDistShapeAttr.setMovable(false);
		timeDistScaleAttr.setMovable(false);
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		state.setPredefined(true);
		timeDistShapeAttr.setPredefined(true);
		timeDistScaleAttr.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 2; }
	
	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }
	
	public Port getPeriodPort() { return periodPort; }
	
	public double getDefaultTimeDistShape() { return defaultTimeDistShape; }
	
	public double getDefaultTimeDistScale() { return defaultTimeDistScale; }
	
	public ModelAttribute getTimeDistShapeAttr() { return timeDistShapeAttr; }
	
	public ModelAttribute getTimeDistScaleAttr() { return timeDistScaleAttr; }
}


class AbstractSustainNativeImpl extends ActivityNativeImplBase
{
	private State state = null;
	private String stateName = "SustainState";
	//private double timeDistShape = 0.0;
	//private double timeDistScale = 0.0;
	private ContinuousDistribution timeDistribution = null;

	boolean stopped = false;
	boolean hasReceivedNonNullEvent = false;
	private Serializable priorInputValue = null;  // the most recently received input
		// event value.
		
	private static final Boolean falseBoolean = new Boolean(false);
	private static final Double zeroDouble = new Double(0.0);
	

	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);

		state = context.getState(stateName);
		initializeTimeDistribution();
	}


	public synchronized void stop()
	{
		stopped = true;

		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		if (events.size() == 0) return;  // startup: do nothing.
		
		
		Serializable inVal = null;
		boolean foundRisingValue = false;
		Serializable newValue = null;
		Serializable zeroValue = null;
		
		
		// Check that there is an event on the input port.

		if (portHasNonCompensatingEvent(events, getInputPort()))
		{
			// Get input value.
			inVal = getActivityContext().getStateValue(getInputPort());


			// Check if the input value is null: if so, verify that we have not already
			// received a non-null event, since once we receive a non-null event we
			// should not receive any more.
			
			if (inVal == null)
			{
				if (hasReceivedNonNullEvent) throw new ModelContainsError(
					"Sustain has received a null-valued event after having " +
						"previously receiving a non-null valued event");
						
				return; // do not respond to null-valued events.
			}
			else
				hasReceivedNonNullEvent = true;
	
			
			// Get the Numeric or Boolean value.
			
			Boolean bVal = null;
			Number numVal = null;
			try
			{
				bVal = (Boolean)inVal;
				newValue = new Boolean(bVal);
				zeroValue = falseBoolean;
			}
			catch (ClassCastException cce)
			{
				try
				{
					numVal = (Number)inVal;
					newValue = new Double(numVal.doubleValue());
					zeroValue = zeroDouble;
				}
				catch (ClassCastException cce2) { throw new ModelContainsError(
					"Sustain input event must be Numeric or Boolean"); }
			}
	
				
			// Check for rising value.
			
			if (inVal instanceof Boolean)
			{
				if (! ((priorInputValue != null) && (priorInputValue instanceof Boolean) &&
					((! bVal.booleanValue()) && ((Boolean)priorInputValue).booleanValue())))
				{
					foundRisingValue = true;
				}
			}
			else if (inVal instanceof Number)
			{
				if (! ((priorInputValue != null) && (priorInputValue instanceof Number) &&
					(numVal.doubleValue() <= ((Number)priorInputValue).doubleValue())))
				{
					foundRisingValue = true;
				}
			}

			
			// Save the input value so that we can check it on next time around.
			
			priorInputValue = inVal;  // Note that if Numbers were mutable we
										// would have to copy the value.
		}
		
		// Check the activation condition.
		if (events.size() == 0) return;
		if (! foundRisingValue) return;
		//if (! (events.size() == 0) || foundRisingValue) return;
		
		
		// Activated.
		
		double timeDelayDays = 0;
		
		try
		{
			if (timeDistribution == null)
			{
				throw new RuntimeException("Time dist is null for " + getSustain().getName());
			}
			
			timeDelayDays = getTimeDistribution().inverseCumulativeProbability(
				getModelContext().getSimulationRun().getRandomGenerator().nextDouble());
		}
		catch (MathException ex)
		{
			throw new ModelContainsError(
				"When computing inverse cumulative probability;", ex);
		}
		catch (Throwable t) { GlobalConsole.printStackTrace(t); return; }
		

		long timeDelayMs = (long)(timeDelayDays * DateAndTimeUtils.MsInADay);

		
		// Schedule future events, for the output and the count.
		
		GeneratedEvent event = null;
		GeneratedEvent countEvent = null;
		try
		{
			getActivityContext().setState(this.state, newValue, events);	
			getActivityContext().scheduleDelayedStateChange(this.state, timeDelayMs,
				zeroValue, events);
		}
		catch (ParameterError ex)
		{
			throw new ModelContainsError(
				"When scheduling delayed state change", ex);
		}
	}


	protected ContinuousDistribution getTimeDistribution() { return timeDistribution; }

	protected void setTimeDistribution(ContinuousDistribution timeDistribution)
	{
		this.timeDistribution = timeDistribution;
	}

	protected State getState() { return state; }
	
	protected String getStateName() { return stateName; }
	
	protected AbstractSustain getSustain() { return (AbstractSustain)(getActivity()); }
	
	protected Port getInputPort() { return getSustain().getInputPort(); }
	
	protected Port getOutputPort() { return getSustain().getOutputPort(); }
	
	protected void initializeTimeDistribution()
	{
		//double timeDistShape = getSustain().getTimeDistShape();
		//double timeDistScale = getSustain().getTimeDistScale();
		
		double timeDistShape = getAttributeDoubleValue(getSustain().getTimeDistShapeAttr());
		double timeDistScale = getAttributeDoubleValue(getSustain().getTimeDistScaleAttr());
		
		if (timeDistShape == 0)
			timeDistribution = new DeterministicDistribution(timeDistScale);
		else
			timeDistribution = 
				new NormalizedGammaDistribution(timeDistShape, timeDistScale);
				
		if (timeDistribution == null) throw new RuntimeException(
			"Time distribution initialized to null");
	}
}
