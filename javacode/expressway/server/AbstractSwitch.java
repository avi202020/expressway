/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Set;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public abstract class AbstractSwitch extends ActivityBase implements Switch
{
	public static final boolean DefaultStartConducting = false;
	
	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public Port controlPort = null;
	
	/** reference only. */
	public State state = null;  // the output state.
	
	public boolean defaultStartConducting;
	
	/** reference only. */
	public ModelAttribute startConductingAttr = null;
	
	
	public String getInputPortNodeId() { return getNodeIdOrNull(inputPort); }
	public String getOutputPortNodeId() { return getNodeIdOrNull(outputPort); }
	public String getControlPortNodeId() { return getNodeIdOrNull(controlPort); }
	public String getStateNodeId() { return getNodeIdOrNull(state); }
	public String getStartConductingAttrNodeId() { return getNodeIdOrNull(startConductingAttr); }
	
	
	public Class getSerClass() { return AbstractSwitchSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((AbstractSwitchSer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((AbstractSwitchSer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((AbstractSwitchSer)nodeSer).controlPortNodeId = this.getControlPortNodeId();
		((AbstractSwitchSer)nodeSer).stateNodeId = this.getStateNodeId();
		((AbstractSwitchSer)nodeSer).defaultStartConducting = this.getDefaultStartConducting();
		((AbstractSwitchSer)nodeSer).startConductingAttrNodeId = this.getStartConductingAttrNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		AbstractSwitch newInstance = (AbstractSwitch)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.controlPort = cloneMap.getClonedNode(controlPort);
		newInstance.state = cloneMap.getClonedNode(state);
		newInstance.startConductingAttr = cloneMap.getClonedNode(startConductingAttr);
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deletePort(controlPort, null);
		deleteState(state, null);
		deleteAttribute(startConductingAttr, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		controlPort = null;
		state = null;
		startConductingAttr = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public AbstractSwitch(String name, ModelContainer parent, ModelDomain domain,
		boolean conducting)
	{
		super(name, parent, domain);
		init(conducting);
	}
	
	
	public AbstractSwitch(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultStartConducting);
	}
	
	
	public String getTagName() { return "switch"; }
	
	
	private void init(boolean conducting)
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		this.defaultStartConducting = conducting;
		
		try
		{
			this.startConductingAttr = createModelAttribute("start_conducting", 
				new Boolean(conducting), this, null);

			inputPort = super.createPort("input_port", PortDirectionType.input, 
				Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
			controlPort = super.createPort("control_port", PortDirectionType.input, 
				Position.bottom, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}

		//inputPort.setSide(Position.left);
		//outputPort.setSide(Position.right);
		//controlPort.setSide(Position.top);
		
		
		// Automatically bind the output Port to the internal State.

		try { state = super.createState("OutputState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		

		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		controlPort.setMovable(false);
		state.setMovable(false);
		startConductingAttr.setMovable(false);
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		controlPort.setPredefined(true);
		state.setPredefined(true);
		startConductingAttr.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }
	
	public Port getControlPort() { return controlPort; }
	
	public State getState() { return state; }
	
	public boolean getDefaultStartConducting() { return defaultStartConducting; }
	
	public ModelAttribute getStartConductingAttr() { return startConductingAttr; }
}


class AbstractSwitchNativeImpl extends ActivityNativeImplBase
{
	boolean stopped = false;
	boolean conducting = false;  // internal control state.
	

	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);
		this.conducting = getAttributeBooleanValue(getSwitch().getStartConductingAttr());
	}


	public synchronized void stop()
	{
		stopped = true;

		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		if (events.size() == 0) return;


		/*
		 * If there is an event on the control port (including a startup event),
		 *	Read the control port and update the control state.
		 *
		 * If the control state is conducting,
		 *	Create an event on the output state, matching the value on the
		 *		input port.
		 */
		
		if (portHasEvent(events, getControlPort()))
		{
			Serializable cVal = getActivityContext().getStateValue(getControlPort());
			
			if (! (cVal instanceof Boolean)) throw new ModelContainsError(
				"Control value must be Boolean");
			
			conducting = ((Boolean)cVal).booleanValue();
			
		}
		
		if (conducting) updateOutput(events);		
	}
	
	
	/**
	 * Generate an output event to match the current driven state of the input
	 * port.
	 */
	
	protected void updateOutput(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		// Get the current driven value of the input port.
		Serializable inVal = getActivityContext().getStateValue(getInputPort());
		
		// Drive the output port.
		getActivityContext().setState(getSwitch().getState(), inVal, events);
	}
	
	
	protected Port getInputPort() { return getSwitch().getInputPort(); }
	protected Port getControlPort() { return getSwitch().getControlPort(); }

	
	protected AbstractSwitch getSwitch() { return (AbstractSwitch)(getActivity()); }
}

