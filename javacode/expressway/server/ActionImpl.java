/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.server.MotifElement.*;
import expressway.server.ModelElement.ModelDomain;
import expressway.ser.*;
import expressway.common.ModelAPITypes.*;
import generalpurpose.TreeSetNullDisallowed;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;


public class ActionImpl extends MenuItemBase implements Action
{
	private static final SortedSet<PersistentNode> EmptyNodeSet = new TreeSetNullDisallowed<PersistentNode>();
	private String javaMethodName;
	
	
	ActionImpl(String text, MenuOwner mo, MenuOwner definedBy)
	{
		super(text, mo, definedBy);
	}
	
	
	public String getTagName() { return "action"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ActionIconImageName);
	}
	
	
	public Class getSerClass() { return ActionSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		
		// Set Ser fields.
		
		ActionSer ser = (ActionSer)nodeSer;
		
		ser.javaMethodName = javaMethodName;
		
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ActionImpl newInstance = (ActionImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		
		writer.println(indentString + "<menuitem text=\"" + this.getText() +
			" \" action=\"" + (javaMethodName == null ? "" : javaMethodName) + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "/>");
	}
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{}	
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{}
	
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		this.javaMethodName = null;
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		Attribute attr = constructAttribute(name);
		attr.setDefaultValue(defaultValue);
		return attr;
	}
	

	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new ActionImpl(name, this, this);
	}
	
	
	public Attribute constructAttribute(String name)
	{
		return new ActionAttribute(name);
	}
	
	
	public class ActionAttribute extends PersistentNodeImpl implements Attribute
	{
		private AttributeValidator validator = null;
		private Serializable defaultValue;
		//private Serializable value;
		
		
		ActionAttribute(String name)
		{
			super(ActionImpl.this);
			try { setName(ActionImpl.this.createUniqueChildName(name)); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
		}
		
		
		ActionAttribute(String name, Serializable defaultValue)
		{
			this(name);
			this.defaultValue = defaultValue;
			//this.value = defaultValue;
		}
		
		
		public String getTagName() { return "attribute"; }
		
		
		public Class getSerClass() { return AttributeSer.class; }
	
	
		public void prepareForDeletion()
		{
			super.prepareForDeletion();
		}
		
		public void setIconImageToDefault() throws IOException
		{
			setIconImage(NodeIconImageNames.AttributeIconImageName);
		}
		
		public int getNoOfHeaderRows() { return 0; }
		
		public SortedSet<PersistentNode> getPredefinedNodes()
		{
			return new TreeSetNullDisallowed<PersistentNode>();
		}
		
		public double getPreferredWidthInPixels() { return 50; }
		public double getPreferredHeightInPixels() { return 50; }
		
		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{
			return new ActionAttribute(name, defaultValue);
		}
		
		public Attribute constructAttribute(String name)
		{
			return new ActionAttribute(name);
		}
		
		public NodeSer externalize(NodeSer nodeSer) throws ParameterError
		{
			AttributeSer ser = (AttributeSer)nodeSer;
			ser.setDefaultValue(defaultValue);
			return super.externalize(ser);
		}
		
		/* From Attribute */
		
		public void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
		throws
			IOException
		{
			AttributeHelper.writeAsXML(this, writer, indentation, appliesTo);
		}
		
		public void setValidator(AttributeValidator validator) { this.validator = validator; }
		
		public AttributeValidator getValidator() { return validator; }
	
		public void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (validator != null) validator.validate(newValue);
		}
		
		public Serializable getDefaultValue() { return defaultValue; }
		
		public void setDefaultValue(Serializable defaultValue)
		throws
			ParameterError
		{
			this.defaultValue = defaultValue;
		}
		
		public void setValue(Scenario scenario, Serializable value)
		throws
			ParameterError
		{
			scenario.setAttributeValue(this, value);
		}
		
		
		public Serializable getValue(Scenario scenario)
		throws
			ParameterError
		{
			return scenario.getAttributeValue(this);
		}
		
		//public void setValue(Serializable value)
		//throws
		//	ParameterError
		//{
		//	this.value = value;
		//}
		
		
		//public Serializable getValue()
		//throws
		//	ParameterError
		//{
		//	return this.value;
		//}
	}
	
	
	public SortedSet<PersistentNode> getPredefinedNodes() { return EmptyNodeSet; }
	
	
	public ModelDomain getModelDomain() { return (ModelDomain)(getDomain()); }
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public String getJavaMethodName() { return javaMethodName; }
	
	public void setJavaMethodName(String name) { this.javaMethodName = name; }
}

