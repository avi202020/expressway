/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import java.io.Serializable;



/**
 * Not a persistent class.
 */

public class ActivityContextImpl extends ModelContextImpl implements ActivityContext
{
	ActivityContextImpl(SimulationRunImpl simRun, 
		ModelComponent component)
	{
		super(simRun, component);
	}


	public Serializable getStateValue(State state)
	{
		return getSimulationRun().getStateValueAtEpoch(
			state, getCurrentSimulationEpoch());
	}
}

