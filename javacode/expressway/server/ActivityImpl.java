/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.Event.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.*;


/**
 * The concrete type for user-defined Activities.
 */

public class ActivityImpl extends ActivityBase
	implements MenuOwner, ModelTemplate, TemplateInstance
{
	public ActivityImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		setResizable(true);
		setUseBorderWhenIconified(true);
	}


	public ActivityImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		setResizable(true);
		setUseBorderWhenIconified(true);
	}
	
	
	public String getTagName() { return "activity"; }
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		//for (MenuItem mi : menuItems) deleteMenuItem(mi);
		this.setHTMLDescription(null);
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		//if (modelTemplate != null) modelTemplate.templateInstanceDeleted(this);
		
		// Nullify all references.
		//menuItems = null;
		//modelTemplate = null;
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		return super.externalize(nodeSer);
	}


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ActivityImpl newInstance = (ActivityImpl)(super.clone(cloneMap, cloneParent));
		return newInstance;
	}
	

	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.println(indentString + "<activity name=\"" + this.getName() 
			+ "\" width=\"" + this.getWidth() 
			+ "\" height=\"" + this.getHeight() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\">");
		
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		
		// Write MenuItems, if any.
		
		List<MenuItem> mis = getMenuItems();
		if (mis != null)
		{
			for (MenuItem mi : mis) mi.writeAsXML(writer, indentation+1);
		}
		
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		
		// Write each Port.
		
		List<Port> ports = this.getPorts();
		for (Port port : ports) port.writeAsXML(writer, indentation+1);
		
		
		// Write each State.
		
		List<State> states = this.getStates();
		for (State state : states) state.writeAsXML(writer, indentation+1);
		

		// Write each child component.
		
		Set<ModelComponent> children = this.getSubcomponents();
		for (ModelComponent child : children)
		{
			writer.println();
			child.writeAsXML(writer, indentation+1);
		}
		
			
		writer.println(indentString + "</activity>");
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		return super.getChild(name);
	}
	
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		return super.getChildren();
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		super.deleteChild(child, outermostAffectedRef);
	}

	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	
	
	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		ActivityImpl act = new ActivityImpl(this, name);
		act.setReasonableLocation();
		return act;
	}
		
		
	public boolean isOwnedByActualTemplateInstance()
	{
		return super.isOwnedByActualTemplateInstance();
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ActivityIconImageName);
	}
	
	
	public List<String> getInstantiableNodeKinds()
	{
		List<String> names = new Vector<String>(super.getInstantiableNodeKinds());
		
		return names;
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
  /* From TemplateInstance */
	

	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		getModelContainer().initActivity(this, nativeImplClass, layoutBound,
			outermostAffectedRef);
	}
}

