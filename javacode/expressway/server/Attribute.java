/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.server;


import expressway.common.ModelAPITypes.*;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;


public interface Attribute extends PersistentNode
{
	Serializable getDefaultValue();
	
	
	void setDefaultValue(Serializable defaultValue)
	throws
		ParameterError;  // if unable to copy the value (e.g., not cloneable).
	
	
	void setValue(Scenario scenario, Serializable value)
	throws
		ParameterError;  // if unable to copy the value (e.g., not cloneable).
	
	
	Serializable getValue(Scenario scenario)
	throws
		ParameterError;  // if the Sceanrio is not a Scenario of the Attribute's Domain.
	
	
	/*
	 * Set the value of this Attribute. If the Domain has Scenarios, set the
	 * Attribute's value for each Scenario.
	 *
	 
	void setValue(Serializable value)
	throws
		ParameterError;  // if unable to copy the value (e.g., not cloneable).
	
	
	Serializable getValue()
	throws
		ParameterError;  // if the Attribute value is only defined in the context
			// of a Scenario.*/


	void setValidator(AttributeValidator validator);
	
	
	AttributeValidator getValidator();
	
	
	void validate(Serializable newValue)
	throws
		ParameterError;  // if newValue is not valid for this Attribute.


	/**
	 * Write the Attribute as a 'for' Attribute: that is, the Attribute
	 * applies to the specified pre-defined child of the containing Node.
	 */
	
	void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
	throws
		IOException;
}

