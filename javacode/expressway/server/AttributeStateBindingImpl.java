/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.commons.math.stat.descriptive.SummaryStatisticsImpl;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;


public class AttributeStateBindingImpl extends ModelElementImpl
	implements AttributeStateBinding, Cloneable
{
	private ModelAttribute attribute = null;  // parent.
	private State state = null;  // must be in a different Model Domain.
	
	
	public Class getSerClass() { return AttributeStateBindingSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((AttributeStateBindingSer)nodeSer).attributeNodeId = getNodeIdOrNull(attribute);
		((AttributeStateBindingSer)nodeSer).stateNodeId = getNodeIdOrNull(state);
		((AttributeStateBindingSer)nodeSer).stateFullName = 
			(state == null ? null : state.getFullName());
		((AttributeStateBindingSer)nodeSer).stateDomainId = 
			(state == null ? null : state.getModelDomain().getNodeId());
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		AttributeStateBindingImpl newInstance = (AttributeStateBindingImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.attribute = (ModelAttribute)(cloneMap.getClonedNode(attribute));
		newInstance.state = (State)(cloneMap.getClonedNode(state));
		
		return newInstance;
	}
	

	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<state_binding "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ (state == null ? "" : ("state=\"" + state.getFullName())) + "\">"); // fully qualified
		
		writer.println(indentString + "</state_binding>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		if (state != null) state.attributeStateBindingDeleted(this);
		this.getModelDomain().attributeStateBindingDeleted(this);
		
		// Nullify all references.
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = super.setNameAndNotify(newName);
		
		if (state != null) state.attributeStateBindingNameChanged(this, oldName);
		this.getModelDomain().attributeStateBindingNameChanged(this, oldName);
		
		return oldName;
	}
	

	public AttributeStateBindingImpl(ModelAttribute attribute, State state)
	throws
		ParameterError
	{
		super(attribute.getName() + "_binding", attribute, attribute.getModelDomain());
		//this.kind = kind;

		setResizable(false);

		ModelDomain aDomain = attribute.getModelDomain();
		ModelDomain sDomain = state.getModelDomain();

		if (aDomain == sDomain) throw new ParameterError(
			"Attempt to create an Attribute/State binding within the same model Domain.");

		this.attribute = attribute;
		this.state = state;
		
		setSizeToStandard();
	}
	
	
	public String getTagName() { return "state_binding"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.AttributeStateBindingIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }

	private static final double PreferredWidthInPixels = 10.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public ModelAttribute getAttribute()
	throws
		ValueNotSet
	{
		if (attribute == null) throw new ValueNotSet("Field 'attribute' for '" + this.getFullName() + "'");
		return attribute;
	}


	public State getForeignState()
	throws
		ValueNotSet
	{
		if (state == null) throw new ValueNotSet("Field 'state' for '" + this.getFullName() + "'");
		return state;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
		setSizeToStandard();
	}
}
