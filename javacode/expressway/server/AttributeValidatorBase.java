/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.PersistentNode.AttributeValidator;


public abstract class AttributeValidatorBase implements AttributeValidator, Cloneable
{
	public AttributeValidator makeCopy()
	throws
		CloneNotSupportedException
	{
		return (AttributeValidator)(clone());
	}
	
	protected Object clone() throws CloneNotSupportedException { return super.clone(); }
}

