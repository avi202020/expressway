/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import generalpurpose.TreeSetNullDisallowed;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


public abstract class BasicAttribute extends PersistentNodeImpl implements Attribute
{
	private Serializable defaultValue;
	//private Serializable value;
	private AttributeValidator validator;
	
	
	public BasicAttribute(String name, DecisionElementImpl parent, Serializable defaultValue)
	throws
		ParameterError
	{
		super(parent);
		parent.checkForUniqueness(name);
		this.setName(name);
		this.defaultValue = defaultValue;
	}
	
	
	public void prepareForDeletion()
	{
		super.prepareForDeletion();
	}
		

  /* ***************************************************************************
   * From Attribute:
   */


	public Serializable getDefaultValue()
	{
		return defaultValue;
	}
	
	
	public void setDefaultValue(Serializable defaultValue)
	throws
		ParameterError
	{
		this.defaultValue = defaultValue;
	}
	
	
	public void setValue(Scenario scenario, Serializable value)
	throws
		ParameterError
	{
		scenario.setAttributeValue(this, value);
	}
	
	
	public Serializable getValue(Scenario scenario)
	throws
		ParameterError
	{
		return scenario.getAttributeValue(this);
	}
	
	
	//public void setValue(Serializable value)
	//throws
	//	ParameterError
	//{
	//	this.value = value;
	//}
	
	
	//public Serializable getValue()
	//throws
	//	ParameterError
	//{
	//	return this.value;
	//}


	public void setValidator(AttributeValidator validator)
	{
		this.validator = validator;
	}
	
	
	public AttributeValidator getValidator()
	{
		return validator;
	}
	
	
	public void validate(Serializable newValue)
	throws
		ParameterError
	{
		if (validator == null) throw new RuntimeException("No Validator set for Attribute '" +
			getFullName() + "'");
		
		validator.validate(newValue);
	}


	public void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
	throws
		IOException
	{
		if (this.isPredefined()) return;

		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		if (this.getName() == null) throw new IOException(new Exception(
			"Attribute name is null; parent is " + this.getParent().getFullName()));
		
		writer.println(indentString + "<attribute name=\"" + this.getName() + "\" "
			+ (appliesTo == null? "" : "for=\"" + appliesTo.getName() + "\" ")
			+ (defaultValue == null? "" : "default=\"" + defaultValue + "\" ")
			+ (defaultValue == null? "" : 
				(isParsableJavaType(defaultValue) ? "" : "default_type=\"" 
					+ defaultValue.getClass().getName() + "\" "))
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
			
		
		// Write each Attribute of this Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</attribute>");
	}
	
	
	/**
	 * Return true if the specified Object is of a type that is allowed in a
	 * DecisionAttribute expression.
	 */
	 
	protected boolean isParsableJavaType(Object obj)
	{
		return (obj instanceof String) || (obj instanceof Number) || (obj instanceof Character);
	}



  /* ***************************************************************************
   * From PersistentNodeImpl:
   */

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.BasicAttributeIconImageName);
	}


	public int getNoOfHeaderRows() { return 1; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes() { return new TreeSetNullDisallowed<PersistentNode>(); }
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 50; }
}

