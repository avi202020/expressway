/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;


public class ChoiceImpl extends DecisionImpl implements Choice
{
	public ChoiceImpl(DecisionScenario scenario, Serializable value)
	throws
		CloneNotSupportedException
	{
		super(scenario, value);
		setResizable(false);
	}
	

	public Class getSerClass() { return ChoiceSer.class; }
	
	
	public String getTagName() { return "choice"; }


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ChoiceImpl newInstance = (ChoiceImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		Variable variable = null;
		try { variable = getVariable(); }
		catch (ValueNotSet w) {}
		DecisionPoint dp = null;
		try { dp = getDecisionPoint(); }
		catch (ValueNotSet w) {}
		
		writer.println(indentString + "<choice name=\"" + this.getName() + "\" "
			+ (dp == null ? "" : 
				("decis_point=\"" + dp.getName() + "\" "))
			+ (variable == null ? "" : ("variable=\"" + variable.getName() + "\" "))
			+ "value=\"" + value + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		writer.println(indentString + "</choice>");
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ChoiceIconImageName);
	}
}
