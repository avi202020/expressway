/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import geometry.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.common.ClientModel.InconsistencyError;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.Event.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.*;


public class ConduitImpl extends ModelComponentImpl
	implements Conduit, MenuOwner, ModelTemplate, TemplateInstance
{
	/** Ownership.
		In coordinate space of this Conduit. */
	private InflectionPoint[] inflectionPoints = new InflectionPoint[0];
	
	/** Reference only. The source connection.*/
	private Port p1;
	
	/** Reference only. The destination connection.*/
	private Port p2;


	public static final double HalfPi = Math.PI / 2.0;


	public ConduitImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		
		setResizable(false);
		setDimensionsBasedOnParent();
	}
	
	
	public String getTagName() { return "conduit"; }
	
	
	public Class getSerClass() { return ConduitSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Determine starting point and ending point of this Conduit, in the
		// reference frame of the Conduit.

		PointImpl p1Location = null; // location in frame of this Conduit.
		try { p1Location = translatePortLocationToConduit(p1); }
		catch (ParameterError pe) {} // ok

		PointImpl p2Location = null; // location in frame of this Conduit.
		try { p2Location = translatePortLocationToConduit(p2); }
		catch (ParameterError pe) {} // ok
		
		// Set Ser fields.
		
		((ConduitSer)nodeSer).p1NodeId = this.getNodeIdOrNull(p1);
		((ConduitSer)nodeSer).p2NodeId = this.getNodeIdOrNull(p2);
		((ConduitSer)nodeSer).inflectionPoints = this.getInflectionPoints();
		((ConduitSer)nodeSer).p1Location = p1Location;
		((ConduitSer)nodeSer).p2Location = p2Location;
		((ConduitSer)nodeSer).htmlDescription = this.getHTMLDescription();
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ConduitImpl newInstance = (ConduitImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.p1 = cloneMap.getClonedNode(p1);
		newInstance.p2 = cloneMap.getClonedNode(p2);
		
		newInstance.inflectionPoints = PointImpl.clonePointArray(inflectionPoints);
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		String p1RelativePath = null;
		String p2RelativePath = null;
		try
		{
			if (p1 != null) p1RelativePath = p1.getNameRelativeTo(getContainer());
			if (p2 != null) p2RelativePath = p2.getNameRelativeTo(getContainer());
		}
		catch (ParameterError pe) { throw new IOException(pe); }
		
		
		writer.println(indentString + "<conduit name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ (p1 == null ? "" : ("portA=\"" + p1RelativePath + "\" "))
			+ (p2 == null ? "" : ("portB=\"" + p2RelativePath + "\" "))
			+ ">");
		
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		
		// Write MenuItems, if any.
		
		List<MenuItem> mis = getMenuItems();
		if (mis != null)
		{
			for (MenuItem mi : mis) mi.writeAsXML(writer, indentation+1);
		}
		
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		
		writer.println(indentString + "</conduit>");
	}


	public void renderAsChild(Graphics2D g, double pixPerModel)
	{
		g.setColor(Color.blue);
		
		double xPrior = 0.0;
		double yPrior = 0.0;

		if (p1 != null)
		{
			xPrior = p1.getX();
			yPrior = p1.getY();
		}
		
		boolean firstTime = true;
		for (InflectionPoint ip : inflectionPoints)
		{
			if ((firstTime && (p1 != null)) || (! firstTime))
				g.drawLine((int)(xPrior * pixPerModel), (int)(yPrior * pixPerModel), 
					(int)(ip.getX() * pixPerModel), (int)(ip.getY() * pixPerModel));
			
			xPrior = ip.getX();
			yPrior = ip.getY();
			firstTime = false;
		}
		
		if (p2 != null && ((p1 != null) || (firstTime == false)))
			g.drawLine((int)(xPrior * pixPerModel), (int)(yPrior * pixPerModel),
				(int)(p2.getX() * pixPerModel), (int)(p2.getY() * pixPerModel));
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		//for (MenuItem mi : menuItems) deleteMenuItem(mi);
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		//menuItems = null;
		inflectionPoints = null;
		setHTMLDescription(null);
		//modelTemplate = null;
	}
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		return super.getChild(name);
	}
	
	
	public SortedSet<PersistentNode> getChildren()
	{
		return super.getChildren();
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		super.deleteChild(child, outermostAffectedRef);
	}

	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		//if (this.modelTemplate != null) return true;
		return super.isOwnedByActualTemplateInstance();
	}
	
	
	/** For use by PersistentNodeImpl.clone(). */
	//ConduitImpl() { this(null, null, null); }


	public int getNoOfHeaderRows() { return 0; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ConduitIconImageName);
	}

	
	public void setSizeToStandard() {}
	

	public double getPreferredWidthInPixels() { throw new RuntimeException(
		"Should not be called"); }
	public double getPreferredHeightInPixels() { throw new RuntimeException(
		"Should not be called"); }
	
	
	/**
	 * Set the dimensions of this Conduit so that it is placed exactly over the
	 * non border area of its parent Container. This method should be called
	 * whenever the parent Container's geometry changes.
	 */
	 
	void setDimensionsBasedOnParent()
	{
		if (this.display == null) throw new RuntimeException();
		if (this.getParent() == null) throw new RuntimeException(
			"parent of " + this.getName() + " is null");
		
		// The following statement is here for testing only and should be commented out:
		//this.display = new FinalDisplay(0, 0, this.parent.getScale(), 0.0);
			
		this.setWidth(this.getParent().getWidth());
		this.setHeight(this.getParent().getHeight());
	}
	
	
	/** Used for testing only. May be commented out. 
	static class FinalDisplay extends DisplayImpl
	{
		public FinalDisplay(double x, double y, double scale, double orientation)
		{
			this.x = x;
			this.y = y;
			this.scale = scale;
			this.orientation = orientation;
		}
		
		public void setX(double x) { throw new RuntimeException("Cannot change Display"); }
		public void setY(double y) { throw new RuntimeException("Cannot change Display"); }
		public void setScale(double scale) { throw new RuntimeException("Cannot change Display"); }
		public void setOrientation(double angle) { throw new RuntimeException("Cannot change Display"); }
	}*/


	public void setPort1(Port p)
	{
		this.p1 = p;
		if (p1 != null) this.p1.addSourceConnection(this);
	}
	
	
	public void setPort2(Port p)
	{
		this.p2 = p;
		if (p2 != null) this.p2.addDestinationConnection(this);
	}


	public ModelContainer getContainer() { return (ModelContainer)(this.getParent()); }
	
	
	public void disconnectFromPort(Port p)
	{	
		if (p1 != null)
			if (p1 == p)
			{
				p.removeSourceConnection(this);
				p1 = null;
			}
		
		if (p2 != null)
			if (p2 == p)
			{
				p.removeDestinationConnection(this);
				p2 = null;
			}
	}
	

	public Port getPort1() throws ValueNotSet
	{
		if (p1 == null) throw new ValueNotSet("Field 'p1' for '" + getFullName() + "'");
		return p1;
	}


	public Port getPort2() throws ValueNotSet
	{
		if (p2 == null) throw new ValueNotSet("Field 'p2' for '" + getFullName() + "'");
		return p2;
	}


	public void setInflectionPoints(InflectionPoint[] ipArray)
	{
		this.inflectionPoints = ipArray;
	}


	public InflectionPoint[] getInflectionPoints()
	{
		return inflectionPoints;
	}
	
	
	public InflectionPoint insertInflectionPointAt(double x, double y, int position)
	throws
		ParameterError  // if the inflection point is not inside this Conduit's Container.
	{
		if (inflectionPoints.length < position) throw new RuntimeException(
			"Attempt to insert an InflectionPoint at the position " + position +
			" when there are only " + inflectionPoints.length + " Inflection Points.");
			
		// Check if the new Inflection Point will result in any segments that overlap
		// other Conduits. If so, adjust the Inflection Point so that no overlap will occur.
		
		double ipx = x;
		double ipy = y;
		
		// Create a new array to serve as the new InflectionPoint list for this Conduit.
		InflectionPoint[] ips = new InflectionPoint[inflectionPoints.length + 1];
		
		// Copy existing Inflection Point references to the new array, up to
		// position 'position'.
		for (int ipPos = 0; ipPos < position; ipPos++)
			ips[ipPos] = inflectionPoints[ipPos];
		
		// Insert a new Inflection Point at the specified position.
		InflectionPoint newIp = new InflectionPointImpl(ipx, ipy);
		ips[position] = newIp;
		
		// Copy remaining Inflection Point references to the new array.
		for (int ipPos = position+1; ipPos < ips.length; ipPos++)
			ips[ipPos] = inflectionPoints[ipPos-1];
		
		// Set this Conduit's array of Inflection Points to point to the new array.
		inflectionPoints = ips;
		
		return newIp;
	}
	
	
	public void deleteInflectionPoint(InflectionPoint point)
	throws
		ValueNotSet,
		ParameterError
	{
		boolean found = false;
		Set<InflectionPoint> newInflectionPoints = new HashSet<InflectionPoint>();
		
		for (InflectionPoint p : inflectionPoints)
		{
			if (p == point) found = true;  // don't add it.
			else newInflectionPoints.add(p);
		}
		
		if (! found) throw new ParameterError("Did not find Inflection Point.");
		
		this.inflectionPoints = newInflectionPoints.toArray(new InflectionPoint[1]);
	}
	
	
	/**
	 * Compute the location of each Port, translated into the coordinate system
	 * of this Conduit. Assume that the Ports are legally connected.
	 * 
	 * If the Port's owner is the same as this
	 * Conduit's owner, the Port's owner will be full size (not iconified)
	 * and p1Location will be set accordingly; but if the Port's owner is internal
	 * to this Conduit's owner, then the Port's owner will be iconified and p1Location
	 * will be set accordingly.
	 *
	 * See the slide "Conduits".
	 *
	 * See also ModelContainerImpl.portsMayBeConnected(...).
	 */
	
	PointImpl translatePortLocationToConduit(Port p)
	throws
		ParameterError
	{
		if (p == null) throw new ParameterError("Port argument is null");
		PortedContainer portContainer = p.getContainer();
		ModelContainer condContainer = this.getContainer();
		
		if (portContainer == condContainer) 
			// same Container: Port belongs to the Conduit's Container, and
			// therefore in the same coordinate space.
			return (PointImpl)(p.getDisplay());
		
		// The Conduit is external to the Port's Container.
		// Transform to coordinate space of Conduit's Container.
		
		PointImpl point = (PointImpl)(portContainer.translateToContainer(p.getLocationInIcon()));
		
		return point;
	}
	
	
	Point[] translatePortLocationsToConduit()
	throws
		ParameterError
	{
		Point[] points = new Point[2];
		
		points[0] = translatePortLocationToConduit(p1);
		points[1] = translatePortLocationToConduit(p2);
		
		return points;
	}
	
	
	/** ************************************************************************
	 * For algorithm, see the slide "Repositioning Conduits on the Server".
	 */
	
	public void reroute()
	throws
		ParameterError
	{
		if ((p1 == null) || (p2 == null)) return;
		
		//if (getParent() instanceof ModelDomain)
		//	(new Exception("rerouting " + this.getName() + " in Domain")).printStackTrace();
		
		// Delete the Inflection Points currently defined for this Conduit.
		
		this.inflectionPoints = new InflectionPoint[0];
		
		
		// Don't bother to route inside zero-sized Components.
		ModelContainer conduitContainer = this.getContainer();
		if (conduitContainer.getWidth() == 0.0) return;
		if (conduitContainer.getHeight() == 0.0) return;
		
		
		// Translate the coordinates of each Port to this Conduit's Model Container.
		// Note that Inflection Points are expressed in the coordinate space of
		// the Conduit's Container.
		
		Point[] portLocationsInContainer = translatePortLocationsToConduit();
		
		Point p1InContainer = portLocationsInContainer[0];
		Point p2InContainer = portLocationsInContainer[1];
		
		if (p1InContainer == null) throw new RuntimeException();
		if (p2InContainer == null) throw new RuntimeException();
		
		final double p1x = p1InContainer.getX();
		final double p1y = p1InContainer.getY();
		final double p2x = p2InContainer.getX();
		final double p2y = p2InContainer.getY();


		/*	********************************************************************
			1. Try to connect with one Segment.
		
			Required condition: both Ports share a coordinate.
			*/
			
		//GlobalConsole.println("Trying one segment");
		
		double Tolerance = 0.1;  // Allow 10% resilience to misalignment of components.
		
		double pxTolerance = Math.abs(p1x + p2x) * Tolerance / 2.0;
		double pyTolerance = Math.abs(p1y + p2y) * Tolerance / 2.0;
		
		if 
		(
			((Math.abs(p1x - p2x) < pxTolerance) || (Math.abs(p1y - p2y) < pyTolerance))
			&&
			PortImpl.portsFaceEachOther(p1, p2) 
		)
		{
			// Ports can be connected with a single horizontal or vertical segment.
			// No need to create any Inflection Points.
			
			//GlobalConsole.println("No need to create any Inflection Points.");
			return;
		}
		
		
		/*	********************************************************************
			2. Try to connect with two Segments
			
			Required condition: Rays projected from each Port (perpendicular to
			the Port's face) will intersect. If a Port is on the Conduit's Container,
			the Ray should point inward; otherwise it should point outward.
			*/
		
		//GlobalConsole.println("Trying two segments");
		
		Ray ray1;
		Ray ray2;
		try
		{
			Face face1 = this.p1.getContainer().getFace(this.p1, this.getParent() != this.p1.getContainer());
			Face face2 = this.p2.getContainer().getFace(this.p2, this.getParent() != this.p2.getContainer());

			
			// Create an outward Ray from each Port's Face, with origin at each Port.
			ray1 = Ray.createRay(face1.createOutwardRay(), p1InContainer);
			ray2 = Ray.createRay(face2.createOutwardRay(), p2InContainer);
			
			// Make Rays that are on the Conduit's Container's edge point inward.
			if (conduitContainer instanceof PortedContainer)
			{
				PortedContainer portedContainer = (PortedContainer)conduitContainer;
				
				if (portedContainer.getPorts().contains(p1))
				{
					ray1.reverse();
				}
				
				if (portedContainer.getPorts().contains(p2))
				{
					ray2.reverse();
				}
			}
			
			Point intersection = null;
			try { intersection = Ray.findIntersection(ray1, ray2); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
				return;
			}
			
			
			if (intersection != null)
			{
				// Create one Inflection Point, at the intersection Point.
	
				this.insertInflectionPointAt(//p1InContainer, p2InContainer,
					intersection.getX(), intersection.getY(), 0);
				
				return;
			}
		}
		catch (Exception ex) { throw new ParameterError(ex); }
		
		
		/*	********************************************************************
			3. Try to connect with three Segments.
			
			Required condition: Lines projected from each Port are parallel.
			*/
			
		double rayDotProduct = Ray.dot(ray1, ray2);
		
		try
		{
			if (Math.abs(rayDotProduct - (-1.0)) < Tolerance)
			{
				// The two Faces face each other.
				// Create two Inflection Points, to arrange the segments in an "S" shape.
			
				Point midpoint = 
					PointImpl.findMidpoint(p1InContainer, p2InContainer);
					
				Point pA = ray1.findClosestPoint(midpoint);
				Point pB = ray2.findClosestPoint(midpoint);
				
				double pax = pA.getX();
				double pay = pA.getY();
				double pbx = pB.getX();
				double pby = pB.getY();
				
				VisualGuidance guidance = this.getModelDomain().getVisualGuidance();
				double delta = guidance.getProbablePixelSize() * 100.0;
				
				if (pax == pbx)
				{
					delta *= (pay / this.getContainer().getHeight());
					pax += delta;
					pbx += delta;
				}
				
				if (pay == pby)
				{
					delta *= (pax / this.getContainer().getWidth());
					pay += delta;
					pby += delta;
				}
				
				this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
					pax, pay, 0);
				this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
					pbx, pby, 1);
				
				return;
			}
			
			if (Math.abs(rayDotProduct - 1.0) < Tolerance)
			{
				// The two Faces face the same way.
				// Create two Inflection Points, to arrange the segments in a "C" shape.
				
				double MinInitialSegmentLength = 
					(p1.getContainer().getWidth() + p1.getContainer().getHeight()) / 10.0;
					
				Point pA = 
					(new Ray(ray1.getOrigin(), ray1.getRadians(), MinInitialSegmentLength))
					.getEndpoint();
					
				Point pB = 
					(new Ray(ray2.getOrigin(), ray2.getRadians(), MinInitialSegmentLength))
					.getEndpoint();
				
				
				// Ensure that the "C" does not extend past the boundary of the
				// Component that contains this Conduit.
				
				confine(pA, this.getContainer());
				confine(pB, this.getContainer());
								
				
				// Insert the Iflection Points into this Conduit.
				
				this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
					pA.getX(), pA.getY(), 0);
								
				this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
					pB.getX(), pB.getY(), 1);
				
				return;
			}
		}
		catch (Exception ex) { throw new ParameterError(ex); }
		finally  // diagnostic only.
		{
		}
		
		
		/*	********************************************************************
			4. Try to connect with four Segments.
			
			Required condition: The prior conditions are not met, and the faces
			are perpendicular.
			*/
			
		//GlobalConsole.println("Trying four segments");
		
		if (Math.abs(rayDotProduct) < Tolerance) try
		{
			// Create a Segment projecting from one Port, of length at least equal
			// to a standard minimum amount. If the Segment's length
			// spans any portion of the other Port's Container (in the coordinate of
			// the length of the Segment), then make sure that the Segment extends
			// past the other Port's Container � at least half the width of that
			// Container. Place an Inflection Point at that location. Do the same
			// for the other Port. Now extend a Segment from each Inflection Point,
			// perpendicular to each Segment at that point, and join these by an
			// Inflection Point where they intersect.
		
			// Determine first Inflection Point.
			
			double MinInitialSegmentLength = 
				(p1.getContainer().getWidth() + p1.getContainer().getHeight()) / 10.0;
				
			Point ipA   // first Inflection Point.
				= ray1.scalarMult(MinInitialSegmentLength).getEndpoint();
			
			confine(ipA, this.getContainer());

			//GlobalConsole.println("First segment: " + p1InContainer.toString() +
			//	", " + ipA.toString());
				
				
			// Determine second Inflection Point. The segment will be at a right
			// angle relative to the first segment.
			
			Ray rayAt90Degrees = new Ray(ipA, ray1.getRadians() + HalfPi);
			Ray rayAt270Degrees = new Ray(ipA, ray1.getRadians() - HalfPi);
			
			Point closestPointFor90 = rayAt90Degrees.findClosestPoint(p2InContainer);
			Point closestPointFor270 = rayAt270Degrees.findClosestPoint(p2InContainer);
			
			//GlobalConsole.println("rayAt90Degrees=" + rayAt90Degrees.toString());
			//GlobalConsole.println("closestPointFor90=" + closestPointFor90);
			//GlobalConsole.println("closestPointFor270=" + closestPointFor270);
			
			
			double distFor90 = PointImpl.findDistance(closestPointFor90, p2InContainer);
			double distFor270 = PointImpl.findDistance(closestPointFor270, p2InContainer);
			
			Ray rayB;
			
			if (distFor90 > distFor270)
				rayB = rayAt270Degrees;
			else
				rayB = rayAt90Degrees;
			
			// Extend the second segment slightly.
			
			Ray rayBetweenPorts = new Ray(p1InContainer, p2InContainer);
			double d = Ray.dot(rayBetweenPorts, rayB);  // find distance along the
				// direction of rayB.
				
			d /= 2.0;  // take half the distance.
			if (d < 0.0)
				if (distFor90 == distFor270) d = -d;
				else
				{
					//((15, 0), -pi).findClosestPoint(0, 30) should yield (0, 0)
					GlobalConsole.println("((15, 0), -pi).findClosestPoint(0, 30)=" +
						(new Ray(new PointImpl(15.0, 0.0), -Math.PI)).findClosestPoint(
							new PointImpl(0.0, 30.0)));
					GlobalConsole.println(
						rayAt270Degrees + ".findClosestPoint(" + p2InContainer + ")=" +
						rayAt270Degrees.findClosestPoint(p2InContainer));
					GlobalConsole.println("Conduit is " + getFullName());
					GlobalConsole.println("Container at " + getContainer().getX() + ", " + getContainer().getY());
					GlobalConsole.println("Container's width=" + getContainer().getWidth());
					GlobalConsole.println("Container's height=" + getContainer().getHeight());
					GlobalConsole.println("p1's container dimensions: " + p1.getContainer().getWidth()
						+ ", " + p1.getContainer().getHeight());
					GlobalConsole.println("p2's container dimensions: " + p2.getContainer().getWidth()
						+ ", " + p2.getContainer().getHeight());
					GlobalConsole.println("p1's container location: " + p1.getContainer().getX()
						+ ", " + p1.getContainer().getY());
					GlobalConsole.println("p2's container location: " + p2.getContainer().getX()
						+ ", " + p2.getContainer().getY());
					GlobalConsole.println(p1);
					GlobalConsole.println(p2);
					GlobalConsole.println("p1InContainer=" + p1InContainer);
					GlobalConsole.println("p2InContainer=" + p2InContainer);
	
					GlobalConsole.println("MinInitialSegmentLength=" + MinInitialSegmentLength);
					GlobalConsole.println("ipA=" + ipA);
					GlobalConsole.println("rayAt90Degrees=" + rayAt90Degrees);
					GlobalConsole.println("rayAt270Degrees=" + rayAt270Degrees);
					GlobalConsole.println("closestPointFor90=" + closestPointFor90);
					GlobalConsole.println("closestPointFor270=" + closestPointFor270);
					GlobalConsole.println("distFor90=" + distFor90);
					GlobalConsole.println("distFor270=" + distFor270);
					GlobalConsole.println("rayB=" + rayB);
					GlobalConsole.println("rayBetweenPorts=" + rayBetweenPorts);
					GlobalConsole.println("d=(rayBetweenPorts dot rayB)/2 = " + d);
				}
			
			rayB.setMagnitude(d);
						
			Point ipB = rayB.getEndpoint();
			confine(ipB, this.getContainer());
			
			
			// Determine third Inflection Point.
			
			rayAt90Degrees = new Ray(ipB, rayB.getRadians() + HalfPi);
			rayAt270Degrees = new Ray(ipB, rayB.getRadians() - HalfPi);
			
			closestPointFor90 = rayAt90Degrees.findClosestPoint(p2InContainer);
			closestPointFor270 = rayAt270Degrees.findClosestPoint(p2InContainer);
			
			distFor90 = PointImpl.findDistance(closestPointFor90, p2InContainer);
			distFor270 = PointImpl.findDistance(closestPointFor270, p2InContainer);			
			
			Point ipC;
			
			if (distFor90 > distFor270)
				ipC = closestPointFor270;
			else
				ipC = closestPointFor90;
			
			confine(ipC, this.getContainer());
			
			
			// Create actual Inflection Point instances.
			
			this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
				ipA.getX(), ipA.getY(), 0);
			
			this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
				ipB.getX(), ipB.getY(), 1);
			
			this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
				ipC.getX(), ipC.getY(), 2);

			
			return;
		}
		catch (Exception ex) { throw new ParameterError(ex); }
		
		
		/*	********************************************************************
			5. Try to connect with five Segments.
			*/

		//GlobalConsole.println("Trying five segments");
		
		try
		{
			// Project a Segment from each Port, of length half the width of the
			// Port's Container, and add an Inflection Point there. Extend a Segment
			// from each of these Inflection Points, perpendicular to the existing
			// Segments, and toward each other, until they share a coordinate.
			// Place Inflection Points there. Finally, join these with a Segment.
			
			// Determine first Inflection Point.
			
			double MinInitialSegmentLength = 
				(this.getWidth() + this.getHeight()) / 10.0;
				
			Point ipA = 
				ray1.scalarMult(MinInitialSegmentLength).getEndpoint();
				
			confine(ipA, this.getContainer());
			
				
			// Determine second Inflection Point.
			
			Ray rayAt90Degrees = new Ray(ipA, Math.PI * 0.5);
			Ray rayAt270Degrees = new Ray(ipA, Math.PI * 1.5);
			
			Point closestPointFor90 = rayAt90Degrees.findClosestPoint(p2InContainer);
			Point closestPointFor270 = rayAt270Degrees.findClosestPoint(p2InContainer);
			
			double distFor90 = PointImpl.findDistance(closestPointFor90, p2InContainer);
			double distFor270 = PointImpl.findDistance(closestPointFor270, p2InContainer);
			
			Ray rayB;
			
			if (distFor90 > distFor270)
				rayB = rayAt270Degrees;
			else
				rayB = rayAt90Degrees;
			
			// Extend the second segment slightly.
			
			Point ipB = rayB.getEndpoint();
			rayB.setMagnitude(PointImpl.findDistance(ipA, ipB) * 1.1);
			ipB = rayB.getEndpoint();
			confine(ipB, this.getContainer());
			
			
			// Determine third Inflection Point.
			
			rayAt90Degrees = new Ray(ipB, Math.PI * 0.5);
			rayAt270Degrees = new Ray(ipB, Math.PI * 1.5);
			
			closestPointFor90 = rayAt90Degrees.findClosestPoint(p2InContainer);
			closestPointFor270 = rayAt270Degrees.findClosestPoint(p2InContainer);
			
			distFor90 = PointImpl.findDistance(closestPointFor90, p2InContainer);
			distFor270 = PointImpl.findDistance(closestPointFor270, p2InContainer);
			
			Ray rayC;
			
			if (distFor90 > distFor270)
				rayC = rayAt270Degrees;
			else
				rayC = rayAt90Degrees;
			
			// Extend the segment slightly.
			
			Point ipC = rayC.getEndpoint();
			rayC.setMagnitude(PointImpl.findDistance(ipB, ipC) * 1.1);
			ipC = rayC.getEndpoint();
			confine(ipC, this.getContainer());
			
			
			// Determine fourth Inflection Point.
			
			rayAt90Degrees = new Ray(ipC, Math.PI * 0.5);
			rayAt270Degrees = new Ray(ipC, Math.PI * 1.5);
			
			closestPointFor90 = rayAt90Degrees.findClosestPoint(p2InContainer);
			closestPointFor270 = rayAt270Degrees.findClosestPoint(p2InContainer);
			
			distFor90 = PointImpl.findDistance(closestPointFor90, p2InContainer);
			distFor270 = PointImpl.findDistance(closestPointFor270, p2InContainer);
			
			Point ipD;
			
			if (distFor90 > distFor270)
				ipD = closestPointFor270;
			else
				ipD = closestPointFor90;
			
			confine(ipD, this.getContainer());
			
			
			// Create actual Inflection Points.
			
			this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
				ipA.getX(), ipA.getY(), 0);
			
			this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
				ipB.getX(), ipB.getY(), 1);
			
			this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
				ipC.getX(), ipC.getY(), 2);
			
			this.insertInflectionPointAt(//p1InContainer, p2InContainer, 
				ipD.getX(), ipD.getY(), 3);
			
			return;
		}
		catch (Exception ex) { throw new ParameterError(ex); }
	}
	
	
	/**
	 * If Point p is outside the bounds of the specified Model Element, adjust
	 * p so that it is within e's bounds.
	 */
	 
	static void confine(Point p, ModelElement e)
	{
		if (p == null) return;
		if (p.getX() < 0.0) p.setX(0.0);
		else if (p.getX() > e.getWidth())
			p.setX(e.getWidth());
		
		if (p.getY() < 0.0) p.setY(0.0);
		else if (p.getY() > e.getHeight())
			p.setY(e.getHeight());
	}
	
	
	
	/* *************************************************************************
	 * *************************************************************************
	 * From ModelElement.
	 */
	 
	public void dump(int indentation)
	{
		super.dump(indentation);
		
		// Generate indentation string.
		String indentString = "";
		for (int i = 1; i <= indentation; i++) indentString = indentString + "\t";

		// Print the current element.
		GlobalConsole.println("\t" + indentString + "port1: " + 
			(p1 == null ? "(null)" : p1.getFullName()) + "; port2: " +
			(p2 == null ? "(null)" : p2.getFullName()));
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
		setDimensionsBasedOnParent();
	}
	
	
	/* *************************************************************************
	 * *************************************************************************
	 * From PortChangeListener.
	 */
	 
	public void portDeleted(Port port)
	{
		if (port == null) return;
		
		disconnectFromPort(port);
	}
	
	
	public void portNameChanged(Port port, String oldName)
	{
	}



	/* *************************************************************************
	 * *************************************************************************
	 * From Object.
	 */
	 
	public String toString()
	{
		String inflectionPointStr = "";
		for (InflectionPoint ip : inflectionPoints)
		{
			inflectionPointStr += ip.toString() + " ";
		}
		
		return
			super.toString() + " p1=" + (p1 == null ? "(null)" : p1.toString()) +
			", p2=" + (p2 == null ? "(null)" : p2.toString()) +
			", inflections: " + inflectionPointStr;
	}
}
