/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;


public class ConstraintImpl extends ModelComponentImpl implements Constraint
{
	public Class getSerClass() { return ConstraintSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		//((ConstraintSer)nodeSer).parentNodeId = this.getParent().getNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ConstraintImpl newInstance = (ConstraintImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public ConstraintImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		setResizable(false);
	}


	public ConstraintImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		setResizable(false);
	}
	
	
	public String getTagName() { return "constraint"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ConstraintIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public ModelContainer getModelContainer()
	{
		return (ModelContainer)(getParent());
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
	}
}
