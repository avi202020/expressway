/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;
import java.util.Set;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;


public abstract class DecisionBase implements DecisionPoint.NativeDecisionPointImplementation
{
	private DecisionActivityContext context = null;
	private DecisionElement decisionElement = null;


	public void setDecisionElement(DecisionElement element)
	{
		this.decisionElement = element;
	}


	public DecisionElement getDecisionElement()
	{
		return decisionElement;
	}


	public void start(DecisionActivityContext context) throws Exception
	{
		this.context = context;
		if(decisionElement == null) throw new Exception(
			"decisionElement is null for DecisionPoint " +
				(decisionElement instanceof DecisionPoint ?
					((DecisionPoint)decisionElement).getName() : "(unnamed)"));
	}


	public void stop()
	{
	}
	
	
	protected DecisionActivityContext getDecisionContext() { return context; }
}
