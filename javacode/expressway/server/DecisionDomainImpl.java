/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;import expressway.common.ClientModel.*;
import expressway.server.DecisionElement.*;
import expressway.server.MotifElement.*;
import expressway.server.NamedReference.*;import expressway.server.HierarchyElement.Hierarchy;
import expressway.ser.*;
import geometry.*;import generalpurpose.SetVector;
import java.util.Set;
import java.util.List;import java.util.Vector;
import generalpurpose.TreeSetNullDisallowed;
import java.util.SortedSet;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;


public class DecisionDomainImpl extends DecisionElementImpl implements DecisionDomain
{
	private static final double PreferredWidthInPixels = 1000.0;
	private static final double PreferredHeightInPixels = 800.0;

	private DomainHelper helper;
	
	
	/** Ownership.
	 * Includes DecisionPoints, PrecursorRelationships, Dependencies.
	 */
	List<PersistentNode> listChildNodes = new Vector<PersistentNode>();


	/** Ownership. */
	//Set<DecisionPoint> decisionPoints = new TreeSetNullDisallowed<DecisionPoint>();

	/** Ownership. */
	//Set<Dependency> dependencies = new TreeSetNullDisallowed<Dependency>();

	/** Ownership. */
	//Set<PrecursorRelationship> precursorRelationships =
	//	new TreeSetNullDisallowed<PrecursorRelationship>();


	/**
	 * Constructor.
	 */

	public DecisionDomainImpl(String name)
	throws
		ParameterError
	{
		super(null);
		this.setName(name);
		this.helper = new DomainHelper(this, new SuperDomain());
	}


	public Class getSerClass() { return DecisionDomainSer.class; }
	
	
	public String getTagName() { return "decision_domain"; }
	
	
	public String getDefaultDomainViewTypeName() { return "Decision"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DecisionDomainIconImageName);
	}


  /* *************************************************************************
   * From Domain
   */
	
	
	public void setScenarios(List<Scenario> ss) { helper.setScenarios(ss); }
	
	
	public void setMenuItems(List<MenuItem> mis) { helper.setMenuItems(mis); }
	
	
	public void setMotifDefs(Set<MotifDef> mds) { helper.setMotifDefs(mds); }
	
	
	public void setScenarioSets(List<ScenarioSet> sss) { helper.setScenarioSets(sss); }
	
	
	public void setNamedReferences(Set<NamedReference> nrs) { helper.setNamedReferences(nrs); }
	
	
	public void setVisualGuidance(VisualGuidance vg) { helper.setVisualGuidance(vg); }


	public String getSourceName() { return helper.getSourceName(); }
	
	
	public void setSourceName(String name) { helper.setSourceName(name); }
	
	
	public URL getSourceURL() { return helper.getSourceURL(); }
	
	
	public void setSourceURL(URL url) { helper.setSourceURL(url); }
	
	
	public String getDeclaredName() { return helper.getDeclaredName(); }
	
	
	public void setDeclaredName(String name) { helper.setDeclaredName(name); }
	
	
	public String getDeclaredVersion() { return helper.getDeclaredVersion(); }
	
	
	public void setDeclaredVersion(String version) { helper.setDeclaredVersion(version); }
	
		
	public Set<MotifDef> getMotifDefs()
	{
		return helper.getMotifDefs();
	}
	
	
	public void addMotifDef(MotifDef md)
	{
		helper.addMotifDef(md);
	}
	
	
	public void removeMotifDef(MotifDef md)
	{
		helper.removeMotifDef(md);
	}
	
	
	public Set<NamedReference> getNamedReferences() { return helper.getNamedReferences(); }
	
	
	public NamedReference getNamedReference(String nrName)
	{
		return helper.getNamedReference(nrName);
	}

	
	public NamedReference createNamedReference(String name, String desc,
		Class fromType, Class toType)
	throws
		ParameterError
	{
		return helper.createNamedReference(name, desc, fromType, toType);
	}

	
	public void deleteNamedReference(NamedReference nr)
	{
		helper.deleteNamedReference(nr);
	}
	
	
	public boolean usesMotif(MotifDef md) { return helper.usesMotif(md); }
	

	public List<Scenario> getScenarios()
	{
		return helper.getScenarios();
	}


	public Scenario getScenario(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return helper.getScenario(name);
	}	


	public Scenario getCurrentScenario()
	{
		return helper.getCurrentScenario();
	}
	
	
	public void setCurrentScenario(Scenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		helper.setCurrentScenario(scenario);
	}


	public Scenario createScenario(String name)
	throws
		ParameterError
	{
		return helper.createScenario(name);
	}


	public Scenario createScenario()
	throws
		ParameterError
	{
		return helper.createScenario();
	}


	public Scenario createScenario(String name, Scenario scenario)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		if (scenario != null)
			if (! (scenario instanceof DecisionScenario)) throw new RuntimeException(
				"Scenario '" + scenario.getFullName() + "' is not a DecisionScenario");
		
		if (name == null) name = createUniqueChildName("Scenario");
		else if (name.equals("")) throw new ParameterError(
			"Cannot use an empty string for a Scenario name");
		else
			try
			{
				getScenario(name);
				throw new ParameterError("Domain " + this.getName() + 
					" already contains a Scenario named " + name);
			}
			catch (ElementNotFound ex) {} // ok
		
		DecisionScenario newScenario = null;

		if (scenario == null)
			newScenario = new DecisionScenarioImpl(name, this);
		else
		{
			newScenario = (DecisionScenario)(scenario.createIndependentCopy());
			((DecisionScenarioImpl)newScenario).setName(createUniqueChildName(name));
		}

		helper.addScenario(newScenario);

		ServiceContext context = ServiceContext.getServiceContext();
		context.addScenario(newScenario);

		try { helper.setCurrentScenario(newScenario); }
		catch (ElementNotFound ex) { throw new RuntimeException("Should not happen"); }

		return newScenario;
	}
	
	
	public void deleteScenario(Scenario scenario)
	throws
		ParameterError
	{
		helper.deleteScenario(scenario);
	}
	
	
	public void addScenario(Scenario scenario)
	{
		helper.addScenario(scenario);
	}
	
	
	public ScenarioSet createScenarioSet(String name, 
		Scenario baseScenario, Attribute attr, Serializable[] values)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		if (! (baseScenario instanceof DecisionScenario)) throw new ParameterError(
			"Scenario '" + baseScenario.getFullName() + "' is not a DecisionScenario");
		if (! (attr instanceof DecisionAttribute)) throw new RuntimeException(
			"Attribute '" + attr.getFullName() + "' is not a DecisionAttribute");

		String setName = name;
		
		if (setName == null) setName = baseScenario.getName() + "_" + attr.getName();
		

		// For each value of the Attribute's Iterator (in the context of a selected Scenario)

		SortedSet<DecisionScenario> newScenarios = new TreeSetNullDisallowed<DecisionScenario>();
		
		if (values != null) for (Serializable value : values)
		{
			// Create a Scenario for that value and link it to the ScenarioSet.
			
			DecisionScenario newScenario = null;
			try { newScenario = (DecisionScenario)(baseScenario.createIndependentCopy()); }
			catch (CloneNotSupportedException ex) { throw new ParameterError(ex); }
			
			newScenario.setAttributeValue(attr, value);
			newScenarios.add(newScenario);
		}
		
		DecisionScenarioSetImpl scenarioSet = new DecisionScenarioSetImpl(
			(DecisionScenario)baseScenario,
			createUniqueDecisionScenarioSetName(setName), (DecisionAttribute)attr, newScenarios);
		
		helper.addScenarioSet(scenarioSet);
		
		return scenarioSet;
	}
	
	
	public void deleteScenarioSet(ScenarioSet scenarioSet)
	throws
		ParameterError
	{
		helper.deleteScenarioSet(scenarioSet);
	}
	
	
	public void addScenarioSet(ScenarioSet scenarioSet)
	{
		helper.addScenarioSet(scenarioSet);
	}
	
	
	public List<ScenarioSet> getScenarioSets()
	{
		return helper.getScenarioSets();
	}
	
	
	public ScenarioSet getScenarioSet(String name)
	throws
		ElementNotFound
	{
		return helper.getScenarioSet(name);
	}
	
	
	public VisualGuidance getVisualGuidance()
	{
		return helper.getVisualGuidance();
	}
 	
	
  /* *************************************************************************
   * From PersistentNode
   */
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DecisionDomainImpl newInstance = (DecisionDomainImpl)(helper.clone(cloneMap, cloneParent));
		newInstance.listChildNodes = cloneNodeList(listChildNodes, cloneMap, newInstance);
		//newInstance.decisionPoints = cloneNodeSet(decisionPoints, cloneMap, newInstance);
		//newInstance.dependencies = cloneNodeSet(dependencies, cloneMap, newInstance);
		//newInstance.precursorRelationships = cloneNodeSet(precursorRelationships, cloneMap, newInstance);
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.println(indentString 
			+ (this instanceof MotifDef? 
				"<decision_motif name=\"" : "<decision_domain name=\"")
			
			+ (getDeclaredName() == null? this.getName() : getDeclaredName())
			+ "\" "
			+ (getSourceName() == null? "" : "source_name=\"" + getSourceName() + "\" ")
			+ (getSourceURL() == null? "" : "source_url=\"" + getSourceURL() + "\" ")
			+ (getDeclaredVersion() == null? "" : "version=\"" + getDeclaredVersion() + "\" ")
			
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "width=\"" + this.getWidth() + "\" "
			+ "height=\"" + this.getHeight() + "\">");
			
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		
		// Write MenuItems, if any.
		
		if (this instanceof MenuOwner)
		{
			List<MenuItem> mis = ((MenuOwner)this).getMenuItems();
			if (mis != null)
			{
				for (MenuItem mi : mis) mi.writeAsXML(writer, indentation+1);
			}
		}
		
		
		// Write each child.
		
		Set<PersistentNode> children = this.getChildren();
		for (PersistentNode child : children)
		{
			writer.println();
			child.writeAsXML(writer, indentation+1);
		}
		
		writer.println();
		writer.println(indentString + 
			(this instanceof MotifDef? "</decision_motif>" : "</decision_domain>")
			);


		// Write Scenario Sets.
		
		writer.println();
		List<ScenarioSet> sss = getScenarioSets();
		for (ScenarioSet scenSet : sss)
		{
			writer.println();
			scenSet.writeAsXML(writer, indentation);
		}
		
		
		// Write Scenarios.
		
		writer.println();
		List<Scenario> ss = getScenarios();
		for (Scenario scenario : ss)
		{
			writer.println();
			scenario.writeAsXML(writer, indentation);
		}
	}
	
	
	public void prepareForDeletion()
	{
		helper.prepareForDeletion();

		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		List<DecisionPoint> dpCopy = getDecisionPoints();
			//new TreeSetNullDisallowed<DecisionPoint>(decisionPoints);
		for (DecisionPoint dp : dpCopy) deleteDecisionPoint(dp);
		
		List<Dependency> depCopy = getDependencies();
			//new TreeSetNullDisallowed<Dependency>(dependencies);
		for (Dependency dep : depCopy) deleteDependency(dep);
		
		List<PrecursorRelationship> prCopy = getPrecursorRelationships();
		for (PrecursorRelationship pr : prCopy) deletePrecursorRelationship(pr);
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.

		// Nullify all references.
		listChildNodes = null;
	}
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		return helper.getChild(name);
	}
	
	
	public SortedSet<PersistentNode> getChildren()
	{
		SortedSet<PersistentNode> children = helper.getChildren();
		//children.addAll(decisionPoints);
		//children.addAll(dependencies);
		//children.addAll(precursorRelationships);
		return children;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		helper.deleteChild(child, outermostAffectedRef);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public byte[] renderAsSVG(double wCmMax, double hCmMax)
	throws
		IOException
	{
		throw new RuntimeException("Not implemented yet"); // TBD
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		DecisionDomainSer ser = (DecisionDomainSer)(helper.externalize(nodeSer));
		ser.decisionPointNodeIds = createNodeIdArray(getDecisionPoints());
		ser.dependencyNodeIds = createNodeIdArray(getDependencies());
		ser.precursorRelationshipNodeIds = createNodeIdArray(getPrecursorRelationships());
		return ser;
	}
	
		
  /* *************************************************************************
   * From DecisionDomain
   */
	
	
	public synchronized DecisionPoint getDecisionPoint(String name)
	throws
		ParameterError
	{
		if (name == null) throw new ParameterError("Name specified is null");
		if (name.equals("")) throw new ParameterError("Name specified is empty string");

		for (DecisionPoint decisionPoint : getDecisionPoints())
		{
			if (decisionPoint.getName().equals(name)) return decisionPoint;
		}

		return null;
	}


	public synchronized DecisionScenario getDecisionScenario(String name)
	throws
		ParameterError
	{
		if (name == null) throw new ParameterError("Name specified is null");
		if (name.equals("")) throw new ParameterError("Name specified is empty string");

		for (DecisionScenario decisionScenario : getDecisionScenarios())
		{
			if (decisionScenario.getName().equals(name)) return decisionScenario;
		}

		return null;
	}


	public synchronized DecisionPoint createDecisionPoint(String name)
	throws
		ParameterError
	{
		DecisionPoint dp = new DecisionPointImpl(name, this);
		listChildNodes.add(dp);
		//decisionPoints.add(dp);
		return dp;
	}


	public synchronized DecisionPoint createDecisionPoint(String name,
			Class nativeImplClass)
	throws
		ParameterError
	{
		DecisionPoint dp = createDecisionPoint(name);

		//System.out.println("333Calling setNativeImplementation for " +
		//	dp.getName());
		
		if (! DecisionPoint.NativeDecisionPointImplementation.class.isAssignableFrom(
			nativeImplClass)) throw new ParameterError(
				"Decision Point native impl class must implement " +
				"NativeDecisionPointImplementation");

		dp.setNativeImplementationClass(nativeImplClass);
		return dp;
	}


	public synchronized void deleteDecisionPoint(DecisionPoint dp)
	{
		try { deleteChild(dp); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}


	public synchronized Precludes createPrecludes(DecisionPoint a, DecisionPoint b)
	{
		Precludes precludes = new DependencyImpl.PrecludesImpl(this, a, b);
		listChildNodes.add(precludes);
		return precludes;
	}
	
	
	public void deleteDependency(Dependency d)
	{
		try { deleteChild(d); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}


	public synchronized PrecursorRelationship createPrecursorRelationship(
		DecisionPoint a, DecisionPoint b)
	{
		PrecursorRelationship preRln = new PrecursorRelationshipImpl(this, a, b);
		listChildNodes.add(preRln);
		return preRln;
	}
	
	
	public void deletePrecursorRelationship(PrecursorRelationship pr)
	{
		try { deleteChild(pr); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}


	public synchronized Set<DecisionScenario> getDecisionScenarios()
	{
		Set<DecisionScenario> dss = new TreeSetNullDisallowed<DecisionScenario>();
		List<Scenario> ss = getScenarios();
		for (Scenario s : ss) dss.add((DecisionScenario)s);
		return dss;
	}


	public synchronized DecisionScenario getCurrentDecisionScenario()
	{
		return (DecisionScenario)(getCurrentScenario());
	}


	public synchronized void setCurrentDecisionScenario(DecisionScenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		setCurrentScenario(scenario);
	}


	public synchronized void addDecisionScenario(DecisionScenario scenario)
	{
		addScenario(scenario);
	}
	
	
	public synchronized List<DecisionPoint> getDecisionPoints()
	{
		List prs = new Vector<DecisionPoint>();
		List<PersistentNode> nodes = getOrderedNodesOfKind(DecisionPoint.class);
		for (PersistentNode n : nodes)
			if (n instanceof DecisionPoint) prs.add((DecisionPoint)n);
			else throw new RuntimeException("'" + n .getFullName() + "' is not a DecisionPoint");
		return prs;
	}
	
	
	public synchronized List<Dependency> getDependencies()
	{
		List prs = new Vector<Dependency>();
		List<PersistentNode> nodes = getOrderedNodesOfKind(Dependency.class);
		for (PersistentNode n : nodes)
			if (n instanceof Dependency) prs.add((Dependency)n);
			else throw new RuntimeException("'" + n .getFullName() + "' is not a Dependency");
		return prs;
	}
	
	
	public synchronized List<PrecursorRelationship> getPrecursorRelationships()
	{
		List prs = new Vector<PrecursorRelationship>();
		List<PersistentNode> nodes = getOrderedNodesOfKind(PrecursorRelationship.class);
		for (PersistentNode n : nodes)
			if (n instanceof PrecursorRelationship) prs.add((PrecursorRelationship)n);
			else throw new RuntimeException("'" + n .getFullName() + "' is not a PrecursorRelationship");
		return prs;
	}


	public synchronized Set<Decision> updateDecisions(DecisionScenario decisionScenario,
		DecisionCallback callback, Set<Variable> variables)
	throws
		ModelContainsError,
		ParameterError
	{
		//if (abort) { callback.abort(); return; }

		Set<Decision> decisions = new TreeSetNullDisallowed<Decision>();  // this will be
			// the return result.


		/*
		 * Identify all of the DecisionPoints that are affected by the changed
		 * variables.
		 */

		Set<DecisionPoint> affected = getAffectedDecisionPoints(variables, false);

		//System.out.println();
		//System.out.println("Affected set now has " + affected.size() +
		//	" DecisionPoints:");


		// Start each affected DecisionPoint.

		Set<DecisionPoint.NativeDecisionPointImplementation> startedDPImpls = 
			new TreeSetNullDisallowed<DecisionPoint.NativeDecisionPointImplementation>();

		try
		{
		for (DecisionPoint dp : affected)
		{
			if (dp.getNativeImplementationClass() != null)
			{
				DecisionPoint.DecisionActivityContext context =
					new DecisionActivityContextImpl();

				//System.out.print(
				//	"\tStarting Decision Point " + dp.getName() + "...");
				
				DecisionPoint.NativeDecisionPointImplementation impl = null;
				try
				{
					impl = 
						(DecisionPoint.NativeDecisionPointImplementation)
						(dp.getNativeImplementationClass().newInstance());
					
					decisionScenario.setNativeImplementation(dp, impl);
				}
				catch (InstantiationException ie)
				{
					throw new ModelContainsError(
						"The native implementation class " +
						dp.getNativeImplementationClass().getName() +
						" could not be instantiated: perhaps it is abstract");
				}
				catch (ClassCastException cce)
				{
					throw new RuntimeException(
						dp.getNativeImplementationClass().getName() +
						" was expected to implement " +
						"NativeDecisionPointImplementation but does not");
				}
				catch (Throwable t)
				{
					throw new ModelContainsError(
						"Unable to instantiate " +
							dp.getNativeImplementationClass().getName() +
							": " + t.getMessage());
				}
					
				impl.setDecisionElement(dp);
				
				try { impl.start(context); }
				catch (Exception ex)
				{
					//System.out.println("could not start.");
					throw new ModelContainsError(
						"While starting DecisionPoint " + dp.getName(), ex);
				}

				startedDPImpls.add(impl);
			}
		}


		/*
		 * Evaluate each affected DecisonPoint, in a sequence such that no
		 * DecisionPoint is evaluted more than once.
		 */
		
		for	// each set of <highest-level> nodes, until no more...
		(
			;;
		)
		{
			// Filter from <affected> those nodes that have no precursors
			// in <affected>. Add the removed nodes to a new set
			// called <highest-level>.

			Set<DecisionPoint> highestLevel = new TreeSetNullDisallowed<DecisionPoint>();

			for (DecisionPoint dp : affected)
			{
				// If dp has no precursor that exists in <affected> or
				// <highest-level>, then remove it from <affected> and add it
				// to <highest-level>.

				boolean remove = true;

				for (DecisionPoint precursor : dp.getPrecursorDecisionPoints())
				{
					if
					(	affected.contains(precursor) ||
						highestLevel.contains(precursor)
					)
					{
						remove = false;
						break;
					}
				}

				if (remove)
				{
					//affected.remove(dp);
					highestLevel.add(dp);
					continue;
				}
			}

			affected.removeAll(highestLevel);


			// If <highest-level> is empty, then we are done.

			if (highestLevel.isEmpty()) break;


			// Evaluate each of <highest-level>.

			for (DecisionPoint dp : highestLevel)
			{
				//System.out.println("Evaluating DecisionPoint " +
				//	dp.getName());

				Set<Decision> dpDecisions = dp.evaluate(decisionScenario);

				//System.out.println("\tResult of evaluation: " +
				//	dpDecisions.size() + " decisions.");

				decisions.addAll(dpDecisions);
			}
		}
		}
		finally  // Stop any native implementations that were started.
		{
			for (DecisionPoint.NativeDecisionPointImplementation impl : startedDPImpls)
			{
				try { impl.stop(); }
				catch (Exception ex2)
				{
					callback.showMessage("Could not stop DecisionPoint " +
						impl.getClass().getName());
				}
			}
		}


		return decisions;
	}


	public synchronized Set<DecisionPoint> getAffectedDecisionPoints(
		Set<Variable> variables, boolean otherDecisionDomainsAllowed)
	throws
		ParameterError
	{
		Set<DecisionPoint> affected = new TreeSetNullDisallowed<DecisionPoint>();

		for (Variable variable : variables)
		{
			if ((! otherDecisionDomainsAllowed) &&
					(variable.getDecisionDomain() != this))
			{
				throw new ParameterError(
				"Variable " + variable.getName() + " does not belong to " +
					this.getName());
			}
					
			affected.add(variable.getDecisionPoint());
			//System.out.print(
			//	" (DecisionDomain.getAffectedDecisionPoints: Adding DecisionPoint " +
			//		variable.getDecisionPoint().getName() + ") ");
		}


		// Identify all nodes that are directly or indirectly downstream
		// of <affected>. Add these to the set <affected>. This comprises the
		// total set of DecisionPoints that are directly or indirectly affected.

		Set<DecisionPoint> downstream = new TreeSetNullDisallowed<DecisionPoint>();

		for (DecisionPoint decisionPoint : affected)
		{
			getDownstreamDecisionPoints(decisionPoint, downstream);
		}

		//System.out.println(" (DecisionDomain.getAffectedDecisionPoints: Adding " + 
		//	downstream.size() +
		//	" downstream DecisionPoints to the affected Set." + ") ");

		affected.addAll(downstream);

		return affected;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		this.helper = new DomainHelper(this, new SuperDomain());
	}
	
	
  /* *************************************************************************
   * From PersistentListNode
   */
	

	public List<PersistentNode> getOrderedChildren()
	{
		return listChildNodes;
	}
	
	public int getNoOfOrderedChildren()
	{
		return listChildNodes.size();
	}

	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{
		for (PersistentNode n : listChildNodes) if (n.getName().equals(name)) return n;
		throw new ElementNotFound(getFullName() + "." + name);
	}
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{
		try { return listChildNodes.get(index); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		return listChildNodes.indexOf(child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{
		try { listChildNodes.add(index, child); }
		catch (Exception ex) { throw new ParameterError(ex); }
	}
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{
		try { return listChildNodes.add(child); }
		catch (Exception ex) { throw new ParameterError(ex); }
	}
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{
		try { return listChildNodes.remove(child); }
		catch (Exception ex) { throw new ParameterError(ex); }
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{
		List<PersistentNode> nodes = new SetVector<PersistentNode>();
		for (PersistentNode n : listChildNodes)
			if (c.isAssignableFrom(n.getClass())) nodes.add(n);
		return nodes;
	}
	
	public void removeOrderedNodesOfKind(Class c)
	{
		List<PersistentNode> nodes = new Vector<PersistentNode>(listChildNodes);
		for (PersistentNode n : nodes)
			if (c.isAssignableFrom(n.getClass())) listChildNodes.remove(n);
	}
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{
		try { return listChildNodes.addAll(nodes); }
		catch (Exception ex) { throw new ParameterError(ex); }
	}
		
	/* end of implementation methods */
	
	
	
	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return ListNodeHelper.createListChild(this, nodeBefore, nodeAfter,
			scenario, copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


  /* *************************************************************************
   * Internal only methods.
   */
	
	
	protected String createUniqueDecisionDomainName()
	{
		return createUniqueDecisionDomainName(null);
	}
	
	
	protected String createUniqueDecisionScenarioSetName(String name)
	{
		return createUniqueChildName(name);
	}
	
	
	protected String createUniqueDecisionDomainName(String baseName)
	{
		ModelEngineLocal modelEngineLocal = 
			(ModelEngineLocal)(ServiceContext.getServiceContext().getModelEngine());
		
		if (baseName == null)
		{
			baseName = "";
		}
		else try
		{
			if (modelEngineLocal.getDecisionDomainPersistentNode(baseName) == null)
				
				return baseName;
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
		
		
		// Create a unique name that is based on the base name.
		
		int i = 0;
		for (;;) try
		{
			i++;
			String name = baseName + i;
		
			try { modelEngineLocal.getDecisionDomainPersistentNode(name); }
			catch (ElementNotFound enf)
			{
				return name;
			}
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}


	public void dump(int indentation)
	{
		helper.dump(indentation);
		
		String padding = "";
		for (int i = 1; i <= indentation; i++) padding +="\t";

		GlobalConsole.println(padding + "\tDecisionPoints:");
		List<DecisionPoint> decisionPoints = getDecisionPoints();
		for (DecisionPoint decPoint : decisionPoints) decPoint.dump(indentation+1);

		GlobalConsole.println(padding + "\tDependencies:");
		List<Dependency> dependencies = getDependencies();
		for (Dependency dep : dependencies) dep.dump(indentation+1);

		GlobalConsole.println(padding + "\tPrecursorRelationships:");
		List<PrecursorRelationship> precursorRelationships = getPrecursorRelationships();
		for (PrecursorRelationship preRel : precursorRelationships) preRel.dump(indentation+1);
	}


	/**
	 * Recursive method to identify the set of DecisionPoints that are
	 * downstream of the specified DecisionPoint. Downstream DecisionPoints
	 * are added to 'downstream'.
	 */

	protected void getDownstreamDecisionPoints(DecisionPoint decisionPoint,
		Set<DecisionPoint> downstream)
	{
		Set<DecisionPoint> ds = decisionPoint.getSubordinateDecisionPoints();
		downstream.addAll(ds);

		for (DecisionPoint dp : ds)
		{
			getDownstreamDecisionPoints(dp, downstream);
		}
	}


  /* *************************************************************************
   * From Listeners
   */
 
 
  /* Proxy classes needed by DomainHelper */

	
	private class SuperDomain extends SuperMenuOwner
	{
		/* No need to list Domain methods because they do not exist in the superclass
			and hence their super method is never called. */
	}
	
	
	private class SuperMenuOwner extends SuperCrossReferenceable implements MenuOwner
	{
		public Action createAction(int position, String text, String desc, String javaMethodName)
			throws ParameterError
		{ return DecisionDomainImpl.super.createAction(position, text, desc, javaMethodName); }
		
		public Action createAction(String text, String desc, String javaMethodName) throws ParameterError
		{ return DecisionDomainImpl.super.createAction(text, desc, javaMethodName); }
		
		public MenuTree createMenuTree(int position, String text, String desc) throws ParameterError
		{ return DecisionDomainImpl.super.createMenuTree(position, text, desc); }
		
		public MenuTree createMenuTree(String text, String desc) throws ParameterError
		{ return DecisionDomainImpl.super.createMenuTree(text, desc); }
		
		public void deleteMenuItem(int menuItemPos) throws ParameterError
		{ DecisionDomainImpl.super.deleteMenuItem(menuItemPos); }
		
		public void deleteMenuItem(MenuItem menuItem) throws ParameterError
		{ DecisionDomainImpl.super.deleteMenuItem(menuItem); }
		
		public List<MenuItem> getMenuItems()
		{ return DecisionDomainImpl.super.getMenuItems(); }
		
		public int getIndexOf(MenuItem mi)
		{ return DecisionDomainImpl.super.getIndexOf(mi); }
	}
	
	
	private class SuperCrossReferenceable extends SuperPersistentNode implements CrossReferenceable
	{
		public Set<CrossReference> getCrossReferences()
		{ return DecisionDomainImpl.super.getCrossReferences(); }
		
		
		public void addRef(CrossReference cr)
		throws
			ParameterError
		{ DecisionDomainImpl.super.addRef(cr); }
		
		
		public Set<NamedReference> getNamedReferences()
		{ return DecisionDomainImpl.super.getNamedReferences(); }
		
		
		public NamedReference getNamedReference(String refName)
		{ return DecisionDomainImpl.super.getNamedReference(refName); }
		
		
		public Set<CrossReferenceable> getToNodes(NamedReference nr)
		{ return DecisionDomainImpl.super.getToNodes(nr); }
		
		
		public Set<CrossReferenceable> getFromNodes(NamedReference nr)
		{ return DecisionDomainImpl.super.getFromNodes(nr); }
		
		
		public Set<CrossReferenceable> getToNodes(String refName)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.getToNodes(refName); }
		
		
		public Set<CrossReferenceable> getFromNodes(String refName)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.getFromNodes(refName); }
		
		
		public void removeRef(CrossReference cr)
		throws
			ParameterError
		{ DecisionDomainImpl.super.removeRef(cr); }
			
			
		public void addCrossReferenceCreationListener(CrossReferenceCreationListener listener)
		{ DecisionDomainImpl.super.addCrossReferenceCreationListener(listener); }
		
		public void addCrossReferenceDeletionListener(CrossReferenceDeletionListener listener)
		{ DecisionDomainImpl.super.addCrossReferenceDeletionListener(listener); }
		
		public List<CrossReferenceCreationListener> getCrossRefCreationListeners()
		{ return DecisionDomainImpl.super.getCrossRefCreationListeners(); }
		
		public List<CrossReferenceDeletionListener> getCrossRefDeletionListeners()
		{ return DecisionDomainImpl.super.getCrossRefDeletionListeners(); }
		
		public void removeCrossReferenceListener(CrossReferenceListener listener)
		{ DecisionDomainImpl.super.removeCrossReferenceListener(listener); }
		
		public void signalCrossReferenceCreated(CrossReference cr)
		throws
			Exception
		{ DecisionDomainImpl.super.signalCrossReferenceCreated(cr); }
		
		public void signalCrossReferenceDeleted(NamedReference nr,
			CrossReferenceable fromNode, CrossReferenceable toNode)
		throws
			Exception
		{ DecisionDomainImpl.super.signalCrossReferenceDeleted(nr, fromNode, toNode); }
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return DecisionDomainImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return DecisionDomainImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return DecisionDomainImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return DecisionDomainImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ DecisionDomainImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ DecisionDomainImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return DecisionDomainImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return DecisionDomainImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ return DecisionDomainImpl.super.getSerClass(); }
		
		
		public Set<Attribute> getAttributes()
		{ return DecisionDomainImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return DecisionDomainImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.getAttributeSeqNo(attr); }


		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ DecisionDomainImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ DecisionDomainImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ DecisionDomainImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ DecisionDomainImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ DecisionDomainImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return DecisionDomainImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return DecisionDomainImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return DecisionDomainImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return DecisionDomainImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return DecisionDomainImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return DecisionDomainImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return DecisionDomainImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ DecisionDomainImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ DecisionDomainImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return DecisionDomainImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return DecisionDomainImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return DecisionDomainImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return DecisionDomainImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ DecisionDomainImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return DecisionDomainImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ DecisionDomainImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return DecisionDomainImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return DecisionDomainImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ DecisionDomainImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ DecisionDomainImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return DecisionDomainImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return DecisionDomainImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return DecisionDomainImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return DecisionDomainImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return DecisionDomainImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return DecisionDomainImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return DecisionDomainImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return DecisionDomainImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ DecisionDomainImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return DecisionDomainImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return DecisionDomainImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ DecisionDomainImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return DecisionDomainImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ DecisionDomainImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return DecisionDomainImpl.super.isVisible(); }
		
		
		public void layout()
		{ DecisionDomainImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return DecisionDomainImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ DecisionDomainImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ DecisionDomainImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ DecisionDomainImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return DecisionDomainImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ DecisionDomainImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return DecisionDomainImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ DecisionDomainImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return DecisionDomainImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ DecisionDomainImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return DecisionDomainImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return DecisionDomainImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return DecisionDomainImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ DecisionDomainImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ DecisionDomainImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return DecisionDomainImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ DecisionDomainImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return DecisionDomainImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ DecisionDomainImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return DecisionDomainImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return DecisionDomainImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return DecisionDomainImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return DecisionDomainImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ DecisionDomainImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return DecisionDomainImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ throw new RuntimeException("Should not be called"); }
		
		public void setViewClassName(String name)
		{ DecisionDomainImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return DecisionDomainImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return DecisionDomainImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return DecisionDomainImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return DecisionDomainImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return DecisionDomainImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return DecisionDomainImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ DecisionDomainImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return DecisionDomainImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return DecisionDomainImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return DecisionDomainImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return DecisionDomainImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return DecisionDomainImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return DecisionDomainImpl.super.getX(); }
		public double getY() { return DecisionDomainImpl.super.getY(); }
		public double getScale() { return DecisionDomainImpl.super.getScale(); }
		public double getOrientation() { return DecisionDomainImpl.super.getOrientation(); }
		public void setX(double x) { DecisionDomainImpl.super.setX(x); }
		public void setY(double y) { DecisionDomainImpl.super.setY(y); }
		public void setLocation(double x, double y) { DecisionDomainImpl.super.setLocation(x, y); }
		public void setScale(double scale) { DecisionDomainImpl.super.setScale(scale); }
		public void setOrientation(double angle) { DecisionDomainImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return DecisionDomainImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return DecisionDomainImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return DecisionDomainImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return DecisionDomainImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ DecisionDomainImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return DecisionDomainImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return DecisionDomainImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return DecisionDomainImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return DecisionDomainImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return DecisionDomainImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return DecisionDomainImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ DecisionDomainImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return DecisionDomainImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return DecisionDomainImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ DecisionDomainImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ DecisionDomainImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ DecisionDomainImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return DecisionDomainImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return DecisionDomainImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return DecisionDomainImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return DecisionDomainImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return DecisionDomainImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ DecisionDomainImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return DecisionDomainImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ DecisionDomainImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ DecisionDomainImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ DecisionDomainImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return DecisionDomainImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return DecisionDomainImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return DecisionDomainImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ DecisionDomainImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return DecisionDomainImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ DecisionDomainImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return DecisionDomainImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ DecisionDomainImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ DecisionDomainImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ DecisionDomainImpl.super.printChildrenRecursive(indentationLevel); }
	}
}

