/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.PersistentNode.*;
import expressway.server.MotifElement.MotifDef;
import expressway.server.NamedReference.CrossReferenceable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;


/**
 * DecisionElements represent a decision model for a design that consists
 * of ModelElements. The decision model provides the rationale for the
 * design. This allows design choices and decision criteria to be documented
 * in a manner that relates them to design elements.
 *
 * DecisionElement is the supertype for all decision elements.
 *
 * None of these methods manage transactions. It is assumed that the caller
 * performs transaction management.
 */

public interface DecisionElement extends PersistentNode, CrossReferenceable
{
	DecisionAttribute createDecisionAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	DecisionAttribute createDecisionAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	DecisionAttribute createDecisionAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
		
	
	interface DecisionDomainMotifDef extends DecisionDomain, MotifDef
	{
	}
	

	
  /* ***************************************************************************
	 * *************************************************************************
	 * Accessor methods.
	 */

	DecisionDomain getDecisionDomain();


	double getWidth();


	double getHeight();


	void setWidth(double width);


	void setHeight(double height);

	
	
  /* ***************************************************************************
	 * *************************************************************************
	 * Diagnostic methods.
	 */

	void dump();


	void dump(int indentation);


  /* ***************************************************************************
	 * *************************************************************************
	 * Interfaces that extend DecisionElement.
	 */

	/**
	 * Core elements of a DecisionDomain: DecisionPoints, PrecursorRelationships
	 * Dependencies, Scenarios, and ScenarioSets.
	 */
	 
	public interface DecisionDomain extends DecisionElement, Domain
	{
		DecisionDomain getDecisionDomain();


		String getName();


		/**
		 * Find the specified DecisionPoint, relative to this DecisionDomain.
		 * Return null if not found.
		 */

		DecisionPoint getDecisionPoint(String name)
		throws
			ParameterError;


		/**
		 * Find the specified DecisionSceario, relative to this DecisionDomain.
		 * Return null if not found.
		 */

		DecisionScenario getDecisionScenario(String name)
		throws
			ParameterError;


		DecisionPoint createDecisionPoint(String name) throws ParameterError;


		/**
		 * The nativeImplClass must implement NativeDecisionPointImplementation.
		 */
		 
		DecisionPoint createDecisionPoint(String name, Class nativeImplClass)
		throws
			ParameterError;  // if the specified class does not
				// implement NativeDecisionPointImplementation.
				
				
		void deleteDecisionPoint(DecisionPoint dp);


		Precludes createPrecludes(DecisionPoint a, DecisionPoint b);
		
		
		void deleteDependency(Dependency d);


		PrecursorRelationship createPrecursorRelationship(DecisionPoint a, DecisionPoint b);
		
		
		void deletePrecursorRelationship(PrecursorRelationship pr);



		/**
		 * Retrieve a Set of the Scenarios that are defined for this DecisionDomain.
		 */

		Set<DecisionScenario> getDecisionScenarios();


		/**
		 * Retrieve the DecisionScenario that the user considers to be the "current"
		 * one at this moment. I.e., the user desires that the current scenario be
		 * used when simuation is performed on the DecisionDomain.
		 * May return null.
		 */

		DecisionScenario getCurrentDecisionScenario();


		void setCurrentDecisionScenario(DecisionScenario scenario)
		throws
			ElementNotFound,
			ParameterError;


		void addDecisionScenario(DecisionScenario scenario);
		
		
		/**
		 * Return the Set of DecisionPoints owned by this DecisionDomain.
		 */

		List<DecisionPoint> getDecisionPoints();


		/**
		 * Recompute any DecisionPoints that are affected by changes to the specified
		 * Variables, and return the resulting Decisions. The Variables must all
		 * belong to this DecisionDomain.
		 */

		Set<Decision> updateDecisions(DecisionScenario decisionScenario,
			DecisionCallback callback, Set<Variable> variables)
		throws
			ModelContainsError,
			ParameterError;  // if a Variable does not belong to this DecisionDomain.


		/**
		 * Determine the complete Set of DecisionPoints that are potentially
		 * affected by the specified Variables. If otherDecisionDomainsAllowed
		 * is false, then each of the Variables must belong to this DecisionDomain.
		 */
	
		Set<DecisionPoint> getAffectedDecisionPoints(Set<Variable> variables,
			boolean otherDecisionDomainsAllowed)
		throws
			ParameterError;  // if a Variable does not belong to this DecisionDomain.
	}
	
	
	public interface DecisionAttribute extends DecisionElement, Attribute
	{
	}


	/**
	 * Callback interface for signaling the client on the progress of a Decision-
	 * making process.
	 */

	public interface DecisionCallback extends ProgressCallback
	{
		/* --------------------------------------------------------------------
		 * Simulator-facing methods. I.e., these are methods for the simulator (or
		 * the simulation thread) to call to report progress.
		 */

		/**
		 * To be called by the Decision engine when a new DecisionScenario has
		 * been created.
		 */
		
		///DecisionScenario reportNewScenario();
		
		
		/**
		 * To be called by the Decision engine when a new Decision has
		 * been made.
		 */

		///Decision reportNewDecision();
		

		/* --------------------------------------------------------------------
		 * Client-facing methods. I.e., these are methods for the client (or
		 * the request thread) to call to interrogate progress and manage
		 * the simulation (e.g., to abort it if it takes too long).
		 */

		/**
		 * To be called by a client to get a new Decision Set.
		 */

		Set<Decision> getNewDecisions();
	}


	public interface DecisionPoint extends DecisionElement
	{
		String getName();


		Set<Parameter> getParameters();  // decision determined by these.


		/**
		 * Find the specified Variable, relative to this DecisionPoint.
		 * Return null if not found.
		 */

		Variable getVariable(String name)
		throws
			ParameterError;


		Variable createVariable(String name) throws ParameterError;


		DecisionFunction createDecisionFunction(String name) throws ParameterError;


		ParameterAlias createParameterAlias(String name) throws ParameterError;


		ConstantValue createConstantValue(String name) throws ParameterError;
		
		
		void deleteParameter(Parameter param);


		Set<Decision> getDecisions();


		void addDecision(Decision decision);


		/**
		 * Remove this DecisionPoint's reference to the specified Decision, if a reference
		 * is held. Also nullify any reference back from the Decision to this DecisionPoint.
		 */

		void removeDecision(Decision decision);


		Set<DecisionPoint> getPrecursorDecisionPoints();


		Set<DecisionPoint> getSubordinateDecisionPoints();


		/**
		 * Evaluate the decision that is represented by the DecisionPoint.
		 * The evluation is stricly local: the decision result is not
		 * propagated to other DecisionPoints. (Propagation is the job of
		 * the DecisionDomain.)
		 *
		 * Evaluation consists of executing the rules embedded in the DecisionPoint
		 * based on the current state of the DecisionPoint Parameters, and then
		 * creating zero or more Decisions.
		 */

		Set<Decision> evaluate(DecisionScenario decisionScenario)
		throws
			ModelContainsError;


		/**
		 * Identify a native implementation class to use for this DecisionPoint. If a
		 * native implementation class is identified, then it will be used instead
		 * of the DecisionPoint's internal rules (if any). The class must
		 * implement NativeDecisionPointImplementation.
		 */

		void setNativeImplementationClass(Class implClass)
		throws
			ParameterError;  // if the specified Class does not
					// implement NativeDecisionPointImplementation.


		/**
		 * Return the NativeDecisionPointImplementation class identified for
		 * this DecisionPoint.
		 */

		Class getNativeImplementationClass();
		
		
		/**
		 * Native DecisionPoint implementations must implement this interface. A native
		 * DecisionPoint implementation provides a hand-coded implementation of a
		 * DecisionPoint, in Java or some other language.
		 */

		interface NativeDecisionPointImplementation extends NativeDecisionElementImplementation
		{
			Set<Decision> evaluate(DecisionPoint decisionPoint, DecisionScenario scenario)
			throws
				ModelContainsError;
		}


		/**
		 * Java native DecisionPoint implementations must implement this interface.
		 */

		interface JavaDecisionPointImplementation extends NativeDecisionPointImplementation
		{
		}
	}


	/**
	 * Native implementations must implement this interface. A native
	 * implementation provides a hand-coded implementation of a DecisionElement,
	 * in Java or some other language.
	 */

	interface NativeDecisionElementImplementation
	{
		void setDecisionElement(DecisionElement element);
		DecisionElement getDecisionElement();
		void start(DecisionActivityContext context) throws Exception;
		void stop();
	}


	/**
	 * The DecisionDomain passes an instance of this to a NativeImplementation when
	 * it starts. This provides a gateway for the implementation to access
	 * DecisionDomain functions when a DecisionPoint executes, such as accessing
	 * Parameter values and creating Decisions.
	 */

	interface DecisionActivityContext
	{
		/**
		 * Retrieve the value of the specified Variable from the Attribute to
		 * which it is bound, in the context of the current ModelScenario
		 * of any ModelDomain.
		 * If no value exists in a ModelScenario, return the Attribute's
		 * default value. If there is no default value, return null.
		 */

		Serializable getBoundAttributeValue(Variable variable)
		throws
			ModelContainsError; // if there are multiple values for the Attribute
				// across various ModelScenarios.
	}


	public interface PrecursorRelationship extends DecisionElement
	{
		DecisionPoint getPrecursorDecisionPoint() throws ValueNotSet;
		DecisionPoint getSubordinateDecisionPoint() throws ValueNotSet;
	}


	public interface Dependency extends DecisionElement
	{
		DecisionPoint getA() throws ValueNotSet;
		DecisionPoint getB() throws ValueNotSet;
	}


	public interface Precludes extends Dependency {}


	public interface Requires extends Dependency {}


	public interface Modifies extends Dependency {}


	public interface CoDependency extends Dependency {}


	public interface Decision extends DecisionElement
	{
		DecisionScenario getDecisionScenario();
		void setDecisionScenario(DecisionScenario scenario);
		
		DecisionPoint getDecisionPoint() throws ValueNotSet;
		void setDecisionPoint(DecisionPoint dp);
		
		Variable getVariable() throws ValueNotSet;
		void setVariable(Variable variable);

		//boolean isActive();  // if this decision is active, given current choices.
		Serializable getValue();  // the value for the decision point.

		public Object clone()
		throws
			CloneNotSupportedException;
	}


	public interface Parameter extends DecisionElement
	{
		String getName();
		DecisionPoint getDecisionPoint();
	}


	public interface ConstantValue extends Parameter
	{
	}


	public interface ParameterAlias extends Parameter
	{
	}


	public interface DecisionFunction extends Parameter, VariableChangeListener
	{
		void addVariableRef(Variable variable);
		
		void removeVariableRef(Variable variable);
		
		Set<Variable> getVariableRefs();
	}


	public interface VariableAttributeBinding extends DecisionElement, AttributeChangeListener
	{
		Variable getVariable() throws ValueNotSet;

		ModelElement.ModelAttribute getAttribute() throws ValueNotSet;
	}


	public interface Variable extends Parameter, DecisionFunctionChangeListener
	{
		Set<VariableAttributeBinding> getAttributeBindings();
		
		
		/**
		 * Return the DecisionFunctions that reference this Variable.
		 */

		Set<DecisionFunction> getReferencedBy();


		/**
		 * Create a binding between this Variable and the specified Attribute. If a
		 * binding already exists, simply return it without creating a new one.
		 */

		VariableAttributeBinding bindToAttribute(ModelElement.ModelAttribute a);
		
		
		/** ********************************************************************
		 * Remove the specified binding. Notify VariableAttributeBindingChangeListeners
		 * that the binding has been remove, so that they can nullify their
		 * references to it.
		 */
		 
		void deleteAttributeBinding(VariableAttributeBinding binding)
		throws
			ParameterError;


		/**
		 * Retrieve all Decisions that exist for this Variable. Note that no two
		 * Decisions for a Variable may be in the same DecisionScenario.
		 */

		Set<Decision> getDecisions();

		/**
		 * Add the specified Decision to this Variable's Set of Decisions. All mandatory
		 * relationships are created and checked.
		 */

		void addDecision(Decision decision);


		/**
		 * Remove this Variable's reference to the specified Decision, if a reference
		 * is held. Also nullify any reference back from the Decision to this Variable.
		 */

		void removeDecision(Decision decision);
	}


	/**
	 * A manually-made Decision. A Choice overrides any competing computed
	 * Decision. (A 'computed decision'is meant to mean any Decision made through
	 * the rules embedded in a DecisionPoint).
	 */

	public interface Choice extends Decision
	{
	}


	public interface DecisionScenario extends DecisionElement, Scenario,
		DecisionScenarioSetChangeListener
	{
		void setDecisionScenarioSet(DecisionScenarioSet ss);
		
		DecisionScenarioSet getDecisionScenarioSet();
		
		void setDecisions(Set<Decision> decs);

		Set<Decision> getDecisions();  // these make up the scenario.


		/**
		 * Retrieve the Set of Decisions owned by this DecisionScenario
		 * that pertain to the specified DecisionPoint.
		 */
		 
		Set<Decision> getDecisions(DecisionPoint decisionPoint);
		

		/**
		 * Retrieve the Decision (if any) for the specified Variable.
		 * Note that within a DecisionScenario it is semantically disallowed
		 * for more than one Decision to exist for a single Variable.
		 * May return null.
		 */

		Decision getDecision(Variable variable);


		/**
		 * Construct a Decision for the specified Variable.
		 */

		Decision createDecision(Variable variable, Serializable value, DecisionPoint dp)
		throws
			CloneNotSupportedException;  // if unable to clone a decision value.

			
		void deleteDecision(Decision decision)
		throws
			ParameterError;  // if the Decision is not found.
			

		/**
		 * Create a Choice for a Variable value within this DecisionScenario.
		 */

		Choice makeChoice(Variable variable, Serializable value)
		throws
			ParameterError;  // if unable to clone the value.


		/**
		 * Clone this DecisionScenario for the purpose of creating a new, initialized
		 * DecisionScenario. The criteria for cloning is that a deep copy is made for
		 * any elements that are owned by the scenario, and references are merely
		 * copied for other elements. However, an element is not copied if the element's
		 * value should be unique for the scenario. (See the CloneForSimulation pattern
		 * in the Expressway Product Architecture.)
		 */

		DecisionScenario cloneForSimulation()
		throws
			CloneNotSupportedException;
		
		
		/**
		 * Associate a native implementation with a DecisionPoint, in the context
		 * of this Scenario.
		 */
		
		void setNativeImplementation(DecisionPoint dp,
			DecisionPoint.NativeDecisionPointImplementation impl);
		
		
		/**
		 * Retrieve the native implementation that is associated with the specified
		 * DecisionPoint, in the context of this Scenario, or return null if none
		 * is associated.
		 */
		
		DecisionPoint.NativeDecisionPointImplementation getNativeImplementation(
			DecisionPoint decisionPoint);
	}
	
	
	/**
	 * An aggregator for Model Scenarios. The Scenarios in a Scenario Set differ
	 * only in their value for a specified Attribute.
	 */
	 
	interface DecisionScenarioSet extends DecisionElement, ScenarioSet,
		DecisionScenarioChangeListener
	{
		SortedSet<DecisionScenario> getDecisionScenarios();
	}


	//public interface DecisionModelRelation extends DecisionElement
	//{
	//	DecisionDomain getDecisionDomain();
	//	ModelElement.ModelDomain getModelDomain();
	//}
	
	
	/* *************************************************************************
	 * The following interfaces define methods to be called to notify non-owners
	 * of objects that references to those objects need to be nullified.
	 */
		 
	interface DecisionElementChangeListener
	{
		void decisionElementDeleted(DecisionElement element);
		void decisionElementNameChanged(DecisionElement element, String oldName);
	}


	interface VariableAttributeBindingChangeListener
	{
		void variableAttributeBindingDeleted(VariableAttributeBinding binding);
		void variableAttributeBindingNameChanged(VariableAttributeBinding binding, String oldName);
	}
	
		 
	interface VariableChangeListener
	{
		void variableDeleted(Variable variable);
		void variableNameChanged(Variable variable, String oldName);
	}
	
	
	interface DecisionFunctionChangeListener
	{
		void decisionFunctionDeleted(DecisionFunction df);
		void decisionFunctionNameChanged(DecisionFunction df, String oldName);
	}
	
	
	interface DecisionScenarioChangeListener
	{
		void decisionScenarioDeleted(DecisionScenario ds);
		void decisionScenarioNameChanged(DecisionScenario ds, String oldName);
	}
	
	
	interface DecisionScenarioSetChangeListener
	{
		void decisionScenarioSetDeleted(DecisionScenarioSet dss);
		void decisionScenarioSetNameChanged(DecisionScenarioSet dss, String oldName);
	}
}
