/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public abstract class DecisionElementImpl
	extends TemplatizableNode
	implements DecisionElement, Cloneable
{
	DecisionElementImpl(DecisionElement parent)
	{
		super(parent);
	}


	public String getDecisionDomainNodeId() { return getDomain().getNodeId(); }

	public Class getSerClass() { return DecisionElementSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DecisionElementSer)nodeSer).width = this.getWidth();
		((DecisionElementSer)nodeSer).height = this.getHeight();
		((DecisionElementSer)nodeSer).setViewClassName(getViewClassName());
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DecisionElementImpl newInstance = (DecisionElementImpl)(super.clone(cloneMap, cloneParent));
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public Object clone()
	throws
		CloneNotSupportedException
	{
		return super.clone();
	}


	public DecisionDomain getDecisionDomain() { return (DecisionDomain)(getDomain()); }


	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		return new TreeSetNullDisallowed<PersistentNode>();
	}
	
	
	public void dump() { dump(0); }


	public void dump(int indentation)
	{
		// Generate indentation.
		for (int i = 1; i <= indentation; i++) GlobalConsole.print("\t");

		// Print the current element.
		GlobalConsole.println(this);
	}

	
	
	/* *************************************************************************
	 * From PersistentNode.
	 */


	public Attribute constructAttribute(String name, Serializable defaultValue)
	{
		try { return new DecisionAttributeImpl(name, this, defaultValue); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	

	public Attribute constructAttribute(String name)
	{
		try { return new DecisionAttributeImpl(name, this, null); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public DecisionAttribute createDecisionAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (DecisionAttribute)(createAttribute(name, defaultValue, layoutBound,
			outermostAffectedRef));
	}
	
	
	public DecisionAttribute createDecisionAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (DecisionAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}
	
	
	public DecisionAttribute createDecisionAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (DecisionAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}


	protected String createUniqueAttributeName()
	{
		return createUniqueAttributeName(null);
	}
	
	
	protected String createUniqueAttributeName(String baseName)
	{
		if (baseName == null)
		{
			baseName = "";
		}
		else
		{
			Attribute attr = getAttribute(baseName);
			if (attr == null) return baseName;
		}
		
		// Create a unique name that is based on the base name.
		
		int i = 0;
		for (;;)
		{
			i++;
			String name = baseName + i;
		
			Attribute s = getAttribute(name);
			if (s == null) return name;
		}
	}
	
	
  /* From DecisionElementChangeListener */
	
		
	public void decisionElementDeleted(DecisionElement element)
	{
	}
	
	
	public void decisionElementNameChanged(DecisionElement element, String oldName)
	{
	}
}
