/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.SortedSet;


public class DecisionImpl extends DecisionElementImpl implements Decision
{
	private DecisionPoint decisionPoint = null;  // may be null.
	private Variable variable = null;

	/** Ownership. */
	public Serializable value = null;
	

	public DecisionImpl(DecisionScenario scenario, Serializable value)
	throws
		CloneNotSupportedException  // if unable to copy the decision value.
	{
		this(scenario, value, null);
	}


	public DecisionImpl(DecisionScenario scenario, Serializable value,
		DecisionPoint decisionPoint)
	throws
		CloneNotSupportedException  // if unable to copy the decision value.
	{
		super(scenario);
		setResizable(false);
		this.decisionPoint = decisionPoint;
		this.value = PersistentNodeImpl.copyObject(value);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public Class getSerClass() { return DecisionSer.class; }
	
	
	public String getTagName() { return "decision"; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DecisionSer)nodeSer).decisionPointNodeId = getNodeIdOrNull(decisionPoint);
		((DecisionSer)nodeSer).variableNodeId = getNodeIdOrNull(variable);
		((DecisionSer)nodeSer).value = this.getValue();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DecisionImpl newInstance = (DecisionImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.decisionPoint = cloneMap.getClonedNode(decisionPoint);
		newInstance.variable = cloneMap.getClonedNode(variable);
		newInstance.value = copyObject(value);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.

		// Nullify all references.
		value = null;
	}
	
	
	public Object clone()
	throws
		CloneNotSupportedException
	{
		DecisionImpl newDecisionImpl = (DecisionImpl)(super.clone());
		newDecisionImpl.value = PersistentNodeImpl.copyObject(value);
		return newDecisionImpl;
	}


	public DecisionScenario getDecisionScenario()
	{
		return (DecisionScenario)(getParent());
	}


	public void setDecisionScenario(DecisionScenario scenario)
	{
		setParent(scenario);
	}


	public DecisionPoint getDecisionPoint() throws ValueNotSet
	{
		if (decisionPoint == null) throw new ValueNotSet("Field 'decisionPoint' for '" + getFullName() + "'");
		return decisionPoint;
	}


	public void setDecisionPoint(DecisionPoint dp)
	{
		this.decisionPoint = dp;
	}


	public Variable getVariable() throws ValueNotSet
	{
		if (variable == null) throw new ValueNotSet("Field 'variable' for '" + getFullName() + "'");
		return variable;
	}


	public void setVariable(Variable variable)
	{
		this.variable = variable;
	}


	public Serializable getValue()
	{
		return value;
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DecisionIconImageName);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
}
