/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.server.NamedReference.*;
import expressway.ser.*;
import geometry.*;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Vector;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.Collection;
import java.util.Map;import java.util.List;
import java.util.Date;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.IOException;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import expressway.common.ClientModel.*;


public class DecisionScenarioImpl extends DecisionElementImpl implements DecisionScenario
{
	private ScenarioHelper helper;
	

	/** Ownership. */
	Set<Decision> decisions = new TreeSetNullDisallowed<Decision>();

	private transient Map<DecisionPoint, DecisionPoint.NativeDecisionPointImplementation>
		nativeImpls = new HashMap<DecisionPoint,
			DecisionPoint.NativeDecisionPointImplementation>();
	

	public DecisionScenarioImpl(String name, DecisionDomain dd)
	throws
		ParameterError
	{
		super(dd);
		dd.renameChild(this, name);
		this.helper = new ScenarioHelper(this, new SuperScenario());
	}
	
	
  /* From PersistentNode */
	
	
	public Class getSerClass() { return DecisionScenarioSer.class; }
	
	
	public String getTagName() { return "decision_scenario"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DecisionScenarioIconImageName);
	}
	
	
	public double getPreferredWidthInPixels() { return helper.getPreferredWidthInPixels(); }
	public double getPreferredHeightInPixels() { return helper.getPreferredHeightInPixels(); }
	
	
	public int getNoOfHeaderRows() { return helper.getNoOfHeaderRows(); }


	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		try { helper.renameChild(child, newName); }
		catch (ParameterError pe)
		{
			if (child instanceof Decision)
				if (decisions.contains(child))
					child.setNameAndNotify(newName);
				else
					throw pe;
			else
				throw pe;
		}
	}
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		try { return super.getChild(name); }
		catch (ElementNotFound ex)
		{
			for (Decision decision : decisions)
				if (decision.getName().equals(name)) return decision;
			throw ex;
		}
	}
	
	
	public SortedSet<PersistentNode> getChildren()
	{
		SortedSet<PersistentNode> nodes = super.getChildren();
		nodes.addAll(decisions);
		return nodes;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			if (child instanceof Decision)
			{
				if (! decisions.remove(child)) throw pe;
			}
			else
				throw pe;
			
			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		DecisionScenario newInstance = (DecisionScenario)(helper.clone(cloneMap, cloneParent));
		newInstance.setDecisions(cloneNodeSet(decisions, cloneMap, newInstance));
		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		helper.writeAsXML(writer, indentation);
	}


	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
		super.writeSpecializedXMLAttributes(writer);
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
		super.writeSpecializedXMLElements(writer, indentation);
		
		for (Decision decision : decisions)
		{
			decision.writeAsXML(writer, indentation+1);
		}
	}
	
	
	public void prepareForDeletion()
	{
		helper.prepareForDeletion();

		try
		{
			Set<Decision> decisionsCopy = new TreeSetNullDisallowed<Decision>(decisions);
			for (Decision d : decisionsCopy) deleteDecision(d);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		decisions = null;
	}


	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		DecisionScenarioSer ser = (DecisionScenarioSer)(helper.externalize(nodeSer));
		ser.setDecisionNodeIds(createNodeIdArray(decisions));
		return ser;
	}
	
	
  /* From PersistentListNode */


	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public void removeOrderedNodesOfKind(Class c)
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{ throw new RuntimeException("A Scenario does not have ordered child nodes"); }

	public List<PersistentNode> getOrderedChildren()
	{ return new Vector<PersistentNode>(); }
	
	public int getNoOfOrderedChildren() { return 0; }


	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return ListNodeHelper.createListChild(this, nodeBefore, nodeAfter,
			scenario, copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
	}
	
	
  /* From Scenario */
	
	
	public Scenario update(ScenarioSer ser)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		return helper.update(ser);
	}
	
	
	public void setDate(Date d) { helper.setDate(d); }
	
	
	public Date getDate() { return helper.getDate(); }
	
	
	public void setDerivedFrom(Scenario originalScenario)
	{
		helper.setDerivedFrom(originalScenario);
	}
	
	
	public Scenario derivedFrom() { return helper.derivedFrom(); }
	
	
	public void setAttributeValue(Attribute attr, Serializable value)
	throws
		ParameterError
	{
		helper.setAttributeValue(attr, value);
	}
	
	
	public Serializable getAttributeValue(Attribute attr)
	throws
		ParameterError
	{
		return helper.getAttributeValue(attr);
	}
	
	
	public void setAttributeValues(Map<Attribute, Serializable> attrValues)
	{
		helper.setAttributeValues(attrValues);
	}


	public Set<Attribute> getAttributesWithValues()
	{
		return helper.getAttributesWithValues();
	}
	
	
	public Set<Attribute> getAttributesWithValuesOfType(PersistentNode elt, 
		Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		return helper.getAttributesWithValuesOfType(elt, type, onlyOne);
	}
			
		
	public void setScenarioSet(ScenarioSet s) { helper.setScenarioSet(s); }
	
	
	public ScenarioSet getScenarioSet() { return helper.getScenarioSet(); }
	
	
	public void setScenarioThatWasCopied(Scenario scen) { helper.setScenarioThatWasCopied(scen); }
	
	
	public Scenario getScenarioThatWasCopied() { return helper.getScenarioThatWasCopied(); }
	

	public Scenario createIndependentCopy()
	throws
		CloneNotSupportedException
	{
		DecisionScenario newScenario = (DecisionScenario)(helper.createIndependentCopy());
		return newScenario;
	}
	
	
	public SortedSet<Attribute> identifyTags(Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		return helper.identifyTags(type, onlyOne);
	}
	
	
	public ScenarioHelper getHelper() { return helper; }
	
	
  /* From DecisionScenario */
	
	
	public void setDecisionScenarioSet(DecisionScenarioSet ss) { setScenarioSet(ss); }
	
	
	public DecisionScenarioSet getDecisionScenarioSet() { return (DecisionScenarioSet)(getScenarioSet()); }
	
	
	public void setDecisions(Set<Decision> decs) { this.decisions = decs; }
	
	
	public synchronized Set<Decision> getDecisions() { return decisions; }


	public synchronized Set<Decision> getDecisions(DecisionPoint decisionPoint)
	{
		Set<Decision> ds = new TreeSetNullDisallowed<Decision>();
		for (Decision d : decisions)
			try { if (d.getDecisionPoint() == decisionPoint) ds.add(d); }
			catch (ValueNotSet w) {}
		return ds;
	}


	public synchronized Decision getDecision(Variable variable)
	{
		Decision decision = null;

		for (Decision d : decisions)  // each Decision
		{
			try
			{
				Variable dv = d.getVariable();
				if ((variable != null) && (d.getVariable() == variable))
				{
					if (decision != null) throw new RuntimeException(
						"Multiple decisions for Variable " + variable.getName());
	
					decision = d;
				}
			}
			catch (ValueNotSet w) {}
		}

		return decision;
	}


	public synchronized Decision createDecision(Variable variable, Serializable value,
		DecisionPoint dp)
	throws
		CloneNotSupportedException
	{
		// First check that a Decision does not already exist for the Variable in
		// this DecisionScenario.

		if (getDecision(variable) != null)
			throw new RuntimeException(
				"Attempt to add a second Decision to Variable " + variable.getName() +
					" within DecisionScenario " + getName());


		// Ok to create the Decision.

		Decision decision = new DecisionImpl(this, value);
		decision.setVariable(variable);
		decisions.add(decision);

		if (getDecision(variable) == null)
		{
			throw new RuntimeException(
				"Unable to find a Decision that was just created.");
		}

		variable.addDecision(decision);

		dp.addDecision(decision);

		return decision;
	}


	public void deleteDecision(Decision decision)
	throws
		ParameterError
	{
		deleteChild(decision);
	}
	
	
	public synchronized Choice makeChoice(Variable variable, Serializable value)
	throws
		ParameterError
	{
		Choice choice = null;

		// First check that a Decision does not already exist for the Variable in
		// this DecisionScenario.

		Decision priorDecision = getDecision(variable);

		if (priorDecision != null)
		{
			if (priorDecision instanceof Choice)
			{
				// Assume that the new Choice is an overriding one: remove the
				// older Choice from this scenario.

				removeChoice((Choice)priorDecision);
			}
			else
			{
				throw new RuntimeException(
					"Attempt to add a conflicting Choice to Variable " +
						variable.getName() + " within DecisionScenario " + getName());
			}
		}


		try
		{
			choice = new ChoiceImpl(this, value);
		}
		catch (CloneNotSupportedException ex)
		{
			throw new ParameterError("Unable to copy the Variable's value");
		}

		choice.setVariable(variable);
		decisions.add(choice);

		variable.addDecision(choice);
		return choice;
	}


	public synchronized DecisionScenario cloneForSimulation()
	throws
		CloneNotSupportedException
	{
		DecisionScenarioImpl newDecisionScenario = 
			(DecisionScenarioImpl)(this.clone(new CloneMap(this), getDecisionDomain()));
			
		newDecisionScenario.setDerivedFrom(this);
		return newDecisionScenario;
	}


	public synchronized void setNativeImplementation(DecisionPoint dp,
		DecisionPoint.NativeDecisionPointImplementation impl)
	{
		nativeImpls.put(dp, impl);
	}
	

	public synchronized DecisionPoint.NativeDecisionPointImplementation
		getNativeImplementation(DecisionPoint decisionPoint)
	{
		return nativeImpls.get(decisionPoint);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		this.helper = new ScenarioHelper(this, new SuperScenario());
	}
	

  /* From DecisionScenarioChangeListener */
	

	public void decisionScenarioDeleted(DecisionScenario scenario)
	{
		if (scenario == getScenarioThatWasCopied())
			setScenarioThatWasCopied(null);
	}
	
	
  /* From DecisionScenarioSetChangeListener */	
	
	
	public void decisionScenarioSetDeleted(DecisionScenarioSet dss)
	{
		if (dss == getScenarioSet()) setScenarioSet(null);
	}
	
	
	public void decisionScenarioSetNameChanged(DecisionScenarioSet ds, String oldName)
	{
	}
	
	
  /* Internal */


	/**
	 * Remove the specified Choice from this DecisionScenario, and from any other
	 * reference lists to which it might belong.
	 */

	protected synchronized void removeChoice(Choice choice)
	{
		// Remove from this Scenario.

		decisions.remove(choice);

		// Detach from DecisionPoint.

		try { choice.getDecisionPoint().removeDecision(choice); }
		catch (ValueNotSet w) {}

		// Detach from Variable (if any).

		try { choice.getVariable().removeDecision(choice); }
		catch (ValueNotSet w) {}
	}
	
	
  /* Miscellaneous */
	
	
	public synchronized void dump(int indentation)
	{
		String padding = "";
		for (int i = 1; i <= indentation; i++) padding +="\t";
		GlobalConsole.println(padding + super.toString() + ": " + getName());
	}
	
	
  /* Proxy classes needed by ScenarioHelper */

	
	private class SuperScenario extends SuperPersistentNode
	{
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return DecisionScenarioImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return DecisionScenarioImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return DecisionScenarioImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return DecisionScenarioImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return DecisionScenarioImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return DecisionScenarioImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ return DecisionScenarioImpl.super.getSerClass(); }
		
		
		public Set<Attribute> getAttributes()
		{ return DecisionScenarioImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return DecisionScenarioImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.getAttributeSeqNo(attr); }


		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return DecisionScenarioImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return DecisionScenarioImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return DecisionScenarioImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return DecisionScenarioImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return DecisionScenarioImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return DecisionScenarioImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return DecisionScenarioImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return DecisionScenarioImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return DecisionScenarioImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return DecisionScenarioImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return DecisionScenarioImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ DecisionScenarioImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return DecisionScenarioImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ DecisionScenarioImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return DecisionScenarioImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return DecisionScenarioImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ DecisionScenarioImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return DecisionScenarioImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return DecisionScenarioImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return DecisionScenarioImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return DecisionScenarioImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return DecisionScenarioImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return DecisionScenarioImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return DecisionScenarioImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return DecisionScenarioImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ DecisionScenarioImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return DecisionScenarioImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return DecisionScenarioImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ DecisionScenarioImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return DecisionScenarioImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ DecisionScenarioImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return DecisionScenarioImpl.super.isVisible(); }
		
		
		public void layout()
		{ DecisionScenarioImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return DecisionScenarioImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ DecisionScenarioImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ DecisionScenarioImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ DecisionScenarioImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return DecisionScenarioImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ DecisionScenarioImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return DecisionScenarioImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ DecisionScenarioImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return DecisionScenarioImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ DecisionScenarioImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return DecisionScenarioImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return DecisionScenarioImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return DecisionScenarioImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ DecisionScenarioImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ DecisionScenarioImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return DecisionScenarioImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ DecisionScenarioImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return DecisionScenarioImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ DecisionScenarioImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return DecisionScenarioImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return DecisionScenarioImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return DecisionScenarioImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return DecisionScenarioImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ DecisionScenarioImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return DecisionScenarioImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ return DecisionScenarioImpl.super.getDefaultViewTypeName(); }
		
		public void setViewClassName(String name)
		{ DecisionScenarioImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return DecisionScenarioImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return DecisionScenarioImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return DecisionScenarioImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return DecisionScenarioImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return DecisionScenarioImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return DecisionScenarioImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ DecisionScenarioImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return DecisionScenarioImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return DecisionScenarioImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return DecisionScenarioImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return DecisionScenarioImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return DecisionScenarioImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return DecisionScenarioImpl.super.getX(); }
		public double getY() { return DecisionScenarioImpl.super.getY(); }
		public double getScale() { return DecisionScenarioImpl.super.getScale(); }
		public double getOrientation() { return DecisionScenarioImpl.super.getOrientation(); }
		public void setX(double x) { DecisionScenarioImpl.super.setX(x); }
		public void setY(double y) { DecisionScenarioImpl.super.setY(y); }
		public void setLocation(double x, double y) { DecisionScenarioImpl.super.setLocation(x, y); }
		public void setScale(double scale) { DecisionScenarioImpl.super.setScale(scale); }
		public void setOrientation(double angle) { DecisionScenarioImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return DecisionScenarioImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return DecisionScenarioImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return DecisionScenarioImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return DecisionScenarioImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ DecisionScenarioImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return DecisionScenarioImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return DecisionScenarioImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return DecisionScenarioImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return DecisionScenarioImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return DecisionScenarioImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return DecisionScenarioImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ DecisionScenarioImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return DecisionScenarioImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ DecisionScenarioImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ DecisionScenarioImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ DecisionScenarioImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return DecisionScenarioImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return DecisionScenarioImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return DecisionScenarioImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return DecisionScenarioImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return DecisionScenarioImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ DecisionScenarioImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return DecisionScenarioImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ DecisionScenarioImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ DecisionScenarioImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ DecisionScenarioImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return DecisionScenarioImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return DecisionScenarioImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return DecisionScenarioImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ DecisionScenarioImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return DecisionScenarioImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ DecisionScenarioImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return DecisionScenarioImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ DecisionScenarioImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ DecisionScenarioImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ DecisionScenarioImpl.super.printChildrenRecursive(indentationLevel); }
	}
}
