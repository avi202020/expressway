/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;

import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.DelaySer;
import expressway.ser.*;
import java.io.PrintWriter;
import java.io.IOException;


public class DelayImpl extends AbstractDelay
{
	/**
	 * Constructor.
	 */

	public DelayImpl(String name, ModelContainer parent, ModelDomain domain,
		long delay)
	{
		super(name, parent, domain, delay);
	}
	
	
	public DelayImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation) {} // ok
	

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DelayIconImageName);
	}
	
	
	public Class getSerClass() { return DelaySer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DelayImpl newInstance = (DelayImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.println(indentString + "<delay name=\"" + this.getName() + "\" "
			+ "time_delay=\"" + this.getDefaultDelay() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\">");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		writer.println(indentString + "</delay>");
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
}


class DelayNativeImpl extends AbstractDelayNativeImpl
{
}
