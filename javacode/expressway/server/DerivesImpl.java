/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.*;
import java.io.IOException;


public class DerivesImpl extends DirectedRelationImpl implements Derives
{
	public DerivesImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		setResizable(false);
	}


	public DerivesImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		setResizable(false);
	}


	public Class getSerClass() { return DerivesSer.class; }


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DerivesImpl newInstance = (DerivesImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DerivesIconImageName);
	}
}
