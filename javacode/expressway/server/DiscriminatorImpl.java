/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;

import expressway.common.ClientModel.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;

import java.util.Set;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import generalpurpose.TreeSetNullDisallowed;
import java.io.IOException;


public class DiscriminatorImpl extends ActivityBase implements Discriminator
{
	public static final Double DefaultThreshold = 0.0;
	

	public static final AttributeValidator ThresholdValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof Number)) throw new ParameterError(
				"Threshold value must be a Number");
			
			Number value = (Number)newValue;
			
			double doubleValue = value.doubleValue();
			if (doubleValue <= 0.0) throw new ParameterError("When validating '" +
				newValue.toString() + "', value must be positive");
		}
	};


	public String getTagName() { return "discriminator"; }
	
	
	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public Port controlPort = null;
	
	/** reference only. */
	public State state = null;  // the output state.
	
	/** Ownership. */
	public Double defaultThreshold = DefaultThreshold;  // discrimination threshold.
	
	/** reference only. */
	public ModelAttribute thresholdAttr = null;
	
	
	public String getInputPortNodeId() { return getNodeIdOrNull(inputPort); }
	public String getOutputPortNodeId() { return getNodeIdOrNull(outputPort); }
	public String getControlPortNodeId() { return getNodeIdOrNull(controlPort); }
	public String getStateNodeId() { return getNodeIdOrNull(state); }
	public String getThresholdAttrNodeId() { return getNodeIdOrNull(thresholdAttr); }
	

	public Class getSerClass() { return DiscriminatorSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DiscriminatorSer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((DiscriminatorSer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((DiscriminatorSer)nodeSer).controlPortNodeId = this.getControlPortNodeId();
		((DiscriminatorSer)nodeSer).stateNodeId = this.getStateNodeId();
		((DiscriminatorSer)nodeSer).defaultThreshold = this.getDefaultThreshold();
		((DiscriminatorSer)nodeSer).thresholdAttrNodeId = this.getThresholdAttrNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DiscriminatorImpl newInstance = (DiscriminatorImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.controlPort = cloneMap.getClonedNode(controlPort);
		newInstance.state = cloneMap.getClonedNode(state);
		newInstance.thresholdAttr = cloneMap.getClonedNode(thresholdAttr);
		newInstance.defaultThreshold = copyObject(defaultThreshold);
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.println(indentString + "<discriminator name=\"" + this.getName()  + "\""
			+ " threshold=\"" + this.defaultThreshold + "\""
			+ " width=\"" + this.getWidth() 
			+ "\" height=\"" + this.getHeight() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\">");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);


		writer.println(indentString + "</discriminator>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deletePort(controlPort, null);
		deleteState(state, null);
		deleteAttribute(thresholdAttr, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.

		// Nullify all references.
		inputPort = null;
		outputPort = null;
		controlPort = null;
		state = null;
		thresholdAttr = null;
		defaultThreshold = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public DiscriminatorImpl(String name, ModelContainer parent, ModelDomain domain,
		Double threshold)
	{
		super(name, parent, domain);
		init(threshold);
	}
	
	
	public DiscriminatorImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultThreshold);
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DiscriminatorIconImageName);
	}
	
	
	private void init(Double threshold)
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		//this.threshold = threshold;

		try
		{
			inputPort = super.createPort("input_port", PortDirectionType.input, 
				Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
			if (threshold == null)
				controlPort = super.createPort("control_port", PortDirectionType.input, 
					Position.bottom, this, null);
			else
				this.thresholdAttr = createModelAttribute("threshold", threshold, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		this.thresholdAttr.setValidator(ThresholdValidator);


		//inputPort.setSide(Position.left);
		//outputPort.setSide(Position.right);
		//if (controlPort != null) controlPort.setSide(Position.top);


		// Automatically bind the output Port to the internal State.

		try { state = super.createState("OutputState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		state.setX((this.getWidth() - state.getWidth())/2.0);
		state.setY(this.getHeight()/2.0);


		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		if (controlPort != null) controlPort.setMovable(false);
		state.setMovable(false);
		if (thresholdAttr != null) thresholdAttr.setMovable(false);
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		if (controlPort != null) controlPort.setPredefined(true);
		state.setPredefined(true);
		if (thresholdAttr != null) thresholdAttr.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 60.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }
	
	public Port getControlPort() { return controlPort; }
	
	public ModelAttribute getThresholdAttr() { return thresholdAttr; }
	
	public Double getDefaultThreshold() { return defaultThreshold; }

	public State getState() { return state; }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init(DefaultThreshold);
	}
}


class DiscriminatorNativeImpl extends ActivityNativeImplBase
{
	boolean stopped = false;
	double threshold;
	

	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);
		
		if (getDiscriminator().getThresholdAttr() != null)
			this.threshold = getAttributeLongValue(getDiscriminator().getThresholdAttr());
	}


	public synchronized void stop()
	{
		stopped = true;

		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		if (events.size() == 0) return;


		/*
		 * Discriminator must return its output to baseline (0) whenever its input
		 * falls below the threshold or when the threshold rises above the input.
		 * A pulse should be conveyed as a pulse. A discriminator's threshold
		 * must be > 0. Pulse Events on the Control Port should be treated as 
		 * conveying the pulse's height as the new threshold.
		 */
		
		
		// If there is an event on the control port (including a startup event),
		//	Read the control port and update the discrimination threshold state.
		//
		// If the control state is conducting,
		//	Create a true event on the output state.
		
		if (getControlPort() != null)
		{
			boolean controlIsNull = true;
			if (portHasNonCompensatingEvent(events, getControlPort()))
			{
				Serializable cVal = getActivityContext().getStateValue(getControlPort());
				
				if (cVal == null) controlIsNull = true;
				else
				{
					controlIsNull = false;
					if (! (cVal instanceof Number)) throw new ModelContainsError(
						"The value on a Discriminator's contol port must be numeric.");
					
					threshold = ((Number)cVal).doubleValue();
				}
			}
			
			
			if (controlIsNull) return; // do not respond when the control input
									// is null.
		}
		
		
		// Check if there is an event on either the control port or the input port.
		
		if
		(
			((getControlPort() != null) && portHasNonCompensatingEvent(events, 
				getControlPort()))
			  || 
				portHasEvent(events, getInputPort())
		)
		{		
			// Get value on input port.
			
			Serializable inputVal = getActivityContext().getStateValue(getInputPort());
			if (inputVal == null)
			{
				getActivityContext().setState(getDiscriminator().getState(), 
					new Boolean(false), events);
					
				return;
			}
			
			if (! (inputVal instanceof Number)) throw new ModelContainsError(
				"The value on the input port of a Discriminator must be numeric.");
			
			double value = ((Number)inputVal).doubleValue();
			
			
			// Determine if discrimination condition met.

			boolean newValue = false;
			if (value >= threshold) newValue = true;
			
			if (portHasNonCompensatingEvent(events, getInputPort()))
				getActivityContext().setState(getDiscriminator().getState(), 
					newValue, events);
			else
				getActivityContext().scheduleCompensationEvent(
					getDiscriminator().getState(), newValue, events);
		}
	}
	
	
	protected Port getInputPort() { return getDiscriminator().getInputPort(); }
	protected Port getControlPort() { return getDiscriminator().getControlPort(); }

	
	protected DiscriminatorImpl getDiscriminator() { return (DiscriminatorImpl)(getActivity()); }
}

