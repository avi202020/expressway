/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.server.HierarchyElement.*;
import expressway.server.MotifElement.*;
import expressway.ser.*;
import generalpurpose.SetVector;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Provide standard behavior for specialized Domain types to delegate to.
 */
 
public class DomainHelper extends ListNodeHelper
{
	private static final double PreferredWidthInPixels = 20.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	private MenuOwner superDomain;

	private String sourceName;
	private URL sourceURL;
	private String declaredName;
	private String declaredVersion;

	Scenario currentScenario = null;
	
	Set<MotifDef> motifDefs = new TreeSetNullDisallowed<MotifDef>();
	

	/** Ownership. */
	private List<MenuItem> menuItems = new Vector<MenuItem>();
	
	/** Ownership. */
	Set<NamedReference> namedReferences = new TreeSetNullDisallowed<NamedReference>();

	private VisualGuidance visualGuidance;

	
	DomainHelper(Domain domain, MenuOwner superDomain)
	{
		super(domain);
		this.superDomain = superDomain;
	}
	
	
	protected Domain getDomain() { return (Domain)(getListNode()); }


  /* From Domain */
  
	
	public void setScenarios(List<Scenario> ss)
	{
		getDomain().removeOrderedNodesOfKind(Scenario.class);
		try { getDomain().addAllAsOrderedNodes(ss); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public void setMenuItems(List<MenuItem> mis) { menuItems = mis; }
	
	
	public void setMotifDefs(Set<MotifDef> mds) { motifDefs = mds; }
	
	
	public void setScenarioSets(List<ScenarioSet> sss)
	{
		getDomain().removeOrderedNodesOfKind(ScenarioSet.class);
		try { getDomain().addAllAsOrderedNodes(sss); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public void setNamedReferences(Set<NamedReference> nrs) { namedReferences = nrs; }
	
	
	public void setVisualGuidance(VisualGuidance vg) { visualGuidance = vg; }


	public String getSourceName()
	{
		return sourceName;
	}
	
	
	public void setSourceName(String name)
	{
		this.sourceName = sourceName;
	}
	
	
	public URL getSourceURL()
	{
		return sourceURL;
	}
	
	
	public void setSourceURL(URL url)
	{
		this.sourceURL = url;
	}
	
	
	public String getDeclaredName()
	{
		return declaredName;
	}
	
	
	public void setDeclaredName(String name)
	{
		this.declaredName = name;
	}
	
	
	public String getDeclaredVersion()
	{
		return declaredVersion;
	}
	
	
	public void setDeclaredVersion(String version)
	{
		this.declaredVersion = version;
	}
	
	
	public Set<MotifDef> getMotifDefs() { return motifDefs; }
	
	
	public boolean usesMotif(MotifDef md) { return motifDefs.contains(md); }
		
		
	public void addMotifDef(MotifDef md)
	{
		motifDefs.add(md);
		
		// Add each MenuItem of the Motif to this Domain.
		List<MenuItem> mis = md.getMenuItems();
		for (MenuItem mi : mis)
		{
			CloneMap cloneMap = new CloneMap(md);
			MenuItem newmi;
			try { newmi = (MenuItem)(mi.clone(cloneMap, getDomain())); }
			catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
			
			this.menuItems.add(newmi);
		}
	}
		
	
	public void removeMotifDef(MotifDef md)
	{
		motifDefs.remove(md);
		
		// Remove each MenuItem of the Motif from this Domain, but only if the
		List<MenuItem> mis = new Vector<MenuItem>(this.menuItems);
		for (MenuItem mi : mis)
		{
			if (mi.getDefinedBy().getDomain() == md) this.menuItems.remove(mi);
		}
	}
	

	public Set<NamedReference> getNamedReferences() { return namedReferences; }
	
	
	public NamedReference getNamedReference(String nrName)
	{
		PersistentNode child;
		try { child = getDomain().getChild(nrName); }
		catch (ElementNotFound ex) { return null; }
		
		if (! (child instanceof NamedReference)) throw new RuntimeException(
			"Child '" + nrName + "' of '" + getDomain().getFullName() + "' is not a NamedReference");
		
		return (NamedReference)child;
	}

	
	public NamedReference createNamedReference(String name, String desc,
		Class fromType, Class toType)
	throws
		ParameterError
	{
		PersistentNodeImpl.checkName(name);
		try
		{
			getDomain().getChild(name);
			throw new ParameterError("Child with name '" + name + "' already exists");
		}
		catch (ElementNotFound ex) {}

		NamedReference nr = 
			NamedReference.createReference(name, desc, getDomain(), fromType, toType);
			
		namedReferences.add(nr);
		return nr;
	}

	
	public void deleteNamedReference(NamedReference nr)
	{
		try { getDomain().deleteChild(nr); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}
	
	
	public List<Scenario> getScenarios()
	{
		List<Scenario> ss = new Vector<Scenario>();
		List<PersistentNode> ns = getDomain().getOrderedNodesOfKind(Scenario.class);
		for (PersistentNode n : ns)
		{
			if (! (n instanceof Scenario)) throw new RuntimeException(
				"'" + n.getFullName() + "' is not a Scenario");
			ss.add((Scenario)n);
		}
		return ss;
	}
	
	
	public Scenario getScenario(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		PersistentNode node = getDomain().getOrderedChild(name);
		if (! (node instanceof Scenario)) throw new ParameterError(
			"Node with name '" + name + "' is not a Scenario");
		return (Scenario)node;
	}	


	public Scenario getCurrentScenario()
	{
		return currentScenario;
	}
	
	
	public void setCurrentScenario(Scenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		this.currentScenario = scenario;
	}
	
	
	public Scenario createScenario(String name)
	throws
		ParameterError
	{
		try
		{
			return getDomain().createScenario(name, null);
		}
		catch (CloneNotSupportedException ex)
		{
			throw new RuntimeException("Should not happen");
		}
	}
		

	public Scenario createScenario()
	throws
		ParameterError
	{
		try
		{
			return getDomain().createScenario(null, null);
		}
		catch (CloneNotSupportedException ex)
		{
			throw new RuntimeException("Should not happen");
		}
	}
		

	public void deleteScenario(Scenario scenario)
	throws
		ParameterError
	{
		getDomain().deleteChild(scenario);
	}
	
	
	public void addScenario(Scenario scenario)
	{
		try { getDomain().appendOrderedChild(scenario); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public void deleteScenarioSet(ScenarioSet scenarioSet)
	throws
		ParameterError
	{
		getDomain().deleteChild(scenarioSet);
	}
	
	
	public void addScenarioSet(ScenarioSet scenarioSet)
	{
		try { getDomain().appendOrderedChild(scenarioSet); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public List<ScenarioSet> getScenarioSets()
	{
		List<ScenarioSet> ss = new Vector<ScenarioSet>();
		List<PersistentNode> ns = getDomain().getOrderedNodesOfKind(ScenarioSet.class);
		for (PersistentNode n : ns)
		{
			if (! (n instanceof ScenarioSet)) throw new RuntimeException(
				"'" + n.getFullName() + "' is not a ScenarioSet");
			ss.add((ScenarioSet)n);
		}
		return ss;
	}


	public ScenarioSet getScenarioSet(String name)
	throws
		ElementNotFound
	{
		PersistentNode node = getDomain().getOrderedChild(name);
		if (! (node instanceof ScenarioSet)) throw new RuntimeException(
			"Node with name '" + name + "' is not a ScenarioSet");
		return (ScenarioSet)node;
	}
	
	
	public VisualGuidance getVisualGuidance()
	{
		if (this.visualGuidance == null)
			this.visualGuidance = DefaultVisualGuidance.guidance;
		
		return this.visualGuidance;
	}


  /* From PersistentNode */
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		Domain newDomain;
		try
		{
			newDomain = (Domain)(superDomain.clone(cloneMap, cloneParent));
		}
		catch (CloneNotSupportedException ce) { throw ce; }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		newDomain.setDomain(newDomain);
		try { newDomain.setCurrentScenario(cloneMap.getClonedNode(currentScenario)); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		newDomain.setScenarios(PersistentNodeImpl.cloneNodeList(getScenarios(), cloneMap, newDomain));
		newDomain.setMenuItems(PersistentNodeImpl.cloneNodeList(menuItems, cloneMap, newDomain));
		newDomain.setMotifDefs(PersistentNodeImpl.cloneNodeSet(motifDefs, cloneMap, newDomain));
		newDomain.setScenarioSets(PersistentNodeImpl.cloneNodeList(getScenarioSets(), cloneMap, newDomain));
		newDomain.setNamedReferences(PersistentNodeImpl.cloneNodeSet(namedReferences, cloneMap, newDomain));
		newDomain.setVisualGuidance(visualGuidance.makeCopy());

		return newDomain;
	}
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		Set<NamedReference> nrsCopy = new TreeSetNullDisallowed<NamedReference>(namedReferences);
		for (NamedReference nr : nrsCopy) try { getDomain().deleteChild(nr); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }

		try
		{
		Set<Scenario> scenariosCopy = new TreeSetNullDisallowed<Scenario>(getScenarios());
		for (Scenario scenario : scenariosCopy) getDomain().deleteScenario(scenario);
		
		SortedSet<ScenarioSet> scenSetsCopy = new TreeSetNullDisallowed<ScenarioSet>(getScenarioSets());
		for (ScenarioSet scenSet : scenSetsCopy) deleteScenarioSet(scenSet);

		Set<MenuItem> menuItemsCopy = new TreeSetNullDisallowed<MenuItem>(menuItems);
		for (MenuItem i : menuItemsCopy) getDomain().deleteMenuItem(i);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		superDomain.prepareForDeletion();

		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		namedReferences = null;
		menuItems = null;
	}


	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		// Add Scenarios, ScenarioSets, and MenuItems.
		try { return superDomain.getChild(name); }
		catch (ElementNotFound ex) {}  // ok
		
		try { return getDomain().getOrderedChild(name); }
		catch (ElementNotFound ex) {}  // ok
		
		for (MenuItem mi : menuItems)
			if (mi.getName().equals(name)) return mi;
		
		throw new ElementNotFound("'" + name + "', owned by '" + getDomain().getFullName() + "'");
	}


	public SortedSet<PersistentNode> getChildren()
	{
		SortedSet<PersistentNode> children = 
			(SortedSet<PersistentNode>)(superDomain.getChildren());
		children.addAll(getDomain().getOrderedChildren());
		children.addAll(menuItems);
		return children;
	}


	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		try
		{
			superDomain.deleteChild(child, outermostAffectedRef);
			return;
		}
		catch (ParameterError pe)
		{
			if (child instanceof MenuItem)
			{
				if (! menuItems.remove((MenuItem)child)) throw new RuntimeException(
					"Node '" + child.getName() + "' not found within '" + getDomain().getFullName() + "'");
			}
			else if (child instanceof NamedReference)
			{
				if (! namedReferences.remove((NamedReference)child))
					throw new RuntimeException(
					"Node '" + child.getName() + "' not found within '" + getDomain().getFullName() + "'");
			}
			else if (getDomain().removeOrderedChild(child))
			{
				outermostAffectedRef[0] = getDomain();
				if (child instanceof Scenario)
				{
					if (getCurrentScenario() == child)
						try { setCurrentScenario(null); }
						catch (ElementNotFound ex) { throw new RuntimeException("Should not happen"); }
					ServiceContext.getServiceContext().removeScenario((Scenario)child);
				}
			}
			else
				throw pe;
			
			getDomain().setRefArgIfNotNull(outermostAffectedRef, getDomain());
		}
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		NodeDomainSer ser = (NodeDomainSer)nodeSer;
		
		ser.setSourceName(sourceName);
		ser.setSourceURL(sourceURL);
		ser.setDeclaredName(declaredName);
		ser.setDeclaredVersion(declaredVersion);
		String[] nrNames = new String[namedReferences.size()];
		int i = 0; for (NamedReference nr : namedReferences) nrNames[i++] = nr.getName();
		ser.setNamedReferenceNames(nrNames);
		
		ser.setScenarioSetNodeIds(getDomain().createNodeIdArray(getScenarioSets()));
		ser.setCurrentScenarioNodeId(getDomain().getNodeIdOrNull(currentScenario));
		ser.setScenarioIds(getDomain().createNodeIdArray(getScenarios()));
		ser.setMotifDefNodeIds(getDomain().createNodeIdArray(motifDefs));
		MenuOwnerHelper.copySerValues(getDomain(), ser);
		ser.setMenuItemNodeIds(getDomain().createNodeIdArray(menuItems));

		return superDomain.externalize(nodeSer);
	}


  /* Other */
	
	
	public String createUniqueDomainName(String baseName)
	{
		ModelEngineLocal modelEngineLocal = 
			(ModelEngineLocal)(ServiceContext.getServiceContext().getModelEngine());
		
		if (baseName == null)
		{
			baseName = "";
		}
		else try
		{
			if (modelEngineLocal.getDomainPersistentNode(baseName) == null)
				
				return baseName;
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
		
		
		// Create a unique name that is based on the base name.
		
		int i = 0;
		for (;;) try
		{
			i++;
			String name = baseName + i;
		
			try { modelEngineLocal.getDomainPersistentNode(name); }
			catch (ElementNotFound enf)
			{
				return name;
			}
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}


	public void dump(int indentation)
	{
		superDomain.dump(indentation);
		
		String padding = "";
		for (int i = 1; i <= indentation; i++) padding +="\t";

		GlobalConsole.println(padding + super.toString() + ": " + getDomain().getName());

		GlobalConsole.println(padding + "\tScenarios:");
		List<Scenario> ss = getScenarios();
		for (Scenario scen : ss) scen.dump(indentation+1);
	}
}

