/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.ModelElement.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.util.Set;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;


/**
 * See the slide "Object Model for Domain Types".
 */
 
public class DomainType
	extends PersistentNodeImpl
	implements MotifDefChangeListener
{
	/** If true, this DomainType represents one of the built-in base Domain types. */
	private boolean builtin;
	
	/** The concrete class of the base Domain type. Should not be a MotifDef. */
	private Class domainClass;
	
	/** The MotifDefs that are automatically included when a Domain of this type
		is instantiated. */
	private Set<MotifDef> defaultMotifs;
	
	
	public DomainType(boolean builtin, Class domainClass, MotifDef... motifDefs)
	{
		super(null);
		this.builtin = builtin;
		if (MotifDef.class.isAssignableFrom(domainClass)) throw new RuntimeException(
			"Attempt to create a DomainType with a MotifDef as its base Domain class");
		this.domainClass = domainClass;
		defaultMotifs = new TreeSetNullDisallowed<MotifDef>();
		for (MotifDef md : motifDefs) defaultMotifs.add(md);
	}
	
	
	DomainType(String name, Class domainClass)
	throws
		ParameterError
	{
		super(null);
		setName(name);
		this.builtin = true;
		if (MotifDef.class.isAssignableFrom(domainClass)) throw new RuntimeException(
			"Attempt to create a DomainType with a MotifDef as its base Domain class");
		this.domainClass = domainClass;
		defaultMotifs = new TreeSetNullDisallowed<MotifDef>();
	}
	
	
	public boolean isBuiltin() { return builtin; }
	
	public Class getDomainClass() { return domainClass; }
	
	public Set<MotifDef> getDefaultMotifs() { return defaultMotifs; }
	
	public void addDefaultMotif(MotifDef md) { defaultMotifs.add(md); }
	
	public void removeDefaultMotif(MotifDef md) { defaultMotifs.remove(md); }
	
	
	public Domain createDomain(String domainName, boolean useNameExactly)
	throws
		ParameterError
	{
		Domain domain = getModelEngine().createDomain(domainClass, domainName, useNameExactly);
		for (MotifDef md : defaultMotifs) domain.addMotifDef(md);
		return domain;
	}
	
	
	public Domain createDomain()
	throws
		ParameterError
	{
		return getModelEngine().createDomainNode(this);
	}
	
	
	/**
	 * Create a MotifDef that correponds to the type of base Domain of this DomainType.
	 */
	 
	public MotifDef createMotifDef()
	throws
		ParameterError,
		IOException
	{
		ModelEngineLocal modelEngine = getModelEngine();
		if (ModelDomain.class.isAssignableFrom(domainClass))
			return (MotifDef)(modelEngine.createModelDomainMotifDefPersistentNode());
		else if (HierarchyDomain.class.isAssignableFrom(domainClass))
			return (MotifDef)(modelEngine.createHierarchyDomainMotifDefPersistentNode());
		else if (DecisionDomain.class.isAssignableFrom(domainClass))
			return (MotifDef)(modelEngine.createDecisionDomainMotifDefPersistentNode());
		else
			throw new RuntimeException("Unable to determine MotifDef type");
	}
	
	
	public DomainType getBaseDomainType()
	{
		return getModelEngine().getBaseDomainTypeForDomainClass(this.domainClass);
	}
	
	
  /* From PersistentNode */
	
	
	public String getTagName() { return "domain_type"; }
	
	
	public Class getSerClass() { return DomainTypeSer.class; }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		DomainType newInstance = (DomainType)(super.clone(cloneMap, cloneParent));
		newInstance.defaultMotifs = PersistentNodeImpl.cloneNodeSet(defaultMotifs, 
			cloneMap, newInstance);
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString 
			+ "<domain_type name=\"" + this.getName() + "\" "
			+ "base_domain_type=\"" + getBaseDomainType() + "\">");
		
		for (MotifDef md : defaultMotifs)
		{
			writer.println(indentString2 + "<uses motif=\"" + md.getFullName() + "\"/>");
		}
		
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		writer.println(indentString + "</domain_type>");
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		domainClass = null;
		defaultMotifs = null;
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new DomainTypeAttribute(name, defaultValue);
	}
	
	
	public Attribute constructAttribute(String name)
	{
		try { return new DomainTypeAttribute(name, null); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DomainTypeIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }

	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		 { return new TreeSetNullDisallowed<PersistentNode>(); }
	}
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 0; }
	
	
  /* From PersistentNodeImpl */
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		DomainTypeSer ser = (DomainTypeSer)nodeSer;
		
		ser.builtin = builtin;
		ser.domainTypeName = getName();
		ser.defaultMotifIds = new String[defaultMotifs.size()];
		int i = 0;
		for (MotifDef md : defaultMotifs) ser.defaultMotifIds[i++] = md.getNodeId();
		
		return super.externalize(ser);
	}


  /* From MotifDefChangeListener */


	public void motifDefDeleted(MotifDef md)
	{
		defaultMotifs.remove(md);
	}
	
	
  /* Attribute types */


	public class DomainTypeAttribute extends BasicAttribute
	{
		public DomainTypeAttribute(String name, Serializable value)
		throws
			ParameterError
		{
			super(name, null, value);
		}
		
		
		public String getTagName() { return "attribute"; }
		
		
		public Class getSerClass() { return DomainTypeAttributeSer.class; }
		
		
		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{
			return new DomainTypeAttribute(name, defaultValue);
		}
		
	
		public Attribute constructAttribute(String name)
		{
			try { return new DomainTypeAttribute(name, null); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
		}
	}
}

