/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;

import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ExpressionInterpreter.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import generalpurpose.XMLTools;
import generalpurpose.DateAndTimeUtils;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PushbackReader;
import java.io.StringReader;
import java.io.PrintWriter;
import java.io.IOException;

import expressway.tools.parser.*;
import expressway.tools.lexer.*;
import expressway.tools.node.*;
import expressway.tools.analysis.*;


public class DoubleExpressionImpl extends ActivityBase implements DoubleExpression
{
	public static final String DefaultExpr = "0";
	
	
	public static final AttributeValidator ExpressionValidator = new AttributeValidatorBase()
	{
		public synchronized void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (! (newValue instanceof String)) throw new ParameterError(
				"Expression value must be a String");
			
			String expr = ((String)newValue).trim();
			
			try
			{
				// Create expression lexer.
				Lexer lexer = new Lexer (new PushbackReader(new StringReader(expr)));
		
				// Parse expression string.
				(new Parser(lexer)).parse();
			}
			catch (Throwable t) { throw new ParameterError("When validating '" +
				newValue.toString() + "', " + t.getMessage()); }
		}
	};
	
	
	public String getTagName() { return "expression"; }
	
	
	/** reference only. */
	public Port[] inputPorts = null;
	
	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public State state = null;  // the output state.
	
	/** Ownership. */
	public String defaultExpr = DefaultExpr;  // internal modulation factor.
	
	/** reference only. */
	public ModelAttribute exprAttr = null;
	

	public String[] getInputPortNodeIds()
	{
		String[] inputPortNodeIds;
		int noOfInputPorts = inputPorts.length;
		inputPortNodeIds = new String[noOfInputPorts];
		for (int i = 0; i < noOfInputPorts; i++)
			inputPortNodeIds[i] = getNodeIdOrNull(inputPorts[i]);
		return inputPortNodeIds;
	}
	
	public String getOutputPortNodeId() { return getNodeIdOrNull(outputPort); }
	public String getStateNodeId() { return getNodeIdOrNull(state); }
	public String getExpressionAttrNodeId() { return getNodeIdOrNull(exprAttr); }
	
	
	public Class getSerClass() { return DoubleExpressionSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((DoubleExpressionSer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((DoubleExpressionSer)nodeSer).inputPortNodeIds = this.getInputPortNodeIds();
		((DoubleExpressionSer)nodeSer).stateNodeId = this.getStateNodeId();
		((DoubleExpressionSer)nodeSer).defaultExpr = this.getDefaultExpressionString();
		((DoubleExpressionSer)nodeSer).exprAttrNodeId = this.getExpressionAttrNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		DoubleExpressionImpl newInstance = (DoubleExpressionImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.state = cloneMap.getClonedNode(state);
		newInstance.exprAttr = cloneMap.getClonedNode(exprAttr);
		newInstance.inputPorts = cloneNodeArray(inputPorts, cloneMap, newInstance);
		newInstance.defaultExpr = copyObject(defaultExpr);
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<expression name=\"" + this.getName() + "\" "
			+ "expr=\"" + (defaultExpr == null ? "" : 
				XMLTools.encodeAsXMLAttributeValue(defaultExpr)) + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		

		// Write each input Port.
		
		List<Port> inputPorts = this.getInputPorts();
		for (Port port : inputPorts) writer.println(indentString2 +
			"<input_port name=\"" + port.getName() + "\"/>");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</expression>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(outputPort, null);
		deleteState(state, null);	
		deleteAttribute(exprAttr, null);
		for (Port p : inputPorts) deletePort(p, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.

		// Nullify all references.
		outputPort = null;
		state = null;	
		exprAttr = null;
		inputPorts = null;
		defaultExpr = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public DoubleExpressionImpl(String name, ModelContainer parent, ModelDomain domain,
		String defaultExpr, String[] inputPortNames)
	{
		super(name, parent, domain);
		if (defaultExpr == null) throw new RuntimeException("null Expression value");
		if (defaultExpr.trim().equals("")) defaultExpr = DefaultExpr;
		
		init(defaultExpr.trim(), inputPortNames);
	}

	
	public DoubleExpressionImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init(DefaultExpr, new String[0]);
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.DoubleExpressionIconImageName);
	}
	
	
	private void init(String defaultExpr, String[] inputPortNames)
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		this.defaultExpr = defaultExpr;

		try
		{
			this.exprAttr = createModelAttribute("Expression", defaultExpr, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		
		this.exprAttr.setValidator(ExpressionValidator);
		
		
		//outputPort.setSide(Position.right);


		// Automatically bind the output Port to the internal State.

		try { state = super.createState("OutputState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		//state.setX(getParent().getWidth()/2.0);
		//state.setY(getParent().getHeight()/2.0);


		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		
		// Create each input Port.
		
		if (inputPortNames == null) throw new RuntimeException("Null input ports");
		
		Set<Port> portSet = new TreeSetNullDisallowed<Port>();
		for (String portName : inputPortNames)
		{
			if (portName == null) throw new RuntimeException("Null input port name");
			if (portName.equals("")) throw new RuntimeException("Empty input port name");
			
			Port inputPort;
			try { portSet.add(inputPort = createPort(portName, PortDirectionType.input, 
				Position.left, this, null)); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
			
			//inputPort.setSide(Position.left);
			
			inputPort.setMovable(false);
		}
		
		inputPorts = portSet.toArray(new Port[portSet.size()]);
		
		outputPort.setMovable(false);
		state.setMovable(false);
		exprAttr.setMovable(false);
		
		outputPort.setPredefined(true);
		state.setPredefined(true);
		exprAttr.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	public void validateExpression(String expr)
	throws
		ParameterError
	{
		ExpressionValidator.validate(expr);
	}

	
	/** Override createPort so that all Ports created are input. */
	public Port createPort(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createNewInputPort(name, layoutBound, outermostAffectedRef);
	}
	

	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not a Port for this PortedContainer.
	{
		if (child == outputPort) throw new ParameterError(
			"Cannot delete the output port of an Expression.");
		
		super.deleteChild(child, outermostAffectedRef);
	}
	
		
	public void deletePort(Port port, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not a Port for this PortedContainer.
	{
		deleteChild(port, outermostAffectedRef);
	}
	

	/** ************************************************************************
	 * Create a new input Port for this DoubleExpression.
	 */
	 
	Port createNewInputPort(String portName, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if a Port with the specified name already exists.
	{
		double portDiam = getModelDomain().getVisualGuidance().getProbablePortDiameter();
		this.setHeight(this.getHeight() + (portDiam * 2.0));
		
		Port newPort = createPort(portName, PortDirectionType.input, Position.left,
			layoutBound, outermostAffectedRef);
		
		Port[] newInputPorts = new Port[inputPorts.length+1];
		for (int i = 0; i < inputPorts.length; i++) newInputPorts[i] = inputPorts[i];
		newInputPorts[inputPorts.length] = newPort;
		inputPorts = newInputPorts;
		newPort.setMovable(false);

		
		// Reroute and reposition all Conduits connected to this Tally, both
		// internally and externally.
		
		for (Port p : inputPorts) p.setSide(Position.left);
		outputPort.setSide(Position.right);
		
		return newPort;
	}
	

	void deleteInputPort(Port port, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not an input Port for this DoubleExpression.
	{
		// Verify that the Port is an input Port.
		
		boolean found = false;
		for (int i = 0; i < inputPorts.length; i++) if (inputPorts[i] == port)
		{
			found = true; break;
		}
			
		if (! found) throw new ParameterError(
			"Port " + port.getName() + " is not an input Port of " + this.getFullName());
		
		super.deleteChild(port, outermostAffectedRef);
	}


	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		if (port == outputPort) throw new ParameterError(
			"Cannot change Port direction for the output port of an Expression.");
		
		super.setPortDirection(port, dir);
	}

	
	public Port getOutputPort() { return outputPort; }
	
	public String getDefaultExpressionString() { return defaultExpr; }
	
	public State getState() { return state; }

	public ModelAttribute getExpressionAttr() { return exprAttr; }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init(DefaultExpr, new String[0]);
	}
}


class DoubleExpressionNativeImpl extends ActivityNativeImplBase
{
	boolean stopped = false;
	Start ast = null;
	String expr;
	

	public synchronized void start(ActivityContext context) throws Exception
	{
		if (stopped) throw new RuntimeException("Already stopped");
		super.start(context);
		
		expr = getAttributeStringValue(getExpression().getExpressionAttr());

		// Create expression lexer.
		Lexer lexer = new Lexer (new PushbackReader(new StringReader(expr)));

		// Parse expression string.
		Parser parser = new Parser(lexer);
		ast = parser.parse();
	}


	public synchronized void stop()
	{
		stopped = true;
		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);
		
		// Check that at least one input port has an event. Include startup
		// events.
		
		boolean hasInputEvents = false;
		boolean nonCompensatingInputEventExists = false;
		for (Port p : getExpression().getPorts())
		{
			if (p == getOutputPort()) continue;  // don't respond to Events on the output.
			if (portHasEvent(events, p))
			{
				if (! nonCompensatingInputEventExists)
					if (portHasNonCompensatingEvent(events, p))
						nonCompensatingInputEventExists = true;
				
				hasInputEvents = true;
				break;
			}
		}
		
		// Check if there are either no events (the initialization condition) or
		// there is at least one Event on an input Port.
		
		if (! ((events.size() == 0) || hasInputEvents)) return;


		/*
		 * Evaluate the expression.
		 */
		
		InterpreterScope.ClassScope classScope = new InterpreterScope.ClassScope()
		{
			public Package findJavaPackage(String name)
			{
				return ServiceContext.getMotifClassLoader().getPackage(name);
			}
		};


		DoubleExpressionInterpreter interpreter = new DoubleExpressionInterpreter(
			classScope, this, events);
		
		Serializable newValue = null;
		try
		{
			ast.apply(interpreter);
			double result = interpreter.getFinalDoubleResult();
			
			//System.out.println("Expression result: " + result);
			
			newValue = new Double(result);
		}
		catch (InterpretationError ie) { throw new ModelContainsError(ie.getMessage()); }
		catch (NullError ne) { newValue = null; }  // output is undefined.
		
		
		// Update output value.
		
		if (nonCompensatingInputEventExists)
			getActivityContext().setState(getExpression().getState(), newValue, events);
		else
			getActivityContext().scheduleCompensationEvent(getExpression().getState(), 
				newValue, events);
	}
	
	
	Port getPort(String name)
	throws ElementNotFound
	{
		ModelElement me = getExpression().getSubcomponent(name);
		if (! (me instanceof Port)) throw new ElementNotFound();
		
		return (Port)me;
	}
	
	
	protected DoubleExpressionImpl getExpression() 
	{ return (DoubleExpressionImpl)(getActivity()); }
	
	protected Port getOutputPort() { return getExpression().getOutputPort(); }
	
	
	/**
	 * For computing simulation-time expressions.
	 */
	 
	static class DoubleExpressionInterpreter extends ExpressionInterpreter
	{
		DoubleExpressionNativeImpl nativeImpl = null;
		SortedEventSet<GeneratedEvent> events;
		
		
		DoubleExpressionInterpreter(InterpreterScope.ClassScope classScope,
			DoubleExpressionNativeImpl nativeImpl, SortedEventSet<GeneratedEvent> events)
		{
			super(classScope);
			this.nativeImpl = nativeImpl;
			this.events = events;
		}
		
		
		protected double getPredefinedPathValue(List<String> pathParts)
		throws
			ModelContainsError, ElementNotFound
		{
			double value;
			
			try
			{
				value = super.getPredefinedPathValue(pathParts);
				return value;
			}
			catch (ElementNotFound enf) {}
			
			if (pathParts.size() != 1) throw new ModelContainsError(
				"Illegal name: " + PathUtilities.assemblePath(pathParts));
			
			String name = pathParts.get(0);
			
			
			// Identify the id. It must be either a Port or a predefined identfier.
			// (Ports have priority.)
			
			Port port = null;
			try
			{
				port = nativeImpl.getPort(name);
				
				// Obtain the value driving the Port.
				Serializable portValue = 
					nativeImpl.getStateValue(port);
				
				// Push the value onto the expression stack.
				if (portValue == null) throw new NullError(
					"Null value for " + port.getFullName());
				
				if (! (portValue instanceof Number)) throw new InterpretationError(
					"Expression value must be numeric");
				
				value = ((Number)portValue).doubleValue();
			}
			catch (ElementNotFound enf)
			{
				if (name.equals("EventElapsedTime"))
				{
					// Obtain the elapsed time of the Event from the simulation context.
					// If there is more than one triggering Event, use the oldest one.
					
					long oldestMs = 0;
					for (GeneratedEvent event : this.events) try
					{
						long ms = nativeImpl.getActivityContext().getElapsedMs((GeneratedEvent)event);
						oldestMs = Math.max(oldestMs, ms);
					}
					catch (Exception ex) { throw new InterpretationError(ex.getMessage()); }
					
					// Convert to days.
					double elapsedDays = ((double)oldestMs) / (double)(DateAndTimeUtils.MsInADay);
					value = elapsedDays;
				}
				else if (name.equals("SimulationElapsedTime"))
				{
					value = (double)(nativeImpl.getActivityContext().getElapsedSimulationMs())
						/ (double)(DateAndTimeUtils.MsInADay);
				}
				else
					throw new InterpretationError("Port " + name + " not found");
			}
			
			return value;
		}


		protected double callBuiltinFunction(String name, List argObjects)
		throws
			ModelContainsError
		{
			throw new InterpretationError("Function " + name + " not recognized");
		}
	}
}

