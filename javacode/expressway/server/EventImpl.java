/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.ser.*;
import java.util.Date;
import java.util.Set;
import java.io.*;


public class EventImpl implements Event
{
	public State state;


	/** Ownership. */
	public Date timeToOccur;
	
	
	/** Ownership. */
	public Serializable newValue = null;
	
	
	private boolean pulse = false;
	
	
	public EventSer externalize() throws ModelContainsError
	{
		EventSer eventSer;
		try { eventSer = (EventSer)(this.getSerClass().newInstance()); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		// Set all transient externalizable Node fields.
		
		
		// Set Ser fields.
		
		eventSer.stateNodeId = getState().getNodeId();
		eventSer.timeToOccur = this.timeToOccur;
		eventSer.pulse = this.pulse;
		
		if ((newValue != null) && (!(newValue instanceof Serializable))) 
		{
			ModelContainsError pe = new ModelContainsError(
			"Value of Event (" + this.toString() + ") does not implement Serializable;"
				+ " Event value is a " + newValue.getClass().getName()
				+ " with value " + newValue.toString());
			
			GlobalConsole.printStackTrace(pe);
			throw pe;
		}
		
		eventSer.newValue = newValue;
		
		return eventSer;
		//return super.externalize(nodeSer);
	}
	
	
	public Class getSerClass() { return EventSer.class; }
	
	
	//public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	//throws
	//	CloneNotSupportedException
	//{
	//	EventImpl newInstance = (EventImpl)(super.clone(cloneMap, cloneParent));
	//	
	//	return newInstance;
	//}


	public void prepareForDeletion()
	{
		this.timeToOccur = null;
		this.newValue = null;
	}
	
	
	public EventImpl(State state, Date timeToOccur, Serializable newValue)
	throws
		ParameterError
	{
		this(state, timeToOccur, newValue, false);
	}
	
	
	public EventImpl(State state, Date timeToOccur, Serializable newValue, boolean pulse)
	throws
		ParameterError
	//public EventImpl(String name, ModelElement parent)
	{
		//super(name, parent, parent.getModelDomain());
		//setResizable(false);
		this.state = state;
		this.timeToOccur = timeToOccur;
		this.pulse = pulse;

		try
		{
			this.newValue = PersistentNodeImpl.copyObject(newValue);
		}
		catch (CloneNotSupportedException ex)
		{
			throw new ParameterError(ex);
		}
	}


	public int getNoOfHeaderRows() { return 1; }
	
	
	private static final double PreferredWidthInPixels = 10.0;
	private static final double PreferredHeightInPixels = 10.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public Object clone() throws CloneNotSupportedException
	{
		EventImpl newEvent = (EventImpl)(super.clone());

		return newEvent;
	}
	
	
	public final Date getTimeToOccur() { return this.timeToOccur; }
	
	
	void setTimeToOccur(Date newDate) { this.timeToOccur = newDate; }
	
	
	public Serializable getNewValue() { return newValue; }
	
	
	void setNewValue(Serializable newValue) { this.newValue = newValue; }
	
	
	public State getState() { return state; }
	
	
	public boolean isPulse() { return pulse; }
		
		
	public void setPulse(boolean p) { this.pulse = p; }


	public String toString()
	{
		return
			getClass().getName() + " (" +
			(pulse? "pulse" : "flow") +
			"): State " + 
			(state == null ? "null" : state.toString()) + " -> " + 
			(newValue == null? "null" : newValue.toString()) + " @ " +
			(timeToOccur == null? "null" : timeToOccur.toString())
			;
	}
	
	
	public void dump(int indentation)
	{
		for (int i = 1; i <= indentation; i++) GlobalConsole.print("\t");
		GlobalConsole.println(
			this.getClass().getName() +
			": State=" + 
			(this.getState() == null? "null" : this.getState().getFullName()) + 
			", pulse=" +
			isPulse());
	}
	
	
	public static void dump(Set<Event> events, int indentation, String title)
	{
		for (int i = 1; i <= indentation; i++) GlobalConsole.print("\t");
		GlobalConsole.println(title);
		for (Event event : events)
		{
			((EventImpl)event).dump(indentation+1);
		}
	}
}
