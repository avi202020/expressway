/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.GlobalConsole;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;
import java.text.NumberFormat;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;


/**
 * Write the Event history of a Simulation Run in either HTML or tabbed format.
 */

public class EventWriter
{
	public static final int MaxFractionDigits = 2;
	public enum FormatType { tabbed, html, arc }
	
	
	/**
	 * Write the Event history of a Simulation Run in either HTML or tabbed format.
	 * Do not assume that the content written is the entire document: that is, do
	 * not write any header or terminator such as "<html>" or "</html>".
	 */
	 
	public static void writeEvents(FormatType formatType, String simRunName, 
		List<String> pathStrings,
		List<String> stateIds,
		List<GeneratedEvent>[] eventListArray, PrintWriter pw)
	throws
		IOException
	{
		boolean html = (formatType == FormatType.html);
		boolean tabbed = (formatType == FormatType.tabbed);
		
		NumberFormat numberFormat = NumberFormat.getInstance();

		numberFormat.setMaximumFractionDigits(MaxFractionDigits);
		
		if (pathStrings.size() != stateIds.size()) throw new IOException(
			"List of path Strings is different from length of State Ids");
		
		
		if (html) pw.println("<hr/>");
		
		//System.out.println("\t...for " + simRunName + "...");
		if (tabbed) pw.println("Sim run " + simRunName);
		if (html) pw.println("Sim run " + simRunName);
		
		if (html) pw.println("<table cellspacing=\"4\">");
		
		if (html)  // write a header row.
		{
			pw.println("<tr>");
			pw.print("<td>Iter</td>");  // leave a column for the iteration number.
			pw.print("<td>Sim Date/Time</td>");  // leave a column for the epoch time.
			for (String colTitle : pathStrings)  // each state (column)
			{
				pw.print("<td>");
				pw.print(colTitle.replace(".", ". "));
				pw.print("</td>");
			}
			pw.println("</tr>");
		}
		
		SimulationTimeEvent[] drivingEvents = new SimulationTimeEvent[pathStrings.size()];

		int epochNo = 0;
		for (List<GeneratedEvent> eventList : eventListArray)  // each epoch.
		{
			epochNo++;
			
			boolean wrongNumberOfValues = false;
			/*
			if (eventList.size() != pathStrings.size())
			{
				GlobalConsole.println("EventWriter Error: " + eventList.size() +
					" Events in epoch no. " + epochNo + "; but there are " + 
					pathStrings.size() + " States");
				wrongNumberOfValues = true;
			}*/
			
			// Write a table row.
			
			if (html)
			{
				pw.println("<tr>");
				
				// Write the iteration number column.
				
				//if (wrongNumberOfValues) pw.print("<td bgcolor=\"red\">");
				//else 
					pw.print("<td>");
				
				if (eventList.size() > 0)
				{
					GeneratedEvent firstEvent = eventList.get(0);
					if (firstEvent.getEpoch() != null)
						pw.print(firstEvent.getIteration());
				}
										
				pw.print("</td>");
				

				// Write the epoch time column.
				
				if (wrongNumberOfValues) pw.print("<td bgcolor=\"red\">");
				else pw.print("<td>");
				
				if (eventList.size() > 0)
				{
					GeneratedEvent firstEvent = eventList.get(0);
					if (firstEvent.getEpoch() != null)
						pw.print(firstEvent.getEpoch().getDate());
				}
				
				pw.print("</td>");
			}
			
			int stateNo = -1;
			for (String stateId : stateIds)
			//for (Event event : eventList)  // each column
			{
				stateNo++;
				
				// Write a table column entry for this row.

				boolean found = false;
				SimulationTimeEvent event = null;
				for (SimulationTimeEvent e : eventList)
				{
					if (e.getState().getNodeId().equals(stateId))
						// the Event source (State) matches the column
					{
						found = true;
						event = e;
						drivingEvents[stateNo] = e;
						break;
					}
				}
				
				if (html)
					if (event == null)
						pw.print("<td>");
					//else if (event instanceof NoChangeEvent)
					//	pw.print("<td>");
					else if (event instanceof EventConflict)
						pw.print("<td bgcolor=\"red\">");
					else
						pw.print("<td bgcolor=\"yellow\">");
				
				//State state = event.getState();
				//Serializable value = event.getNewValue();
				
				if (event == null)
				{
					SimulationTimeEvent drivingEvent = drivingEvents[stateNo];
					String s = formatObjectValue(
						drivingEvent == null? null : drivingEvent.getNewValue(),
							numberFormat, epochNo);
					if (tabbed) pw.print(s);
					if (html) pw.print(s);

					if (tabbed) pw.print(" (no change)");
					if (html) pw.print(" (no change)");
				}
				else if (event instanceof EventConflict)
				{
					if (tabbed) pw.print(" (conflict)");
					if (html) pw.print(" (conflict)");
				}
				else
				{
					String s = formatObjectValue(event.getNewValue(), numberFormat, epochNo);
					if (tabbed) pw.print(s);
					if (html) pw.print(s);
				}
				
				if (tabbed) pw.print(" \t");
				if (html) pw.print(" \t");

				if (html) pw.print("</td>");
			}
			
			if (tabbed) pw.println();
			if (html) pw.println();
			
			if (html) pw.println("</tr>");
		}
		
		if (tabbed) pw.println();
		if (html) pw.println();
		
		if (html) pw.println("</table>");
	}


	protected static String formatObjectValue(Serializable value, NumberFormat numberFormat,
		int epochNo)
	{
		if (value == null) return("(null)");
		else if (value instanceof Number) try {
			return numberFormat.format(value); }
		catch (IllegalArgumentException iae)
		{
			GlobalConsole.println("EventWriter Error: for epoch no. " + epochNo +
				", formatting Number '" + value.toString() + "'");
			return "<err>";
		}
		else if (value instanceof Boolean) {
			if (value.equals(Boolean.TRUE)) return "true";
			else return "false"; }
		else {
			GlobalConsole.println("EventWriter Error: for epoch no. " + epochNo +
				" Unable to format object of type " + value.getClass().getName());
			return "(" + value.toString() + ")"; }
	}
}

