/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import generalpurpose.ThrowableUtil;
import generalpurpose.JarUtils;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import java.io.*;
import java.util.List;
import java.util.Vector;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import org.xml.sax.SAXParseException;


/**
 * Main program for locally running Expressway and the XML interpreter.
 *
 * Options:
 	//-random_seed=<number>
	-val_archive_file=<file path>
	-val_element=<fully qualified name of a Model Element Node>
	-domain=<name of model domain to validate>
	-scenario=<name of model scenario to validate>
	-motif=<path of a motif JAR file>
	
 * Arguments:
 	<model_file_path>
 */
 
public class Expressway
{
	public static void main(final String[] args)
	{
		/*
		 * Set properties needed by RMI.
		 */
		
		String classpath = System.getProperty("java.class.path");
		String rmiCodebase = classpath.replace(':', ' ').replace(';', ' ');
			
		System.setProperty("java.rmi.server.codebase", rmiCodebase);
		
		GlobalConsole.println("java.rmi.server.codebase=" + System.getProperty(
			"java.rmi.server.codebase"));
			
					
		/*
		 * Retrieve and validate input options.
		 */
		
		int noOfOptions = 0;
		//long randomSeed = 0;
		//boolean randomSeedProvided = false;
		String validationArchiveFilePath = null;
		String eventSerFilePath = null;
		List<String> nodePaths = new Vector<String>();
		String modelDomainName = null;
		String modelScenarioName = null;
		List<String> jarFilePaths = new Vector<String>();
		String importFile = null;
		String dbdir = null;
		List<String> libraryPathStrs = new Vector<String>();
		
		for (int i = 0; i < args.length; i++)
		{
			if (args[i].substring(0,1).equals("-"))
			{
				noOfOptions++;
				String optionString = args[i].substring(1);
				String[] optionParts = optionString.split("=");
				
				/*
				if (optionParts[0].equals("random_seed"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option random_seed must specify a seed value");
						return;
					}
					
					try { randomSeed = Long.parseLong(optionParts[1]); }
					catch (NumberFormatException nfe)
					{
						GlobalConsole.println(
							"Option random_seed must specify a long value");
						return;
					}
					
					randomSeedProvided = true;
				}
				else */
					
				if (optionParts[0].equals("motif"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option motif must specify a JAR file path");
						return;
					}
					
					jarFilePaths.add(optionParts[1]);
					
				}
				else if (optionParts[0].equals("library"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option library must specify the file path of a Java JAR file.");
						return;
					}
					
					libraryPathStrs.add(optionParts[1]);
				}
				else if (optionParts[0].equals("val_archive_file"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option val_archive_file must specify a file path");
						return;
					}
					
					validationArchiveFilePath = optionParts[1];
				}
				else if (optionParts[0].equals("val_element"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option val_element must specify a model element path");
						return;
					}
					
					nodePaths.add(optionParts[1]);
				}
				else if (optionParts[0].equals("domain"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option  must specify a model domain");
						return;
					}
					
					modelDomainName = optionParts[1];
				}
				else if (optionParts[0].equals("scenario"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"Option  must specify a model scenario");
						return;
					}
					
					modelScenarioName = optionParts[1];
				}
				else if (optionParts[0].equals("import"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"The import option must specify an XML file path");
						return;
					}
					
					importFile = optionParts[1];
				}
				else if (optionParts[0].equals("dbdir"))
				{
					if (optionParts.length != 2)
					{
						GlobalConsole.println(
							"The dbdir option must specify a directory path");
						return;
					}
					
					dbdir = optionParts[1];
				}
				
			}
			else break;  // past the options.
		}
		
		
		/*
		 * Validate options.
		 */
		 
		if (validationArchiveFilePath != null)
		{
			if (modelDomainName == null)
			{
				GlobalConsole.println("To validate, must specify a domain");
				return;
			}
			
			if (modelScenarioName == null)
			{
				GlobalConsole.println("To validate, must specify a scenario");
				return;
			}
		}
		
		
		/*
		 * Retrieve and validate input arguments.
		 */
		 
		String uri = args[noOfOptions];
		
		
		/*
		 * Load the database specified by the -dbdir option, if any.
		 */
		
		ModelEngineLocalPojoImpl modelEngineLocal = null;
		if (dbdir == null) dbdir = System.getProperty("user.dir");

		// Read in and reconstitute a Model Engine previously serialized.
		
		File dbDirFile = new File(dbdir);
		if (dbDirFile.exists()) try
		{
			File dbfile = ModelEngineLocalPojoImpl.findDatabaseFile(dbDirFile);
			modelEngineLocal = ModelEngineLocalPojoImpl.loadDatabase(dbfile, new File(dbDirFile, "simrundir"));
		}
		catch (Exception ex)  // did not find a database file
		{
			try
			{
				//if (randomSeedProvided) 
				//	modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile, randomSeed);
				//else
					modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile);
			}
			catch (IOException ex2)
			{
				GlobalConsole.printStackTrace(ex2);
				return;
			}
		}
		else try // No database file exists
		{
			if ((! dbDirFile.exists()) && (! dbDirFile.mkdir())) throw new IOException(
				"Counld not create directory " + dbDirFile);
			
			//if (randomSeedProvided) 
			//	modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile, randomSeed);
			//else
				modelEngineLocal = new ModelEngineLocalPojoImpl(dbDirFile);
		}
		catch (Exception ex)
		{
			GlobalConsole.println(
				"While creating database directory or instantiating server local instance: " +
				ThrowableUtil.getAllMessages(ex));
			GlobalConsole.printStackTrace(ex);
			return;
		}
		
		modelEngineLocal.reinitialize();
		
		
		/*
		 * Establish a ModelEngine service context for this thread, so that it can
		 * make calls to the ModelEngine.
		 */
		
		// Add motif directory to codebase, by setting the context Class Loader.
		MotifClassLoader motifClassLoader = new MotifClassLoader();
		ServiceContext.setMotifClassLoader(motifClassLoader);
		ServiceContext.create(modelEngineLocal, ModelAPITypes.StandaloneClientId);
		
		
		/*
		 * Register a PeerListener to receive Notices from asynchronous processing
		 * threads.
		 */
		
		try
		{
			ListenerRegistrar registrar = modelEngineLocal.registerPeerListener(
				ServiceContext.getCurrentClientId(), new PeerListener()
				{
					public String getId() throws IOException
					{
						return "StandaloneClient Listener";
					}
					
					public void notify(PeerNotice notice)
					throws
						NotInterested,
						InconsistencyError,
						IOException
					{
						GlobalConsole.println(
							"Received " + notice.getClass().getName());
					}
				});
		}
		catch (Exception ex)
		{
			GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
			return;
		}		
		
		
		/*
		 * Load the file specified by the -import option, if any.
		 */
		
		if (importFile != null)
		{
			XMLParser xmlParser = new XMLParser();
			try
			{
				xmlParser.parse(importFile);
				xmlParser.setModelEngineLocal(modelEngineLocal);
				xmlParser.setClientId(ServiceContext.getCurrentClientId());
				xmlParser.genDocument();
				System.err.println("Done parsing input file");
			}
			catch (SAXParseException spe)
			{
				GlobalConsole.println("At line " + spe.getLineNumber() +
					", column " + spe.getColumnNumber() + ", " + spe.getMessage());
				GlobalConsole.printStackTrace(spe);
			}
			catch (Exception ex)
			{
				GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
				GlobalConsole.printStackTrace(ex);
				return;
			}
			finally
			{
				List<String> warnings = xmlParser.getWarnings();
				GlobalConsole.println("Warnings generated:");
				for (String warning : warnings) GlobalConsole.println("\t" + warning);
			}
		}
		
		
		/*
		 * Process libraries, if any.
		 */
		
		for (String libraryPathStr : libraryPathStrs)
		{
			try
			{
				File jarFile = JarUtils.getJarFile(libraryPathStr);
				byte[] byteAr = JarUtils.getBytes(jarFile);
			
				ModelEngineLocal.JarProcessor jarProcessor = new JarProcessorImpl(
					modelEngineLocal,
					false, // boolean replace,
					null, // no PeerListener because this call is synchronous.
					ModelAPITypes.StandaloneClientId, // String clientId,
					byteAr);
				
				jarProcessor.processJar(false /* not XML */);
			}
			catch (Exception ex)
			{
				GlobalConsole.println("While processing library option, " +
					ThrowableUtil.getAllMessages(ex));
				return;
			}
		}


		/*
		 * Process the JAR files, if any.
		 */
		
		for (String jarFilePath : jarFilePaths) try
		{
			File jarFile = JarUtils.getJarFile(jarFilePath);
			byte[] jarByteAr = JarUtils.getBytes(jarFile);
			
			ModelEngineLocal.JarProcessor jarProcessor = modelEngineLocal.getJarProcessor(
				true /* replace */, null, //PeerListener
				ServiceContext.getCurrentClientId(),
				jarByteAr);
			
			jarProcessor.processJar();
		}
		catch (Exception ex)
		{
			GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
			return;
		}

		
		/*
		 * Parse the XML input.
		 */
		
		XMLParser xmlParser = new XMLParser();
			
		xmlParser.setModelEngineLocal(modelEngineLocal);
		xmlParser.setClientId(ServiceContext.getCurrentClientId());

		try { xmlParser.parse(uri); }
		catch (SAXParseException spe)
		{
			GlobalConsole.println("At line " + spe.getLineNumber() +
				", column " + spe.getColumnNumber() + ", " + spe.getMessage());
			GlobalConsole.printStackTrace(spe);
		}
		catch (Exception ex)
		{
			GlobalConsole.println("While parsing XML input: " + ThrowableUtil.getAllMessages(ex));
			GlobalConsole.printStackTrace(ex);
			return;
		}
		

		/*
		 * Traverse the XML Document and generate the required Persistent Nodes.
		 * If the XML contains <simulate> statements, this will cause simulations
		 * to be run.
		 */
		
		try { xmlParser.genDocument(); }
		catch (Exception ex)
		{
			GlobalConsole.println(ThrowableUtil.getAllMessages(ex));
			GlobalConsole.printStackTrace(ex);
			return;
		}
		finally
		{
			List<String> warnings = xmlParser.getWarnings();
			GlobalConsole.println("Warnings generated:");
			for (String warning : warnings) GlobalConsole.println("\t" + warning);
		}
		
		
		/*
		 * Validate the results, if requested.
		 */
		
		if (validationArchiveFilePath != null)
		{
			GlobalConsole.println("Validating against file " + validationArchiveFilePath);
			
			try
			{
				if (nodePaths.size() == 0)
					Validator.validate(ServiceContext.getCurrentClientId(), modelEngineLocal, 
					modelDomainName, modelScenarioName, validationArchiveFilePath);
				else
					Validator.validate(ServiceContext.getCurrentClientId(), modelEngineLocal,
						modelDomainName, modelScenarioName, nodePaths, validationArchiveFilePath);
			}
			catch (ResultMismatch rm)
			{
				GlobalConsole.println("Validation found a discrepancy: " + 
					ThrowableUtil.getAllMessages(rm));
				
				System.exit(1);
			}
			catch (ModelContainsError mce)
			{
				GlobalConsole.println("Validation found a problem with the model: " + 
					ThrowableUtil.getAllMessages(mce));
				
				System.exit(2);
			}
			catch (Throwable t)
			{
				GlobalConsole.printStackTrace(t);
				System.exit(-1);
			}
		}
		
		
		
		// for test only:
		/*
		XMLWriter xmlWriter = new XMLWriter(modelEngineLocal);
		java.io.File f = new java.io.File("TestXMLWriter.xml");
		try { xmlWriter.writeLibrary(f); }
		catch (java.io.IOException ex) { GlobalConsole.printStackTrace(ex); }
		*/
		
		System.exit(0);
	}
}

