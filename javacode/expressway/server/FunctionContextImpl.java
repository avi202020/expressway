/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Date;
import java.io.Serializable;



/**
 * Not a persistent class.
 */

public class FunctionContextImpl extends ModelContextImpl implements FunctionContext
{
	FunctionContextImpl(SimulationRunImpl simRun, ModelComponent component)
	{
		super(simRun, component);
	}


	public GeneratedEvent scheduleFutureStateChange(State state, Date time,
		Serializable value, SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ModelContainsError,
		ParameterError // if the specified time is earlier than
			// the current time.
	{
		throw new ModelContainsError(
			"Attempt by a Function to schedule a future event.");
	}


	public GeneratedEvent scheduleDelayedStateChange(State state,
		long msDelay, Serializable value, SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ModelContainsError,
		ParameterError
	{
		throw new ModelContainsError(
			"Attempt by a Function to schedule a delayed event.");
	}
}

