/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.util.*;
import java.io.PrintWriter;
import java.io.IOException;


public class FunctionImpl extends FunctionBase
	implements MenuOwner, ModelTemplate, TemplateInstance
{
	public FunctionImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		setResizable(true);
		setUseBorderWhenIconified(true);
	}


	public FunctionImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		setResizable(true);
		setUseBorderWhenIconified(true);
	}
	
	
	public String getTagName() { return "function"; }
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		setHTMLDescription(null);
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		//if (modelTemplate != null) modelTemplate.templateInstanceDeleted(this);
		
		// Nullify all references.
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		return super.externalize(nodeSer);
	}


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		FunctionImpl newInstance = (FunctionImpl)(super.clone(cloneMap, cloneParent));
		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<function name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		
		// Write MenuItems, if any.
		
		List<MenuItem> mis = getMenuItems();
		if (mis != null)
		{
			for (MenuItem mi : mis) mi.writeAsXML(writer, indentation+1);
		}
		
		
		// Write each Port.
		
		List<Port> ports = this.getPorts();
		for (Port port : ports) port.writeAsXML(writer, indentation+1);
		
		
		// Write each State.
		
		List<State> states = this.getStates();
		for (State state : states) state.writeAsXML(writer, indentation+1);
		

		// Write each child component.
		
		Set<ModelComponent> children = this.getSubcomponents();
		for (ModelComponent child : children)
		{
			writer.println();
			child.writeAsXML(writer, indentation+1);
		}
		
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</function>");
	}
	

	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		return super.getChild(name);
	}
	
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		return super.getChildren();
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		super.deleteChild(child, outermostAffectedRef);
	}

	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return super.isOwnedByActualTemplateInstance();
	}
	
	
	/** For use by PersistentNodeImpl.clone(). */
	//FunctionImpl() { this(null, null); }


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.FunctionIconImageName);
	}
	
	
	public List<String> getInstantiableNodeKinds()
	{
		List<String> names = new Vector<String>(super.getInstantiableNodeKinds());
		
		return names;
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
  /* From TemplateInstance */
	

	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		getModelContainer().initFunction(this, nativeImplClass, layoutBound, outermostAffectedRef);
	}
}

