/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.MathException;

import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.io.Serializable;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;


public class FunctionNativeImplBase implements Function.JavaFunctionImplementation
{
	private ModelComponent modelComponent = null;
	private FunctionContext context = null;

	public void setModelComponent(ModelComponent modelComponent)
	{
		this.modelComponent = modelComponent;
	}


	public FunctionBase getFunction() { return (FunctionBase)(context.getComponent()); }


	public ModelComponent getModelComponent() { return modelComponent; }


	public void start(FunctionContext context) throws Exception
	{
		this.context = context;
	}


	public void stop() {}


	public SortedEventSet<GeneratedEvent> getAndPurgeNewEvents()
	{
		return context.getAndPurgeNewEvents();
	}


	public void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
	}
	
	
	/**
	 * Return true of the owning component is current in "watch" mode, which
	 * requires that changes to its state and its actions are reported to
	 * the client.
	 */
	 
	protected boolean watch()
	{
		return modelComponent.getWatch();
	}


	protected void notifyAllListeners(PeerNotice peerNotice)
	{
		try { modelComponent.notifyAllListeners(peerNotice); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}


	protected FunctionContext getFunctionContext() { return context; }


	public ModelContext getModelContext() { return context; }


	/**
	 * Return the Events that are entering through the specified Port
	 * in this epoch.
	 */
	 
	protected SortedEventSet<GeneratedEvent> getEventsOnPort(Port port, 
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		SortedEventSet<GeneratedEvent> portEvents = new SortedEventSetImpl();
		
		for (GeneratedEvent event : events)
		{
			State state = event.getState();
			if (state == null) continue; // skip Startup events.
			
			Set<Port> connectedPorts = 
				getFunctionContext().getSensitivePorts(state);
				//getFunctionContext().getConnectedPorts(state);
			
			//if ((connectedPorts.contains(port)) && port.allowsInputEvents())
			if (connectedPorts.contains(port))
				portEvents.add(event);
		}
		
		return portEvents;
	}
	
	
	/**
	 * If there is an Event on the specified Port in this epoch, return true.
	 * If includeStartup is true, then include the startup condition; otherwise
	 * do not. The startup condition is when events is empty: the assumption
	 * is that the Function is only activated with an empty event set when
	 * the simulation first starts.
	 */
	 
	protected boolean portHasEvent(SortedEventSet<GeneratedEvent> events, Port port, 
		boolean includeStartup)
	throws
		ModelContainsError
	{
		// Check for startup condition.
		if (includeStartup && (events.size() == 0)) return true;
		
		// Check each Event, to see if it is traversing the specified Port.
		
		SortedEventSet<GeneratedEvent> portEvents = getEventsOnPort(port, events);
		
		for (GeneratedEvent event : portEvents)
		{
			if (event instanceof StartupEvent) // this should never happen, since
				// we now prevent startup events from reaching the Activity. This
				// is here in legacy and should be removed.
			{
				if (includeStartup) return true;
			}
			else
			{
				//System.out.println("...has a non-startup event");
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Return the value that is currently driving each Conduit that is connected
	 * to the specified Port. If the Port is a "black" Port, then the values may
	 * conflict; otherwise they may not. The Set that is returned may contain null
	 * entries, indicating null State values.
	 */
	 
	protected Set<Serializable> readAllConduits(Port port)
	throws
		ModelContainsError,
		ParameterError
	{
		Set<Serializable> values = new HashSet<Serializable>();
		Set<Conduit> externalConduits = port.getExternalConnections();
		for (Conduit conduit : externalConduits)
		{
			Port otherPort;
			try
			{
				if (conduit.getPort1() == port) otherPort = conduit.getPort2();
				else otherPort = conduit.getPort1();
			}
			catch (ValueNotSet w) { throw new ModelContainsError(w); }
			
			State state = context.getCurrentDriver(otherPort);
			values.add(context.getStateValue(state));
		}
		
		return values;
	}


	/**
	 * Retrieve the (String) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected String getAttributeStringValue(ModelAttribute attr)
	{
		ModelScenario scenario = getFunctionContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof String)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a String: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return (String)value;
	}


	/**
	 * Retrieve the (double) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected boolean getAttributeBooleanValue(ModelAttribute attr)
	{
		ModelScenario scenario = getFunctionContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof Boolean)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a boolean: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return ((Boolean)value).booleanValue();
	}


	/**
	 * Retrieve the (double) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected double getAttributeDoubleValue(ModelAttribute attr)
	{
		ModelScenario scenario = getFunctionContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof Double)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a double: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return ((Double)value).doubleValue();
	}


	/**
	 * Retrieve the (long) value of the specified Attribute, in the context of
	 * the current Scenario that is being simulated.
	 */
	 
	protected long getAttributeLongValue(ModelAttribute attr)
	{
		ModelScenario scenario = getFunctionContext().getSimulationRun().getModelScenario();
		Serializable value = null;
		try { value = scenario.getAttributeValue(attr); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (! (value instanceof Long)) throw new RuntimeException(
			"Attribute " + attr.getFullName() + " is not a long: is " +
				(value == null ? "null" : value.toString() + " (" +
					value.getClass().getName() + ")"));
		
		return ((Number)value).longValue();
	}
}
