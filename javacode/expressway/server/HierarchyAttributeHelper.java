/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.HierarchyElement.*;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;


public class HierarchyAttributeHelper extends AttributeHelper
{
	/**
	 * Find the HierarchyAttribute (if any) within the subtree of node, that
	 * has the same name as the specified HierarchyAttribute. If none found,
	 * return null. If there is more than one, simply return the first one found.
	 * Recursive.
	 */
	 
	public static HierarchyAttribute findNestedAttrWithSameName(PersistentNode node,
		HierarchyAttribute ha)
	{
		for (PersistentNode child : hier.getChildren())
		{
			Attribute matchingAttr = child.getAttribute(ha.getName());
			if (matchingAttr != null) return matchingAttr;
			
			matchingAttr = findNestedAttrWithSameName(child, ha);  // recursive
			if (matchingAttr != null) return matchingAttr;
		}
		
		return null;
	}
	
	
	/**
	 * Create a HierarchyAttribute for each Hierarchy child of ha's
	 * parent (recursively), and set the definedBy field of each created
	 * HierarchyAttribute to reference ha. Acts based on the isDefining Attributes
	 * that belong to ha's parent and its parents. Idempotent.
	 */
	
	public static void applyToHierarchy(HierarchyAttribute ha)
	throws
		ParameterError
	{
		ha.setDefining(true);
		
		/* Create a HierarchyAttribute for each Hierarchy child of this Attribute's
		  parent (recursively), and sets the definedBy field of each created
		  HierarchyAttribute to the target. */
		
		PersistentNode parent = ha.getParent();
		PersistentNode layoutBound = parent;
		PersistentNode[] outermostAffectedRef = new PersistentNode[1];
		outermostAffectedRef[0] = null;
		
		// Recursively creates a HierarchyAttribute for each child of ha's parent.
		Set<PersistentNode> children = parent.getChildren();
		for (PersistentNode child : children)
		{
			if (child instanceof Attribute) continue;
			if (! (child instanceof Hierarchy)) continue;
			Hierarchy hc = (Hierarchy)child;
			
			// See if hc already has an attribute defined by ha. If so, do nothing.
			Attribute a = hc.getAttribute(ha.getName());
			if ((a != null) && a.getDefinedBy() == this) continue;
			if (a != null) throw new RuntimeException(
				"Unexpected error: child " + ha.getFullName() + 
				" already has an attribute with the name " + ha.getName());
			
			newa = hc.createHierarchyAttribute(ha, layoutBound, outermostAffectedRef);
			applyToHierarchy(newa);
		}
	}
	
	
	/**
	 * Remove from each child of ha's parent (and each child of
	 * that, recursively) any HierarchyAttribute that references ha.
	 * The values of any removed Attribute will also be removed from its Scenarios.
	 */
	
	public static void removeFromHierarchy(HierarchyAttribute ha)
	throws
		ParameterError
	{
		/* Remove from each child of this Attribute's parent (and each child of
		   that, recursively) any HierarchyAttribute that references this Attribute.
		   The values of any removed Attribute will also be removed from its Scenarios. */
		
		ha.setDefining(false);
		ha.setDefinedBy(null);
		
		PersistentNode parent = ha.getParent();
		if (parent == null) return;
		
		Set<PersistentNode> children = parent.getChildren();
		for (PersistentNode child : children)
		{
			if (child instanceof Attribute) continue;
			if (! (child instanceof Hierarchy)) continue;
			Hierarchy hc = (Hierarchy)child;

			Set<Attribute> peerAttrs = hc.getAttributes();
			Set<Attribute> peerAttrsCopy = new TreeSetNullDisallowed<Attribute>(peerAttrs);
			for (Attribute attr : peerAttrsCopy)
			{
				if (attr instanceof HierarchyAttribute)
				{
					hca = (HierarchyAttribute)attr;
					if ((hca.getDefinedBy() == ha) || (hca.getDefinedBy() == ha.getDefinedBy()))
						hca.removeFromHierarchy();
			
					hc.deleteAttribute(attr);
				}
			}
		}
	}
}

