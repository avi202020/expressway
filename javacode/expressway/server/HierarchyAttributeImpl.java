/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;


public class HierarchyAttributeImpl
	extends HierarchyElementImpl
	implements HierarchyAttribute, Cloneable
{
	private Serializable defaultValue;
	private AttributeValidator validator;
	private HierarchyAttribute definedBy = null;
	
	
  /* Constructors */
  
	
	HierarchyAttributeImpl(String baseName, HierarchyElement parent)
	{
		super(baseName, parent);
	}
	
	
	HierarchyAttributeImpl(String baseName, HierarchyElement parent,
		Serializable defaultValue)
	{
		super(baseName, parent);
		this.defaultValue = defaultValue;
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.AttributeIconImageName);
	}
	
	
	public String getTagName() { return "attribute"; }
	
	
	public Class getSerClass() { return HierarchyAttributeImplSer.class; }
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
  /* From HierarchyElementImpl */


	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
	}
	
	
  /* From HierarchyAttribute */


	public void setDefining(boolean newValue)
	throws
		DisallowedOperation
	{
		if (newValue && (getParent() instanceof Attribute)) throw new DisallowedOperation(
			"An Attribute cannot have defining Attributes");
		
		if (! newValue) HierarchyAttributeHelper.removeFromHierarchy(this);
		this.defining = true;
	}
	
	
	boolean isDefining() { return this.defining; }
	
		
	public HierarchyAttribute getDefinedBy() { return definedBy; }
	
	
	public void setDefinedBy(HierarchyAttribute definedBy)
	throws
		ParameterError
	{
		if (definedBy == null)
			this.definedBy = null;
		else
		{
			if (definedBy == this) throw new ParameterError(
				"Attempt to set definedBy Attribute to itself");
			if (! (getParent().isDescendantOf(definedBy.getParent())))
				throw new ParameterError(
					"Parent of '" + getFullName() + "' is not a descendant of '" +
					definedBy.getParent().getFullName() + "'");
			if (getParent().getAttribute(definedBy.getName()) != null)
				throw new ParameterError("'" + getParent().getFullName() + 
					" already has an Attribute with the name '" + definedBy.getName() + "'");
			this.definedBy = definedBy;
		}
	}
	
	
	public void applyToHierarchy()
	throws
		ParameterError, DisallowedOperation
	{
		if (getParent() instanceof Attribute) throw new DisallowedOperation(
			"An Attribute cannot have defining Attributes");
		
		Hierarchy hierParent = (Hierarchy)(getParent());
		
		a = HierarchyAttributeHelper.findNestedAttrWithSameName(hierParent, this);
		if ((a != null) && a.getDefinedBy() == this) return;
		if (a != null) throw new DisallowedOperation(
			"An attribute with that name is already owned by " + a.getParent().getFullName());
		
		try { HierarchyAttributeHelper.applyToHierarchy(this); }
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public void removeFromHierarchy()
	throws
		ParameterError
	{
		if (! isDefining()) throw new ParameterError(
			"Attempt to remove a non-defining Attribute as if it were defining");
		
		HierarchyAttributeHelper.removeFromHierarchy(this);
	}
	
	
  /* From PersistentNode and PersistentNodeImpl */
	
	
	@Override
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		HierarchyAttributeSer ser = (HierarchyAttributeSer)nodeSer;
		ser.setDefaultValue(defaultValue);
		//ser.setValue(value);
		return super.externalize(ser);
	}
	
	
	@Override
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		HierarchyAttributeImpl newInstance =
			(HierarchyAttributeImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.defaultValue = copyObject(defaultValue);
		//newInstance.value = copyObject(value);
		newInstance.validator = getValidator().makeCopy();
		if (definedBy != null)
		{
			newInstance.definedBy = cloneMap.getClonedNode(definedBy);
			if (! newInstance.getParent().isDescendantOf(newInstance.definedBy.getParent()))
			{
				try { newInstance.setDefinedBy(null); }
				catch (ParameterError pe) { throw new RuntimeException(pe); }
			}
		}
		
		return newInstance;
	}

	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		writeAsXML(writer, indentation, null);
	}
	
	
	@Override
	public final void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
	throws
		IOException
	{
		if (this.isPredefined()) return;

		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		if (this.getName() == null) throw new IOException(new Exception(
			"Attribute name is null; parent is " + this.getParent().getFullName()));
		
		writer.print(indentString + "<attribute name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ (definedBy == null ? "" : "defined_by=\"" + definedBy.getFullName() + "\" ")
			+ (appliesTo == null? "" : "for=\"" + appliesTo.getName() + "\" ")
			+ (defaultValue == null? "" : "default=\"" + defaultValue + "\" ")
			+ (defaultValue == null? "" : 
				(isParsableJavaType(defaultValue) ? "" :
					("default_type=\"" + defaultValue.getClass().getName() + "\" "))));

		writeSpecializedXMLAttributes(writer);

		writer.println("/>");
				
		
		writeSpecializedXMLElements(writer, indentation);


		// Write each Attribute of this Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</attribute>");
	}

	
	@Override
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		defaultValue = null;
		//value = null;
		validator = null;
		definedBy = null;
	}
	
	
	@Override
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		return super.getChild(name);
	}
	

	@Override
	public SortedSet<PersistentNode> getChildren()
	{
		return super.getChildren();
	}
	
	
	public String getName()
	{
		if (definedBy == null) return super.getName();
		else return definedBy.getName();
	}
	
	
	public void setName(String name)
	throws
		ParameterError
	{
		if (definedBy == null) super.setName(name);
		else definedBy.setName(name);
	}
	
	
  /* From Attribute */
  
  
	public Serializable getDefaultValue()
	{
		return defaultValue;
	}


	public void setDefaultValue(Serializable defaultValue)
	{
		this.defaultValue = defaultValue;
	}
	
	
	public void setValue(Scenario scenario, Serializable value)
	throws
		ParameterError
	{
		scenario.setAttributeValue(this, value);
	}
	
	
	public Serializable getValue(Scenario scenario)
	throws
		ParameterError
	{
		return scenario.getAttributeValue(this);
	}
	
	
	public void setValidator(AttributeValidator validator)
	{
		this.validator = validator;
	}
	
	
	public AttributeValidator getValidator()
	{
		return validator;
	}
	
	
	public void validate(Serializable newValue)
	throws
		ParameterError
	{
		if (validator != null) validator.validate(newValue);
	}


	/**
	 * Return true if the specified Object is of a type that is allowed in a
	 * ModelAttribute expression.
	 */
	 
	protected boolean isParsableJavaType(Object obj)
	{
		return AttributeHelper.isParsableJavaType(obj);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
	
	
  /* From AttributeChangeListener */


	public void attributeDeleted(Attribute attribute)
	{
		if (definedBy != null)
		{
			if (definedBy == attribute)  // This Attribute should also be deleted.
			{
				try { getParent().deleteAttribute(this); }
				catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
			}
		}
	}
	
	
	public void attributeNameChanged(Attribute attribute, String oldName)
	{
	}
}

