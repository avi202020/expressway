/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.ser.*;
import generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.File;
import java.io.IOException;

	
public class HierarchyDomainMotifDefImpl extends HierarchyDomainImpl implements HierarchyDomainMotifDef
{
	/** ownwership. */
	private String[] classNames = null;
		
	
	public Class getSerClass() { return HierarchyDomainMotifDefSer.class; }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		HierarchyDomainMotifDefImpl newInstance = (HierarchyDomainMotifDefImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		this.classNames = null;
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set Ser fields.
		
		HierarchyDomainMotifDefSer ser = (HierarchyDomainMotifDefSer)nodeSer;
		
		//ser.motifJarFileName = (this.jarFile == null? "" :
		//	this.jarFile.getAbsolutePath());
		ser.motifClassNames = this.classNames;

		MenuOwnerHelper.copySerValues(this, (HierarchyDomainMotifDefSer)nodeSer);

		//ser.templateNodeIds = createNodeIdArray(templates);
		
		
		return super.externalize(nodeSer);
	}


	HierarchyDomainMotifDefImpl(String name)
	{
		super(name);
		this.setViewClassName("expressway.gui.hier.HierarchyMotifViewPanel");
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.MotifDefIconImageName);
	}
	
	
	public Set<Template> getTemplates()
	{
		Set<Template> templates = new HashSet<Template>();
		Set<PersistentNode> children = getChildren();
		for (PersistentNode elt : children)
		{
			if (elt instanceof Template)
				templates.add((Template)elt);
			else throw new RuntimeException(
				"MotifDef contains a HierarchyElement that is not a Template");
		}
		
		return templates;
	}
	
	
	public Template getTemplate(String name)
	throws
		ElementNotFound
	{
		Set<Template> templates = getTemplates();
		for (Template t : templates)
		{
			if (t.getName().equals(name)) return t;
		}
		
		throw new ElementNotFound("Template named '" + name + "'");
	}
	
	
	public Set<Domain> getReferencingDomains()
	{
		List<HierarchyDomain> mds;
		try { mds = getModelEngine().getHierarchyDomainPersistentNodes(); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		Set<Domain> refDomains = new TreeSetNullDisallowed<Domain>();
		for (HierarchyDomain d : mds)
		{
			if (d.usesMotif(this)) refDomains.add(d);
		}
		
		return refDomains;
	}
	
	
	public void setMotifClassNames(String[] classNames) {this.classNames = classNames; }
	
	
	public String[] getMotifClassNames() { return this.classNames; }
}

