/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.NamedReference.*;
import java.util.Set;
import java.util.SortedSet;
import java.util.List;
import java.io.Serializable;


/**
 * Nodes that define a hierarchy of ListNodes. See the slide "Hierarchy".
 */
 
public interface HierarchyElement extends CrossReferenceable
{
	HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;


	HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	HierarchyAttribute createHierarchyAttribute(HierarchyAttribute definingAttr,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError;
	
	
	HierarchyDomain getHierarchyDomain();
		
		
	interface HierarchyDomain extends Hierarchy, Domain
	{
		HierarchyScenarioSet createHierarchyScenarioSet(String name, 
			HierarchyScenario baseScenario, HierarchyAttribute attr, 
			Serializable[] values)
		throws
			ParameterError,
			CloneNotSupportedException;


		List<HierarchyScenario> getHierarchyScenarios();
		
		
		HierarchyScenario getHierarchyScenario(String name)
		throws
			ElementNotFound, // if the HierarchyScenario cannot be found.
			ParameterError;
		
		
		void addHierarchyScenario(HierarchyScenario scenario);
		
		
		HierarchyScenarioSet getHierarchyScenarioSet(String name)
		throws
			ElementNotFound,
			ParameterError;
			
			
		/**
		 * Return this Domain's HierarchyScenarioSets.
		 */
		 
		List<HierarchyScenarioSet> getHierarchyScenarioSets();
		
		
		void addHierarchyScenarioSet(HierarchyScenarioSet s);
		
		
		void deleteHierarchyScenarioSet(HierarchyScenarioSet s)
		throws
			ParameterError;
	
	
		/**
		 * Retrieve the HierarchyScenario that the user considers to be the "current"
		 * one at this moment. I.e., the user desires that the current scenario be
		 * used when simuation is performed on the HierarchyDomain, or when the
		 * simulator needs to obtain the value of a State from another HierarchyDomain
		 * and it needs to determine which HierarchyScenario to choose for that
		 * other HierarchyDomain.
		 * May return null.
		 */
	
		HierarchyScenario getCurrentHierarchyScenario();
	
	
		void setCurrentHierarchyScenario(HierarchyScenario scenario)
		throws
			ElementNotFound,
			ParameterError;
	}
	
	
	interface HierarchyScenario extends Hierarchy, Scenario,
		HierarchyScenarioChangeListener,
		HierarchyScenarioSetChangeListener
	{
		/**
		 * Return the HierarchyScenarioSet that this HierarchyScenario is a member of.
		 * May be null.
		 */
		 
		HierarchyScenarioSet getHierarchyScenarioSet();
		
		
		/** Accessor. */
		void setHierarchyScenarioSet(HierarchyScenarioSet scenSet);
		
		
		HierarchyScenarioHelper getHierarchyScenarioHelper();
	}
	
	
	interface HierarchyScenarioSet extends Hierarchy, ScenarioSet,
		HierarchyScenarioChangeListener
	{
		List<HierarchyScenario> getHierarchyScenarios();
	}
	
	
	interface HierarchyAttribute extends HierarchyElement, Attribute, AttributeChangeListener
	{
		/** If set to true, then this HierarchyAttribute may be referenced by other
			HierarchyAttributes as defining. */
		void setDefining(boolean newValue)
		throws
			DisallowedOperation;  // If 'newValue' is false, and this Attribute's
				// descendants have HierarchyAttributes that are defined by this Attribute.
		
		boolean isDefining();
		
		/** See the slide "Hierarchy Attributes". If non-null, then the Attribute's
			characteristics (e.g., name) are defined by the referenced Attribute.. */
		HierarchyAttribute getDefinedBy();
		
		void setDefinedBy(HierarchyAttribute definedBy)
		throws
			ParameterError;
			
		/** Creates a HierarchyAttribute for each Hierarchy child of this Attribute's
			parent (recursively), and set the definedBy field of each created
			HierarchyAttribute to reference this Attribute. Acts based on the isDefining
			Attributes that belong to this Attribute's parent and its parents. Idempotent. */
		void applyToHierarchy()
		throws
			ParameterError,
			DisallowedOperation;  // if a child in the parent's tree already has an
				// Attribute with the same name as this Attribute.
		
		/** Remove from each child of this Attribute's parent (and each child of
			that, recursively) any HierarchyAttribute that references this Attribute.
			Acts based on the isDefining Attributes that belong to this Attribute's
			parent and its parents.
			The values of any removed Attribute will also be removed from its Scenarios. */
		void removeFromHierarchy()
		throws
			ParameterError;
	}
	
	
	interface HierarchyDomainMotifDef extends HierarchyDomain, MotifDef
	{
	}
	
	
	interface HierarchyTemplate extends Template
	{
	}


	interface Hierarchy extends HierarchyElement,
		HierarchyTemplate, TemplateInstance, PersistentListNode
	{
		int getNoOfSubHierarchies();
		
		Hierarchy getChildHierarchyAt(int index) throws ParameterError;
		
		int getIndexOfChildHierarchy(Hierarchy child);
		
		void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError;
		
		boolean appendChildHierarchy(Hierarchy child) throws ParameterError;
		
		boolean removeChildHierarchy(Hierarchy child) throws ParameterError;
		
		/**
		 * Add HierarchyAttributes that are needed by this HierarchyElement
		 * and its children, that they do not yet have.  See the slide "Hierarchy Attributes"
		 * in the pattern set "Composition".
		 */
		void addHierarchyAttributes();

		
		/**
		 * Remove all HierarchyAttributes that are owned by this HierarchyElement and
		 * its children, that are defined by (reference) Attributes above this
		 * HierarchyElement in the hierarchy.
		 */
		void removeHierarchyAttributes();
		
		
		/**
		 * Create a child instance for this Hierarchy. The child should be of an
		 * appropriate class to be a child of this Hierarchy. Should ensure that
		 * the newly constructed Node's Domain field is set.
		 */
		 
		Hierarchy constructSubHierarchy(String name)
		throws
			DisallowedOperation;
		
		
		/**
		 * Set the location of this Hierarchy so that it is not overlapping any other
		 * Nodes inside its parent. Attempt to locate it between its immediately prior
		 * and next sibling. Other Nodes may be moved to make room.
		 */
		 
		public void setReasonableLocation();
		
		
		/**
		 * For use by ModelEngine methods that support Graphic Views.
		 */
		 
		Hierarchy createSubHierarchy(String name, int position,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		
		/**
		 * For use by ModelEngine in support of Tabular Views.
		 */
		 
		Hierarchy createSubHierarchy(String name, int position)
		throws
			ParameterError;
			
			
		/** For use by ModelEngine in support of Tabular Views.
		 * Create a new Hierarchy at the end of this Hierarchy's List of
		 * Hierarchies.
		 */
			
		Hierarchy createSubHierarchy(String name)
		throws
			ParameterError;
		
		
		/**
		 * For use by ModelEngine in support of client "paste" operations.
		 */
		 
		Hierarchy createSubHierarchy(String name, int position, Scenario sceanrio,
			PersistentListNode copiedNode)
		throws
			ParameterError;
		
		
		/**
		 * For use by ModelEngine in support of client "paste" operations.
		 */
		 
		Hierarchy createSubHierarchy(int position, Scenario scenario,
			PersistentListNode copiedNode)
		throws
			ParameterError;
		
		
		/**
		 * To support the instantiation of TemplateInstances that define new
		 * types of Hierarchies.
		 */
		 
		Hierarchy createSubHierarchy(String name, int position, 
			Scenario scenario, HierarchyTemplate template)
		throws
			ParameterError;
		
		
		void deleteHierarchy(Hierarchy hier)
		throws
			ParameterError;
		
		
		/**
		 * Set the position of this Hierarchy among its parent's sub-Hierarchies.
		 * Position 0 is the first position. There may be no gaps: that is, if the
		 * last element of the list is at position 10, one may not add an element
		 * at position 12.
		 */
		 
		void changePosition(int pos)
		throws
			ParameterError;
		
		
		/**
		 * Return the ordinal position of this Hierarchy among its sibling
		 * Hierarchy Nodes. This normally excludes Attributes and Scenarios.
		 */
		 
		int getPosition()
		throws
			ParameterError;
		
		
		Hierarchy getPriorHierarchy();
		
		
		Hierarchy getNextHierarchy();
		
		
		int getNoOfHierarchies();
		
		
		Hierarchy getParentHierarchy();
		
		
		/**
		 * Change the owning Hierarchy for this Hierarchy. Note that the outermost
		 * Hierarchy is always a HierarchyDomain.
		 */
		 
		void setParentHierarchy(Hierarchy parent)
		throws
			ParameterError;
		
		
		/**
		 * Return all child Nodes that implement Hierarchy. It is assumed that
		 * Attribute and Scenarios do not implement Hierarchy.
		 */
		 
		List<Hierarchy> getSubHierarchies();
		
		
		List<Hierarchy> getSubHierarchies(Class kind);
		
		
		Hierarchy getSubHierarchy(String name)
		throws
			ElementNotFound;
	}


	interface HierarchyElementChangeListener
	{
		void hierarchyElementDeleted(HierarchyElement element);
		void hierarchyElementNameChanged(HierarchyElement element, String oldName);
	}
	
	
	interface HierarchyScenarioChangeListener
	{
		void hierarchyScenarioDeleted(HierarchyScenario s);
	}
	
	
	interface HierarchyScenarioSetChangeListener
	{
		void hierarchyScenarioSetDeleted(HierarchyScenarioSet s);
	}
}

