/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.awt.Dimension;
import java.awt.Graphics2D;


public abstract class HierarchyElementImpl
	extends TemplatizableNode
	implements HierarchyElement
{
	private String viewClassName = null;
	
	
  /* Constructors */
  
  
	/**
	 * Used to instantiate new Elements
	 * that will then be attributed by the user. This constructor must provide
	 * reasonable values for any required fields. Parent may not be null.
	 */
	 
	protected HierarchyElementImpl(String baseName, HierarchyElement parent)
	{
		super(parent);
		if (parent == null)
		{
			if (this instanceof Domain)
			{
				this.setDomain((Domain)this);
				try
				{
					if (this instanceof MotifDef)
						this.setName(getModelEngine().createUniqueMotifName(baseName));
					else
						this.setName(getModelEngine().createUniqueDomainName(baseName));
				}
				catch (ParameterError pe) { throw new RuntimeException(pe); }
			}
			else throw new RuntimeException("Null parent");
		}
		else
		{
			this.setDomain((HierarchyDomain)(parent.getDomain()));
			try { this.setName(((HierarchyElementImpl)parent).createUniqueChildName(baseName)); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
		}
	}
	
	
  /* From HierarchyElement */


	public HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, defaultValue, layoutBound,
			outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}


	public HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(HierarchyAttribute definingAttr,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyElementHelper.createHierarchyAttribute(this, definingAttr, layoutBound,
			outermostAffectedRef);
	}
	
	
	public HierarchyDomain getHierarchyDomain() { return (HierarchyDomain)(getDomain()); }
	
	
  /* From PersistentNodeImpl */
	
	
	public abstract Class getSerClass();
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		HierarchyElementSer ser = (HierarchyElementSer)nodeSer;
		ser.setViewClassName(viewClassName);
		return super.externalize(ser);
	}
	
	
	public byte[] renderAsSVG(double wCmMax, double hCmMax)
	throws
		IOException
	{
		return RenderingHelper.renderHierarchyAsSVG(this, wCmMax, hCmMax);
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new HierarchyAttributeImpl(name, this, defaultValue);
	}
	

	public Attribute constructAttribute(String name)
	{
		return new HierarchyAttributeImpl(name, this);
	}


  /* From PersistentNode */
	
	
	@Override
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		HierarchyElementImpl newInstance =
			(HierarchyElementImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}

	
	@Override
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	@Override
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		return super.getChild(name);
	}
	

	@Override
	public SortedSet<PersistentNode> getChildren()
	{
		return super.getChildren();
	}


	@Override
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}


	public int getNoOfHeaderRows() { return 0; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		return new TreeSetNullDisallowed<PersistentNode>();
	}
	
	
	public double getPreferredWidthInPixels() { return 200; }
	

	public double getPreferredHeightInPixels() { return 50; }


  /* From PersistentNode */


  /* Implementation */


	/**
	 * Add HierarchyAttributes that are needed by this HierarchyElement and its
	 * children, that they do not yet have, based on the isDefining Attributes
	 * that belong to this HierarchyElement and its parents.
	 */
	
	protected void addHierarchyAttributes(Hierarchy parent)
	{
		if (this instanceof HierarchyAttribute) throw new RuntimeException(
			"Cannot apply hierarchy attributes to an attribute");
		
		/* Traverse upward to identify Attributes that are needed, calling
			applyToHierarchy() on each one. */
		
		for (PersistentNode parent = this; parent != null; parent = parent.getParent())
		{
			if (! (parent instanceof HierarchyElement)) return;  // done
			for (Attribute a : parent.getAttributes())
			{
				HierarchyAttribute ha = (HierarchyAttribute)a;
				if (ha.isDefining()) ha.applyToHierarchy();
			}
		}
	}


	/**
	 * Remove all HierarchyAttributes that are owned by this HierarchyElement
	 * and its children, based on the isDefining Attributes
	 * that belong to this HierarchyElement and its parents.
	 */
	
	protected void removeHierarchyAttributes(Hierarchy parent)
	{
		/* Traverse upward to identify Attributes that are needed, calling
			removeFromHierarchy() on each one. */
		
		for (PersistentNode parent = this; parent != null; parent = parent.getParent())
		{
			if (! (parent instanceof HierarchyElement)) return;  // done
			for (Attribute a : parent.getAttributes())
			{
				HierarchyAttribute ha = (HierarchyAttribute)a;
				if (ha.isDefining()) ha.removeFromHierarchy();
			}
		}
	}
}

