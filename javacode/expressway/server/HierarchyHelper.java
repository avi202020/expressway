/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.awt.Dimension;
import java.awt.Graphics2D;


public class HierarchyHelper extends HierarchyElementHelper
{
	private static final int ChildOffsetX = 20;
	private static final int ChildOffsetY = 20;
	private Hierarchy hier;
	private HierarchyElement superHier;
	
	
	public HierarchyHelper(Hierarchy hier, HierarchyElement superHier)
	{
		this.hier = hier;
		this.superHier = superHier;
	}
	
	
	public HierarchyElement getSuperHier() { return superHier; }
	

	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		List<Attribute> attrs = hier.getAttributesInSequence();
		String[] fieldNames = new String[attrs.size()];
		int i = 0;
		for (Attribute attr : attrs) fieldNames[i++] = attr.getName();
		
		HierarchySer ser = (HierarchySer)nodeSer;
		List<Hierarchy> subHierarchies = getSubHierarchies(hier, Hierarchy.class);
		ser.setSubHierarchyNodeIds(hier.createNodeIdArray(subHierarchies));
		ser.setFieldNames(fieldNames);
		return superHier.externalize(ser);
	}
	

	public static void setReasonableLocation(Hierarchy hier)
	{
		PersistentNode parent = hier.getParent();
		if (parent == null) return;
		if (! (parent instanceof PersistentListNode)) throw new RuntimeException(
			"Parent expected to be a PersistentListNode; is a " + parent.getClass().getName());
		
		PersistentListNode lparent = (PersistentListNode)parent;
		
		try
		{
			PersistentNode priorSibling = lparent.getOrderedChildBefore(hier);
			PersistentNode nextSibling = lparent.getOrderedChildAfter(hier);
			
			if (priorSibling == null)
			{
				if (nextSibling == null) return;
				else
				{
					hier.setX(nextSibling.getX() - ChildOffsetX);
					hier.setY(nextSibling.getY() + ChildOffsetY);
				}
			}
			else
			{
				hier.setX(priorSibling.getX() + ChildOffsetX);
				hier.setY(priorSibling.getY() - ChildOffsetY);
			}
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}
	
	
	public static final Hierarchy createSubHierarchy(Hierarchy hier, String name, int position,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		Hierarchy hierarchy = createSubHierarchy(hier, name, position);
		hier.layout(layoutBound, outermostAffectedRef);
		return hierarchy;
	}
	
	
	public static final Hierarchy createSubHierarchy(Hierarchy hier, String name, int position)
	throws
		ParameterError
	{
		Hierarchy hierarchy;
		try { hierarchy = hier.constructSubHierarchy(name); }
		catch (DisallowedOperation ex) { throw new RuntimeException(ex); }
		
		try { hier.insertChildHierarchyAt(hierarchy, position); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
		hierarchy.setParentHierarchy(hier);
		hierarchy.setReasonableLocation();
		return hierarchy;
	}
		
		
	public static final Hierarchy createSubHierarchy(Hierarchy hier, String name)
	throws
		ParameterError
	{
		Hierarchy hierarchy;
		try { hierarchy = hier.constructSubHierarchy(name); }
		catch (DisallowedOperation ex) { throw new RuntimeException(ex); }
		
		hier.appendChildHierarchy(hierarchy);
		hierarchy.setParentHierarchy(hier);
		hierarchy.setReasonableLocation();
		return hierarchy;
	}
	
	
	public static final Hierarchy createSubHierarchy(Hierarchy hier, String name, int position, 
		Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		PersistentNode node;
		if (copiedNode == null) // Construct a new Node.
		{
			try { node = hier.constructSubHierarchy(name); }
			catch (DisallowedOperation ex) { throw new RuntimeException(ex); }
		}	
		else // Clone the copied Node.
		{
			if (copiedNode instanceof Domain) throw new ParameterError(
				"Cannot create sub hierarchy that copies a Domain");
		
			try { node = (PersistentNode)(copiedNode.clone(new CloneMap(hier), hier)); }
			catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
			((PersistentNodeImpl)node).setName(hier.createUniqueChildName(name));
		}
		
		if (! (node instanceof Hierarchy)) throw new RuntimeException(
			"Cloned Node is not a Hierarchy");
		
		Hierarchy hierarchy = (Hierarchy)node;
		
		try { hier.insertChildHierarchyAt(hierarchy, position); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
		
		hierarchy.setParentHierarchy(hier);
		hierarchy.setReasonableLocation();
		
		
		return hierarchy;
	}
	
	
	public static final Hierarchy createSubHierarchy(Hierarchy hier, int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return createSubHierarchy(hier, null, position, scenario, copiedNode);
	}
	
	
	public static Hierarchy createSubHierarchy(Hierarchy hier, String name, int position, 
		Scenario scenario, HierarchyTemplate template)
	throws
		ParameterError
	{
		Hierarchy hierarchy = (Hierarchy)(template.createInstance(name, hier));
		try { hier.insertChildHierarchyAt(hierarchy, position); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
		hierarchy.setParentHierarchy(hier);
		return hierarchy;
	}
	
	
	public static void deleteHierarchy(Hierarchy hier, Hierarchy subhier)
	throws
		ParameterError
	{
		hier.deleteChild(subhier);
	}
		
		
	public static final void changePosition(Hierarchy hier, int pos)
	throws
		ParameterError
	{
		Hierarchy parent = (Hierarchy)(hier.getParent());
		if (parent == null) throw new ParameterError("No parent");
		if (pos >= parent.getNoOfSubHierarchies()) throw new ParameterError(
			"Position is invalid");
		parent.removeChildHierarchy(hier);
		parent.insertChildHierarchyAt(hier, pos);
	}
	
	
	public static final int getPosition(Hierarchy hier)
	throws
		ParameterError
	{
		Hierarchy parent = (Hierarchy)(hier.getParent());
		if (parent == null) return 0;
		int pos = parent.getIndexOfChildHierarchy(hier);
		if (pos < 0) throw new RuntimeException("'" + hier.getFullName() +
			"' not found among its parent's ('" + parent.getFullName() + "') sub-Hierarchies");
		return pos;
	}
	
	
	public static final Hierarchy getPriorHierarchy(Hierarchy hier)
	{
		PersistentListNode parent = (PersistentListNode)(hier.getParent());
		if (parent == null) return null;
		try { return (Hierarchy)(parent.getOrderedChildBefore(hier)); }
		catch (ParameterError pe) { throw new RuntimeException("Should not happen", pe); }
	}
	
	
	public static final Hierarchy getNextHierarchy(Hierarchy hier)
	{
		PersistentListNode parent = (PersistentListNode)(hier.getParent());
		if (parent == null) return null;
		try { return (Hierarchy)(parent.getOrderedChildAfter(hier)); }
		catch (ParameterError pe) { throw new RuntimeException("Should not happen", pe); }
	}
		
		
	public static final int getNoOfHierarchies(Hierarchy hier)
	{
		return hier.getSubHierarchies().size();
	}
	
	
	public static final Hierarchy getParentHierarchy(Hierarchy hier)
	{
		return (Hierarchy)(hier.getParent());
	}
	
	
	public static final void setParentHierarchy(Hierarchy hier, Hierarchy parent)
	throws
		ParameterError
	{
		if (parent == null)
		{
			if (! (hier instanceof HierarchyDomain)) throw new ParameterError(
				"Cannot nullify the parent of a non-Domain Hierarchy");
		}
		else // Update subHierarchy Lists.
		{
			Hierarchy curHier = getParentHierarchy(hier);
			if (curHier != null)
				if (curHier != parent)
					curHier.removeChildHierarchy(hier);
			
			if ((curHier == null) || (curHier != parent))
				parent.appendChildHierarchy(hier);
		}
		
		hier.setParent(parent);
	}
	
	
	public static final List<Hierarchy> getSubHierarchies(Hierarchy hier, Class kind)
	{
		List<Hierarchy> hs = new Vector<Hierarchy>();
		List<Hierarchy> shs = hier.getSubHierarchies();
		for (Hierarchy h : shs) if (kind.isAssignableFrom(h.getClass())) hs.add(h);
		return hs;
	}
	
	
	public static final Hierarchy getSubHierarchy(Hierarchy hier, String name)
	throws
		ElementNotFound
	{
		List<Hierarchy> shs = hier.getSubHierarchies();
		for (Hierarchy h : shs) if (h.getName().equals(name)) return h;
		throw new ElementNotFound(name);
	}
	
	
	public static final PersistentNode createListChild(Hierarchy hier,
		PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		int pos;
		
		if (nodeBefore == null)
			if (nodeAfter == null)
				pos = hier.getNoOfSubHierarchies();
			else
				pos = hier.getOrderedChildPosition(nodeAfter);
		else
			pos = hier.getOrderedChildPosition(nodeBefore) + 1;
		
		return hier.createSubHierarchy(null, pos, scenario, copiedNode);
	}
}

