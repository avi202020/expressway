/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import expressway.server.HierarchyElement.*;
import expressway.server.MotifElement.*;
import expressway.server.NamedReference.*;
import geometry.Point;
import geometry.Face;
import generalpurpose.SetVector;
import java.util.List;
import java.util.Vector;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.awt.Image;
import java.awt.Graphics2D;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import javax.swing.ImageIcon;


public class HierarchyImpl
	extends HierarchyElementImpl
	implements Hierarchy//, TemplateInstance, Template, 
{
	private HierarchyHelper helper = new HierarchyHelper(this, new SuperHierarchyElement());
	
	/** Primary Node Reference. Maintain the ordering of each child Hierarchy. */
	private List<Hierarchy> subHierarchies = new SetVector<Hierarchy>();
	
	
	protected HierarchyHelper getHierarchyHelper() { return helper; }
	
	
  /* Constructors */
  
  
	protected HierarchyImpl(String baseName, Hierarchy parent)
	{
		super(baseName, parent);
	}
	
	
  /* From HierarchyElementImpl. Extend as needed. */


	protected String getTextToRender()
	{
		return super.getTextToRender();
	}
	
	
  /* From Hierarchy */
	
	
	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new HierarchyImpl(name, this);
	}
	
	
	public int getNoOfSubHierarchies() { return subHierarchies.size(); }
	
	public Hierarchy getChildHierarchyAt(int index)  throws ParameterError
	{
		try { return subHierarchies.get(index); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	public int getIndexOfChildHierarchy(Hierarchy child) { return subHierarchies.indexOf(child); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{
		....See this method in ModelContainerImpl.
		
		
		
		try { addSubHierarchy(index, child); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{
		try { return addSubHierarchy(child); }
		catch (ClassCastException ex) { throw new ParameterError(ex); }
	}
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{
		try { return removeSubHierarchy(child); }
		catch (ClassCastException ex) { throw new ParameterError(ex); }
	}
		
		
	public void addHierarchyAttributes()
	{
		addHierarchyAttributes(getParentHierarchy());
	}


	public void removeHierarchyAttributes()
	{
		removeHierarchyAttributes(getParentHierarchy());
	}


	public void setReasonableLocation()
	{
		HierarchyHelper.setReasonableLocation(this);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			layoutBound, outermostAffectedRef);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position);
	}
		
		
	public Hierarchy createSubHierarchy(String name)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, copiedNode);
	}
	
	
	public Hierarchy createSubHierarchy(int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, position, scenario, copiedNode);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position, 
		Scenario scenario, HierarchyTemplate template)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, template);
	}
	
	
	public void deleteHierarchy(Hierarchy hier)
	throws
		ParameterError
	{
		HierarchyHelper.deleteHierarchy(this, hier);
	}
	
	
	public void changePosition(int pos)
	throws
		ParameterError
	{
		HierarchyHelper.changePosition(this, pos);
	}
	
	
	public int getPosition()
	throws
		ParameterError
	{
		return HierarchyHelper.getPosition(this);
	}
	
	
	public Hierarchy getPriorHierarchy()
	{
		return HierarchyHelper.getPriorHierarchy(this);
	}
	
	
	public Hierarchy getNextHierarchy()
	{
		return HierarchyHelper.getNextHierarchy(this);
	}
	
	
	public int getNoOfHierarchies()
	{
		return HierarchyHelper.getNoOfHierarchies(this);
	}
	
	
	public Hierarchy getParentHierarchy()
	{
		return HierarchyHelper.getParentHierarchy(this);
	}
	
	
	public void setParentHierarchy(Hierarchy parent)
	throws
		ParameterError
	{
		HierarchyHelper.setParentHierarchy(this, parent);
	}
	
	
	public final List<Hierarchy> getSubHierarchies()
	{
		return subHierarchies;
	}
	
	
	public List<Hierarchy> getSubHierarchies(Class kind)
	{
		return HierarchyHelper.getSubHierarchies(this, kind);
	}
	
	
	public Hierarchy getSubHierarchy(String name)
	throws
		ElementNotFound
	{
		return HierarchyHelper.getSubHierarchy(this, name);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
		
		
  /* From PersistentNode */
	
	
	public Class getSerClass() { return HierarchyImplSer.class; }
	
	
	public String getTagName() { return "hierarchy"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.HierarchyIconImageName);
	}
	
	
	@Override
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		HierarchyImpl newInstance =
			(HierarchyImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.subHierarchies = cloneNodeList(subHierarchies, cloneMap, newInstance);
		
		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.print(indentString + "<" + getTagName() + " name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			);
		writeSpecializedXMLAttributes(writer);
		writer.println("/>");
		
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		
		writeSpecializedXMLElements(writer, indentation);
		
		
		// Write MenuItems, if any.
		
		List<MenuItem> mis = getMenuItems();
		if (mis != null)
		{
			for (MenuItem mi : mis) mi.writeAsXML(writer, indentation+1);
		}
		
		
		// Write each child.
		
		Set<PersistentNode> children = this.getChildren();
		for (PersistentNode child : children)
		{
			writer.println();
			child.writeAsXML(writer, indentation+1);
		}
		
			
		writer.println(indentString + "</" + getTagName() + ">");
	}

	
	@Override
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		List<Hierarchy> subHierarchiesCopy = new Vector<Hierarchy>(subHierarchies);
		for (Hierarchy h : subHierarchiesCopy) deleteChild(h);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		subHierarchies = null;
	}
	
	
	@Override
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		for (Hierarchy h : subHierarchies) if (h.getName().equals(name)) return h;
		return super.getChild(name);
	}
	

	@Override
	public SortedSet<PersistentNode> getChildren()
	{
		SortedSet children = new TreeSetNullDisallowed<PersistentNode>(super.getChildren());
		children.addAll(subHierarchies);
		return children;
	}
	
	
	@Override
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			if (child instanceof Hierarchy)
			{
				if (! removeSubHierarchy(child)) throw new RuntimeException(
					"Node '" + child.getName() + "' not found within '" + getFullName() + "'");
			}
			else
				throw pe;

			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	
	
  /* From PersistentNodeImpl */
	
	
	@Override
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		return helper.externalize(nodeSer);
	}
	
	
  /* From PersistentListNode */


	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{
		for (Hierarchy h : subHierarchies) if (h.getName().equals(name)) return h;
		throw new ElementNotFound("Ordered child with name '" + name + "'");
	}
	
	public List<PersistentNode> getOrderedChildren()
	{
		List<Hierarchy> hs = getSubHierarchies();
		List<PersistentNode> cs = new Vector<PersistentNode>();
		for (Hierarchy h : hs) cs.add(h);
		return cs;
	}
	
	public int getNoOfOrderedChildren() { return getNoOfHierarchies(); }

	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{ return getChildHierarchyAt(index); }
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return getIndexOfChildHierarchy((Hierarchy)child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		insertChildHierarchyAt((Hierarchy)child, index);
	}
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return appendChildHierarchy((Hierarchy)child);
	}
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return removeChildHierarchy((Hierarchy)child);
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{
		List<PersistentNode> nodes = new Vector<PersistentNode>();
		for (Hierarchy h : subHierarchies) if (c.isAssignableFrom(h.getClass()))
			nodes.add(h);
		return nodes;
	}
	
	public void removeOrderedNodesOfKind(Class c)
	{
		List<Hierarchy> subHierarchiesCopy = new Vector<Hierarchy>(subHierarchies);
		for (Hierarchy h : subHierarchiesCopy)
		{
			if (c.isAssignableFrom(h.getClass()))
				removeSubHierarchy(h);
		}
	}
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{
		boolean changed = false;
		for (PersistentNode node : nodes)
		{
			if (! (node instanceof Hierarchy)) throw new RuntimeException(
				"Node '" + node.getFullName() + "' is not a Hierarchy");
			addSubHierarchy((Hierarchy)node);
			changed = true;
		}
		return changed;
	}


	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createListChild(this, nodeBefore, nodeAfter, scenario,
				copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}
	
	
  /* Implementation */


	protected boolean addSubHierarchy(Hierarchy hier)
	{
		return subHierarchies.add(hier);
		
		// Whenever a Hierarchy is added as a child to another Hierarchy, the
		// inherited Attributes must be adjusted so that that child and its children
		// include all Attributes that must be inherited.
		
		hier.addHierarchyAttributes();
	}
	
	
	protected boolean removeSubHierarchy(Hierarchy hier)
	{
		// Remove inherited Attributes.
		hier.removeHierarchyAttributes();
		
		return subHierarchies.remove(hier);
	}
	
	
  /* Proxy classes needed by HierarchHelper */
	

	private class SuperHierarchyElement extends SuperCrossReferenceable implements HierarchyElement
	{
		public HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return HierarchyImpl.super.createHierarchyAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		public HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return HierarchyImpl.super.createHierarchyAttribute(layoutBound, outermostAffectedRef); }
	
		public HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return HierarchyImpl.super.createHierarchyAttribute(name, layoutBound, outermostAffectedRef); }
		
		public HierarchyAttribute createHierarchyAttribute(HierarchyAttribute definingAttr,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return HierarchyImpl.super.createHierarchyAttribute(definingAttr,
			layoutBound, outermostAffectedRef); }
		
		public HierarchyDomain getHierarchyDomain()
		{ return HierarchyImpl.super.getHierarchyDomain(); }
	}
	
	
	private class SuperCrossReferenceable extends SuperPersistentNode implements CrossReferenceable
	{
		public Set<CrossReference> getCrossReferences()
		{ return HierarchyImpl.super.getCrossReferences(); }
		
		
		public void addRef(CrossReference cr)
		throws
			ParameterError
		{ HierarchyImpl.super.addRef(cr); }
		
		
		public Set<NamedReference> getNamedReferences()
		{ return HierarchyImpl.super.getNamedReferences(); }
		
		
		public NamedReference getNamedReference(String refName)
		{ return HierarchyImpl.super.getNamedReference(refName); }
		
		
		public Set<CrossReferenceable> getToNodes(NamedReference nr)
		{ return HierarchyImpl.super.getToNodes(nr); }
		
		
		public Set<CrossReferenceable> getFromNodes(NamedReference nr)
		{ return HierarchyImpl.super.getFromNodes(nr); }
		
		
		public Set<CrossReferenceable> getToNodes(String refName)
		throws
			ParameterError
		{ return HierarchyImpl.super.getToNodes(refName); }
		
		
		public Set<CrossReferenceable> getFromNodes(String refName)
		throws
			ParameterError
		{ return HierarchyImpl.super.getFromNodes(refName); }
		
		
		public void removeRef(CrossReference cr)
		throws
			ParameterError
		{ HierarchyImpl.super.removeRef(cr); }
			
			
		public void addCrossReferenceCreationListener(CrossReferenceCreationListener listener)
		{ HierarchyImpl.super.addCrossReferenceCreationListener(listener); }
		
		public void addCrossReferenceDeletionListener(CrossReferenceDeletionListener listener)
		{ HierarchyImpl.super.addCrossReferenceDeletionListener(listener); }
		
		public List<CrossReferenceCreationListener> getCrossRefCreationListeners()
		{ return HierarchyImpl.super.getCrossRefCreationListeners(); }
		
		public List<CrossReferenceDeletionListener> getCrossRefDeletionListeners()
		{ return HierarchyImpl.super.getCrossRefDeletionListeners(); }
		
		public void removeCrossReferenceListener(CrossReferenceListener listener)
		{ HierarchyImpl.super.removeCrossReferenceListener(listener); }
		
		public void signalCrossReferenceCreated(CrossReference cr)
		throws
			Exception
		{ HierarchyImpl.super.signalCrossReferenceCreated(cr); }
		
		public void signalCrossReferenceDeleted(NamedReference nr,
			CrossReferenceable fromNode, CrossReferenceable toNode)
		throws
			Exception
		{ HierarchyImpl.super.signalCrossReferenceDeleted(nr, fromNode, toNode); }
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return HierarchyImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return HierarchyImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return HierarchyImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return HierarchyImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ HierarchyImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return HierarchyImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return HierarchyImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Set<Attribute> getAttributes()
		{ return HierarchyImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return HierarchyImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return HierarchyImpl.super.getAttributeSeqNo(attr); }
		

		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ HierarchyImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ HierarchyImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ HierarchyImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return HierarchyImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return HierarchyImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return HierarchyImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return HierarchyImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return HierarchyImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return HierarchyImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return HierarchyImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return HierarchyImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return HierarchyImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ HierarchyImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return HierarchyImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return HierarchyImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return HierarchyImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return HierarchyImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ HierarchyImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return HierarchyImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ HierarchyImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return HierarchyImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return HierarchyImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return HierarchyImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ HierarchyImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return HierarchyImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ HierarchyImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return HierarchyImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return HierarchyImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return HierarchyImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return HierarchyImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return HierarchyImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return HierarchyImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return HierarchyImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return HierarchyImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ HierarchyImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return HierarchyImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return HierarchyImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return HierarchyImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ HierarchyImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return HierarchyImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ HierarchyImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return HierarchyImpl.super.isVisible(); }
		
		
		public void layout()
		{ HierarchyImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return HierarchyImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ HierarchyImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return HierarchyImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ HierarchyImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ HierarchyImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return HierarchyImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ HierarchyImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return HierarchyImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ HierarchyImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return HierarchyImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ HierarchyImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return HierarchyImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return HierarchyImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return HierarchyImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ HierarchyImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ HierarchyImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return HierarchyImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ HierarchyImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return HierarchyImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ HierarchyImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return HierarchyImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return HierarchyImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return HierarchyImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return HierarchyImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ HierarchyImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return HierarchyImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ throw new RuntimeException("Should not be called"); }
		
		public void setViewClassName(String name)
		{ HierarchyImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return HierarchyImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return HierarchyImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return HierarchyImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return HierarchyImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return HierarchyImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return HierarchyImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return HierarchyImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ HierarchyImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return HierarchyImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return HierarchyImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return HierarchyImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return HierarchyImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return HierarchyImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return HierarchyImpl.super.getX(); }
		public double getY() { return HierarchyImpl.super.getY(); }
		public double getScale() { return HierarchyImpl.super.getScale(); }
		public double getOrientation() { return HierarchyImpl.super.getOrientation(); }
		public void setX(double x) { HierarchyImpl.super.setX(x); }
		public void setY(double y) { HierarchyImpl.super.setY(y); }
		public void setLocation(double x, double y) { HierarchyImpl.super.setLocation(x, y); }
		public void setScale(double scale) { HierarchyImpl.super.setScale(scale); }
		public void setOrientation(double angle) { HierarchyImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return HierarchyImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return HierarchyImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return HierarchyImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return HierarchyImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ HierarchyImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return HierarchyImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return HierarchyImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return HierarchyImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return HierarchyImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return HierarchyImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return HierarchyImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ HierarchyImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return HierarchyImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return HierarchyImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return HierarchyImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ HierarchyImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return HierarchyImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return HierarchyImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return HierarchyImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return HierarchyImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return HierarchyImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ HierarchyImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return HierarchyImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ HierarchyImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ HierarchyImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ HierarchyImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return HierarchyImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return HierarchyImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return HierarchyImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ HierarchyImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return HierarchyImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ HierarchyImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return HierarchyImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ HierarchyImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ HierarchyImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ HierarchyImpl.super.printChildrenRecursive(indentationLevel); }
	}
}


