/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.ser.*;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;


public class HierarchyScenarioHelper extends ScenarioHelper
{
	public HierarchyScenarioHelper(HierarchyScenario scenario,
		PersistentNode superScenario)
	{
		super(scenario, superScenario);
	}
	
	
	protected HierarchyScenario getHierarchyScenario() { return (HierarchyScenario)(getScenario()); }
	
	
 /* From PersistentNode */
	
	
	public void prepareForDeletion()
	{
		super.prepareForDeletion();
		
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		if (this.getScenarioThatWasCopied() != null)
			((HierarchyScenario)(this.getScenarioThatWasCopied())).hierarchyScenarioDeleted(getHierarchyScenario());
		if (this.getHierarchyScenarioSet() != null) this.getHierarchyScenarioSet().hierarchyScenarioDeleted(getHierarchyScenario());
		
		// Nullify all references.
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		HierarchyScenarioSer ser = (HierarchyScenarioSer)nodeSer;
		HierarchyScenario scenario = (HierarchyScenario)(getScenario());
		Set<Attribute> attrs = scenario.getAttributes();
		String[] attrIds = new String[attrs.size()];
		String[] fieldNames = new String[attrs.size()];
		Serializable[] values = new Serializable[attrs.size()];
		int i = 0;
		for (Attribute attr : attrs)
		{
			attrIds[i] = attr.getNodeId();
			fieldNames[i] = attr.getName();
			values[i] = scenario.getAttributeValue(attr);
			i++;
		}
		
		Scenario scenCopied = scenario.getScenarioThatWasCopied();
		ScenarioSet scenSet = scenario.getScenarioSet();
		
		/* For ScenarioSer */
		
		ser.setDate(scenario.getDate());
		ser.setScenarioThatWasCopiedNodeId(scenCopied == null ? null : scenCopied.getNodeId());
		ser.setScenarioSetNodeId(scenSet == null ? null : scenSet.getNodeId());
		ser.setAttributeNodeIds(attrIds);
		ser.setAttributeValues(values);

		/* For HierarchySer */

		ser.setSubHierarchyNodeIds(new String[0]);
		ser.setFieldNames(fieldNames);
		ser.setSeqNo(scenario.getPosition());

		return getSuperScenario().externalize(nodeSer);
	}
	
	
  /* From Hierarchy */

	
	public int getNoOfSubHierarchies() { return 0; }
	
	public Hierarchy getChildHierarchyAt(int index) throws ParameterError
	{ throw new RuntimeException("Should not be called"); }
	
	public int getIndexOfChildHierarchy(Hierarchy child)
	{ throw new RuntimeException("Should not be called"); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{ throw new RuntimeException("Should not be called"); }
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{ throw new RuntimeException("Should not be called"); }
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{ throw new RuntimeException("Should not be called"); }
	
	
  /* From HierarchyScenario */
	
	
	public HierarchyScenarioSet getHierarchyScenarioSet() { return (HierarchyScenarioSet)(getScenarioSet()); }
	
	
	public void setHierarchyScenarioSet(HierarchyScenarioSet s) { setScenarioSet(s); }


  /* From HierarchyScenarioChangeListener */
	

	public void hierarchyScenarioDeleted(HierarchyScenario scen)
	{
		if (scen == getScenarioThatWasCopied())
			getScenario().setScenarioThatWasCopied(null);
	}
	
	
  /* From HierarchyScenarioSetChangeListener */
	
	
	public void hierarchyScenarioSetDeleted(HierarchyScenarioSet scenSet)
	{
		if (scenSet == getHierarchyScenarioSet())
			((HierarchyScenario)(getScenario())).setHierarchyScenarioSet(null);
	}
}

