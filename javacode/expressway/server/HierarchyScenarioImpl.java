/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import generalpurpose.DateAndTimeUtils;
import generalpurpose.XMLTools;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.server.HierarchyElement.*;
import expressway.server.NamedReference.*;
import expressway.ser.*;
import geometry.*;
import java.util.Date;
import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import java.text.DateFormat;
import java.io.Serializable;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;


public class HierarchyScenarioImpl extends HierarchyElementImpl implements HierarchyScenario
	// is a HierarchyScenarioChangeListener, HierarchyScenarioSetChangeListener
{
	private HierarchyScenarioHelper helper;
	

	public HierarchyScenarioImpl(String name, HierarchyDomain domain)
	{
		super(name, domain);
		helper = new HierarchyScenarioHelper(this, new SuperScenario());
	}
	
	
	public ScenarioHelper getScenarioHelper() { return helper; }
	
	
	public HierarchyScenarioHelper getHierarchyScenarioHelper() { return helper; }
	
	
	public String getTagName() { return "hierarchy_scenario"; }
	
	
	public Class getSerClass() { return HierarchyScenarioImplSer.class; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.HierarchyScenarioIconImageName);
	}
	
	
  /* From PersistentNode */
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		helper.renameChild(child, newName);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		return helper.clone(cloneMap, cloneParent);
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		helper.writeAsXML(writer, indentation);
	}
	
	
	public void prepareForDeletion()
	{
		helper.prepareForDeletion();
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		return helper.getChild(name);
	}
	
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		return helper.getChildren();
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		helper.deleteChild(child, outermostAffectedRef);
	}

	
	public int getNoOfHeaderRows() { return helper.getNoOfHeaderRows(); }
	
	
	public double getPreferredWidthInPixels() { return helper.getPreferredWidthInPixels(); }
	public double getPreferredHeightInPixels() { return helper.getPreferredHeightInPixels(); }

	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		return helper.externalize(nodeSer);
	}
	
	
  /* From PersistentListNode */
	
	
	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{ throw new RuntimeException("Should not be called"); }
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{ return getChildHierarchyAt(index); }
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return getIndexOfChildHierarchy((Hierarchy)child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		insertChildHierarchyAt((Hierarchy)child, index);
	}
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return appendChildHierarchy((Hierarchy)child);
	}
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return removeChildHierarchy((Hierarchy)child);
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{ throw new RuntimeException("Should not be called"); }
	
	public void removeOrderedNodesOfKind(Class c)
	{ throw new RuntimeException("Should not be called"); }
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{ throw new RuntimeException("Should not be called"); }

	public List<PersistentNode> getOrderedChildren()
	{
		return new Vector<PersistentNode>();
	}
	
	public int getNoOfOrderedChildren() { return getNoOfHierarchies(); }
		
		
	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		throw new RuntimeException("A Scenario may not have a child Hierarchy");
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}
	
	
  /* From Scenario */
	
		
	public Scenario update(ScenarioSer scenarioSer)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		return helper.update(scenarioSer);
	}
	
	
	public void setDate(Date d) { helper.setDate(d); }
	
	
	public Date getDate() { return helper.getDate(); }
	
	
	public void setDerivedFrom(Scenario originalScenario)
	{
		helper.setDerivedFrom(originalScenario);
	}
	
	
	public Scenario derivedFrom() { return helper.derivedFrom(); }
	
	
	public void setAttributeValue(Attribute attribute, Serializable value)
	throws
		ParameterError
	{
		helper.setAttributeValue(attribute, value);
	}
	
	
	public Serializable getAttributeValue(Attribute attribute)
	throws
		ParameterError
	{
		return helper.getAttributeValue(attribute);
	}


	public void setAttributeValues(Map<Attribute, Serializable> attrValues)
	{
		helper.setAttributeValues(attrValues);
	}


	public Set<Attribute> getAttributesWithValues()
	{
		return helper.getAttributesWithValues();
	}
	
	
	public Set<Attribute> getAttributesWithValuesOfType(PersistentNode elt, 
		Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		return helper.getAttributesWithValuesOfType(elt, type, onlyOne);
	}
		
	
	public void setScenarioSet(ScenarioSet s) { helper.setScenarioSet(s); }
	
	
	public ScenarioSet getScenarioSet() { return helper.getScenarioSet(); }
	
	
	public void setScenarioThatWasCopied(Scenario scen) { helper.setScenarioThatWasCopied(scen); }
	
	
	public Scenario getScenarioThatWasCopied() { return helper.getScenarioThatWasCopied(); }
	

	public Scenario createIndependentCopy()
	throws
		CloneNotSupportedException
	{
		return helper.createIndependentCopy();
	}


	public SortedSet<Attribute> identifyTags(Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		return helper.identifyTags(type, onlyOne);
	}
	
	
	public ScenarioHelper getHelper() { return helper; }
	
	
  /* From HierarchyScenario */
	
	
	public HierarchyScenarioSet getHierarchyScenarioSet() { return helper.getHierarchyScenarioSet(); }
	
	
	public void setHierarchyScenarioSet(HierarchyScenarioSet s) { helper.setHierarchyScenarioSet(s); }


  /* From HierarchyScenarioChangeListener */
	

	public void hierarchyScenarioDeleted(HierarchyScenario scenario)
	{
		helper.hierarchyScenarioDeleted(scenario);
	}
	
	
  /* From HierarchyScenarioSetChangeListener */
	
	
	public void hierarchyScenarioSetDeleted(HierarchyScenarioSet scenSet)
	{
		helper.hierarchyScenarioSetDeleted(scenSet);
	}
	
	
  /* From Hierarchy */
	
	
	public int getNoOfSubHierarchies() { return helper.getNoOfSubHierarchies(); }
	
	public Hierarchy getChildHierarchyAt(int index) throws ParameterError
	{ return helper.getChildHierarchyAt(index); }
	
	public int getIndexOfChildHierarchy(Hierarchy child) { return helper.getIndexOfChildHierarchy(child); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{ helper.insertChildHierarchyAt(child, index); }
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.appendChildHierarchy(child); }
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.removeChildHierarchy(child); }
		
		
	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		return new HierarchyScenarioImpl(name, (HierarchyDomain)(getDomain()));
	}
		
		
	public void setReasonableLocation()
	{
		HierarchyHelper.setReasonableLocation(this);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			layoutBound, outermostAffectedRef);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position);
	}
		
		
	public Hierarchy createSubHierarchy(String name)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, copiedNode);
	}
	
	
	public Hierarchy createSubHierarchy(int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, position, scenario, copiedNode);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position, 
		Scenario scenario, HierarchyTemplate template)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, template);
	}
	
	
	public void deleteHierarchy(Hierarchy hier)
	throws
		ParameterError
	{
		HierarchyHelper.deleteHierarchy(this, hier);
	}
	
	
	public void changePosition(int pos)
	throws
		ParameterError
	{
		HierarchyHelper.changePosition(this, pos);
	}
	
	
	public int getPosition()
	throws
		ParameterError
	{
		return HierarchyHelper.getPosition(this);
	}
	
	
	public Hierarchy getPriorHierarchy()
	{
		return HierarchyHelper.getPriorHierarchy(this);
	}
	
	
	public Hierarchy getNextHierarchy()
	{
		return HierarchyHelper.getNextHierarchy(this);
	}
	
	
	public int getNoOfHierarchies()
	{
		return HierarchyHelper.getNoOfHierarchies(this);
	}
	
	
	public Hierarchy getParentHierarchy()
	{
		return HierarchyHelper.getParentHierarchy(this);
	}
	
	
	public void setParentHierarchy(Hierarchy parent)
	throws
		ParameterError
	{
		HierarchyHelper.setParentHierarchy(this, parent);
	}
	
	
	public final List<Hierarchy> getSubHierarchies()
	{
		return new Vector<Hierarchy>();
	}
	
	
	public List<Hierarchy> getSubHierarchies(Class kind)
	{
		return HierarchyHelper.getSubHierarchies(this, kind);
	}
	
	
	public Hierarchy getSubHierarchy(String name)
	throws
		ElementNotFound
	{
		return HierarchyHelper.getSubHierarchy(this, name);
	}
	
	
  /* From HierarchyElementImpl */
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		helper = new HierarchyScenarioHelper(this, new SuperScenario());
	}
	
	
  /* Debug */
	
	
	public void dumpAttrValues()
	{
		helper.dumpAttrValues();
	}
	
	
	void dumpAttrValues(HierarchyElement elt)
	{
		helper.dumpAttrValues(elt);
	}
	
	
  /* Proxy classes needed by HierarchyScenarioHelper */

	
	private class SuperScenario extends SuperPersistentNode
	{
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return HierarchyScenarioImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return HierarchyScenarioImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return HierarchyScenarioImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return HierarchyScenarioImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return HierarchyScenarioImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return HierarchyScenarioImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Set<Attribute> getAttributes()
		{ return HierarchyScenarioImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return HierarchyScenarioImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.getAttributeSeqNo(attr); }


		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return HierarchyScenarioImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return HierarchyScenarioImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return HierarchyScenarioImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return HierarchyScenarioImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return HierarchyScenarioImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return HierarchyScenarioImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return HierarchyScenarioImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return HierarchyScenarioImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return HierarchyScenarioImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return HierarchyScenarioImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return HierarchyScenarioImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ HierarchyScenarioImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return HierarchyScenarioImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ HierarchyScenarioImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return HierarchyScenarioImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return HierarchyScenarioImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ HierarchyScenarioImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return HierarchyScenarioImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return HierarchyScenarioImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return HierarchyScenarioImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return HierarchyScenarioImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return HierarchyScenarioImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return HierarchyScenarioImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return HierarchyScenarioImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return HierarchyScenarioImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ HierarchyScenarioImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return HierarchyScenarioImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return HierarchyScenarioImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ HierarchyScenarioImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return HierarchyScenarioImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ HierarchyScenarioImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return HierarchyScenarioImpl.super.isVisible(); }
		
		
		public void layout()
		{ HierarchyScenarioImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return HierarchyScenarioImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ HierarchyScenarioImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ HierarchyScenarioImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ HierarchyScenarioImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return HierarchyScenarioImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ HierarchyScenarioImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return HierarchyScenarioImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ HierarchyScenarioImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return HierarchyScenarioImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ HierarchyScenarioImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return HierarchyScenarioImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return HierarchyScenarioImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return HierarchyScenarioImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ HierarchyScenarioImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ HierarchyScenarioImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return HierarchyScenarioImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ HierarchyScenarioImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return HierarchyScenarioImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ HierarchyScenarioImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return HierarchyScenarioImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return HierarchyScenarioImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return HierarchyScenarioImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return HierarchyScenarioImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ HierarchyScenarioImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return HierarchyScenarioImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ return HierarchyScenarioImpl.super.getDefaultViewTypeName(); }
		
		public void setViewClassName(String name)
		{ HierarchyScenarioImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return HierarchyScenarioImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return HierarchyScenarioImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return HierarchyScenarioImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return HierarchyScenarioImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return HierarchyScenarioImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return HierarchyScenarioImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ HierarchyScenarioImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return HierarchyScenarioImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ return HierarchyScenarioImpl.super.getNoOfHeaderRows(); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return HierarchyScenarioImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return HierarchyScenarioImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return HierarchyScenarioImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ return HierarchyScenarioImpl.super.getPreferredWidthInPixels(); }
	
		public double getPreferredHeightInPixels()
		{ return HierarchyScenarioImpl.super.getPreferredHeightInPixels(); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return HierarchyScenarioImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return HierarchyScenarioImpl.super.getX(); }
		public double getY() { return HierarchyScenarioImpl.super.getY(); }
		public double getScale() { return HierarchyScenarioImpl.super.getScale(); }
		public double getOrientation() { return HierarchyScenarioImpl.super.getOrientation(); }
		public void setX(double x) { HierarchyScenarioImpl.super.setX(x); }
		public void setY(double y) { HierarchyScenarioImpl.super.setY(y); }
		public void setLocation(double x, double y) { HierarchyScenarioImpl.super.setLocation(x, y); }
		public void setScale(double scale) { HierarchyScenarioImpl.super.setScale(scale); }
		public void setOrientation(double angle) { HierarchyScenarioImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return HierarchyScenarioImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return HierarchyScenarioImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return HierarchyScenarioImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return HierarchyScenarioImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ HierarchyScenarioImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return HierarchyScenarioImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return HierarchyScenarioImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return HierarchyScenarioImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return HierarchyScenarioImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return HierarchyScenarioImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return HierarchyScenarioImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ HierarchyScenarioImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return HierarchyScenarioImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyScenarioImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ HierarchyScenarioImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ HierarchyScenarioImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return HierarchyScenarioImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return HierarchyScenarioImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return HierarchyScenarioImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return HierarchyScenarioImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return HierarchyScenarioImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ HierarchyScenarioImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return HierarchyScenarioImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ HierarchyScenarioImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ HierarchyScenarioImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ HierarchyScenarioImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return HierarchyScenarioImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return HierarchyScenarioImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return HierarchyScenarioImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ HierarchyScenarioImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return HierarchyScenarioImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ HierarchyScenarioImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return HierarchyScenarioImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ HierarchyScenarioImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ HierarchyScenarioImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ HierarchyScenarioImpl.super.printChildrenRecursive(indentationLevel); }
	}
}

