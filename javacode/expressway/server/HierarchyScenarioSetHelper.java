/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.ser.*;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.io.IOException;
import java.io.PrintWriter;


public class HierarchyScenarioSetHelper extends ScenarioSetHelper
{
	public HierarchyScenarioSetHelper(HierarchyScenarioSet scenarioSet,
		PersistentNode superScenarioSet)
	{
		super(scenarioSet, superScenarioSet);
	}
	
	
  /* From PersistentNode */


	public void prepareForDeletion()
	{
		super.prepareForDeletion();

		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		List<Scenario> scenarios = getScenarios();
		for (Scenario scenario : scenarios)
			((HierarchyScenario)scenario).hierarchyScenarioSetDeleted(
				(HierarchyScenarioSet)(getScenarioSet()));
		
		// Nullify all references.
	}
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		HierarchyScenarioSetSer ser = (HierarchyScenarioSetSer)nodeSer;
		List<PersistentNode> hs = getScenarioSet().getOrderedChildren();
		String[] ids = new String[hs.size()];
		
		int i = 0;
		for (PersistentNode n : hs) ids[i++] = n.getNodeId();
		
		Set<Attribute> hattrs =
			((HierarchyScenarioSet)(getScenarioSet())).getAttributes();
		String[] fieldNames = new String[hattrs.size()];
		int j = 0;
		for (Attribute hattr : hattrs) fieldNames[j++] = hattr.getName();
		
		ser.setSubHierarchyNodeIds(ids);
		ser.setFieldNames(fieldNames);
		ser.setSeqNo(((Hierarchy)(getScenarioSet())).getPosition());  // should be 0
		return (NodeSer)(getSuperScenarioSet().externalize(nodeSer));
	}


  /* From ScenarioSet */


	public void addScenario(Scenario scen)
	{
		super.addScenario(scen);
		((HierarchyScenario)scen).setHierarchyScenarioSet(
			(HierarchyScenarioSet)(getScenarioSet()));
	}


	public void removeScenario(Scenario scen)
	{
		super.removeScenario(scen);
		((HierarchyScenario)scen).setHierarchyScenarioSet(null);
	}


  /* From HierarchyScenarioSet */


	public List<HierarchyScenario> getHierarchyScenarios()
	{
		List<Scenario> ss = getScenarios();
		List<HierarchyScenario> hss = new Vector<HierarchyScenario>();
		for (Scenario s : ss) hss.add((HierarchyScenario)s);
		return hss;
	}
	
	
  /* From Hierarchy */
	

	public int getNoOfSubHierarchies() { return getScenarios().size(); }
	
	public Hierarchy getChildHierarchyAt(int index) throws ParameterError
	{ return (HierarchyScenario)(getScenarios().get(index)); }
	
	public int getIndexOfChildHierarchy(Hierarchy child)
	{ return getScenarios().indexOf((Scenario)child); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{
		insertOrderedChildAt(child, index);
	}
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{
		return appendOrderedChild(child);
	}
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{
		return removeOrderedChild(child);
	}
	

  /* From HierarchyScenarioChangeListener */

	
	public void hierarchyScenarioDeleted(HierarchyScenario scen)
	{
		removeScenario(scen);
	}
}

