/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.ser.*;
import java.util.Set;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.awt.Dimension;
import java.awt.Graphics2D;


public class ListNodeHelper
{
	/* This class can be used via either its static methods or its instance methods. */
	
	
	/* The following twelve PersistentListNode methods are used to implement these
		helper methods:
	
	List<PersistentNode> getOrderedChildren();
	
	PersistentNode getOrderedChild(String name) throws ElementNotFound;
	
	PersistentNode getOrderedChildAt(int index) throws ParameterError;
	
	int getIndexOfOrderedChild(PersistentNode child);
	
	void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError;
	
	boolean appendOrderedChild(PersistentNode child) throws ParameterError;
	
	boolean removeOrderedChild(PersistentNode child) throws ParameterError;
	
	List<PersistentNode> getOrderedNodesOfKind(Class c);
	
	void removeOrderedNodesOfKind(Class c);
	
	boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError;

	List<PersistentNode> getOrderedChildren();
	
	int getNoOfOrderedChildren();
	
	*/
	
	
	private PersistentListNode instanceListNode;
	
	public PersistentListNode getListNode() { return instanceListNode; }
	
	
  /* From PersistentListNode */
	
	
	public static final PersistentNode getFirstOrderedChild(PersistentListNode listNode)
	{
		if (listNode.getNoOfOrderedChildren() == 0) return null;
		try { return listNode.getOrderedChildAt(0); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public static final PersistentNode getLastOrderedChild(PersistentListNode listNode)
	{
		if (listNode.getNoOfOrderedChildren() == 0) return null;
		try { return listNode.getOrderedChildAt(listNode.getNoOfOrderedChildren()-1); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public static final int getOrderedChildPosition(PersistentListNode listNode,
		PersistentNode child)
	throws
		ParameterError
	{
		if (child == null) throw new RuntimeException("child is null");
		int index = listNode.getIndexOfOrderedChild(child);
		if (index < 0) throw new ParameterError(
			"Child Node '" + child.getName() + "' not found in list of '" +
			listNode.getFullName() + "'");
		return index;
	}


	public static final PersistentNode getOrderedChildBefore(PersistentListNode listNode,
		PersistentNode child)
	throws
		ParameterError
	{
		if (child == null) throw new RuntimeException("child is null");
		int index = listNode.getOrderedChildPosition(child);
		if (index < 0) throw new ParameterError("Child Node not found in list");
		if (index == 0) return null;
		return listNode.getOrderedChildAt(index-1);
	}
	
	
	public static final PersistentNode getOrderedChildAfter(PersistentListNode listNode,
		PersistentNode child)
	throws
		ParameterError
	{
		if (child == null) throw new RuntimeException("child is null");
		int index = listNode.getOrderedChildPosition(child);
		if (index < 0) throw new ParameterError("Child Node not found in list");
		if (index == (listNode.getNoOfOrderedChildren()-1)) return null;
		return listNode.getOrderedChildAt(index+1);
	}
	
	
	public static PersistentNode createListChild(PersistentListNode listNode,
		PersistentListNode nodeBefore, PersistentListNode nodeAfter,
		Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		CloneMap cloneMap = new CloneMap(listNode);
		PersistentNode copy;
		
		if (copiedNode == null)
			throw new ParameterError("Cannot determine what kind of Node to construct");
		else
			try { copy = copiedNode.clone(cloneMap, listNode); }
			catch (CloneNotSupportedException ex) { throw new ParameterError(ex); }
		
		addOrderedChildAfter(listNode, copy, nodeBefore);
		return copy;
	}


	public static void addOrderedChildBefore(PersistentListNode listNode, 
		PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		try
		{
			listNode.getChild(child.getName());
			throw new ParameterError("Child with name '" + child.getName() +
				"' already exists for '" + listNode.getFullName() + "'");
		}
		catch (ElementNotFound ex) {} // ok
		
		int pos;
		if (nextChild == null) // add as last child.
			pos = listNode.getNoOfOrderedChildren();
		else
			pos = listNode.getOrderedChildPosition(nextChild);
		
		listNode.insertOrderedChildAt(child, pos);
		((PersistentNodeImpl)child).setParent(listNode);
	}
	
	
	public static final void addOrderedChildAfter(PersistentListNode listNode, 
		PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		try
		{
			listNode.getChild(child.getName());
			throw new ParameterError("Child with name '" + child.getName() +
				"' already exists for '" + listNode.getFullName() + "'");
		}
		catch (ElementNotFound ex) {} // ok
		
		int pos;
		if (priorChild == null) // add as first child.
			pos = 0;
		else
			pos = listNode.getOrderedChildPosition(priorChild) + 1;
		
		listNode.insertOrderedChildAt(child, pos);
		((PersistentNodeImpl)child).setParent(listNode);
	}
	
	
	public static final boolean removeOrderedChild(PersistentListNode listNode,
		PersistentNode child)
	throws
		ParameterError
	{
		listNode.getOrderedChildPosition(child);  // Verify it is in the List.
		if (listNode.removeOrderedChild(child))
		{
			child.setParent(null);
			return true;
		}
		else
			return false;
	}
	
	
  /* Instance methods. This may be used instead of the static methods above. */

	
	public ListNodeHelper(PersistentListNode node)
	{
		this.instanceListNode = node;
	}
	
	public final PersistentNode getFirstOrderedChild()
	{ return getFirstOrderedChild(instanceListNode); }
	
	public final PersistentNode getLastOrderedChild()
	{ return getLastOrderedChild(instanceListNode); }
	
	public final int getOrderedChildPosition(PersistentNode child)
	throws ParameterError
	{ return getOrderedChildPosition(instanceListNode, child); }
	
	public final PersistentNode getOrderedChildBefore(PersistentNode child)
	throws ParameterError
	{ return getOrderedChildBefore(instanceListNode, child); }
		
	public final PersistentNode getOrderedChildAfter(PersistentNode child)
	throws ParameterError
	{ return getOrderedChildAfter(instanceListNode, child); }
		
	public PersistentNode createListChild(
		PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws ParameterError
	{ return createListChild(instanceListNode, nodeBefore, nodeAfter, scenario, copiedNode); }
		
	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws ParameterError
	{ addOrderedChildBefore(instanceListNode, child, nextChild); }

	public final void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws ParameterError
	{ addOrderedChildAfter(instanceListNode, child, priorChild); }
		
	public final boolean removeOrderedChild(PersistentNode child)
	throws ParameterError
	{ return removeOrderedChild(instanceListNode, child); }
}

