/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.ser.*;


abstract class MenuItemBase extends MotifElementBase implements MenuItem
{
	private MenuOwner definedBy;
	private String description;
	
	
	public Class getSerClass() { return MenuItemSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		// Set Ser fields.
		
		MenuItemSer ser = (MenuItemSer)nodeSer;
		
		ser.definedByNodeId = getNodeIdOrNull(definedBy);
		ser.description = description;
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		MenuItemBase newInstance = (MenuItemBase)(super.clone(cloneMap, cloneParent));
		newInstance.definedBy = cloneMap.getClonedNode(definedBy);
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		this.definedBy = null;
		this.description = null;
	}
	
	
	public MenuItemBase(String text, MenuOwner mo, MenuOwner definedBy)
	{
		super(text, (Hierarchy)mo);
		this.definedBy = definedBy;
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public MenuOwner getMenuOwner() { return (MenuOwner)(super.getParent()); }
	
	public MenuOwner getDefinedBy() { return definedBy; }
	
	public void setDefinedBy(MenuOwner db) { this.definedBy = db; }

	public String getText() { return getName(); }
	
	public void setText(String text)
	{
		try { this.setName(text); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	public int getMenuItemPosition()
	{
		MenuOwner parent = (MenuOwner)(getParent());
		if (parent == null) throw new RuntimeException(this.getName() + " has no parent");
		return parent.getIndexOf(this);
	}
}

