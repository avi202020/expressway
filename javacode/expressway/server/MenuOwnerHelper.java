/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.ser.*;
import java.util.List;


class MenuOwnerHelper
{
	static void copySerValues(MenuOwner menuOwner, MenuOwnerSer menuOwnerSer)
	{
		List<MenuItem> menuItems = menuOwner.getMenuItems();
		String[] nodeIdAr = new String[menuItems.size()];
		int i = 0;
		for (MenuItem mi : menuItems) nodeIdAr[i++] = mi.getNodeId();
		
		menuOwnerSer.setMenuItemTextAr(new String[menuItems.size()]);
		menuOwnerSer.setMenuItemDescAr(new String[menuItems.size()]);
		menuOwnerSer.setJavaMethodNames(new String[menuItems.size()]);
		menuOwnerSer.setMenuTreeNodeIds(new String[menuItems.size()]);
		menuOwnerSer.setMenuItemNodeIds(new String[menuItems.size()]);
		
		i = 0;
		for (MenuItem mi : menuItems)
		{
			menuOwnerSer.getMenuItemNodeIds()[i] = mi.getNodeId();
			menuOwnerSer.getMenuItemTextAr()[i] = mi.getText();
			menuOwnerSer.getMenuItemDescAr()[i] = mi.getHTMLDescription();
			
			if (mi instanceof Action)
			{
				menuOwnerSer.getJavaMethodNames()[i] = ((Action)mi).getJavaMethodName();
				menuOwnerSer.getMenuTreeNodeIds()[i] = null;
			}
			else if (mi instanceof MenuTree)
			{
				menuOwnerSer.getJavaMethodNames()[i] = null;
				menuOwnerSer.getMenuTreeNodeIds()[i] = ((MenuTree)mi).getNodeId();
			}
			else
				throw new RuntimeException("Unexpected MenuItem type");
			
			i++;
		}
	}
}

