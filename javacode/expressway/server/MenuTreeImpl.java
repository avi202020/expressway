/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.MotifElement.*;
import expressway.server.ModelElement.ModelDomain;
import expressway.server.HierarchyElement.*;
import expressway.ser.*;
import generalpurpose.TreeSetNullDisallowed;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;


public class MenuTreeImpl extends MenuOwnerBase implements MenuTree
{
	//private MenuOwner parentMenuOwner;
	private MenuOwner definedBy;
	private String text;
	private String description;
	
	
	public Class getSerClass() { return MenuTreeSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		
		// Set Ser fields.
		
		MenuTreeSer ser = (MenuTreeSer)nodeSer;
		ser.text = text;
		
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		MenuTreeImpl newInstance = (MenuTreeImpl)(super.clone(cloneMap, cloneParent));
		newInstance.definedBy = cloneMap.getClonedNode(definedBy);
		
		return newInstance;
	}
	

	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		
		writer.println(indentString + "<menuitem text=\"" + this.getText() + " \""
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ ">");
		
		List<MenuItem> menuItems = getMenuItems();
		for (MenuItem mi : menuItems)
		{
			mi.writeAsXML(writer, indentation+1);
		}
		
		writer.println(indentString + "</menuitem>");
	}
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{}	


	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{} // ok
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		text = null;
	}
	
	
	public MenuTreeImpl(String baseName, MenuOwner parent, MenuOwner definedBy)
	{
		super(baseName, (Hierarchy)parent);
		this.definedBy = definedBy;
	}
	
	
	public String getTagName() { return "menu"; }
	
	
	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		MenuTreeImpl mt = new MenuTreeImpl(this.getName(), this, this);
		mt.setReasonableLocation();
		return mt;
	}
		
		
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		Attribute attr = constructAttribute(name);
		attr.setDefaultValue(defaultValue);
		return attr;
	}
	

	public Attribute constructAttribute(String name)
	{
		return new MenuTreeAttribute(name);
	}
	
	
	public class MenuTreeAttribute extends PersistentNodeImpl implements Attribute
	{
		private AttributeValidator validator = null;
		private Serializable defaultValue = null;
		//private Serializable value = null;
		
		MenuTreeAttribute(String name)
		{
			super(MenuTreeImpl.this);
			try { setName(MenuTreeImpl.this.createUniqueChildName(name)); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
		}
		
		public String getTagName() { return "attribute"; }
		
		public Class getSerClass() { return MenuTreeAttributeSer.class; }
		
		public void prepareForDeletion()
		{
			super.prepareForDeletion();
		}
		
		public void setIconImageToDefault() throws IOException
		{
			setIconImage(NodeIconImageNames.AttributeIconImageName);
		}
			
		public int getNoOfHeaderRows() { return 0; }
		
		public SortedSet<PersistentNode> getPredefinedNodes()
		{
			return new TreeSetNullDisallowed<PersistentNode>();
		}
		
		public double getPreferredWidthInPixels() { return 50; }
		public double getPreferredHeightInPixels() { return 50; }

		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{
			Attribute attr = constructAttribute(name);
			attr.setDefaultValue(defaultValue);
			return attr;
		}
		
	
		public Attribute constructAttribute(String name)
		{
			return new MenuTreeAttribute(name);
		}
		
		/* From Attribute */
		public void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
		throws
			IOException
		{
			AttributeHelper.writeAsXML(this, writer, indentation, appliesTo);
		}
		
		public void setValidator(AttributeValidator validator) { this.validator = validator; }
		
		public AttributeValidator getValidator() { return validator; }
	
		public void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (validator != null) validator.validate(newValue);
		}
		
		public Serializable getDefaultValue() { return defaultValue; }
		
		public void setDefaultValue(Serializable defaultValue)
		throws
			ParameterError
		{
			this.defaultValue = defaultValue;
		}
		
		public void setValue(Scenario scenario, Serializable value)
		throws
			ParameterError
		{
			scenario.setAttributeValue(this, value);
		}
		
		
		public Serializable getValue(Scenario scenario)
		throws
			ParameterError
		{
			return scenario.getAttributeValue(this);
		}
		
		//public void setValue(Serializable value)
		//throws
		//	ParameterError
		//{
		//	this.value = value;
		//}
		
		
		//public Serializable getValue()
		//throws
		//	ParameterError
		//{
		//	return this.value;
		//}
	}
	
	
	private static final SortedSet<PersistentNode> EmptyNodeSet = new TreeSetNullDisallowed<PersistentNode>();
	
	
	public SortedSet<PersistentNode> getPredefinedNodes() { return EmptyNodeSet; }
	
	
	/*public Set<PersistentNode> getImmediateOwnedNodes()
	{
		return new HashSet<PersistentNode>(getMenuItems());
	}*/
	
	
	public ModelDomain getModelDomain() { return (ModelDomain)(getDomain()); }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.MenuTreeIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 50.0;
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	//public MenuOwner getMenuOwner() { return parentMenuOwner; }

	public MenuOwner getMenuOwner() { return (MenuOwner)(getParent()); }
	
	public MenuOwner getDefinedBy() { return definedBy; }
	
	public void setDefinedBy(MenuOwner mo) { this.definedBy = mo; }

	public String getText() { return text; }
	
	public void setText(String text) { this.text = text; }
	
	public int getMenuItemPosition()
	{
		MenuOwner parent = (MenuOwner)(getParent());
		if (parent == null) throw new RuntimeException(this.getName() + " has no parent");
		return parent.getIndexOf(this);
	}
	
	public String getDescription() { return description; }
		
	public void setDescription(String desc) { this.description = desc; }
}


