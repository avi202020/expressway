/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.DecisionElement.VariableAttributeBinding;
import expressway.ser.*;
import expressway.server.Event.*;
import generalpurpose.StringUtils;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;
import java.util.List;
import java.util.Vector;
import generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class ModelAttributeImpl
	extends
		TemplatizableNode
	implements
		ModelAttribute, MenuOwner, ModelTemplate, TemplateInstance, Cloneable
{
	/** Primary Node Reference. */
	AttributeStateBinding attrStateBinding = null;
		
	/** Ownership. */
	public Serializable defaultValue = null;

	private Set<TemplateInstance> templateInstances = new TreeSetNullDisallowed<TemplateInstance>();
	
	VariableAttributeBinding varAttrBinding = null;

	AttributeValidator validator = null;
	
	private boolean lockedForSimulation = false;

	public transient String varAttrBindingNodeId = null;
	public transient String attrStateBindingNodeId = null;
	
  
	public ModelAttributeImpl(String name, ModelElement parent,
		Serializable defaultValue)
	{
		super(parent);
		try { this.setName(parent.createUniqueChildName(name)); }
		catch (ParameterError pe ) { throw new RuntimeException("Should not happen"); }
		init(defaultValue);
	}


	public ModelAttributeImpl(ModelElement parent, String baseName)
	{
		super(parent);
		try { this.setName(parent.createUniqueChildName(baseName)); }
		catch (ParameterError pe ) { throw new RuntimeException("Should not happen"); }
		init(null);
	}


	public String getVarAttrBindingNodeId() { return varAttrBindingNodeId; }
	public String getAttrStateBindingNodeId() { return attrStateBindingNodeId; }
	

	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}


	public SortedSet<PersistentNode> getChildren() // must extend
	{
		SortedSet<PersistentNode> children = super.getChildren();
		if (attrStateBinding != null) children.add(attrStateBinding);
		return children;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			if (child instanceof AttributeStateBinding)
			{
				attrStateBinding = null;
			}
			else
				throw pe;
		
			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		try { return super.getChild(name); }
		catch (ElementNotFound ex)
		{
			if (attrStateBinding.getName().equals(name)) return attrStateBinding;
			throw ex;
		}
	}
	

	public Class getSerClass() { return ModelAttributeSer.class; }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		ModelAttributeImpl newInstance = (ModelAttributeImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.attrStateBinding = (AttributeStateBinding)(attrStateBinding.clone(cloneMap, newInstance));
			
		newInstance.defaultValue = copyObject(defaultValue);
	
		newInstance.templateInstances = cloneNodeSet(templateInstances, cloneMap, newInstance);
		
		newInstance.varAttrBinding = cloneMap.getClonedNode(varAttrBinding);
	
		AttributeValidator validator = null;
			
		return newInstance;
	}
	

	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		writeAsXML(writer, indentation, null);
	}
	
	
  /* From PersistentNode */


	public String getTagName() { return "attribute"; }
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
		super.writeSpecializedXMLElements(writer, indentation);
		String indentString = getIndentString(indentation);
		if (attrStateBinding != null) try
		{
			State s = attrStateBinding.getForeignState();
			writer.println(indentString + "<state_binding state=\"" +
				s.getFullName() + "\"/>");
		}
		catch (ValueNotSet w) {}
	}
	
	
	/**
	 * Return true if the specified Object is of a type that is allowed in a
	 * ModelAttribute expression.
	 */
	 
	protected boolean isParsableJavaType(Object obj)
	{
		return (obj instanceof String) || (obj instanceof Number) || (obj instanceof Character);
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		if (attrStateBinding != null) deleteStateBinding(attrStateBinding, null);
		this.setHTMLDescription(null);
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		this.getModelDomain().attributeDeleted(this);
		if (varAttrBinding != null) varAttrBinding.attributeDeleted(this);
		
		// Nullify all references.

		varAttrBinding = null;
		attrStateBinding = null;
		defaultValue = null;
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = super.setNameAndNotify(newName);
		
		if (varAttrBinding != null)
			varAttrBinding.attributeNameChanged(this, oldName);
		
		this.getModelDomain().attributeNameChanged(this, oldName);
		
		return oldName;
	}
	

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.AttributeIconImageName);
	}
	
	
	private void init(Serializable defaultValue)
	{
		this.defaultValue = defaultValue;
		setResizable(false);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	

	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 0.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public void layout()
	{
		super.layout();
		if (attrStateBinding != null) attrStateBinding.setLocation(0, 0);
	}

	
	public Set<PersistentNode> getNodesWithSpecialLayout()
	{
		Set<PersistentNode> nodesToIgnore = super.getNodesWithSpecialLayout();
		if (nodesToIgnore == null) nodesToIgnore = new TreeSetNullDisallowed<PersistentNode>();
		nodesToIgnore.add(this.getStateBinding());
		return nodesToIgnore;
	}


	public ModelElement getModelElement() { return (ModelElement)(getParent()); }


	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		varAttrBindingNodeId = getNodeIdOrNull(varAttrBinding);
		attrStateBindingNodeId = getNodeIdOrNull(attrStateBinding);
		
		
		// Set Ser fields.
		
		((ModelAttributeSer)nodeSer).varAttrBindingNodeId = this.getVarAttrBindingNodeId();
		
		((ModelAttributeSer)nodeSer).defaultValue = this.getDefaultValue();
		if (((ModelAttributeSer)nodeSer).defaultValue != null)
			if (!( ((ModelAttributeSer)nodeSer).defaultValue instanceof Serializable)) throw new ParameterError(
				"Default value of Attribute " + this.getFullName() +
				" does not implement Serializable.Value is " + ((ModelAttributeSer)nodeSer).defaultValue +
					" (" + ((ModelAttributeSer)nodeSer).defaultValue.getClass().getName() + ")");
		
		((ModelAttributeSer)nodeSer).attrStateBindingNodeId = this.getAttrStateBindingNodeId();
		((ModelAttributeSer)nodeSer).attributeNo = getModelElement().getAttributeSeqNo(this);
		
		//MenuOwnerHelper.copySerValues(this, (ModelAttributeSer)nodeSer);
		
		return super.externalize(nodeSer);
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new ModelAttributeImpl(name, this, defaultValue);
	}
	

	public Attribute constructAttribute(String name)
	{
		return new ModelAttributeImpl(this, name);
	}
	
	
  /* From PersistentNodeImpl */
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		return new TreeSetNullDisallowed<PersistentNode>();
	}
	

  /* From ModelElement */
	
	
	public void lockForSimulation(boolean lockValue)
	throws
		CannotObtainLock
	{
		ModelElementHelper.lockForSimulation(this, lockValue);
	}
	
	
	public boolean isLockedForSimulation() { return lockedForSimulation; }
	
	
	public void setLockedForSimulation(boolean locked) { lockedForSimulation = locked; }


	public ModelElement findModelElement(String qualifiedName)
	throws
		ParameterError
	{
		return ModelElementHelper.findModelElement(this, qualifiedName);
	}
	
	
	public ModelElement findModelElement(String[] pathParts)
	throws
		ParameterError
	{
		return ModelElementHelper.findModelElement(this, pathParts);
	}
	
	
	public void shiftAllChildren(double dx, double dy)
	{
		ModelElementHelper.shiftAllChildren(this, dx, dy);
	}

	
	public ModelAttribute createModelAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, name, defaultValue, layoutBound,
			outermostAffectedRef);
	}
	
	
	public ModelAttribute createModelAttribute(PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, layoutBound, outermostAffectedRef);
	}
	
	
	public ModelAttribute createModelAttribute(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return ModelElementHelper.createModelAttribute(this, name, layoutBound, outermostAffectedRef);
	}


	public final ModelDomain getModelDomain()
	{
		return (ModelDomain)(getDomain());
	}


	public ModelAttribute getModelAttribute(String name)
	{
		return ModelElementHelper.getModelAttribute(this, name);
	}
	

	public SortedSet<ModelAttribute> getModelAttributes()
	{
		return ModelElementHelper.getModelAttributes(this);
	}


	public ModelElement findFirstNestedElement(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return ModelElementHelper.findFirstNestedElement(this, name);
	}
	
	
	public List<String> getInstantiableNodeKinds()
	{
		return super.getInstantiableNodeKinds();
	}
	
	
	public List<String> getStaticCreateMethodNames()
	{
		return staticCreateMethodNames;
	}
	
	
	public List<String> getInstantiableNodeDescriptions()
	{
		return super.getInstantiableNodeDescriptions();
	}
	
		
	public void dump() { dump(0); }
	
	
  /* From Attribute */


	public Serializable getDefaultValue() { return defaultValue; }

	
	public void setDefaultValue(Serializable defaultValue) { this.defaultValue = defaultValue; }
	
	
	public void setValue(Scenario scenario, Serializable value)
	throws
		ParameterError
	{
		if (! (scenario instanceof ModelScenario)) throw new ParameterError(
			"Scenario '" + scenario.getFullName() + "' is not a ModelScenario");
		
		((ModelScenario)scenario).setAttributeValue(this, value);
	}
	
	
	public Serializable getValue(Scenario scenario)
	throws
		ParameterError
	{
		if (! (scenario instanceof ModelScenario)) throw new ParameterError(
			"Scenario '" + scenario.getFullName() + "' is not a ModelScenario");
		
		return ((ModelScenario)scenario).getAttributeValue(this);
	}
	
	
	public void setValidator(AttributeValidator validator) { this.validator = validator; }
	
	
	public AttributeValidator getValidator() { return validator; }
	
	
	public void validate(Serializable newValue)
	throws
		ParameterError
	{
		if (validator != null) validator.validate(newValue);
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
	throws
		IOException
	{
		if (this.isPredefined()) return;

		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		if (this.getName() == null) throw new IOException(new Exception(
			"Attribute name is null; parent is " + this.getParent().getFullName()));
		
		writer.println(indentString + "<attribute name=\"" + this.getName() + "\" "
			+ (appliesTo == null? "" : "for=\"" + appliesTo.getName() + "\" ")
			+ (defaultValue == null? "" : "default=\"" + defaultValue + "\" ")
			+ (defaultValue == null? "" : 
				(isParsableJavaType(defaultValue) ? "" : "default_type=\"" 
					+ defaultValue.getClass().getName() + "\" "))
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" ");
			
		writeSpecializedXMLAttributes(writer);
	
		writer.println(">");
		
		
		writeSpecializedXMLElements(writer, indentation);


		// Write State Bindings, if any.
		
		if (attrStateBinding != null)
		{
			attrStateBinding.writeAsXML(writer, indentation+1);
		}
		
		
		// Write each Attribute of this Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</attribute>");
	}


  /* From ModelAttribute */


	public AttributeStateBinding bindToState(State state, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Check that the State and this Attribute is not owned by a Template or a
		// TemplateInstance that references a Template.
		
		if (this.isOwnedByMotifDef()) throw new ParameterError(
			"Cannot bind an Attribute that is owned by a Motif");
			
		if (this.isOwnedByActualTemplateInstance()) throw new ParameterError(
			"Cannot bind an Attribute that is owned by an instance of a Motif Template");
		
		if (state.isOwnedByMotifDef()) throw new ParameterError(
			"Cannot bind an Attribute to a State that is owned by a Motif");
		
		if (state.isOwnedByActualTemplateInstance()) throw new ParameterError(
			"Cannot bind an Attribute to a State that is owned by an instance of a Motif Template");
		
		
		// Check if this Attribute is already bound to a State.
		
		if (attrStateBinding != null) throw new ParameterError(
			"Attribute is already bound to a State");
		
		
		// Check if the binding would create a cycle.
		
		checkForAttrStateBindingCycles(this.getModelDomain(), this.getModelDomain());
		
		
		// Create a new binding.
		
		AttributeStateBinding binding = new AttributeStateBindingImpl(this, state);
		
		this.attrStateBinding = binding;

		state.addAttributeBinding(binding);

		//binding.setSizeToStandard();
		
		//binding.setLocation(0, 0);
		binding.setMovable(false);
		binding.setPredefined(false);
		binding.layout(layoutBound, outermostAffectedRef);
		

		// Add it to the ModelDomain's list of AttributeStateBindings. This list
		// is redundant but it helps to quickly list bindings later.

		Set<AttributeStateBinding> bindings =
			getModelDomain().getAttributeStateBindings();

		bindings.add(binding);
		
		return binding;
	}
	
	
	void checkForAttrStateBindingCycles(ModelDomain originalDomain, ModelDomain cur)
	throws
		ParameterError
	{
		Set<AttributeStateBinding> bindings = cur.getAttributeStateBindings();
		for (AttributeStateBinding b : bindings)
		{
			State s;
			try { s = b.getForeignState(); }
			catch (ValueNotSet w) { continue; }
			
			ModelDomain d = s.getModelDomain();
			if (d == originalDomain) throw new ParameterError(
				"A cycle in AttributeStateBindings will exist");
			
			checkForAttrStateBindingCycles(originalDomain, d);
		}
	}


	public void deleteStateBinding(AttributeStateBinding binding,
		PersistentNode[] outermostAffectedRef)
	{
		try { deleteChild(binding, outermostAffectedRef); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}
	
	
	public VariableAttributeBinding getParameterBinding()
	{
		return varAttrBinding;
	}


	public void setParameterBinding(VariableAttributeBinding binding)
	{
		varAttrBinding = binding;
	}


	public AttributeStateBinding getStateBinding()
	{
		return attrStateBinding;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init(null);
	}
	
	
  /* ***************************************************************************
   * Listener interfaces.
   */
   
   
	public void stateDeleted(State state)
	{
		// Remove any of this Attribute's AttributeStateBindings for the State.
		if (this.attrStateBinding != null)
		{
			State s;
			try { s = this.attrStateBinding.getForeignState(); }
			catch (ValueNotSet w) { return; }
			
			if (s == state) this.attrStateBinding = null;
		}
	}
	
	
	public void stateNameChanged(State state, String oldName)
	{
	}
	
	
	public void attributeDeleted(Attribute attribute)
	{
	}
	
	
	public void attributeNameChanged(Attribute attr, String oldName)
	{
	}


	public void variableAttributeBindingDeleted(VariableAttributeBinding binding)
	{
		this.varAttrBinding = null;
	}


	public void variableAttributeBindingNameChanged(VariableAttributeBinding vaBinding, String oldName)
	{
	}
	
	
	public String toString()
	{
		return super.toString() + " (def val=" +
			(defaultValue == null? "null" : defaultValue.toString()) +
				")";
	}


	static void dump(Set<AttributeStateBinding> bindings, int indentation,
		String title)
	{
		GlobalConsole.print(StringUtils.tabs[indentation]);
		GlobalConsole.println(title);
		for (AttributeStateBinding binding : bindings) binding.dump(indentation+1);
	}
}

