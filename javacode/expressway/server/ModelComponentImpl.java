/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import geometry.Point;
import geometry.PointImpl;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public abstract class ModelComponentImpl extends ModelElementImpl implements ModelComponent
{
	public Class getSerClass() { return ModelComponentSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		if (this.getParent() == null) ((ModelComponentSer)nodeSer).parentNodeId = null;
		else ((ModelComponentSer)nodeSer).parentNodeId = this.getParent().getNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ModelComponentImpl newInstance = (ModelComponentImpl)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		getModelDomain().modelComponentDeleted(this);
		
		// Nullify all references.
	}
	
	
	public ModelComponentImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		//init();
	}


	public ModelComponentImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		//init();
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		ModelContainer parent = this.getModelContainer();
		if (parent == null) return false;
		return parent.isOwnedByActualTemplateInstance();
	}
	
	
	//private void init()
	//{
	//	if (getParent() != null) try { ((ModelContainerImpl)parent).addComponent(this); }
	//	catch (ParameterError pe) { throw new RuntimeException(pe); }
	//}
	
	
	public ModelContainer getModelContainer() { return (ModelContainer)(getParent()); }
		// may be null.

	
	public Point translateToContainer(Point pointInComponent)
	throws
		ParameterError
	{
		if (this.getParent() == null) throw new ParameterError("No parent");
		
		double pTranslatedx = this.getX() + pointInComponent.getX();
		double pTranslatedy = this.getY() + pointInComponent.getY();
		
		return new PointImpl(pTranslatedx, pTranslatedy);
	}
	
	
	public Point translateFromContainer(Point pointInContainer)
	throws
		ParameterError
	{
		if (this.getParent() == null) throw new ParameterError("No parent");
		
		double pTranslatedx = pointInContainer.getX() - this.getDisplay().getX();
		double pTranslatedy = pointInContainer.getY() - this.getDisplay().getY();
		
		return new PointImpl(pTranslatedx, pTranslatedy);
	}
}
