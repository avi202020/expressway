/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.NamedReference.*;
import expressway.server.Event.*;
import expressway.ser.*;
import geometry.*;
import generalpurpose.SetVector;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;
import java.util.List;
import java.util.Vector;
import java.awt.Image;
import java.awt.Graphics2D;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import javax.swing.ImageIcon;
import org.apache.commons.math.distribution.ContinuousDistribution;


public abstract class ModelContainerImpl extends ModelComponentImpl implements ModelContainer
{
	public Class getSerClass() { return ModelContainerSer.class; }  // must extend
	
	private HierarchyHelper helper = new HierarchyHelper(this, new SuperHierarchyElement());
		
	
	/** Primary Node Reference. Maintain the ordering of each child Hierarchy. */
	private List<Hierarchy> subHierarchies = new SetVector<Hierarchy>();


	public String[] getFunctionNodeIds() { return createNodeIdArray(getFunctions()); }
	public String[] getActivityNodeIds() { return createNodeIdArray(getActivities()); }
	public String[] getConduitNodeIds() { return createNodeIdArray(getConduits()); }
	public String[] getConstraintNodeIds() { return createNodeIdArray(getConstraints()); }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError  // must extend
	{
		ModelContainerSer ser = (ModelContainerSer)nodeSer;
		ser.functionNodeIds = getFunctionNodeIds();
		ser.activityNodeIds = getActivityNodeIds();
		ser.conduitNodeIds = getConduitNodeIds();
		ser.constraintNodeIds = getConstraintNodeIds();
		ser.setSubHierarchyNodeIds(createNodeIdArray(getSubHierarchies()));
		ser.setSeqNo(getPosition());
		
		Set<Attribute> attrs = getAttributes();

		int noOfFields = 0;
		for (Attribute attr : attrs)
			if (attr instanceof HierarchyAttribute)
				noOfFields++;

		String[] fieldNames = new String[noOfFields];
		int i = 0;
		for (Attribute attr : attrs)
			if (attr instanceof HierarchyAttribute)
				fieldNames[i++] = attr.getName();
		ser.setFieldNames(fieldNames);
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)  // must extend
	throws CloneNotSupportedException
	{
		return (ModelContainerImpl)(super.clone(cloneMap, cloneParent));
	}
	

	public void prepareForDeletion()  // must extend
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public ModelContainerImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
	}


	public ModelContainerImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}

	
	/**
	 * Override this method as necessary when subclassing.
	 */
	 
	public Set<ModelComponent> getSubcomponents() // must extend
	{
		Set<ModelComponent> children = new TreeSetNullDisallowed<ModelComponent>();
		
		List<Hierarchy> shs = getSubHierarchies();
		for (Hierarchy h : shs)
			if (h instanceof ModelComponent) children.add((ModelComponent)h);
		
		return children;
	}
	
	
	/**
	 * Override this method as necessary when subclassing.
	 */
	 
	public void deleteSubcomponent(ModelElement child,
		PersistentNode[] outermostAffectedRef) // must extend
	throws
		ParameterError
	{
		deleteChild(child, outermostAffectedRef);
	}

	
	public final ModelElement getSubcomponent(String name)
	throws
		ElementNotFound
	{
		Set<ModelComponent> children = this.getSubcomponents();
		
		for (ModelComponent child : children)
			if (child.getName().equals(name))
				return child;
		
		throw new ElementNotFound(name + " not found within " + this.getName());
	}
	

	public Set<Function> getFunctions()
	{
		List<Hierarchy> hs = getSubHierarchies(Function.class);
		Set<Function> fs = new TreeSetNullDisallowed<Function>();
		for (Hierarchy h : hs) fs.add((Function)h);
		return fs;
	}


	public Set<Activity> getActivities()
	{
		List<Hierarchy> hs = getSubHierarchies(Activity.class);
		Set<Activity> fs = new TreeSetNullDisallowed<Activity>();
		for (Hierarchy h : hs) fs.add((Activity)h);
		return fs;
	}


	public Set<Conduit> getConduits()
	{
		List<Hierarchy> hs = getSubHierarchies(Conduit.class);
		Set<Conduit> fs = new TreeSetNullDisallowed<Conduit>();
		for (Hierarchy h : hs) fs.add((Conduit)h);
		return fs;
	}


	public Set<Constraint> getConstraints()
	{
		List<Hierarchy> hs = getSubHierarchies(Constraint.class);
		Set<Constraint> fs = new TreeSetNullDisallowed<Constraint>();
		for (Hierarchy h : hs) fs.add((Constraint)h);
		return fs;
	}
	
	
	public synchronized List<State> getStatesRecursive()
	{
		List<State> states = new Vector<State>();
		this.getStatesRecursive(states);
		return states;
	}
	
	
	public Set<PersistentNode> getNodesWithSpecialLayout()
	{
		Set<PersistentNode> nodesToIgnore = super.getNodesWithSpecialLayout();
		if (nodesToIgnore == null) nodesToIgnore = new TreeSetNullDisallowed<PersistentNode>();
		nodesToIgnore.addAll(this.getConduits());
		return nodesToIgnore;
	}
	

	protected HierarchyHelper getHierarchyHelper() { return helper; }


	protected void getStatesRecursive(List<State> states)
	{
		if (this instanceof PortedContainer)
			states.addAll(((PortedContainer)this).getStates());
		
		Set<Function> functions = this.getFunctions();
		for (Function f : functions) ((ModelContainerImpl)f).getStatesRecursive(states);
		
		Set<Activity> activities = this.getActivities();
		for (Activity a : activities) ((ModelContainerImpl)a).getStatesRecursive(states);
	}


	public Rectangle getRectangle(boolean asIcon)
	{
		if (asIcon)
			return new Rectangle(
				new PointImpl(this.getX(), this.getY()),
				this.getIconWidth(), this.getIconHeight());
		else
			return new Rectangle(
				new PointImpl(this.getX(), this.getY()),
				this.getWidth(), this.getHeight());
	}
	
	
	/*
	 * The dimensions of Conduits owned by this Container depend on the border
	 * size and dimensions of this Container, because a Conduit is rooted at the
	 * bottom left of its Container, inside the Container's border.
	 */
	 
	 
	public void setWidth(double width)
	{
		super.setWidth(width);
		updateConduitDimensions();
	}
	
	
	public void setHeight(double height)
	{
		super.setHeight(height);
		updateConduitDimensions();
	}
	
	
	/** Override to prevent Conduits from being shifted. */
	public boolean allowShift(PersistentNode childNode)
	{
		if (childNode instanceof Conduit) return false;
		return true;
	}
	
	
	/**
	 * Update the dimensions of each Conduit owned by this Container, to reflect
	 * any changes in the dimensions or border of this Container.
	 */
	 
	void updateConduitDimensions()
	{
		Set<Conduit> conduits = getConduits();
		Set<Conduit> cs = new TreeSetNullDisallowed<Conduit>(conduits);
		for (Conduit c : cs) ((ConduitImpl)c).setDimensionsBasedOnParent();
	}
	
	
	public boolean contains(ModelElement element)
	{
		try
		{
			PersistentNode child = getChild(element.getName());
			if (child == element) return true;
			return false;
		}
		catch (ElementNotFound ex) { return false; }
	}
	

	public boolean chechForConduitOverlaps(Conduit thisConduit, Point segBegin, 
		Point segEnd, double minDist, double minRadians)
	throws
		ParameterError
	{
		if (segBegin.equals(segEnd)) 
			throw new ParameterError("Segment endpoints are equal");
		
		LineSegment segment = new LineSegment(segBegin, segEnd);
		double d = 0.0;
		double radians = 0.0;
		ConduitImpl conduit = null;
		
		LineSegment otherSegment = new LineSegment(null, null);
		Set<Conduit> conduits = getConduits();
		for (Conduit c : conduits) try
		{
			conduit = (ConduitImpl)c;
			
			if (conduit == thisConduit) continue;
			
			InflectionPoint[] ips = conduit.getInflectionPoints();
			
			Point[] portLocsInConduit = conduit.translatePortLocationsToConduit();
			
			Point p1 = portLocsInConduit[0];
			otherSegment.p1 = p1;
			for (InflectionPoint ip : ips)
			{
				Point p2 = ip;
				otherSegment.p2 = p2;
				
				if (p1.equals(p2)) throw new RuntimeException("p1=p2; p1=" + p1 + ", " + p2);
				
				if (Math.abs(radians = segment.radiansBetween(otherSegment)) >= Math.abs(minRadians))
					if ((d = segment.distanceTo(otherSegment)) < minDist) return true;
				
				p1 = p2;
				otherSegment.p1 = p1;
			}
			
			otherSegment.p2 = portLocsInConduit[1];

			if (p1.equals(otherSegment.p2)) throw new RuntimeException("p1=p2; p1=" + p1 + ", " + otherSegment.p2);	
			if (Math.abs(radians = segment.radiansBetween(otherSegment)) >= Math.abs(minRadians))
				if ((d = segment.distanceTo(otherSegment)) < minDist) return true;
		}
		catch (Exception ex) { throw new RuntimeException(ex); }
		finally {
			/*GlobalConsole.println("Distance from one segment to " +
				"another is " + d + ", angle is " + radians); 
			GlobalConsole.println(segment);
			GlobalConsole.println(otherSegment);
			GlobalConsole.println(thisConduit);
			GlobalConsole.println(conduit);*/
		}
		
		return false;
	}
	
	
	public void rerouteAllConduits()
	throws
		ParameterError
	{
		Set<Conduit> conduits = getConduits();
		for (Conduit conduit : conduits) conduit.reroute();
	}
	
	
	public void flowComponentsHorizontally(ModelComponent... components)
	{
		// Compute the distance available inbetween the Components.
		
		double totalComponentWidths = 0.0;
		
		for (ModelComponent component : components)
		{
			totalComponentWidths += component.getWidth();
		}
		
		if (totalComponentWidths >= this.getWidth())
		{
			// Expand width of this ModelContainer.
			
			this.setWidth(totalComponentWidths * 1.1);
		}
		
		double spacing = (this.getWidth() - totalComponentWidths) / (components.length + 1);
		
		double edgeOfPrior = 0.0;
		
		for (ModelComponent component : components)
		{
			double newX = edgeOfPrior + spacing;
			component.getDisplay().setX(newX);
			edgeOfPrior = newX + component.getWidth();
		}
	}
		
		
	public void flowComponentsVertically(ModelComponent... components)
	{
		// Compute the distance available inbetween the Components.
		
		double totalComponentHeights = 0.0;
		
		for (ModelComponent component : components)
		{
			totalComponentHeights += component.getHeight();
		}
		
		if (totalComponentHeights >= this.getHeight())
		{
			// Expand width of this ModelContainer.
			
			this.setHeight(totalComponentHeights * 1.1);
		}
		
		double spacing = (this.getHeight() - totalComponentHeights) / (components.length + 1);
		
		double edgeOfPrior = 0.0;
		
		for (ModelComponent component : components)
		{
			double newY = edgeOfPrior + spacing;
			component.getDisplay().setY(newY);
			edgeOfPrior = newY + component.getHeight();
		}
	}
	
	
	public ModelElement createSubcomponent(String kind, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (ModelElement)(this.createChild(kind, layoutBound, outermostAffectedRef));
	}
		
		
	void addComponent(ModelComponent component)
	throws
		ParameterError
	{
		if (component instanceof ModelDomain)
			throw new ParameterError("Cannot add a ModelDomain to a ModelComponent");

		addOrderedChildAfter(component, null);
	}
	
	
	void removeComponent(ModelComponent component)
	throws
		ParameterError
	{
		removeOrderedChild(component);
	}


	void addConstraint(Constraint constraint)
	throws
		ParameterError
	{
		addComponent(constraint);
	}
	
	
	public Activity createActivity(String name, Class nativeImplClass, 
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		Activity activity = new ActivityImpl(name, this, this.getModelDomain());
		try { initActivity(activity, nativeImplClass, layoutBound, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			activity.deleteFromAllNodesTable();
			throw pe;
		}
		
		return activity;
	}


	public Activity createActivity(String name, Template template,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		Activity activityInstance = 
			(Activity)(template.createInstance(name, this));
		initActivity(activityInstance, null, layoutBound, outermostAffectedRef);
		return activityInstance;
	}


	public Activity createActivity(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createActivity(name, (Class)null, layoutBound, outermostAffectedRef);
	}
	
	
	public void initActivity(Activity activity, Class nativeImplClass,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (nativeImplClass != null) activity.setNativeImplementationClass(nativeImplClass);
		addComponent(activity);
		activity.setSizeToStandard();
		activity.layout(layoutBound, outermostAffectedRef);
	}
	

	/**
	 * Implements the Delete Pattern.
	 */

	public void deleteActivity(Activity activity, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(activity, outermostAffectedRef);
	}

		
	public Function createFunction(String name, Class nativeImplClass, 
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		Function function = new FunctionImpl(name, this, this.getModelDomain());
		try { initFunction(function, nativeImplClass, layoutBound, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			function.deleteFromAllNodesTable();
			throw pe;
		}
		
		return function;
	}


	public Function createFunction(String name, Template template,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		Function functionInstance = 
			(Function)(template.createInstance(name, this));
		initFunction(functionInstance, null, layoutBound, outermostAffectedRef);
		return functionInstance;
	}


	public Function createFunction(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createFunction(name, (Class)null, layoutBound, outermostAffectedRef);
	}
	

	public void initFunction(Function function, Class nativeImplClass,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (nativeImplClass != null) function.setNativeImplementationClass(nativeImplClass);
		addComponent(function);
		function.setSizeToStandard();
		function.layout(layoutBound, outermostAffectedRef);
	}
	

	public void deleteFunction(Function function, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(function, outermostAffectedRef);
	}

		
	public Terminal createTerminal(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Terminal terminal = new TerminalImpl(name, this, this.getModelDomain());
		terminal.setNativeImplementationClass(TerminalNativeImpl.class);

		addComponent(terminal);

		terminal.setSizeToStandard();
		terminal.layout(layoutBound, outermostAffectedRef);

		return terminal;
	}


	public Generator createGenerator(String name, boolean variable,
		double timeDistShape, double timeDistScale, double valueDistShape,
		double valueDistScale, boolean ignoreStartup, boolean pulse,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Generator generator = null;
		//if (variable)
		//{
			if (pulse)
				generator = new VariablePulseGeneratorImpl(name, this, this.getModelDomain(),
					timeDistShape, timeDistScale, valueDistShape, valueDistScale,
					ignoreStartup);
			else
				generator = new VariableGeneratorImpl(name, this, this.getModelDomain(),
					timeDistShape, timeDistScale, valueDistShape, valueDistScale,
					ignoreStartup);
					
			// Make sure tht XMLParser does not create Attributes that are created
			// by the constructor.
			
		addComponent(generator);
		
		generator.setSizeToStandard();
		generator.layout(layoutBound, outermostAffectedRef);
		
		return generator;
	}


	public Generator createGenerator(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createGenerator(name, 
			true, // boolean variable,
			AbstractGenerator.DefaultTimeDistShape,
			AbstractGenerator.DefaultTimeDistScale,
			AbstractGenerator.DefaultValueDistShape,
			AbstractGenerator.DefaultValueDistScale,
			AbstractGenerator.DefaultIgnoreStartup,
			false, // boolean pulse
			layoutBound,
			outermostAffectedRef
			);
	}


	public Generator createPulseGenerator(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createGenerator(name, 
			true, // boolean variable,
			AbstractGenerator.DefaultTimeDistShape,
			AbstractGenerator.DefaultTimeDistScale,
			AbstractGenerator.DefaultValueDistShape,
			AbstractGenerator.DefaultValueDistScale,
			AbstractGenerator.DefaultIgnoreStartup,
			true, // boolean pulse
			layoutBound,
			outermostAffectedRef
			);
	}
	
	
	private int connectorNumber = 0;
	
	
	public static String getRepeatingConduitName(Generator gen)
	{
		return GeneratorImpl.RepeatingConduitName + "_for_" + gen.getName() + "_";
	}


	public Conduit makeGeneratorRepeating(Generator gen, boolean repeating)
	throws
		ParameterError,
		ModelContainsError
	{
		if (repeating)
		{
			try { if (isGeneratorRepeating(gen)) return null; }  // nothing to do.
			catch (ElementNotFound ex) { throw new ParameterError(ex); }
			
			Conduit repeatConduit = null;
			repeatConduit = this.createConduit(getRepeatingConduitName(gen), 
				//GeneratorImpl.RepeatingConduitName + connectorNumber++, 
					gen.getCountPort(), gen.getInputPort(), this, null);
			
			repeatConduit.setVisible(false);
			repeatConduit.setPredefined(true);
			repeatConduit.reroute();
			
			return repeatConduit;
		}
		else // Remove the repeat Conduit
		{
			try { if (! isGeneratorRepeating(gen)) return null; }
			catch (ElementNotFound ex) { throw new ParameterError(ex); }
			
			ModelElement elt;
			try { elt = getSubcomponent(getRepeatingConduitName(gen)); }
			catch (ElementNotFound ex) { throw new RuntimeException(
				"Should not happen: possible race condition."); }
				
			Conduit conduit = ((Conduit)elt);
			
			this.deleteConduit(conduit, new PersistentNode[1]);
			
			elt.deleteFromAllNodesTable();
			
			return null;
		}
	}

	
	public boolean isGeneratorRepeating(Generator gen)
	throws
		ElementNotFound, // if the Generator is not found in this Model Container.
		ModelContainsError
	{
		// there is a Conduit named ModelElement.Generator.RepeatingConduitName<...>
		// that connects the Generator count port and input port
		
		ModelElement child;
		ModelContainer genContainer = gen.getModelContainer();
		
		try { child = genContainer.getSubcomponent(getRepeatingConduitName(gen)); }
		catch (ElementNotFound ex) { return false; }
		
		if (! (child instanceof Conduit)) throw new ModelContainsError(
			"A Model Element exists with the repeating pattern name but is not a Conduit.");
		
		Conduit conduit = (Conduit)child;
		Port p1 = null;
		Port p2 = null;
		try
		{
			p1 = conduit.getPort1();
			p2 = conduit.getPort2();
		}
		catch (ValueNotSet w) { throw new RuntimeException("Should not occur"); }
		
		if ((p1 == gen.getInputPort()) && (p2 == gen.getCountPort()))
			return true;
		
		if ((p2 == gen.getInputPort()) && (p1 == gen.getCountPort()))
			return true;
		
		throw new ModelContainsError(
			"A Conduit that fits the repeat naming pattern exists but is not properly connected.");
	}
	

	public Tally createTally(String name, Number initValue, String[] inputPortNames,
		boolean doubleCount, PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Tally tally = new TallyImpl(name, this, this.getModelDomain(), initValue,
			inputPortNames, doubleCount);
		
		tally.setNativeImplementationClass(TallyNativeImpl.class);

		addComponent(tally);
		
		tally.setSizeToStandard();
		tally.layout(layoutBound, outermostAffectedRef);

		return tally;
	}
	
	
	public Tally createTally(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createTally(name, AbstractTally.DefaultInitValue, new String[] {}, 
			AbstractTally.DefaultDoubleCount, layoutBound, outermostAffectedRef);
	}
	

	public Max createMax(String name, Number initValue, String[] inputPortNames,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Max max = new MaxImpl(name, this, this.getModelDomain(), initValue,
			inputPortNames);
		
		max.setNativeImplementationClass(TallyNativeImpl.class);

		addComponent(max);
		
		max.setSizeToStandard();
		max.layout(layoutBound, outermostAffectedRef);

		return max;
	}
	
	
	public Max createMax(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createMax(name, AbstractMax.DefaultInitValue, new String[] {},
			layoutBound, outermostAffectedRef);
	}
	

	public Switch createSwitch(String name, boolean conducting,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Switch theSwitch = new SwitchImpl(name, this, this.getModelDomain(),
			conducting);
		
		theSwitch.setNativeImplementationClass(SwitchNativeImpl.class);

		addComponent(theSwitch);
		
		theSwitch.setSizeToStandard();
		theSwitch.layout(layoutBound, outermostAffectedRef);

		return theSwitch;
	}
	
	
	public Switch createSwitch(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createSwitch(name, AbstractSwitch.DefaultStartConducting,
			layoutBound, outermostAffectedRef);
	}
	

	public Modulator createModulator(String name, Double factor,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Modulator modulator = new ModulatorImpl(name, this, this.getModelDomain(),
			factor);
		
		modulator.setNativeImplementationClass(ModulatorNativeImpl.class);

		addComponent(modulator);
		
		modulator.setSizeToStandard();
		modulator.layout(layoutBound, outermostAffectedRef);

		return modulator;
	}
	
	
	public Modulator createModulator(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createModulator(name, ModulatorImpl.DefaultDefaultFactor,
			layoutBound, outermostAffectedRef);
	}
	

	public DoubleExpression createDoubleExpression(String name, String expr,
		String[] inputPortNames, PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		DoubleExpression expression = new DoubleExpressionImpl(name, this, this.getModelDomain(),
			expr, inputPortNames);
		
		expression.setNativeImplementationClass(DoubleExpressionNativeImpl.class);

		addComponent(expression);
		
		expression.setSizeToStandard();
		expression.layout(layoutBound, outermostAffectedRef);

		return expression;
	}
	
	
	public DoubleExpression createDoubleExpression(String name,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createDoubleExpression(name, DoubleExpressionImpl.DefaultExpr,
			new String[0], layoutBound, outermostAffectedRef);
	}
	

	public NotExpression createNotExpression(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		NotExpression expression = new NotExpressionImpl(name, this, this.getModelDomain());
		
		expression.setNativeImplementationClass(NotExpressionNativeImpl.class);

		addComponent(expression);
		
		expression.setSizeToStandard();
		expression.layout(layoutBound, outermostAffectedRef);

		return expression;
	}
	
	
	public Summation createSummation(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Summation summation = new SummationImpl(name, this, this.getModelDomain());
		
		summation.setNativeImplementationClass(SummationNativeImpl.class);

		addComponent(summation);
		
		summation.setSizeToStandard();
		summation.layout(layoutBound, outermostAffectedRef);

		return summation;
	}
	
	
	public Discriminator createDiscriminator(String name, Double threshold,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Discriminator discriminator = new DiscriminatorImpl(name, this,
			this.getModelDomain(), threshold);
		
		discriminator.setNativeImplementationClass(DiscriminatorNativeImpl.class);

		addComponent(discriminator);
		
		discriminator.setSizeToStandard();
		discriminator.layout(layoutBound, outermostAffectedRef);

		return discriminator;
	}
	
	
	public Discriminator createDiscriminator(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createDiscriminator(name, DiscriminatorImpl.DefaultThreshold,
			layoutBound, outermostAffectedRef);
	}
	

	public Delay createDelay(String name, long delay, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Delay delayActivity = new DelayImpl(name, this, this.getModelDomain(), delay);
		
		delayActivity.setNativeImplementationClass(DelayNativeImpl.class);

		addComponent(delayActivity);
		
		delayActivity.setSizeToStandard();
		delayActivity.layout(layoutBound, outermostAffectedRef);

		return delayActivity;
	}


	public Delay createDelay(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createDelay(name, AbstractDelay.DefaultDelay, layoutBound, outermostAffectedRef);
	}
	
	
	public Sustain createSustain(String name, double timeDistShape, double timeDistScale,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Sustain sustain = new SustainImpl(name, this, this.getModelDomain(),
			timeDistShape, timeDistScale);
			
		sustain.setNativeImplementationClass(SustainNativeImpl.class);
		
		addComponent(sustain);
		
		sustain.setSizeToStandard();
		sustain.layout(layoutBound, outermostAffectedRef);

		return sustain;
	}
	
	
	public Sustain createSustain(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createSustain(name, AbstractSustain.DefaultTimeDistShape,
			AbstractSustain.DefaultTimeDistScale, layoutBound, outermostAffectedRef);
	}
	

	public OneWay createOneWay(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		OneWay oneWay = new OneWayImpl(name, this, this.getModelDomain());
			
		oneWay.setNativeImplementationClass(OneWayNativeImpl.class);
		
		addComponent(oneWay);
		
		oneWay.setSizeToStandard();
		oneWay.layout(layoutBound, outermostAffectedRef);

		return oneWay;
	}
	
	
	/**
	 * See also ConduitImpl.translatePortLocationsToConduit(...).
	 */
	 
	boolean portsMayBeConnected(Port p1, Port p2)
	{
		if (p1 == p2) return false;
		
		PortedContainer p1Container = p1.getContainer();
		PortedContainer p2Container = p2.getContainer();
		
		if (p1Container.getParent() == null) throw new RuntimeException(
			"Unexpected: '" + p1Container.getFullName() + "' is not within a Container.");
		
		if (p2Container.getParent() == null) throw new RuntimeException(
			"Unexpected: '" + p2Container.getFullName() + "' is not within a Container.");
		
		
		if (p1Container == p2Container) return true;
		else
			// containers not the same
			
			if (p1Container.contains(p2Container)) return true;
			else
				// p1Container does NOT immediately contain p2Container
				
				if (p2Container.contains(p1Container)) return true;
				else
					// p2Container does NOT immediately contain p1Container
					
					if (p1Container.getParent() == p2Container.getParent()) return true;
					else
						return false;
	}
	
	
	public Conduit createConduit(String name, Port portA, Port portB,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		Conduit conduit = new ConduitImpl(name, this, this.getModelDomain());
		
		try { addConduit(conduit, portA, portB, layoutBound, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			conduit.deleteFromAllNodesTable();
			throw pe;
		}
		
		return conduit;
	}
	
	
	public Conduit createConduit(String name, Template template,
		Port portA, Port portB,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		Conduit conduitInstance = 
			(Conduit)(template.createInstance(name, this));
		addConduit(conduitInstance, portA, portB, layoutBound, outermostAffectedRef);
		return conduitInstance;
	}


	protected void addConduit(Conduit conduit, Port portA, Port portB,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(conduit.getName());

		if ((portA != null) && (portB != null))
		{
			// Check if the Ports are already connected by a Conduit in this Container.
			// If so, throw an Exception.
			
			if (this.connects(portA, portB)) throw new ParameterError(
				"Ports " + portA.getFullName() + " and " + portB.getFullName() +
					" are already connected within Container " + this.getName());
					
			
			// Verify that the Ports are permitted to be connected.
			
			if (! portsMayBeConnected(portA, portB)) throw new ParameterError(
				"Ports '" + portA.getFullName() + "' and '" + portB.getFullName() +
				"' may not be connected.");
		}
		
		conduit.setPort1(portA);
		conduit.setPort2(portB);
		
		addComponent(conduit);
		
		conduit.reroute();
		
		if (outermostAffectedRef != null) outermostAffectedRef[0] = conduit;
	}
	

	public void deleteConduit(Conduit conduit, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(conduit, outermostAffectedRef);
	}

		
	public Satisfies createSatisfiesRelation(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Satisfies satisfies = new SatisfiesImpl(name, this, this.getModelDomain());

		addConstraint(satisfies);
		
		satisfies.setSizeToStandard();
		satisfies.layout(layoutBound, outermostAffectedRef);

		return satisfies;
	}


	public Derives createDerivesRelation(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Derives derives = new DerivesImpl(name, this, this.getModelDomain());

		addConstraint(derives);
		
		derives.setSizeToStandard();
		derives.layout(layoutBound, outermostAffectedRef);
		
		return derives;
	}


	public void deleteConstraint(Constraint constraint, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(constraint, outermostAffectedRef);
	}
	
	
  /* From HierarchyElement */
	
	
	public HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}


	public HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(HierarchyAttribute attr,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyElementHelper.createHierarchyAttribute(this, attr, layoutBound,
			outermostAffectedRef);
	}
	
	
	public HierarchyDomain getHierarchyDomain()
	{
		return (HierarchyDomain)(getDomain());
	}
	
	
  /* From Hierarchy */
	
	
	public int getNoOfSubHierarchies() { return subHierarchies.size(); }
	
	public Hierarchy getChildHierarchyAt(int index) throws ParameterError
	{
		try { return subHierarchies.get(index); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	public int getIndexOfChildHierarchy(Hierarchy child) { return subHierarchies.indexOf(child); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{
		....
		Seems like method roles are not clear:
		addSubhierarchy does not actually add the child: it must maintains the
		hierarchy. Also, in HierarchyScenarioSetHelper, the insertChildHierarchyAt
		method delegates to insertOrderedChildAt, but elsewhere that method
		delegates to insertChildHierarchyAt.
		
		Same problems in ModelContainerImpl.
		
		
		try { addSubHierarchy(child); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{
		try { return addSubHierarchy(child); }
		catch (ClassCastException ex) { throw new ParameterError(ex); }
	}
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{
		try { return removeSubHierarchy(child); }
		catch (ClassCastException ex) { throw new ParameterError(ex); }
	}
		
		
	public void setReasonableLocation()
	{
		HierarchyHelper.setReasonableLocation(this);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			layoutBound, outermostAffectedRef);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position);
	}
		
		
	public Hierarchy createSubHierarchy(String name)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, copiedNode);
	}
	
	
	public Hierarchy createSubHierarchy(int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, position, scenario, copiedNode);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position, 
		Scenario scenario, HierarchyTemplate template)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, template);
	}
	
	
	public void deleteHierarchy(Hierarchy hier)
	throws
		ParameterError
	{
		HierarchyHelper.deleteHierarchy(this, hier);
	}
	
	
	public void changePosition(int pos)
	throws
		ParameterError
	{
		HierarchyHelper.changePosition(this, pos);
	}
	
	
	public int getPosition()
	throws
		ParameterError
	{
		return HierarchyHelper.getPosition(this);
	}
	
	
	public Hierarchy getPriorHierarchy()
	{
		return HierarchyHelper.getPriorHierarchy(this);
	}
	
	
	public Hierarchy getNextHierarchy()
	{
		return HierarchyHelper.getNextHierarchy(this);
	}
	
	
	public int getNoOfHierarchies()
	{
		return HierarchyHelper.getNoOfHierarchies(this);
	}
	
	
	public Hierarchy getParentHierarchy()
	{
		return HierarchyHelper.getParentHierarchy(this);
	}
	
	
	public void setParentHierarchy(Hierarchy parent)
	throws
		ParameterError
	{
		HierarchyHelper.setParentHierarchy(this, parent);
	}
	
	
	public final List<Hierarchy> getSubHierarchies()
	{
		return subHierarchies;
	}
	
	
	public List<Hierarchy> getSubHierarchies(Class kind)
	{
		return HierarchyHelper.getSubHierarchies(this, kind);
	}
	
	
	public Hierarchy getSubHierarchy(String name)
	throws
		ElementNotFound
	{
		return HierarchyHelper.getSubHierarchy(this, name);
	}
	
	
  /* From PersistentListNode */
	
	
	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{
		for (Hierarchy h : subHierarchies) if (h.getName().equals(name)) return h;
		throw new ElementNotFound("Ordered child with name '" + name + "'");
	}
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{ return getChildHierarchyAt(index); }
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return getIndexOfChildHierarchy((Hierarchy)child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		insertChildHierarchyAt((Hierarchy)child, index);
	}
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return appendChildHierarchy((Hierarchy)child);
	}
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{
		if (! (child instanceof Hierarchy)) throw new RuntimeException(
			"Node '" + child.getFullName() + "' is not a Hierarchy");
		return removeChildHierarchy((Hierarchy)child);
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{
		List<PersistentNode> nodes = new Vector<PersistentNode>();
		for (Hierarchy h : subHierarchies) if (c.isAssignableFrom(h.getClass()))
			nodes.add(h);
		return nodes;
	}
	
	public void removeOrderedNodesOfKind(Class c)
	{
		List<Hierarchy> subHierarchiesCopy = new Vector<Hierarchy>(subHierarchies);
		for (Hierarchy h : subHierarchiesCopy)
		{
			if (c.isAssignableFrom(h.getClass()))
				removeSubHierarchy(h);
		}
	}
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{
		//return subHierarchies.addAll(nodes);
		boolean changed = false;
		for (PersistentNode node : nodes)
		{
			if (! (node instanceof Hierarchy)) throw new RuntimeException(
				"Attempt to add a non-Hierarchy to ModelContainer's sub-hierarchy");
			addSubHierarchy((Hierarchy)node);
			changed = true;
		}
		return changed;
	}

	public List<PersistentNode> getOrderedChildren()
	{
		List<PersistentNode> nodes = new Vector<PersistentNode>();
		for (Hierarchy h : subHierarchies) nodes.add(h);
		return nodes;
	}
	
	public int getNoOfOrderedChildren() { return subHierarchies.size(); }


	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return ListNodeHelper.createListChild(this, nodeBefore, nodeAfter, scenario,
			copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}
	
	
  /* From PersistentNode */
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new ModelContainerAttributeImpl(name, this, defaultValue);
	}
	

	public Attribute constructAttribute(String name)
	{
		return new ModelContainerAttributeImpl(this, name);
	}
	
	
  /* Implementation */

	
	/** Initialization data for the instantiable Element Lists. */
	private static final Object[][] instantiableElements =
	{
		{ NodeKindNames.Activity, "createActivity", "@Activities" },
		{ NodeKindNames.Function, "createFunction", "@Functions" },
		{ NodeKindNames.Terminal, "createTerminal", "@Terminals" },
		{ NodeKindNames.PulseGenerator, "createPulseGenerator", "@Pulse Generators" },
		{ NodeKindNames.Generator, "createGenerator", "@Flow Generators" },
		{ NodeKindNames.Tally, "createTally", "@Tallies" },
		{ NodeKindNames.Max, "createMax", "@Maxes" },
		{ NodeKindNames.Switch, "createSwitch", "@Switches" },
		{ NodeKindNames.Modulator, "createModulator", "@Modulators" },
		{ NodeKindNames.DoubleExpression, "createDoubleExpression", "@Expressions" },
		{ NodeKindNames.NotExpression, "createNotExpression", "@Not Expressions" },
		{ NodeKindNames.Summation, "createSummation", "@Summations" },
		{ NodeKindNames.Discriminator, "createDiscriminator", "@Discriminators" },
		{ NodeKindNames.Delay, "createDelay", "@Delays" },
		{ NodeKindNames.Sustain, "createSustain", "@Sustains" },
		{ NodeKindNames.OneWay, "createOneWay", "@OneWays" }
	};
	
	
	protected static List<String> staticInstantiableNodeKinds;
	protected static List<String> staticCreateMethodNames;
	protected static List<String> staticNodeDescriptions;
	
	
	static
	{
		staticInstantiableNodeKinds = 
			new Vector<String>(ModelElementImpl.staticInstantiableNodeKinds);
		staticCreateMethodNames = 
			new Vector<String>(ModelElementImpl.staticCreateMethodNames);
		staticNodeDescriptions = 
			new Vector<String>(ModelElementImpl.staticNodeDescriptions);
		
		for (Object[] oa : instantiableElements)
			staticInstantiableNodeKinds.add((String)(oa[0]));
		
		for (Object[] oa : instantiableElements)
			staticCreateMethodNames.add((String)(oa[1]));
		
		for (Object[] oa : instantiableElements)
			staticNodeDescriptions.add((String)(oa[2]));
	}
	
	
	List<String> getStaticNodeKinds()
	{
		return staticInstantiableNodeKinds;
	}
	
	
	List<String> getStaticNodeDescriptions()
	{
		return staticNodeDescriptions;
	}
		
		
	public List<String> getInstantiableNodeKinds()
	{
		List<String> names = staticInstantiableNodeKinds;
		getDynamicInstantiableNodeKind(names);
		return names;
	}
	
	
	public List<String> getStaticCreateMethodNames()
	{
		return staticCreateMethodNames;
		// Note: only statics are included here.
	}
	
	
	public List<String> getInstantiableNodeDescriptions()
	{
		// Put all of the statics first. This includes statics from derived classes.
		
		List<String> allDescriptions = new Vector<String>(getStaticNodeDescriptions());
		
		
		// Include elements defined by each MotifDef.
		// Note that this must be refreshed when a Motif changes.
		
		Set<MotifDef> motifDefs = this.getModelDomain().getMotifDefs();
		for (MotifDef motifDef : motifDefs)
		{
			// Add the description of each ModelElement in the MotifDef.
			
			Set<PersistentNode> motifElements = motifDef.getChildren();
			for (PersistentNode me : motifElements)
			{
				if (me instanceof Template)
				{
					allDescriptions.add(((Template)me).getHTMLDescription());
				}
			}
		}
		
		return allDescriptions;
	}
	
	
	protected void initializeTemplateInstance(TemplateInstance instance)
	throws
		ParameterError
	{
		if (instance instanceof Activity)
			initActivity((Activity)instance, null, null, null);
		else if (instance instanceof Function)
			initFunction((Function)instance, null, null, null);
		else if (instance instanceof Conduit)
			throw new ParameterError("Cannot instantiate a Conduit in this manner");
		else
			super.initializeTemplateInstance(instance);
	}
	
	
	protected boolean addSubHierarchy(Hierarchy hier)
	{
		return subHierarchies.add(hier);
		
		// Whenever a Hierarchy is added as a child to another Hierarchy, the
		// inherited Attributes must be adjusted so that that child and its children
		// include all Attributes that must be inherited.

		hier.addHierarchyAttributes();
	}
	
	
	protected boolean removeSubHierarchy(Hierarchy hier)
	{
		// Remove inherited Attributes.
		hier.removeHierarchyAttributes();
		
		return subHierarchies.remove(hier);
	}
	
	
	public void deleteChild(ModelElement child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Delete (per the Delete Pattern) or disconnect the Node from any other
		// child Nodes known to this class that depend on the existence of the Node
		// that is being deleted. These are normally siblings of the Node that
		// is being deleted.
		
		// Nullify references to the Node that this class knows about.
		if (child instanceof Activity)
		{
			Activity activity = (Activity)child;
			List<Port> ports = activity.getPorts();
			for (Port port : ports)
			{
				Set<Conduit> conduits = new TreeSetNullDisallowed<Conduit>();
				conduits.addAll(port.getSourceConnections());
				conduits.addAll(port.getDestinationConnections());
				
				for (Conduit conduit : conduits)
				{
					ModelContainer conduitParent = conduit.getModelContainer();
					conduitParent.deleteConduit(conduit, outermostAffectedRef);
				}
			}
		}
		else if (child instanceof Function)
		{
			Function function = (Function)child;
			List<Port> ports = function.getPorts();
			for (Port port : ports)
			{
				Set<Conduit> conduits = new TreeSetNullDisallowed<Conduit>();
				conduits.addAll(port.getSourceConnections());
				conduits.addAll(port.getDestinationConnections());
				
				for (Conduit conduit : conduits)
				{
					ModelContainer conduitParent = conduit.getModelContainer();
					conduitParent.deleteConduit(conduit, outermostAffectedRef);
				}
			}
		}
		else if (child instanceof Conduit)
		{
			Conduit conduit = (Conduit)child;
			
			try { conduit.disconnectFromPort(conduit.getPort1()); }
			catch (ValueNotSet w) {}
			
			try { conduit.disconnectFromPort(conduit.getPort2()); }
			catch (ValueNotSet w) {}
		}
		else if (child instanceof Constraint)
		{
		}

		// Set outermostAffectedRef argument, if any.
		setRefArgIfNotNull(outermostAffectedRef, this);
		
		// Call super.deleteChild.
		super.deleteChild(child, outermostAffectedRef);
	}

			
	/**
	 * Return true if this Container owns a Conduit (as an immediate child) that
	 * connects the specified Ports.
	 */
	 
	boolean connects(Port portA, Port portB)
	{
		Set<Conduit> conduits = getConduits();
		for (Conduit conduit : conduits)
		{
			Port port1 = null;
			Port port2 = null;
			
			try
			{
				port1 = conduit.getPort1();
				port2 = conduit.getPort2();
			}
			catch (ValueNotSet w) { continue; }
			
			if (
				((port1 == portA) && (port2 == portB)) ||
				((port2 == portA) && (port1 == portB))
				)
				return true;
		}
		
		return false;
	}


	public void dump() { dump(0); }


	public void dump(int indentation)
	{
		super.dump(indentation);

		Set<Function> functions = getFunctions();
		Set<Activity> activities = getActivities();
		Set<Conduit> conduits = getConduits();
		Set<Constraint> constraints = getConstraints();
		for (ModelElement e: functions) e.dump(indentation + 1);
		for (ModelElement e: activities) e.dump(indentation + 1);
		for (ModelElement e: conduits) e.dump(indentation + 1);
		for (ModelElement e: constraints) e.dump(indentation + 1);
	}
	

  /* Proxy classes needed by HierarchHelper */
	

	/**
	 * Note: All of the methods in this class delegate to the corresponding methods
	 * in this ModelContainerImpl class because the super class of ModelContainerImpl
	 * does not implement HierarchyElement, but ModelContainerImpl does. For this to
	 * work, those methods in ModelContainerImpl must not directly or indirectly
	 * delegate to the methods in SuperHierarchyElement: otherwise, there will be
	 * an endless loop.
	 */
	
	private class SuperHierarchyElement extends SuperCrossReferenceable implements HierarchyElement
	{
		public HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return ModelContainerImpl.this.createHierarchyAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		public HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return ModelContainerImpl.this.createHierarchyAttribute(layoutBound, outermostAffectedRef); }
	
		public HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return ModelContainerImpl.this.createHierarchyAttribute(name, layoutBound, outermostAffectedRef); }
		
		public HierarchyAttribute createHierarchyAttribute(HierarchyAttribute definingAttr,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws ParameterError
		{ return ModelContainerImpl.this.createHierarchyAttribute(definingAttr,
			layoutBound, outermostAffectedRef); }
		
		public HierarchyDomain getHierarchyDomain()
		{ return ModelContainerImpl.this.getHierarchyDomain(); }
	}
	
	
	private class SuperCrossReferenceable extends SuperPersistentNode implements CrossReferenceable
	{
		public Set<CrossReference> getCrossReferences()
		{ return ModelContainerImpl.super.getCrossReferences(); }
		
		
		public void addRef(CrossReference cr)
		throws
			ParameterError
		{ ModelContainerImpl.super.addRef(cr); }
		
		
		public Set<NamedReference> getNamedReferences()
		{ return ModelContainerImpl.super.getNamedReferences(); }
		
		
		public NamedReference getNamedReference(String refName)
		{ return ModelContainerImpl.super.getNamedReference(refName); }
		
		
		public Set<CrossReferenceable> getToNodes(NamedReference nr)
		{ return ModelContainerImpl.super.getToNodes(nr); }
		
		
		public Set<CrossReferenceable> getFromNodes(NamedReference nr)
		{ return ModelContainerImpl.super.getFromNodes(nr); }
		
		
		public Set<CrossReferenceable> getToNodes(String refName)
		throws
			ParameterError
		{ return ModelContainerImpl.super.getToNodes(refName); }
		
		
		public Set<CrossReferenceable> getFromNodes(String refName)
		throws
			ParameterError
		{ return ModelContainerImpl.super.getFromNodes(refName); }
		
		
		public void removeRef(CrossReference cr)
		throws
			ParameterError
		{ ModelContainerImpl.super.removeRef(cr); }
			
			
		public void addCrossReferenceCreationListener(CrossReferenceCreationListener listener)
		{ ModelContainerImpl.super.addCrossReferenceCreationListener(listener); }
		
		public void addCrossReferenceDeletionListener(CrossReferenceDeletionListener listener)
		{ ModelContainerImpl.super.addCrossReferenceDeletionListener(listener); }
		
		public List<CrossReferenceCreationListener> getCrossRefCreationListeners()
		{ return ModelContainerImpl.super.getCrossRefCreationListeners(); }
		
		public List<CrossReferenceDeletionListener> getCrossRefDeletionListeners()
		{ return ModelContainerImpl.super.getCrossRefDeletionListeners(); }
		
		public void removeCrossReferenceListener(CrossReferenceListener listener)
		{ ModelContainerImpl.super.removeCrossReferenceListener(listener); }
		
		public void signalCrossReferenceCreated(CrossReference cr)
		throws
			Exception
		{ ModelContainerImpl.super.signalCrossReferenceCreated(cr); }
		
		public void signalCrossReferenceDeleted(NamedReference nr,
			CrossReferenceable fromNode, CrossReferenceable toNode)
		throws
			Exception
		{ ModelContainerImpl.super.signalCrossReferenceDeleted(nr, fromNode, toNode); }
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return ModelContainerImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return ModelContainerImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return ModelContainerImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return ModelContainerImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelContainerImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelContainerImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ ModelContainerImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return ModelContainerImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return ModelContainerImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Set<Attribute> getAttributes()
		{ return ModelContainerImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return ModelContainerImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return ModelContainerImpl.super.getAttributeSeqNo(attr); }
		

		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ ModelContainerImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelContainerImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelContainerImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ ModelContainerImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ ModelContainerImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return ModelContainerImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return ModelContainerImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return ModelContainerImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return ModelContainerImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return ModelContainerImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return ModelContainerImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return ModelContainerImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return ModelContainerImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return ModelContainerImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelContainerImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelContainerImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelContainerImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ ModelContainerImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelContainerImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return ModelContainerImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return ModelContainerImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return ModelContainerImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return ModelContainerImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ ModelContainerImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return ModelContainerImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ ModelContainerImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return ModelContainerImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return ModelContainerImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return ModelContainerImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ ModelContainerImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return ModelContainerImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ ModelContainerImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return ModelContainerImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return ModelContainerImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return ModelContainerImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return ModelContainerImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return ModelContainerImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return ModelContainerImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return ModelContainerImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return ModelContainerImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ ModelContainerImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return ModelContainerImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return ModelContainerImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return ModelContainerImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return ModelContainerImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return ModelContainerImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ ModelContainerImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return ModelContainerImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ ModelContainerImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return ModelContainerImpl.super.isVisible(); }
		
		
		public void layout()
		{ ModelContainerImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return ModelContainerImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ ModelContainerImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return ModelContainerImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ ModelContainerImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ ModelContainerImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return ModelContainerImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ ModelContainerImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return ModelContainerImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ ModelContainerImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return ModelContainerImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ ModelContainerImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return ModelContainerImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return ModelContainerImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return ModelContainerImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ ModelContainerImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ ModelContainerImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return ModelContainerImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ ModelContainerImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return ModelContainerImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ ModelContainerImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return ModelContainerImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return ModelContainerImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return ModelContainerImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return ModelContainerImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ ModelContainerImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return ModelContainerImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ throw new RuntimeException("Should not be called"); }
		
		public void setViewClassName(String name)
		{ ModelContainerImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return ModelContainerImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return ModelContainerImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return ModelContainerImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return ModelContainerImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return ModelContainerImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return ModelContainerImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return ModelContainerImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ ModelContainerImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return ModelContainerImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return ModelContainerImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return ModelContainerImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return ModelContainerImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return ModelContainerImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return ModelContainerImpl.super.getX(); }
		public double getY() { return ModelContainerImpl.super.getY(); }
		public double getScale() { return ModelContainerImpl.super.getScale(); }
		public double getOrientation() { return ModelContainerImpl.super.getOrientation(); }
		public void setX(double x) { ModelContainerImpl.super.setX(x); }
		public void setY(double y) { ModelContainerImpl.super.setY(y); }
		public void setLocation(double x, double y) { ModelContainerImpl.super.setLocation(x, y); }
		public void setScale(double scale) { ModelContainerImpl.super.setScale(scale); }
		public void setOrientation(double angle) { ModelContainerImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return ModelContainerImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return ModelContainerImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return ModelContainerImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return ModelContainerImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ ModelContainerImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return ModelContainerImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return ModelContainerImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return ModelContainerImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return ModelContainerImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return ModelContainerImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return ModelContainerImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ ModelContainerImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return ModelContainerImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return ModelContainerImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return ModelContainerImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelContainerImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ ModelContainerImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelContainerImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return ModelContainerImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return ModelContainerImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return ModelContainerImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return ModelContainerImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return ModelContainerImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ ModelContainerImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return ModelContainerImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ ModelContainerImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ ModelContainerImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ ModelContainerImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return ModelContainerImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return ModelContainerImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return ModelContainerImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ ModelContainerImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return ModelContainerImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ ModelContainerImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return ModelContainerImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ ModelContainerImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ ModelContainerImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ ModelContainerImpl.super.printChildrenRecursive(indentationLevel); }
	}
}
