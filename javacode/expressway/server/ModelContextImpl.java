/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.Set;
import java.util.List;
import generalpurpose.TreeSetNullDisallowed;
import java.io.Serializable;


/**
 * See interface specification, {@link ModelElement.ModelContext}.
 *
 * Not a persistent class.
 */

public abstract class ModelContextImpl implements ModelContext
{
	private SimulationRunImpl simRun = null;
	private ModelComponent component = null;  // that is represented by this ModelContext.

	private SortedEventSet<GeneratedEvent> newEvents = new SortedEventSetImpl<GeneratedEvent>();


	ModelContextImpl(SimulationRunImpl simRun, ModelComponent component)
	{
		this.simRun = simRun;
		this.component = component;
	}


	public ModelComponent getComponent() { return component; }


	public SimulationRun getSimulationRun() { return simRun; }


	public long getSimulationStartTimeMs()
	{
		return simRun.getStartingDate().getTime();
	}


	public GeneratedEvent setState(State state, Serializable value, 
		SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ModelContainsError
	{
		// Check that the State is owned by the ModelComponent that is represented
		// by this ModelContext.

		EventProducer eventProducer = state.getEventProducer();
		if (eventProducer != this.component)
			throw new ModelContainsError(
				"Attempt to generate an Event for a State that is not owned " +
				"by the enclosing component.");


		GeneratedEvent genEvent = null;
		try
		{
			genEvent =
				internal_schFutureStateChng(simRun, state,
					getCurrentSimulationEpoch().getDate(), value, triggeringEvents);
		}
		catch (ParameterError er)
		{
			throw new RuntimeException(er);  // should never happen.
		}

		return genEvent;
	}


	public GeneratedEvent scheduleFutureStateChange(State state, Date time,
		Serializable value, SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ModelContainsError,
		ParameterError // if the specified time is earlier than
			// the current time.
	{
		if (time == null) time = getCurrentSimulationEpoch().getDate();
		else if (! time.after(getCurrentSimulationEpoch().getDate()))
			throw new ParameterError(
				"Attempt to schedule a future event at the present or an earlier time.");

		return internal_schFutureStateChng(simRun, state, time, value, triggeringEvents);
	}
	
	
	public GeneratedEvent scheduleCompensationEvent(State state, Serializable value,
		SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ModelContainsError
	{
		try { return internal_schFutureStateChng(simRun, state, 
			getCurrentSimulationEpoch().getDate(), value, triggeringEvents); }
		catch (ParameterError pe)
		{
			throw new RuntimeException(pe);
		}
	}
	

	public GeneratedEvent scheduleDelayedStateChange(State state,
		long msDelay, Serializable value, SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ModelContainsError,
		ParameterError
	{
		// Check that the delay value is non-negative.

		if (msDelay < 0) throw new ParameterError(
			"Negative delay specified for future event");

		if (msDelay == 0)
			if (getCurrentSimulationEpoch() != null)  // not prior to first iteration
			{
				String triggers = "";
				if (triggeringEvents != null)
				{
					boolean first = true;
					for (GeneratedEvent e : triggeringEvents)
					{
						if (first)
						{
							first = false;
							triggers += "; triggering Events: ";
						}
						else
							triggers += ",\n";
						
						triggers += ("[" + e.toString() + "]");
					}
				}
				
				throw new ParameterError("In iteration " +
						getCurrentSimulationEpoch().getIteration() +
						", zero delay specified for future event on " + 
						state.getFullName() + triggers);
			}


		Date futureTime = null;
		try
		{
			futureTime = new Date(getCurrentSimulationEpoch().getTime() + msDelay);
		}
		catch (Throwable t)
		{
			throw new ParameterError(
				"Unable to construct future time: probably out of range");
		}

		return internal_schFutureStateChng(simRun, state, futureTime, value, triggeringEvents);
	}


	public ModelComponent getComponent(String name)
	{
		// First look within this Component.

		if (component instanceof ModelContainer)
		{
			// Look within this component.

			ModelElement element = null;
			try
			{
				element = ((ModelContainer)component).getSubcomponent(name);
			}
			catch (ElementNotFound ex)
			{
				// nothing.
			}

			if ((element != null) && (element instanceof ModelComponent))
				return (ModelComponent)element;
		}

		// Not found: look within enclosing ModelContainer (if any).

		for	// each parent, until name is found among parent's children.
		(
			ModelContainer container = component.getModelContainer();
			container != null;
			container = container.getModelContainer()
		)
		{
			ModelElement element = null;

			try
			{
				element = container.getSubcomponent(name);
			}
			catch (ElementNotFound ex)
			{
				// nothing.
			}

			if ((element != null) && (element instanceof ModelComponent))
				return (ModelComponent)element;
		}

		return null;
	}


	public Port getPort(String name)
	{
		if (! (component instanceof PortedContainer)) return null;
		
		List<Port> ports = ((PortedContainer)component).getPorts();

		for (Port port : ports)
		{
			if (port.getName().equals(name)) return port;
		}
		
		return null;
	}


	public State getState(String name)
	{
		// First look within this Component.

		if (component instanceof EventProducer)
		{
			// Look within this component.

			State state = ((EventProducer)component).getState(name);

			if (state != null) return state;
		}

		// Not found: look within enclosing ModelContainer (if any).

		for	// each parent, until name is found among parent's states.
		(
			ModelContainer container = component.getModelContainer();
			container != null;
			container = container.getModelContainer()
		)
		{
			if (! (container instanceof EventProducer)) continue;

			State state = ((EventProducer)container).getState(name);
			if (state != null) return state;
		}

		return null;
	}


	public Serializable getAttributeValue(String qualifiedName)
	throws
		ModelContainsError,
		ParameterError
	{
		// Find within the component's ModelDomain.

		ModelElement element = component.findModelElement(qualifiedName);
		if (element == null)
		{
			//System.out.println("Found no Attribute named " + qualifiedName);
			throw new ModelContainsError("Found no Attribute named " +
				qualifiedName);
		}

		if (! (element instanceof ModelAttribute))
		{
			//System.out.println("Found " + element.toString());
			throw new ModelContainsError(qualifiedName + " is not a ModelAttribute");
		}

		ModelScenario modelScenario = simRun.getModelScenario();

		Serializable value = modelScenario.getAttributeValue((ModelAttribute)element);

		return value;
	}


	public Epoch getCurrentSimulationEpoch()
	{
		return simRun.getCurrentSimulationEpoch();
	}
	
	
	public long getElapsedSimulationMs()
	{
		return simRun.getActualElapsedTime();
	}


	public SortedEventSet<GeneratedEvent> getAndPurgeNewEvents()
	{
		SortedEventSet<GeneratedEvent> newEvents = this.newEvents;
		this.newEvents = new SortedEventSetImpl();
		return newEvents;
	}


	public int purgeFutureEvents()
	{
		 if (! (component instanceof Activity)) return 0;
		 
		 return simRun.purgeFutureEvents((Activity)component);
	}


	public Set<Port> getSensitivePorts(State state)
	//public Set<Port> getConnectedPorts(State state)
	throws
		ModelContainsError
	{
		return component.getModelDomain().determineSensitivePorts(state);
		//return component.getModelDomain().determineConnectedPorts(state);
	}
	
	
	public Set<Port> getEventPorts(Event event)
	throws
		ModelContainsError
	{
		return getSensitivePorts(event.getState());
	}

	
	public Serializable getStateValue(State state)
	{
		return getSimulationRun().getStateValue(state);
	}


	public Serializable getStateValue(Port port)
	{
		// Identify the State that is currently driving the specified Port.
		// This is done by retrieving the State from the Map<Port, State>.

		State state = getSimulationRun().getCurrentDriver(port);
				
		if (state == null) return null;
		
		Serializable value = ((SimulationRunImpl)(getSimulationRun())).getStateValue(state);

		//Serializable value = getSimulationRun().getStateValueAtEpoch(
		//	state, getCurrentSimulationEpoch());
			
			
		return value;
	}


	public State getCurrentDriver(Port port)
	throws
		ModelContainsError,  // if the parameter is not found.
		ParameterError
	{
		return getSimulationRun().getCurrentDriver(port);
	}
	
	
	public long getElapsedMs(GeneratedEvent e)
	throws
		ModelContainsError,
		ParameterError
	{
		long msNow = getCurrentSimulationEpoch().getTime();
		
		long msScheduled;
		
		if (e.getWhenScheduled() == null)  // use the simulation start time
			msScheduled = this.getSimulationStartTimeMs();
		else
			msScheduled = e.getWhenScheduled().getDate().getTime();
		
		return msNow - msScheduled;
	}
	
	
	/*
	public boolean hasEventInThisEpoch(State state)
	throws
		ParameterError
	{
		...
	}


	public boolean hasEventInThisEpoch(Port port)
	throws
		ParameterError
	{
		...
	}
	*/


	/**
	 * Create a future Event for the SimulationRun and add it to the
	 * Set of new Events for this ModelContext; then return the Event.
	 */
	 
	protected GeneratedEvent internal_schFutureStateChng(SimulationRun simRun,
		State state, Date time, Serializable value, SortedEventSet<GeneratedEvent> triggeringEvents)
	throws
		ModelContainsError,
		ParameterError // if the specified time is earlier than
			// the current time.
	{
		if (time == null) throw new RuntimeException("time is null.");
		
		//if (value == null) throw new ModelContainsError(
		//	"New value for State " + state.getFullName() + " at time " + time +
		//	" is null");

		if (time.before(getCurrentSimulationEpoch().getDate()))
			throw new ParameterError("Attempt to generate event at a time that is " +
				"earlier than the current simulation time");


		GeneratedEvent genEvent = simRun.generateEvent(state, time, value, triggeringEvents);
		newEvents.add(genEvent);

		return genEvent;
	}
}


