/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.DecisionElement.*;
import expressway.server.Event.*;
import expressway.server.OperationMode.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import java.net.URL;

import expressway.server.NamedReference.*;
import geometry.*;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;


public class ModelDomainImpl extends ModelContainerImpl implements ModelDomain,
	MenuOwner, Cloneable
{
	private static final double PreferredWidthInPixels = 1000.0;
	private static final double PreferredHeightInPixels = 800.0;
	
	private int simRunNumber = 0;
	private HierarchyDomainHelper helper;
	
	
	/** Ownership. */
	Boolean externalStateBindingType = true;  // use averages
	
	/** Ownership. */
	TimePeriodType externalStateGranularity = DefaultTimePeriodForExternalStates;
	
	Set<AttributeStateBinding> attributeStateBindings =
		new TreeSetNullDisallowed<AttributeStateBinding>();  // Note that this list is redundant with
			// respect to each ModelElement's Attribute's list of AttributeStateBindings.

	Set<VariableAttributeBinding> varAttrBindings =
		new TreeSetNullDisallowed<VariableAttributeBinding>();    // Note that this list is
			// redundant with respect to each Variable's list of VariableAttributeBindings.

	Set<ModelComponent> compsWithNativeImpls = new HashSet<ModelComponent>();
		// This is a convenience Set. Each model component that has a native
		// implementation class defined should be added to this Set. Note that
		// this Set is NOT an ownership set.

	
	public ModelDomainImpl(String name)
	{
		super(name, null, null);
		this.setDomain(this);
		this.setViewClassName("expressway.gui.modeldomain.ModelScenarioViewPanel");
		setResizable(true);
		
		// Provide initial values that will make the new Domain visible.
		this.setWidth(500);
		this.setHeight(500);
		
		this.helper = new HierarchyDomainHelper(this, new SuperDomain(), getHierarchyHelper());
	}
	
	
	public String getTagName() { return "model_domain"; }
	
	
	public String getDefaultDomainViewTypeName() { return "Model"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ModelDomainIconImageName);
	}
	
	
	public Class getSerClass() { return ModelDomainSer.class; }
	
	
  /* *************************************************************************
   * From Domain
   */
	
	
	public void setScenarios(List<Scenario> ss) { helper.setScenarios(ss); }
	
	
	public void setMenuItems(List<MenuItem> mis) { helper.setMenuItems(mis); }
	
	
	public void setMotifDefs(Set<MotifDef> mds) { helper.setMotifDefs(mds); }
	
	
	public void setScenarioSets(List<ScenarioSet> sss) { helper.setScenarioSets(sss); }
	
	
	public void setNamedReferences(Set<NamedReference> nrs) { helper.setNamedReferences(nrs); }
	
	
	public void setVisualGuidance(VisualGuidance vg) { helper.setVisualGuidance(vg); }


	public String getSourceName() { return helper.getSourceName(); }
	
	
	public void setSourceName(String name) { helper.setSourceName(name); }
	
	
	public URL getSourceURL() { return helper.getSourceURL(); }
	
	
	public void setSourceURL(URL url) { helper.setSourceURL(url); }
	
	
	public String getDeclaredName() { return helper.getDeclaredName(); }
	
	
	public void setDeclaredName(String name) { helper.setDeclaredName(name); }
	
	
	public String getDeclaredVersion() { return helper.getDeclaredVersion(); }
	
	
	public void setDeclaredVersion(String version) { helper.setDeclaredVersion(version); }
	
		
	public Set<MotifDef> getMotifDefs()
	{
		return helper.getMotifDefs();
	}
	
	
	public void addMotifDef(MotifDef md)
	{
		helper.addMotifDef(md);
	}
	
	
	public void removeMotifDef(MotifDef md)
	{
		helper.removeMotifDef(md);
	}
	
	
	public Set<NamedReference> getNamedReferences() { return helper.getNamedReferences(); }
	
	
	public NamedReference getNamedReference(String nrName)
	{
		return helper.getNamedReference(nrName);
	}

	
	public NamedReference createNamedReference(String name, String desc,
		Class fromType, Class toType)
	throws
		ParameterError
	{
		return helper.createNamedReference(name, desc, fromType, toType);
	}

	
	public void deleteNamedReference(NamedReference nr)
	{
		helper.deleteNamedReference(nr);
	}
	
	
	public boolean usesMotif(MotifDef md) { return helper.usesMotif(md); }
	

	public List<Scenario> getScenarios()
	{
		return helper.getScenarios();
	}


	public Scenario getScenario(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return helper.getScenario(name);
	}	


	public Scenario getCurrentScenario()
	{
		return helper.getCurrentScenario();
	}
	
	
	public void setCurrentScenario(Scenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		helper.setCurrentScenario(scenario);
	}


	public Scenario createScenario(String name)
	throws
		ParameterError
	{
		return helper.createScenario(name);
	}


	public Scenario createScenario()
	throws
		ParameterError
	{
		return helper.createScenario();
	}


	public Scenario createScenario(String name, Scenario scenario)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		if (scenario != null)
			if (! (scenario instanceof ModelScenario)) throw new ParameterError(
				"Scenario '" + scenario.getFullName() + "' is not a ModelScenario");

		if (name == null) name = createUniqueChildName("Scenario");
		else if (name.equals("")) throw new ParameterError(
			"Cannot use an empty string for a Scenario name");
		else
		{
			List<Scenario> scenarios = getScenarios();
			for (Scenario s : scenarios)
			{
				if (s.getName().equals(name)) throw new ParameterError(
					"Domain " + this.getName() + 
					" already contains a Scenario named " + name);
			}
		}
		
		ModelScenario newScenario = null;

		if (scenario == null)
			newScenario = new ModelScenarioImpl(name, this);
		else
		{
			newScenario = ((ModelScenario)scenario).cloneForSimulation();
			((ModelScenarioImpl)newScenario).setName(createUniqueChildName(name));
		}

		helper.addScenario(newScenario);
		
		ServiceContext context = ServiceContext.getServiceContext();
		context.addScenario(newScenario);

		try { helper.setCurrentScenario(newScenario); }
		catch (ElementNotFound ex) { throw new RuntimeException("Should not happen"); }

		return newScenario;
	}
	
	
	public void deleteScenario(Scenario scenario)
	throws
		ParameterError
	{
		helper.deleteScenario(scenario);
	}
	
	
	public void addScenario(Scenario scenario)
	{
		helper.addScenario(scenario);
	}
	
	
	public ScenarioSet createScenarioSet(String name, 
		Scenario baseScenario, Attribute attr, Serializable[] values)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		if (! (baseScenario instanceof ModelScenario)) throw new ParameterError(
			"base Scenario '" + baseScenario.getFullName() + "' is not a ModelScenario");
		ModelScenario baseModelScenario = (ModelScenario)baseScenario;
		
		if (! (attr instanceof ModelAttribute)) throw new ParameterError(
			"Attribute '" + attr.getFullName() + "' is not a ModelAttribute");
		
		String setName = name;
		if (setName == null) setName = baseScenario.getName() + "_" + attr.getName();
		

		// For each value of the Attribute's Iterator (in the context of a selected Scenario)

		SortedSet<ModelScenario> newScenarios = new TreeSetNullDisallowed<ModelScenario>();
		
		if (values != null) for (Serializable value : values)
		{
			// Create a Scenario for that value and link it to the ScenarioSet.
			
			ModelScenario newScenario = null;
			try { newScenario = (ModelScenario)(baseScenario.createIndependentCopy()); }
			catch (CloneNotSupportedException ex) { throw new ParameterError(ex); }
			
			newScenario.setAttributeValue(attr, value);
			newScenarios.add(newScenario);
		}
		
		ModelScenarioSetImpl scenarioSet = new ModelScenarioSetImpl(baseModelScenario,
			createUniqueModelScenarioSetName(setName), (ModelAttribute)attr, newScenarios);
		
		scenarioSet.setStartingDate(baseModelScenario.getStartingDate());
		scenarioSet.setFinalDate(baseModelScenario.getFinalDate());
		scenarioSet.setIterationLimit(baseModelScenario.getIterationLimit());
		scenarioSet.setDuration(baseModelScenario.getDuration());
		
		helper.addScenarioSet(scenarioSet);
		
		return scenarioSet;
	}
	
	
	public void deleteScenarioSet(ScenarioSet scenarioSet)
	throws
		ParameterError
	{
		helper.deleteScenarioSet(scenarioSet);
	}
	
	
	public void addScenarioSet(ScenarioSet scenarioSet)
	{
		helper.addScenarioSet(scenarioSet);
	}
	
	
	public List<ScenarioSet> getScenarioSets()
	{
		return helper.getScenarioSets();
	}
	
	
	public ScenarioSet getScenarioSet(String name)
	throws
		ElementNotFound
	{
		return helper.getScenarioSet(name);
	}
	
	
	public VisualGuidance getVisualGuidance()
	{
		return helper.getVisualGuidance();
	}
	
	
  /* *************************************************************************
   * From ModelDomain
   */
	
	
	public List<ModelScenario> getModelScenarios()
	{
		List<Scenario> ss = getScenarios();
		List<ModelScenario> mss = new Vector<ModelScenario>();
		for (Scenario s : ss) mss.add((ModelScenario)s);
		return mss;
	}
	
	
	public ModelDomain createIndependentCopy()
	throws
		CloneNotSupportedException
	{
		String name = createUniqueModelDomainName(this.getName());
		
		ModelDomain newDomain = (ModelDomain)(this.clone(new CloneMap(this), null));
		
		try { ((ModelDomainImpl)newDomain).setName(name); }
		catch (ParameterError pe) { throw new RuntimeException(
			"Should not happen: error setting name, when the name is created by Expressway"); }
		
		return newDomain;
	}
	
	
	public Set<State> getBoundStates()
	{
		Set<State> states = new TreeSetNullDisallowed<State>();
		
		for (AttributeStateBinding asBinding : attributeStateBindings) try
		{
			states.add(asBinding.getForeignState());
		}
		catch (ValueNotSet w) { continue; } // to next binding.
		
		return states;
	}
	
	
	public Set<ModelDomain> getBindingDomains()
	{
		// Return the Domains that own Attributes that are bound to States of 
		// this Domain.
		
		Set<ModelDomain> domains = new TreeSetNullDisallowed<ModelDomain>();
		
		List<State> states = this.getStatesRecursive();
		for (State state: states)
		{
			Set<AttributeStateBinding> bindings = state.getAttributeBindings();
			for (AttributeStateBinding binding : bindings) try
			{
				domains.add(binding.getAttribute().getModelDomain());
			}
			catch (ValueNotSet w) { continue; }  // to next binding.
		}
		
		return domains;
	}
		
		
	public Set<ModelDomain> getBoundDomains()
	{
		Set<ModelDomain> domains = new TreeSetNullDisallowed<ModelDomain>();
		
		for (AttributeStateBinding asBinding : attributeStateBindings) try
		{
			domains.add(asBinding.getForeignState().getModelDomain());
		}
		catch (ValueNotSet w) { continue; }  // to next binding.
		
		return domains;
	}


	public boolean getExternalStateBindingType()
	{
		return this.externalStateBindingType;
	}
	
	
	public void setExternalStateBindingType(Boolean averages)
	{
		this.externalStateBindingType = averages;
	}
	
	
	public TimePeriodType getExternalStateGranularity()
	{
		return this.externalStateGranularity;
	}
	
	
	public void setExternalStateGranularity(TimePeriodType periodType)
	{
		this.externalStateGranularity = periodType;
	}


	public boolean hasSimulationRuns()
	{
		List<Scenario> ss = getScenarios();
		for (Scenario scenario : ss)
		{
			if (((ModelScenario)scenario).getSimulationRuns().size() > 0) return true;
		}
		
		return false;
	}
	
	
	public Set<SimulationRun> getDependentSimulationRuns()
	{
		return getDependentSimulationRuns(new TreeSetNullDisallowed<SimulationRun>());
	}
	
	
	Set<SimulationRun> getDependentSimulationRuns(Set<SimulationRun> simRuns)
	{
		// A SimulationRun is dependent on this ModelDomain if it belongs to a
		// Domain that binds this Domain, directly or indirectly.
		
		simRuns.addAll(this.getSimulationRuns());
		
		Set<ModelDomain> boundDomains = this.getBindingDomains();
		for (ModelDomain d : boundDomains)
		{
			((ModelDomainImpl)d).getDependentSimulationRuns(simRuns);
		}
		
		return simRuns;
	}


	public boolean dependentSimulationRunsExist()
	{
		// Return true if (1) there are SimulationRuns for this ModelDomain, or
		// (2) there are SimulationRuns for any higher level models that
		// bind to this Domain.
		
		if (this.hasSimulationRuns()) return true;
		
		Set<ModelDomain> boundDomains = this.getBindingDomains();
		for (ModelDomain d : boundDomains)
			if (d.dependentSimulationRunsExist()) return true;
		
		return false;
	}
	
	
	public Set<ModelDomain> deleteDependentSimulationRuns()
	{
		Set<ModelDomain> dependentDomains = new TreeSetNullDisallowed<ModelDomain>();
		dependentDomains.add(this);
		
		this.deleteSimulationRuns();
		
		Set<ModelDomain> boundDomains = this.getBindingDomains();
		for (ModelDomain d : boundDomains)
			dependentDomains.addAll(d.deleteDependentSimulationRuns());
		
		return dependentDomains;
	}
	
	
	public ModelScenario getModelScenario(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return (ModelScenario)(getScenario(name));
	}
	
	
	public void addModelScenario(ModelScenario scenario)
	{
		addScenario(scenario);
	}
	

	public ModelScenarioSet getModelScenarioSet(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return (ModelScenarioSet)(getScenarioSet(name));
	}
	
	
	public List<ModelScenarioSet> getModelScenarioSets()
	{
		List<ScenarioSet> sss = getScenarioSets();
		List<ModelScenarioSet> msss = new Vector<ModelScenarioSet>();
		for (ScenarioSet ss : sss) msss.add((ModelScenarioSet)ss);
		return msss;
	}
	
	
	public void addModelScenarioSet(ModelScenarioSet s)
	{
		addScenarioSet(s);
	}
		
		
	public ModelScenario getCurrentModelScenario()
	{
		return (ModelScenario)(getCurrentScenario());
	}


	public void setCurrentModelScenario(ModelScenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		setCurrentScenario(scenario);
	}

	
	public void addVariableAttributeBinding(
		VariableAttributeBinding binding)
	{
		varAttrBindings.add(binding);
	}


	public Set<AttributeStateBinding> getAttributeStateBindings() {
		return attributeStateBindings; }


	public Set<ModelComponent> getComponentsWithNativeImpls()
	{
		return compsWithNativeImpls;
	}
	
	
	public Set<Port> determineReachablePorts(Set<Conduit> conduits)
	{
		Set<Port> reachablePorts = new TreeSetNullDisallowed<Port>();
		
		for (Conduit conduit : conduits)
		{
			try { reachablePorts.add(conduit.getPort1()); } catch (ValueNotSet w) {}
			try { reachablePorts.add(conduit.getPort2()); } catch (ValueNotSet w) {}
		}
		
		return reachablePorts;
	}


	public Set<Port> determineReachablePorts(State state)
	throws
		ModelContainsError
	{
		// Find all Conduits that are sensitive to the State.
		
		Set<Conduit> sensitiveConduits = this.determineSensitiveConduits(state);
		
		
		// Identify the Ports that are connected to those Conduits, are input-capable, 
		// and belong to native Components. Note that this relies on the fact that
		// behavioral Components may not contain Conduits.
		
		Set<Port> sensitivePorts = new TreeSetNullDisallowed<Port>();
		
		Port[] ports = new Port[] { null, null} ;
		
		for (Conduit c : sensitiveConduits)
		{
			try { ports[0] = c.getPort1(); } catch (ValueNotSet w) {}
			try { ports[1] = c.getPort2(); } catch (ValueNotSet w) {}
		
			for (Port p : ports)
			{
				if (p == null) continue;
				if (p.allowsInputEvents()) sensitivePorts.add(p);
			}
		}
		
		sensitivePorts.addAll(state.getPortBindings());
		
		return sensitivePorts;
	}
	

	public Set<Port> determineSensitivePorts(State state)
	//public Set<Port> determineConnectedPorts(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		// Find all Conduits that are sensitive to the State.
		
		Set<Conduit> sensitiveConduits = this.determineSensitiveConduits(state);
		
		
		// Identify the Ports that are connected to those Conduits, are input-capable, 
		// and belong to native Components. Note that this relies on the fact that
		// behavioral Components may not contain Conduits.
		
		Set<Port> sensitivePorts = new TreeSetNullDisallowed<Port>();
		
		Port[] ports = new Port[] { null, null };
		
		for (Conduit c : sensitiveConduits)
		{
			try { ports[0] = c.getPort1(); } catch (ValueNotSet w) {}
			try { ports[1] = c.getPort2(); } catch (ValueNotSet w) {}
		
			for (Port p : ports)
			{
				if (p == null) continue;
				if (p.allowsInputEvents())
				{
					PortedContainer container = p.getContainer();
					if (container instanceof Function)
					{
						if (((Function)container).getNativeImplementationClass() != null)
							sensitivePorts.add(p);
					}
					else if (container instanceof Activity)
					{
						if (((Activity)container).getNativeImplementationClass() != null)
							sensitivePorts.add(p);
					}
				}
			}
		}
		
		sensitivePorts.addAll(state.getPortBindings());
		
		return sensitivePorts;
	}


	/**
	 * Recursively identify all of the Ports that are sensitive to 'state'.
	 * A Port is sensitive to a State if (1) the Port can receive Events on the 
	 * State, (2) the Port can pass the Event into its Component, and (3) the 
	 * Component has native behavior. 
	 * The result includes Ports that are bound to the State. Do not visit the 
	 * same port more than once.
	 * The input argument contains the set of Ports that have already been
	 * visited.
	 *

	protected void determineSensitivePorts(Port port, Set<Port> connectedPorts)
	//protected void determineConnectedPorts(Port port, Set<Port> connectedPorts)
	{
		connectedPorts.add(port);

		Set<Port> ports = new TreeSetNullDisallowed<Port>();

		for (Conduit c : port.getSourceConnections())
		{
			//ports.add(c.getPort1());
			ports.add(c.getPort2());
		}

		for (Conduit c : port.getDestinationConnections())
		{
			ports.add(c.getPort1());
			//ports.add(c.getPort2());
		}

		for (Port p : ports)
		{
			if (! connectedPorts.contains(p))
				determineConnectedPorts(p, connectedPorts);
		}
	}*/
	

	public Set<Conduit> determineSensitiveConduits(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		Set<Port> boundPorts = state.getPortBindings(); // that are bound to state,
		Set<Conduit> conduits = determineSensitiveConduits(boundPorts);
		
		return determineReachableConduits(conduits);
	}
	
	
	public Set<Conduit> determineSensitiveConduits(Set<Port> ports)
	{
		Set<Conduit> conduits = new TreeSetNullDisallowed<Conduit>();
		
		for (Port port : ports)
		{
			conduits.addAll(determineSensitiveConduits(port));
		}
		
		return conduits;
	}
	
	
	public Set<Conduit> determineSensitiveConduits(Port port)
	{
		Set<Conduit> conduits = new TreeSetNullDisallowed<Conduit>();
		
		Set<Conduit> internalConnectedConduits = new TreeSetNullDisallowed<Conduit>();
			// all Conduits that belong to port's Container and that are attached 
			// to port.
			
		ModelContainer portContainer = port.getContainer();
		Set<Conduit> portConduits = portContainer.getConduits();
		for (Conduit portConduit : portConduits) try
		{
			if // portConduit is attached to port
			(
				(portConduit.getPort1() == port) || (portConduit.getPort2() == port)
			)
				internalConnectedConduits.add(portConduit);
		}
		catch (ValueNotSet w) { continue; }  // to next conduit.
			
		conduits.addAll(internalConnectedConduits);
		
		PortDirectionType dir = port.getDirection();
		if ((dir == PortDirectionType.output) || (dir == PortDirectionType.bi))
		{
			Set<Conduit> externalConnectedConduits = new TreeSetNullDisallowed<Conduit>();
				// all Conduits that are external to port's Container and that
				// are connected to port.
			
			externalConnectedConduits.addAll(port.getSourceConnections());
			externalConnectedConduits.addAll(port.getDestinationConnections());
				// Don't need to remove the internal connections, because
				// they must be included in 'conduits' regardless.
				
			conduits.addAll(externalConnectedConduits);
		}
		
		return conduits;
	}
	
	
	public Set<Function> determineSensitiveFunctions(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		Set<Function> functions = new TreeSetNullDisallowed<Function>();

		Set<Port> connectedPorts = determineSensitivePorts(state);
			// Includes bound ports.
			
		for (Port q : connectedPorts)
		{
			// Check if this is a Function.
			
			PortedContainer qContainer = q.getContainer();
			if (! (qContainer instanceof Function)) continue;
			
			Function qFunction = (Function)qContainer;
			
			
			// Check that the Function is not responding to its own Event.
				
			if (state.getEventProducer() == qFunction) continue;
			
			functions.add(qFunction);
			
			/*
			
			// Check that the Function is behavioral.
			
			if (qFunction.getNativeImplementationClass() == null) continue;
			
			
			// Check that the Port allows input Events.
			
			if (! q.allowsInputEvents()) continue;
			
			
			/
			 * A behavioral component C is sensitive to a change in State value if 
			 * (and only if) there is a path (via Conduits) from an output-capable 
			 * (output or bi) bound Port of the State owner to an input-capable
			 * (input or bi) Port of C. In the case in which C is the State owner, 
			 * the two Ports must not be the same.
			 * 
			 * I.e., Determine if there is a path from an output-capable bound Port
			 * of state to q, and q is not the bound Port of state.
			 *
			 * I.e., for each Port p bound to state, if p is output-capable and
			 * if Port q is in the connection set for p, then q's component is
			 * sensitive to state.
			 *
			
			for (Port p : state.getPortBindings())
			{
				if (p == q) continue;
				
				if (! p.allowsOutputEvents()) continue;
				
				Set<Port> connectedToP = new TreeSetNullDisallowed<Port>();
				...determineConnectedPorts(p, connectedToP);
				if (connectedToP.contains(q))
					functions.add(qFunction);
			}

			/
			boolean externalPathExists = false;
			if (state.getEventProducer() == function)
			{
				for (Port p : connectedPorts)
				{
					if (p == port) continue;
					
					if (p.getContainer() == function)
					{
						externalPathExists = true;
						break;  // found an external path between two ports.
					}
				}
					
				if (externalPathExists)
					functions.add(function);
			}
			else
				functions.add(function);
			*/
		}

		return functions;
	}
	
	
	public Set<Activity> determineSensitiveActivities(State state)
	throws
		ModelContainsError // when a model conflict of any kind is detected.
	{
		Set<Activity> activities = new TreeSetNullDisallowed<Activity>();

		Set<Port> connectedPorts = determineSensitivePorts(state);
			// Includes bound ports.

		
		for (Port q : connectedPorts)
		{
			// Check if the container is an Activity.
			
			PortedContainer qContainer = q.getContainer();
			if (! (qContainer instanceof Activity)) continue;
			

			Activity qActivity = (Activity)qContainer;
			
			
			// Check that the Activity is behavioral.
			
			if (qActivity.getNativeImplementationClass() == null) continue;
			
			
			// Check that the Port allows input Events.
			
			if (! q.allowsInputEvents()) continue;
			
			activities.add(qActivity);
			
			/*
			
			/
			 * A behavioral component C is sensitive to a change in State value if 
			 * (and only if) there is a path (via Conduits) from an output-capable 
			 * (output or bi) bound Port of the State owner to an input-capable
			 * (input or bi) Port of C. In the case in which C is the State owner, 
			 * the two Ports must not be the same.
			 * 
			 * I.e., Determine if there is a path from an output-capable bound Port
			 * of state to q, and q is not the bound Port of state.
			 *
			 * I.e., for each Port p bound to state, if p is output-capable and
			 * if Port q is in the connection set for p, then q's component is
			 * sensitive to state.
			 *
			
			for (Port p : state.getPortBindings())
			{
				if (p == q) continue;
				
				if (! p.allowsOutputEvents()) continue;
				
				Set<Port> connectedToP = new TreeSetNullDisallowed<Port>();
				...determineConnectedPorts(p, connectedToP);
				if (connectedToP.contains(q))
					activities.add(qActivity);
			}
		
		
			/
			boolean externalPathExists = false;
			if (state.getEventProducer() == qActivity)
			{
				for (Port p : connectedPorts)
				{
					if (p == port) continue;
					
					if (p.getContainer() == qActivity)
					{
						externalPathExists = true;
						break;  // found an external path between two ports.
					}
				}
					
				if (externalPathExists)
					activities.add(qActivity);
			}
			else
				activities.add(qActivity);
			*/
		}
				

		return activities;
	}


	/**
	 * Creates a thread to perform the required simulation, starts the thread,
	 * and then returns a reference to the simulation results container.
	 */

	public SimServiceThread simulate(
		ValueHistoryFactory valueHistoryFactory,
		SimRunSet simRunSet,
		ModelScenario modelScenario,
		SimCallback callback, Date startingEpoch,
		Date finalEpoch, int iterationLimit, long duration, long randomSeed)
	throws
		ParameterError,
		ModelContainsError
	{
		SimulationRun simRun = null;
		
		synchronized (this)
		{
			// Check if the model contains feedback among its Functions.
			
			checkForFunctionFeedback();
			
			
			// Select the values to be used for any States to which Attributes
			// of the Domain being simulated are bound.
			
			ValueHistory externalStateValues = null;
			try { externalStateValues = valueHistoryFactory.getValueHistory(); }
			catch (UndefinedValue uv) { throw new ModelContainsError(
				"External State values cannot be imported", uv); }
			
			
			// Create a persistent container for the configuration and results of the
			// simulation run.
	
			String simRunName = Integer.toString(++simRunNumber);
	
			simRun = modelScenario.createSimulationRun(
				simRunName, externalStateValues,
				modelScenario, this, callback, startingEpoch,
				finalEpoch, iterationLimit,
				duration, randomSeed);
				
			if (simRunSet != null) simRunSet.addSimRun(simRun);
				
			callback.setSimRunNodeId(simRun.getNodeId());
				// Setting this enables the SimCallback to inform clients of the
				// Node ID of the Sim Run.
		}
			
		// Notify clients that a SimulationRun Node has been created.

		try { notifyAllListeners(new PeerNoticeBase.NodeAddedNotice(modelScenario.getNodeId(),
			simRun.getNodeId())); }
		catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
		

		// Create a thread to run the sim run.
		
		ServiceThread ct = (ServiceThread)(Thread.currentThread());
		SimServiceThreadImpl t = new SimServiceThreadImpl(ct, simRun);
		t.setDaemon(true);
		
		
		// Start the thread, which runs the simulation.

		t.start();
		
		
		return t;
	}


	public SimServiceThread simulate(ValueHistoryFactory valueHistoryFactory,
		SimRunSet simRunSet,
		ModelScenario modelScenario,
		SimCallback callback, int iterationLimit, long randomSeed)
	throws
		ParameterError,
		ModelContainsError
	{
		return simulate(valueHistoryFactory, simRunSet, modelScenario, callback, 
			null, null, iterationLimit, 0, randomSeed);
	}
	
	
	public SortedSet<SimulationRun> getSimulationRuns()
	{
		SortedSet<SimulationRun> simRuns = new TreeSetNullDisallowed<SimulationRun>();
		
		List<Scenario> ss = getScenarios();
		for (Scenario s : ss) simRuns.addAll(((ModelScenario)s).getSimulationRuns());
		
		return simRuns;
	}
	
	
	public void deleteSimulationRuns()
	{
		List<Scenario> ss = getScenarios();
		for (Scenario s : ss) ((ModelScenario)s).deleteSimulationRuns();
	}
	
	
  /* *************************************************************************
	* From HierarchyDomain
	*/
	

	public HierarchyScenarioSet createHierarchyScenarioSet(String name, 
		HierarchyScenario baseScenario, HierarchyAttribute attr, 
		Serializable[] values)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		return (HierarchyScenarioSet)(createScenarioSet(name, baseScenario, attr, values));
	}


	public List<HierarchyScenario> getHierarchyScenarios()
	{
		return helper.getHierarchyScenarios();
	}
	
	
	public HierarchyScenario getHierarchyScenario(String name)
	throws
		ElementNotFound, // if the HierarchyScenario cannot be found.
		ParameterError
	{
		return helper.getHierarchyScenario(name);
	}
	
	
	public void addHierarchyScenario(HierarchyScenario scenario)
	{
		helper.addHierarchyScenario(scenario);
	}
	
	
	public HierarchyScenarioSet getHierarchyScenarioSet(String name)
	throws
		ElementNotFound,
		ParameterError
	{
		return helper.getHierarchyScenarioSet(name);
	}
		
		
	public List<HierarchyScenarioSet> getHierarchyScenarioSets()
	{
		return helper.getHierarchyScenarioSets();
	}
	
	
	public void addHierarchyScenarioSet(HierarchyScenarioSet s)
	{
		helper.addHierarchyScenarioSet(s);
	}
	
	
	public void deleteHierarchyScenarioSet(HierarchyScenarioSet s)
	throws
		ParameterError
	{
		helper.deleteHierarchyScenarioSet(s);
	}


	public HierarchyScenario getCurrentHierarchyScenario()
	{
		return helper.getCurrentHierarchyScenario();
	}
	
	
	public void setCurrentHierarchyScenario(HierarchyScenario scenario)
	throws
		ElementNotFound,
		ParameterError
	{
		helper.setCurrentHierarchyScenario(scenario);
	}
	
	
  /* *************************************************************************
   * From ModelContainer
   */
	
	
	public void deleteSubcomponent(ModelElement child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(child, outermostAffectedRef);
	}
	
	
  /* *************************************************************************
   * From PersistentNode
   */
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		super.renameChild(child, newName);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		ModelDomainImpl newDomain = (ModelDomainImpl)(helper.clone(cloneMap, cloneParent));
		newDomain.attributeStateBindings = PersistentNodeImpl.cloneNodeSet(attributeStateBindings, cloneMap, newDomain);
		newDomain.varAttrBindings = PersistentNodeImpl.cloneNodeSet(varAttrBindings, cloneMap, newDomain);
		newDomain.compsWithNativeImpls = PersistentNodeImpl.cloneNodeSet(compsWithNativeImpls, cloneMap, newDomain);
		return newDomain;
	}

	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		
		writer.println(indentString 
			+ (this instanceof MotifDef? 
				"<model_motif name=\"" : "<model_domain name=\"")
			
			+ (getDeclaredName() == null? this.getName() : getDeclaredName())
			+ "\" "
			+ (getSourceName() == null? "" : "source_name=\"" + getSourceName() + "\" ")
			+ (getSourceURL() == null? "" : "source_url=\"" + getSourceURL() + "\" ")
			+ (getDeclaredVersion() == null? "" : "version=\"" + getDeclaredVersion() + "\" ")
			
			+ (" external_state_binding_type=\"" +
				(externalStateBindingType.booleanValue() ? "average" : "final") + "\"")
			
			+ (" external_state_time_granularity=\"" +
					externalStateGranularity.toString() + "\"") + " "
			
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "width=\"" + this.getWidth() + "\" "
			+ "height=\"" + this.getHeight() + "\">");
			
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		
		// Write MenuItems, if any.
		
		if (this instanceof MenuOwner)
		{
			List<MenuItem> mis = ((MenuOwner)this).getMenuItems();
			if (mis != null)
			{
				for (MenuItem mi : mis) mi.writeAsXML(writer, indentation+1);
			}
		}
		
		
		// Write each child.
		
		Set<PersistentNode> children = this.getChildren();
		for (PersistentNode child : children)
		{
			writer.println();
			child.writeAsXML(writer, indentation+1);
		}
		
		writer.println();
		writer.println(indentString + 
			(this instanceof MotifDef? "</model_motif>" : "</model_domain>")
			);


		// Write Scenario Sets.
		
		writer.println();
		List<ScenarioSet> sss = getScenarioSets();
		for (ScenarioSet scenSet : sss)
		{
			writer.println();
			scenSet.writeAsXML(writer, indentation);
		}
		
		
		// Write Scenarios.
		
		writer.println();
		List<Scenario> ss = getScenarios();
		for (Scenario scenario : ss)
		{
			writer.println();
			scenario.writeAsXML(writer, indentation);
		}
	}
	
	
	public void prepareForDeletion()
	{
		helper.prepareForDeletion();
		
		externalStateGranularity = null;
		attributeStateBindings = null;
		varAttrBindings = null;
		compsWithNativeImpls = null;
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		return helper.getChild(name);
	}
	
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		return helper.getChildren();
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		helper.deleteChild(child, outermostAffectedRef);
	}

	
	public int getNoOfHeaderRows() { return 2; }
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	
  /* *************************************************************************
   * From PersistentNodeImpl
   */
	
	
	public NodeSer externalize(final NodeSer nodeSer) throws ParameterError
	{
		ModelDomainSer ser = (ModelDomainSer)(helper.externalize(nodeSer));
			//(ModelDomainSer)(helper.externalize(
			//new Closure() { public Object invoke(Object... args) throws Exception
			//{ return ModelDomainImpl.super.externalize(nodeSer); } }, nodeSer));
		
		ser.attributeStateBindingNodeIds = createNodeIdArray(attributeStateBindings);
		ser.varAttrBindingNodeIds = createNodeIdArray(varAttrBindings);
		ser.compsWithNativeImplNodeIds = createNodeIdArray(compsWithNativeImpls);
		ser.externalStateBindingType = externalStateBindingType;
		ser.externalStateGranularity = externalStateGranularity;
		return ser;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		this.setDomain(this);
		this.setViewClassName("expressway.gui.modeldomain.ModelScenarioViewPanel");
		setResizable(true);
		
		// Provide initial values that will make the new Domain visible.
		this.setWidth(500);
		this.setHeight(500);
		
		this.helper = new HierarchyDomainHelper(this, new SuperDomain(),
			getHierarchyHelper());
	}
	
	
  /* *************************************************************************
   * Internal only methods.
   */
	
	
	ScenarioSet createScenarioSet(String name, ModelScenario baseScenario,
		ModelAttribute attr)
	throws
		ParameterError,
		CloneNotSupportedException
	{
		return createScenarioSet(name, baseScenario, attr, null);
	}


	protected String createUniqueModelDomainName()
	{
		return createUniqueModelDomainName(null);
	}
	
	
	protected String createUniqueModelScenarioSetName(String name)
	{
		return createUniqueChildName(name);
	}
	
	
	protected String createUniqueModelDomainName(String baseName)
	{
		return helper.createUniqueDomainName(baseName);
	}
	
	
	public Set<VariableAttributeBinding> getVariableAttributeBindings()
	{
		return varAttrBindings;
	}


	/**
	 * Find all Conduits that can receive Events from the specified Set of Conduits,
	 * and add those to 'conduits'. Return a reference to 'conduits'.
	 * Recursive. See the slide "Propagation Algorithm".
	 */
	 
	protected Set<Conduit> determineReachableConduits(Set<Conduit> conduits)
	{
		Set<Conduit> conduitsCopy = new TreeSetNullDisallowed<Conduit>(conduits);
		
		for (Conduit c : conduitsCopy) try
		{
			Port[] ports = { c.getPort1(), c.getPort2() };
			for (Port p : ports)
			{
				Set<Conduit> connectedConduits = new TreeSetNullDisallowed<Conduit>();
				connectedConduits.addAll(p.getSourceConnections());
				connectedConduits.addAll(p.getDestinationConnections());
				
				connectedConduits.removeAll(conduits);
				
				for (Conduit connC : connectedConduits)
					if (canPass(c, connC, p)) conduits.add(connC);
			}
		}
		catch (ValueNotSet w) { continue; }  // to next Conduit.
		
		return conduits;
	}
	
	
	/**
	 * A signal can pass from Conduit Ca to Conduit Cb via Port P if:
		1. Both Conduits are external to P's Container and P is not �black�; or
		2. Both Conduits are internal to P's Container; or
		3. Ca is internal to P's Container (but Cb is not), and P is {out, bi}; or
		4. Cb is internal to P's Container (but Ca is not), and P is {in, bi}.
	 */
	 
	protected boolean canPass(Conduit cA, Conduit cB, Port p)
	{
		// Determine which Conduits are internal and which are external.
		
		ModelContainer pContainer = p.getContainer();
		ModelContainer pContainerContainer = pContainer.getModelContainer();
		ModelContainer caContainer = cA.getContainer();
		ModelContainer cbContainer = cB.getContainer();
		
		boolean caIsInternal = (caContainer == pContainer);
			// true if Ca is owned by P's Container.
			
		boolean caIsExternal = (caContainer == pContainerContainer);
			// true if Ca is owned by the Container that owns P's Container.
		
		
		boolean cbIsInternal = (cbContainer == pContainer);
			// true if Cb is owned by P's Container.
		
		boolean cbIsExternal = (cbContainer == pContainerContainer);
			// true if Cb is owned by the Container that owns P's Container.
		
		
		if (caIsInternal != (!caIsExternal)) throw new RuntimeException(
			"Conduit is neither internal nor external");
		
		if (cbIsInternal != (!cbIsExternal)) throw new RuntimeException(
			"Conduit is neither internal nor external");
		
		
		// Determine whether Port can conduct Events in or out.
		
		PortDirectionType pDir = p.getDirection();
		
		boolean pCanConductIn = 
			(pDir == PortDirectionType.input) || (pDir == PortDirectionType.bi);
			
		boolean pCanConductOut = 
			(pDir == PortDirectionType.output) || (pDir == PortDirectionType.bi);
			
			
		// Evaluate conditions.
		
		return
			(caIsExternal && cbIsExternal && (! p.getBlack()))
			||
			(caIsInternal && cbIsInternal)
			||
			(caIsInternal && (! cbIsInternal) && pCanConductOut)
			||
			(cbIsInternal && (! caIsInternal) && pCanConductIn)
			;
	}
	

	/**
	 * See slide "Detecting Function Feedback".
	 */
	 
	protected void checkForFunctionFeedback()
	throws
		ModelContainsError  // if feedback is detected.
	{
		Set<Function> functionsChecked = new TreeSetNullDisallowed<Function>();
		
		Set<Function> functions = this.getFunctions();
		
		for (Function f : functions) checkForFunctionFeedback(f, functionsChecked);
	}
	
	
	/**
	 * 
	 */
	 
	protected void checkForFunctionFeedback(Function f, Set<Function> functionsChecked)
	throws
		ModelContainsError  // if feedback is detected.
	{
		if (functionsChecked.contains(f)) return;
		
		f.checkForFeedback(functionsChecked);
		
		Set<Function> subFunctions = f.getFunctions();
		
		for (Function g : subFunctions)  // each nested Function g in f
		{
			checkForFunctionFeedback(g, functionsChecked);
		}
	}


  /* *************************************************************************
   * From AttributeChangeListener,
	AttributeStateBindingChangeListener, ModelComponentChangeListener,
	DecisionElement.VariableAttributeBindingChangeListener
   */
	
	
	public void attributeDeleted(Attribute attribute)
	{
		// Delete any AttributeStateBindings.
		
		Set<AttributeStateBinding> attributeStateBindingsCopy =
			new HashSet<AttributeStateBinding>(attributeStateBindings);
			
		for (AttributeStateBinding asBinding : attributeStateBindingsCopy)
		{
			try
			{
				if (asBinding.getAttribute() == attribute)
					attributeStateBindings.remove(asBinding);
			}
			catch (ValueNotSet w) {} // ok
		}
		
		
		// Remove any VariableAttributeBindings.
		
		Set<VariableAttributeBinding> varAttrBindingsCopy =
			new HashSet<VariableAttributeBinding>(varAttrBindings);
			
		for (VariableAttributeBinding vaBinding : varAttrBindingsCopy)
		{
			try
			{
				if (vaBinding.getAttribute() == attribute)
					varAttrBindings.remove(vaBinding);
			}
			catch (ValueNotSet w) {}  // ok
		}
	}
	
	
	public void attributeNameChanged(Attribute attr, String oldName)
	{
	}
	
	
	public void modelComponentDeleted(ModelComponent c)
	{
		compsWithNativeImpls.remove(c);
	}
	
	
	public void modelComponentNameChanged(ModelComponent comp, String oldName)
	{
	}
	
	
	static class SimServiceThreadImpl extends ServiceThreadImpl 
		implements SimControl, SimServiceThread
	{
		private SimulationRun simRun;
		
		
		SimServiceThreadImpl(ServiceThread thread, SimulationRun simRun)
		{
			super(thread, simRun /* is Runnable */);
			this.simRun = simRun;
		}
		
		
		public SimulationRun getSimulationRun() { return this.simRun; }
	}


	public void variableAttributeBindingDeleted(VariableAttributeBinding binding)
	{
		if (binding == null) return;
		this.varAttrBindings.remove(binding);
	}
	
	
	public void variableAttributeBindingNameChanged(VariableAttributeBinding vaBinding, String oldName)
	{
	}
	
	
	public void attributeStateBindingDeleted(AttributeStateBinding binding)
	{
		this.attributeStateBindings.remove(binding);
	}


	public void attributeStateBindingNameChanged(AttributeStateBinding asBinding, String oldName)
	{
	}
	
	
  /* Proxy classes needed by HierarchyDomainHelper */

	
	private class SuperDomain extends SuperMenuOwner
	{
		/* No need to list Domain methods because they do not exist in the superclass
			and hence their super method is never called. */
	}
	
	
	private class SuperMenuOwner extends SuperCrossReferenceable implements MenuOwner
	{
		public Action createAction(int position, String text, String desc, String javaMethodName)
			throws ParameterError
		{ return ModelDomainImpl.super.createAction(position, text, desc, javaMethodName); }
		
		public Action createAction(String text, String desc, String javaMethodName) throws ParameterError
		{ return ModelDomainImpl.super.createAction(text, desc, javaMethodName); }
		
		public MenuTree createMenuTree(int position, String text, String desc) throws ParameterError
		{ return ModelDomainImpl.super.createMenuTree(position, text, desc); }
		
		public MenuTree createMenuTree(String text, String desc) throws ParameterError
		{ return ModelDomainImpl.super.createMenuTree(text, desc); }
		
		public void deleteMenuItem(int menuItemPos) throws ParameterError
		{ ModelDomainImpl.super.deleteMenuItem(menuItemPos); }
		
		public void deleteMenuItem(MenuItem menuItem) throws ParameterError
		{ ModelDomainImpl.super.deleteMenuItem(menuItem); }
		
		public List<MenuItem> getMenuItems()
		{ return ModelDomainImpl.super.getMenuItems(); }
		
		public int getIndexOf(MenuItem mi)
		{ return ModelDomainImpl.super.getIndexOf(mi); }
	}
	
	
	private class SuperCrossReferenceable extends SuperPersistentNode implements CrossReferenceable
	{
		public Set<CrossReference> getCrossReferences()
		{ return ModelDomainImpl.super.getCrossReferences(); }
		
		
		public void addRef(CrossReference cr)
		throws
			ParameterError
		{ ModelDomainImpl.super.addRef(cr); }
		
		
		public Set<NamedReference> getNamedReferences()
		{ return ModelDomainImpl.super.getNamedReferences(); }
		
		
		public NamedReference getNamedReference(String refName)
		{ return ModelDomainImpl.super.getNamedReference(refName); }
		
		
		public Set<CrossReferenceable> getToNodes(NamedReference nr)
		{ return ModelDomainImpl.super.getToNodes(nr); }
		
		
		public Set<CrossReferenceable> getFromNodes(NamedReference nr)
		{ return ModelDomainImpl.super.getFromNodes(nr); }
		
		
		public Set<CrossReferenceable> getToNodes(String refName)
		throws
			ParameterError
		{ return ModelDomainImpl.super.getToNodes(refName); }
		
		
		public Set<CrossReferenceable> getFromNodes(String refName)
		throws
			ParameterError
		{ return ModelDomainImpl.super.getFromNodes(refName); }
		
		
		public void removeRef(CrossReference cr)
		throws
			ParameterError
		{ ModelDomainImpl.super.removeRef(cr); }
			
			
		public void addCrossReferenceCreationListener(CrossReferenceCreationListener listener)
		{ ModelDomainImpl.super.addCrossReferenceCreationListener(listener); }
		
		public void addCrossReferenceDeletionListener(CrossReferenceDeletionListener listener)
		{ ModelDomainImpl.super.addCrossReferenceDeletionListener(listener); }
		
		public List<CrossReferenceCreationListener> getCrossRefCreationListeners()
		{ return ModelDomainImpl.super.getCrossRefCreationListeners(); }
		
		public List<CrossReferenceDeletionListener> getCrossRefDeletionListeners()
		{ return ModelDomainImpl.super.getCrossRefDeletionListeners(); }
		
		public void removeCrossReferenceListener(CrossReferenceListener listener)
		{ ModelDomainImpl.super.removeCrossReferenceListener(listener); }
		
		public void signalCrossReferenceCreated(CrossReference cr)
		throws
			Exception
		{ ModelDomainImpl.super.signalCrossReferenceCreated(cr); }
		
		public void signalCrossReferenceDeleted(NamedReference nr,
			CrossReferenceable fromNode, CrossReferenceable toNode)
		throws
			Exception
		{ ModelDomainImpl.super.signalCrossReferenceDeleted(nr, fromNode, toNode); }
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return ModelDomainImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return ModelDomainImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return ModelDomainImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return ModelDomainImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelDomainImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelDomainImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ ModelDomainImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return ModelDomainImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return ModelDomainImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ return ModelDomainImpl.super.getSerClass(); }
		
		
		public Set<Attribute> getAttributes()
		{ return ModelDomainImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return ModelDomainImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return ModelDomainImpl.super.getAttributeSeqNo(attr); }
		

		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ ModelDomainImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelDomainImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelDomainImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ ModelDomainImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ ModelDomainImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return ModelDomainImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return ModelDomainImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return ModelDomainImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return ModelDomainImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return ModelDomainImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return ModelDomainImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return ModelDomainImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return ModelDomainImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return ModelDomainImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelDomainImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelDomainImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelDomainImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ ModelDomainImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelDomainImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return ModelDomainImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return ModelDomainImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return ModelDomainImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return ModelDomainImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ ModelDomainImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return ModelDomainImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ ModelDomainImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return ModelDomainImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return ModelDomainImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return ModelDomainImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ ModelDomainImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return ModelDomainImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ ModelDomainImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return ModelDomainImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return ModelDomainImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return ModelDomainImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return ModelDomainImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return ModelDomainImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return ModelDomainImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return ModelDomainImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return ModelDomainImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ ModelDomainImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return ModelDomainImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return ModelDomainImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return ModelDomainImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return ModelDomainImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return ModelDomainImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ ModelDomainImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return ModelDomainImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ ModelDomainImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return ModelDomainImpl.super.isVisible(); }
		
		
		public void layout()
		{ ModelDomainImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return ModelDomainImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ ModelDomainImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return ModelDomainImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ ModelDomainImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ ModelDomainImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return ModelDomainImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ ModelDomainImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return ModelDomainImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ ModelDomainImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return ModelDomainImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ ModelDomainImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return ModelDomainImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return ModelDomainImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return ModelDomainImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ ModelDomainImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ ModelDomainImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return ModelDomainImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ ModelDomainImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return ModelDomainImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ ModelDomainImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return ModelDomainImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return ModelDomainImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return ModelDomainImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return ModelDomainImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ ModelDomainImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return ModelDomainImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ throw new RuntimeException("Should not be called"); }
		
		public void setViewClassName(String name)
		{ ModelDomainImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return ModelDomainImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return ModelDomainImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return ModelDomainImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return ModelDomainImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return ModelDomainImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return ModelDomainImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return ModelDomainImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ ModelDomainImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return ModelDomainImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return ModelDomainImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return ModelDomainImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return ModelDomainImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return ModelDomainImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return ModelDomainImpl.super.getX(); }
		public double getY() { return ModelDomainImpl.super.getY(); }
		public double getScale() { return ModelDomainImpl.super.getScale(); }
		public double getOrientation() { return ModelDomainImpl.super.getOrientation(); }
		public void setX(double x) { ModelDomainImpl.super.setX(x); }
		public void setY(double y) { ModelDomainImpl.super.setY(y); }
		public void setLocation(double x, double y) { ModelDomainImpl.super.setLocation(x, y); }
		public void setScale(double scale) { ModelDomainImpl.super.setScale(scale); }
		public void setOrientation(double angle) { ModelDomainImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return ModelDomainImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return ModelDomainImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return ModelDomainImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return ModelDomainImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ ModelDomainImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return ModelDomainImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return ModelDomainImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return ModelDomainImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return ModelDomainImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return ModelDomainImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return ModelDomainImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ ModelDomainImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return ModelDomainImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return ModelDomainImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return ModelDomainImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelDomainImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ ModelDomainImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelDomainImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return ModelDomainImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return ModelDomainImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return ModelDomainImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return ModelDomainImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return ModelDomainImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ ModelDomainImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return ModelDomainImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ ModelDomainImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ ModelDomainImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ ModelDomainImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return ModelDomainImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return ModelDomainImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return ModelDomainImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ ModelDomainImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return ModelDomainImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ ModelDomainImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return ModelDomainImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ ModelDomainImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ ModelDomainImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ ModelDomainImpl.super.printChildrenRecursive(indentationLevel); }
	}
}
