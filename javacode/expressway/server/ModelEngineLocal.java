/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.DecisionElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.common.ClientModel.*;
import java.util.List;
import java.util.Vector;
import java.util.Date;
import java.util.Set;
import java.io.IOException;
import java.io.File;
import java.rmi.Remote;
import java.awt.Image;
import javax.swing.ImageIcon;


/**
 * Local (non-remote) interface to a Model Engine.
 *
 * These methods are assumed to be transactional, for implementations
 * that support multiple concurrent requests. However, it is assumed that these
 * methods are called from within a transaction, and that there is already
 * a ServiceContext associated with the current Thread.
 *
 * Structural changes (other than Attribute values) are disallowed
 * while a simulation is underway.
 */

public interface ModelEngineLocal extends ModelEngineRemote
{
	/** ************************************************************************
	 * 
	 */
	 
	void setHttpManager(HTTPManager m);
	
	
	HTTPManager getHttpManager();
	
	
	/** ************************************************************************
	 * 
	 */
	 
	ImageIcon createLocalImageIcon(byte[] bytes, String handle);
	
	
	/** ************************************************************************
	 * 
	 */
	 
	ImageIcon getLocalStoredImageIcon(String handle);
	
	
	/** ************************************************************************
	 * 
	 */
	 
	void removeLocalStoredImageIcon(String handle);
	
	
	void addMotifClassByteArray(byte[] bytes);


	/** ************************************************************************
	 * Control whether the simulator may start new simulations. Simulations
	 * that are currently running are not affected.
	 */
	 
	void allowNewSimulations(boolean allow);
	
	
	/** ************************************************************************
	 * Return true if the simulator is permitted to start new simulations.
	 */
	 
	boolean newSimulationsAreAllowed();
	
	
	/** ************************************************************************
	 * If there are simulations running, return true.
	 */
	 
	boolean simulationsAreRunning();
	
	
	String createUniqueDomainName(String base);
	

	String createUniqueDomainName();
	

	String createUniqueMotifName(String base);
	

	String createUniqueMotifName();
	

	Domain createDomain(Class domainClass, String name, boolean useNameExactly)
	throws
		ParameterError;
		
	
	Domain createDomain(Class domainClass)
	throws
		ParameterError;
		
	
	Domain createDomainNode(DomainType ofType);
	
	
	DomainType createDomainTypeNode(DomainType baseType, MotifDef... usesType);
	
	
	void deleteDomainTypeNode(DomainType typeToDelete)
	throws
		DisallowedOperation;

	
	Set<DomainType> getDomainTypeNodes();
	
	
	DomainType getDomainType(String name);
	
	
	DomainType getBaseDomainTypeForDomainClass(Class domainClass);
	
	
	/** ************************************************************************
	 * The directory in which motif JAR files are stored.
	 */
	 
	//File getMotifResourceDirectory();
	
	
	/** ************************************************************************
	 * Note that there may be more than one motif defined in a JAR file. Also,
	 * a motif is not necessarily defined in a JAR file: it might have been
	 * defined interactively rather than imported: thus, this method may
	 * return null.
	 *
	 
	File getMotifJarFile(String motifName);*/
	
	
	/** ************************************************************************
	 * Retrieve the ModelDomains owned by thie Model Engine.
	 */
	
	List<ModelDomain> getModelDomainPersistentNodes()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	List<MotifDef> getMotifDefPersistentNodes();
	
	
	/** ************************************************************************
	 * Retrieve the specified Domain.
	 */
	
	Domain getDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	/** ************************************************************************
	 * Retrieve the specified ModelDomain.
	 */
	
	ModelDomain getModelDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	MotifDef getMotifDefPersistentNode(String name)
	throws
		ElementNotFound,
		ModelContainsError;
	
	
	List<DecisionDomain> getDecisionDomainPersistentNodes()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	/** ************************************************************************
	 * Retrieve the specified DecisionDomain. If not found, return null.
	 */
	
	DecisionDomain getDecisionDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	/** ************************************************************************
	 * Create a new ModelDomain for this ModelEngine. If useNameExactly is not
	 * true, and if a Domain with the specified name already exists, then
	 * construct a unique name based on the name provided.
	 */

	ModelDomain createModelDomainPersistentNode(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException;
	
	
	ModelDomainMotifDef createModelDomainMotifDefPersistentNode(String name,
		boolean useNameExactly)
	throws
		ParameterError,
		IOException;


	ModelDomainMotifDef createModelDomainMotifDefPersistentNode()
	throws
		ParameterError,
		IOException;


	/** ************************************************************************
	 * Create a new DecisionDomain for this ModelEngine.
	 */

	DecisionDomain createDecisionDomainPersistentNode(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException;


	DecisionDomain createDecisionDomainMotifDefPersistentNode(String name,
		boolean useNameExactly)
	throws
		ParameterError,
		IOException;


	DecisionDomain createDecisionDomainMotifDefPersistentNode()
	throws
		ParameterError,
		IOException;


	List<HierarchyDomain> getHierarchyDomainPersistentNodes()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	HierarchyDomain getHierarchyDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	HierarchyDomain createHierarchyDomainPersistentNode(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException;


	HierarchyDomain createHierarchyDomainMotifDefPersistentNode(String name,
		boolean useNameExactly)
	throws
		ParameterError,
		IOException;


	HierarchyDomain createHierarchyDomainMotifDefPersistentNode()
	throws
		ParameterError,
		IOException;


	/** ************************************************************************
	 * Return the PersistentNode specified by the Node Id. If this method is
	 * called remotely, a Remote reference is returned. If it is called locally
	 * then a local reference is returned.
	 */
	 
	PersistentNode getPersistentNode(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
	
	
	/** ************************************************************************
	 * Return a Set of externalized versions of the children of the specified
	 * PersistentNode.
	 */
	
	Set<PersistentNode> getChildPersistentNodes(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
	/** ************************************************************************
	 * Locate the specified ModelElement in the database.
	 *
	 * If the argument 'elementName' is a simple alphanumeric string, then
	 * the element is assumed to exist at the top level within the database.
	 * On the other hand, if the name is qualified (with period separators)
	 * such as 'abc.def.ghi', then the first part of the name is assumed
	 * to be the top-level element (ModelDomain) within the database, and the sub-parts
	 * are nested component instances that can be found by traversing the
	 * namespace (children) of each element.
	 *
	 * If the fullElementPath is null, return the Domain.
	 */

	PersistentNode getNodeForPath(String fullElementPath)
	throws
		ElementNotFound,
		ParameterError;


	/** ************************************************************************
	 * Same as getNodeForPath, except that the Node must be within
	 * the specified Domain.
	 *
	 * If the elementName is null, return the Domain.
	 */

	PersistentNode getNodeForPath(String domainName, String elementName)
	throws
		ElementNotFound,
		ParameterError;


	/** ************************************************************************
	 * Locate the specified DecisionElement in the database.
	 * 
	 *
	 * If the elementName is null, return the DecisionDomain.
	 */

	DecisionElement getDecisionElementTree(String decisionDomainName, String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;


	/** ************************************************************************
	 * Same as RemoteModelEngine.getEventsByEpoch, but returns Persistent Nodes.
	 */
	 
	List<GeneratedEvent>[] getEventNodesByEpoch(String modelDomainName, 
		String modelScenarioName, String simRunName, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException;
		
		
	/** ************************************************************************
	 * Render the specified Node using the list format defined in the slide
	 * 'HTML List Format'. (See also the slide 'Expressway URLs'.)
	 */
	 
	byte[] renderHTMLList(String nodeId)
	throws
		ParameterError,
		IOException;
	
	
	/** ************************************************************************
	 * Produce an SVG rendering of the specified Node, within the size specified
	 * by wCmMax, hCmMax (in cm). If wCmMax or hCmMax is zero, use a default size.
	 * The image is never stretched: the wCmMax and hCmMax are maximums.
	 */
	 
	byte[] renderAsSVG(String nodeId, double wCmMax, double hCmMax)
	throws
		ParameterError,
		IOException;
		

	byte[] renderAsHTML(String nodeId)
	throws
		ParameterError,
		IOException;
		

	/** ************************************************************************
	 * Register the specified PeerListener with this ModelEngine. The ModelEngine
	 * will notify the PeerListener when PersistentNodes change.
	 */
	
	ListenerRegistrar registerPeerListener(String clientId, PeerListener listener)
	throws
		ParameterError,
		IOException;
		
	
	/** ************************************************************************
	 * Unregister the specified Peer Listener from this Model Engine.
	 */
	 
	void unregisterPeerListener(PeerListener listener)
	throws
		ParameterError,
		IOException;
		
		
	/** ************************************************************************
	 * Return a Set of the Listener Registrars owned by this Model Engine.
	 */
	 
	Set<ListenerRegistrarImpl> getListenerRegistrars();
	
	
	/** ************************************************************************
	 * Return this Model Engine's CallbackManager instance.
	 */
	 
	CallbackManager getCallbackManager();
	
	
	/** ************************************************************************
	 * Factory to create and return a JarProcessor for processing a motif JAR file.
	 */
	 
	JarProcessor getJarProcessor(boolean replace, PeerListener peerListener,
		String clientId,
		byte[] byteAr);


	/** ************************************************************************
	 * Diagnostic methods.
	 */
	 
	void dump()
	throws
		IOException;
		
		
	/** ************************************************************************
	 * Diagnostic method.
	 */
	 
	void dump(int indentation)
	throws
		IOException;
	
	
	/** ************************************************************************
	 * Process an Expressway XML file. Processing an XML files results in database
	 * changes.
	 */
	 
	interface XMLProcessor
	{
		/** Perform processing. This results in database changes. Returns a non-zero
			result if not successful. */
		int processXML();
		
		/** Return false if an Exception should be thrown if a new Domain has the
			same name as an existing name. */
		boolean getReplace();
		
		PeerListener getPeerListener();
		String getClientId();
		char[] getCharAr();
		List<PersistentNode> getRootNodesCreated();
	}
	
	
	/** ************************************************************************
	 * Process a Jar file containing Expressway motif classes, resources, and
	 * XML files.
	 */
	 
	interface JarProcessor
	{
		/** Perform processing. This results in database, class, and resource changes.
			Returns a non-zero result if not successful.*/
		int processJar();
		
		/** Same as processJar(), but allows called to specify if XML files are
			alloed in the JAR file. */
		int processJar(boolean allowXML);
		
		/*
		/**
		 * Load the specified Jar file into the Jar ClassLoader. The Jar, in effect,
		 * becomes part of the server's and client's classpath. Provide a handler
		 * for each type of Jar entry that should be handled, e.g., an Expressway
		 * XML file handler, a Java class handler, etc.
		 *
		 
		void loadJar(File jarFile, JarEntryRecognizer... recognizers)
		throws
			Exception;


		interface JarEntryRecognizer
		{
			/** Attempt to recognize the type of Java entry. Return false if
				not recognized. If an entry is recognized, prepare this JarProcessor
				to process it. This recognizer itself should not make any database
				changes. The reason is because some entries may require other
				entries to have been read before they can be processed. *
			boolean recognizerEntry(JarEntry entry)
			throws
				Warning,  // if the entry should be processed, but the user should
					// be notified of an issue.
				Exception;  // if there is an error and the entry should not be
					// processed.
		}
		*/
	}
}
