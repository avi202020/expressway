/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import expressway.server.DecisionElement.*;
import expressway.server.NamedReference.CrossReference;
import expressway.server.NamedReference.CrossReferenceable;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.common.ClientModel.*;
import statistics.HistogramDomainParameters;
import statistics.TimeSeriesParameters;
import awt.AWTTools;
import generalpurpose.ThrowableUtil;
import generalpurpose.DateAndTimeUtils;
import generalpurpose.SVGUtils;
import generalpurpose.TreeSetNullDisallowed;
import java.io.File;
import java.io.Serializable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.FileReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.StringWriter;  // for test only
import java.io.PrintWriter;  // for test only
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.*;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.commons.math.stat.descriptive.SummaryStatisticsImpl;
import org.apache.commons.math.stat.regression.SimpleRegression;


/** ****************************************************************************
 * Implements the ModelEngine interface using a Java object without any
 * persistence. Methods are synchronized to provide guarded re-entrancy.
 *
 * This implementation requires the user to identify a 'current' Decision
 * Domain, and that is used as the Decision Domain for all requests until
 * the user specifies another Decision Domain.
 * 
 * The option -load=<file_path> may be specified. This causes the Model Engine
 * to load the specified model definition file at startup. This is useful for
 * testing, since this implementation does not persist models.
 */

public class ModelEngineLocalPojoImpl
	implements ModelEngineLocal, java.io.Serializable
{
	private static String StandardBaseDomainName = "New Domain";

	private static String StandardBaseMotifName = "New Motif";
	

	long priorUniqueId;
	
	private int storedImageNumber = 0;
	
	VisualGuidance defaultVisualGuidance;
	
	private RandomGenerator random;
	
	/** May be null */
	private HTTPManager httpManager;
	
	private File motifResourceDir;
	
	private List<byte[]> motifClassByteArrays = new Vector<byte[]>();
	
	/** Nullify this when persisting server data. */
	private Map<String, byte[]> imageData = new HashMap<String, byte[]>();
	
	/** Map of node ID to node reference. */
	Map<String, PersistentNode> allNodes;
	
	/** All Domains. */
	private Map<String, Domain> domains = new HashMap<String, Domain>();
	
	/** See the slide 'Object Model for Domain Types'. */
	private Map<String, DomainType> domainTypes = null;
	
	private DomainTypeSer[] baseDomainTypeSers = null;
	
	/** Build this from "imageData" when loading database. Ownerhsip. */
	private transient Map<String, ImageIcon> imageIconCache = new HashMap<String, ImageIcon>();
	
	/** Directory into which the database should be written when flushed to disk. */
	private transient File databasedir;  // may be null.
	
	/** Directory into which the SimulationRuns should be written when flushed to disk. */
	private transient File simrundir;  // may be null.
	
	private transient boolean allowSimulations = true;
		
	private transient boolean thereAreUnflushedChanges = false;
		
	private transient CallbackManager callbackManager;
	
	private transient Map<Integer, ServiceThread> threads;
	
	private transient Set<ListenerRegistrarImpl> listenerRegistrars;
	

	
	
	/** ************************************************************************
	 * Constructor.
	 */

	public ModelEngineLocalPojoImpl(File dir)
	throws
		RemoteException
	{
		if (dir == null) throw new RemoteException("No database directory specified");
		this.databasedir = dir;
		this.simrundir = new File(dir, "simrundir");
		RandomGenerator.reset();
		
		reinitialize();
	}
	
	
	/** ************************************************************************
	 * This constructor is primarily for debugging. Setting the random number
	 * seed provides repeatability.
	 */
	 
	public ModelEngineLocalPojoImpl(File dir, long randomSeed)
	throws
		RemoteException
	{
		if (dir == null) throw new RemoteException("No database directory specified");
		this.databasedir = dir;
		this.simrundir = new File(dir, "simrundir");
		RandomGenerator.setRandomSeed(randomSeed);
		
		reinitialize();
	}
	
	
	/**
	 * Initialize any transient state. This is called during construction, and 
	 * also after reconstitution from a serialized state.
	 */
	 
	synchronized void reinitialize()
	{
		threads = new HashMap<Integer, ServiceThread>();
		listenerRegistrars = new HashSet<ListenerRegistrarImpl>();
		callbackManager = new CallbackManager(this);
		
		// *********************************************************************
		// Establish a ModelEngine service context for this thread, so that it can
		// make calls to the ModelEngine.
		// TBD: Handle situation when database is being RE-loaded. In that case,
		// there is already a MotifClassLoader in the chain.

		ServiceContext.create(this, ModelAPITypes.LocalServerClientId);
		
		MotifClassLoader motifClassLoader = new MotifClassLoader();
		ServiceContext.setMotifClassLoader(motifClassLoader);
		
		// Load Domain Types.
		
		try
		{
			DomainType hierarchyDomainType;
			DomainType modelDomainType;
			DomainType decisionDomainType;
		
			hierarchyDomainType = new DomainType("Hierarchy", HierarchyDomainImpl.class);
			modelDomainType = new DomainType("Model", ModelDomainImpl.class);
			decisionDomainType = new DomainType("Decision", DecisionDomainImpl.class);
			
			domainTypes = new HashMap<String, DomainType>();
			domainTypes.put("Hierarchy", hierarchyDomainType);
			domainTypes.put("Model", modelDomainType);
			domainTypes.put("Decision", decisionDomainType);
			
			baseDomainTypeSers = new DomainTypeSer[]
			{
				(DomainTypeSer)(hierarchyDomainType.externalize()),
				(DomainTypeSer)(modelDomainType.externalize()),
				(DomainTypeSer)(decisionDomainType.externalize())
			};
		}
		catch (ParameterError pe) { throw new RuntimeException(pe); }
			
		// Load image icon cache.
		
		this.imageIconCache = new HashMap<String, ImageIcon>();
		Set<String> imageDataKeys = imageData.keySet();
		for (String key : imageDataKeys)
		{
			this.imageIconCache.put(key, new ImageIcon(imageData.get(key), key));
		}
		
		GlobalConsole.println("Simulation run directory set to " + this.simrundir);
	}
	
	
	synchronized void setRandomSeed(long seed)
	{
		RandomGenerator.setRandomSeed(seed);
	}
	
	
	public synchronized VisualGuidance getDefaultVisualGuidance() { return defaultVisualGuidance; }
	
	
	public synchronized void setDefaultVisualGuidance(VisualGuidance vg)
	{
		this.defaultVisualGuidance = vg;
	}
	
	
	synchronized void setDatabaseDirectory(File dbDirFile)
	{
		this.databasedir = dbDirFile;
	}
	
	
	synchronized File getDatabaseDirectory() { return databasedir; }
	
	
	synchronized void setSimulationRunDirectory(File simrundirFile)
	{
		this.simrundir = simrundirFile;
	}
	
	
	synchronized File getSimulationRunDirectory() { return simrundir; }
	
	
	synchronized static File findDatabaseFile(File databasedir)
	throws
		ParameterError  // if database directory or database file not found.
	{
		if (databasedir == null) throw new ParameterError(
			"Database directory is null");
		
		if (! databasedir.exists()) throw new ParameterError(
			"Database directory " + databasedir + " not found.");
		
		if (! databasedir.isDirectory()) throw new ParameterError(
			"Path " + databasedir + " is not a directory");
		
		File dbfile = new File(databasedir, "expresswaydb.ser");
		
		if (! dbfile.exists()) throw new ParameterError(
			"Database file " + dbfile + " not found.");
		
		return dbfile;
	}


	synchronized static ModelEngineLocalPojoImpl loadDatabase(File dbfile, File simrundir)
	throws
		ParameterError,  // if database directory or database file not found.
		IOException
	{
		File databasedir = dbfile.getParentFile();
		
		
		// Reconstitute the ModelEngineLocal instance.
		
		FileInputStream fis = new FileInputStream(dbfile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		ModelEngineLocalPojoImpl modelEngineLocal = null;
		try { modelEngineLocal = (ModelEngineLocalPojoImpl)(ois.readObject()); }
		catch (ClassNotFoundException cnfe) { throw new IOException(cnfe); }
		
		
		// Restore variables that are not persisted.
		
		modelEngineLocal.reinitialize();
		modelEngineLocal.allowSimulations = true;
		modelEngineLocal.thereAreUnflushedChanges = false;
		modelEngineLocal.databasedir = databasedir;
		modelEngineLocal.simrundir = simrundir;
		
		PersistentNodeImpl.setPriorUniqueNodeId(modelEngineLocal.priorUniqueId);
		PersistentNodeImpl.setAllNodes(modelEngineLocal.allNodes);
		
		DefaultVisualGuidance.guidance = modelEngineLocal.defaultVisualGuidance;
		
		RandomGenerator.setRandom(modelEngineLocal.random);
		

		// Load motif classes and resources.
		
		modelEngineLocal.defineMotifClasses();
		modelEngineLocal.defineMotifResources();
		
		
		// Reconstitute SimulationRuns.
		
		if (simrundir.exists())
		{
			File[] simRunFiles = simrundir.listFiles(new FilenameFilter()
			{
				public boolean accept(File dir, String name)
				{
					if (name.endsWith(".simrun")) return true;
					return false;
				}
			});
			
			for (File simRunFile : simRunFiles)
			{
				// Check that enough memory remains.
				if (Runtime.getRuntime().freeMemory() < 30000000) throw new IOException(
					"Server memory is running too low; try allocating more Java memory");
				
				BufferedReader br = new BufferedReader(new FileReader(simRunFile));
				try
				{
					EventArchiver.ArchiveReader archiveReader =
						EventArchiver.getArchiveReader(br, modelEngineLocal);
					
					SimulationRun simRun = archiveReader.readSimRun();
				}
				catch (Exception ex)
				{
					GlobalConsole.println("For SimRun file " + simRunFile.getName()
						+ ": " + ex.getMessage());
					GlobalConsole.printStackTrace(ex);
				}
				finally
				{
					try { br.close(); }
					catch (IOException ex2) { GlobalConsole.printStackTrace(ex2); }
				}
			}
		}
		
		return modelEngineLocal;
	}


	
	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Methods from ModelEngineRemote.
	 *
	 */


	public synchronized String ping(String clientId)
	throws
		IOException
	{
		return (new Date()).toString();
	}
	
	
	public synchronized boolean hasUnflushedChanges()
	{
		return thereAreUnflushedChanges;
	}
	
	
	public synchronized void flush(String clientId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if (this.databasedir == null) throw new ParameterError(
				"No file path was set by -dbdir option when the database was created.");
			
			File dbfile = new File(this.databasedir, "expresswaydb.ser");
			
			FileOutputStream fos = new FileOutputStream(dbfile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			
			// Copy static values to the ModelEngineLocal instance.
			
			this.allowSimulations = true;
			this.thereAreUnflushedChanges = false;
			this.random = RandomGenerator.getRandomGenerator();
			this.priorUniqueId = PersistentNodeImpl.getPriorUniqueNodeId();
			this.allNodes = PersistentNodeImpl.getAllNodes();
			this.defaultVisualGuidance = DefaultVisualGuidance.guidance;
			

			// Write the ModelEngine object state to a file. NOte that the Events
			// of Simulation Runs are not serialized by this call because they are
			// declared to be transient.
			
			oos.writeObject(this);
			
			
			// Write unsaved simulations (including their Events) to files in
			// directory simrundir.
			
			File simDir = this.simrundir;
			if (! simDir.exists())
				if (! simDir.mkdir()) throw new IOException(
					"Could not create directory " + simDir);
			
			List<ModelDomain> mds = getModelDomainPersistentNodes();
			
			for (ModelDomain domain : mds) try
			{
				List<ModelScenario> scenarios = domain.getModelScenarios();
				
				for (ModelScenario scenario : scenarios)
				{
					List<SimulationRun> simRuns = 
						new Vector<SimulationRun>(scenario.getSimulationRuns());
					
					for (SimulationRun simRun : simRuns)
					{
						String fileName = domain.getName() + "." + scenario.getName() +
							"." + simRun.getName() + ".simrun";
							
						File file = new File(simDir, fileName);
	
						PrintWriter pw = new PrintWriter(new FileWriter(file));
						
						EventArchiver.ArchiveWriter archiveWriter = 
							EventArchiver.getArchiveWriter(pw);
						
						try { archiveWriter.writeSimRun(simRun); }
						catch (IOException ex)
						{
							GlobalConsole.printStackTrace(ex);
							GlobalConsole.println(
								"While writing Simulation Run: " +
									ThrowableUtil.getAllMessages(ex));
							
							throw ex;
						}
						finally { pw.close(); }
					}
				}
			}
			catch (Exception ex)
			{
				GlobalConsole.println("While syncing database: " +
					ThrowableUtil.getAllMessages(ex));
				
				GlobalConsole.printStackTrace(ex);
			}
			
			thereAreUnflushedChanges = false;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized Integer parseXMLCharAr(String clientId, final char[] charAr, 
		PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			// Execute the parser asychronously so that it does not tie up
			// the ModelEngine interface.
			
			Integer threadHandle = createMonitorThreadHandle();
			XMLMonitorThread thread = 
				new XMLMonitorThread(threadHandle, peerListener, clientId, charAr);
			
			thereAreUnflushedChanges = true;
			
			threads.put(threadHandle, thread);
			
			thread.setDaemon(true);
			thread.start();
			
			return threadHandle;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized Integer processJARByteAr(String clientId, byte[] byteAr,
		PeerListener peerListener)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		BaseException,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			

			// Process asychronously so that it does not tie up
			// the ModelEngine interface.
			
			Integer threadHandle = createMonitorThreadHandle();
			JarMonitorThread thread = 
				new JarMonitorThread(threadHandle, peerListener, clientId, byteAr);
			
			thereAreUnflushedChanges = true;
			
			threads.put(threadHandle, thread);
			
			thread.setDaemon(true);
			thread.start();
			
			return threadHandle;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainSer getModelDomain(String clientId, String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Domain domain = domains.get(name);
			if (domain == null) throw new ElementNotFound("Domain " + name);
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + name + "' is not a Model Domain");
			
			String domainId = domain.getNodeId();
			return (ModelDomainSer)(domain.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized DecisionDomainSer getDecisionDomain(String clientId, String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Domain domain = domains.get(name);
			if (domain == null) throw new ElementNotFound("Domain " + name);
			if (! (domain instanceof DecisionDomain)) throw new ParameterError(
				"Domain '" + name + "' is not a Decision Domain");
			
			return (DecisionDomainSer)(domain.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<String> getDomainNames(String clientId)
	throws
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Set<String> domainNames = domains.keySet();
			List<String> domainNameList = new Vector<String>(domains.keySet());
			return domainNameList;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized List<NodeDomainSer> getDomains(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Vector<NodeSer> domainSers = new Vector<NodeSer>();
			Collection<Domain> coll = domains.values();
			for (Domain d : coll) domainSers.add(d.externalize());
			return (List)domainSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<ModelDomainSer> getModelDomains(String clientId)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Vector<NodeSer> domainSers = new Vector<NodeSer>();
			List<ModelDomain> mdColl = getModelDomainPersistentNodes();
			for (ModelDomain md : mdColl) domainSers.add(md.externalize());
			return (List)domainSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
		
	public synchronized List<ModelDomainMotifDefSer> getModelDomainMotifs(String clientId)
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			List<NodeSer> motifSers = new Vector<NodeSer>();
			List<ModelDomain> mdList = getModelDomainPersistentNodes();
			for (ModelDomain md : mdList)
				if (md instanceof MotifDef) motifSers.add(md.externalize());
			
			return (List)motifSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<MotifDefSer> getMotifsUsedByDomain(String clientId,
		String domainId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ParameterError("Cannot identify Node with Id " + domainId);
			if (! (node instanceof Domain)) throw new ParameterError(
				"Node with Id " + domainId + " is not a Domain");
			
			Domain md = (Domain)node;
			
			Set<MotifDef> motifDefs = md.getMotifDefs();
			List<MotifDefSer> motifDefSers = new Vector<MotifDefSer>();
			for (MotifDef motifDef : motifDefs)
			{
				motifDefSers.add((MotifDefSer)(motifDef.externalize()));
			}
			
			return motifDefSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized boolean domainUsesMotif(String clientId, String domainId, String motifId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ParameterError("Cannot identify Node with Id " + domainId);
			if (! (node instanceof Domain)) throw new ParameterError(
				"Node with Id " + domainId + " is not a Domain");
			
			Domain md = (Domain)node;
			
			node = PersistentNodeImpl.getNode(motifId);
			if (node == null) throw new ParameterError("Cannot identify Node with Id " + motifId);
			if (! (node instanceof MotifDef)) throw new ParameterError(
				"Node with Id " + motifId + " is not a MotifDef");
			
			MotifDef motifDef = (MotifDef)node;
			
			return md.usesMotif(motifDef);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized void useMotif(String clientId, String domainId, String motifId, boolean yes)
	throws
		ParameterError,
		Warning,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ParameterError("Cannot identify Node with Id " + domainId);
			if (! (node instanceof Domain)) throw new ParameterError(
				"Node with Id " + domainId + " is not a Domain");
			
			Domain md = (Domain)node;
			
			node = PersistentNodeImpl.getNode(motifId);
			if (node == null) throw new ParameterError("Cannot identify Node with Id " + motifId);
			if (! (node instanceof MotifDef)) throw new ParameterError(
				"Node with Id " + motifId + " is not a MotifDef");
			
			MotifDef motifDef = (MotifDef)node;
			
			if (yes) md.addMotifDef(motifDef);
			else
			{
				if (md.actuallyUsesMotif(motifDef)) throw new ParameterError(
					"Cannot remove motif unless you first remove references to it.");
				
				md.removeMotifDef(motifDef);
			}
			
			
			// Notify clients that Views of the Domain should now include the Motif.
			
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.MotifUseChangedNotice(domainId, motifDef.getNodeId(), yes)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized byte[] getMotifResource(String clientId, String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			byte[] bytes = 
				ServiceContext.getMotifClassLoader().getMotifResource(pathWithinMotifJar);
			if (bytes != null) return bytes;
			
			throw new ResourceNotFound(pathWithinMotifJar);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public byte[] getMotifClass(String clientId, String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			byte[] bytes = 
				ServiceContext.getMotifClassLoader().getMotifClassBytes(pathWithinMotifJar);
			if (bytes != null) return bytes;
			
			throw new ResourceNotFound(pathWithinMotifJar);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized List<DecisionDomainSer> getDecisionDomains(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Vector<DecisionDomainSer> ddSers = new Vector<DecisionDomainSer>();
			Collection<Domain> coll = domains.values();
			for (Domain d : coll)
				if (d instanceof DecisionDomain) ddSers.add((DecisionDomainSer)(d.externalize()));
			return ddSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String exportNodeAsXML(String clientId, String nodeId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError("Node not found");
			
			XMLWriter writer = new XMLWriter(this, clientId);
			
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			
			writer.writeNode(printWriter, node);
			
			printWriter.flush();
			
			return stringWriter.toString();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized DomainTypeSer[] getDomainTypes(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			DomainTypeSer[] dtSers = new DomainTypeSer[domainTypes.size()];
			int i = 0;
			Collection<DomainType> dts = domainTypes.values();
			for (DomainType dt : dts) dtSers[i++] = (DomainTypeSer)(dt.externalize());
			return dtSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		
	
	public synchronized void createDomainForDomainType(String clientId, String domainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(domainTypeId);
			if (node == null) throw new ElementNotFound("Node not found");
			if (! (node instanceof DomainType)) throw new ParameterError(
				"Node with Id " + domainTypeId + " is not a DomainType");
			DomainType domainType = (DomainType)node;
			
			domainType.createDomain();
			thereAreUnflushedChanges = true;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized DomainTypeSer[] getBaseDomainTypes(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			return baseDomainTypeSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		
	
	public synchronized void createDomainType(String clientId, String baseDomainTypeId,
		String... motifDefIds)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(baseDomainTypeId);
			if (node == null) throw new ElementNotFound("Node with Id " + baseDomainTypeId +
				" not found");
			if (! (node instanceof DomainType)) throw new ParameterError(
				"Node with Id " + baseDomainTypeId + " is not a DomainType");
			DomainType baseDomainType = (DomainType)node;
			
			Class baseDomainClass = baseDomainType.getDomainClass();
			Set<MotifDef> baseDefaultMotifDefs = baseDomainType.getDefaultMotifs();
			Set<MotifDef> motifDefs = new TreeSetNullDisallowed<MotifDef>();
			if (baseDefaultMotifDefs != null) motifDefs.addAll(baseDefaultMotifDefs);
			for (String motifDefId : motifDefIds)
			{
				PersistentNode n = PersistentNodeImpl.getNode(motifDefId);
				if (n == null) throw new ElementNotFound("Node with Id " + motifDefId + " not found");
				if (! (n instanceof MotifDef)) throw new ParameterError(
					"Node with Id " + motifDefId + " is not a MotifDef");
				MotifDef motifDef = (MotifDef)n;
				motifDefs.add(motifDef);
			}

			DomainType dt = new DomainType(false, baseDomainClass,
				motifDefs.toArray(new MotifDef[motifDefs.size()]));
			domainTypes.put(dt.getName(), dt);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		
	
	public synchronized void deleteDomainType(String clientId, String domainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainTypeId);
			if (node == null) throw new ElementNotFound("Node with Id " + domainTypeId +
				" not found");
			if (! (node instanceof DomainType)) throw new ParameterError(
				"Node with Id " + domainTypeId + " is not a DomainType");
			DomainType domainType = (DomainType)node;
			
			if (domainType.isBuiltin()) throw new ParameterError(
				"DomainType '" + domainType.getName() + "' is built-in and cannot be deleted.");
			
			domainType.prepareForDeletion();
			DomainType dt = domainTypes.remove(domainType.getName());
			if (dt != domainType) throw new RuntimeException(
				"Internal Error: Domain Type '" + domainType.getName() +
				"' was not found in the server's list of Domain Types, but another " +
				"was of the same name");
			
			// Notify.
			
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.NodeDeletedNotice(domainType.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized MotifDefSer[] getMotifsForBaseDomainType(String clientId,
		String baseDomainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			return getMotifsForBaseDomainType_internal(baseDomainTypeId);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	protected MotifDefSer[] getMotifsForBaseDomainType_internal(String baseDomainTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		PersistentNode node = PersistentNodeImpl.getNode(baseDomainTypeId);
		if (node == null) throw new ElementNotFound("DomainType with Id " + baseDomainTypeId);
		if (! (node instanceof DomainType)) throw new ParameterError(
			"Node with Id " + baseDomainTypeId + " is not a DomainType");
		DomainType baseDomainType = (DomainType)node;

		Set<MotifDefSer> motifDefSers = new HashSet<MotifDefSer>();
		Collection<Domain> domaincol = domains.values();
		for (Domain domain : domaincol)
		{
			if (domain instanceof MotifDef)
			{
				MotifDef motifDef = (MotifDef)domain;
				if (baseDomainType.getDomainClass().isAssignableFrom(motifDef.getClass()))
					motifDefSers.add((MotifDefSer)(motifDef.externalize()));
			}
		}
		
		return motifDefSers.toArray(new MotifDefSer[motifDefSers.size()]);
	}
		
	
	public synchronized MotifDefSer[] getMotifsCompatibleWithDomainId(String clientId,
		String domainId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ElementNotFound("Domain with Id " + domainId);
			if (! (node instanceof Domain)) throw new ParameterError(
				"Node with Id " + domainId + " is not a Domain");
			Domain domain = (Domain)node;
			
			DomainType domainType = getBaseDomainTypeForDomainClass(domain.getClass());
			return getMotifsForBaseDomainType_internal(domainType.getNodeId());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized DomainTypeSer[] getUserDefinedDomainTypes(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Set<DomainTypeSer> userDefinedDomainTypes = new HashSet<DomainTypeSer>();
			Collection<DomainType> dts = domainTypes.values();
			for (DomainType domainType : dts)
			{
				if (domainType.isBuiltin()) continue;
				userDefinedDomainTypes.add((DomainTypeSer)(domainType.externalize()));
			}
			
			return userDefinedDomainTypes.toArray(new DomainTypeSer[userDefinedDomainTypes.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}	
	
	
	public synchronized MotifDefSer createMotifForDomainType(String clientId,
		String domainBaseTypeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainBaseTypeId);
			if (node == null) throw new ElementNotFound(
				"Node with Id " + domainBaseTypeId + " not found");
			if (! (node instanceof DomainType)) throw new ParameterError(
				"Node with Id " + domainBaseTypeId + " is not a DomainType");
			DomainType domainBaseType = (DomainType)node;
			MotifDef motifDef = domainBaseType.createMotifDef();
			thereAreUnflushedChanges = true;
			return (MotifDefSer)(motifDef.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized String getViewType(String clientId, String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError("Node not found");
			
			return node.getDomain().getDefaultDomainViewTypeName();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}		


	public synchronized String getViewClassName(String clientId, String nodeId)
	throws
		ElementNotFound,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError("Node not found");
			Domain domain = node.getDomain();
			return domain.getViewClassName();  // may return null.
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		
		
	public synchronized ModelDomainSer createModelDomain(String clientId, String name, 
		boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelDomainSer domainSer =
				(ModelDomainSer)(createModelDomain_internal(name, useNameExactly)
					.externalize());
			
			thereAreUnflushedChanges = true;
			return domainSer;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainSer createModelDomain(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ModelDomainSer domainSer =
				(ModelDomainSer)(createModelDomain_internal().externalize());
			
			thereAreUnflushedChanges = true;
			return domainSer;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainMotifDefSer createModelDomainMotif(String clientId, 
		String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			ModelDomainMotifDefImpl md;
			
			if (useNameExactly) synchronized (domains)
			{
				if (domains.get(name) != null) throw new ParameterError(
					"Name '" + name + "' is already in use.");
				
				md = new ModelDomainMotifDefImpl(name);
				domains.put(name, md);
			}
			else synchronized (domains)
			{
				String uniqueName = createUniqueMotifName(name);
				md = new ModelDomainMotifDefImpl(uniqueName);
				domains.put(md.getName(), md);
			}
			
			
			// Notify.
			
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.MotifCreatedNotice(md.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
			
			return (ModelDomainMotifDefSer)(md.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		

	public synchronized ModelDomainMotifDefSer createModelDomainMotif(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			MotifDef md;
			synchronized (domains)
			{
				String uniqueName = createUniqueMotifName(StandardBaseMotifName);
				md = createModelDomainMotifDefPersistentNode(uniqueName, false);
			}
			
			return (ModelDomainMotifDefSer)(md.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		

	public synchronized ModelDomainSer createIndependentCopy(String clientId, 
		String modelDomainId)
	throws
		ParameterError,
		ModelContainsError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			try
			{
				PersistentNode node = PersistentNodeImpl.getNode(modelDomainId);
				if (node == null) throw new ParameterError("Domain not found");
				
				if (! (node instanceof ModelDomain)) throw new ParameterError(
					"The Node " + modelDomainId + " is not a Model Domain");
				
				ModelDomain domain = (ModelDomain)node;
				
				ModelDomain domainCopy = domain.createIndependentCopy();
				
				
				// Add it to this ModelEngine's Map of Domains.
				
				domains.put(domainCopy.getName(), domainCopy);
				
				
				// Notify clients.
				
				try { callbackManager.notifyAllListeners(
					new PeerNoticeBase.DomainCreatedNotice(domainCopy.getNodeId())); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}			
				
				ModelDomainSer domainSer = (ModelDomainSer)(domainCopy.externalize());
				
				thereAreUnflushedChanges = true;
				return domainSer;
			}
			catch (CloneNotSupportedException ex)
			{
				throw new ModelContainsError(ex);
			}
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized DecisionDomainSer createDecisionDomain(String clientId, 
		String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			DecisionDomainSer domainSer =
				(DecisionDomainSer)(createDecisionDomain_internal(name, useNameExactly)
					.externalize());
			
			thereAreUnflushedChanges = true;
			return domainSer;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ScenarioSer createScenario(String clientId, String domainName, 
		String scenarioName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ScenarioSer scenarioSer =
				(ScenarioSer)(createScenario_internal(domainName, scenarioName).externalize());
					
			thereAreUnflushedChanges = true;
			return scenarioSer;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized ScenarioSer createScenario(String clientId, String domainName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ScenarioSer scenarioSer =
				(ScenarioSer)(createScenario_internal(domainName).externalize());
					
			thereAreUnflushedChanges = true;
			return scenarioSer;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized ScenarioSer copyScenario(String clientId, 
		String scenarioId)
	throws
		ModelContainsError,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(scenarioId);
			if (node == null) throw new ParameterError("Could not find Node " + scenarioId);
			
			if (! (node instanceof Scenario)) throw new ParameterError(
				"Node is not a Scenario");
			
			Scenario scenario = (Scenario)node;
			
			Domain domain = scenario.getDomain();

			Scenario newScenario = null;
			try { newScenario = domain.createScenario(null, scenario); }
			catch (CloneNotSupportedException ex)
			{
				throw new ModelContainsError(ex);
			}
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.ScenarioCreatedNotice(
					newScenario.getDomain().getNodeId(), newScenario.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
			
			return (ScenarioSer)(newScenario.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized AttributeSer createAttribute(String clientId, boolean confirm, 
		String parentId, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Attribute attribute = createAttribute_internal(confirm, parentId, x, y);
			
			
			// Notify clients that the Element was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(attribute.getParent());
			try
			{
				peer.notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(parentId));
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (AttributeSer)(attribute.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized AttributeSer createAttributeBefore(String clientId, 
		boolean confirm, String nextAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if (nextAttrId == null) throw new ParameterError("nextAttrId is null");
			
			Attribute attribute = createAttribute_internal(confirm, null, 
				null, nextAttrId, 0.0, 0.0);
			
			// Notify clients that the Element was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(attribute.getParent());
			try
			{
				peer.notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(
					attribute.getParent().getNodeId()));
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (AttributeSer)(attribute.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	
	public synchronized AttributeSer createAttributeAfter(String clientId, 
		boolean confirm, String priorAttrId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if (priorAttrId == null) throw new ParameterError("priorAttrId is null");
			
			Attribute attribute = createAttribute_internal(confirm, null, 
				priorAttrId, null, 0.0, 0.0);
			
			// Notify clients that the Element was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(attribute.getParent());
			try
			{
				peer.notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(
					attribute.getParent().getNodeId()));
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (AttributeSer)(attribute.externalize());
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized int getNoOfAttributes(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node with Id " + nodeId);
			return node.getNumberOfAttributes();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized AttributeSer createAttribute(String clientId, boolean confirm, 
		String parentId, Serializable value, String scenarioId, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Attribute attribute = createAttribute_internal(confirm, parentId, x, y);
		
			Domain domain = attribute.getDomain();
			if ((! (value instanceof Types.Inspect)) && (domain instanceof ModelDomain))
				deleteDependentSimulationRuns((ModelDomain)domain);
			
			// Set default value.
			
			attribute.setDefaultValue(value);
			
			// Set scenario value.
			
			if (scenarioId == null)
			{
				List<Scenario> scenarios = attribute.getDomain().getScenarios();
				for (Scenario scenario : scenarios)
					scenario.setAttributeValue(attribute, value);
			}
			else
			{
				PersistentNode node = PersistentNodeImpl.getNode(scenarioId);
				if (node == null) throw new ParameterError(
					"Cannot identify Node with ID " + scenarioId);
				
				if (! (node instanceof Scenario)) throw new ParameterError(
					"Node with ID " + scenarioId + " is not a Scenario");
				
				Scenario scenario = (Scenario)node;
				
				scenario.setAttributeValue(attribute, value);
			}
			
			// Notify clients that the Element was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(attribute.getParent());
			try
			{
				peer.notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(
					attribute.getParent().getNodeId()));
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (AttributeSer)(attribute.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized AttributeSer createHierarchyDomainAttributeBefore(String clientId,
		boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			HierarchyAttribute newAttr = createHierarchyDomainAttribute_internal(clientId,
				confirm, true,
				domainId, attrId, value, scenarioId);
			return (AttributeSer)(newAttr.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		
		
	public synchronized AttributeSer createHierarchyDomainAttributeAfter(String clientId,
		boolean confirm,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			HierarchyAttribute newAttr = createHierarchyDomainAttribute_internal(clientId,
				confirm, false,
				domainId, attrId, value, scenarioId);
			return (AttributeSer)(newAttr.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	protected HierarchyAttribute createHierarchyDomainAttribute_internal(String clientId,
		boolean confirm, boolean beforeOrAfter,
		String domainId, String attrId, Serializable value, String scenarioId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		// Find the Domain.
		// Find the corresponding Attribute in the Domain.
		// Call createAttributeBefore or createAttributeAfter on the Domain.
		// Make the new Attribute a column Attribute by calling applyToHierarchy on it.

		PersistentNode node = PersistentNodeImpl.getNode(domainId);
		if (node == null) throw new ElementNotFound(domainId);
		if (! (node instanceof HierarchyDomain)) throw new ParameterError(
			"Node with Id '" + domainId + "' is not a HierarchyDomain");
		HierarchyDomain domain = (HierarchyDomain)node;
		
		String newAttrName = domain.createUniqueChildName("Attribute");
		
		HierarchyAttribute newAttr = domain.createHierarchyAttribute(newAttrName,
			value, null, null);
		
		node = PersistentNodeImpl.getNode(attrId);
		if (node != null)  // Position new Attribute before the specified Attribute
		{
			if (! (node instanceof HierarchyAttribute)) throw new ParameterError(
				"Node with Id '" + attrId + "' is not a HierarchyAttribute");
			HierarchyAttribute attr = (HierarchyAttribute)node;
			if (attr.getParent() != domain) throw new ParameterError(
				"Attribute '" + attr.getFullName() + "' does not belong to '" +
				domain.getFullName() + "'");
			
			int attrSeqNo = attr.getParent().getAttributeSeqNo(attr);
			
			if (beforeOrAfter)
				attr.getParent().setAttributeSeqNo(newAttr, attrSeqNo);
			else
				attr.getParent().setAttributeSeqNo(newAttr, attrSeqNo+1);
		}
		
		node = PersistentNodeImpl.getNode(scenarioId);
		if (node == null)  // set value in each Scenario.
		{
			List<Scenario> scenarios = domain.getScenarios();
			for (Scenario scenario : scenarios)
				scenario.setAttributeValue(newAttr, value);
		}
		else  // set value only in the specified Scenario.
		{
			if (! (node instanceof HierarchyScenario)) throw new ParameterError(
				"Node with Id '" + scenarioId + "' is not a HierarchyScenario");
			HierarchyScenario scenario = (HierarchyScenario)node;
			scenario.setAttributeValue(newAttr, value);
		}
		
		newAttr.applyToHierarchy();
		
		// Notify clients that the Element was created.
		Peer peer = ServiceContext.getServiceContext().getPeer(domain);
		try
		{
			peer.notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(domain.getNodeId()));
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}

		return newAttr;
	}


	protected Attribute createAttribute_internal(boolean confirm, String parentId,
		double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		return createAttribute_internal(confirm, parentId, null, null, x, y);
	}
	
	
	protected Attribute createAttribute_internal(boolean confirm, String parentId, 
		String priorAttrId, String nextAttrId, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		PersistentNode parentNode = null;
		Attribute priorAttr = null;
		Attribute nextAttr = null;
		PersistentNode node = null;
		if (priorAttrId == null)
		{
			if (nextAttrId == null)
			{
				parentNode = PersistentNodeImpl.getNode(parentId);
			}
			else
			{
				node = PersistentNodeImpl.getNode(nextAttrId);
				if (! (node instanceof Attribute)) throw new ParameterError(
					"Node with Id " + nextAttrId + " is not an Attribute");
				nextAttr = (Attribute)node;
			}
		}
		else
		{
			node = PersistentNodeImpl.getNode(priorAttrId);
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Node with Id " + priorAttrId + " is not an Attribute");
			priorAttr = (Attribute)node;
		}
			
		if (parentId == null) parentNode = node.getParent();
		else
		{
			parentNode = PersistentNodeImpl.getNode(parentId);
			if ((node != null) && (parentNode != node.getParent())) throw new RuntimeException(
				"Parent Node does not match the parent Node specified");
		}
		
		if ((parentNode instanceof ModelElement) &&
			((ModelElement)parentNode).isLockedForSimulation()) 
			throw new CannotModifyDuringSimulation(parentNode.getFullName());
		
		
		Domain domain = parentNode.getDomain();
		
		if ((! confirm) && (domain instanceof ModelDomain))
		{
			if (((ModelDomain)domain).dependentSimulationRunsExist())
				throw new Warning("Simulation Runs will be invalid and will be deleted");
		}
		
		checkIfTemplateInstance(parentNode);
		checkIfTemplate(parentNode);

		PersistentNode[] outermostAffectedRef = new PersistentNode[1];
		Attribute attribute = 
			parentNode.createAttribute(null, outermostAffectedRef);
		
		thereAreUnflushedChanges = true;
		
		
		// Move to required position in list of Attributes.
		
		if (priorAttr == null)
			if (nextAttr == null)
				if (parentNode == null) throw new RuntimeException(
					"No prior, next, or parent Node could be identified");
				else
					parentNode.setAttributeAsLastInSeq(attribute);
			else
				parentNode.moveAttributeBefore(attribute, nextAttr);
		else
			parentNode.moveAttributeAfter(attribute, priorAttr);
		
		attribute.setX(x);
		attribute.setY(y);
		
		return attribute;
	}

	
	public synchronized AttributeStateBindingSer bindAttributeToState(String clientId, 
		boolean confirm, String attrId, String stateFullName)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ElementNotFound("Node with Id " + attrId +
				" not found");
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Node with ID " + attrId + " is not an Attribute");
			
			ModelAttribute attr = (ModelAttribute)node;

			if (attr.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(attr.getFullName());
			
			if (! confirm)
			{
				if (attr.getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			checkIfTemplateInstance(attr);
			checkIfTemplate(attr);

			node = findNode(stateFullName);
			if (node == null) throw new ElementNotFound("Node with name '" +
				stateFullName + "' not found.");
			
			if (! (node instanceof State)) throw new ParameterError(
				"Node with path '" + stateFullName + "' is not a State");
			
			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
			
			if (attr.getModelDomain() == state.getModelDomain()) throw new ParameterError(
				"Attribute and State belong to the same Model Domain.");
			
			AttributeStateBinding binding = attr.bindToState(state, null, null);
			
			try
			{
				attr.notifyAllListeners(new PeerNoticeBase.AttributeStateBindingChanged(
					attr.getModelDomain().getNodeId(), attr.getNodeId(), state.getNodeId()));
				
				state.notifyAllListeners(new PeerNoticeBase.AttributeStateBindingChanged(
					state.getModelDomain().getNodeId(), attr.getNodeId(), state.getNodeId()));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			
			return (AttributeStateBindingSer)(binding.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void deleteAttributeStateBinding(String clientId,
		boolean confirm, String bindingNodeId)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(bindingNodeId);
			if (node == null) throw new ElementNotFound("Node with Id " + bindingNodeId);
			
			if (! (node instanceof AttributeStateBinding)) throw new ParameterError(
				"Node with Id " + bindingNodeId + " is not an AttributeStateBinding");
			
			AttributeStateBinding binding = (AttributeStateBinding)node;

			if (((ModelElement)node).isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(node.getFullName());
			
			if (! confirm)
			{
				if (((ModelElement)node).getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			checkIfTemplateInstance(binding);
			checkIfTemplate(binding);

			deleteDependentSimulationRuns(binding.getModelDomain());
			deleteDependentSimulationRuns(binding.getForeignState().getModelDomain());
			
			String domainId = binding.getModelDomain().getNodeId();
			String foreignDomainId = binding.getForeignState().getNodeId();
			
			
			// Delete the binding.
			
			binding.getAttribute().deleteStateBinding(binding, null);
			
			
			// Notify Listeners.
			
			try
			{
				node.notifyAllListeners(new PeerNoticeBase.AttributeStateBindingChanged(
					domainId));
				
				node.notifyAllListeners(new PeerNoticeBase.AttributeStateBindingChanged(
					foreignDomainId));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setStateBindingParameters(String clientId, boolean confirm, 
		String domainOrScenarioId, Boolean type, TimePeriodType period)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		// Note: Only a ModelDomain or a ModelScenario can be specified.
		// Note: if a Domain is specified, the arguments may not be null.
		
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(domainOrScenarioId);
			if (node == null) throw new ElementNotFound("Node with Id " + domainOrScenarioId);
			
			ModelDomain domain = null;
			ModelScenario scenario = null;
			
			if (node instanceof ModelDomain)
				domain = (ModelDomain)node;
			else if (node instanceof ModelScenario)
				scenario = (ModelScenario)node;
			else throw new ParameterError("Node with Id " + domainOrScenarioId +
				" is not a Model Domain or a Model Scenario");

			if (domain != null)
				if ((type == null) || (period == null)) throw new ParameterError(
					"For a Domain, the 'type' and 'period' arguments may not be null");
			
			if (((ModelElement)node).isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(node.getFullName());
			
			if (! confirm)
			{
				if (((ModelElement)node).getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			if (domain != null)
				deleteDependentSimulationRuns((ModelDomain)node);
			else if (scenario != null)
				deleteDependentSimulationRuns((ModelScenario)node);
			
			
			// Set binding parameters.
			
			if (scenario != null)
			{
				scenario.setExternalStateBindingType(type);
				scenario.setExternalStateGranularity(period);
			}
			
			if (domain != null)
			{
				domain.setExternalStateBindingType(type);
				domain.setExternalStateGranularity(period);
			}
			
			
			// Notify Listeners.
			
			try
			{
				node.notifyAllListeners(new PeerNoticeBase.AttributeStateBindingChanged(
					((ModelElement)node).getModelDomain().getNodeId()));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void bindScenarioToScenarioForDomain(String clientId,
		boolean confirm, String bindingScenarioId, String boundScenarioId,
		String boundDomainId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (boundDomainId == null) throw new ParameterError("Null bound Domain Id");

			PersistentNode node = PersistentNodeImpl.getNode(boundDomainId);
			if (node == null) throw new ElementNotFound("Domain with Id " + boundDomainId);
			if (! (node instanceof ModelDomain)) throw new ParameterError(
				"Node with Id " + boundDomainId + " is not a ModelDomain");
			ModelDomain boundDomain = (ModelDomain)node;
			
			node = PersistentNodeImpl.getNode(bindingScenarioId);
			if (node == null) throw new ElementNotFound("Node with Id " + bindingScenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + bindingScenarioId + " is not a ModelScenario");
			ModelScenario bindingScenario = (ModelScenario)node;
			
			ModelScenario boundScenario = null;
			if (boundScenarioId != null)
			{
				node = PersistentNodeImpl.getNode(boundScenarioId);
				if (node == null) throw new ElementNotFound("Node with Id " + boundScenarioId);
				if (! (node instanceof ModelScenario)) throw new ParameterError(
					"Node with Id " + boundScenarioId + " is not a ModelScenario");
				boundScenario = (ModelScenario)node;
			}
			
			ModelScenario previouslyBoundScenario = 
				bindingScenario.getBoundScenarioForDomain(boundDomain);
			
			if (bindingScenario != null)
			{
				if (((ModelScenario)bindingScenario).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(bindingScenario.getFullName());
			}
			
			if (previouslyBoundScenario != null)
			{
				if (previouslyBoundScenario != null)
					if (((ModelScenario)previouslyBoundScenario).isLockedForSimulation())
						throw new CannotModifyDuringSimulation(previouslyBoundScenario.getFullName());
			}
			
			if (boundScenario != null)
				if (boundScenario.isLockedForSimulation())
					throw new CannotModifyDuringSimulation(boundScenario.getFullName());
			
			if (! confirm)
			{
				if (bindingScenario.getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			deleteDependentSimulationRuns(bindingScenario);
			
			
			// Remove prior binding.
			
			if (previouslyBoundScenario != null)
				bindingScenario.removeBoundScenario(previouslyBoundScenario);
			
			
			// Create new binding.
			
			if (boundScenario != null)
				bindingScenario.addBoundScenario(boundScenario);
			
			
			// Notify.
			
			try
			{
				if (previouslyBoundScenario != null)
					previouslyBoundScenario.notifyAllListeners(new PeerNoticeBase.ScenarioBindingRemoved(
						bindingScenario.getDomain().getNodeId(), 
						previouslyBoundScenario.getDomain().getNodeId()));
				
				bindingScenario.notifyAllListeners(new PeerNoticeBase.ScenarioBindingCreated(
					bindingScenario.getDomain().getNodeId(), boundScenario.getDomain().getNodeId()));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized AttributeStateBindingSer getAttributeStateBinding(
		String clientId, String attrId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ElementNotFound(
				"Node with Id " + attrId + " not found");
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Node with Id " + attrId + " is not an Attribute");
			
			ModelAttribute attr = (ModelAttribute)node;
			
			AttributeStateBinding asBinding = attr.getStateBinding();
			if (asBinding == null) return null;
			
			return (AttributeStateBindingSer)(asBinding.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainSer[] getBoundDomains(String clientId,
		String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(modelDomainId);
			if (node == null) throw new ElementNotFound(
				"Node with Id " + modelDomainId + " not found");
			
			if (! (node instanceof ModelDomain)) throw new ParameterError(
				"Node with Id " + modelDomainId + " is not a Model Domain");
			
			ModelDomain domain = (ModelDomain)node;
			

			// Return the Domains that own States to which Attributes of the specified
			// Domain are bound.
			
			Set<AttributeStateBinding> bindings = domain.getAttributeStateBindings();
			
			Set<ModelDomain> boundDomains = new TreeSetNullDisallowed<ModelDomain>();
			for (AttributeStateBinding b : bindings)
			{
				try { boundDomains.add(b.getForeignState().getModelDomain()); }
				catch (ValueNotSet w) {}  // ok
			}
			
			ModelDomainSer[] boundDomainSers = new ModelDomainSer[boundDomains.size()];
			int i = 0;
			for (ModelDomain d : boundDomains)
				boundDomainSers[i++] = (ModelDomainSer)(d.externalize());
			
			return boundDomainSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelDomainSer[] getBindingDomains(String clientId, 
		String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(modelDomainId);
			if (node == null) throw new ElementNotFound("Node with id " + modelDomainId);
			
			if (! (node instanceof ModelDomain)) throw new ParameterError(
				"Node with Id " + modelDomainId + " is not a Model Domain");
			
			ModelDomain domain = (ModelDomain)node;

			Set<ModelDomain> domains = domain.getBindingDomains();
			
			Set<ModelDomainSer> domainSers = new TreeSetNullDisallowed<ModelDomainSer>();
			for (ModelDomain d : domains)
			{
				domainSers.add((ModelDomainSer)(d.externalize()));
			}
			
			return domainSers.toArray(new ModelDomainSer[domainSers.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSer getBoundScenarioForBoundDomain(String clientId,
		String bindingScenarioId, String modelDomainId)
	throws
		ElementNotFound,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(bindingScenarioId);
			if (node == null) throw new ElementNotFound("Node with id " + bindingScenarioId);
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + bindingScenarioId + " is not a Model Scenario");
			
			ModelScenario bindingScenario = (ModelScenario)node;
			
			node = PersistentNodeImpl.getNode(modelDomainId);
			if (node == null) throw new ElementNotFound("Node with id " + modelDomainId);
			
			if (! (node instanceof ModelDomain)) throw new ParameterError(
				"Node with Id " + modelDomainId + " is not a Model Domain");
			
			ModelDomain domain = (ModelDomain)node;

			ModelScenario boundScenario = bindingScenario.getBoundScenarioForDomain(domain);
			
			if (boundScenario == null) return null;
			
			return (ModelScenarioSer)(boundScenario.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized StateSer[] getStatesForModelDomain(String clientId,
		String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Domain domain = this.domains.get(modelDomainName);
			if (domain == null) throw new ParameterError(
				"Domain " + modelDomainName + " not found");
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain md = (ModelDomain)domain;
			
			List<State> states = md.getStatesRecursive();
			
			Set<StateSer> stateSers = new TreeSetNullDisallowed<StateSer>();
			for (State state : states)
			{
				stateSers.add((StateSer)(state.externalize()));
			}
			
			return stateSers.toArray(new StateSer[stateSers.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized StateSer[] getTallyStatesForModelDomain(String clientId,
		String modelDomainName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Domain domain = this.domains.get(modelDomainName);
			if (domain == null) throw new ParameterError(
				"Domain " + modelDomainName + " not found");
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain md = (ModelDomain)domain;
			
			List<State> states = md.getStatesRecursive();
			
			Set<StateSer> tallyStateSers = new TreeSetNullDisallowed<StateSer>();
			for (State state : states)
			{
				if (  // state is a Tally State
					(state.getParent() instanceof Tally)
					&&
					(((Tally)(state.getParent())).getTallyState() == state))
					
					tallyStateSers.add((StateSer)(state.externalize()));
			}
			
			return tallyStateSers.toArray(new StateSer[tallyStateSers.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized NodeSer createListNodeBefore(String clientId, boolean confirm,
		String parentId, String nextNodeId, String scenarioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! (parentNode instanceof PersistentListNode)) throw new ParameterError(
				"Node with Id " + parentId + " is not a ListNode");
			
			PersistentNode nextNode = PersistentNodeImpl.getNode(nextNodeId);
			if ((nextNode != null) && (! (nextNode instanceof PersistentListNode)))
				throw new ParameterError(
				"Node with Id " + nextNodeId + " is not a ListNode");
			
			Scenario scenario = null;
			if (scenarioId != null)
			{
				PersistentNode node = PersistentNodeImpl.getNode(scenarioId);
				if (node == null) throw new ParameterError("Cannot find Scenario with Id " + scenarioId);
				scenario = (Scenario)node;
			}
			
			PersistentNode priorNode = null;
			if (nextNode != null) priorNode = ((Hierarchy)nextNode).getPriorHierarchy();
			
			PersistentNode node = createListNode_internal(confirm, 
				(PersistentListNode)parentNode, (PersistentListNode)priorNode,
				(PersistentListNode)nextNode, scenario, copiedNodeId);
			
			return node.externalize();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer createListNodeAfter(String clientId, boolean confirm,
		String parentId, String priorNodeId, String scenarioId, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! (parentNode instanceof PersistentListNode)) throw new ParameterError(
				"Node with Id " + parentId + " is not a ListNode");
			
			PersistentNode priorNode = PersistentNodeImpl.getNode(priorNodeId);
			if ((priorNode != null) && (! (priorNode instanceof PersistentListNode)))
				throw new ParameterError(
				"Node with Id " + priorNodeId + " is not a ListNode");
			
			Scenario scenario = null;
			if (scenarioId != null)
			{
				PersistentNode node = PersistentNodeImpl.getNode(scenarioId);
				if (node == null) throw new ParameterError("Cannot find Scenario with Id " + scenarioId);
				scenario = (Scenario)node;
			}
			
			PersistentNode nextNode = null;
			if (priorNode != null) nextNode = ((Hierarchy)priorNode).getNextHierarchy();
			
			PersistentNode node = createListNode_internal(confirm, 
				(PersistentListNode)parentNode, (PersistentListNode)priorNode,
				(PersistentListNode)nextNode, scenario, copiedNodeId);

			return node.externalize();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer cloneListNodeAfter(String clientId, boolean confirm,
		String nodeId)
	throws
		Warning,
		ParameterError,
		InternalEngineError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + nodeId);
			
			CloneMap cloneMap = new CloneMap(node);
			PersistentNode nodeCopy;
			try { nodeCopy = node.clone(cloneMap, node.getParent()); }
			catch (CloneNotSupportedException ex) { throw new InternalEngineError(ex); }
			
			PersistentListNode parent = (PersistentListNode)(node.getParent());
			
			if (! (node instanceof PersistentListNode)) throw new ParameterError(
				"Node with Id " + nodeId + " is not a ListNode");
			
			if (parent == null)
			{
				if (! (nodeCopy instanceof Domain)) throw new RuntimeException(
					"Node with Id " + nodeId + " has no parent and it is not a Domain");
				
				String newName = createUniqueDomainName();
				((PersistentNodeImpl)nodeCopy).setName(newName);
				addNewDomain((Domain)nodeCopy);
				
				// Notify clients that the Domain was created.
				
				Peer peer = ServiceContext.getServiceContext().getPeer(node);
				try { peer.notifyAllListeners(
					new PeerNoticeBase.DomainCreatedNotice(nodeCopy.getNodeId())); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}			
			}
			else
			{
				String newName = parent.createUniqueChildName();
				((PersistentNodeImpl)nodeCopy).setName(newName);
				
				if (parent instanceof ModelElement)
				{
					ModelElement parentElement = (ModelElement)parent;
					if (parentElement.isLockedForSimulation())
						throw new CannotModifyDuringSimulation(parent.getFullName());

					if (! confirm)
					{
						if (parentElement.getModelDomain().dependentSimulationRunsExist())
							throw new Warning(
								"Simulation Runs will be invalid and will be deleted");
					}
				}
				
				parent.addOrderedChildAfter(nodeCopy, node);
			
				// Notify clients that the Node was added.
				
				Peer peer = ServiceContext.getServiceContext().getPeer(node);
				try { peer.notifyAllListeners(
					new PeerNoticeBase.NodeAddedNotice(parent.getNodeId(), nodeCopy.getNodeId())); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}			
			}
			
			return nodeCopy.externalize();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	private static final String[] NodeKindsForNamedRefConstraints =
	{
		"expressway.server.PersistentNode",
		"expressway.server.HierarchyElement",
		"expressway.server.ModelElement",
		"expressway.server.DecisionElement"
	};
	
	
	public synchronized String[] getInterfaceNodeKinds(String clientId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			return NodeKindsForNamedRefConstraints;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized ModelElementSer createModelElement(String clientId, boolean confirm,
		String parentId, String kind, double x, double y)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			ModelElement parentElement =
				(ModelElement)(identifyNode_internal(parentId, ModelElement.class));
			
			if (parentElement.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(parentElement.getFullName());
			
			if (! confirm)
			{
				if (parentElement.getModelDomain().dependentSimulationRunsExist()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
			}
			
			PersistentNode node = createGraphicNode_internal(confirm, parentElement, kind, x, y);
			ModelElement me = (ModelElement)node;
			
			// Delete simulation runs associated with all Scenarios of the Domain.
			deleteDependentSimulationRuns(me.getModelDomain());
			
			return (ModelElementSer)(me.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String[] getInstantiableNodeKinds(String clientId, 
		String parentId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! (parentNode instanceof ModelContainer)) throw new ParameterError(
				"Can only call this method on a Model Container.");
			
			List<String> names = 
				((ModelContainer)parentNode).getInstantiableNodeKinds();
				
			return names.toArray(new String[names.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String[] getInstantiableNodeDescriptions(String clientId, 
		String parentId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Cannot identify the Node with ID " + parentId);
			
			if (! (parentNode instanceof ModelContainer)) throw new ParameterError(
				"Can only call this method on a Model Container.");
			
			List<String> descriptions = 
				((ModelContainer)parentNode).getInstantiableNodeDescriptions();
				
			return descriptions.toArray(new String[descriptions.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void deleteNode(String clientId, boolean confirm, 
		String nodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + nodeId);
			
			//if (! (node instanceof ModelElement)) throw new ParameterError(
			//	"Can only call this method for a Model Element.");
			
			
			// Verify that deletion is allowed.
			
			if (node instanceof ModelElement)
			{
				ModelElement me = (ModelElement)node;
				if (me.isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(me.getFullName());
				
				if (! (node instanceof SimulationRun))
					if (((ModelElement)node).getModelDomain().dependentSimulationRunsExist())
						if (! confirm) throw new Warning(
							"Simulation Runs will be invalid and will be deleted");
			}
			
			PersistentNode parentNode = node.getParent();
			if (parentNode != null)
			{
				checkIfTemplateInstance(parentNode);
				checkIfTemplate(parentNode);
			}

			boolean canDeleteSimRuns = false;
			
			if (node instanceof ModelAttribute)
			{
				ModelAttribute attr = (ModelAttribute)node;
				ModelDomain domain = attr.getModelDomain();
				List<ModelScenario> scenarios = domain.getModelScenarios();
				
				for (ModelScenario scenario : scenarios)
					// each Scenario in which the Attribute has a value
				{
					Serializable value = scenario.getAttributeValue(attr);
					if ((value != null) && (! (value instanceof Types.Inspect)))
					{
						if (attr.getModelDomain().dependentSimulationRunsExist())
							if (confirm)
								canDeleteSimRuns = true;
							else
								throw new Warning(
									"Simulation Runs will be invalid and will be deleted");
						
						break;
					}
				}
			}
			
			if (node instanceof MotifDef)
			{
				MotifDef motifDef = (MotifDef)node;
				
				if (! confirm) throw new Warning(
					"Server should be restarted after deleting a motif; otherwise " +
						"the motif's classes might still be available.");
				
				Set<Domain> refDomains = motifDef.getReferencingDomains();
				if (refDomains.size() > 0)
				{
					String domainStr = null;
					for (Domain d : refDomains)
					{
						if (domainStr == null) domainStr = "";
						else domainStr += ", ";
						
						domainStr += d.getName();
					}
					
					throw new ParameterError("The following Domains use motif " +
						node.getName() + ": " + domainStr);
				}
				
				// Notify all MotifDefChangeListeners (DomainTypes).
				Collection<DomainType> dts = domainTypes.values();
				for (DomainType dt : dts) dt.motifDefDeleted(motifDef);
			}
			
			String scopeId = nodeId;
			
			if (node instanceof Domain)
			{
				Domain domain = (Domain)node;
				
				List<Scenario> scenarios = domain.getScenarios();
				if (scenarios.size() != 0)
				{
					if (! confirm) throw new Warning("Scenarios will be deleted.");
					
					for (Scenario scenario : scenarios)
						scenario.prepareForDeletion();
				}
				
				scenarios.clear();
				domains.remove(domain.getName());
				domain.deleteFromAllNodesTable();
			}
			
			if (node.isPredefined()) throw new ParameterError(
				"Cannot delete a predefined Node");
				
			
			// Delete dependent Nodes.
			
			if (node instanceof ScenarioSet)
			{
				// The Scenarios in the ScenarioSet should be deleted too, even
				// though they are not "owned by" the ScenarioSet (according to
				// the Ownership pattern). This is ok because a Scenario cannot
				// belong to more than one ScenarioSet, and so there will be no
				// unexpected outcome.
				
				// Delete (from the Domain) each Scenario in the Scenario Set.
				
				Domain domain = ((ScenarioSet)node).getDomain();
				List<Scenario> scenarios = ((ScenarioSet)node).getScenarios();
				for (Scenario scenario : scenarios)
				{
					scenario.prepareForDeletion();
					domain.deleteChild(scenario, null);
					
					try { scenario.notifyNodeDeleted(); }
					catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
				}
				
				// Delete the ScenarioSet
				domain.deleteChild((ScenarioSet)node, null);
			}
			else if (node instanceof SimRunSet)
			{
				// The SimulationRuns in the SimRunSet should be deleted too, even
				// though they are not "owned by" the SimRunSet (according to
				// the Ownership pattern). This is ok because a SimulationRun cannot
				// belong to more than one SimRunSet, and so there will be no
				// unexpected outcome.
				
				ModelDomain domain = ((SimRunSet)node).getModelDomain();
				Set<SimulationRun> simRuns = ((SimRunSet)node).getSimulationRuns();
				for (SimulationRun simRun : simRuns)
				{
					simRun.prepareForDeletion();
					domain.deleteSubcomponent(simRun, null);
					
					try { simRun.notifyNodeDeleted(); }
					catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
				}
			}
			else if (node instanceof SimulationRun)
			{
				if (! (parentNode instanceof ModelScenario)) throw new ParameterError(
					"Can only delete a Simulation Run from its parent Model Scenario.");
				
				((ModelScenario)parentNode).deleteSimulationRun((SimulationRun)node);
			}
			
			if (node instanceof ModelElement)
			{
				if (canDeleteSimRuns)
					deleteDependentSimulationRuns(((ModelElement)node).getModelDomain());
			}
			
			
			// Perform deletion.
			
			PersistentNode[] outermostAffectedRef = new PersistentNode[1];
			if (parentNode != null) parentNode.deleteChild(node, outermostAffectedRef);
			//scopeId = outermostAffectedRef[0].getNodeId();
			
			thereAreUnflushedChanges = true;

			
			// Notify clients that the Element was deleted.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeDeletedNotice(nodeId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void demoteListNode(String clientId, boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + nodeId);
			
			if (! (node instanceof PersistentListNode)) throw new ParameterError(
				"Node with ID " + nodeId + " is not a PersistentListNode");
			
			PersistentListNode listNode = (PersistentListNode)node;
			
			
			// Identify prior sibling, if any.
			
			PersistentNode parent = node.getParent();
			if (parent == null) throw new ParameterError("Cannot demote a top-level Node");
			if (! (parent instanceof PersistentListNode)) throw new ParameterError(
				"Cannot demote a Node that does not belong to a ListNode");
			
			PersistentListNode parentListNode = (PersistentListNode)parent;
			
			PersistentNode priorNode = parentListNode.getOrderedChildBefore(node);
			
			if (priorNode == null) return;
			
			if (! (priorNode instanceof PersistentListNode)) throw new ParameterError(
				"Cannot demote: prior Node is not a ListNode");
			
			PersistentListNode priorListNode = (PersistentListNode)priorNode;
			
			
			// Remove Node from its parent.
			
			parentListNode.removeOrderedChild(node);
			
			
			// Add Node to the child List of its prior sibling, as the last child.
			
			PersistentNode lastChildOfPrior = priorListNode.getLastOrderedChild();
			priorListNode.addOrderedChildAfter(node, lastChildOfPrior);
			
			
			// Notify.
			
			PeerNoticeBase notice = new PeerNoticeBase.NodeChangedNotice(parent.getNodeId());
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(notice); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void promoteListNode(String clientId, boolean confirm, String nodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + nodeId);
			
			if (! (node instanceof PersistentListNode)) throw new ParameterError(
				"Node with ID " + nodeId + " is not a PersistentListNode");
			
			PersistentListNode listNode = (PersistentListNode)node;
			
			
			// If this Node has a parent, and if the parent is not a Domain, make this
			// Node the parent's next sibling.
			
			PersistentNode parent = node.getParent();
			if (parent == null) throw new ParameterError("Cannot promote a top-level Node");
			if (parent instanceof Domain) throw new ParameterError(
				"Cannot promote a Node to Domain level");
			if (! (parent instanceof PersistentListNode)) throw new ParameterError(
				"Cannot promote a Node that does not belong to a ListNode");
			
			PersistentListNode parentListNode = (PersistentListNode)parent;
			
			PersistentNode parentParent = parentListNode.getParent();
			if (parentParent == null) throw new ParameterError(
				"Cannot promote a Node to the Domain level");
			if (! (parentParent instanceof PersistentListNode)) throw new ParameterError(
				"Cannot promote a Node whose parent does not belong to a ListNode");
			
			PersistentListNode parentParentListNode = (PersistentListNode)parentParent;
			
			// Make this Node the parent's next sibling.
			
			parentListNode.removeOrderedChild(node);
			parentParentListNode.addOrderedChildAfter(node, parent);
			
			
			// Notify.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(parentParent);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(parentParent.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		

	public synchronized void createNamedReference(String clientId, boolean confirm,
		String domainId, String name, String desc, String fromTypeClassName, String toTypeClassName)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ElementNotFound("Node with Id " + domainId);
			if (! (node instanceof Domain)) throw new ParameterError(
				"Node with Id " + domainId + " is not a Domain");
			Domain domain = (Domain)node;
			
			if (domain instanceof ModelDomain)
			{
				ModelDomain md = (ModelDomain)domain;
				if (md.isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(md.getFullName());
				
				if (! confirm)
				{
					if (md.dependentSimulationRunsExist()) throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
				}
				
				// Delete simulation runs associated with all Scenarios of the Domain.
				deleteDependentSimulationRuns(md);
			}

			Class fromType;
			try { fromType = domain.getNodeKind(fromTypeClassName); }
			catch (ClassNotFoundException ex) { throw new ParameterError(ex); }
			Class toType;
			try { toType = domain.getNodeKind(toTypeClassName); }
			catch (ClassNotFoundException ex) { throw new ParameterError(ex); }
			domain.createNamedReference(name, desc, fromType, toType);
			
			// Notify clients.
			try { domain.notifyAllListeners(); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized void deleteNamedReference(String clientId, boolean confirm,
		String namedRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Find the Node.
			PersistentNode node = PersistentNodeImpl.getNode(namedRefId);
			if (node == null) throw new ElementNotFound("Node with Id " + namedRefId);
			
			// Confrim
			Domain d = node.getDomain();
			if (d instanceof ModelDomain)
			{
				ModelDomain md = (ModelDomain)d;
				if ((! confirm) && md.dependentSimulationRunsExist()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			}
			
			// Delete the NamedReference.
			if (! (node instanceof NamedReference)) throw new ParameterError(
				"Node with Id " + namedRefId + " is not a Named Reference");
			NamedReference nr = (NamedReference)node;
			Domain domain = (Domain)(nr.getOwner());
			domain.deleteNamedReference(nr);

			// Notify clients.
			try { domain.notifyAllListeners(); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void createCrossReference(String clientId, boolean confirm, //String domainId, 
		String namedRefName, String fromNodeId, String toNodeId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode fromNode = PersistentNodeImpl.getNode(fromNodeId);
			if (fromNode == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + fromNodeId);
			if (! (fromNode instanceof CrossReferenceable)) throw new ParameterError(
				"Node " + fromNode.getFullName() + " is not a CrossReferenceable Node");
			
			PersistentNode toNode = PersistentNodeImpl.getNode(toNodeId);
			if (toNode == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + toNodeId);
			if (! (toNode instanceof CrossReferenceable)) throw new ParameterError(
				"Node " + toNode.getFullName() + " is not a CrossReferenceable Node");
			
			Domain domain = null;
			Domain fromNodeDomain = fromNode.getDomain();
			Domain toNodeDomain = toNode.getDomain();
			NamedReference nr = fromNodeDomain.getNamedReference(namedRefName);
			CrossReference cr = null;
			ParameterError p1 = null;
			ParameterError p2;
			if (nr != null) try { cr = nr.link((CrossReferenceable)fromNode, (CrossReferenceable)toNode); }
			catch (ParameterError pe) { p1 = pe; }
			
			if (cr == null)
			{
				nr = toNodeDomain.getNamedReference(namedRefName);
				if (nr != null) try { cr = nr.link((CrossReferenceable)fromNode, (CrossReferenceable)toNode); }
				catch (ParameterError pe)
				{
					p2 = pe;
					if (p1 != null) GlobalConsole.printStackTrace(p1);
					GlobalConsole.printStackTrace(p2);
					throw new ParameterError("Unable to link '" + fromNode.getFullName() +
						"' to '" + toNode.getFullName() + "' via '" + nr.getName() + "'");
				}
			}
			
			if (nr == null) throw new ParameterError(
				"A compatible Named Reference '" + namedRefName + "' not found for Domain '" +
				fromNodeDomain.getName() + "' or '" + toNodeDomain.getName() + "'");
			
			// Nofify clients.
			try { nr.notifyAllListeners(new PeerNoticeBase.CrossReferenceAddedNotice(
				cr.getFromNode().getNodeId(), cr.getToNode().getNodeId(), cr.getNodeId())); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized void deleteCrossReference(String clientId, boolean confirm,
		String crossRefId)
	throws
		Warning,
		ParameterError,
		ElementNotFound,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Find the Node.
			PersistentNode node = PersistentNodeImpl.getNode(crossRefId);
			if (node == null) throw new ElementNotFound("Node with Id " + crossRefId);
			
			// Confrim
			Domain d = node.getDomain();
			if (d instanceof ModelDomain)
			{
				ModelDomain md = (ModelDomain)d;
				if ((! confirm) && md.dependentSimulationRunsExist()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			}
			
			// Delete the Cross Reference.
			if (! (node instanceof CrossReference)) throw new ParameterError(
				"Node with Id " + crossRefId + " is not a Cross Reference");
			CrossReference cr = (CrossReference)node;

			String fromNodeId = cr.getFromNode().getNodeId();
			String toNodeId = cr.getToNode().getNodeId();
			NamedReference nr = cr.getNamedReference();
			nr.unlink(cr);

			// Notify clients.
			try { nr.notifyAllListeners(new PeerNoticeBase.CrossReferenceDeletedNotice(
				fromNodeId, toNodeId, crossRefId)); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized NodeSer[] getNamedRefFromNodes(String clientId, String nodeId,
		String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + nodeId);
			
			if (! (node instanceof CrossReferenceable)) throw new ParameterError(
				"Node type cannot participate in NamedReferences");
			
			CrossReferenceable cr = (CrossReferenceable)node;
			
			Set<CrossReferenceable> allFromNodes = cr.getFromNodes(namedRefName);
			Set<NodeSer> fromNodeSers = new TreeSetNullDisallowed<NodeSer>();
			for (CrossReferenceable fromNode : allFromNodes)
			{
				if (fromNode == cr) continue;
				fromNodeSers.add(fromNode.externalize());
			}
			
			return fromNodeSers.toArray(new NodeSer[fromNodeSers.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized NodeSer[] getNamedRefToNodes(String clientId, String nodeId,
		String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + nodeId);
			
			if (! (node instanceof CrossReferenceable)) throw new ParameterError(
				"Node type cannot participate in NamedReferences");
			
			CrossReferenceable cr = (CrossReferenceable)node;
			
			Set<CrossReferenceable> allToNodes = cr.getToNodes(namedRefName);
			Set<NodeSer> toNodeSers = new TreeSetNullDisallowed<NodeSer>();
			for (CrossReferenceable toNode : allToNodes)
			{
				if (toNode == cr) continue;
				toNodeSers.add(toNode.externalize());
			}
			
			return toNodeSers.toArray(new NodeSer[toNodeSers.size()]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized String[] getNamedReferences(String clientId, String domainId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + domainId);
			
			if (! (node instanceof Domain)) throw new ParameterError(
				"Node is not a Domain");
			
			Domain domain = (Domain)node;
			Set<NamedReference> nrs = domain.getNamedReferences();
			
			String[] names = new String[nrs.size()];
			int i = 0;
			for (NamedReference nr : nrs)
			{
				names[i++] = nr.getName();
			}
			
			return names;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized NamedReferenceSer getNamedReferenceForDomain(String clientId, String domainId,
		String namedRefName)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ElementNotFound("Node with Id " + domainId);
			if (! (node instanceof Domain)) throw new ParameterError(
				"Node with Id " + domainId + " is not a Domain");
			Domain domain = (Domain)node;
			
			NamedReference namedRef = domain.getNamedReference(namedRefName);
			if (namedRef == null) throw new ElementNotFound(
				"NamedReference with name '" + namedRefName + "'");
			return (NamedReferenceSer)(namedRef.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized CrossReferenceSer[] getCrossReferences(String clientId, String namedRefId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(namedRefId);
			if (node == null) throw new ElementNotFound("Node with Id " + namedRefId);
			if (! (node instanceof NamedReference)) throw new ParameterError(
				"Node with Id " + namedRefId + " is not a NamedReference");
			NamedReference namedRef = (NamedReference)node;
			
			Set<CrossReference> crs = namedRef.getCrossReferences();
			CrossReferenceSer[] crSers = new CrossReferenceSer[crs.size()];
			int i = 0;
			for (CrossReference cr : crs)
			{
				crSers[i++] = (CrossReferenceSer)(cr.externalize());
			}
			
			return crSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized CrossReferenceSer[] getCrossReferences(String clientId,
		String fromId, String toId)
	throws
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if ((fromId == null) && (toId == null)) throw new ParameterError(
				"Both Node Ids are null");
			
			CrossReferenceable fromNode = null;
			CrossReferenceable toNode = null;
			
			PersistentNode node;
			
			if (fromId != null)
			{
				node = PersistentNodeImpl.getNode(fromId);
				if (node == null) throw new ElementNotFound("Node with Id " + fromId);
				if (! (node instanceof CrossReferenceable))
					throw new ParameterError("Node with Id " + fromId + " is not CrossReferenceable");
				fromNode = (CrossReferenceable)node;
			}
			
			if (toId != null)
			{
				node = PersistentNodeImpl.getNode(toId);
				if (node == null) throw new ElementNotFound("Node with Id " + toId);
				if (! (node instanceof CrossReferenceable))
					throw new ParameterError("Node with Id " + toId + " is not CrossReferenceable");
				toNode = (CrossReferenceable)node;
			}
			
			Set<CrossReference> fromNodeCrs = null;
			if (fromNode != null) fromNodeCrs = fromNode.getCrossReferences();
			Set<CrossReference> toNodeCrs = null;
			if (toNode != null) toNodeCrs = toNode.getCrossReferences();
			
			Set<CrossReference> crs = new TreeSetNullDisallowed<CrossReference>();
			if (fromNodeCrs == null) crs.addAll(toNodeCrs);
			else if (toNodeCrs == null) crs.addAll(fromNodeCrs);
			else
			{
				for (CrossReference cr : fromNodeCrs)
				{
					if (cr.getToNode() == toNode) crs.add(cr);
				}
			}
			
			CrossReferenceSer[] crSers = new CrossReferenceSer[crs.size()];
			int i = 0;
			for (CrossReference cr : crs) crSers[i++] = (CrossReferenceSer)(cr.externalize());
			
			return crSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void bindStateAndPort(String clientId, boolean confirm,
		String stateId, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + stateId);
			
			if (! (node instanceof State)) throw new ParameterError(
				"Node with ID " + stateId + " is not a State");

			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
			
			node = PersistentNodeImpl.getNode(portId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + portId);
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Node with ID " + portId + " is not a Port");

			Port port = (Port)node;
			
			if (port.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(port.getFullName());
			

			// Check if the State or Port is pre-defined.
			
			if (state.isPredefined() || port.isPredefined()) throw new ParameterError(
				"Cannot bind to a predefined component");
			

			checkIfTemplateInstance(state);
			checkIfTemplate(state);

			if (! confirm)
			{
				if (port.getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			
			state.bindPort(port);

			
			// Notify clients.
			
			PersistentNode container = state.getParent();
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NonStructuralChangeNotice(container.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void unbindPort(String clientId, boolean confirm, String portId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(portId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify the Node with ID " + portId);
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Node with ID " + portId + " is not a Port");
			
			Port port = (Port)node;
			
			PortedContainer container = (PortedContainer)(port.getParent());
			
			if (container.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(container.getFullName());
			
			checkIfTemplateInstance(container);
			checkIfTemplate(container);

			if (! confirm)
			{
				if (port.getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}

			List<State> states = container.getStates();
			boolean foundBinding = false;
			for (State state : states)
			{
				if (state.getPortBindings().contains(port))
				{
					state.unbindPort(port);
					foundBinding = true;
					break;
				}
			}
			
			if (! foundBinding) throw new ParameterError("Port " + port.getFullName() + 
				" is not bound to a State within " + container.getFullName());

			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NonStructuralChangeNotice(container.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized StateSer getPortBinding(String clientId, String portId)
	throws
		ParameterError,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(portId);
			if (node == null) throw new ElementNotFound(
				"Node with ID " + portId + " not found");
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Node with ID " + portId + " is not a Port");
			
			Port port = (Port)node;
			
			State state = port.getBoundState();
			if (state == null) return null;
			
			return (StateSer)(state.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setPredefEventDefaultTime(String clientId, boolean confirm, 
		String predEventNodeId, String timeExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(predEventNodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + predEventNodeId);
			
			if (! (node instanceof PredefinedEvent)) throw new ParameterError(
				"Node with ID " + predEventNodeId + " is not a PredefinedEvent");
			
			PredefinedEvent predEvent = (PredefinedEvent)node;
			
			if (predEvent.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(predEvent.getFullName());
			
			checkIfTemplateInstance(predEvent);
			checkIfTemplate(predEvent);

			if (! confirm)
			{
				if (predEvent.getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			predEvent.setDefaultTimeExpression(timeExpr);
			
			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(predEventNodeId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void setPredefEventDefaultValue(String clientId, boolean confirm, 
		String predEventNodeId, String valueExpr)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(predEventNodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + predEventNodeId);
			
			if (! (node instanceof PredefinedEvent)) throw new ParameterError(
				"Node with ID " + predEventNodeId + " is not a PredefinedEvent");
			
			PredefinedEvent predEvent = (PredefinedEvent)node;
			
			if (predEvent.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(predEvent.getFullName());
			
			checkIfTemplateInstance(predEvent);
			checkIfTemplate(predEvent);

			if (! confirm)
			{
				if (predEvent.getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			predEvent.setDefaultValueExpression(valueExpr);


			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(predEventNodeId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setPredefEventIsPulse(String clientId, boolean confirm,
		String predEventNodeId, boolean pulse)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			
			PersistentNode node = PersistentNodeImpl.getNode(predEventNodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + predEventNodeId);
			
			if (! (node instanceof PredefinedEvent)) throw new ParameterError(
				"Node with ID " + predEventNodeId + " is not a PredefinedEvent");
			
			PredefinedEvent predEvent = (PredefinedEvent)node;
			
			if (predEvent.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(predEvent.getFullName());
			
			checkIfTemplateInstance(predEvent);
			checkIfTemplate(predEvent);

			if (! confirm)
			{
				if (predEvent.getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			predEvent.setPulse(pulse);


			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(predEventNodeId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setNodeName(String clientId, boolean confirm, 
		String nodeId, String newName)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (newName == null) throw new ParameterError("Null name argument");
			if (newName.equals("")) throw new ParameterError("Blank name argument");

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + nodeId);
			
			if ((node instanceof ModelElement) && (! (node instanceof ModelDomain)) && (! confirm))
			{
				if (((ModelElement)node).getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
						"Simulation Runs will be invalid and will be deleted");
			}
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if (node instanceof Domain)
			{
				domains.remove((Domain)node);
				node.setNameAndNotify(newName);
				domains.put(node.getName(), (Domain)node);
			}
			else  // Note: the Node might be in some Maps or Sets that map the Node's name.
			{
				PersistentNode parent = node.getParent();
				if (parent == null) throw new ParameterError(
					"Node '" + node.getName() + " (" + node.getClass().getName() +
					", id=" + node.getNodeId() + ") has no parent");
				
				parent.renameChild(node, newName);
			}
			
			thereAreUnflushedChanges = true;
			
			// Notify clients that the Conduit was created.
			
			PeerNotice notice;
			Peer peer;
			if (node instanceof Attribute)
			{
				notice = new PeerNoticeBase.NodeChangedNotice(node.getParent().getNodeId());
				peer = ServiceContext.getServiceContext().getPeer(node.getParent());
			}
			else
			{
				notice = new PeerNoticeBase.NameChangedNotice(nodeId, node.getName(),
					node.getFullName());
				peer = ServiceContext.getServiceContext().getPeer(node);
			}
			
			try { peer.notifyAllListeners(notice); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setNodeHTMLDescription(String clientId, String nodeId,
		String newHTMLDesc)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + nodeId);
			
			node.setHTMLDescription(newHTMLDesc);
			
			
			// Notify clients that the Conduit was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(node);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(nodeId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setPortReflectivity(String clientId, boolean confirm,
		String portId, boolean black)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(portId);
			
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + portId);
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Can only call this method on a Port.");
			
			Port port = (Port)node;
			
			if (port.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(port.getFullName());
			
			checkIfTemplateInstance(port);
			checkIfTemplate(port);

			if (! confirm)
			{
				if (((ModelElement)node).getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
			}
			
			
			// Set the Port's reflectivity.
			
			port.setBlack(black);
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Port was changed.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(port);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(portId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void setPortDirection(String clientId, boolean confirm, 
		String portId, PortDirectionType dir)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		ModelContainsError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(portId);
			
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + portId);
			
			if (! (node instanceof Port)) throw new ParameterError(
				"Can only call this method on a Port.");
			
			Port port = (Port)node;
			
			if (port.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(port.getFullName());
			
			checkIfTemplateInstance(port);
			checkIfTemplate(port);

			if (! confirm)
			{
				if (((ModelElement)node).getModelDomain().dependentSimulationRunsExist())
					throw new Warning(
					"Simulation Runs will be invalid and will be deleted");
			}
			
			
			// Set the Port's direction.
			
			PortedContainer container = port.getContainer();
			if (container == null) throw new ModelContainsError(
				"Port has no parent Container");
			
			container.setPortDirection(port, dir);
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Port was changed.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(port);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(portId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ConduitSer createConduit(String clientId, boolean confirm,
		String parentId, String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Conduit conduit = createConduit_internal(confirm, parentId, portAId, portBId);
				
			thereAreUnflushedChanges = true;
				
			deleteDependentSimulationRuns(conduit.getModelDomain());
			
			
			// Notify clients that the Conduit was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(conduit.getContainer());
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeAddedNotice(parentId, conduit.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (ConduitSer)(conduit.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ConduitSer createConduit(String clientId, boolean confirm,
		String portAId, String portBId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			Conduit conduit = createConduit_internal(confirm, null, portAId, portBId);
				
			thereAreUnflushedChanges = true;
				
			deleteDependentSimulationRuns(conduit.getModelDomain());
			

			// Notify clients that the Conduit was created.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(conduit.getContainer());
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeAddedNotice(conduit.getContainer().getNodeId(),
					conduit.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
			
			return (ConduitSer)(conduit.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void rerouteConduit(String clientId, String conduitId)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(conduitId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + conduitId);
			
			if (! (node instanceof Conduit)) throw new ParameterError(
				"Can only call this method on a Conduit.");
			
			Conduit conduit = (Conduit)node;

			if (conduit.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(conduit.getFullName());

			conduit.reroute();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Conduit was modified.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(conduit);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(conduitId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void rerouteConduits(String clientId, String containerId,
		String[] conduitIds)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(containerId);
			if (node == null) throw new ParameterError(
				"Cannot identify the Node with ID " + containerId);
			
			if (! (node instanceof ModelContainer)) throw new ParameterError(
				"Can only call this method on a Model Container.");
			
			ModelContainer container = (ModelContainer)node;
			
			if (container.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(container.getFullName());
			
			
			Set<Conduit> conduits = null;
			
			if (conduitIds == null) conduits = container.getConduits();
			else
			{
				conduits = new TreeSetNullDisallowed<Conduit>();
				for (String conduitId : conduitIds)
				{
					PersistentNode cNode = PersistentNodeImpl.getNode(conduitId);
					if (cNode == null) throw new ParameterError(
						"Cannot identify the Node with ID " + conduitId);
					
					if (! (cNode instanceof Conduit)) throw new ParameterError(
						"Conduit with ID " + conduitId + " not found");
					
					Conduit conduit = (Conduit)cNode;
					
					conduits.add(conduit);
				}
			}
			
			if (conduits == null) return;
			
			for (Conduit conduit : conduits)  // each Conduit
			{
				if (conduit.getContainer() != container) throw new ParameterError(
					"Conduit " + conduit.getFullName() + " is not owned by " + 
						container.getFullName());
						
				// reroute the Conduit
				
				conduit.reroute();
			}
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients that the Container was modified.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(container);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.NodeChangedNotice(containerId)); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized NodeSer getNode(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (nodeId == null) throw new ParameterError("Node Id is null");
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("For Node Id " + nodeId);
			
			NodeSer nodeSer = node.externalize();
			if (nodeSer.getName() == null) throw new RuntimeException("Invalid NodeSer");
			if (nodeSer.getNodeId() == null) throw new RuntimeException("Invalid NodeSer");
			if (nodeSer == null) throw new RuntimeException("nodeSer is null");
			
			return nodeSer;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer getNode(String clientId, String nodeId, Class nodeSerKind)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (nodeId == null) throw new ParameterError("Node Id is null");
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("For Node Id " + nodeId);
			
			NodeSer nodeSer = node.externalize();
			if (nodeSer == null) throw new RuntimeException("nodeSer is null");
			
			if (! (nodeSerKind.isAssignableFrom(nodeSer.getClass())))
				throw new ParameterError("Node with ID " + nodeId +
					" is not a " + nodeSerKind.getName());
			
			return nodeSer;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized NodeSer getChild(String clientId, String parentId, String childName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
			if (parentNode == null) throw new ParameterError(
				"Node with Id " + parentId + " not found");
			
			return parentNode.getChild(childName).externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	/*public synchronized NodeSer updateNode(String clientId, boolean confirm, NodeSer nodeSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeSer.getNodeId());
			
			if ((node instanceof ModelElement) && (! confirm))
			{
				if (((ModelElement)node).getModelDomain().dependentSimulationRunsExist()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted. Continue?");
			}
			
			if ((node.getVersionNumber() > nodeSer.getVersionNumber()) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + nodeSer.getVersionNumber() +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			node = node.update(nodeSer);
			
			thereAreUnflushedChanges = true;
			
			if (node instanceof ModelScenario)
				deleteDependentSimulationRuns(((ModelScenario)node));
			if (node instanceof ModelElement)
				deleteDependentSimulationRuns(((ModelElement)node).getModelDomain());
			
			NodeSer newNodeSer = node.externalize();

			return newNodeSer;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}*/
	
	
	public synchronized String[] getAvailableHTTPFormats(String clientId, String nodeId,
		String[] descriptions)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + nodeId);
			return node.getAvailableHTTPFormats(descriptions);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized String getWebURLString(String clientId, String nodeId, String format)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + nodeId);
			
			return node.getWebURLString(format);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized int getHTTPPort(String clientId)
	throws
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			HTTPManager m = getHttpManager();
			if (m == null) throw new IOException("HttpManager is null");
			
			return m.getInetSocketAddress().getPort();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized ScenarioSer updateScenario(String clientId, boolean confirm, 
		ScenarioSer scenarioSer)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(scenarioSer.getNodeId());
			
			if (! (node instanceof Scenario)) throw new ParameterError(
				"Node with ID " + scenarioSer.getNodeId() + " is not a Scenario");
			
			Scenario scenario = (Scenario)node;
			
			if ((scenario instanceof ModelScenario) && (
				((ModelScenario)scenario).isLockedForSimulation()))
				throw new CannotModifyDuringSimulation(scenario.getFullName());
			
			if ((! confirm) && (scenario instanceof ModelScenario))
			{
				if (((ModelScenario)scenario).dependentSimulationRunsExist()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted. Continue?");
			}
			
			if ((scenario.getVersionNumber() > scenarioSer.getVersionNumber()) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + 
					scenarioSer.getVersionNumber() +
					" (created by client " + clientId +
					") is older than server version " + scenario.getVersionNumber() +
					" (created by client " + scenario.getClientThatLastModified() +
					" at " + (new Date(scenario.getLastUpdated())) +
					") for Node " + scenario.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			scenario.update(scenarioSer);
			
			thereAreUnflushedChanges = true;
			
			if (scenario instanceof ModelScenario)
				((ModelScenario)scenario).deleteDependentSimulationRuns();
			
			
			// Notify clients.
			
			try { node.notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(node.getNodeId())); }
			catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			
			
			ScenarioSer newScenarioSer = (ScenarioSer)(scenario.externalize());

			return newScenarioSer;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setModelScenarioTimeParams(String clientId, boolean confirm,
		String scenarioId, Date newFromDate, Date newToDate, long newDuration, int newIterLimit)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(scenarioId);
			if (node == null) throw new ElementNotFound("Cannot find Node with Id " + scenarioId);
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + scenarioId + " is not a Model Scenario");
			
			ModelScenario scenario = (ModelScenario)node;
			
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
			
			if (! confirm)
			{
				if (scenario.dependentSimulationRunsExist()) throw new Warning(
					"Simulation Runs will be invalid and will be deleted. Continue?");
			}
			
			scenario.setStartingDate(newFromDate);
			scenario.setFinalDate(newToDate);
			scenario.setDuration(newDuration);
			scenario.setIterationLimit(newIterLimit);
			
			try { scenario.notifyAllListeners(new PeerNoticeBase.ModelScenarioSimTimeChangedNotice(
				scenarioId, newFromDate, newToDate, newDuration, newIterLimit)); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized NodeSer incrementNodeSize(String clientId,
		int lastRefreshed, String nodeId,
		double dw, double dh, double childShiftX, double childShiftY,
		boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			node.setWidth(node.getWidth() + dw);
			node.setHeight(node.getHeight() + dh);
			
			if (node instanceof ModelElement)
				((ModelElement)node).shiftAllChildren(childShiftX, childShiftY);
			
			if (node instanceof ModelContainer)  // Reroute all Conduits belonging
				// to the Node as well as to the Node's parent (if any).
			{
				ModelContainer container = (ModelContainer)node;
				container.rerouteAllConduits();
				
				PersistentNode containerParent = container.getParent();
				if (containerParent != null)
					if (containerParent instanceof ModelContainer)
						((ModelContainer)containerParent).rerouteAllConduits();
			}
				
			
			node.update();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			if (notify)
				try { node.notifyAllListeners(new PeerNoticeBase.NodeResizedNotice(node.getNodeId())); }
				catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
				// This will require the client to refresh the entire Node.
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer incrementNodeLocation(String clientId, 
		int lastRefreshed, String nodeId,
		double dx, double dy, boolean notify)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			if (node instanceof Port)  // Ports have special positioning rules.
			{
				Port port = (Port)node;
				
				double newX = node.getX() + dx;
				double newY = node.getY() + dy;
				
				PortedContainer container = port.getContainer();
				
				Position newPos = container.findSideClosestToInternalPoint(newX, newY);
				
				port.setSide(newPos);
			}
			else
			{
				node.getDisplay().setX(node.getX() + dx);
				node.getDisplay().setY(node.getY() + dy);
			}

			node.update();

			thereAreUnflushedChanges = true;
			
			if (notify)
				if (node instanceof Port)
				{
					Port port = (Port)node;
					PortedContainer container = port.getContainer();
					PersistentNode containerContainer = container.getParent();
					try { node.notifyAllListeners(
						new PeerNoticeBase.NodeChangedNotice(containerContainer.getNodeId())); }
					catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
				}
				else
					try { node.notifyAllListeners(new PeerNoticeBase.NodeMovedNotice(nodeId)); }
					catch (Exception nex) { GlobalConsole.printStackTrace(nex); }

			
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized NodeSer changeNodeSize(String clientId, int lastRefreshed, 
		String nodeId,
		double newWidth, double newHeight, double childShiftX, double childShiftY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			node.setWidth(newWidth);
			node.setHeight(newHeight);
			
			if (node instanceof ModelElement)
				((ModelElement)node).shiftAllChildren(childShiftX, childShiftY);
			
			if (node instanceof ModelContainer)  // Reroute all Conduits belonging
				// to the Node as well as to the Node's parent (if any).
			{
				ModelContainer container = (ModelContainer)node;
				container.rerouteAllConduits();
				
				PersistentNode containerParent = container.getParent();
				if (containerParent != null)
					if (containerParent instanceof ModelContainer)
						((ModelContainer)containerParent).rerouteAllConduits();
			}

			node.update();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			try { node.notifyAllListeners(new PeerNoticeBase.NodeResizedNotice(node.getNodeId())); }
			catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			// This will require the client to refresh the entire Node.
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized NodeSer changeNodeLocation(String clientId,
		int lastRefreshed, String nodeId,
		double newX, double newY)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		ClientIsStale,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			if (node instanceof ModelElement)
				if (((ModelElement)node).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(((ModelElement)node).getFullName());
			
			if ((node.getVersionNumber() > lastRefreshed) 
				&& (! clientId.equals(node.getClientThatLastModified())))
			{
				ClientIsStale ex = new ClientIsStale("Client version " + lastRefreshed +
					" (created by client " + clientId +
					") is older than server version " + node.getVersionNumber() +
					" (created by client " + node.getClientThatLastModified() +
					" at " + (new Date(node.getLastUpdated())) +
					") for Node " + node.getFullName());
					
				GlobalConsole.printStackTrace(ex);
				throw ex;
			}
			
			node.getDisplay().setX(newX);
			node.getDisplay().setY(newY);
			
			node.update();
			
			thereAreUnflushedChanges = true;
			
			
			// Notify clients.
			
			try { node.notifyAllListeners(new PeerNoticeBase.NodeMovedNotice(node.getNodeId())); }
			catch (Exception nex) { GlobalConsole.printStackTrace(nex); }
			
			
			return node.externalize();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized String resolveNodePath(String clientId, String path)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			return resolveNodePath_internal(path);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String[] getChildNodeIds(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			return getChildNodeIds_internal(nodeId);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized String[] getSubcomponentNodeIds(String clientId,
		String modelContainerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			return getSubcomponenteNodeIds_internal(modelContainerId);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized AttributeSer getAttribute(String clientId, String nodeId, 
		String attrName)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + nodeId);
			if (! (node instanceof ModelElement)) throw new ParameterError(
				"The specified Node is not a Model Element, so it has no Attribute.");
			
			Attribute attribute = node.getAttribute(attrName);
			if (attribute == null) throw new ElementNotFound("Attribute " + attrName);
			
			return (AttributeSer)(attribute.externalize());
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized AttributeSer[] getAttributes(String clientId, 
		String nodeId, String className, String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + nodeId);
			
			PersistentNode node2 = PersistentNodeImpl.getNode(scenarioId);
			if (node2 == null) throw new ElementNotFound("Node with ID " + scenarioId);
			if (! (node2 instanceof Scenario)) throw new ParameterError(
				"The specified Scenario Id is not a Scenario.");
			Scenario scenario = (Scenario)node2;
			
			Class c;
			try { c = Class.forName(className); }
			catch (ClassNotFoundException ex) { throw new ParameterError(
				"Class '" + className + "' was not found"); }
			
			Domain domain = scenario.getDomain();
			
			List<Attribute> attrList = node.getAttributesInSequence();
			List<AttributeSer> attrSers = new Vector<AttributeSer>();
			
			for (Attribute attr : attrList)
			{
				Serializable value = scenario.getAttributeValue(attr);
				if (value == null) continue;
				if (c.isAssignableFrom(value.getClass()))
					attrSers.add((AttributeSer)(attr.externalize()));
			}
		
			return attrSers.toArray(new AttributeSer[attrSers.size()]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized NodeSer[] getNodesWithAttributeDeep(String clientId, 
		String nodeId, String className, String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + nodeId);
			
			PersistentNode node2 = PersistentNodeImpl.getNode(scenarioId);
			if (node2 == null) throw new ElementNotFound("Node with ID " + scenarioId);
			if (! (node2 instanceof Scenario)) throw new ParameterError(
				"The specified Scenario ID is not a Scenario.");
			
			Scenario scenario = (Scenario)node2;
			
			Class c;
			try { c = Class.forName(className); }
			catch (ClassNotFoundException ex) { throw new ParameterError(
				"Class '" + className + "' was not found"); }
			
			List<PersistentNode> nodes = new Vector<PersistentNode>();
			getNodesWithAttributeRecursive(nodes, node, c, scenario);
			
			List<NodeSer> nodeSers = new Vector<NodeSer>();
			for (PersistentNode n : nodes) nodeSers.add((NodeSer)(n.externalize()));
			
			return nodeSers.toArray(new NodeSer[nodeSers.size()]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized AttributeSer[] getAttributesDeep(String clientId, String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + nodeId);
			
			SortedSet<PersistentNode> attrs = node.getChildrenRecursive(Attribute.class);
			
			AttributeSer[] attrSers = new AttributeSer[attrs.size()];
			int i = 0;
			for (PersistentNode attr : attrs)
				attrSers[i++] = (AttributeSer)(attr.externalize());
			
			return attrSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<String> getProgress(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ServiceThread thread = threads.get(new Integer(callbackId));

			if (thread == null)  // not found.
				throw new CallbackNotFound();

			return thread.getProgress();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<String> getMessages(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			ServiceThread thread = threads.get(new Integer(callbackId));

			if (thread == null)  // not found.
				throw new CallbackNotFound();

			return thread.getMessages();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized boolean isComplete(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			return isComplete(callbackId);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized boolean isAborted(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			return isAborted(callbackId);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void abort(String clientId, int callbackId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			abort(callbackId);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized Integer simulate(String clientId,
		String modelDomainName, 
		String scenarioName, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int runs, long duration, 
		PeerListener peerListener, boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (! newSimulationsAreAllowed()) throw new CannotObtainLock(
				"Simulations are currently disabled");
			
			if (modelDomainName == null) throw new ParameterError(
				"Null modelDomainName");
		
			Domain domain = domains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound();
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain modelDomain = (ModelDomain)domain;
			
			ModelScenario modelScenario = null;
			if ((scenarioName == null) || scenarioName.equals(""))
				// use current scenario for the domain.
				modelScenario = modelDomain.getCurrentModelScenario();
			else
				modelScenario = modelDomain.getModelScenario(scenarioName);
			
			if (modelScenario == null) throw new ParameterError(
				"Unidentified scenario");
			
			
			// Determine how to treat bound States.
			
			boolean averages = modelScenario.getExternalStateBindingType();
			TimePeriodType externalStateTimeGranularity = 
				modelScenario.getExternalStateGranularity();
			
			
			// Create and start a monitoring thread, to start the simulations in
			// batches and monitor them. The monitoring thread is a ServiceThread
			// and so it automatically inherits the ServiceContext of this thread.
			
			Integer threadHandle = createMonitorThreadHandle();
			SimulationMonitorThread thread = new SimulationMonitorThread(threadHandle,
				averages, externalStateTimeGranularity,
				modelScenario, initialEpoch, finalEpoch, iterations, runs, duration,
				peerListener, repeatable);
			
			thread.setDaemon(true);
			thread.start();
			
			thereAreUnflushedChanges = true;
			
			threads.put(threadHandle, thread);
			
			return threadHandle;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSetSer createScenariosOverRange(String clientId,
		String baseScenId, String attrId, Serializable[] values)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(baseScenId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify Scenario with Id " + baseScenId);
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + baseScenId + " is not a Model Scenario");
			
			ModelScenario baseScenario = (ModelScenario)node;
			
			
			node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ElementNotFound(
				"Cannot identify Attribute with Id " + attrId);
			
			if (! (node instanceof ModelAttribute)) throw new ParameterError(
				"Node with Id " + attrId + " is not a ModelAttribute");
			
			ModelAttribute attr = (ModelAttribute)node;
			
			
			// Create a VariableScenarioSet over the range of the Attribute's allowed value.
			
			ModelScenarioSet scenarioSet = null;
			try { scenarioSet =
				baseScenario.createModelScenarioSet(null /* name */, attr, values);
			} catch (CloneNotSupportedException ex) { throw new ModelContainsError(ex); }
			
			
			// Notify

			PeerNotice notice = new PeerNoticeBase.ScenarioSetCreatedNotice(
				scenarioSet.getModelDomain().getNodeId(), scenarioSet.getNodeId());
					
			try { callbackManager.notifyAllListeners(notice); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
			
			return (ModelScenarioSetSer)(scenarioSet.externalize());
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Integer simulateScenarioSet(String clientId,
		String scenSetId, 
		Date initialEpoch, Date finalEpoch,
		int iterations, boolean step, int noOfRuns, long duration, 
		PeerListener peerListener, boolean repeatable
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(scenSetId);
			if (node == null) throw new ElementNotFound(
				"Could not identify Node with Id " + scenSetId);
			
			if (! (node instanceof ModelScenarioSet)) throw new ParameterError(
			"Node with Id " + scenSetId + " is not a Variable Model Scenario Set");
			
			ModelScenarioSet scenSet = (ModelScenarioSet)node;
			
			
			// Determine how to treat bound States.
			
			boolean averages = scenSet.getExternalStateBindingType();
			
			TimePeriodType externalStateTimeGranularity = scenSet.getExternalStateGranularity();
			
			
			// Create and start a monitoring thread, to start the simulations in
			// batches and monitor them. The monitoring thread is a ServiceThread
			// and so it automatically inherits the ServiceContext of this thread.
			
			Integer threadHandle = createMonitorThreadHandle();
			SimulationMonitorThread thread = new SimulationMonitorThread(threadHandle,
				averages, externalStateTimeGranularity,
				scenSet, initialEpoch, finalEpoch, iterations, noOfRuns, duration,
				peerListener, repeatable);
			
			thread.setDaemon(true);
			thread.start();
			
			thereAreUnflushedChanges = true;
			
			threads.put(threadHandle, thread);
			
			return threadHandle;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
		
	/** ************************************************************************
	 * This implementation is synchronous, and returns as soon as the request
	 * has completed.
	 */

	public synchronized Integer[] updateVariable(String clientId, int lastRefreshed,
		String decisionDomainName,
		String scenarioName, String decisionPointName, String variableName,
		Vector<Serializable> valueVector)
	throws
		ClientIsStale,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			/*
			 * Locate the named Variable.
			 */
	
			if (variableName == null) throw new ParameterError(
				"updateVariable: Null Variable name specified");
	
			if (valueVector.size() != 1) throw new ParameterError(
				"Parameter valueVector must contain a single element (it contains " +
				valueVector.size() + ")");
	
			Serializable value = valueVector.get(0);
	
			/*
			GlobalConsole.println(
				"Entered updateVariable(" + decisionDomainName + ", " +
					decisionPointName + ", " + variableName + ", " + value + 
					"(" + value.getClass().getName() + "))...");
			*/
	
			Variable variable = null;
	
			Domain domain = domains.get(decisionDomainName);
			if (! (domain instanceof DecisionDomain)) throw new ParameterError(
				"Domain '" + decisionDomainName + "' is not a Decision Domain");
			DecisionDomain decisionDomain = (DecisionDomain)domain;
	
			/*
			GlobalConsole.println("domainName=" + decisionDomainName);
			for (java.util.Map.Entry<String, DecisionDomain> entry : decisionDomains.entrySet())
				GlobalConsole.println(
					"Key: " + entry.getKey() + ", " +
					"Value name: " + entry.getValue().getName());
			*/
	
			if (decisionDomain == null) throw new ElementNotFound(
				"Cannot locate DecisionDomain " + decisionDomainName);
	
	
			DecisionPoint decisionPoint =
				decisionDomain.getDecisionPoint(decisionPointName);
	
			if (decisionPoint != null)
			{
				Parameter parameter = decisionPoint.getVariable(variableName);
	
				if (parameter != null)
				{
					variable = (Variable)parameter;
				}
			}
	
			if (variable == null) throw new ElementNotFound(
				decisionDomainName + ":" + decisionPointName + "." + variableName);
	
	
			/*
			 * Create a new DecisionScenario to represent (contain) the new
			 * Decisions that result. Create the new scenario by
			 * cloning the specified one, or the current one if none specified,
			 * or create a blank one if neither of these exist. Then, set the
			 * new scenario to be the current one for the DecisionDomain.
			 */
	
			DecisionScenario baseScenario = null;
			if (scenarioName != null)
			{
				// Try to find the specified Scenario.
				baseScenario = decisionDomain.getDecisionScenario(scenarioName);
			}
	
			if (baseScenario == null)
			{
				baseScenario = decisionDomain.getCurrentDecisionScenario();
			}
			
			DecisionScenario decisionScenario = null;
			try
			{
				decisionScenario = (DecisionScenario)(decisionDomain.createScenario(
					decisionDomain.getName(), baseScenario));
			}
			catch (ParameterError pe)
			{
				throw new RuntimeException(pe);
			}
			catch (CloneNotSupportedException ex)
			{
				throw new ModelContainsError("Unable to create a DecisionScenario", ex);
			}
			
			thereAreUnflushedChanges = true;
	
			decisionDomain.setCurrentDecisionScenario(decisionScenario);
	
	
			/*
			 * Define the Variable's new value as a Choice within the
			 * DecisionScenario.
			 */
	
			decisionScenario.makeChoice(variable, value);
	
	
			/*
			 * Re-compute any affected decisions in this DecisionDomain.
			 */
			
			Set<Decision> decisions = null;
			SynchronousDecisionCallback syncDecisionCallback =
				new SynchronousDecisionCallback();
	
			try
			{
				decisions = decisionScenario.getDecisionDomain().updateDecisions(
					decisionScenario, syncDecisionCallback,
					new TreeSetNullDisallowed<Variable>(Arrays.asList(variable)));
			}
			catch (ModelContainsError ex)
			{
				syncDecisionCallback.showMessage(ex.getMessage());
				throw ex;
			}
			catch (RuntimeException re)
			{
				syncDecisionCallback.showMessage(
					"RuntimeException in decision thread; aborting; " +
						ThrowableUtil.getAllMessages(re));
	
				GlobalConsole.printStackTrace(re);
				//abortThread();
				throw new InternalEngineError(re);
			}
			finally
			{
				if (decisions != null) syncDecisionCallback.addDecisions(decisions);
			}
	
	
			/*
			 * Determine if any Decisions in this DecisionDomain have changed.
			 * This is done to provide impact analysis to the client.
			 */
	
			if (decisions.isEmpty())
			{
				syncDecisionCallback.showMessage(
					"No decision changes for " + decisionDomain.getName());
			}
			else
			{
				syncDecisionCallback.showMessage(
					"New decisions for these DecisionPoints:");
	
				for (Decision decision : decisions)
				{
					DecisionPoint dp;
					try
					{
						dp = decision.getDecisionPoint();
						//syncDecisionCallback.showMessage("\t" + dp.getName());
					}
					catch (ValueNotSet w) {}
	
					Variable v;
					try
					{
						v = decision.getVariable();
						Object val = decision.getValue();
						//syncDecisionCallback.showMessage("\t\t" + v.getName() + "=" +
						//	val.toString() +
						//	"(" + val.getClass().getName() + ")");
					}
					catch (ValueNotSet w) {}
				}
			}
	
			
			/*
			 * Determine if propagation to other models is requried.
			 */
			
			Integer[] newThreadHandles = new Integer[0];
			
			
			/*
			 * Identify the set of Model Domains that have Attributes that
			 * are bound to the Variable. These are the "dependent" models.
			 */
	
			Set<ModelDomain> modelDomains = new TreeSetNullDisallowed<ModelDomain>();
			Set<VariableAttributeBinding> varAttrBindings = 
				variable.getAttributeBindings();
	
			for (VariableAttributeBinding binding : varAttrBindings)
			{
				ModelAttribute attribute;
				try { attribute = binding.getAttribute(); }
				catch (ValueNotSet w) { continue; }  // just skip it.
				ModelDomain modelDomain = attribute.getModelElement().getModelDomain();
				
				modelDomains.add(modelDomain);
			}
	
	
			/*
			 * Start a simulation for each dependent ModelDomain, and return a set
			 * of handles to these simulations.
			 */
	
	
			// Create the Thread that will compute the new element state and update
			// the DecisionDomains.
	
			newThreadHandles = new Integer[modelDomains.size()];
	
	
			// Start each simulation.
	
			int i = 0;
			for (ModelDomain modelDomain : modelDomains)
			{
				ModelScenario modelScenario = modelDomain.getCurrentModelScenario();
	
	
				//
				// Create a ModelScenario to use for the simulation.
	
				String newName = modelDomain.getName();
	
				ModelScenario newModelScenario = modelScenario;
	
				try
				{
					newModelScenario = (ModelScenario)(modelDomain.createScenario(newName, 
						modelScenario));
	
					modelDomain.setCurrentModelScenario(newModelScenario);
				}
				catch (ParameterError pe)
				{
					throw new ModelContainsError(
						"Context already contains a Scenario for " +
							modelDomain.getName());
				}
				catch (CloneNotSupportedException ex)
				{
					throw new ModelContainsError(ex);
				}
	
	
				//
				// Simulate.
				
				//newThreadHandles[i++] = 
				//	sim(newModelScenario, 
				//		propagate,
				//		decisionScenario);
			}
	
	
			// Return the simulation handles so that the client can monitor
			// and manage the simulations, and request the results when they
			// have completed.
			
			return newThreadHandles;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized List<double[]> getResultStatisticsForScenario(String clientId, 
		List<String> nodePaths,
		String modelDomainName, String modelScenarioName, String[] optionsStrings)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Parse the options.
						
			List<StatOptionsType> options = parseStatOptionString(optionsStrings);
		
	
			// Find the specified ModelScenario.
			
			Domain domain = domains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain modelDomain = (ModelDomain)domain;
			
			ModelScenario scenario = modelDomain.getModelScenario(modelScenarioName);
			if (scenario == null) throw new ElementNotFound(modelScenarioName);
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
			
	
			// Find the elements specified by the nodePaths.
		
			List<PersistentNode> nodes = new Vector<PersistentNode>();
	
			int nodeNo = 0;
			for (String nodePath : nodePaths)
			{
				PersistentNode node = modelDomain.findModelElement(nodePath);
				if (node == null) throw new ElementNotFound(nodePath + " not found");
				if (! (node instanceof State)) throw new ParameterError(
					"Node must be a State");
	
				nodes.add(node);
				nodeNo++;
			}
			
			
			// Compute the statistics for each State Node.
			
			List<double[]> statsArrayList = new Vector<double[]>();
			for (PersistentNode node : nodes)
			{
				if (! (node instanceof State)) throw new ParameterError(
					"Statistics can only be computed on a State's value");
				
				State state = (State)node;
				
				double[] stats = this.getResultStatistics_internal(state,
					scenario, options);
				
				statsArrayList.add(stats);
			}
				
	
			return statsArrayList;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<double[]> getResultStatisticsById(String clientId, 
		List<String> nodeIds,
		String distOwnerId, String[] optionsStrings)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Parse the options.
						
			List<StatOptionsType> options = parseStatOptionString(optionsStrings);
		
	
			// Identify the Model Scenario.
			
			PersistentNode node = PersistentNodeImpl.getNode(distOwnerId);
			if (node == null) throw new ElementNotFound(distOwnerId + " Id not found");
			if (! (node instanceof DistributionOwner)) throw new ParameterError(
				"Node with Id " + distOwnerId + " is not a Model Scenario or Simulation Run Set.");
			
			DistributionOwner distOwner = (DistributionOwner)node;
	
			if (distOwner.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(distOwner.getFullName());
			

			// Find the elements specified by the nodePaths.
		
			List<State> states = new Vector<State>();
	
			int nodeNo = 0;
			for (String nodeId : nodeIds)
			{
				node = PersistentNodeImpl.getNode(nodeId);
				if (node == null) throw new ElementNotFound("Node Id " + nodeId + " not found");
				if (! (node instanceof State)) throw new ParameterError(
					"Node must be a State");
	
				states.add((State)node);
				nodeNo++;
			}
			
			
			// Compute the statistics for each State Node.
			
			List<double[]> statsArrayList = new Vector<double[]>();
			for (State state : states)
			{
				double[] stats = this.getResultStatistics_internal(state,
					distOwner, options);
				
				statsArrayList.add(stats);
			}
				
	
			return statsArrayList;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized double[] getResultStatisticsById(String clientId, String nodeId,
		String distOwnerId, String[] optionsStrings)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			// Parse the options.
						
			List<StatOptionsType> options = parseStatOptionString(optionsStrings);
		
	
			// Find the specified ModelScenario.
			
			PersistentNode node = PersistentNodeImpl.getNode(distOwnerId);
			if (node == null) throw new ElementNotFound(distOwnerId + ": Id not found");
			if (! (node instanceof DistributionOwner)) throw new ParameterError(
				"Node with Id " + distOwnerId + " is not a Model Scenario or a Sim Run Set.");
			
			DistributionOwner distOwner = (DistributionOwner)node;
	
			if (distOwner.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(distOwner.getFullName());
			
			node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound(nodeId + ": Id not found");
			if (! (node instanceof State)) throw new ParameterError(
				"Statistics can only be computed on a State's value");
				
			State state = (State)node;
			
			double[] stats = this.getResultStatistics_internal(state,
				distOwner, options);
	
			return stats;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized double[] computeAssurances(String clientId, String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(scenSetId);
			if (node == null) throw new ElementNotFound(
				"Could not identify Node with Id " + scenSetId);
			
			if (! (node instanceof ModelScenarioSet)) throw new ParameterError(
			"Node with Id " + scenSetId + " is not a Variable Model Scenario Set");
			
			ModelScenarioSet scenSet = (ModelScenarioSet)node;
			
			if (scenSet.isLockedForSimulation()) 
				throw new CannotObtainLock(scenSet.getFullName());
			
			return scenSet.computeAssurances();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	/** Note: This method is not currently used. */
	
	public synchronized List<int[]> getHistograms(String clientId, 
		List<String> statePaths,
		String modelDomainName, String modelScenarioName, double bucketSize,
		double bucketStart, int noOfBuckets, boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);


			// Find the specified ModelScenario.
			
			Domain domain = domains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain modelDomain = (ModelDomain)domain;
			
			ModelScenario scenario = modelDomain.getModelScenario(modelScenarioName);
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
	
			// Identify the states.
			
			State[] states = new State[statePaths.size()];
			int noOfStates = 0;
			for (String statePath : statePaths)
			{
				ModelElement me = modelDomain.findModelElement(statePath);
				if (me == null) throw new ElementNotFound(
					"Specified state " + statePath + " not found");
	
				try { states[noOfStates++] = (State)me; }
				catch (ClassCastException cce) { throw new ElementNotFound(
					"Specified state " + statePath + " not found"); }
			}
			
			
			// Tally the histograms.
			
			List<int[]> histograms = new Vector<int[]>();
			
			for (State state : states) // each State
			{
				int[] histogram = getHistogram_internal(state, scenario, bucketSize,
					bucketStart, noOfBuckets, allValues);
				
				histograms.add(histogram);
			}
			
			return histograms;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized int[] getHistogramById(String clientId, String stateId,
		String distOwnerId, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(distOwnerId);
			if (! (node instanceof DistributionOwner)) throw new ParameterError(
				"Node " + distOwnerId + " is not a Model Scenario or a Sim Run Set.");
			
			DistributionOwner distOwner = (DistributionOwner)node;
			
			if (distOwner.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(distOwner.getFullName());
				
			node = PersistentNodeImpl.getNode(stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Node " + stateId + " is not a State");
			
			State state = (State)node;
			
			int[] histogram = getHistogram_internal(state, distOwner, bucketSize,
				bucketStart, noOfBuckets, allValues);
				
			return histogram;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized HistogramDomainParameters defineOptimalHistogramById(
		String clientId, String stateId, String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError, // thrown if a value falls outside all histrogram buckets.
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound("State Id " + stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Id " + stateId + " is not a State");
			
			State state = (State)node;
			
			node = PersistentNodeImpl.getNode(distOwnerId);
			if (node == null) throw new ElementNotFound("Id " + distOwnerId);
			if (! (node instanceof DistributionOwner)) throw new ParameterError(
				"Id " + distOwnerId + " is not a Model Scenario or a Sim Run Set.");
			
			DistributionOwner distOwner = (DistributionOwner)node;
			
			if (distOwner.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(distOwner.getFullName());
				
			
			// Get the final value for each simulation of the State and Scenario.
			
			Serializable[] values = distOwner.getStateFinalValues(state);
			
			
			// Determine the width of the domain of values.
			
			int noOfValues = values.length;
			double minValue = 0.0;
			double maxValue = 0.0;
			for (int i = 0; i < noOfValues; i++)
			{
				Serializable v = values[i];
				if (! (v instanceof Number)) throw new ParameterError(
					"Numeric graph cannot be made of this State, which has value " +
						(v == null ? "null" : v.toString()) + " in simulation " + i + 
							" (from 0) of Scenario " + distOwner.getModelScenario().getName());
				
				Number value = (Number)v;
				double doubleValue = value.doubleValue();
				
				if (i == 0) { minValue = doubleValue; maxValue = doubleValue; }
				else
				{
					minValue = Math.min(minValue, doubleValue);
					maxValue = Math.max(maxValue, doubleValue);
				}
			}
			
			double domainWidth = maxValue - minValue;
			
			
			// Select a bucket size such that there are at least a few hits in most
			// buckets, and there are at least five buckets.
			
			int noOfBuckets = noOfValues / 3;  // 3 is "a few"
			if (noOfBuckets < 5) // increase the number of buckets so that there
				noOfBuckets = 5;		// are at least 5 buckets.
			
			double bucketSize = domainWidth / noOfBuckets;
			
			
			// Address the case in which the values are clustered such that the
			// bucketSize is zero.
			
			if (bucketSize == 0.0)
			{
				// Arbitrarily choose a bucket size that is equal to the minValue
				// divided by the number of buckets.
				
				bucketSize = minValue / noOfBuckets;
			}
			
			noOfBuckets++;  // because we will shift them all left, to center them
				// and in the process the right-most values are not in a bucket.
			
			
			// Create container for returning results.
			
			HistogramDomainParameters histoParams = new HistogramDomainParameters();
			
			histoParams.bucketSize = bucketSize;
			histoParams.bucketStart = minValue - bucketSize/2.0;
			histoParams.noOfBuckets = noOfBuckets;
	
			return histoParams;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized TimeSeriesParameters defineOptimalValueHistoryById(
		String clientId, String stateId, String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound("State Id " + stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Id " + stateId + " is not a State");
			
			State state = (State)node;
			
			node = PersistentNodeImpl.getNode(distOwnerId);
			if (node == null) throw new ElementNotFound("Id " + distOwnerId);
			if (! (node instanceof DistributionOwner)) throw new ParameterError(
				"Id " + distOwnerId + " is not a Model Scenario or a Sim Run Set.");
			
			DistributionOwner distOwner = (DistributionOwner)node;
			
			if (distOwner.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(distOwner.getFullName());
				
			
			// Get the Event history for each simulation of the State and Scenario.
			
			List<SimulationRun> simRuns = 
					new Vector<SimulationRun>(distOwner.getSimulationRuns());
			int noOfSimRuns = simRuns.size();
			
			// Values to be determined.
			long minTimeDelta = 0;
			//long maxDuration = 0;
			//long earliestStartTime = 0;
			long latestStartTime = 0;
			long earliestEndTime = 0;
			
			// Working variables.
			int simRunNo = 0;
			long time = 0;
			long priorTime = 0;
			boolean firstTimeThrough = true;
			
			for (SimulationRun simRun : simRuns)
			{
				//maxDuration = Math.max(maxDuration, simRun.getDuration());
				
				latestStartTime = Math.max(latestStartTime, simRun.getActualStartTime());
				
				if (firstTimeThrough) earliestEndTime = simRun.getActualEndTime();
				else earliestEndTime = Math.min(earliestEndTime, simRun.getActualEndTime());
				
				/*GlobalConsole.println("Simulation " + simRun.getName() + " is from " +
					(new Date(simRun.getActualStartTime())) + " to " + (new Date(simRun.getActualEndTime())));
					
					GlobalConsole.println("\tlatestStartTime=" + latestStartTime);
					GlobalConsole.println("\tearliestEndTime=" + earliestEndTime);*/
				
				
				// Determine the minimum time delta between any Events.
				
				List<SimulationTimeEvent> eventsForSimRun = simRun.getEvents(state);
				for (SimulationTimeEvent event : eventsForSimRun)
				{
					/*
					Serializable oValue = event.getNewValue();
					if (! (oValue instanceof Number)) throw new ParameterError(
						"Value '" + oValue + "' must be a Number in order to display it");
					
					double value = ((Number)oValue).doubleValue();
					*/
					
					time = event.getEpoch().getTime();
					
					long timeDelta = time - priorTime;
					if (timeDelta != 0)  // ignore zero deltas
						if (minTimeDelta == 0) minTimeDelta = timeDelta;  // capture first delta
						else if (firstTimeThrough) minTimeDelta = time - priorTime;
							else minTimeDelta = Math.min(minTimeDelta, time - priorTime);
					
					
					//if (firstTimeThrough) earliestStartTime = time;
					//else earliestStartTime = Math.min(earliestStartTime, time);
					
					priorTime = time;
					firstTimeThrough = false;
				}
			}
			
			
			long duration = earliestEndTime - latestStartTime;		
			
			if (duration < 0) throw new ParameterError(
				"No time period could be found that is spanned by every simulation");
			
			if (duration == 0) throw new ParameterError(
				"The time span shared by all simulations is of zero duration");
			
			
			// Determine the granularity of time, based on the shortest interval over
			// which the State changes in any simulation.
			
			if (minTimeDelta == 0) throw new ParameterError(
				"Elapsed time for all simulations is 0; cannot compute time-based averages");
			
			double d = minTimeDelta * DateAndTimeUtils.MsInADay;
			if (d > (double)Long.MAX_VALUE) throw new ParameterError(
				"Time interval of " + d + "ms exceeds allowed range of " + Long.MAX_VALUE);
			
			long shortestChangeInterval = (long)d;
			
			if (shortestChangeInterval == 0) throw new ParameterError(
				"Events do not span any time");
			
			long noOfPeriods = duration / shortestChangeInterval;
			noOfPeriods = Math.min(30, noOfPeriods);  // Prevent an intractable
															// number of periods.
			noOfPeriods = Math.max(5, noOfPeriods);
			long periodDuration = duration / noOfPeriods;
			
			
			// Create container for returning results.
			
			TimeSeriesParameters params = new TimeSeriesParameters();
			params.periodDuration = periodDuration;
			params.startTime = latestStartTime;
			params.noOfPeriods = (int)noOfPeriods;
			
			return params;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized double[][] getExpectedValueHistory(String clientId, String stateId, 
		String distOwnerId, Date startingEpoch, int noOfPeriods, long periodInMs)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if (periodInMs <= 0) throw new ParameterError("Period must be positive");
			
			PersistentNode node = PersistentNodeImpl.getNode(distOwnerId);
			if (node == null) throw new ElementNotFound("Node Id " + distOwnerId);
			if (! (node instanceof DistributionOwner)) throw new ParameterError(
				"Node " + distOwnerId + " is not a Model Scenario or a Sim Run Set: " +
				"it is a " + node.getClass().getName());
			
			DistributionOwner distOwner = (DistributionOwner)node;
			
			node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ElementNotFound("Node Id " + stateId);
			if (! (node instanceof State)) throw new ParameterError(
				"Node " + stateId + " is not a State");
			
			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
				
			double[][] result = new double[noOfPeriods][];
			
			List<SimulationRun> simRuns = 
					new Vector<SimulationRun>(distOwner.getSimulationRuns());
			if (simRuns.size() == 0) throw new ParameterError("No simulation runs");
			
			long startTime = startingEpoch.getTime();
			if (periodInMs == Long.MAX_VALUE) throw new ParameterError(
				"Period exceeds maximum that can be supported by the long data type");
			
			for (int p = 0; p < noOfPeriods; p++)
			{
				long endTime = startTime + periodInMs;
				
				double[] timeAveragedValues = new double[simRuns.size()];
				double lowestValue = 0.0;
				double highestValue = 0.0;
				
				double sumOfTimeAveragedValues = 0.0;
				int simRunNo = 0;
				for (SimulationRun simRun : simRuns)
				{
					// Identify any changes to the State's value that occur within the
					// period. Based on these Events, compute the time-averaged value of
					// the State for the period in the Scenario.
					
					double timeAveragedValue;
					try { timeAveragedValue = simRun.computeTimeAveragedValue(state,
						startTime, endTime); }
					catch (UndefinedValue uv) { throw new ParameterError(uv); }
						
					timeAveragedValues[simRunNo] = timeAveragedValue;
					sumOfTimeAveragedValues += timeAveragedValue;
	
					if (simRunNo == 0)
					{
						lowestValue = timeAveragedValue;
						highestValue = timeAveragedValue;
					}
					else
					{
						lowestValue = Math.min(lowestValue, timeAveragedValue);
						highestValue = Math.max(highestValue, timeAveragedValue);
					}
					
					simRunNo++;
				}
				
				double mean = sumOfTimeAveragedValues / timeAveragedValues.length;
				double median = timeAveragedValues[timeAveragedValues.length/2];
				
				double variance = 0.0;
				for (double timeAveragedValue : timeAveragedValues)
				{
					double delta = timeAveragedValue - mean;
					variance += delta * delta;
				}
				
				variance /= timeAveragedValues.length;
				double sigma = Math.sqrt(variance);
				
				
				// Compute the distribution of the average values.
				
				// Basic approach (tabulate percentiles):
				// Order the simRuns by time-averaged-value, lowest first.
				// Starting with the lowest value, sum the runs that produce that
				// value or lower, until the number of runs is one standard deviation
				// from the median, based on a normal distribution. (This would be
				// where 34.1% of the values are below the median. Two sds would be 
				// where (34.1+13.6)% of the values are below the median. Three sds
				// would be where (2.1+34.1+13.6)% of the values are below the median.)
				// Continue on to find the values for which the samples are one, two,
				// and three sds above the median. (See
				// http://en.wikipedia.org/wiki/File:Standard_deviation_diagram.svg
				// for a diagram.)
				
				int noBelow15pt9Pct = 0;
				int noBelow84pt1Pct = 0;
				double countAt15pt9Percentile = 15.9 * timeAveragedValues.length;
				double countAt84pt1Percentile = 84.1 * timeAveragedValues.length;
				double percentile15pt9 = timeAveragedValues[0];
				double percentile84pt1 = timeAveragedValues[0];
				int count = 0;
				for (double timeAveragedValue : timeAveragedValues)
				{
					count++;
					if (count < countAt15pt9Percentile) percentile15pt9 = timeAveragedValue;
					if (count < countAt84pt1Percentile) percentile84pt1 = timeAveragedValue;
				}
				
				
				// Advanced approach that does not require a normal distribution
				// of mean values and that produces a piece-wise continuous function
				// that can be used in a mathematical optimization algorithm:
				// First create a uniform "clamped" interpolating spline in which 
				// one polynomial is used for each period. (See CubicSpline.html
				// in Sources.)
				/*
				...
				PolynomialSplineFunction spline = ..
				PolynomialFunction[] polynomials = spline.getPolynomials();
				*/
				
				
				// Compute statistics, using the distribution.
				
				double[] stats = new double[7];
				
				stats[0] = percentile15pt9;
				stats[1] = mean;
				stats[2] = percentile84pt1;
				stats[3] = lowestValue;
				stats[4] = highestValue;
				stats[5] = sigma;
				stats[6] = median;
				
	
				result[p] = stats;
				
				startTime = endTime;
			}
			
			return result;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	/** Note: This method is not currently used. */
	
	public synchronized double[] getCorrelations(String clientId, List<String[]> statePaths,
		String modelDomainName, String modelScenarioName, String optionString)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if ((statePaths == null) || (statePaths.size() == 0)) throw new ParameterError(
				"No state paths");
			
			StatCorrelationType option = StatCorrelationType.cc;
			if (optionString == null) option = StatCorrelationType.cc;
			else if (optionString.equals("cv")) option = StatCorrelationType.cv;
			else if (optionString.equals("cc")) option = StatCorrelationType.cc;
			else throw new ParameterError("Option must be one of cc or cv");
			
			// Find the specified ModelScenario.
			
			Domain domain = domains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain modelDomain = (ModelDomain)domain;
			
			ModelScenario scenario = modelDomain.getModelScenario(modelScenarioName);
	
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
			// Define containers for the States that are identified by the
			// state paths.
			
			State[] states1 = new State[statePaths.size()];
			State[] states2 = new State[statePaths.size()];
	
			
			// Iterate over each state pair, and compute their correlation.
			
			double[] results = new double[statePaths.size()];
			int i = 0;
			for (String[] pair : statePaths)  // each state path pair i
			{
				if (pair.length != 2) throw new InternalEngineError(
					"The length of a state path pair is " + pair.length);
				
				i++;
				
				ModelElement me0 = modelDomain.findModelElement(pair[0]);
				if (me0 == null) throw new ElementNotFound("States " + pair[0] + " not found");
	
				ModelElement me1 = modelDomain.findModelElement(pair[1]);
				if (me1 == null) throw new ElementNotFound("States " + pair[1] + " not found");
	
				try
				{
					states1[i-1] = (State)me0;
					states2[i-1] = (State)me1;
				}
				catch (ClassCastException cce) { throw new ElementNotFound(
					"Specified states not found"); }
				
				SimpleRegression regression = new SimpleRegression();
				
				List<SimulationRun> runs = 
					new Vector<SimulationRun>(scenario.getSimulationRuns());
	
				Serializable[] node1Histories = null;
				Serializable[] node2Histories = null;
				if (option.equals(StatCorrelationType.cv))
				{
					node1Histories = new Serializable[runs.size()];
					node2Histories = new Serializable[runs.size()];
				}
				
				int r = 0;
				for (SimulationRun run : runs)  // each run in the scenario
				{
					r++;
					Serializable path1Value = run.getStateFinalValue(states1[i-1]);
					Serializable path2Value = run.getStateFinalValue(states2[i-1]);
					
					try
					{
						double path1Double = ((Number)path1Value).doubleValue();
						double path2Double = ((Number)path2Value).doubleValue();
						regression.addData(path1Double, path2Double);
					}
					catch (ClassCastException cce) { throw new ParameterError(
						"The specified states have non-numeric values", cce); }
					
					if (option.equals(StatCorrelationType.cv))
					{
						node1Histories[r-1] = path1Value;
						node2Histories[r-1] = path2Value;
					}
				}
					
				if (option.equals(StatCorrelationType.cc))
				{
					results[i-1] = regression.getR();
				}
				else if (option.equals(StatCorrelationType.cv))
				{
					double sigma1 = computeStatistic(StatOptionsType.sd, node1Histories);
					double sigma2 = computeStatistic(StatOptionsType.sd, node2Histories);
					results[i-1] = regression.getR() * sigma1 * sigma2;
				}
			}
			
			return results;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	/** Note: This method is not currently used. */
	
	public synchronized List<String> getSimulationRuns(String clientId, String modelDomainName,
		String modelScenarioName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			// Identify the Model Domain.

			Domain domain = domains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain modelDomain = (ModelDomain)domain;
				
			// Identify the scenario.
			
			ModelScenario modelScenario =
				modelDomain.getModelScenario(modelScenarioName);
						
			if (modelScenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(modelScenario.getFullName());
				
			List<SimulationRun> runs = 
				new Vector<SimulationRun>(modelScenario.getSimulationRuns());
			List<String> runNames = new Vector<String>();
			
			for (SimulationRun run : runs)
			{
				runNames.add(run.getName());
			}
			
			return runNames;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	/** Note: This method is not currently used. */
	
	public synchronized List<SimulationRunSer> getSimulationRuns(String clientId, 
		ModelScenarioSer s)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			if (s == null) throw new ParameterError("Scenario is null");
			String domainId = s.domainId;
			if (domainId == null) throw new ParameterError(
				"Scenario domain Id is null");
			
			PersistentNode n = PersistentNodeImpl.getNode(domainId);
			if (n == null) throw new ParameterError(
				"ModelDomain (" + domainId + ") for scenario not found");
			
			if (! (n instanceof ModelDomain)) throw new ParameterError(
				"Id of Scenario's ModelDomain is not a ModelDomain");
			
			ModelDomain modelDomain = (ModelDomain)n;
			
			// Identify the scenario.
			
			String modelScenarioName = s.getName();
			if (s == null) throw new ParameterError("Scenario name is null");
			
			ModelScenario modelScenario =
				modelDomain.getModelScenario(modelScenarioName);
				
			if (modelScenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(modelScenario.getFullName());
				
			List<SimulationRun> runs = 
				new Vector<SimulationRun>(modelScenario.getSimulationRuns());
			Vector<NodeSer> eruns = new Vector<NodeSer>();
			for (SimulationRun r : runs) eruns.add(r.externalize());
			return (List)eruns;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<SimulationRunSer> getSimulationRuns(String clientId, 
		String distOwnerId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(distOwnerId);
			if (n == null) throw new ParameterError(
				"Id " + distOwnerId + " not found");
			
			if (! (n instanceof DistributionOwner)) throw new ParameterError(
				"Id is not a ModelScenario or a Sim Run Set: it is a " +
				n.getClass().getName());
			
			DistributionOwner distOwner = (DistributionOwner)n;
			
			if (distOwner.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(distOwner.getFullName());
				
			List<SimulationRun> runs = 
				new Vector<SimulationRun>(distOwner.getSimulationRuns());
			Vector<NodeSer> eruns = new Vector<NodeSer>();
			for (SimulationRun r : runs) eruns.add(r.externalize());
			return (List)eruns;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSer[] getScenariosForScenarioSetId(String clientId,
		String scenSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(scenSetId);
			if (n == null) throw new ParameterError(
				"Scenario Set with ID " + scenSetId + " not found");
			
			if (! (n instanceof ModelScenarioSet)) throw new ParameterError(
				"Id of ScenarioSet is not a ModelScenarioSet");
			
			ModelScenarioSet scenSet = (ModelScenarioSet)n;
			
			//if (scenSet.isLockedForSimulation()) 
			//	throw new CannotModifyDuringSimulation(scenSet.getFullName());
				
			List<ModelScenario> scenarios = scenSet.getModelScenarios();
			
			ModelScenarioSer[] scenSers = new ModelScenarioSer[scenarios.size()];
			int i = 0;
			for (ModelScenario scenario : scenarios)
			{
				scenSers[i++] = (ModelScenarioSer)(scenario.externalize());
			}
			
			return scenSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized SimulationRunSer[] getSimulationRunsForSimRunSetId(String clientId,
		String simRunSetId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(simRunSetId);
			if (n == null) throw new ParameterError(
				"Simulation Run Set with ID " + simRunSetId + " not found");
			
			if (! (n instanceof SimRunSet)) throw new ParameterError(
				"Id is not a Simulation Run Set");
			
			SimRunSet simRunSet = (SimRunSet)n;
			
			if (simRunSet.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRunSet.getFullName());
				
			SortedSet<SimulationRun> simRuns = simRunSet.getSimulationRuns();
			
			SimulationRunSer[] simRunSers = new SimulationRunSer[simRuns.size()];
			int i = 0;
			for (SimulationRun simRun : simRuns)
			{
				simRunSers[i++] = (SimulationRunSer)(simRun.externalize());
			}
			
			return simRunSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized SimRunSetSer[] getSimRunSetsForScenarioId(String clientId, String scenarioId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(scenarioId);
			if (n == null) throw new ParameterError(
				"ModelScenario with ID " + scenarioId + " not found");
			
			if (! (n instanceof ModelScenario)) throw new ParameterError(
				"Id is not a ModelScenario");
			
			ModelScenario scenario = (ModelScenario)n;
			
			if (scenario.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(scenario.getFullName());
				
			SortedSet<SimRunSet> simRunSets = scenario.getSimRunSets();
			
			SimRunSetSer[] simRunSetSers = new SimRunSetSer[simRunSets.size()];
			int i = 0;
			for (SimRunSet simRunSet : simRunSets)
			{
				simRunSetSers[i++] = (SimRunSetSer)(simRunSet.externalize());
			}
			
			return simRunSetSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized List<GeneratedEventSer> getEventsForSimNodeForIteration(
		String clientId, String simRunId, int iteration)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(simRunId);
			if (node == null) throw new ParameterError(
				"Could not identify Node with ID " + simRunId);
			
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with Id " + simRunId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			SortedEventSet<GeneratedEvent> events = 
				simRun.getGeneratedEventsAtIteration(iteration);
			
			
			List<GeneratedEventSer> eventSers = new Vector<GeneratedEventSer>();
			for (GeneratedEvent event : events)
				eventSers.add((GeneratedEventSer)(event.externalize()));
			
			return eventSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<GeneratedEventSer>[] getEventsByEpoch(String clientId, 
		String modelDomainName,
		String modelScenarioName,
		String simRunName, List<String> paths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			SimulationRun simRun = getSimulationRunByName(modelDomainName,
				modelScenarioName, simRunName);
				
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			int noOfEpochs = simRun.getEpochs().size();
			
			List[][] eventSersHolder = { null };
			
			getEventsByEpoch_internal(modelDomainName, modelScenarioName, simRunName,
				paths, 
				null,  // no filter
				null,  // null if filter is null
				null,  // null if filter is null
				true,
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			return (List<GeneratedEventSer>[])(eventSersHolder[0]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String clientId, 
		String simNodeId, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simNodeId);
			
			if (node == null) throw new ElementNotFound("Node " + simNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simNodeId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List[][] eventSersHolder = { null };
			
			getEventsByEpoch_internal(simRun.getModelScenario().getModelDomain().getName(),
				simRun.getModelScenario().getName(), simRun.getName(),
				statePaths,
				null,  // no filter
				null,  // null if filter is null
				null,  // null if filter is null
				true,  // return EventSers
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			return (List<GeneratedEventSer>[])(eventSersHolder[0]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String clientId, 
		String simNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simNodeId);
			
			if (node == null) throw new ElementNotFound("Node " + simNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simNodeId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
						
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List[][] eventSersHolder = { null };
			
			List<State> states = 
				simRun.getModelScenario().getModelDomain().getStatesRecursive();
			
			List<String> statePaths = new Vector<String>();
			for (State state : states) statePaths.add(state.getFullName());
			
			List[] filteredPathsHolder = { null };

			getEventsByEpoch_internal(simRun.getModelScenario().getModelDomain().getName(),
				simRun.getModelScenario().getName(), simRun.getName(),
				statePaths,
				null,  // no filter
				null,  // null if filter is null
				null,  // null if filter is null
				true,  // return EventSers
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			return (List<GeneratedEventSer>[])(eventSersHolder[0]);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized List<GeneratedEventSer>[] getEventsForSimNodeByEpoch(String clientId, 
		String simNodeId, List<String> statePaths, Class[] stateTags, 
		List<String>[] filteredPathsHolder, List<String>[] filteredIdsHolder
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simNodeId);
			
			if (node == null) throw new ElementNotFound("Node " + simNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simNodeId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
						
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List[][] eventSersHolder = { null };
			
			getEventsByEpoch_internal(simRun.getModelScenario().getModelDomain().getName(),
				simRun.getModelScenario().getName(), simRun.getName(),
				statePaths,
				stateTags,  // filter
				filteredPathsHolder,
				filteredIdsHolder,
				true,  // return EventSers
				eventSersHolder  // a var-args - passed by reference.
				);
			
			if (filteredPathsHolder[0] == null) throw new RuntimeException(
				"filteredPathsHolder[0] is null");
			
			if (eventSersHolder[0] == null) throw new RuntimeException("Element 0 is null");
			
			return (List<GeneratedEventSer>[])(eventSersHolder[0]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized StateSer[] getStatesRecursive(String clientId, String modelContainerNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(modelContainerNodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + 
				modelContainerNodeId + " not found");
			
			if (! (node instanceof ModelContainer)) throw new ParameterError(
				"Node with ID " + modelContainerNodeId + " is not a Model Container");
			
			ModelContainer container = (ModelContainer)node;
			
			if (container.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(container.getFullName());
				
			List<State> states = container.getStatesRecursive();
			
			StateSer[] stateSers = new StateSer[states.size()];
			int i = 0;
			for (State state : states) stateSers[i++] = (StateSer)(state.externalize());
			
			return stateSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized String getEventsByEpochAsHTML(String clientId, String simRunNodeId, 
		List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simRunNodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + simRunNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simRunNodeId + " is not a Simulation Run");
			
			SimulationRun simRun = (SimulationRun)node;

			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List<GeneratedEvent>[] eventListArray = getEventsByEpoch_internal(
				simRun.getModelScenario().getModelDomain().getName(), 
				simRun.getModelScenario().getName(), simRun.getName(), statePaths,
				null, null, null);
				
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			
			
			// Resolve State names
			
			List<String> stateIds = new Vector<String>();
			
			ModelDomain domain = simRun.getModelDomain();
			for (String pathString : statePaths)
			{
				ModelElement elt = domain.findModelElement(pathString);
				
				if (! (elt instanceof State)) throw new ParameterError(
					"Element " + pathString + " is not a State");
				
				stateIds.add(elt.getNodeId());
			}
			
			
			EventWriter.writeEvents(EventWriter.FormatType.html, simRun.getName(), statePaths,
				stateIds, eventListArray, pw);
			
			pw.flush();
			return sw.toString();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized String getEventsByEpochAsHTML(String clientId, String simRunNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(simRunNodeId);
			if (node == null) throw new ElementNotFound("Node with ID " + simRunNodeId);
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with ID " + simRunNodeId + " is not a Simulation Run");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			
			// Identify all of the States in the model. Ignore 
			
			List<State> states = 
				simRun.getModelScenario().getModelDomain().getStatesRecursive();
			
			List<String> statePaths = new Vector<String>();
			List<String> stateIds = new Vector<String>();
			for (State state : states)
			{
				statePaths.add(state.getFullName());
				stateIds.add(state.getNodeId());
			}
			
			List<GeneratedEvent>[] eventListArray = getEventsByEpoch_internal(
				simRun.getModelScenario().getModelDomain().getName(), 
				simRun.getModelScenario().getName(), simRun.getName(), statePaths,
				null, null, null);
				
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			
			EventWriter.writeEvents(EventWriter.FormatType.html, simRun.getName(), 
				statePaths, stateIds, eventListArray, pw);
			
			pw.flush();
			return sw.toString();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Serializable getAttributeValue(String clientId, String attrPath, 
		String scenarioName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(attrPath);
			if (node == null) throw new ParameterError("Not found: " + attrPath);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrPath);
			
			Attribute attribute = (Attribute)node;
			Domain domain = attribute.getDomain();
			Scenario scenario = null;
			try { scenario = domain.getScenario(scenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(
				"Not found: " + scenarioName); }
			
			return scenario.getAttributeValue(attribute);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized Serializable getAttributeValueForId(String clientId, 
		String attrId, String scenarioName)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ParameterError("Not found: " + attrId);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrId);
			
			Attribute attribute = (Attribute)node;
			
			Domain domain = attribute.getDomain();
			
			Scenario scenario = null;
			try { scenario = domain.getScenario(scenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(
				"Not found: " + scenarioName); }
			
			return scenario.getAttributeValue(attribute);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Serializable getAttributeValueById(String clientId, 
		String attrId, String scenarioId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ParameterError("Not found: " + attrId);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrId);
			
			Attribute attribute = (Attribute)node;
			
			node = PersistentNodeImpl.getNode(scenarioId);
			if (node == null) throw new ParameterError("Not found: " + scenarioId);
			
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Not a ModelScenario: " + scenarioId);
			
			ModelScenario modelScenario = (ModelScenario)node;
			
			return modelScenario.getAttributeValue(attribute);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Serializable[] getAttributeValuesForNode(String clientId,
		String nodeId, String scenarioId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Not found: " + nodeId);
			
			PersistentNode node2 = PersistentNodeImpl.getNode(scenarioId);
			if (node2 == null) throw new ParameterError("Scenario not found: " + scenarioId);
			if (! (node2 instanceof Scenario)) throw new ParameterError(
				"Node with Id " + scenarioId + " is not a Scenario");
			Scenario scenario = (Scenario)node2;
			
			List<Attribute> attrs = node.getAttributesInSequence();
			Serializable[] values = new Serializable[attrs.size()];
			int i = 0;
			for (Attribute attr : attrs) values[i++] = scenario.getAttributeValue(attr);
			return values;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Serializable getAttributeDefaultValueById(String clientId, String attrId)
	throws
		ParameterError,  // if the Attribute does not exist.
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ParameterError("Not found: " + attrId);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrId);
			
			Attribute attribute = (Attribute)node;
			
			return attribute.getDefaultValue();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
		
		
	public synchronized Serializable[] getAttributeDefaultValuesForNode(String clientId,
		String nodeId)
	throws
		ElementNotFound,  // if the Node does not exist.
		ParameterError,  // if the Scenario does not exist
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Not found: " + nodeId);
			
			List<Attribute> attrs = node.getAttributesInSequence();
			Serializable[] values = new Serializable[attrs.size()];
			int i = 0;
			for (Attribute attr : attrs) values[i++] = attr.getDefaultValue();
			return values;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized void setAttributeDefaultValue(String clientId, boolean confirm,
		String attrId, Serializable value)
	throws
		Warning,
		CannotObtainLock,
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ParameterError("Not found: " + attrId);
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: " + attrId);
			
			Attribute attribute = (Attribute)node;

			if (attribute.getDomain() instanceof ModelDomain)
			{
				ModelDomain md = (ModelDomain)(attribute.getDomain());
				if (md.dependentSimulationRunsExist())
				{
					if (confirm)
						deleteDependentSimulationRuns(md);
					else
						throw new Warning("Simulation Runs will be invalid and will be deleted");
				}
			}

			attribute.setDefaultValue(value);
			
			// Notify

			PeerNotice notice = new PeerNoticeBase.NonStructuralChangeNotice(attrId);
					
			try { callbackManager.notifyAllListeners(notice); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized Serializable getAttributeDefaultValue(String clientId, String attrPath)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node;
			try { node = getNodeForPath(attrPath); }
			catch (ElementNotFound ex) { throw new ParameterError(ex); }
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Not an Attribute: '" + attrPath + "'");
			
			Attribute attribute = (Attribute)node;
			
			return attribute.getDefaultValue();
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized Object[] getStateValuesForIteration(String clientId,
		String simRunId, int iteration)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			if (iteration < 0) throw new ParameterError(
				"Iteration must be 1 or greater, or 0 to specify initial values");

			PersistentNode node = PersistentNodeImpl.getNode(simRunId);
			if (node == null) throw new ParameterError(
				"Could not identify Node with ID " + simRunId);
			
			if (! (node instanceof SimulationRun)) throw new ParameterError(
				"Node with Id " + simRunId + " is not a SimulationRun");
			
			SimulationRun simRun = (SimulationRun)node;
			
			if (simRun.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(simRun.getFullName());
				
			List<State> states = simRun.getModelDomain().getStatesRecursive();
			
			List<String> stateIds = new Vector<String>();
			
			List<Serializable> values = new Vector<Serializable>();
			
			for (State state : states)
			{
				stateIds.add(state.getNodeId());
				
				if (iteration == 0)
					values.add(null);
				else
				{
					Epoch epoch = simRun.getEpochForIteration(iteration);
					Serializable value = simRun.getStateValueAtEpoch(state, epoch);
					values.add(value);
				}
			}
			
			Serializable[] valuesAr = values.toArray(new Serializable[values.size()]);
			
			Object[] result = new Object[2];
			
			result[0] = valuesAr;
			result[1] = stateIds;
			
			return result;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	

	public synchronized ScenarioSer[] getScenarios(String clientId, String domainName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Domain domain = domains.get(domainName);
			if (domain != null)
			{
				List<Scenario> scenarios = domain.getScenarios();
				ScenarioSer[] scenarioSers = new ScenarioSer[scenarios.size()];
				int scenarioNo = 0;
				for (Scenario scenario : scenarios)
					scenarioSers[scenarioNo++] = (ScenarioSer)(scenario.externalize());
				
				return scenarioSers;
			}
			
			throw new ParameterError("Domain " + domainName + " not found");
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized ScenarioSer[] getScenariosForDomainId(String clientId, 
		String domainId)
	throws
		ParameterError,  // if the Domain does not exist.
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ParameterError("Node " + domainId + " not found");
			
			if (node instanceof ModelDomain)
			{
				ModelDomain modelDomain = (ModelDomain)node;
				
				List<ModelScenario> scenarios = modelDomain.getModelScenarios();
				ScenarioSer[] scenarioSers = new ScenarioSer[scenarios.size()];
				int scenarioNo = 0;
				for (Scenario scenario : scenarios)
					scenarioSers[scenarioNo++] = (ScenarioSer)(scenario.externalize());
				
				if (scenarioSers == null) throw new RuntimeException("scenarioSers is null");
				return scenarioSers;
			}
			else if (node instanceof DecisionDomain)
			{
				DecisionDomain decisionDomain = (DecisionDomain)node;
				
				Set<DecisionScenario> scenarios = decisionDomain.getDecisionScenarios();
				ScenarioSer[] scenarioSers = new ScenarioSer[scenarios.size()];
				int scenarioNo = 0;
				for (Scenario scenario : scenarios)
					scenarioSers[scenarioNo++] = (ScenarioSer)(scenario.externalize());
				
				if (scenarioSers == null) throw new RuntimeException("scenarioSers is null");
				return scenarioSers;
			}
			else throw new ParameterError("Node " + domainId + " is not a Domain");
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized ModelScenarioSer getModelScenario(String clientId, 
		String modelDomainName, String modelScenarioName)
	throws
		ParameterError,
		ElementNotFound,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			Domain domain = domains.get(modelDomainName);
			if (domain == null) throw new ElementNotFound(modelDomainName);
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + modelDomainName + "' is not a Model Domain");
			ModelDomain modelDomain = (ModelDomain)domain;
			
			ModelScenario scenario;
			try { scenario = modelDomain.getModelScenario(modelScenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(enf); }
			
			return (ModelScenarioSer)(scenario.externalize());
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized ModelScenarioSetSer[] getScenarioSetsForDomainId(
		String clientId, String domainId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(domainId);
			if (node == null) throw new ParameterError("Node " + domainId + " not found");
			
			if (! (node instanceof ModelDomain)) throw new ParameterError(
				"Node with ID " + domainId + " is not a ModelDomain");
			
			ModelDomain domain = (ModelDomain)node;
		
			List<ModelScenarioSet> scenarioSets = domain.getModelScenarioSets();
			
			ModelScenarioSetSer[] scenSetSers = new ModelScenarioSetSer[scenarioSets.size()];
			int i = 0;
			for (ModelScenarioSet scenSet : scenarioSets)
			{
				scenSetSers[i++] = (ModelScenarioSetSer)(scenSet.externalize());
			}
			
			return scenSetSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	public synchronized AttributeSer[] getAttributes(String clientId, String nodePath)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(nodePath);
			if (node == null) throw new ParameterError("Not found: " + nodePath);
			
			Set<Attribute> attributes = node.getAttributes();
			
			AttributeSer[] attributeSers = new AttributeSer[attributes.size()];
			int attributeNo = 0;
			for (Attribute attribute : attributes)
				attributeSers[attributeNo++] = (AttributeSer)(attribute.externalize());
			return attributeSers;
			
			//return attributes.toArray(new Attribute[0]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized AttributeSer[] getAttributesForId(String clientId, 
		String nodeId)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			List<Attribute> attributes = node.getAttributesInSequence();
			
			AttributeSer[] attributeSers = new AttributeSer[attributes.size()];
			int attributeNo = 0;
			for (Attribute attribute : attributes)
				attributeSers[attributeNo++] = (AttributeSer)(attribute.externalize());
			return attributeSers;
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized boolean isGeneratorRepeating(String clientId, String genNodeId)
	throws
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(genNodeId);
			if (! (node instanceof Generator)) throw new ParameterError(
				"Not a Generator: " + genNodeId);
			
			Generator gen = (Generator)node;
			
			ModelContainer container = gen.getModelContainer();
			if (container == null) throw new ModelContainsError("Generator has no parent");
			
			return container.isGeneratorRepeating(gen);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}	
	
	
	public synchronized void makeGeneratorRepeating(String clientId, boolean confirm, 
		String genNodeId, boolean repeating)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,  // if the specified Node is not found.
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(genNodeId);
			if (! (node instanceof Generator)) throw new ParameterError(
				"Not a Generator: " + genNodeId);
			
			Generator gen = (Generator)node;
			
			if (gen.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(gen.getFullName());
				
			ModelContainer container = gen.getModelContainer();
			if (container == null) throw new ModelContainsError("Generator has no parent");
			
			boolean isAlreadyRepeating = container.isGeneratorRepeating(gen);
			if (repeating && isAlreadyRepeating) return;
			if ((! repeating) && (! isAlreadyRepeating)) return;
			
			if (gen.getDomain() instanceof ModelDomain)
			{
				ModelDomain md = (ModelDomain)(gen.getDomain());
				if (md.dependentSimulationRunsExist())
				{
					if (confirm)
						deleteDependentSimulationRuns(md);
					else
						throw new Warning("Simulation Runs will be invalid and will be deleted");
				}
			}
			
			Conduit repeatingConduit = container.makeGeneratorRepeating(gen, repeating);
			
			thereAreUnflushedChanges = true;
			
			
			// Notify

			PeerNotice notice = new PeerNoticeBase.NonStructuralChangeNotice(genNodeId);
					
			try { callbackManager.notifyAllListeners(notice); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized void setGeneratorIgnoreStartup(String clientId, boolean confirm, 
		String genNodeId, boolean ignore)
	throws
		Warning,
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = PersistentNodeImpl.getNode(genNodeId);
			if (! (node instanceof Generator)) throw new ParameterError(
				"Not a Generator: " + genNodeId);
			
			Generator gen = (Generator)node;
			
			if (gen.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(gen.getFullName());
				
			if (gen.getDomain() instanceof ModelDomain)
			{
				ModelDomain md = (ModelDomain)(gen.getDomain());
				if (md.dependentSimulationRunsExist())
				{
					if (confirm)
						deleteDependentSimulationRuns(md);
					else
						throw new Warning("Simulation Runs will be invalid and will be deleted");
				}
			}
			
			gen.setIgnoreStartup(ignore);
			
			thereAreUnflushedChanges = true;
			
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.NonStructuralChangeNotice(genNodeId)); }
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }			
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}


	public synchronized DecisionSer[] getDecisions(String clientId, 
		String decisionPointPath, String scenarioName)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(decisionPointPath);
			if (node == null) throw new ParameterError("Not found: " + decisionPointPath);
			
			if (! (node instanceof DecisionPoint)) throw new ParameterError(
				"Not a DecisionPoint (and therefore has no Decisions): " + decisionPointPath);
			
			DecisionPoint decisionPoint = (DecisionPoint)node;
			
			DecisionDomain decisionDomain = decisionPoint.getDecisionDomain();
			
			DecisionScenario decisionScenario = decisionDomain.getDecisionScenario(scenarioName);
			if (decisionScenario == null) throw new ParameterError(
				"Not found: " + scenarioName);
			
			Set<Decision> decisions = decisionScenario.getDecisions(decisionPoint);
			
			DecisionSer[] decisionSers = new DecisionSer[decisions.size()];
			int decisionNo = 0;
			for (Decision decision : decisions)
				decisionSers[decisionNo++] = (DecisionSer)(decision.externalize());
			return decisionSers;
			
			//return decisions.toArray(new Decision[0]);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	public synchronized ImageIcon getStoredImageIcon(String clientId, String handle)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			return getLocalStoredImageIcon(handle);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	/** Note: This method is not currently used. */
	
	public synchronized Serializable[] getFinalStateValues(String clientId, 
		String statePath, String scenarioName)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = findNode(statePath);
			if (node == null) throw new ParameterError("Not found: " + statePath);
			
			if (! (node instanceof State)) throw new ParameterError(
				"Not a State: " + statePath);
			
			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
				
			ModelDomain modelDomain = state.getModelDomain();
			
			ModelScenario modelScenario = null;
			try { modelScenario = modelDomain.getModelScenario(scenarioName); }
			catch (ElementNotFound enf) { throw new ParameterError(
				"Not found: " + scenarioName); }
			
			return modelScenario.getStateFinalValues(state);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	/** Note: This method is not currently used. */
	
	public synchronized Serializable[] getFinalStateValuesForId(String clientId, 
		String stateId, String scenarioId)
	throws
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);
			
			PersistentNode node = PersistentNodeImpl.getNode(stateId);
			if (node == null) throw new ParameterError("Not found: State with Id " + stateId);
			
			if (! (node instanceof State)) throw new ParameterError(
				"Not a State: node with Id " + stateId);
			
			State state = (State)node;
			
			if (state.isLockedForSimulation()) 
				throw new CannotModifyDuringSimulation(state.getFullName());
				
			ModelDomain modelDomain = state.getModelDomain();
			
			node = PersistentNodeImpl.getNode(scenarioId);
			if (node == null) throw new ParameterError(
				"Not found: Node Id " + scenarioId);
			if (! (node instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + scenarioId + " is not a Model Scenario");
			
			ModelScenario modelScenario = (ModelScenario)node;
			
			return modelScenario.getStateFinalValues(state);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
		
	public void setAttributeValue(String clientId, boolean confirm, 
		String attrNodeId, String scenarioId, Serializable value)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		if (scenarioId == null)
			throw new ParameterError("scenarioId is null");
		else try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(attrNodeId);
			if (n == null) throw new ParameterError("Attribute with Id " + attrNodeId +
				" not found.");
			if (! (n instanceof Attribute)) throw new ParameterError(
				"Node with Id " + attrNodeId + " is not an Attribute.");
			Attribute attr = (Attribute)n;
			
			if (attr instanceof ModelAttribute)
				if (((ModelAttribute)attr).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(attr.getFullName());
				
			if (! (value instanceof Types.Inspect))
				if (! confirm)
				{
					if (attr instanceof ModelAttribute)
						if (((ModelAttribute)attr).getModelDomain().dependentSimulationRunsExist()) 
							throw new Warning(
							"Simulation Runs will be invalid and will be deleted");
				}
			
			n = PersistentNodeImpl.getNode(scenarioId);
			if (n == null) throw new ParameterError("Scenario with Id " + scenarioId +
				" not found.");
			if (! (n instanceof ModelScenario)) throw new ParameterError(
				"Node with Id " + scenarioId + " is not a Model Scenario.");
			ModelScenario scenario = (ModelScenario)n;
			
			scenario.setAttributeValue(attr, value);
			thereAreUnflushedChanges = true;
			
			if (! (value instanceof Types.Inspect)) deleteDependentSimulationRuns(scenario);
			
			
			// Notify clients.
			
			Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
			try { peer.notifyAllListeners(
				new PeerNoticeBase.AttributeValueUpdated(scenario.getNodeId(), 
					attr.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}
	
	
	/*protected void setAttributeValue(String clientId, boolean confirm, 
		String attrNodeId, Serializable value)
	throws
		Warning,
		ParameterError,
		CannotObtainLock,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode n = PersistentNodeImpl.getNode(attrNodeId);
			if (n == null) throw new ParameterError("Attribute with Id " + attrNodeId +
				" not found.");
			if (! (n instanceof Attribute)) throw new ParameterError(
				"Node with Id " + attrNodeId + " is not an Attribute.");
			Attribute attr = (Attribute)n;
			
			if (attr instanceof ModelAttribute)
				if (((ModelAttribute)attr).isLockedForSimulation()) 
					throw new CannotModifyDuringSimulation(attr.getFullName());
				
			if (! (value instanceof Types.Inspect))
				if (! confirm)
				{
					if (attr instanceof ModelAttribute)
						if (((ModelAttribute)attr).getModelDomain().dependentSimulationRunsExist())
							throw new Warning(
								"Simulation Runs will be invalid and will be deleted");
				}
			
			// Set the Attribute value in each Scenario.
			
			thereAreUnflushedChanges = true;

			attr.setValue(...scenario, value);

			
			// Notify clients and delete dependent Simulation Runs.
			
			Domain domain = attr.getDomain();
			Set<Scenario> scenarios = domain.getScenarios();
			for (Scenario scenario : scenarios)
			{
				if (scenario instanceof ModelScenario)
					if (! (value instanceof Types.Inspect))
						deleteDependentSimulationRuns((ModelScenario)scenario);


				// Notify clients.
				
				Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
				try { peer.notifyAllListeners(
					new PeerNoticeBase.AttributeValueUpdated(scenario.getNodeId(), 
						attr.getNodeId())); }
				catch (Exception ex)
				{
					GlobalConsole.printStackTrace(ex);
				}
			}
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}*/
	
	
	public synchronized void watchNode(String clientId, boolean enable, String nodePath)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ServiceContext.create(this, clientId);

			PersistentNode node = findNode(nodePath);
			if (node == null) throw new ParameterError("Not found: " + nodePath);
			
			node.setWatch(enable);
		}
		catch (RuntimeException re)
		{
			GlobalConsole.printStackTrace(re);
			throw new InternalEngineError(re);
		}
		finally
		{
			ServiceContext.clear();
		}
	}

	
	
	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Methods from ModelEngineLocal..
	 *
	 */

	
	public synchronized void allowNewSimulations(boolean allow)
	{
		this.allowSimulations = allow;
	}
	
	
	public synchronized boolean newSimulationsAreAllowed()
	{
		return this.allowSimulations;
	}


	public synchronized boolean simulationsAreRunning()
	{
		Set<Integer> threadIds = threads.keySet();
		for (Integer threadId : threadIds)
		{
			ServiceThread thread = threads.get(threadId);
			if (((Thread)thread).isAlive()) return true;
		}
		
		return false;
	}


	public String createUniqueDomainName(String base)
	{
		if (base == null) base = "New Domain";
		String newName = base;
		
		synchronized (domains)
		{
			int i = 1;
			for (;;)
			{
				if (i == 100) newName = (new Date()).toString() + "_" + newName;
				if (i == 1000) throw new RuntimeException(
					"Tried 1000 names and could not find a unique one");
				
				if (domains.get(newName) == null) return newName;
				
				newName = base + i++;
			}
		}
	}
	

	public String createUniqueDomainName()
	{
		return createUniqueDomainName(StandardBaseDomainName);
	}
	
	
	public Domain createDomain(Class domainClass, String name, boolean useNameExactly)
	throws
		ParameterError
	{
		String domainName;
		if (useNameExactly)
		{
			if (name == null) throw new ParameterError("name is null");
			if ((domains.get(name) != null) || (domainTypes.get(name) != null))
				throw new ParameterError("Name '" + name + "' is in use");
			domainName = name;
		}
		else
			domainName = createUniqueDomainName(name);

		synchronized (domains)
		{
			Constructor constructor;
			try { constructor = domainClass.getConstructor(String.class); }
			catch (NoSuchMethodException ex) { throw new ParameterError(
				"Constructor for Domain class " + domainClass.getName() + " not found"); }
			final Domain domain;
			try { domain = (Domain)(constructor.newInstance(domainName)); }
			catch (Exception ex) { throw new ParameterError(
				"While instantiating " + domainClass.getName(), ex); }
			
			addNewDomain(domain);
			return domain;
		}
	}
	
	
	public Domain createDomain(Class domainClass)
	throws
		ParameterError
	{
		String name = createUniqueDomainName();
		return createDomain(domainClass, name, false);
	}
		
	
	public Domain createDomainNode(DomainType ofType)
	{
		try { return createDomain(ofType.getDomainClass(), null, false); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
	public DomainType createDomainTypeNode(DomainType baseType, MotifDef... usesTypes)
	{
		DomainType domainType = new DomainType(false, baseType.getDomainClass(), usesTypes);
		domainTypes.put(domainType.getName(), domainType);
		return domainType;
	}
	
	
	public void deleteDomainTypeNode(DomainType typeToDelete)
	throws
		DisallowedOperation
	{
		if (typeToDelete.isBuiltin()) throw new DisallowedOperation(
			"Attempt to delete a built-in Domain Type");
		domainTypes.remove(typeToDelete);
	}
	
	
	public Set<DomainType> getDomainTypeNodes()
	{
		return new TreeSetNullDisallowed<DomainType>(domainTypes.values());
	}
	
	
	public DomainType getDomainType(String name)
	{
		Collection<DomainType> dts = domainTypes.values();
		for (DomainType domainType : dts)
			if (domainType.getName().equals(name)) return domainType;
		return null;
	}

	
	public DomainType getBaseDomainTypeForDomainClass(Class domainClass)
	{
		if (ModelDomain.class.isAssignableFrom(domainClass)) return getDomainType("Model");
		if (HierarchyDomain.class.isAssignableFrom(domainClass)) return getDomainType("Hierarchy");
		if (DecisionDomain.class.isAssignableFrom(domainClass)) return getDomainType("Decision");
		return null;
	}
	
	
	public synchronized List<ModelDomain> getModelDomainPersistentNodes()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		List<ModelDomain> domainList = new Vector<ModelDomain>();
		Collection<Domain> coll = domains.values();
		for (Domain domain : coll)
			if (domain instanceof ModelDomain) domainList.add((ModelDomain)domain);
		return domainList;
	}
	
	
	public synchronized List<MotifDef> getMotifDefPersistentNodes()
	{
		List<MotifDef> motifDefs = new Vector<MotifDef>();
		Collection<Domain> coll = domains.values();
		for (Domain d : coll)
			if (d instanceof MotifDef) motifDefs.add((MotifDef)d);
		
		return motifDefs;
	}
	
	
	public synchronized Domain getDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		Domain domain = domains.get(name);
		if (domain != null) return domain;
		
		throw new ElementNotFound("Domain named '" + name + "'");
	}
	
	
	public synchronized ModelDomain getModelDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			Domain domain = domains.get(name);
			if (domain == null) throw new ElementNotFound(name);
			if (! (domain instanceof ModelDomain)) throw new ParameterError(
				"Domain '" + name + "' is not a Model Domain");

			return (ModelDomain)domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized MotifDef getMotifDefPersistentNode(String name)
	throws
		ElementNotFound,
		ModelContainsError
	{
		Domain d = domains.get(name);
		if (d != null)
		{
			if (! (d instanceof MotifDef)) throw new ModelContainsError(name + " is not a MotifDef");
			return (MotifDef)d;
		}
		
		throw new ElementNotFound("Motif " + name);
	}


	public synchronized List<DecisionDomain> getDecisionDomainPersistentNodes()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		List<DecisionDomain> domainList = new Vector<DecisionDomain>();
		Collection<Domain> coll = domains.values();
		for (Domain domain : coll)
			if (domain instanceof DecisionDomain) domainList.add((DecisionDomain)domain);
		return domainList;
	}


	public synchronized DecisionDomain getDecisionDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			Domain domain = domains.get(name);
			if (domain == null) throw new ElementNotFound("Domain " + name);
			if (! (domain instanceof DecisionDomain)) throw new ParameterError(
				"Domain '" + name + "' is not a Decision Domain");
			
			return (DecisionDomain)domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized ModelDomain createModelDomainPersistentNode(String name,
		boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			ModelDomain domain = createModelDomain_internal(name, useNameExactly);
			thereAreUnflushedChanges = true;
			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	

	public synchronized ModelDomainMotifDef createModelDomainMotifDefPersistentNode(String name,
		boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		ModelDomainMotifDef md;
		synchronized (domains)
		{
			if (useNameExactly) try
			{
				getModelDomainPersistentNode(name);
				throw new ParameterError("Name '" + name + "' already in use");
			}
			catch (Exception ex) {}  // ok
			
			String uniqueName = createUniqueMotifName(name);
			md = new ModelDomainMotifDefImpl(uniqueName);
			domains.put(uniqueName, md);
		}
		
		// Notify.
		
		try { callbackManager.notifyAllListeners(
			new PeerNoticeBase.MotifCreatedNotice(md.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
		
		return md;
	}


	public synchronized ModelDomainMotifDef createModelDomainMotifDefPersistentNode()
	throws
		ParameterError,
		IOException
	{
		return createModelDomainMotifDefPersistentNode(null, false);
	}
	
	
	public synchronized DecisionDomain createDecisionDomainPersistentNode(String name, 
		boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			DecisionDomain domain = createDecisionDomain_internal(name, useNameExactly);
			thereAreUnflushedChanges = true;
			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized DecisionDomain createDecisionDomainMotifDefPersistentNode(
		String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			String domainName = name;
			if (domains.containsKey(name))
				if (useNameExactly) throw new ParameterError(
					"A Domain with the name " + name + " already exists.");
				else
					domainName = createUniqueMotifName();
	
			DecisionDomainMotifDef domain = new DecisionDomainMotifDefImpl(domainName);
			addNewMotif(domain);
			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized DecisionDomain createDecisionDomainMotifDefPersistentNode()
	throws
		ParameterError,
		IOException
	{
		return createDecisionDomainMotifDefPersistentNode(null, false);
	}
	
	
	public synchronized List<HierarchyDomain> getHierarchyDomainPersistentNodes()
	throws
		CannotObtainLock,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		List<HierarchyDomain> domainList = new Vector<HierarchyDomain>();
		Collection<Domain> coll = domains.values();
		for (Domain domain : coll)
			if (domain instanceof HierarchyDomain) domainList.add((HierarchyDomain)domain);
		return domainList;
	}
	
	
	public synchronized HierarchyDomain createHierarchyDomainPersistentNode(
		String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			String domainName = name;
			if (domains.containsKey(name))
				if (useNameExactly) throw new ParameterError(
					"A Domain with the name " + name + " already exists.");
				else
					domainName = createUniqueDomainName();
	
			HierarchyDomain domain = new HierarchyDomainImpl(domainName);
			addNewDomain(domain);
			return domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}


	public synchronized HierarchyDomain getHierarchyDomainPersistentNode(String name)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			Domain domain = domains.get(name);
			if (domain == null) throw new ElementNotFound(name);
			if (! (domain instanceof HierarchyDomain)) throw new ParameterError(
				"Domain '" + name + "' is not a Hierarchy Domain");

			return (HierarchyDomain)domain;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized HierarchyDomain createHierarchyDomainMotifDefPersistentNode(
		String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		try
		{
			String domainName = name;
			if (domains.containsKey(name))
				if (useNameExactly) throw new ParameterError(
					"A Domain with the name " + name + " already exists.");
				else
					domainName = createUniqueMotifName();
	
			HierarchyDomainMotifDef domain = new HierarchyDomainMotifDefImpl(domainName);
			addNewMotif(domain);
			return domain;
			
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}


	public synchronized HierarchyDomain createHierarchyDomainMotifDefPersistentNode()
	throws
		ParameterError,
		IOException
	{
		return createHierarchyDomainMotifDefPersistentNode(null, false);
	}
	
	
	public synchronized PersistentNode getPersistentNode(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			
			return node;
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}


	public synchronized Set<PersistentNode> getChildPersistentNodes(String nodeId)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			PersistentNode node = PersistentNodeImpl.getNode(nodeId);
			if (node == null) throw new ElementNotFound("Node Id " + nodeId);
			
			return node.getChildren();
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized PersistentNode getNodeForPath(String fullElementPath)
	throws
		ElementNotFound,
		ParameterError
	{
		try
		{
			return getModelElement_internal(fullElementPath);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized PersistentNode getNodeForPath(String domainName, String elementName)
	throws
		ElementNotFound,
		ParameterError
	{
		try
		{
			return getModelElement_internal(domainName, elementName);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}


	public synchronized DecisionElement getDecisionElementTree(String decisionDomainName,
		String elementName)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		try
		{
			return getDecisionElement(decisionDomainName, elementName);
		}
		catch (RuntimeException ex)
		{
			GlobalConsole.printStackTrace(ex);
			throw new InternalEngineError(ex);
		}
	}
	
	
	public synchronized HTTPManager getHttpManager() { return this.httpManager; }
	
	
	public synchronized void setHttpManager(HTTPManager m) { this.httpManager = m; }
	
	
	public synchronized ImageIcon createLocalImageIcon(byte[] bytes, String handle)
	{
		if (bytes == null) throw new RuntimeException("bytes is null");
		if (handle == null) throw new RuntimeException("handle is null");
		
		ImageIcon icon = new ImageIcon(bytes, handle);
		synchronized (imageIconCache)
		{
			imageIconCache.put(handle, icon);
			imageData.put(handle, bytes);
			ServiceContext.getMotifClassLoader().addMotifResource(handle, bytes);
			return icon;
		}
	}
	
	
	public synchronized ImageIcon getLocalStoredImageIcon(String handle)
	{
		synchronized (imageIconCache)
		{
			ImageIcon imageIcon = imageIconCache.get(handle);
			if (imageIcon != null) return imageIcon;
		}
		
		throw new RuntimeException("Image Icon with handle " + handle + " not found");
	}
	
	
	public synchronized void removeLocalStoredImageIcon(String handle)
	{
		synchronized (imageIconCache)
		{
			imageIconCache.remove(handle);
			imageData.remove(handle);
			ServiceContext.getMotifClassLoader().removeMotifResource(handle);
		}
	}


	public void addMotifClassByteArray(byte[] bytes) { this.motifClassByteArrays.add(bytes); }
	 

	/**
	 * Return a list of byte arrays that represent the contents of each class for
	 * each motif.
	 */
	 
	List<byte[]> getMotifClassByteArrays() { return this.motifClassByteArrays; }
	
	
	public List<GeneratedEvent>[] getEventNodesByEpoch(
		String modelDomainName, String modelScenarioName,
		String simRunName, List<String> statePaths)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
			return getEventsByEpoch_internal(modelDomainName, modelScenarioName, simRunName,
				statePaths, null, null, null);
	}
	
	
	public synchronized byte[] renderHTMLList(String nodeId)
	throws
		ParameterError,
		IOException
	{
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ParameterError(
			"Cannot identify Node with Id " + nodeId);
		
		SortedSet<PersistentNode> children = node.getChildren();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(baos);
		
		pw.println("<html>");
		pw.println("<body>");
		pw.println("<table>");
		pw.println("<tr> <!--Table Headings-->");
		pw.println("<td>Node Name</td><td>Type</td><td>ID</td><td>Summary</td><td>List Children</td><td>Download as XML</td>");
		pw.println("</tr>");
		
		for (PersistentNode child : children)
		{
			pw.print("<tr>");
			pw.print("<td>" + child.getName() + "</td>");
			pw.print("<td>" + child.getClass().getName() + "</td>");
			pw.print("<td>" + child.getNodeId() + "</td>");
			pw.print("<td>" + child.getHTMLDescription() + "</td>");
			pw.print("<td>" + "<a href=\"" + URLHelper.getListURL(child.getNodeId()) + "\">list children</a>" + "</td>");
			pw.print("<td>" + "<a href=\"" + URLHelper.getXMLURL(child.getNodeId()) + "\">download XML</a>" + "</td>");
			pw.println("</tr>");
		}
		
		pw.println("</body>");
		pw.println("</table>");
		
		pw.flush();
		return baos.toByteArray();
	}
	
	
	double DefaultSVGWidth = 16.5;  // Fits within margins of a standard page.
	double DefaultSVGHeight = 22.8;  // Fits within margins of a standard page.


	public synchronized byte[] renderAsSVG(String nodeId, double wCmMax, double hCmMax)
	throws
		ParameterError,
		IOException
	{
		org.apache.batik.svggen.SVGGraphics2D g = SVGUtils.getSVGGraphics();
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ParameterError("Could not find Node with Id " + nodeId);
		
		if ((wCmMax <= 0) || (hCmMax <= 0))
		{
			wCmMax = DefaultSVGWidth;
			hCmMax = DefaultSVGHeight;
		}
		
		return node.renderAsSVG(wCmMax, hCmMax);
	}


	public synchronized byte[] renderAsHTML(String nodeId)
	throws
		ParameterError,
		IOException
	{
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ParameterError("Could not find Node with Id " + nodeId);
		
		return node.renderAsHTML();
	}
		

	public synchronized void dump()
	throws
		IOException
	{
		dump(0);
	}


	public synchronized void dump(int indentation)
	throws
		IOException
	{
		for (Domain domain : domains.values())
		{
			domain.dump(indentation);
		}
	}
	

	public ListenerRegistrar registerPeerListener(String clientId, PeerListener listener)
	throws
		ParameterError,
		IOException
	{
		synchronized (listenerRegistrars)
		{
			ListenerRegistrarImpl registrar = new ListenerRegistrarImpl(listener, 
				clientId, this);
			listenerRegistrars.add(registrar);
			
			return registrar;
		}
	}


	public void unregisterPeerListener(PeerListener listener)
	throws
		ParameterError,
		IOException
	{
		synchronized (listenerRegistrars)
		{
			Set<ListenerRegistrarImpl> registrarsCopy = new HashSet(listenerRegistrars);
			for (ListenerRegistrar registrar : registrarsCopy)
			{
				if (((ListenerRegistrarImpl)registrar).getPeerListener().equals(listener))
					listenerRegistrars.remove(registrar);
			}
		}
	}


	public Set<ListenerRegistrarImpl> getListenerRegistrars()
	{
		synchronized (listenerRegistrars)
		{
			return new HashSet<ListenerRegistrarImpl>(listenerRegistrars);
		}
	}


	public CallbackManager getCallbackManager() { return this.callbackManager; }
	
	
	public JarProcessor getJarProcessor(boolean replace, PeerListener peerListener,
		String clientId,
		byte[] byteAr)
	{
		return new JarProcessorImpl(ModelEngineLocalPojoImpl.this,
			replace, peerListener, clientId, byteAr);
	}
	
	

	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Inner classes.
	 *
	 */

		
	/** ************************************************************************
	 * Inner class for handling callback requests when updateDecisions() is
	 * call synchronously (i.e., via a blocking call).
	 */
	
	class SynchronousDecisionCallback implements DecisionCallback
	{
		private List<String> progressMessages = new Vector<String>();
		private List<String> asyncMessages = new Vector<String>();
		private Set<Decision> newDecisions = new TreeSetNullDisallowed<Decision>();
		private boolean bSetToAbort = false;
		
		
		void addDecisions(Set<Decision> newDecisions)
		{
			this.newDecisions.addAll(newDecisions);
		}
		
		public synchronized Set<Decision> getNewDecisions()
		{
			return newDecisions;
		}

		public synchronized boolean checkAbort() { return bSetToAbort; }

		public synchronized void showProgress(String msg)
		{
			progressMessages.add(msg);
			GlobalConsole.println(msg);
		}

		public synchronized void showMessage(String msg)
		{
			asyncMessages.add(msg);
			GlobalConsole.println(msg);
		}


		public synchronized void showErrorMessage(String msg)
		{
			asyncMessages.add(msg);
			GlobalConsole.println(msg);
		}
		

		public synchronized void completed()
		{
			//bIsComplete = true;
			showMessage("Service request completed.");
		}

		public synchronized void aborted()
		{
			//abort = true;
			showMessage("Aborted");
		}

		public synchronized void aborted(String msg)
		{
			//abort = true;
			showMessage("Aborted: " + msg);
		}

		public synchronized void aborted(Exception ex)
		{
			//abort = true;
			GlobalConsole.println("Aborted: ");
			GlobalConsole.printStackTrace(ex);

			for (Throwable t = ex; t != null; t = t.getCause())
			{
				showMessage(t.getMessage());
			}
		}

		public synchronized void aborted(String msg, Exception ex)
		{
			//abort = true;
			GlobalConsole.println("Aborted: " + msg);
			GlobalConsole.printStackTrace(ex);

			for (Throwable t = ex; t != null; t = t.getCause())
			{
				showMessage(t.getMessage());
			}
		}

		public synchronized boolean isComplete()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}

		public synchronized List<String> getProgress()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}

		public synchronized List<String> getMessages()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}

		public synchronized boolean isAborted()
		{
			throw new RuntimeException("Asynchronous call not supported.");
		}
	}



	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Nested classes.
	 *
	 */

		
	private int monitorThreadNumber = 0;
	
	protected Integer createMonitorThreadHandle()
	{
		return new Integer(++monitorThreadNumber);
	}
	
	
	/** ************************************************************************
	 * A thread for initiating and monitoring a series of simulation runs, each
	 * of which may create its own thread. This implementation is currently
	 * single-threaded, so that at any one time there is only one simulation
	 * run in progress. All simulated Scenarios must belong to the same Domain.
	 */
	
	class SimulationMonitorThread extends ServiceThreadImpl
	{
		private static final long DefaultInitialRandomSeed = 1;
		private boolean averages;
		private ModelScenarioSet scenarioSet = null; // may be null
		private ModelScenario modelScenario = null;  // may be null.
		private int runs = 0;
		
		// If any of these four values is non-null or non-zero, it takes precedence
		// over the corresponding value for the Scenario or ScenarioSet.
		private Date initialEpoch = null;
		private Date finalEpoch = null;
		private int iterationLimit = 0;
		private long duration = 0;
		
		private Date preferredStartTime = null;

		private PeerListener peerListener = null;
		private boolean repeatable;
		private ModelDomain modelDomain = null;
		
		private long granularityMs = 0;
		
		private boolean proceed = true;
		
		List<ModelScenario> scenarios = null;
		long effectiveEndTimeMs = 0;
		
		
		SimulationMonitorThread(Integer requestThreadHandle,
			boolean averages,
			TimePeriodType externalStateTimeGranularity,
			ModelScenarioSet scenarioSet, Date initialEpoch,
			Date finalEpoch, int iterationLimit, int runs, long duration,
			PeerListener peerListener,  // may be null
			boolean repeatable
			)
		throws
			CannotObtainLock
		{
			this(requestThreadHandle,
				averages, externalStateTimeGranularity, null, scenarioSet,
				initialEpoch, finalEpoch, iterationLimit, runs, duration, peerListener,
				repeatable);
			
			this.scenarioSet = scenarioSet;
		}


		SimulationMonitorThread(Integer requestThreadHandle,
			boolean averages,
			TimePeriodType externalStateTimeGranularity,
			ModelScenario modelScenario, Date initialEpoch,
			Date finalEpoch, int iterationLimit, int runs, long duration,
			PeerListener peerListener,  // may be null
			boolean repeatable
			)
		throws
			CannotObtainLock
		{
			this(requestThreadHandle,
				averages, externalStateTimeGranularity, modelScenario, null,
				initialEpoch, finalEpoch, iterationLimit, runs, duration, peerListener,
				repeatable);
		}
		
		
		SimulationMonitorThread(Integer requestThreadHandle,
			boolean averages,
			TimePeriodType externalStateTimeGranularity,
			ModelScenario modelScenario,
			ModelScenarioSet scenarioSet,
			Date initialEpoch,
			Date finalEpoch, int iterationLimit, int runs, long duration,
			PeerListener peerListener,  // may be null
			boolean repeatable
			)
		throws
			CannotObtainLock
		{
			super(requestThreadHandle); // saves reference to ServiceContext of creator thread.
			
			// Exactly one of the following two must be null:
			this.modelScenario = modelScenario;
			this.scenarioSet = scenarioSet;
			
			this.averages = averages;
			this.initialEpoch = initialEpoch;
			this.finalEpoch = finalEpoch;
			this.iterationLimit = iterationLimit;
			this.runs = runs;
			this.duration = duration;
			this.peerListener = peerListener;
			this.repeatable = repeatable;
			
			if ((modelScenario == null) && (scenarioSet == null)) throw new RuntimeException(
				"No Model Scenario or Scenario Set specified");
			
			if ((modelScenario != null) && (scenarioSet != null)) throw new RuntimeException(
				"Both a Model Scenario and a Scenario Set are specified");
			
			if (modelScenario == null)
			{
				this.modelDomain = scenarioSet.getModelDomain();
				this.scenarios = scenarioSet.getModelScenarios();
			}
			else
			{
				this.modelDomain = modelScenario.getModelDomain();
				this.scenarios = new Vector<ModelScenario>();
				this.scenarios.add(modelScenario);
			}
			
			modelDomain.lockForSimulation(true);
			
			
			// Compute preferred start time, to ensure that all simulations have
			// the same start time (in simulated time). Otherwise they will be
			// out of sync and difficult to compare.

			if (initialEpoch == null)
			{
				Date epoch = null;
				
				if (modelScenario == null)
					epoch = scenarioSet.getApplicableStartingDate();
				else
					epoch = modelScenario.getApplicableStartingDate();
				
				if (epoch == null)
					this.preferredStartTime = new Date();
				else
					this.preferredStartTime = epoch;
			}
			else
				this.preferredStartTime = initialEpoch;
			
			
			// Determine the time range needed for external imported simulations.
			
			effectiveEndTimeMs = this.preferredStartTime.getTime() + duration;
			if (finalEpoch != null)
				effectiveEndTimeMs = Math.min(effectiveEndTimeMs, finalEpoch.getTime());
			
			
			// Determine the granularity of time with which external simulation
			// results should be imported.
			
			try
			{
				if (externalStateTimeGranularity == TimePeriodType.unspecified)
					granularityMs =
						ModelAPITypes.getMsForTimePeriod(ModelDomain.DefaultTimePeriodForExternalStates);
				else
					granularityMs =
						ModelAPITypes.getMsForTimePeriod(externalStateTimeGranularity);
			}
			catch (UndefinedValue uv) { throw new RuntimeException(uv); }
			
			
			// Identify the ListenerRegistrar for the client and automatically
			// subscribe the client to the Domain being simulated, in the context
			// of each Scenario being simulated.
			
			if (peerListener != null)
			{
				Set<ListenerRegistrarImpl> listenerRegistrars = 
					ModelEngineLocalPojoImpl.this.getListenerRegistrars();
					
				for (ListenerRegistrarImpl registrar : listenerRegistrars)
				{
					if (registrar.getPeerListener().equals(peerListener)) try
					{
						for (ModelScenario scenario : scenarios)
							registrar.subscribe(scenario.getModelDomain().getNodeId(), 
								scenario.getName(), true);
					}
					catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
				}
			}
		}
		
		
		public void go()
		{
			try
			{
				// Note: the superclass run method has restored the ServiceContext of the
				// thread that created this thread.
				
				if (isAborted()) { abortThread(); return; }
				
				
				// Establish an initial random seed.
				
				long initialSeed = DefaultInitialRandomSeed;
				if (! this.repeatable)
					initialSeed = System.currentTimeMillis();
				
				RandomGenerator seedGenerationRandomGenerator = new RandomGenerator(initialSeed);
				
				
				/* Schedule the simulations in batches, depending on how many processors
				 the system has. Do not concurrently execute more simulations than
				 the number of processors.*/
				
				int noOfProcessors = Runtime.getRuntime().availableProcessors();
				int batchSize = noOfProcessors;
				
				SimServiceThread[] serviceThreads = new SimServiceThread[batchSize];
				SimCallback[] simCallbacks = new SimCallback[batchSize];
				
				//simRunCompletedStatus = new boolean[simsToRun];
				//for (int i = 0; i < simsToRun; i++) simRunCompletedStatus[i] = false;
			
				int simRunNumber = 0;
				
				for (ModelScenario scenario : scenarios)
				{
					SimRunSet simRunSet = scenario.createSimRunSet(
						this.initialEpoch, this.finalEpoch, this.duration, this.iterationLimit);
			
					
					// Obtain a ValueHistoryFactory to pass through to the ModelScenario.
					// This is used to obtain values for States that are outside of the 
					// Domain being simulated.
					
					ValueHistoryFactory valueHistoryFactory = null;
					try { valueHistoryFactory = scenario.getValueHistoryFactory(
						averages,
						preferredStartTime.getTime(),
						granularityMs,
						effectiveEndTimeMs); }
					catch (Exception ex)
					{
						ex.printStackTrace();
						
						showErrorMessage(ThrowableUtil.getAllMessages(ex));
						
						abortThread();
						return;
					}

					
					// Perform simulations, in batches. (Batch size is currently
					// limited to 1.)
					
					int simsToRun = runs;
					for (;;) // until all of 'runs' simulations have completed.
					{
						if (simsToRun == 0) break;
						
						// Start at least 'batchSize' more simulations.
						
						//for (int i = 0; i < batchSize; i++)
						for (int i = 0; i < 1; i++)
						{
							if (simsToRun == 0) break;
							
							// Create a callback object to receive notices from
							// the simulation run that we are about to create.
							
							simCallbacks[i] = new SimRunCallbackImpl(this, ++simRunNumber);
							
							
							// Create a RandomGenerator for the Run.
							
							long seed = seedGenerationRandomGenerator.nextLong();
							
						
							// Start an asynchronous simulation run.
							
							try
							{
								//showMessage(
								//	"Simulating model Scenario " + scenario.getName() + "...");
								
								serviceThreads[i] = scenario.getModelDomain().simulate(
									valueHistoryFactory, simRunSet,
									scenario, simCallbacks[i], preferredStartTime, finalEpoch, 
									iterationLimit, duration, seed);
								
								simsToRun--;
								
								SimulationRun simRun = serviceThreads[i].getSimulationRun();
							}
							catch (Exception ex)
							{
								showErrorMessage(ThrowableUtil.getAllMessages(ex));
								abortThread();
								return;
							}
					
							if (isAborted()) { abortThread(); return; }
						}
						
						
						// Wait for all of these simulations to finish.
						// Note: improve by implementing a queue so that all processors
						// are always busy.
		
						for (int i = 0; i < batchSize; i++)
						{
							if (serviceThreads[i] != null)
							{
								try { Thread.currentThread().sleep(100); }
								catch (InterruptedException ie) { return; }
								
								serviceThreads[i].waitForFinish();
							}
							
						}
					}
					
					scenario.updateStatistics();
				}
			}
			catch (Exception ex)
			{
				this.showErrorMessage(ThrowableUtil.getAllMessages(ex));
			}
			finally
			{
				// Report to the client that all simulations have completed.
				GlobalConsole.println("All simulations completed.");
				this.completed();
				
				try { this.modelDomain.lockForSimulation(false); }
				catch (CannotObtainLock col)
				{
					GlobalConsole.printStackTrace(col);
				}
			}
		}
	}
	
	
	/** ************************************************************************
	 * Each simulation run is given an instance of this class, to allow the run
	 * to inform the monitor of the run's progress. These callbacks are a logical
	 * extension of the monitor.
	 */
	 
	class SimRunCallbackImpl implements SimCallback
	{
		private SimulationMonitorThread monitorThread = null;
		private int simRunNumber = 0;
		//private boolean bSetToAbort = false;
		private String simRunNodeId = null;
		private int messageNumber = 0;
		
		
		SimRunCallbackImpl(SimulationMonitorThread monitorThread, int simRunNumber)
		{
			this.monitorThread = monitorThread;
			this.simRunNumber = simRunNumber;
		}
		
		
		public void setSimRunNodeId(String id) { this.simRunNodeId = id; }
		
		
		/*
		 * Methods from SimCallback and ProgressCallback. These are to be called
		 * by the simulation thread (for which this is a callback).
		 */
		
		public synchronized boolean checkAbort()
		{
			return monitorThread.checkAbort();
		}
	

		public synchronized void showProgress(String msg)
		{
			showMessage(msg);
		}
		
		
		public synchronized void showProgress(int totalEpochsCompleted,
			long totalTimeElapsed)
		{
			// Notify clients.
			
			try
			{
				SimulationRun simRun = (SimulationRun)(PersistentNodeImpl.getNode(this.simRunNodeId));
				ModelScenario scenario = simRun.getModelScenario();
				
				String id;
				if (scenario.getModelScenarioSet() == null)
					id = scenario.getNodeId();
				else
					id = scenario.getModelScenarioSet().getNodeId();
			
				ModelEngineLocalPojoImpl.this.callbackManager.notifyAllListeners(
					new PeerNoticeBase.ScenarioSimProgressNotice(
						monitorThread.getRequestHandle(),
						id, this.simRunNodeId,
						totalEpochsCompleted, totalTimeElapsed));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
		
		
		protected void notifyClientsOfError(String msg)
		{
			try
			{
				SimulationRun simRun = (SimulationRun)(PersistentNodeImpl.getNode(this.simRunNodeId));
				ModelScenario scenario = simRun.getModelScenario();
			
				String id;
				if (scenario.getModelScenarioSet() == null)
					id = scenario.getNodeId();
				else
					id = scenario.getModelScenarioSet().getNodeId();
			
				ModelEngineLocalPojoImpl.this.callbackManager.notifyAllListeners(
					new PeerNoticeBase.ScenarioSimErrorMessageNotice(
						monitorThread.getRequestHandle(),
						id, this.simRunNodeId,
						"Msg No. " + (++messageNumber) + ": " + msg));
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	
	
		public synchronized void showMessage(String msg)
		{
			String s = Integer.toString(simRunNumber) + ": " + msg;
			GlobalConsole.println(s);
		}
	
	
		public synchronized void showErrorMessage(String msg)
		{
			String s = "For run " + simRunNumber + ": " + msg;
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
		

		public synchronized void completed()
		{
			GlobalConsole.println("Completed run " + simRunNumber);

			// Notify clients.
			
			try
			{
				SimulationRun simRun = (SimulationRun)(PersistentNodeImpl.getNode(this.simRunNodeId));
				ModelScenario scenario = simRun.getModelScenario();
				SimRunSet simRunSet = simRun.getSimRunSet();
			
				String id;
				if (scenario.getModelScenarioSet() == null)
				{
					id = scenario.getNodeId();
				}
				else
				{
					id = scenario.getModelScenarioSet().getNodeId();
				}
			
				
				ModelEngineLocalPojoImpl.this.callbackManager.notifyAllListeners(
					new PeerNoticeBase.ScenarioSimProgressNotice(
						monitorThread.getRequestHandle(),
						id, this.simRunNodeId,
						true /* completed */));
				
				if (simRunSet != null)
				{
					ModelEngineLocalPojoImpl.this.callbackManager.notifyAllListeners(
						new PeerNoticeBase.ScenarioSimProgressNotice(
							monitorThread.getRequestHandle(),
							simRunSet.getNodeId(), this.simRunNodeId,
							true /* completed */));
				}
			}
			catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		}
	
	
		public synchronized void aborted()
		{
			String s = "Aborted run " + simRunNumber;
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
	
	
		public synchronized void aborted(String msg)
		{
			String s = "Aborted run " + simRunNumber + ": " + msg;
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
		

		public synchronized void aborted(Exception ex)
		{
			String s = "Aborted run " + simRunNumber + ": " + ex.toString();
			
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
	
	
		public synchronized void aborted(String msg, Exception ex)
		{
			String s = "Aborted run " + simRunNumber + ": " + msg + "; " + 
				ex.toString();
			
			GlobalConsole.println(s);
			notifyClientsOfError(s);
		}
		
		
		public synchronized boolean getConfirmationToProceed()
		{
			return true;
		}
		
		
		/*
		 * Implementation methods, to be called by the monitor thread to manage
		 * the simulation thread (for which this is a callback).
		 */
		
		synchronized void abortGracefully()
		{
			monitorThread.abort();
		}
	}
	
	
	/** ************************************************************************
	 * A thread for initiating and monitoring the processing of an XML file,
	 * which might spawn one or more simulations.
	 */
	 
	class XMLMonitorThread extends ServiceThreadImpl
	{
		private XMLProcessor xmlProcessor;
		
		XMLMonitorThread(Integer requestThreadHandle,
			PeerListener peerListener,  // may be null
			String clientId,
			char[] charAr
			)
		{
			super(requestThreadHandle); // saves reference to ServiceContext of creator thread.
			this.xmlProcessor = new XMLProcessorImpl(ModelEngineLocalPojoImpl.this,
				true /* replace */, peerListener, clientId, charAr);
		}
		
		public void go()
		{
			xmlProcessor.processXML();
		}


		protected PeerListener getPeerListener() { return xmlProcessor.getPeerListener(); }
		protected String getClientId() { return xmlProcessor.getClientId(); }
		protected char[] getCharAr() { return xmlProcessor.getCharAr(); }
		//protected void setCharAr(char[] charAr) { xmlProcessor.setCharAr(charAr); }
		protected List<PersistentNode> getRootNodesCreated() { return xmlProcessor.getRootNodesCreated(); }
	}
	
	
	/** ************************************************************************
	 * A thread for initiating and monitoring the processing of a JAR file,
	 * which might contain an Expressway XML file and spawn one or more simulations.
	 */
	 
	class JarMonitorThread extends XMLMonitorThread
	{
		private JarProcessor jarProcessor;
		
		
		JarMonitorThread(Integer requestThreadHandle,
			PeerListener peerListener,
			String clientId,
			byte[] byteAr
			)
		{
			super(requestThreadHandle,
				peerListener,  // may be null
				clientId,
				null  // char[], is set by the go() method.
				);
			
			this.jarProcessor = new JarProcessorImpl(ModelEngineLocalPojoImpl.this,
				true /* replace */, peerListener, clientId, byteAr);
		}


		public void go()
		{
			this.jarProcessor.processJar();
		}
	}



	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Internal methods.
	 *
	 */

	
	/** ************************************************************************
	 * Convert the server's persisted representation of motif classes into
	 * available runtime Classes under the MotifClassLoader. This method is to
	 * be called once on server startup, after the database has been loaded and
	 * after the MotifClassLoader has been created.
	 */
	 
	protected void defineMotifClasses()
	{
		List<byte[]> byteArrays = getMotifClassByteArrays();
		MotifClassLoader motifClassLoader = ServiceContext.getMotifClassLoader();
		for (byte[] bytes : byteArrays) // each motif Class
		{
			// Ask motif Class Loader to define the Class.
			try
			{
				Class c = motifClassLoader.defineMotifClass(
					bytes, 0, bytes.length);
				GlobalConsole.println("Loaded motif class '" + c.getName() + "'");
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		}
	}
	
	
	/** ************************************************************************
	 * Copy each ImageIcon from the database to the MotifClassLoader, so that
	 * the Icons are available via the MotifClassLoader getMotifResource method.
	 */
	
	protected void defineMotifResources()
	{
		
		Set<String> names = imageData.keySet();
		for (String key : names)
		{
			byte[] data = imageData.get(key);
			createLocalImageIcon(data, key);
		}
	}

	
	/**
	 * Delete all Simulation Runs for the specified Model Domain, and notify the
	 * client that the deletion has occurred. Delete SimRunSets as well.
	 */
	 
	void deleteSimulationRuns(ModelDomain domain)
	{
		domain.deleteSimulationRuns();
		
		Peer peer = ServiceContext.getServiceContext().getPeer(domain);
		try { peer.notifyAllListeners(
			new PeerNoticeBase.SimulationRunsDeletedNotice(domain.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
	}


	/**
	 * Delete all Simulation Runs for the specified Model Scenario, and notify the
	 * client that the deletion has occurred. Delete SimRunSets as well.
	 */
	 
	void deleteSimulationRuns(ModelScenario scenario)
	{
		scenario.deleteSimulationRuns();
		
		Peer peer = ServiceContext.getServiceContext().getPeer(scenario);
		try { peer.notifyAllListeners(
			new PeerNoticeBase.SimulationRunsDeletedNotice(scenario.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
	}
	
	
	//void deleteDependentSimulationRuns(ModelElement elt)
	//{
	//	if (elt instanceof ModelDomain)
	//		deleteDependentSimulationRuns((ModelDomain)elt);
	//	else if (elt instanceof ModelScenario)
	//		deleteDependentSimulationRuns((ModelScenario)elt);
	//	else throw new RuntimeException(
	//		"Unexpected: elt is a " + elt.getClass().getName());
	//}
	
	
	void deleteDependentSimulationRuns(ModelDomain domain)
	{
		Set<ModelDomain> dependentDomains = domain.deleteDependentSimulationRuns();
		
		try
		{
			for (ModelDomain d : dependentDomains)
				d.notifyAllListeners(
					new PeerNoticeBase.SimulationRunsDeletedNotice(d.getNodeId()));
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}


	void deleteDependentSimulationRuns(ModelScenario scenario)
	{
		Set<ModelScenario> dependentScenarios = scenario.deleteDependentSimulationRuns();
		
		try
		{
			for (ModelScenario s : dependentScenarios)
				s.notifyAllListeners(
					new PeerNoticeBase.SimulationRunsDeletedNotice(s.getNodeId()));
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
	}


	ModelDomain getModelDomain_internal(String name)
	{
		Domain domain = domains.get(name);
		if (! (domain instanceof ModelDomain)) throw new RuntimeException(
			"Domain '" + name + "' is not a Model Domain");
		return (ModelDomain)domain;
	}
	
	
	Set<Decision> getNewDecisions(int callbackId)
	throws
		ParameterError,
		IOException
	{
		
			ServiceThread thread = threads.get(new Integer(callbackId));

			if (thread == null)  // not found.
				throw new CallbackNotFound();
		
			if (thread instanceof DecisionCallback)
			{
				return ((DecisionCallback)thread).getNewDecisions();
			}

			throw new ParameterError("The specified ID does not return Decisions.");
	}
	

	/** ************************************************************************
	 * Create a simulation thread and start it; then return the handle.
	 *

	protected Integer sim(ModelScenario modelScenario, boolean propagate,
		DecisionScenario decisionScenario // the originating DecisionScenario
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError
	{
		//
		// Create a thread for the simulation.

		UpdateRequestThread thread = new UpdateRequestThread(
			modelScenario,
			propagate,
			decisionScenario,
			null,
			modelScenario.getIterationLimit()
			);

		Integer threadHandle = new Integer(thread.hashCode());

		threads.put(threadHandle, thread);


		//
		// Start the thread.

		GlobalConsole.println(
			"Starting simulation for ModelScenario " + modelScenario.getName() +
			" and DecisionScenario " + 
			(decisionScenario == null ? "null" : decisionScenario.getName()));

		thread.start();
		
		return threadHandle;
	}
	*/

	
	protected PersistentNode identifyNode_internal(String nodeId, Class requiredType)
	throws
		Warning,
		ElementNotFound,
		ParameterError,
		CannotObtainLock
	{
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ElementNotFound(
			"Cannot identify the Node with ID " + nodeId);
		
		if (! requiredType.isAssignableFrom(node.getClass())) throw new ParameterError(
			"Node with ID " + nodeId + " is not a " + requiredType.getClass().getName());
		
		return node;
	}
	
	
	protected PersistentNode createGraphicNode_internal(boolean confirm, PersistentNode parentNode,
		String kind, double x, double y)
	throws
		Warning,
		ParameterError,
		CannotObtainLock
	{
		if ((x < 0) || (y < 0)) throw new ParameterError(
			"x and y must be positive: x=" + x + ", y=" + y);

		checkIfTemplateInstance(parentNode);
		checkIfTemplate(parentNode);

		if (parentNode.getDomain() instanceof ModelDomain)
		{
			ModelDomain md = (ModelDomain)(parentNode.getDomain());
			if (md.dependentSimulationRunsExist())
			{
				if (confirm)
					deleteDependentSimulationRuns(md);
				else
					throw new Warning("Simulation Runs will be invalid and will be deleted");
			}
		}

		
		// Capture the parent's current size so that we can determine if it
		// is changed as a side effect of adding the sub-Element to it.
		
		boolean resizedParent = false;
		
		double parentWidth = parentNode.getWidth();
		double parentHeight = parentNode.getHeight();
		
		
		// Create the child Node.
		
		PersistentNode[] outermostAffectedRef = new PersistentNode[1];
		PersistentNode node = 
			parentNode.createChild(kind, null, outermostAffectedRef);
		thereAreUnflushedChanges = true;
			
			
		// Determine if the parent's size changed.
		
		if (parentWidth != parentNode.getWidth()) resizedParent = true;
		if (parentHeight != parentNode.getHeight()) resizedParent = true;
		
		
		if (parentNode.getLayoutManager() == null)
		{
			node.setX(x);
			node.setY(y);
		}
		
		
		// Notify clients that the Element was created.
		
		Peer peer = ServiceContext.getServiceContext().getPeer(parentNode);
		
		PersistentNode outermostAffected = outermostAffectedRef[0];
		PersistentNode outermostAffectedParent = outermostAffected.getParent();
		
		try { peer.notifyAllListeners(
			new PeerNoticeBase.NodeChangedNotice(
				(outermostAffectedParent == null? 
					outermostAffected.getNodeId() : outermostAffectedParent.getNodeId()))); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		
		return node;
	}


	protected PersistentNode createListNode_internal(boolean confirm,
		PersistentListNode parentNode, PersistentListNode priorNode,
		PersistentListNode nextNode, Scenario scenario, String copiedNodeId)
	throws
		Warning,
		ParameterError,
		CannotObtainLock
	{
		// debug
		Set<Attribute> attrset = parentNode.getAttributes();
		GlobalConsole.println("Entered createListNode_internal...");
		GlobalConsole.println("Node '" + parentNode.getFullName() + "' has " +
			attrset.size() + " attributes.");
		// end debug
		
		
		
		
		checkIfTemplateInstance(parentNode);
		checkIfTemplate(parentNode);
		Domain domain = parentNode.getDomain();

		if (domain instanceof ModelDomain)
		{
			ModelDomain md = (ModelDomain)(parentNode.getDomain());
			if (md.dependentSimulationRunsExist())
			{
				if (confirm)
					deleteDependentSimulationRuns(md);
				else
					throw new Warning("Simulation Runs will be invalid and will be deleted");
			}
		}

		
		// Capture the parent's current size so that we can determine if it
		// is changed as a side effect of adding the sub-Element to it.
		
		boolean resizedParent = false;
		
		double parentWidth = parentNode.getWidth();
		double parentHeight = parentNode.getHeight();
		
		PersistentNode copiedNode = null;
		PersistentListNode copiedListNode = null;
		if (copiedNodeId != null)
		{
			copiedNode = PersistentNodeImpl.getNode(copiedNodeId);
			if (! (copiedNode instanceof PersistentListNode)) throw new ParameterError(
				"Copied Node is not a List Node: it is a " + copiedNode.getClass().getName());
			copiedListNode = (PersistentListNode)copiedNode;
		}
		
		// Create the child Node.
		PersistentNode node = parentNode.createListChild(priorNode, nextNode, scenario, copiedListNode);
		thereAreUnflushedChanges = true;
		
		
		// debug
		GlobalConsole.println("\tCreated child node '" + node.getFullName() +
			"' and it has " + node.getAttributes().size() + " attributes.");
		GlobalConsole.println("\tThe parent node now has " + parentNode.getAttributes().size() +
			" attributes.");
		// end debug
		
		
		
		
		
			
		// Notify clients that the Element was created.
		Peer peer = ServiceContext.getServiceContext().getPeer(parentNode);
		try { peer.notifyAllListeners(
			new PeerNoticeBase.NodeChangedNotice(parentNode.getNodeId())); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		
		return node;
	}
	
	
	protected ModelDomain createModelDomain_internal(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		String domainName = name;
		if (domains.containsKey(name))
			if (useNameExactly) throw new ParameterError(
				"A Domain with the name " + name + " already exists.");
			else
				domainName = createUniqueModelDomainName();

		final ModelDomain domain;
		synchronized (domains)
		{
			domain = new ModelDomainImpl(domainName);
			addNewDomain(domain);
		}

		try { callbackManager.notifyAllListeners(
			new PeerNoticeBase.DomainCreatedNotice(domain.getNodeId())); }
		catch (Exception ex) { throw new IOException(ex); }

		return domain;
	}
	
	
	protected void addNewDomain(Domain domain)
	{
		synchronized (domains)
		{
			domains.put(domain.getName(), domain);
			
			// Notify DomainSpaceVisuals that a new Model Domain has been created.
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.DomainCreatedNotice(domain.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.println("Unable to notify some listeners");
				GlobalConsole.printStackTrace(ex);
			}
		}
	}
	
	
	protected void addNewMotif(MotifDef motifDef)
	{
		synchronized (domains)
		{
			domains.put(motifDef.getName(), motifDef);
			
			// Notify DomainSpaceVisuals that a new MotifDef has been created.
			try { callbackManager.notifyAllListeners(
				new PeerNoticeBase.MotifCreatedNotice(motifDef.getNodeId())); }
			catch (Exception ex)
			{
				GlobalConsole.println("Unable to notify some listeners");
				GlobalConsole.printStackTrace(ex);
			}
		}
	}


	protected ModelDomain createModelDomain_internal()
	throws
		ParameterError,
		IOException
	{
		synchronized (domains)
		{
			String name = createUniqueModelDomainName();
			return createModelDomain_internal(name, true);
		}
	}
	
	
	protected String createUniqueModelDomainName()
	{
		return createUniqueDomainName();
	}
	
	
	public String createUniqueMotifName(String base)
	{
		if (base == null) return createUniqueMotifName(StandardBaseMotifName);
		else 
			return createUniqueDomainName(base);
	}
	
	
	public String createUniqueMotifName()
	{
		return createUniqueMotifName(null);
	}
	
	
	/**
	 * Create a File that does not exist, using a name based on the name of the
	 * File provided. Ideally the File name will be exactly that of BasedOn.
	 */
	 
	protected File createUniqueJarFile(File basedOn)
	throws
		ParameterError,
		IOException
	{
		if (! (basedOn.getName().endsWith(".JAR") || basedOn.getName().endsWith(".jar")))
			throw new ParameterError(basedOn.toString() + " does not appear to be a JAR file");
		
		String pathWithExtension = basedOn.getAbsolutePath();
		String pathWithoutExtension = pathWithExtension.substring(0, pathWithExtension.length()-4);
		
		int i = 0;
		File file = basedOn;
		for (;;)
		{
			if (! file.exists())
				if (file.createNewFile()) return file;
			
			file = new File(pathWithoutExtension + Integer.toString(++i) + ".jar");
		}
	}
	
	

	protected String createUniqueDecisionDomainName()
	{
		return createUniqueDomainName();
	}
	

	public DecisionDomain createDecisionDomain_internal(String name, boolean useNameExactly)
	throws
		ParameterError,
		IOException
	{
		String domainName = name;
		if (domains.containsKey(name))
			if (useNameExactly) throw new ParameterError(
				"A Domain with the name " + name + " already exists.");
			else
				domainName = createUniqueDecisionDomainName();

		DecisionDomain domain = new DecisionDomainImpl(domainName);
		domains.put(domainName, domain);
		return domain;
	}
	
	
	public Scenario createScenario_internal(String domainName,
		String scenarioName)
	throws
		ParameterError,
		IOException
	{
		Domain domain = domains.get(domainName);
		if (domain == null) throw new ParameterError(
			"Unable to identify Model Domain " + domainName);
		
		Scenario scenario = domain.createScenario(scenarioName);
		
		// Notify clients.
		
		try { callbackManager.notifyAllListeners(
			new PeerNoticeBase.ScenarioCreatedNotice(domain.getNodeId(),
				scenario.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
		
		return scenario;
	}


	public Scenario createScenario_internal(String domainName)
	throws
		ParameterError,
		IOException
	{
		Domain domain = domains.get(domainName);
		if (domain == null) throw new ParameterError(
			"Unable to identify Model Domain " + domainName);
		
		Scenario scenario = domain.createScenario();
		
		// Notify clients.
		
		try { callbackManager.notifyAllListeners(
			new PeerNoticeBase.ScenarioCreatedNotice(domain.getNodeId(),
				scenario.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}			
		
		return scenario;
	}


	/*
	protected byte[] getMotifResource_internal(File motifFile,
		String pathWithinMotifJar)
	throws
		ParameterError,
		ResourceNotFound,
		IOException
	{
		// Verify that the JAR exists.
		
		if (! motifFile.exists()) throw new ResourceNotFound("Motif JAR " + motifFile.getAbsolutePath());
		
		
		// Attempt to retrieve the resource as an array of bytes.

		FileInputStream fis = new FileInputStream(motifFile);
		JarInputStream jis = new JarInputStream(fis);
		
		for (;;)
		{
			if (jis.available() == 0) break;
			
			JarEntry entry = jis.getNextJarEntry();
			if (entry == null) break;
			
			String name = entry.getName();
			
			if (entry.isDirectory()) continue;

			if (name.equals(pathWithinMotifJar))
			{
				// Read into char array.
				
				int MaxBytesToRead = 500;
				byte[] buffer = new byte[MaxBytesToRead];
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				for (;;)  
				{
					if (jis.available() == 0) break;
					int bytesRead = jis.read(buffer, 0, MaxBytesToRead);
					
					if (bytesRead == 0) continue;
					if (bytesRead < 0) break;
					baos.write(buffer, 0, bytesRead);
				}
				
				byte[] byteAr = baos.toByteArray();
				baos.close();
				jis.close();
				return byteAr;
			}
		}
		
		jis.close();
		
		throw new ResourceNotFound("Cannot find " + pathWithinMotifJar +
			" within " + motifFile.getAbsolutePath());
	}*/
	

	/** ************************************************************************
	 * 
	 */
	 
	protected double computeStatistic(StatOptionsType option, Serializable[] nodeHistory)
	throws
		ParameterError
	{
		SummaryStatistics sumStat = new SummaryStatisticsImpl();
		
		for (Serializable o : nodeHistory)
		{
			Number number = null;
			try { number = (Number)o; }
			catch (ClassCastException cce) { throw new ParameterError(
				"An object in the node history is not a Number: cannot calculate stats"); }
			
			if (number == null) continue;  // skip null values.
			double d = number.doubleValue();
			sumStat.addValue(d);
		}
		
		if (option.equals(StatOptionsType.mean)) return sumStat.getMean();
		else if (option.equals(StatOptionsType.geomean)) return sumStat.getGeometricMean();
		else if (option.equals(StatOptionsType.max)) return sumStat.getMax();
		else if (option.equals(StatOptionsType.min)) return sumStat.getMin();
		else if (option.equals(StatOptionsType.sd)) return sumStat.getStandardDeviation();
		//else if (option.equals(StatOptionsType.gamma))
		//	return computeGamma(nodeHistory)
		else
			throw new RuntimeException("Unexpected option");
	}
	
	
	protected List<StatOptionsType> parseStatOptionString(String[] optionsStrings)
	throws
		ParameterError
	{
		int n = optionsStrings.length;
		for (int i = 0; i < n; i++) optionsStrings[i] = 
			optionsStrings[i].trim().toLowerCase();
		
		List<StatOptionsType> options = new Vector<StatOptionsType>();
		for (String s : optionsStrings)
		{
			if (s.equals("mean")) options.add(StatOptionsType.mean);
			else if (s.equals("geomean")) options.add(StatOptionsType.geomean);
			else if (s.equals("max")) options.add(StatOptionsType.max);
			else if (s.equals("min")) options.add(StatOptionsType.min);
			else if (s.equals("sd")) options.add(StatOptionsType.sd);
			else if (s.equals("gamma")) throw new ParameterError(
				"The 'gamma' option is not supported yet");
			else throw new ParameterError("Unrecognized option: " + s);
		}
		
		return options;
	}
	
	
	protected double[] getResultStatistics_internal(State state,
		DistributionOwner distOwner, List<StatOptionsType> options)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		Serializable[] values = distOwner.getStateFinalValues(state);
		
		int nStats = options.size();
		
		double[] stats = new double[nStats];
		
		int i = 0;
		for (StatOptionsType option : options)
		{
			double stat = computeStatistic(option, values);
			stats[i++] = stat;
		}
		
		return stats;
	}


	protected int[] getHistogram_internal(State state,
		DistributionOwner distOwner, double bucketSize, double bucketStart, int noOfBuckets,
		boolean allValues)
	throws
		ParameterError
	{
		// Compute the buckets.
		
		double[] bucketMaxValues = new double[noOfBuckets];
			// A value falls into a bucket B if it is > the max
			// value of the prior bucket, and <= the max value of B.
			
		double maxValue = bucketStart;
		for (int bucketNo = 0; bucketNo < noOfBuckets; bucketNo++)
		{
			maxValue += bucketSize;
			bucketMaxValues[bucketNo] = maxValue;
		}
		
		// Retrieve the Simulation Runs for the specified Model Scenario.
		
		List<SimulationRun> runs = 
				new Vector<SimulationRun>(distOwner.getSimulationRuns());
		
		
		int[] histogram = new int[noOfBuckets];
		for (int b = 0; b < noOfBuckets; b++) histogram[b] = 0;  // initialize.
		
		nextRun:
		for (SimulationRun run : runs) // each run
		{
			// Get the final value of the state in this run.
			Serializable value = run.getStateFinalValue(state);
			
			// Convert the value to a double.
			
			double doubleValue = 0.0;
			try { doubleValue = ((Number)value).doubleValue(); }
			catch (ClassCastException cce) { throw new ParameterError(
				"The state " + state.getFullName() +
				" has non-numeric values", cce); }
			
			// Attempt to assign the value to a bucket.
			
			if (doubleValue < bucketStart)  // falls under all buckets.
			{
				if (allValues) throw new ParameterError(
					"Value (" + doubleValue + ") for State " + state.getFullName() +
						" falls under the lowest bucket (" + bucketStart + ")");
				continue;
			}
			
			int bucketNo = 0;
			for (double max : bucketMaxValues) // each bucket
			{
				if (doubleValue <= max)
				{
					histogram[bucketNo]++;
					continue nextRun;
				}
				
				bucketNo++;
			}
			
			// falls above all buckets.
			if (allValues) throw new ParameterError(
				"Value '" + String.valueOf(doubleValue) + "' for State '" + 
					state.getFullName() + "' falls above the highest bucket (" +
						String.valueOf(maxValue) + ")");
		}
		
		return histogram;
	}
	

	/** ************************************************************************
	 * 
	 */
	
	protected String resolveNodePath_internal(String path)
	throws
		ElementNotFound,
		ParameterError
	{			
		if (path == null) throw new ParameterError("path is null");
		if (path.equals("")) throw new ParameterError("Path is an empty string");
		
		String domainName = null;
		
		String[] pathParts = path.split("\\.");
		
		if (pathParts.length < 1) throw new RuntimeException();
		domainName = pathParts[0];
		
		String elementName = "";
		boolean firstTime = true;
		for (int partNo = 1; partNo < pathParts.length; partNo++)
		{
			if (firstTime) firstTime = false;
			else elementName = elementName + ".";
			
			elementName = elementName + pathParts[partNo];
		}
		
		PersistentNode node = null;
		
		try
		{
			node = getModelElement_internal(domainName, elementName);
			return node.getNodeId();
		}
		catch (ElementNotFound enf) {}
		
		try
		{
			node = getDecisionElement(domainName, elementName);
			return node.getNodeId();
		}
		catch (ElementNotFound enf) {}
		
		throw new ElementNotFound(path);
	}
	
	
	/** ************************************************************************
	 * 
	 */
	
	String[] getChildNodeIds_internal(String nodeId)
	throws
		ElementNotFound,
		ParameterError
	{
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ElementNotFound(nodeId);
		
		List<String> childNodeIds = new Vector<String>();
		
		Set<PersistentNode> childNodes = node.getChildren();
		for (PersistentNode childNode : childNodes)
		{
			childNodeIds.add(childNode.getNodeId());
		}
		
		return childNodeIds.toArray(new String[] {} );
	}


	/** ************************************************************************
	 * 
	 */
	
	String[] getSubcomponenteNodeIds_internal(String nodeId)
	throws
		ElementNotFound,
		ParameterError
	{
		PersistentNode node = PersistentNodeImpl.getNode(nodeId);
		if (node == null) throw new ElementNotFound(nodeId);
		
		if (! (node instanceof ModelContainer)) return new String[] {};
		
		List<String> childNodeIds = new Vector<String>();
		
		Set<ModelComponent> childNodes = ((ModelContainer)node).getSubcomponents();
		for (ModelComponent childNode : childNodes)
		{
			childNodeIds.add(childNode.getNodeId());
		}
		
		return childNodeIds.toArray(new String[] {} );
	}


	/** ************************************************************************
	 * 
	 */
	
	protected ModelElement getModelElement_internal(String elementName)
	throws
		ElementNotFound,
		ParameterError
	{
		String elementId = resolveNodePath_internal(elementName);
		PersistentNode node = PersistentNodeImpl.getNode(elementId);
		if (! (node instanceof ModelElement)) throw new ParameterError(
			"'" + elementName + "' is not a ModelElement: it is a " +
			node.getClass().getName());
		
		return (ModelElement)node;
	}
	

	/** ************************************************************************
	 * Find the named Model Element and return its Node Id.
	 */
	
	protected ModelElement getModelElement_internal(String modelDomainName,
		String elementName)
	throws
		ElementNotFound,
		ParameterError
	{
		if (modelDomainName == null) throw new ParameterError(
			"Null modelDomainName");

		Domain domain = domains.get(modelDomainName);
		if (domain == null) throw new ElementNotFound();
		if (! (domain instanceof ModelDomain)) throw new ParameterError(
			"Domain '" + modelDomainName + "' is not a Model Domain");
		ModelDomain modelDomain = (ModelDomain)domain;

		ModelElement element = null;
		if ((elementName == null) || elementName.equals("")) element = modelDomain;
		else element = modelDomain.findModelElement(elementName);

		if (element == null) throw new ElementNotFound();

		return element;
	}


	/** ************************************************************************
	 * Recursively adds to "nodes" all Nodes and child Nodes that have
	 * Attributes of the specified type. If attrType is null or scenario is null
	 * then do not filter based on the Attribute value type.
	 *
	 * Note: See also the methods ModelScenario.identifyTags and
	 * ModelScenario.identifyTaggedStates.
	 */
	 
	protected void getNodesWithAttributeRecursive(List<PersistentNode> nodes,
		PersistentNode node, Class attrType, Scenario scenario)
	throws
		ParameterError
	{
		Set<Attribute> attributes = node.getAttributes();
		
		for (Attribute attr : attributes)
		{
			if ((attrType == null) || (scenario == null)) nodes.add(node);
			else
			{
				Serializable value = scenario.getAttributeValue(attr);
				
				if (value != null)
				{
					if (value instanceof Class)
					{
						if (attrType.isAssignableFrom((Class)value))
							if (! nodes.contains(node)) nodes.add(node);
					}
					else
					{
						if (attrType.isAssignableFrom(value.getClass()))
							if (! nodes.contains(node)) nodes.add(node);
					}
				}
			}
		}
		
		
		// Call recursively.
		
		Set<PersistentNode> children = node.getChildren();
		for (PersistentNode child : children)
			getNodesWithAttributeRecursive(nodes, child, attrType, scenario);
	}
	
	
	/** ************************************************************************
	 * 
	 */
	
	protected DecisionElement getDecisionElement(String decisionDomainName,
		String elementName)
	throws
		ElementNotFound,
		ParameterError
	{
		if (elementName == null) throw new ParameterError(
			"Null decisionDomainName");

		Domain domain = domains.get(decisionDomainName);
		if (domain == null) throw new ElementNotFound();
		if (! (domain instanceof DecisionDomain)) throw new ParameterError(
			"Domain '" + decisionDomainName + "' is not a Decision Domain");
		DecisionDomain decisionDomain = (DecisionDomain)domain;

		PersistentNode node = null;
		if ((elementName == null) || elementName.equals("")) node = decisionDomain;
		else node = decisionDomain.findNode(elementName);
		//else element = decisionDomain.findDecisionElement(elementName);

		if (node == null) throw new ElementNotFound();
		if (! (node instanceof DecisionElement)) throw new ParameterError(
			"Node with name '" + elementName + "' is not a DecsionElement");

		return (DecisionElement)node;
	}


	/** ************************************************************************
	 * Find the Persistent Node specified by the path, or return null if it
	 * is not found.
	 */

	PersistentNode findNode(String path)
	throws
		ParameterError  // if the path is mal-formed.
	{
		PersistentNode node = null;

		int position = path.indexOf('.');
		if (position < 0)
		{
			// Name is not a compound name: might be a domain name.

			node = domains.get(path);
			return node;  // may be null.
		}
		else
		{
			// A compound name. First part might be a domain name.

			String domainName = path.substring(0, position);
			if (domainName.equals("")) return null;

			Domain domain = domains.get(domainName);
			if (domain == null) return null;

			String remainingPath = path.substring(position+1);
			if (remainingPath.equals("")) return null;

			return domain.findNode(remainingPath);
		}
	}


	/** ************************************************************************
	 * Signal the server application to stop all activity gracefully. If a graceful
	 * stop is not possible, then stop all threads forcefully and return false.
	 */
	 
	boolean stopAllServiceThreads()
	{
		boolean couldStopAllTasksGracefully = true;
		
		Set<Integer> callbackIdSet = threads.keySet();

		for (Integer callbackIdInteger : callbackIdSet)
		{
			try
			{
				int callbackId = callbackIdInteger.intValue();
				
				// Try to gracefully stop the task.
				
				abort(callbackId);
				
				// Wait awhile until it stops.
			
				int noOfTries = 0;
				while (! (isAborted(callbackId) || isComplete(callbackId)))
				{
					noOfTries++;
					if (noOfTries > 10)
					{
						couldStopAllTasksGracefully = false;
						
						ServiceThread thread = threads.get(callbackIdInteger);
						((Thread)thread).stop();
						
						break;
					}
					
					try { Thread.currentThread().sleep(100); }
					catch (InterruptedException ie) { return couldStopAllTasksGracefully; }
				}
			}
			catch (Exception ex)
			{
				GlobalConsole.println("While stopping service threads: " +
					ThrowableUtil.getAllMessages(ex));
				couldStopAllTasksGracefully = false;
				continue;
			}
		}
		
		return couldStopAllTasksGracefully;
	}
	
	
	void abort(int callbackId)
	throws
		CallbackNotFound
	{
		ServiceThread thread = this.threads.get(new Integer(callbackId));

		if (thread == null)  // not found.
			throw new CallbackNotFound();

		thread.abort();
	}
	
	
	boolean isAborted(int callbackId)
	throws
		CallbackNotFound
	{
		ServiceThread thread = threads.get(new Integer(callbackId));

		if (thread == null)  // not found.
			throw new CallbackNotFound();

		return thread.isAborted();
	}
	
	
	boolean isComplete(int callbackId)
	throws
		CallbackNotFound
	{
		ServiceThread thread = threads.get(new Integer(callbackId));

		if (thread == null)  // not found.
			throw new CallbackNotFound();

		return thread.isComplete();
	}
	
	
	protected ModelScenario getScenarioByName(String modelDomainName, String scenarioName)
	throws
		ElementNotFound,
		ParameterError
	{
		// Identify the Model Domain.
		Domain domain = domains.get(modelDomainName);
		if (domain == null) throw new ElementNotFound();
		if (! (domain instanceof ModelDomain)) throw new ParameterError(
			"Domain '" + modelDomainName + "' is not a Model Domain");
		ModelDomain modelDomain = (ModelDomain)domain;
		
		// Identify the scenario.
		try { return modelDomain.getModelScenario(scenarioName); }
		catch (ParameterError pe) { throw new ElementNotFound(pe); }
	}
	
	
	protected SimulationRun getSimulationRunByName(String modelDomainName,
		String scenarioName, String simRunName)
	throws
		ElementNotFound,
		ParameterError
	{
		ModelScenario scenario = getScenarioByName(modelDomainName, scenarioName);
		
		SimulationRun simRun = scenario.getSimulationRun(simRunName);
		if (simRun == null) throw new ElementNotFound(
			"Simulation Run '" + simRunName + "' not found within " +
				scenarioName);
				
		return simRun;
	}

	
	/** ************************************************************************
	 * 
	 */
	 
	protected List<GeneratedEvent>[] getEventsByEpoch_internal(
		String modelDomainName, String modelScenarioName,
		String simRunName, List<String> paths, Class[] stateTags,
		List<String>[] filteredStatePathsHolder, List<String>[] filteredIdsHolder
		)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		return getEventsByEpoch_internal(modelDomainName, modelScenarioName,
			simRunName, paths, stateTags, filteredStatePathsHolder, filteredIdsHolder,
			false, null);
	}
	
	
	/** ************************************************************************
	 * Note: The first (0th) element in the returned List is for iteration 1.
	 * If epoch 1 was a startup Epoch, then no Events will be listed for it.
	 */
	 
	protected List<GeneratedEvent>[] getEventsByEpoch_internal(
		String modelDomainName, String modelScenarioName,
		String simRunName, List<String> paths, Class[] stateTags,
		List<String>[] filteredStatePathsHolder,
		List<String>[] filteredIdsHolder,
		boolean returnEventSers, List<GeneratedEventSer>[][] eventSersHolder)
	throws
		CannotObtainLock,
		ElementNotFound,
		ModelContainsError,
		ParameterError,
		InternalEngineError,
		IOException
	{
		if (eventSersHolder != null) returnEventSers = true;		
		
		// Identify the Model Domain.
		Domain domain = domains.get(modelDomainName);
		if (domain == null) throw new ElementNotFound();
		if (! (domain instanceof ModelDomain)) throw new ParameterError(
			"Domain '" + modelDomainName + "' is not a Model Domain");
		ModelDomain modelDomain = (ModelDomain)domain;
		
		// Identify the scenario.
		ModelScenario modelScenario =
			modelDomain.getModelScenario(modelScenarioName);
		
		// Ientify the Simulation Run.
		SimulationRun simRun = modelScenario.getSimulationRun(simRunName);
		if (simRun == null) throw new ElementNotFound(
			"Simulation Run '" + simRunName + "' not found within " +
				modelScenarioName);
		
		// Identify each Element.
		List<ModelElement> elements = new Vector<ModelElement>();
		List<String> filteredStatePaths = new Vector<String>();
		List<String> filteredIds = new Vector<String>();
		for (String path : paths)
		{
			ModelElement e = modelDomain.findModelElement(path);
			if (e == null) throw new ElementNotFound(
				"Path '" + path + "' not found within Model Domain " +
					modelDomain.getName());
					
			if (! (e instanceof State)) throw new ParameterError(
				"History is currently only supported for States.");
			
			
			// Filter.
			boolean keep = false;  // if true, report on this State.
				
			if (stateTags == null) keep = true;  // no filter
			else
			{
				// Find any Attributes that e might have.
				Set<Attribute> attrCol = e.getAttributes();
				attrsloop: for (Attribute a : attrCol)
					for (Class tag : stateTags)
						if (modelScenario.getAttributeValue((ModelAttribute)a).getClass() == tag)
						{
							// attrCol.contains one of stateTags
							keep = true;
							break attrsloop;
						}
			}
				
			if (keep)
			{
				elements.add(e);
				filteredStatePaths.add(e.getFullName());
				filteredIds.add(e.getNodeId());
			}
		}
		
		if (stateTags != null)
		{
			filteredStatePathsHolder[0] = filteredStatePaths;
			filteredIdsHolder[0] = filteredIds;
		}
		
		
		// Retrieve the Event history from the Simulation Run, pertaining
		// to the ModelElements of interest.
		
		List<Epoch> epochs = simRun.getEpochs();
		if (epochs.size() < 1) throw new ModelContainsError("No epochs");
		
		List<GeneratedEvent>[] arrayOfListOfEventsOfInterest = new List[epochs.size()];
		if (returnEventSers) eventSersHolder[0] = new List[epochs.size()];
		List<GeneratedEventSer>[] eventSers = null;
		if (eventSersHolder != null) eventSers = eventSersHolder[0];
		
		int iteration = 0;
		for (Epoch epoch : epochs)
		{
			iteration++;
			
			SortedEventSet<GeneratedEvent> eventsAtEpoch;
			//SortedEventSet eventsAtEpoch;
			try { eventsAtEpoch = simRun.getGeneratedEventsAtIteration(iteration); }
			catch (ParameterError pe)
			{
				// Most likey the simulation aborted before the final iteration completed.
				eventsAtEpoch = new SortedEventSetImpl();
			}
			
			
			// Create a result row for this epoch.
			
			List<GeneratedEvent> eventsAtEpochOfInterest = new Vector<GeneratedEvent>();
			
			List<GeneratedEventSer> eventSersAtEpochOfInterest = null;
			if (returnEventSers) eventSersAtEpochOfInterest = new Vector<GeneratedEventSer>();
			

			// Add one element to the row for each column: one for each
			// caller-specified element.
			
			for (ModelElement element : elements)
			{
				// Check if eventsAtEpoch contains an Event involving element.
				// If so, create a column value; otherwise, write a "No Change" event.
				
				boolean found = false;
				CheckEvents: for (GeneratedEvent event : eventsAtEpoch)
				{
					if (! (event instanceof GeneratedEvent)) continue;  // skip Startup Events.
					
					if ((event.getState() != null) && (event.getState() == element))
					{
						found = true;
						
						// Check if there is already an event for this state in this epoch.
						
						boolean duplicateEvent = false;
						List<GeneratedEvent> eventsCopy = 
							new Vector<GeneratedEvent>(eventsAtEpochOfInterest);
						
						for (GeneratedEvent otherEvent : eventsCopy)
						{
							if (otherEvent.getState() == event.getState())
							{
								if (otherEvent instanceof EventConflict) continue CheckEvents;
								
								Serializable otherValue = otherEvent.getNewValue();
								if (ObjectValueComparator.compare(event.getNewValue(), otherValue))
								{
									duplicateEvent = true;
								}
								else  // Replace the other Event with a Conflict Event.
								{
									EventConflict conflict = new EventConflict((GeneratedEvent)event);
									eventsAtEpochOfInterest.remove(otherEvent);
									eventsAtEpochOfInterest.add(conflict);
									
									if (returnEventSers)
										eventSersAtEpochOfInterest.add((GeneratedEventSer)(conflict.externalize()));
								
									continue CheckEvents;
								}
							}
						}
						
						if (! duplicateEvent) eventsAtEpochOfInterest.add(event);
						
						if (returnEventSers)
						{
							EventSer eventSer = event.externalize();
							
							eventSersAtEpochOfInterest.add((GeneratedEventSer)(eventSer));
						}
					}
					
					if (eventsAtEpochOfInterest.size() > elements.size())
						throw new InternalEngineError(
							"Mismatched no. of elements in result row");
				}
			}
			
			arrayOfListOfEventsOfInterest[iteration-1] = eventsAtEpochOfInterest;
			if (returnEventSers) eventSers[iteration-1] = eventSersAtEpochOfInterest;
			
			if (eventsAtEpochOfInterest == null) throw new RuntimeException(
				"Event list is null for iteration " + iteration);
		}
		
		if (arrayOfListOfEventsOfInterest.length == 0) throw new RuntimeException(
			"Array length is zero");
		
		return arrayOfListOfEventsOfInterest;
	}	
	
	
	protected Conduit createConduit_internal(boolean confirm, 
		String parentId, // may be null
		String portAId, String portBId)
	throws
		Warning,
		CannotObtainLock,
		ParameterError
	{
		ModelContainer container = null;
		
		PersistentNode parentNode = PersistentNodeImpl.getNode(parentId);
		if (parentNode != null)
		{
			container = (ModelContainer)parentNode;
		
			if (! (parentNode instanceof ModelContainer)) throw new ParameterError(
				"Can only call this method on a Model Container.");
		}
		
		PersistentNode na = PersistentNodeImpl.getNode(portAId);
		if (na == null) throw new ParameterError(
			"Cannot identify the Node with ID " + portAId);
		
		if (! (na instanceof Port)) throw new ParameterError(
			"Node " + portAId + " is not a Port");
		
		Port portA = (Port)na;
		
		if (! confirm)
		{
			if (portA.getModelDomain().dependentSimulationRunsExist())
				throw new Warning("Simulation Runs will be invalid and will be deleted");
		}
		
		PersistentNode nb = PersistentNodeImpl.getNode(portBId);
		if (nb == null) throw new ParameterError(
			"Cannot identify the Node with ID " + portBId);
		
		if (! (nb instanceof Port)) throw new ParameterError(
			"Node " + portBId + " is not a Port");
		
		Port portB = (Port)nb;
		
		if (portA.isLockedForSimulation()) 
			throw new CannotModifyDuringSimulation(portA.getFullName());
			
		if (portB.isLockedForSimulation()) 
			throw new CannotModifyDuringSimulation(portB.getFullName());
			
		if (container == null)
			container = portA.chooseContainerForConduit(portB);
		
		checkIfTemplateInstance(container);
		checkIfTemplate(container);

		return container.createConduit(container.createUniqueChildName(
			portA.getName() + "_" + portB.getName()), portA, portB, null, null);
	}


	/**
	 * If elt or any owner of elt is a TemplateInstance, throw a ParameterError.
	 */
	 
	protected void checkIfTemplateInstance(PersistentNode elt)
	throws
		ParameterError
	{
		for (;;)
		{
			if (elt instanceof TemplateInstance)
				if (((TemplateInstance)elt).getTemplate() != null) throw new ParameterError(
				"Cannot change a TemplateInstance.");
			
			elt = elt.getParent();
			if (elt == null) return;
		}
	}

	
	/**
	 * If elt or any owner of elt is a Template, and if that Template 
	 * (or Templates) is (are) referenced by any TemplateInstances, then
	 * throw a ParameterError.
	 */
	 
	protected void checkIfTemplate(PersistentNode elt)
	throws
		ParameterError
	{
		for (;;)
		{
			if (elt instanceof Template)
			{
				if (((Template)elt).getInstances().size() > 0)
				{
					throw new ParameterError(
						"Cannot change the Template: it is in use.\t" +
						"Instead, define a new Template.");
				}
			}
			
			elt = elt.getParent();
			if (elt == null) return;
		}
	}
			


	/* *************************************************************************
	 * *************************************************************************
	 *
	 * Diagnostic methods.
	 *
	 */

	
	/** ************************************************************************
	 * 
	 */
	
	void assertTrue(String msg, boolean condition)
	{
		if (! condition) GlobalConsole.println("ASSERTION FAILURE: " + msg);
	}
}

