/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import java.util.*;
import java.io.*;
import java.text.DateFormat;
import expressway.common.*;
import expressway.common.ClientModel.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import expressway.server.NamedReference.*;
import geometry.*;
import generalpurpose.XMLTools;
import generalpurpose.DateAndTimeUtils;
import java.util.Collection;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.Vector;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.apache.commons.math.distribution.*;


public class ModelScenarioSetImpl extends ModelElementImpl implements ModelScenarioSet
	// is a Hierarchy (of ModelScenarios)
	// is a ModelScenarioChangeListener
{
	private HierarchyScenarioSetHelper helper;
	
	
	public Date startingDate = null;
	public Date finalDate = null;
	public int iterationLimit = 0;
	public long duration = 0;
	
	
	public ModelScenarioSetImpl(String name, ModelAttribute attr)
	{
		super(name, attr.getModelDomain(), attr.getModelDomain());
		this.helper = new HierarchyScenarioSetHelper(this, new SuperScenarioSet());
		helper.setVariableAttribute(attr);
	}
	
	
	public ModelScenarioSetImpl(ModelScenario scenarioCopied, String name, ModelAttribute attr, 
		SortedSet<ModelScenario> scenarios)
	{
		this(name, attr);
		helper.setScenarioThatWasCopied(scenarioCopied);
		
		for (ModelScenario scenario : scenarios) this.addScenario(scenario);
	}


	ModelScenarioSetImpl(ModelScenario scenarioCopied, String name, ModelAttribute attr)
	{
		this(name, attr);
		this.setScenarioThatWasCopied(scenarioCopied);
	}
	
	
	public Class getSerClass() { return ModelScenarioSetSer.class; }
	
	
	public String getTagName() { return "model_scenario_set"; }
	

	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ModelScenarioSetIconImageName);
	}

	
  /* From PersistentNode */


	public int getNoOfHeaderRows() { return helper.getNoOfHeaderRows(); }
	
	
	public double getPreferredWidthInPixels() { return helper.getPreferredWidthInPixels(); }
	

	public double getPreferredHeightInPixels() { return helper.getPreferredHeightInPixels(); }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		ModelScenarioSetImpl newInstance = (ModelScenarioSetImpl)(helper.clone(cloneMap, cloneParent));
		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		helper.writeAsXML(writer, indentation);
	}
	

	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
		super.writeSpecializedXMLAttributes(writer);
		DateFormat df = DateAndTimeUtils.DateTimeFormat;
		writer.print(
			  (this.startingDate == null? "" : ("start_date=\"" + df.format(this.startingDate) + "\" "))
			+ (this.finalDate == null? "" : ("end_date=\"" + df.format(this.finalDate) + "\" "))
			+ "iteration_limit=\"" + iterationLimit + "\" "
			+ "duration=\"" + duration + "\" ");
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
		super.writeSpecializedXMLElements(writer, indentation);
	}
	

	public void prepareForDeletion()
	{
		helper.prepareForDeletion();
		List<Scenario> ss = getScenarios();
		for (Scenario s : ss) ((ModelScenario)s).modelScenarioSetDeleted(this);
		
		// Nullify all references.
		startingDate = null;
		finalDate = null;
		helper = null;
	}


  /* From PersistentNodeImpl */


	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		ModelScenarioSetSer ser = (ModelScenarioSetSer)(helper.externalize(nodeSer));
		ser.startingDate = startingDate;
		ser.finalDate = finalDate;
		ser.iterationLimit = iterationLimit;
		ser.duration = duration;
		return helper.externalize(nodeSer);
	}
	
		
  /* From ModelScenarioSet */


	public List<ModelScenario> getModelScenarios()
	{
		List<ModelScenario> sss = new Vector<ModelScenario>();
		List<Scenario> ss = getScenarios();
		for (Scenario s : ss) sss.add((ModelScenario)s);
		return sss;
	}
	
	
	public boolean getExternalStateBindingType()
	{
		return getModelDomain().getExternalStateBindingType();
	}
	
	
	public TimePeriodType getExternalStateGranularity()
	{
		return getModelDomain().getExternalStateGranularity();
	}
	
	
	public double[] computeAssurances()
	throws
		ModelContainsError
	{
		double[] assurances = new double[getScenarios().size()];
		
		// For each Scenario of the ScenarioSet,
		
		int i = 0;
		List<ModelScenario> mss = getModelScenarios();
		for (ModelScenario scenario : mss)
		{
			// Compute the Assurance.
			
			double assurance = 0.0;
			try { assurance = scenario.getNetValueAssurance(); }
			catch (ElementNotFound ex) { throw new ModelContainsError(ex); }
			
			assurances[i++] = assurance;
		}
		
		return assurances;
	}
	
	
	public ContinuousDistribution getDistributionOfMeanFor(State state)
	throws
		ModelContainsError,
		ElementNotFound
	{
		// Fit a gamma Distribution to the mean value of the State for each
		// Scenario in this ScenarioSet.
		
		double[] means = new double[getScenarios().size()];
		int i = 0;
		List<ModelScenario> mss = getModelScenarios();
		for (ModelScenario scenario : mss)
		{
			double mean = scenario.getMeanFor(state);
			means[i++] = mean;
		}
		
		double meanMean = 0.0;
		double meanSquareSum = 0.0;
		for (double mean : means) meanMean += mean;
		meanMean = meanMean / means.length;
		
		double meanVariance = 0.0;
		for (double mean : means) meanVariance += ((meanMean-mean)*(meanMean-mean));
		meanVariance /= means.length;		
		
		// Properties of Gamma:
		// sigma^2 = scale^2 * shape
		// mean = scale * shape
		
		double shape = meanMean * meanMean / meanVariance;
		double scale = meanMean / shape;
		
		if (shape <= 0.0) throw new ModelContainsError(
			"Gamma distribution shape is negative");
		
		GammaDistributionImpl dist = new GammaDistributionImpl(shape, scale);
		return dist;
	}
	
	
	public void resetCachedStatistics()
	{
	}


	public Date getStartingDate() { return startingDate; }
	
	
	public void setStartingDate(Date newDate) { this.startingDate = newDate; }
	
	
	public Date getFinalDate() { return finalDate; }
	
	
	public void setFinalDate(Date newDate) { this.finalDate = newDate; }
	
	
	public long getDuration() { return duration; }
	
	
	public void setDuration(long dur) { this.duration = dur; }
	
	
	public int getIterationLimit() { return iterationLimit; }

	
	public void setIterationLimit(int limit) { this.iterationLimit = limit; }
	
	
	public Date getApplicableStartingDate() { return this.startingDate; }
	
	
	public Date getApplicableFinalDate() { return this.finalDate; }
	
	
	public long getApplicableDuration() { return this.duration; }
	
	
	public int getApplicableIterationLimit() { return this.iterationLimit; }
	
	
  /* From HierarchyScenaioSet */


	public List<HierarchyScenario> getHierarchyScenarios()
	{
		return helper.getHierarchyScenarios();
	}
	
	
  /* From HierarchyElement */
	
	
	public HierarchyAttribute createHierarchyAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}


	public HierarchyAttribute createHierarchyAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (HierarchyAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
	
	
	public HierarchyAttribute createHierarchyAttribute(HierarchyAttribute attr,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyElementHelper.createHierarchyAttribute(this, attr, layoutBound,
			outermostAffectedRef);
	}
	
	
	public HierarchyDomain getHierarchyDomain()
	{
		return (HierarchyDomain)(getDomain());
	}
	
	
  /* From Hierarchy */
	
	
	public int getNoOfSubHierarchies() { return helper.getNoOfSubHierarchies(); }
	
	public Hierarchy getChildHierarchyAt(int index) throws ParameterError
	{ return helper.getChildHierarchyAt(index); }
	
	public int getIndexOfChildHierarchy(Hierarchy child) { return helper.getIndexOfChildHierarchy(child); }
	
	public void insertChildHierarchyAt(Hierarchy child, int index) throws ParameterError
	{ helper.insertChildHierarchyAt(child, index); }
	
	public boolean appendChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.appendChildHierarchy(child); }
		
	public boolean removeChildHierarchy(Hierarchy child) throws ParameterError
	{ return helper.removeChildHierarchy(child); }
		
		
	public void setReasonableLocation()
	{
		HierarchyHelper.setReasonableLocation(this);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			layoutBound, outermostAffectedRef);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position);
	}
		
		
	public Hierarchy createSubHierarchy(String name)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name);
	}
	
	
	public Hierarchy createSubHierarchy(String name, int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, copiedNode);
	}
	
	
	public Hierarchy createSubHierarchy(int position, Scenario scenario,
		PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, position, scenario, copiedNode);
	}
	
	 
	public Hierarchy createSubHierarchy(String name, int position, 
		Scenario scenario, HierarchyTemplate template)
	throws
		ParameterError
	{
		return HierarchyHelper.createSubHierarchy(this, name, position,
			scenario, template);
	}
	
	
	public void deleteHierarchy(Hierarchy hier)
	throws
		ParameterError
	{
		HierarchyHelper.deleteHierarchy(this, hier);
	}
	
	
	public void changePosition(int pos)
	throws
		ParameterError
	{
		HierarchyHelper.changePosition(this, pos);
	}
	
	
	public int getPosition()
	throws
		ParameterError
	{
		return HierarchyHelper.getPosition(this);
	}
	
	
	public Hierarchy getPriorHierarchy()
	{
		return HierarchyHelper.getPriorHierarchy(this);
	}
	
	
	public Hierarchy getNextHierarchy()
	{
		return HierarchyHelper.getNextHierarchy(this);
	}
	
	
	public int getNoOfHierarchies()
	{
		return HierarchyHelper.getNoOfHierarchies(this);
	}
	
	
	public Hierarchy getParentHierarchy()
	{
		return HierarchyHelper.getParentHierarchy(this);
	}
	
	
	public void setParentHierarchy(Hierarchy parent)
	throws
		ParameterError
	{
		HierarchyHelper.setParentHierarchy(this, parent);
	}
	
	
	public final List<Hierarchy> getSubHierarchies()
	{
		List<Scenario> ss = helper.getScenarios();
		List<Hierarchy> hs = new Vector<Hierarchy>();
		for (Scenario s : ss) hs.add((Hierarchy)s);
		return hs;
	}
	
	
	public List<Hierarchy> getSubHierarchies(Class kind)
	{
		return HierarchyHelper.getSubHierarchies(this, kind);
	}
	
	
	public Hierarchy getSubHierarchy(String name)
	throws
		ElementNotFound
	{
		return HierarchyHelper.getSubHierarchy(this, name);
	}
	
	
  /* From PersistentListNode */
	
	
	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{
		return helper.getOrderedChild(name);
	}
	
	public PersistentNode getOrderedChildAt(int index) throws ParameterError
	{
		return helper.getOrderedChildAt(index);
	}
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		return helper.getIndexOfOrderedChild(child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError
	{
		helper.insertOrderedChildAt(child, index);
	}
	
	public boolean appendOrderedChild(PersistentNode child) throws ParameterError
	{
		return helper.appendOrderedChild(child);
	}
	
	public boolean removeOrderedChild(PersistentNode child) throws ParameterError
	{
		return helper.removeOrderedChild(child);
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{
		return helper.getOrderedNodesOfKind(c);
	}
	
	public void removeOrderedNodesOfKind(Class c)
	{
		helper.removeOrderedNodesOfKind(c);
	}
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{
		return helper.addAllAsOrderedNodes(nodes);
	}

	public List<PersistentNode> getOrderedChildren()
	{
		return helper.getOrderedChildren();
	}
	
	public int getNoOfOrderedChildren()
	{
		return helper.getNoOfOrderedChildren();
	}
		
		
	public PersistentNode getFirstOrderedChild()
	{
		return ListNodeHelper.getFirstOrderedChild(this);
	}
	
	
	public PersistentNode getLastOrderedChild()
	{
		return ListNodeHelper.getLastOrderedChild(this);
	}
	
	
	public int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildPosition(this, child);
	}


	public PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildBefore(this, child);
	}
	
	
	public PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError
	{
		return ListNodeHelper.getOrderedChildAfter(this, child);
	}
	
	
	public PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError
	{
		return ListNodeHelper.createListChild(this, nodeBefore, nodeAfter,
			scenario, copiedNode);
	}


	public void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildBefore(this, child, nextChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}


	public void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError
	{
		ListNodeHelper.addOrderedChildAfter(this, child, priorChild);
		if (child instanceof Hierarchy) ((Hierarchy)child).setReasonableLocation();
	}
	
	
  /* From ScenarioSet */


	public Scenario derivedFrom() { return helper.derivedFrom(); }
	
	
	public void setVariableAttribute(Attribute attr)
	{
		helper.setVariableAttribute(attr);
	}
	
	
	public Attribute getVariableAttribute() { return helper.getVariableAttribute(); }
	
	
	public void setScenarios(List<Scenario> scens)
	{
		helper.setScenarios(scens);
	}


	public List<Scenario> getScenarios()
	{
		return helper.getScenarios();
	}
	
	
	public void addScenario(Scenario scen)
	{
		helper.addScenario(scen);
		((ModelScenario)scen).setModelScenarioSet(this);
	}
	
	
	public void removeScenario(Scenario scen)
	{
		helper.removeScenario(scen);
		((ModelScenario)scen).setModelScenarioSet(null);
	}

	
	public void setScenarioThatWasCopied(Scenario scenario) { helper.setScenarioThatWasCopied(scenario); }
	
	
	public Scenario getScenarioThatWasCopied() { return helper.getScenarioThatWasCopied(); }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		this.helper = new HierarchyScenarioSetHelper(this, new SuperScenarioSet());
	}
	
	
  /* From ModelScenarioChangeListener */

	
	public void modelScenarioDeleted(ModelScenario scenario)
	{
		helper.removeScenario(scenario);
	}
	
	
  /* From HierarchyScenarioChangeListener */
	

	public void hierarchyScenarioDeleted(HierarchyScenario scen)
	{
		helper.hierarchyScenarioDeleted(scen);
	}

	
  /* Proxy classes needed by HierarchyScenarioSetHelper */

	
	private class SuperScenarioSet extends SuperPersistentNode
	{
	}
	
	
	private class SuperPersistentNode implements PersistentNode
	{
		public int compareTo(PersistentNode node)
		{
			throw new RuntimeException("Should not be called");
		}
		
		
		public Object clone() throws CloneNotSupportedException
		{
			throw new RuntimeException("Should not be called");
		}

		
	  /* From PersistentNode */
		
		
		public ModelEngineLocal getModelEngine()
		{ return ModelScenarioSetImpl.super.getModelEngine(); }
	
	
		public List<String> getInstantiableNodeKinds()
		{ return ModelScenarioSetImpl.super.getInstantiableNodeKinds(); }
		
		
		public List<String> getStaticCreateMethodNames()
		{ return ModelScenarioSetImpl.super.getStaticCreateMethodNames(); }
		
		
		public List<String> getInstantiableNodeDescriptions()
		{ return ModelScenarioSetImpl.super.getInstantiableNodeDescriptions(); }
		
		
		public PersistentNode createChild(String kind, 
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.createChild(kind, layoutBound, outermostAffectedRef); }
		
		
		public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.deleteChild(child, outermostAffectedRef); }
			
		
		public void deleteChild(PersistentNode child)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.deleteChild(child); }
			
			
		public Template findTemplate(String templatePath)
		throws
			ElementNotFound,
			ParameterError
		{ return ModelScenarioSetImpl.super.findTemplate(templatePath); }
		
		
		public Class getNodeKind(String className)
		throws
			ClassNotFoundException,
			ParameterError
		{ return ModelScenarioSetImpl.super.getNodeKind(className); }
			
		
		public Class getSerClass()
		{ return ModelScenarioSetImpl.super.getSerClass(); }
		
		
		public Set<Attribute> getAttributes()
		{ return ModelScenarioSetImpl.super.getAttributes(); }
		
		
		public String getTagName()
		{ throw new RuntimeException("Should not be called"); }
		
		
		public Attribute getAttribute(String attrName)
		{ return ModelScenarioSetImpl.super.getAttribute(attrName); }
		
		
		public int getAttributeSeqNo(Attribute attr)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.getAttributeSeqNo(attr); }
		

		public void setAttributeSeqNo(Attribute attr, int seqNo)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.setAttributeSeqNo(attr, seqNo); }
				
				
		public void setAttributeAsFirstInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.setAttributeAsFirstInSeq(attr); }
		
		
		public void setAttributeAsLastInSeq(Attribute attr)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.setAttributeAsLastInSeq(attr); }
		
		
		public void moveAttributeBefore(Attribute attr, Attribute nextAttr)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.moveAttributeBefore(attr, nextAttr); }
		
		
		public void moveAttributeAfter(Attribute attr, Attribute priorAttr)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.moveAttributeAfter(attr, priorAttr); }
	
		
		public int getFirstAttributeSeqNo()
		{ return ModelScenarioSetImpl.super.getFirstAttributeSeqNo(); }
		
		
		public int getLastAttributeSeqNo()
		{ return ModelScenarioSetImpl.super.getLastAttributeSeqNo(); }
		
		
		public int getNumberOfAttributes()
		{ return ModelScenarioSetImpl.super.getNumberOfAttributes(); }


		public List<Attribute> getAttributesInSequence()
		{ return ModelScenarioSetImpl.super.getAttributesInSequence(); }
		
		
		public Attribute getFirstAttribute()
		{ return ModelScenarioSetImpl.super.getFirstAttribute(); }
		
		
		public Attribute getLastAttribute()
		{ return ModelScenarioSetImpl.super.getLastAttribute(); }

		
		public Attribute getAttributeAtSeqNo(int seqNo)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.getAttributeAtSeqNo(seqNo); }


		public Attribute constructAttribute(String name, Serializable defaultValue)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.constructAttribute(name, defaultValue); }
		
	
		public Attribute constructAttribute(String name)
		{ return ModelScenarioSetImpl.super.constructAttribute(name); }
		
	
		public Attribute createAttribute(String name, Serializable defaultValue,
			PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.createAttribute(name, defaultValue,
			layoutBound, outermostAffectedRef); }
		
		
		public Attribute createAttribute(PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.createAttribute(layoutBound, outermostAffectedRef); }
	
	
		public Attribute createAttribute(String name, PersistentNode layoutBound, 
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.createAttribute(name, layoutBound, outermostAffectedRef); }
		
		
		public void deleteAttribute(Attribute attribute)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.deleteAttribute(attribute); }
			
			
		public void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.deleteAttribute(attribute, outermostAffectedRef); }
			
			
		public boolean isOwnedByMotifDef()
		{ return ModelScenarioSetImpl.super.isOwnedByMotifDef(); }
		
		
		public boolean isOwnedByActualTemplateInstance()
		{ return ModelScenarioSetImpl.super.isOwnedByActualTemplateInstance(); }
		
		
		public boolean actuallyUsesMotif(MotifDef md)
		{ return ModelScenarioSetImpl.super.actuallyUsesMotif(md); }
		
		
		public Image getLocalStoredImage(String handle)
		{ return ModelScenarioSetImpl.super.getLocalStoredImage(handle); }
		
		
		public void deleteFromAllNodesTable()
		{ ModelScenarioSetImpl.super.deleteFromAllNodesTable(); }
		
		
		public String getName()
		{ return ModelScenarioSetImpl.super.getName(); }
		
		
		public void setHTMLDescription(String html)
		{ ModelScenarioSetImpl.super.setHTMLDescription(html); }
		
		
		public String getHTMLDescription()
		{ return ModelScenarioSetImpl.super.getHTMLDescription(); }
		
		
		public String getFullName()
		{ return ModelScenarioSetImpl.super.getFullName(); }
		
		
		public String getNameRelativeTo(PersistentNode parent)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.getNameRelativeTo(parent); }
	
	
		public void setName(String name)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.setName(name); }
		
		
		public String setNameAndNotify(String newName)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.setNameAndNotify(newName); }
		
		
		public void renameChild(PersistentNode child, String newName)
		throws
			ParameterError
		{ ModelScenarioSetImpl.super.renameChild(child, newName); }
		
		
		public Domain getDomain()
		{ return ModelScenarioSetImpl.super.getDomain(); }
		
		
		public String createUniqueChildName()
		{ return ModelScenarioSetImpl.super.createUniqueChildName(); }
	
		
		public String createUniqueChildName(String baseName)
		{ return ModelScenarioSetImpl.super.createUniqueChildName(baseName); }
		
		
		public SortedSet<PersistentNode> getChildren()
		{ return ModelScenarioSetImpl.super.getChildren(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive()
		{ return ModelScenarioSetImpl.super.getChildrenRecursive(); }
		
		
		public SortedSet<PersistentNode> getChildrenRecursive(Class kind)
		{ return ModelScenarioSetImpl.super.getChildrenRecursive(kind); }
		
		
		public PersistentNode getChild(String name)
		throws
			ElementNotFound
		{ return ModelScenarioSetImpl.super.getChild(name); }
	
	
		public PersistentNode getParent()
		{ return ModelScenarioSetImpl.super.getParent(); }
		
		
		public void setParent(PersistentNode parent)
		{ ModelScenarioSetImpl.super.setParent(parent); }
		
		
		public boolean isDescendantOf(PersistentNode ancestor)
		{ return ModelScenarioSetImpl.super.isDescendantOf(ancestor); }
		
		
		public PersistentNode findNode(String qualifiedName)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.findNode(qualifiedName); }
	
	
		public PersistentNode findNode(String[] pathParts)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.findNode(pathParts); }
			
		
		public PersistentNode findFirstNestedNode(String name)
		throws
			ElementNotFound
		{ return ModelScenarioSetImpl.super.findFirstNestedNode(name); }
		
		
		public PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.getChildByRelativeQualifiedName(pathParts); }
		
		
		public void prepareForDeletion()
		{ ModelScenarioSetImpl.super.prepareForDeletion(); }
		
		
		public Display getDisplay()
		{ return ModelScenarioSetImpl.super.getDisplay(); }
		
		
		public void setVisible(boolean vis)
		{ ModelScenarioSetImpl.super.setVisible(vis); }
		
		
		public boolean isVisible()
		{ return ModelScenarioSetImpl.super.isVisible(); }
		
		
		public void layout()
		{ ModelScenarioSetImpl.super.layout(); }
		
		
		public Set<PersistentNode> getNodesWithSpecialLayout()
		{ return ModelScenarioSetImpl.super.getNodesWithSpecialLayout(); }
		
		
		public void conditionalLayout()
		{ ModelScenarioSetImpl.super.conditionalLayout(); }
		
		
		public PersistentNode layoutUpward(PersistentNode layoutBound)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.layoutUpward(layoutBound); }
	
		
		public void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
		{ ModelScenarioSetImpl.super.layout(layoutBound, outermostAffectedRef); }
	
	
		public void setPredefined(boolean pre)
		{ ModelScenarioSetImpl.super.setPredefined(pre); }
		
		
		public boolean isPredefined()
		{ return ModelScenarioSetImpl.super.isPredefined(); }
		
		
		public void setDeletable(boolean del)
		{ ModelScenarioSetImpl.super.setDeletable(del); }
		
		
		public boolean isDeletable()
		{ return ModelScenarioSetImpl.super.isDeletable(); }
		
		
		public void setMovable(boolean m)
		{ ModelScenarioSetImpl.super.setMovable(m); }
		
		
		public boolean isMovable()
		{ return ModelScenarioSetImpl.super.isMovable(); }
		
		
		public void setResizable(boolean yes)
		{ ModelScenarioSetImpl.super.setResizable(yes); }
		
		
		public boolean isResizable()
		{ return ModelScenarioSetImpl.super.isResizable(); }
		
		
		public double getWidth()
		{return ModelScenarioSetImpl.super.getWidth();  }
	
	
		public double getHeight()
		{ return ModelScenarioSetImpl.super.getHeight(); }
		
		
		public void setWidth(double width)
		{ ModelScenarioSetImpl.super.setWidth(width); }
	
	
		public void setHeight(double height)
		{ ModelScenarioSetImpl.super.setHeight(height); }
		
		
		public boolean useBorderWhenIconified()
		{ return ModelScenarioSetImpl.super.useBorderWhenIconified(); }
		
		
		public void setUseBorderWhenIconified(boolean yesOrNo)
		{ ModelScenarioSetImpl.super.setUseBorderWhenIconified(yesOrNo); }
		
		
		public Image getIconImage()
		{ return ModelScenarioSetImpl.super.getIconImage(); }
		
		
		public void setIconImage(String path)
		throws
			IOException
		{ ModelScenarioSetImpl.super.setIconImage(path); }
		
		
		public ImageIcon getImageIcon()
		{ return ModelScenarioSetImpl.super.getImageIcon(); }
		
		
		public String getIconImageHandle()
		{ return ModelScenarioSetImpl.super.getIconImageHandle(); }
		
		
		public double getIconWidth()
		{ return ModelScenarioSetImpl.super.getIconWidth(); }
		
		
		public double getIconHeight()
		{ return ModelScenarioSetImpl.super.getIconHeight(); }
		
		
		public void setLayoutManager(LayoutManager lm)
		{ ModelScenarioSetImpl.super.setLayoutManager(lm); }
		
		
		public LayoutManager getLayoutManager()
		{ return ModelScenarioSetImpl.super.getLayoutManager(); }
		
		
		public String getDefaultViewTypeName()
		{ return ModelScenarioSetImpl.super.getDefaultViewTypeName(); }
		
		public void setViewClassName(String name)
		{ ModelScenarioSetImpl.super.setViewClassName(name); }
		
		public String getViewClassName()
		{ return ModelScenarioSetImpl.super.getViewClassName(); }

		
		public boolean contains(Point p)
		{ return ModelScenarioSetImpl.super.contains(p); }
		
		
		public boolean contains(double x, double y)
		{ return ModelScenarioSetImpl.super.contains(x, y); }
		
		
		public Face[] getFaces(double x, double y)
		{ return ModelScenarioSetImpl.super.getFaces(x, y); }
		
		
		public Face[] getIconFaces(double x, double y)
		{ return ModelScenarioSetImpl.super.getIconFaces(x, y); }
		
		
		public Position findSideClosestToInternalPoint(double x, double y)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.findSideClosestToInternalPoint(x, y); }
		
		
		public Position findSideClosestToExternalPoint(Point location)
		{ return ModelScenarioSetImpl.super.findSideClosestToExternalPoint(location); }
		
		
		public void setSizeToStandard()
		{ ModelScenarioSetImpl.super.setSizeToStandard(); }
		
		
		public double getHeaderLabelHeight()
		{ return ModelScenarioSetImpl.super.getHeaderLabelHeight(); }
		
		
		public int getNoOfHeaderRows()
		{ throw new RuntimeException("Should not be called"); }
	
	
		public SortedSet<PersistentNode> getPredefinedNodes()
		{ return ModelScenarioSetImpl.super.getPredefinedNodes(); }
		
		
		public double getMinimumWidth()
		{ return ModelScenarioSetImpl.super.getMinimumWidth(); }
		
		
		public double getMinimumHeight()
		{ return ModelScenarioSetImpl.super.getMinimumHeight(); }
		
		
		public double getPreferredWidthInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		public double getPreferredHeightInPixels()
		{ throw new RuntimeException("Should not be called"); }
	
		
		public int getPreferredHeaderRowHeightInPixels()
		{ return ModelScenarioSetImpl.super.getPreferredHeaderRowHeightInPixels(); }
		
		
		public double getX() { return ModelScenarioSetImpl.super.getX(); }
		public double getY() { return ModelScenarioSetImpl.super.getY(); }
		public double getScale() { return ModelScenarioSetImpl.super.getScale(); }
		public double getOrientation() { return ModelScenarioSetImpl.super.getOrientation(); }
		public void setX(double x) { ModelScenarioSetImpl.super.setX(x); }
		public void setY(double y) { ModelScenarioSetImpl.super.setY(y); }
		public void setLocation(double x, double y) { ModelScenarioSetImpl.super.setLocation(x, y); }
		public void setScale(double scale) { ModelScenarioSetImpl.super.setScale(scale); }
		public void setOrientation(double angle) { ModelScenarioSetImpl.super.setOrientation(angle); }
		
		
		public boolean allowShift(PersistentNode childNode)
		{ return ModelScenarioSetImpl.super.allowShift(childNode); }
		
		
		public long getLastUpdated()
		{ return ModelScenarioSetImpl.super.getLastUpdated(); }
		
		
		public int getVersionNumber()
		{ return ModelScenarioSetImpl.super.getVersionNumber(); }
		
		
		public String getClientThatLastModified()
		{ return ModelScenarioSetImpl.super.getClientThatLastModified(); }
		
		
		public void update()
		{ ModelScenarioSetImpl.super.update(); }
		
	
		public Date getDeletionDate()
		{ return ModelScenarioSetImpl.super.getDeletionDate(); }
		
	
		public PersistentNode getPriorVersion()
		{ return ModelScenarioSetImpl.super.getPriorVersion(); }
		
		
		public String getPriorVersionNodeId()
		{ return ModelScenarioSetImpl.super.getPriorVersionNodeId(); }
		
		
		public String getNodeId()
		{ return ModelScenarioSetImpl.super.getNodeId(); }
		
		
		public String[] getNodeIdPath()
		{ return ModelScenarioSetImpl.super.getNodeIdPath(); }
		
		
		public String getNodeIdPathString()
		{ return ModelScenarioSetImpl.super.getNodeIdPathString(); }
		
		
		public void setWatch(boolean enable)
		{ ModelScenarioSetImpl.super.setWatch(enable); }
		
		
		public boolean getWatch()
		{ return ModelScenarioSetImpl.super.getWatch(); }
	
	
		public NodeSer externalize()
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.externalize(); }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.externalize(nodeSer); }
		
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelScenarioSetImpl.super.writeAsXML(writer, indentation); }
		
		
		public void writeSpecializedXMLAttributes(PrintWriter writer)
		throws
			IOException
		{ ModelScenarioSetImpl.super.writeSpecializedXMLAttributes(writer); }
		
		
		public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
		throws
			IOException
		{ ModelScenarioSetImpl.super.writeSpecializedXMLElements(writer, indentation); }
		
		
		public String[] getAvailableHTTPFormats(String[] descriptions)
		{ return ModelScenarioSetImpl.super.getAvailableHTTPFormats(descriptions); }
		
		
		public String getWebURLString(String format)
		throws
			ParameterError
		{ return ModelScenarioSetImpl.super.getWebURLString(format); }
		
		
		public byte[] renderAsSVG(double wCmMax, double hCmMax)
		throws
			IOException
		{ return ModelScenarioSetImpl.super.renderAsSVG(wCmMax, hCmMax); }
	
		
		public java.awt.Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y,
			double pixPerModel)
		{ return ModelScenarioSetImpl.super.renderHierarchyAsLeaves(g, x, y, pixPerModel); }
		
		
		public java.awt.Dimension render(Graphics2D g, double pixPerModel)
		{ return ModelScenarioSetImpl.super.render(g, pixPerModel); }
	
	
		public void renderAsChild(Graphics2D g, double pixPerModel)
		{ ModelScenarioSetImpl.super.renderAsChild(g, pixPerModel); }
		
		
		public byte[] renderAsHTML()
		throws
			ParameterError,
			IOException
		{ return ModelScenarioSetImpl.super.renderAsHTML(); }
		
	
		public void notifyAllListeners()
		throws
			Exception
		{ ModelScenarioSetImpl.super.notifyAllListeners(); }
		
		
		public void notifyNodeDeleted()
		throws
			Exception
		{ ModelScenarioSetImpl.super.notifyNodeDeleted(); }
		
		
		public void notifyAllListeners(PeerNotice notice)
		throws
			Exception
		{ ModelScenarioSetImpl.super.notifyAllListeners(notice); }
		
		
		public String[] createNodeIdArray(Collection col)
		{ return ModelScenarioSetImpl.super.createNodeIdArray(col); }
		
		
		public String[] createNodeIdArray(Map map)
		{ return ModelScenarioSetImpl.super.createNodeIdArray(map); }
	
	
		public String getNodeIdOrNull(PersistentNode node)
		{ return ModelScenarioSetImpl.super.getNodeIdOrNull(node); }
	
	
		public void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
		{ ModelScenarioSetImpl.super.setRefArgIfNotNull(ar, value); }
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws
			CloneNotSupportedException
		{ return ModelScenarioSetImpl.super.clone(cloneMap, cloneParent); }
		
		
		public void dump(int indentation)
		{ ModelScenarioSetImpl.super.dump(indentation); }
		
		
		public String identify()
		{ return ModelScenarioSetImpl.super.identify(); }
		
		
		public void printChildren(int indentationLevel)
		{ ModelScenarioSetImpl.super.printChildren(indentationLevel); }
		
		
		public void printChildren(int indentationLevel, boolean recursive)
		{ ModelScenarioSetImpl.super.printChildren(indentationLevel, recursive); }
	
	
		public void printChildrenRecursive(int indentationLevel)
		{ ModelScenarioSetImpl.super.printChildrenRecursive(indentationLevel); }
	}
}

