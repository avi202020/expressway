/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.server.NamedReference.CrossReferenceable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.io.File;
import java.io.Serializable;

	
public interface MotifElement extends PersistentNode, CrossReferenceable
{
	interface MotifDef extends MenuOwner, Domain
	{
		Set<Template> getTemplates();
		
		Template getTemplate(String name)
		throws
			ElementNotFound;
		
		Set<Domain> getReferencingDomains();
		
		/** Return the JAR file (in the server) that contains the XML that
			defines this motif. May return null. */
		//File getJarFile();
		
		//void setJarFile(File file);
		
		/** The Java classes that comprise the code of the Motif. */
		void setMotifClassNames(String[] classNames);
	}
	
	
	interface Template extends PersistentNode, MenuOwner, TemplateInstanceChangeListener
	{
		String getHTMLDescription();
		
		void setHTMLDescription(String html);
		
		TemplateInstance createInstance(String name, PersistentNode owner)
		throws
			ParameterError;
		
		Set<TemplateInstance> getInstances();
	}
	
	
	interface TemplateInstance extends PersistentNode
	{
		/**
		 * Set this TemplateInstance's fields sufficiently to make it a valid
		 * and usable instance of the Template type. Initialize is always
		 * called on an instance that was produced by cloning a valid Template
		 * using a CloneMap. Thus, its fields will be set as the Template's are,
		 * in accordance with the CloneMap pattern. This means that if any fields
		 * should be set differently than the corresponding fields of the Template,
		 * the initialize method should perform that setting.
		 */
		 
		void initialize(Class nativeImplClass, PersistentNode layoutBound,
			PersistentNode[] outermostAffectedRef)
		throws
			ParameterError;
		
		Template getTemplate();
		
		void setTemplate(Template t)
		throws
			ParameterError;
		
		
		/** Verify that this TemplateInstance complies with the Template
			from which it was created. An Instance complies if the Instance has
			the same structure and default values as the Template. */
		void verifyAgainstTemplate()
		throws
			PatternViolation;  // if the Template is violated.
	}
	
	
	interface MotifAttribute extends MotifElement, Attribute
	{
	}
	
	
	interface MenuOwner extends MotifElement
	{
		/** The first position is position 0. */
		Action createAction(int position, String text, String desc, String javaMethodName)
			throws ParameterError;
		
		Action createAction(String text, String desc, String javaMethodName) throws ParameterError;
		
		/** The first position is position 0. */
		MenuTree createMenuTree(int position, String text, String desc) throws ParameterError;
		
		MenuTree createMenuTree(String text, String desc) throws ParameterError;
		
		void deleteMenuItem(int menuItemPos) throws ParameterError;
		
		void deleteMenuItem(MenuItem menuItem) throws ParameterError;
		
		List<MenuItem> getMenuItems();
		
		/** Return the index of the specified MenuItem, starting from 0, or return
			-1 if the MenuItem is not found in this MenuOwner's list of MenuItems. */
		int getIndexOf(MenuItem mi);
	}
	
	
	interface MenuItem extends MotifElement
	{
		/** The compositional owner of this MenuItem. */
		MenuOwner getMenuOwner();
		
		/** The MenuOwner, in a MotifDef, that defines this MenuItem. */
		MenuOwner getDefinedBy();

		void setDefinedBy(MenuOwner mo);

		String getText();
		
		void setText(String text);
		
		/** The sequential position of this MenuItem within its MenuOwner, beginning
			with 0. */
		int getMenuItemPosition();
	}
	
	
	interface Action extends MenuItem
	{
		String getJavaMethodName();
		
		void setJavaMethodName(String name);
	}
	
	
	interface MenuTree extends MenuOwner, MenuItem
	{
	}


	interface TemplateInstanceChangeListener
	{
		void templateInstanceDeleted(TemplateInstance inst);
	}
	
	
	interface MotifDefChangeListener
	{
		void motifDefDeleted(MotifDef md);
	}
}

