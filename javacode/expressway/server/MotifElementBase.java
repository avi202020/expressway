/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import java.util.*;
import java.io.Serializable;

	
public abstract class MotifElementBase extends HierarchyImpl implements MotifElement
{
	MotifElementBase(String baseName, Hierarchy parent)
	{
		super(baseName, parent);
	}
	
	
	/* *************************************************************************
	 * From PersistentNode.
	 */


	public Class getSerClass() { return MotifElementSer.class; }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		MotifElementBase newInstance = (MotifElementBase)(super.clone(cloneMap, cloneParent));
		
		return newInstance;
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	throws
		ParameterError
	{
		return new MotifAttributeImpl(name, this, defaultValue);
	}
	
	
	public Attribute constructAttribute(String name)
	{
		try { return new MotifAttributeImpl(name, this); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	
  /* From PersistentNodeImpl */
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		return super.externalize(nodeSer);
	}


  /* From MotifElement */
	
	
	MotifAttribute createMotifAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (MotifAttribute)(createAttribute(name, defaultValue, layoutBound,
			outermostAffectedRef));
	}


	MotifAttribute createMotifAttribute(PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (MotifAttribute)(createAttribute(layoutBound, outermostAffectedRef));
	}
	
	
	MotifAttribute createMotifAttribute(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return (MotifAttribute)(createAttribute(name, layoutBound, outermostAffectedRef));
	}
	
	
  /* From Hierarchy */


	public Hierarchy constructSubHierarchy(String name)
	throws
		DisallowedOperation
	{
		throw new DisallowedOperation();
	}
}

