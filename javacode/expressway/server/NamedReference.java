/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import java.util.Set;
import java.util.List;
import java.util.SortedSet;
import java.util.HashSet;
import generalpurpose.TreeSetNullDisallowed;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Serializable;


/**
 * A generic mechanism for linking Nodes of different kinds. Features
 *	Named.
 *	Type-specific.
 *	Bi-directional.
 *	Many-to-many.
 */
 
public class NamedReference extends PersistentNodeImpl
{
	/**
	 * Create a NamedReference. Any Node that participates in this Reference
	 * should maintain a link (Java reference) to this Reference. The Reference
	 * is logically owned by the Node that creates it. A Reference owner is
	 * normall a Domain. References can span Domains. The types specified for
	 * 'fromType' and 'toType' are the types that are allowed for arguments in
	 * calls to the link method.
	 */
	 
	public static NamedReference createReference(String name, String desc,
		PersistentNode owner, Class fromType, Class toType)
	throws
		ParameterError
	{
		return new NamedReference(name, desc, owner, fromType, toType);
	}
	
	
	public String getDescription() { return description; }
	
	public PersistentNode getOwner() { return getDomain(); }
	
	public Class getFromType() { return fromType; }
	
	public Class getToType() { return toType; }
	
	
	/**
	 * Create a named, directed relationship between two Nodes.
	 */
	 
	public CrossReference link(CrossReferenceable fromNode, CrossReferenceable toNode)
	throws
		ParameterError
	{
		if (! fromType.isAssignableFrom(fromNode.getClass())) throw new ParameterError(
			"Type of fromNode (" + fromNode.getClass().getName() +
			") is not compatible with the 'from' type (" + fromType.getName() +
			") of this NamedReference (" + this.getName() + ").");
		
		if (! toType.isAssignableFrom(toNode.getClass())) throw new ParameterError(
			"Type of toNode (" + toNode.getClass().getName() +
			") is not compatible with the 'to' type (" + toType.getName() +
			") of this NamedReference (" + this.getName() + ").");
		
		CrossReference cr = new CrossReferenceImpl(fromNode, toNode);
		fromNode.addRef(cr);
		toNode.addRef(cr);
		crossReferences.add(cr);
		
		try
		{
			fromNode.signalCrossReferenceCreated(cr);
			toNode.signalCrossReferenceCreated(cr);
		}
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		return cr;
	}
	
	
	public void unlink(CrossReferenceable fromNode, CrossReferenceable toNode)
	throws
		ParameterError
	{
		Set<CrossReference> crs = new HashSet<CrossReference>(fromNode.getCrossReferences());
		for (CrossReference cr : crs)
		{
			if (cr.getFromNode() == fromNode) unlink((CrossReferenceImpl)cr);
		}
		
		crs = new HashSet<CrossReference>(toNode.getCrossReferences());
		for (CrossReference cr : crs)
		{
			if (cr.getToNode() == fromNode) unlink((CrossReferenceImpl)cr);
		}

		try
		{
			fromNode.signalCrossReferenceDeleted(this, fromNode, toNode);
			toNode.signalCrossReferenceDeleted(this, fromNode, toNode);
		}
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	void unlink(CrossReference cr)
	throws
		ParameterError
	{
		if (! (crossReferences.contains(cr))) throw new ParameterError(
			"The Named Reference does not own the specified Cross Reference");
		
		cr.getFromNode().removeRef(cr);
		cr.getToNode().removeRef(cr);
		crossReferences.remove(cr);
		
		try
		{
			cr.getFromNode().signalCrossReferenceDeleted(this, cr.getFromNode(), cr.getToNode());
			cr.getToNode().signalCrossReferenceDeleted(this, cr.getFromNode(), cr.getToNode());
		}
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public Set<CrossReference> getCrossReferences() { return crossReferences; }
	

	/**
	 * Nodes that might need to participate in a NamedReference should implement
	 * this interface.
	 */
	 
	public interface CrossReferenceable extends PersistentNode
	{
		Set<CrossReference> getCrossReferences();
		
		
		void addRef(CrossReference cr)
		throws
			ParameterError;
		
		
		Set<NamedReference> getNamedReferences();
		
		
		NamedReference getNamedReference(String refName);
		
		
		Set<CrossReferenceable> getToNodes(NamedReference nr);
		
		
		Set<CrossReferenceable> getFromNodes(NamedReference nr);
		
		
		Set<CrossReferenceable> getToNodes(String refName)
		throws
			ParameterError;
		
		
		Set<CrossReferenceable> getFromNodes(String refName)
		throws
			ParameterError;
		
		
		void removeRef(CrossReference cr)
		throws
			ParameterError;
			
			
		void addCrossReferenceCreationListener(CrossReferenceCreationListener listener);
		
		void addCrossReferenceDeletionListener(CrossReferenceDeletionListener listener);
		
		List<CrossReferenceCreationListener> getCrossRefCreationListeners();
		
		List<CrossReferenceDeletionListener> getCrossRefDeletionListeners();
		
		void removeCrossReferenceListener(CrossReferenceListener listener);
		
		void signalCrossReferenceCreated(CrossReference cr)
		throws
			Exception;
		
		void signalCrossReferenceDeleted(NamedReference nr,
			CrossReferenceable fromNode, CrossReferenceable toNode)
		throws
			Exception;
	}
	
	
	public interface CrossReference extends PersistentNode
	{
		NamedReference getNamedReference();
		
		CrossReferenceable getFromNode();
		
		CrossReferenceable getToNode();
	}
	
	
	/** Instances of this should be immutable. */
	
	public interface CrossReferenceListener
	{
	}
	
	
	public interface CrossReferenceCreationListener extends CrossReferenceListener
	{
		void invokeHandler(CrossReferenceCreationContext context)
		throws
			Exception;
	}
	
	
	/** Instances of this should be immutable. */
	
	public interface CrossReferenceDeletionListener extends CrossReferenceListener
	{
		void invokeHandler(CrossReferenceDeletionContext context)
		throws
			Exception;
	}

	
	
	/**
	 * A CrossReference handler is called after the CrossReference has been created
	 * or deleted.
	 */
	 
	public interface CrossReferenceContext
	{
		/**
		 * Return the Node to which this handler is associated. This is the enclosing
		 * Node of the <reference_listener> tag.
		 */
		 
		CrossReferenceable getCrossReferenceable();
		
		NamedReference getNamedReference();
		
		CrossReferenceable getFromNode();
		
		CrossReferenceable getToNode();
	}
	
	
	public interface CrossReferenceCreationContext extends CrossReferenceContext
	{
		/**
		 * Return the CrossReference that has been created or deleted.
		 */
		 
		CrossReference getCrossReference();
	}
	
	
	public interface CrossReferenceDeletionContext extends CrossReferenceContext
	{
	}

	
  /* Implementation. */


	private NamedReference(String name, String desc, PersistentNode owner, Class fromType,
		Class toType)
	throws
		ParameterError
	{
		super(owner);
		this.setName(name);
		this.description = desc;
		this.fromType = fromType;
		this.toType = toType;
	}
	

	private String description;
	
	private Class fromType;
	
	private Class toType;
	
	
	/** Ownership */
	private Set<CrossReference> crossReferences = new HashSet<CrossReference>();
	
	
	private class CrossReferenceImpl extends PersistentNodeImpl implements CrossReference
	{
		CrossReferenceable fromNode;
		CrossReferenceable toNode;
		
		CrossReferenceImpl(CrossReferenceable fromNode, CrossReferenceable toNode)
		{
			super(NamedReference.this);
			try { this.setName(NamedReference.this.createUniqueChildName()); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
			this.fromNode = fromNode; this.toNode = toNode;
		}
		
		public String getTagName() { return "reference"; }
		
		public NamedReference getNamedReference() { return NamedReference.this; }
		
		public CrossReferenceable getFromNode() { return fromNode; }
		
		public CrossReferenceable getToNode() { return toNode; }
		
		
	  /* From PersistentNodeImpl */
		

		public Class getSerClass() { return CrossReferenceSer.class; }
		
		
		public NodeSer externalize(NodeSer nodeSer) throws ParameterError
		{
			CrossReferenceSer ser = (CrossReferenceSer)nodeSer;
			ser.namedRefName = NamedReference.this.getName();
			ser.fromNodeId = fromNode.getNodeId();
			ser.toNodeId = toNode.getNodeId();
			
			return super.externalize(ser);
		}
		
		
		public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
		throws CloneNotSupportedException
		{
			CrossReferenceImpl newInstance = (CrossReferenceImpl)(super.clone(cloneMap, cloneParent));
			newInstance.fromNode = cloneMap.getClonedNode(fromNode);
			newInstance.toNode = cloneMap.getClonedNode(toNode);
			return newInstance;
		}
	
		
		public void writeAsXML(PrintWriter writer, int indentation)
		throws
			IOException
		{
			String indentString = getIndentString(indentation);
			
			writer.println(indentString + "<reference name=\"" + this.getName() + "\" "
				+ "from=\"" + fromNode.getFullName() + " "
				+ "to=\"" + toNode.getFullName() + " "
				+ (isDeletable() ? "" : "deletable=\"false\" ")
				+ "x=\"" + this.getX() + "\" "
				+ "y=\"" + this.getY() + "\" >");
	
	
			// Write each Attribute.
			
			Set<Attribute> attrs = getAttributes();
			for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
	
			
			writer.println(indentString + "</reference>");
		}
	
		
		public void prepareForDeletion()
		{
			// Delete (per the Delete Pattern) any Nodes for which the class
			// holds a primary reference. This is how a chain of owned Nodes is deleted.
			
			// Call super.prepareForDeletion, if any.
			super.prepareForDeletion();
			
			// Call <Node-type>Deleted on each ChangeListener that listens for changes
			// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
			// holds an alias (non-owning reference) to the deleted Node, and when its
			// <deleted-Node-type>Deleted method is called, it must nullify that alias.
			
			// Nullify all references.
			fromNode = null;
			toNode = null;
		}
		
		
		public void setIconImageToDefault() throws IOException
		{
			setIconImage(NodeIconImageNames.CrossReferenceImageName);
		}
		
		
		public int getNoOfHeaderRows() { return 0; }
		
		
		public SortedSet<PersistentNode> getPredefinedNodes()
		{
			 { return new TreeSetNullDisallowed<PersistentNode>(); }
		}
		
		
		public double getPreferredWidthInPixels() { return 50; }
		
	
		public double getPreferredHeightInPixels() { return 0; }
		
		
		public Attribute constructAttribute(String name, Serializable defaultValue)
		{
			return new NamedReferenceAttribute(name, defaultValue);
		}
		
		
		public Attribute constructAttribute(String name)
		{
			return new NamedReferenceAttribute(name);
		}
	}
	
	
	public class NamedReferenceAttribute extends PersistentNodeImpl implements Attribute
	{
		private Serializable defaultValue;
		//private Serializable value;
		private AttributeValidator validator;
		
		
		public NamedReferenceAttribute(String name, Serializable defaultValue)
		{
			super(NamedReference.this);
			this.defaultValue = defaultValue;
			//value = defaultValue;
		}
		
		public NamedReferenceAttribute(String name)
		{
			super(NamedReference.this);
		}
		
		
		public String getTagName() { return "attribute"; }
		
		
		public Class getSerClass() { return NamedReferenceAttributeSer.class; }
		
		
		public NodeSer externalize(NodeSer nodeSer)
		{
			NamedReferenceAttributeSer ser = (NamedReferenceAttributeSer)(nodeSer);
			ser.defaultValue = this.defaultValue;
			//ser.value = this.value;
			return ser;
		}


		public void prepareForDeletion()
		{
			super.prepareForDeletion();
		}
		
		public Serializable getDefaultValue()
		{
			return defaultValue;
		}
		
		
		public void setDefaultValue(Serializable defaultValue)
		throws
			ParameterError
		{
			this.defaultValue = defaultValue;
		}
		
		
		public void setValue(Scenario scenario, Serializable value)
		throws
			ParameterError
		{
			throw new ParameterError("Scenarios not supported for NamedReferences");
		}
		
		
		public Serializable getValue(Scenario scenario)
		throws
			ParameterError
		{
			throw new ParameterError("Scenarios not supported for NamedReferences");
		}
		
		
		//public void setValue(Serializable value)
		//throws
		//	ParameterError
		//{
		//	this.value = value;
		//}
		
		
		//public Serializable getValue()
		//throws
		//	ParameterError
		//{
		//	return value;
		//}
	
	
		public void setValidator(AttributeValidator validator)
		{
			this.validator = validator;
		}
		
		
		public AttributeValidator getValidator()
		{
			return validator;
		}
		
		
		public void validate(Serializable newValue)
		throws
			ParameterError
		{
			if (validator != null) validator.validate(newValue);
		}
	
	
		public void writeAsXML(PrintWriter writer, int indentation, PersistentNode appliesTo)
		throws
			IOException
		{
			AttributeHelper.writeAsXML(this, writer, indentation, appliesTo);
		}

		
		public void setIconImageToDefault() throws IOException
		{
			setIconImage(NodeIconImageNames.AttributeIconImageName);
		}
		
		
		public int getNoOfHeaderRows() { return 0; }
		
		
		public SortedSet<PersistentNode> getPredefinedNodes()
		{
			 { return new TreeSetNullDisallowed<PersistentNode>(); }
		}
		
		
		public double getPreferredWidthInPixels() { return 50; }
		
	
		public double getPreferredHeightInPixels() { return 0; }
		
		
		public Attribute constructAttribute(String name, Serializable defaultValue)
		{
			return new NamedReferenceAttribute(name, defaultValue);
		}
		
		
		public Attribute constructAttribute(String name)
		{
			return new NamedReferenceAttribute(name);
		}
	}
	
	
  /* From PersistentNode */
	
	
	public String getTagName() { return "references"; }
	
	
	public Class getSerClass() { return NamedReferenceSer.class; }
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		NamedReference newInstance =
			(NamedReference)(super.clone(cloneMap, cloneParent));
		
		//try { newInstance.setName(getName()); }
		//catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		return newInstance;
	}

	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		
		writer.println(indentString 
			+ "<references name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "from_type=\"" + getFromType().getName() + "\" "
			+ "to_type=\"" + getToType().getName() + "\">");
			
		
		// Write description, if any.
		
		String desc = getHTMLDescription();
		if (desc != null)
		{
			writer.println(indentString + "\t<description>");
			writer.println(indentString + "\t\t" + desc);
			writer.println(indentString + "\t</description>");
		}
		
		writer.println();
		
		
		// Write each reference.
		
		for (CrossReference cr : crossReferences)
		{
			cr.writeAsXML(writer, indentation+1);
		}
		
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);
		
		writer.println(indentString + "</references>");
	}

	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		Set<CrossReference> crs = new TreeSetNullDisallowed<CrossReference>(crossReferences);
		try { for (CrossReference cr : crs) unlink(cr); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		try { setName(null); } catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
		fromType = null;
		toType = null;
	}
	
	
	public Attribute constructAttribute(String name, Serializable defaultValue)
	{
		return new NamedReferenceAttribute(name, defaultValue);
	}


	public Attribute constructAttribute(String name)
	{
		return new NamedReferenceAttribute(name);
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.NamedReferenceIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	public SortedSet<PersistentNode> getPredefinedNodes()
	{
		 { return new TreeSetNullDisallowed<PersistentNode>(); }
	}
	
	
	public double getPreferredWidthInPixels() { return 50; }
	

	public double getPreferredHeightInPixels() { return 0; }
	
	
  /* From PersistentNodeImpl */
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		NamedReferenceSer ser = (NamedReferenceSer)nodeSer;
		
		ser.description = description;
		ser.fromTypeName = fromType.getName();
		ser.toTypeName = toType.getName();
		ser.crossReferenceIds = createNodeIdArray(crossReferences);
		
		return super.externalize(ser);
	}
}

