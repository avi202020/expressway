/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import java.io.IOException;


public class ObjectiveFunctionImpl extends FunctionImpl implements ObjectiveFunction
{
	public ObjectiveFunctionImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
	}


	public ObjectiveFunctionImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ObjectiveFunctionIconImageName);
	}
}
