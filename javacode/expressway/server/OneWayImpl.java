/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.common.ClientModel.*;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class OneWayImpl extends ActivityBase implements OneWay
{
	/** reference only. */
	public Port inputPort = null;

	/** reference only. */
	public Port outputPort = null;
	
	/** reference only. */
	public State state = null;  // the output state.
	

	public transient String inputPortNodeId = null;
	public transient String outputPortNodeId = null;
	public transient String stateNodeId = null;


	public String getInputPortNodeId() { return inputPortNodeId; }
	public String getOutputPortNodeId() { return outputPortNodeId; }
	public String getStateNodeId() { return stateNodeId; }
	
	
	public Class getSerClass() { return OneWaySer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		inputPortNodeId = getNodeIdOrNull(inputPort);
		outputPortNodeId = getNodeIdOrNull(outputPort);
		stateNodeId = getNodeIdOrNull(state);
		
		
		// Set Ser fields.
		
		((OneWaySer)nodeSer).inputPortNodeId = this.getInputPortNodeId();
		((OneWaySer)nodeSer).outputPortNodeId = this.getOutputPortNodeId();
		((OneWaySer)nodeSer).stateNodeId = this.getStateNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		OneWayImpl newInstance = (OneWayImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.inputPort = cloneMap.getClonedNode(inputPort);
		newInstance.outputPort = cloneMap.getClonedNode(outputPort);
		newInstance.state = cloneMap.getClonedNode(state);
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<oneway name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</oneway>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(inputPort, null);
		deletePort(outputPort, null);
		deleteState(state, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		inputPort = null;
		outputPort = null;
		state = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public OneWayImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		init();
	}
	
	
	public OneWayImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}
	
	
	public String getTagName() { return "oneway"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.OneWayIconImageName);
	}
	
	
	private void init()
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		
		try
		{
			inputPort = super.createPort("input_port", PortDirectionType.input, 
				Position.left, this, null);
			outputPort = super.createPort("output_port", PortDirectionType.output, 
				Position.right, this, null);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		//inputPort.setSide(Position.left);
		//outputPort.setSide(Position.right);


		// Automatically bind the output Port to the internal State.

		try { state = super.createState("OutputState", this, null); }
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}


		try { state.bindPort(outputPort); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		inputPort.setMovable(false);
		outputPort.setMovable(false);
		state.setMovable(false);
		
		inputPort.setPredefined(true);
		outputPort.setPredefined(true);
		state.setPredefined(true);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 10.0;
	private static final double PreferredHeightInPixels = 10.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getInputPort() { return inputPort; }

	public Port getOutputPort() { return outputPort; }
	
	State getState() { return state; }


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init();
	}
}


class OneWayNativeImpl extends ActivityNativeImplBase
{
	public synchronized void start(ActivityContext context) throws Exception
	{
		super.start(context);
	}


	public synchronized void stop()
	{
		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		//if (OperationMode.TestMode)
		//	System.out.println(getOneWay().getName() + " responding...");
		
		super.respond(events);

		if (events.size() == 0) return;


		if (portHasEvent(events, getInputPort()))
		{
			// Get the current driven value of the input port.
			Serializable inVal = getActivityContext().getStateValue(getInputPort());
			
			// Drive the output port.
			if (portHasNonCompensatingEvent(events, getInputPort()))
				getActivityContext().setState(getOneWay().getState(), inVal, events);
			else
				getActivityContext().scheduleCompensationEvent(getOneWay().getState(),
					inVal, events);

		}
	}
	
	
	/**
	 * Generate an output event to match the current driven state of the input
	 * port.
	 */
	
	protected void updateOutput(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		// Get the current driven value of the input port.
		Serializable inVal = getActivityContext().getStateValue(getInputPort());
		
		// Drive the output port.
		getActivityContext().setState(getOneWay().getState(), inVal, events);
	}
	
	
	protected Port getInputPort() { return getOneWay().getInputPort(); }
	
	protected OneWayImpl getOneWay() { return (OneWayImpl)(getActivity()); }
}

