/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


/**
 * Constants for operation, including diagnostic flags.
 * For the client application, see expressway.gui.ClientGlobalValues.
 */
 
public class OperationMode
{
	public static boolean TestMode = true;
	
	
	/** If true, print outcome of each step in the process of finding a network
		adapter and IP address for the HTTP server to use. */
	public static boolean PrintNetworkInformation = false;
	
	
	public static boolean PrintNodeDeletions = false;
	
	
	public static boolean PrintSubcribingPeerListeners = false;
	
	/** If true, print all PeerNotices as they are enqueued. */
	public static boolean PrintQueuedNotices = false;
	
	
	public static boolean PrintDequeuedNotices = false;
	
	
	public static boolean PrintPeerListeners = false;
	
	

	/** If true, print all outgoing PeerNotices to the console. */
	public static boolean PrintOutgoingPeerNotices = false;
}

