/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import java.io.IOException;


public class ParameterAliasImpl extends ParameterImpl implements ParameterAlias
{
	ParameterAliasImpl(String name, DecisionPoint dp)
	throws
		ParameterError
	{
		super(name, dp);
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}
	
	
	public String getTagName() { return "parameter_alias"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.ParameterAliasIconImageName);
	}
}
