/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.Event.*;
import expressway.server.ModelElement;
import expressway.server.ModelElement.*;
import expressway.server.MotifElement;
import expressway.server.MotifElement.*;


public class PatternHelper
{
	/**
	 * Verify that the specified Template Instance has the same structure as the
	 * specified Model Template. If not, throw a PatternViolation, and include in
	 * it each identified structural mismatch.
	 */
	 
	public static void verifyStructuralConformity(TemplateInstance instance,
		Template template)
	throws
		PatternViolation
	{
		// TBD
	}
}

