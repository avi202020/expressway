/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import java.util.List;
import java.io.Serializable;


/**
 * For a Node that maintains a List of <u><b>ordered</b></u> children. Such a Node
 * may have other child Nodes that are not members of the List. In other words, for
 * certain child types, a ListNode orders its children. A ListNode may have only
 * one List, because this interface does not provide a means to distinguish
 * among multiple Lists. This type is intended for Nodes that are logical members
 * of ordered sequences, such as a list of requirements. Such a list may be
 * made hierarchical, by making a List member a PersistentListNode.
 */
 
public interface PersistentListNode extends PersistentNode
{
	/* The following twelve methods should be used to implement the other methods.
		These methods mimic the behavior of a List of Nodes. So, for example,
		the first method, getOrderedChildAt(index), behaves as the List method
		get(index) in that if there is no Node at position 'index', then the
		method throws a ArrayIndexOutOfBoundsException. However, these methods
		must be semantically complete for the ListNode: they are not mere
		accessors. */
	
	PersistentNode getOrderedChild(String name) throws ElementNotFound;
	
	/**
	 * Return the child Nodes that are members of this ListNode's List. Other
	 * child Nodes (those that are not members of the List) are not included.
	 * To retrieve all child Nodes, use the PersistentNode method 'getChildren'.
	 */
	 
	List<PersistentNode> getOrderedChildren();
	
	int getNoOfOrderedChildren();
	
	PersistentNode getOrderedChildAt(int index) throws ParameterError;
	
	int getIndexOfOrderedChild(PersistentNode child);
	
	void insertOrderedChildAt(PersistentNode child, int index) throws ParameterError;
	
	boolean appendOrderedChild(PersistentNode child) throws ParameterError;
	
	boolean removeOrderedChild(PersistentNode child) throws ParameterError;

	List<PersistentNode> getOrderedNodesOfKind(Class c);
	
	void removeOrderedNodesOfKind(Class c);
	
	boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError;
		
	/* end of implementation methods */
	
	
	
	/**
	 * Return the first ordered child of this List Node.
	 */
	 
	PersistentNode getFirstOrderedChild();
	
	
	/**
	 * Return the last ordered child of this List Node.
	 */
	 
	PersistentNode getLastOrderedChild();
	
	
	/**
	 * Return the position of the specified ordered child Node, among the ordered
	 * children of this ListNode. Non-ordered child Nodes are ignored and have no
	 * affect on the ordering or position of ordered child Nodes. Position 0 is
	 * the first position.
	 */
	 
	int getOrderedChildPosition(PersistentNode child)
	throws
		ParameterError;  // if child is not in the List.
	
	
	/**
	 * Return the ordered child Node that precedes the spedified ordered child Node.
	 */
	 
	PersistentNode getOrderedChildBefore(PersistentNode child)
	throws
		ParameterError;  // if child is not in the List.
	
	
	/**
	 * Return the ordered child Node that follows the spedified ordered child Node.
	 */
	 
	PersistentNode getOrderedChildAfter(PersistentNode child)
	throws
		ParameterError;  // if child is not in the List.
	
	
	/**
	 * Create and instantiate a new child Node for this Node, at the specified
	 * location in the list. If 'copiedNode' is null, construct a new Node, or
	 * throw ParameterError if the type of Node to be constructed is not determinable
	 * from the context. Otherwise, clone 'copiedNode'.
	 * Note: 'scenario' may be null.
	 */
	 
	PersistentNode createListChild(PersistentListNode nodeBefore,
		PersistentListNode nodeAfter, Scenario scenario, PersistentListNode copiedNode)
	throws
		ParameterError;  // if the kind cannot be instantiated in this Node.


	/**
	 * If nextChild is null, add as the last child of this ListNode.
	 */
	 
	void addOrderedChildBefore(PersistentNode child, PersistentNode nextChild)
	throws
		ParameterError;  // if the child cannot be added for any reason.


	/**
	 * If priorChild is null, add as the first child of this ListNode.
	 */
	 
	void addOrderedChildAfter(PersistentNode child, PersistentNode priorChild)
	throws
		ParameterError;  // if the child cannot be added for any reason.
}

