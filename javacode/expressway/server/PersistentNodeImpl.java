/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import expressway.ser.*;
import expressway.server.MotifElement.*;
import expressway.server.HierarchyElement.*;
import geometry.Point;
import geometry.PointImpl;
import geometry.Face;
import awt.AWTTools;
import generalpurpose.SVGUtils;
import generalpurpose.StringUtils;
import generalpurpose.ListMap;
import generalpurpose.TreeSetNullDisallowed;
import java.util.*;
import java.awt.Image;
import java.awt.Component;
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.lang.reflect.Method;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import org.apache.batik.svggen.SVGGraphics2D;


/**
 * The base class for all server-side database objects.
 * See also CrossReferenceableListNodeImpl.
 */
 
public abstract class PersistentNodeImpl implements PersistentNode
	, Peer
	, java.io.Serializable  // needed only for serializing to disk
{
  /* ***************************************************************************
   * ***************************************************************************
   * Static data.
   */
	 
	/** Initialization data for the instantiable Element Lists. Each concrete
		derived type should re-define this and set it to contain a row for
		each kind of Node that can be instantiated in this Node. Each row
		is as follows:
		<node-kind-name>, <create-method-name>, @<help-page-name>
		*/
	private static final Object[][] instantiableElements =
	{
	};
	
	
	protected static List<String> staticInstantiableNodeKinds;
	protected static List<String> staticCreateMethodNames;
	protected static List<String> staticNodeDescriptions;
	
	
	/** Each concrete derived type should contain a block like this, and
		set these static variables. */
	static
	{
		staticInstantiableNodeKinds = new Vector<String>();
		staticCreateMethodNames = new Vector<String>();
		staticNodeDescriptions = new Vector<String>();
		
		for (Object[] oa : instantiableElements)
			staticInstantiableNodeKinds.add((String)(oa[0]));
		
		for (Object[] oa : instantiableElements)
			staticCreateMethodNames.add((String)(oa[1]));
		
		for (Object[] oa : instantiableElements)
			staticNodeDescriptions.add((String)(oa[2]));
	}
	

	static final char[] DisallowedNameChars =
	{
		'|',  ',',  '@',  '%',  '>',  '<',  '-',  ')',  '(',  ':',  ';',  '.',  
		'/',  '\\','"',  '\'',  '*',  '$',  '\t', '\r', '\n'
	};
	
	
	static final String[] WebFormatNames =
	{
		"svg", "html", "applet"
	};


	static final String[] WebFormatDescriptions = 
	{
		"Scalable Vector Graphics (SVG), displayable in most modern broswers; " +
			"this format can be used for graphical depictions",
		"Hypertext Markup Language (HTML); no scripting is used, displayable " +
			"in all Web browsers",
		"Java applet: interactive access to the Expressway server. Requires " +
			"the Java plugin. Most modern browsers have the Java plugin."
	};


	public static final LayoutManager DefaultLayoutManager = new SingleColumnLayoutManager(
		15.0, 2.0, 0.0, 20.0
		// hContainerSpacing, vContainerSpacing, hNodeSpacing, vNodeSpacing
		);
	
	public static final int DefaultHeaderRowHeightInPixels = 10;

	
	public static final Position[] FacePositions = 
		{ Position.bottom, Position.right, Position.top, Position.left };


	/**  Map of node ID to node reference. */
	private static Map<String, PersistentNode> allNodes =
		new TreeMap<String, PersistentNode>();
		
		
	private static long priorUniqueId = 0;
	
	
	/** Default height of a Node when rendered as an external leaf of a hierarchy. */
	final static int DefaultLeafHeightPixels = 20;
	
	
  /* ***************************************************************************
   * ***************************************************************************
   * Mutable Object instance fields.
   */
	 
	/** reference only. */
	private Domain domain = null;
	
	/** Ownership. */
	private ListMap<String, Attribute> attributes = new ListMap<String, Attribute>();
	
	/** Ownership. */
	private LayoutManager layoutManager = null;
	
	/** Ownership. */
	public Display display = new DisplayImpl();


  /* ***************************************************************************
   * ***************************************************************************
   * Non-mutable, non-Object, and non-owned instance fields.
   */

	/** Ownership. */
	private String name = null;  // must be consistent with the name of this instance
		// in its parent's child Map.
	
	private String viewClassName;
	
	private String htmlDescription;  // may be null.
		
	private PersistentNode parent;  // may be null.
		
	private double width;

	private double height;
	
	private String iconImageHandle;
	
	private boolean iconBorder = false;
	
	private boolean visible = true;
	
	private boolean predefined = false;
	
	private boolean deletable = true;
	
	private boolean movable = true;
	
	private boolean resizable = true;
	
	private String nodeId;
		
	private Date deletionDate = null;
	
	private long lastUpdated = 0;
	
	private int versionNumber = 0;
	
	private String clientId = null;

	private boolean watchEnable = false;

	private ModelElement priorVersion = null;
	
	public final String classname = this.getClass().getName();	
	
	public transient String priorVersionNodeId = null;
	
	
  /* ***************************************************************************
   * ***************************************************************************
   * Constructors.
   */

	protected PersistentNodeImpl()
	{
		synchronized (allNodes)
		{
			this.nodeId = createUniqueId();
			this.allNodes.put(this.nodeId, this);

			try { setIconImageToDefault(); }
			catch (IOException ex) { throw new RuntimeException(ex); }
		}
	}


	protected PersistentNodeImpl(PersistentNode parent)
	{
		try { setIconImageToDefault(); }
		catch (IOException ex) { throw new RuntimeException(ex); }
		
		setConstructionParameters(parent);
	}
	
	
  /* ***************************************************************************
   * ***************************************************************************
   * Methods that can or should be extended by derived classes.
   */
   
   
	/** The the icon image for this Node to the default image for this type of Node. */
	protected abstract void setIconImageToDefault() throws IOException;
	
	
	public abstract int getNoOfHeaderRows();
	
	
	public abstract SortedSet<PersistentNode> getPredefinedNodes();
	
	
	public abstract double getPreferredWidthInPixels();
	

	public abstract double getPreferredHeightInPixels();
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		Collection<Attribute> attrs = attributes.values();
		try { for (Attribute attr : attrs) deleteAttribute(attr, null); }
		catch (ParameterError ex) { GlobalConsole.printStackTrace(ex); }
		
		// Call super.prepareForDeletion, if any.
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public boolean allowShift(PersistentNode childNode) { return true; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		priorVersionNodeId = getNodeIdOrNull(priorVersion);
		
		
		// Set Ser fields.
		
		NodeSerBase ser = (NodeSerBase)nodeSer;
		
		ser.name = this.getName();
		ser.htmlDescription = this.getHTMLDescription();
		ser.fullName = this.getFullName();
		ser.domainName = (this.getDomain() == null ? null : this.getDomain().getName());
		ser.domainId = (this.getDomain() == null ? null : this.getDomain().getNodeId());
		
		String[] ref = new String[4];
		
		ser.availableHTTPFormats = this.getAvailableHTTPFormats(ref);
		ser.width = this.getWidth();
		ser.height = this.getHeight();
		ser.iconBorder = this.useBorderWhenIconified();
		ser.iconWidth = this.getIconWidth();
		ser.iconHeight = this.getIconHeight();
		ser.iconImageHandle = this.iconImageHandle;
		ser.visible = this.isVisible();
		ser.predefined = this.isPredefined();
		ser.movable = this.isMovable();
		ser.resizable = this.isResizable();
		if (this.getNodeId() == null) throw new RuntimeException("Node Id is null");
		ser.setNodeId(this.getNodeId());
		ser.parentNodeId = 
			(this.getParent() == null) ? null : this.getParent().getNodeId();
		ser.display = this.getDisplay();
		ser.deletionDate = this.getDeletionDate();
		ser.watchEnable = this.getWatch();
		ser.priorVersionNodeId = this.getPriorVersionNodeId();
		ser.lastUpdated = this.getLastUpdated();
		ser.versionNumber = this.getVersionNumber();
		ser.childNodeIds = this.getChildNodeIds();
		ser.isOwnedByMotifDef = this.isOwnedByMotifDef();
		ser.isOwnedByActualTemplateInstance = this.isOwnedByActualTemplateInstance();
		
		Set<Attribute> attrs = new TreeSetNullDisallowed<Attribute>(attributes.values());
		
		String[] attributeNodeIds = new String[attrs.size()];
		int i = 0;
		for (Attribute attr : attrs) attributeNodeIds[i++] = attr.getNodeId();
		
		ser.attributeNodeIds = attributeNodeIds;
		
		return nodeSer;
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		return (PersistentNode)(clone(this, cloneMap, cloneParent));
	}
	
	
	public void writeSpecializedXMLAttributes(PrintWriter writer)
	throws
		IOException
	{
	}
	
	
	public void writeSpecializedXMLElements(PrintWriter writer, int indentation)
	throws
		IOException
	{
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		throw new RuntimeException("Should not be called");
	}
	
	
	/**
	 * Return the text that should be rendered when rendering this Node
	 * as a leaf in a tree-oriented visual depiction.
	 */
	 
	protected String getTextToRender()
	{
		return getName();
	}
	
	
	public Set<PersistentNode> getNodesWithSpecialLayout() { return null; }
	
	
	public double getMinimumWidth() { return 0; }
	

	public double getMinimumHeight() { return 0; }
	

	public int getPreferredHeaderRowHeightInPixels() { return DefaultHeaderRowHeightInPixels; }
	

	public String toString() { return getFullName() + "(" + getClass().getName() + ")"; }


	/**
	 * Note: See the slide "Ensuring Randomness and Repeatability".
	 */
	 
	public int compareTo(PersistentNode node)
	{
		String a = nodeId;
		String b = node.getNodeId();
		return a.compareTo(b);
	}
	
	
	public void dump(int indentation)
	{
		// Generate indentation string.
		String indentString = "";
		for (int i = 1; i <= indentation; i++) indentString = indentString + "\t";

		// Print the current element.
		GlobalConsole.println(indentString + this.classname + ": " + this.toString());
		
		Set<PersistentNode> children = getChildren();
		for (PersistentNode child : children)
		{
			child.dump(indentation+1);
		}
	}
	
	
	/**
	 * Each derived class that defines ownership references to Nodes should extend
	 * this method and add those references to the returned result. Other final methods
	 * for finding Nodes use this method.
	 */
	 
	public SortedSet<PersistentNode> getChildren()
	{
		return new TreeSetNullDisallowed<PersistentNode>(attributes.values());
	}


	/**
	 * Each derived class that defines ownership references to Nodes should extend
	 * this method and add those references to the returned result. Other final methods
	 * for finding Nodes use this method.
	 */
	 
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		PersistentNode child = attributes.get(name);
		if (child == null) throw new ElementNotFound(name);
		return child;
	}


	/**
	 * A derived class must extend this method if the class maintains a Collection
	 * that contains this Node and if the Collection maintains an index based on
	 * the names of its members. For example, if this Node belongs to a HashMap
	 * indexed on the Node's name, then when this Node is renamed, it must be
	 * removed and re-added to that HashMap, as the implementation below does
	 * for the Attribute HashMap.
	 */
	 
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		if (child == null) throw new ParameterError("child argument is null");
		try { getChild(child.getName()); }
		catch (ElementNotFound ex) { throw new ParameterError(ex); }
		
		if (newName == null) throw new ParameterError("newName argument is null");
		if (newName.equals("")) throw new ParameterError("newName argument is blank");

		if (child instanceof Attribute)
		{
			Attribute attr = (Attribute)child;
			if (attributes.containsKey(attr.getName()))
			{
				attributes.remove(attr.getName());
				attributes.put(newName, attr);
			}
		}
		
		child.setNameAndNotify(newName);
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (! child.isDeletable()) throw new ParameterError(
			"Child '" + child.getFullName() + "' is not deletable.");
		
		// Delete (per the Delete Pattern) or disconnect the Node from any other
		// child Nodes known to this class that depend on the existence of the Node
		// that is being deleted. These are normally siblings of the Node that
		// is being deleted.
		
		// Call super.deleteChild.
		baseDeleteChild(child, outermostAffectedRef);
		
		// Nullify references to the Node that this class knows about.
		Attribute attr = getAttribute(child.getName());
		if (attr == null) throw new ParameterError("Attribute " + child.getName() +	
			" not found");
		if (attr != child) throw new RuntimeException("Attribute " + child.getName() +
			" does not belong to " + this.getFullName());
		attributes.remove(attr);
	}
	
	
	public boolean isOwnedByActualTemplateInstance()
	{
		return false;
	}


	public boolean actuallyUsesMotif(MotifDef md)
	{
		if (this instanceof TemplateInstance)
			if (((TemplateInstance)this).getTemplate().getDomain() == md)
				return true;
		
		SortedSet<PersistentNode> children = getChildren();
		for (PersistentNode child : children)
		{
			if (child.actuallyUsesMotif(md)) return true;
		}
		
		return false;
	}
	

	public void setWidth(double width)
	{
		this.width = width;
		update();
	}


	public void setHeight(double height)
	{
		this.height = height;
		update();
	}
	
	
	/** */
	
	List<String> getStaticNodeKinds()
	{
		return staticInstantiableNodeKinds;
	}
	
	
	/** Return the standard Expressway Help page names that are available for the
		Nodes that can be instantiated within this Node. See the HelpWindow class
		for an explanation of what a page name is. */
	
	List<String> getStaticNodeDescriptions()
	{
		return staticNodeDescriptions;
	}
	
	
	public List<String> getStaticCreateMethodNames()
	{
		return staticCreateMethodNames;
	}
	
	
	public List<String> getInstantiableNodeKinds()
	{
		// Put all of the statics first. This includes statics from derived classes.
		List<String> names = new Vector<String>(getStaticNodeKinds());
		getDynamicInstantiableNodeKind(names);
		return names;
	}
	
	
	public List<String> getInstantiableNodeDescriptions()
	{
		return staticNodeDescriptions;
	}


	/**
	 * Draw this Node's outline and background and then draw each child's icon.
	 */
	 
	public byte[] renderAsSVG(double wCmMax, double hCmMax)
	throws
		IOException
	{
		SVGGraphics2D g = SVGUtils.getSVGGraphics();
		double pixPerModel = computePixPerModel(g, wCmMax, hCmMax);
		
		// Draw Node.
		
		render(g, pixPerModel);
		
		// Write to SVG stream.
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Writer writer = new OutputStreamWriter(baos);
		g.stream(writer);
		writer.flush();
		return baos.toByteArray();
	}
	
	
	public void layout()
	{
		if (layoutManager == null)
		{
			//setSizeToStandard();
		}
		else layoutManager.layout(this);
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = this.getName();
		this.setName(newName);
		return oldName;
	}


	public double getIconWidth()
	{
		return this.getLocalStoredImageIcon(this.iconImageHandle).getIconWidth();
	}
	
	
	public double getIconHeight()
	{
		return this.getLocalStoredImageIcon(this.iconImageHandle).getIconHeight();
	}
	
	
	/**
	 * Called by TemplateInstance.initialize when createChild instantiates
	 * a Node based on a Template. This method should normally delegate to this
	 * Node (the owner of the TemplateInstance), to perform initialization of
	 * the TemplateInstance based on the type of Node that this (the instance's
	 * owner) is. Thus, any Node type that can instantiate a TemplateInstance
	 * should override this method.
	 */
	 
	protected void initializeTemplateInstance(TemplateInstance instance)
	throws
		ParameterError
	{
		throw new RuntimeException("Template is an unexpected type");
	}
	
	
	public void setSizeToStandard()
	{
		VisualGuidance guidance = getDomain().getVisualGuidance();
		double probablePixelSize = guidance.getProbablePixelSize();
		this.setWidth(probablePixelSize * this.getPreferredWidthInPixels());
		this.setHeight(probablePixelSize * this.getPreferredHeightInPixels());
	}


	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}


	/** Accessor. */
	public void setName(String name)
	throws
		ParameterError
	{
		checkName(name);
		this.name = name;
	}
	

	public void setX(double x)
	{
		//if (x < 0.0) throw new RuntimeException("x (" + x + ") is negative");
		if (parent == null)
			if (x != 0.0) throw new RuntimeException(this.getFullName() +
				": Tring to set non-zero location (x=" + x + ") of Node with no parent");
			else ;
		//else  // parent not null
		//	if (x > parent.getWidth()) throw new RuntimeException(
		//		"For " + this.getFullName() +
		//		", x (" + x + ") is larger than parent width (" + parent.getWidth() + ")");
			
		display.setX(x);
		update();
		//notifyAllListeners();
	}
	
	
	public void setY(double y)
	{
		//if (y < 0.0) throw new RuntimeException("y (" + y + ") is negative");
		if (parent == null)
			if (y != 0.0) throw new RuntimeException(this.getFullName() + 
				": Tring to set non-zero location (y=" + y + ") of Node with no parent");
			else ;
		//else  // parent is null
			//if (y > parent.getHeight()) throw new RuntimeException(
			//	"For " + this.getFullName() +
			//	", y (" + y + ") is larger than parent height (" + parent.getHeight() + ")");
			
		display.setY(y);
		update();
		//notifyAllListeners();
	}
	
	
	public void renderAsChild(Graphics2D g, double pixPerModel)
	{
		ImageIcon icon = getImageIcon();
		if (icon == null) return;
		
		int cx = (int)(getX() * pixPerModel);
		int cy = (int)(getY() * pixPerModel);
		
		Component comp = new JPanel();
		icon.paintIcon(comp, g, cx, cy);
	}
	
	
	public String getName() { return name; }
	
	
  /* ***************************************************************************
   * ***************************************************************************
   * Final static methods
   */
	
	
	static final void setPriorUniqueNodeId(long idStart) { priorUniqueId = idStart; }
	
	
	static final void setAllNodes(Map<String, PersistentNode> nodes) { allNodes = nodes; }
	
	
	static final long getPriorUniqueNodeId() { return priorUniqueId; }
	
	
	static final Map<String, PersistentNode> getAllNodes() { return allNodes; }
	
	
	/**
	 * Return a String that consists of the number of tabs specified by 'indentation';
	 */
	 
	public static final String getIndentString(int indentation)
	{
		return StringUtils.getIndentString(indentation);
	}
	

	/** ************************************************************************
	 * Create an identifier that is unique for the scope and lifetime of the
	 * ModelEngine that contains this Persistent Node.
	 */
	 
	private static final synchronized String createUniqueId()
	{
		long newId = priorUniqueId++;
		Long newIdLong = new Long(newId);
		String newIdString = newIdLong.toString();
		return newIdString;
	}
	
	
	public static final PersistentNode getNode(String nodeId)
	{
		if (nodeId == null) return null;
		
		synchronized (allNodes)
		{
			return allNodes.get(nodeId);
		}
	}
	
	
	public static final void checkName(String newName)
	throws
		ParameterError
	{
		for (char c : DisallowedNameChars)
			if (newName.indexOf(c) >= 0) throw new ParameterError(
				"Disallowed name character: " + (c == '\n' ? "<nl>" : c));
	}
	
	
	/* *************************************************************************
	 * The clone(...) methods below implement the "Clone Map Pattern".
	 */
	
	/**
	 * A convenience method that returns null if 'node' is null, or else clones
	 * the Node using the CloneMap pattern (see slide "Clone Map Pattern").
	 */
	 
	static final <T extends PersistentNode> T cloneNodeOrNull(T node, CloneMap cloneMap,
		PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		if (node == null) return null;
		return (T)(((PersistentNodeImpl)node).clone(cloneMap, cloneParent));
	}


	/**
	 * Clone a Set of Nodes, using the CloneMap pattern.
	 * See slide "Clone Map Pattern", box "Rules for Cloning a Collection of 
	 * PersistentNodes".
	 */
	 
	static final <T extends PersistentNode> Set<T> cloneNodeSet(Set<T> set, 
		CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		Object o;
		if (set instanceof HashSet) o = ((HashSet)set).clone();
		else if (set instanceof TreeSet) o = ((TreeSet)set).clone();
		else throw new RuntimeException("Unexpected Set type");
		
		Set<T> copy = (Set<T>)o;
		copy.clear();
		if (! copy.isEmpty()) throw new RuntimeException("Map was not cleared");
		
		for (T n : set) copy.add((T)(cloneMap.getClonedNode(n)));
		
		return copy;
	}
	
	
	/**
	 * Clone a Sorted Set of Nodes, using the CloneMap pattern.
	 * See slide "Clone Map Pattern", box "Rules for Cloning a Collection of 
	 * PersistentNodes".
	 */
	 
	static final <T extends PersistentNode> SortedSet<T> cloneNodeSortedSet(SortedSet<T> set, 
		CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		Object o;
		if (set instanceof TreeSet) o = ((TreeSet)set).clone();
		else throw new RuntimeException("Unexpected Set type");
		
		SortedSet<T> copy = (SortedSet<T>)o;
		copy.clear();
		if (! copy.isEmpty()) throw new RuntimeException("Map was not cleared");
		
		for (T n : set) copy.add((T)(cloneMap.getClonedNode(n)));
		
		return copy;
	}
	
	
	/**
	 * Clone a List of Nodes, using the CloneMap pattern.
	 * See slide "Clone Map Pattern", box "Rules for Cloning a Collection of 
	 * PersistentNodes".
	 */
	 
	static final <T extends PersistentNode> List<T> cloneNodeList(List<T> list, 
		CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		Object o;
		if (list instanceof Vector) o = ((Vector)list).clone();
		else throw new RuntimeException("Unexpected List type");
		
		List<T> copy = (List<T>)o;
		copy.clear();
		if (! copy.isEmpty()) throw new RuntimeException("Map was not cleared");
		
		for (T n : list) copy.add((T)(cloneMap.getClonedNode(n)));
		
		return copy;
	}


	/**
	 * Clone an array of Nodes, using the CloneMap pattern.
	 * See slide "Clone Map Pattern", box "Rules for Cloning a Collection of 
	 * PersistentNodes".
	 */
	 
	static final <T extends PersistentNode> T[] cloneNodeArray(T[] ar,
		CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		Object o = ar.clone();
		T[] copy = (T[])o;
		
		for (int i = 0; i < ar.length; i++) copy[i] = (T)(cloneMap.getClonedNode(ar[i]));
		
		return copy;
	}


	/**
	 * Clone a Map of Nodes, using the CloneMap pattern.
	 * See slide "Clone Map Pattern", box "Rules for Cloning a Collection of 
	 * PersistentNodes".
	 */
	 
	static final <T extends PersistentNode> Map<String, T> cloneNodeMap(
		Map<String, T> map, CloneMap cloneMap, PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		Object o;
		if (map instanceof HashMap) o = ((HashMap)map).clone();
		else if (map instanceof TreeMap) o = ((TreeMap)map).clone();
		else throw new RuntimeException("Unexpected Map type");
		
		Map<String, T> copy = (Map<String, T>)o;
		copy.clear();
		if (! copy.isEmpty()) throw new RuntimeException("Map was not cleared");
		
		Set<String> keySet = map.keySet();
		for (String key : keySet) copy.put(key, (T)(cloneMap.getClonedNode(map.get(key))));
		
		return copy;
	}
	
	
	/**
	 * Make a value copy of the specified object.
	 *
	 * Intended only for Attribute values, Event values, and Decision values.
	 * These values generally are of some Object type that is not known until runtime.
	 * If such objects are mutable, they should implement Copyable.
	 */

	public static final <T extends Serializable> T copyObject(T object)
	throws
		CloneNotSupportedException
	{
		if (object == null) return null;
		if (object instanceof Copyable) return (T)(((Copyable)object).clone());
		
		Class objectClass = object.getClass();
		String className = objectClass.getName();
		String packageName = objectClass.getPackage().getName();
		
		if (  // is a known immutable type
			packageName.startsWith("java.lang")
			||
			className.equals("java.util.Date")
		)
			return object; // assume type is immutable: return reference.
		
		throw new CloneNotSupportedException("Copying by value not supported for " + 
			objectClass.getName());
	}
	
	
	public static final <T extends PersistentNode> T clone(T obj, CloneMap cloneMap,
		PersistentNode cloneParent)
	throws
		CloneNotSupportedException
	{
		Object o = cloneMap.get(obj);
		T t;
		try { t = (T)o; }
		catch (ClassCastException ex) { throw new RuntimeException(
			"Object in clone map is of wrong type"); }
		
		if (o != null) return (T)o;
		
		PersistentNodeImpl node = (PersistentNodeImpl)(obj.clone());
		node.setParent(cloneParent);
		
		if (cloneParent == null)
		{
			if (! (node instanceof Domain)) throw new RuntimeException(
				"No parent for cloned Node, but it is not a Domain");
			node.domain = (Domain)node;
		}
		else node.domain = cloneParent.getDomain();
		
		cloneMap.addClonedNode(obj, node);
		
		node.layoutManager = 
			(obj.getLayoutManager() == null? null : (LayoutManager)(obj.getLayoutManager().clone()));
		
		node.display = (Display)(((DisplayImpl)(obj.getDisplay())).clone());
		
		node.nodeId = createUniqueId();
		
		node.priorVersion = null;
		
		node.priorVersionNodeId = null;
		
		node.attributes = (ListMap<String, Attribute>)
			(cloneNodeMap(((PersistentNodeImpl)obj).attributes, cloneMap, node));
		
		allNodes.put(node.nodeId, node);
		
		return (T)node;
	}
	
	
	public static final void dumpAllNodes()
	{
		GlobalConsole.println("Nodes:");
		
		synchronized (allNodes)
		{
			Set<String> allNodeKeys = allNodes.keySet();
			for (String key : allNodeKeys)
			{
				PersistentNode n = allNodes.get(key);
				GlobalConsole.println(" " + key + ": " + (n == null? "null" : n.getName()));
			}
		}
	}


  /* ***************************************************************************
   * ***************************************************************************
   * Final instance methods
   */
	
	
	public final ModelEngineLocal getModelEngine()
	{
		return ServiceContext.getServiceContext().getModelEngine();
	}
	
	
	/**
	 * Add elements defined by each MotifDef used by this ModelDomain.
	 */
	 
	protected final void getDynamicInstantiableNodeKind(List<String> names)
	{
		Set<MotifDef> motifDefs = this.getDomain().getMotifDefs();
		for (MotifDef motifDef : motifDefs)
		{
			// Add the name of each Template in the MotifDef.
			
			Set<PersistentNode> motifElements = motifDef.getChildren();
			for (PersistentNode me : motifElements)
			{
				if (me instanceof Template)
				{
					names.add(me.getFullName());
				}
			}
		}
	}
	
	
	public final void setViewClassName(String name) { this.viewClassName = name; }
	

	public final String getViewClassName() { return viewClassName; }
	
	
	private final void setConstructionParameters(PersistentNode parent)
	{
		synchronized (allNodes)
		{
			this.nodeId = createUniqueId();
			if (this.nodeId == null) throw new RuntimeException("nodeId is null");
			
			this.allNodes.put(this.nodeId, this);
			setParent(parent);
			//this.parent = parent;
			if (parent != null) this.domain = parent.getDomain();
			else if (this instanceof Domain) this.domain = (Domain)this;
			
			//try { setIconImageToDefault(); }
			//catch (IOException ex) { throw new RuntimeException(ex); }
		}
	}
	
	
	private final void setConstructionParameters(PersistentNode parent, String baseName)
	{
		setConstructionParameters(parent);
		this.domain = parent.getDomain();
		this.name = parent.createUniqueChildName(baseName);
	}
	
	
	TemplateInstance createTemplateInstance(String kind,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ElementNotFound
	{
		throw new ElementNotFound("Template '" + kind + "'");
	}


	public final PersistentNode createChild(String kind,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Determine if the kind is a Template.
		
		try { return createTemplateInstance(kind, layoutBound, outermostAffectedRef); }
		catch (ElementNotFound enf) {} // ok

		
		// Validate the specified Node kind.
		
		String baseName = kind;
		String name = this.createUniqueChildName(baseName);
		
		boolean found = false;
		List<String> names = getInstantiableNodeKinds();
		int index = 0;
		for (String k : names)
		{
			if (k.equals(kind)) { found = true; break; }
			index++;
		}
		
		if (! found) throw new ParameterError(
			"Cannot instantiate a '" + kind + "' in a " + this.getClass().getName());
		
		// Identify and invoke the method to use to create new child Node.
		
		String methodName;
		try { methodName = getStaticCreateMethodNames().get(index); }
		catch (RuntimeException ex)
		{
			GlobalConsole.println("index=" + index);
			GlobalConsole.println("method names are:");
			List<String> ns = getStaticCreateMethodNames();
			for (String n : ns) GlobalConsole.println("\t" + n);
			throw ex;
		}
		
		try
		{
			Method method = this.getClass().getMethod(methodName, String.class, 
				PersistentNode.class, PersistentNode[].class);
			try { return (ModelElement)(method.invoke(
				this, name, layoutBound, outermostAffectedRef)); }
			catch (Exception ex) { throw new RuntimeException(ex); }
		}
		catch (NoSuchMethodException ex) {} // ok
		
		throw new ParameterError("Could not find method to create Node of kind '" + kind + "'");
	}
	
	
	public final void deleteChild(PersistentNode child)
	throws
		ParameterError
	{
		deleteChild(child, null);
	}


	public final Template findTemplate(String templatePath)
	throws
		ElementNotFound,
		ParameterError
	{
		Template template = null;
		PersistentNode me = null;
		try { me = getModelEngine().getNodeForPath(templatePath); }
		catch (ElementNotFound enf) {}
		
		if (me != null)
			if (me instanceof Template)
				if (me.getDomain() instanceof MotifDef)
					template = (Template)me;
		
		return template;
	}
	
	
	public final Class getNodeKind(String className)
	throws
		ClassNotFoundException,
		ParameterError
	{
		Class c = Class.forName(className);
		if (! (PersistentNode.class.isAssignableFrom(c))) throw new ParameterError(
			"Class with name '" + className + "' is not a PersistentNode");
		return c;
	}
	
	
	protected final void deleteNode(PersistentNode child, PersistentNode[] outermostAffectedRef)
	{
		baseDeleteChild(child, outermostAffectedRef);
	}
	
	
	protected final void deleteNode(PersistentNode child)
	{
		baseDeleteChild(child);
	}
	
	
	/**
	 * See slide "Delete Pattern".
	 */
	 
	protected final void baseDeleteChild(PersistentNode child,
		PersistentNode[] outermostAffectedRef)
	{
		// Call prepareForDeletion on the Node that is to be deleted.
		child.prepareForDeletion();
		
		// Call the layout method, if applicable, to spatially rearrange the
		// other Nodes of the deleted Node's owner.
		layout(null, outermostAffectedRef);
		
		// Call deleteFromAllNodesTable() on the deleted Node.
		child.deleteFromAllNodesTable();
		
		// Call notifyNodeDeleted on the deleted Node. This notifies clients by
		// sending them PeerNotices.
		try { child.notifyNodeDeleted(); }
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}
		
		
	protected final void baseDeleteChild(PersistentNode child)
	{
		baseDeleteChild(child, null);
	}
	
	
	public final void setRefArgIfNotNull(PersistentNode[] ar, PersistentNode value)
	{
		if (ar != null) ar[0] = value;
	}
	

	public final Set<Attribute> getAttributes()
	{
		return new TreeSetNullDisallowed<Attribute>(attributes.values());
	}


	public final int getAttributeSeqNo(Attribute attr)
	throws
		ParameterError
	{
		int index = attributes.indexOfKey(attr.getName());
		if (index < 0) throw new ParameterError("Attribute with name '" +
			attr.getName() + "' not found belonging to Node '" + getFullName() + "'");
		return index+1;
	}
	
	
	public final void setAttributeSeqNo(Attribute attr, int seqNo)
	throws
		ParameterError
	{
		String key = attr.getName();
		Attribute a = attributes.remove(key);
		if (a == null) throw new ParameterError("Attribute '" +
			attr.getName() + "' not found belonging to Node '" + getFullName() + "'");
		try { attributes.add(seqNo-1, key, attr); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	
	public final void setAttributeAsFirstInSeq(Attribute attr)
	throws
		ParameterError
	{
		attributes.remove(attr.getName());
		try { attributes.add(0, attr.getName(), attr); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	
	public final void setAttributeAsLastInSeq(Attribute attr)
	throws
		ParameterError
	{
		attributes.remove(attr.getName());
		try { attributes.add(attr.getName(), attr); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	
	public final void moveAttributeBefore(Attribute attr, Attribute nextAttr)
	throws
		ParameterError
	{
		if (attr == nextAttr) throw new ParameterError(
			"Attempt to move an Attribute before itself");
		if (attr == null) throw new ParameterError("Null attribute");
		if (nextAttr == null) throw new ParameterError("Null next attribute");
		if (attr.getName().equals(nextAttr.getName())) throw new ParameterError(
			"Attribute and next Attribute have the same name");
		
		int index = attributes.indexOfKey(nextAttr.getName());
		if (index < 0) throw new ParameterError("Attribute with name '" +
			nextAttr.getName() + "' not found belonging to '" + getFullName() + "'");
		attributes.remove(attr.getName());
		attributes.add(index, attr.getName(), attr);
	}
	
	
	public final void moveAttributeAfter(Attribute attr, Attribute priorAttr)
	throws
		ParameterError
	{
		if (attr == priorAttr) throw new ParameterError(
			"Attempt to move an Attribute after itself");
		if (attr == null) throw new ParameterError("Null attribute");
		if (priorAttr == null) throw new ParameterError("Null prior attribute");
		if (attr.getName().equals(priorAttr.getName())) throw new ParameterError(
			"Attribute and prior Attribute have the same name");
		
		int index = attributes.indexOfKey(priorAttr.getName());
		if (index < 0) throw new ParameterError("Attribute with name '" +
			priorAttr.getName() + "' not found belonging to '" + getFullName() + "'");
		attributes.remove(attr.getName());
		attributes.add(index+1, attr.getName(), attr);
	}

	
	public final int getFirstAttributeSeqNo()
	{
		if (attributes.size() == 0) return 0; else return 1;
	}
	
	
	public final int getLastAttributeSeqNo()
	{
		return attributes.size();
	}
	
	
	public final int getNumberOfAttributes()
	{
		return attributes.size();
	}

	
	public final List<Attribute> getAttributesInSequence()
	{
		return attributes.asList();
	}
	
	
	public final Attribute getFirstAttribute()
	{
		String name = attributes.getKey(0);
		if (name == null) return null;
		else return attributes.get(name);
	}
	
	
	public final Attribute getLastAttribute()
	{
		int index = attributes.size()-1;
		if (index < 0) return null;
		String name = attributes.getKey(index);
		if (name == null) return null;
		else return attributes.get(name);
	}
	
	
	public final Attribute getAttributeAtSeqNo(int seqNo)
	throws
		ParameterError // if there is no Attribute at the seqNo.
	{
		if (seqNo > attributes.size()) throw new ParameterError(
			"There is no Attribute at seqNo " + seqNo);
		
		int index = seqNo-1;
		if (index < 0) throw new ParameterError(
			"There is no Attribute at seqNo " + seqNo);
		
		String name = attributes.getKey(index);
		if (name == null) return null;
		else return attributes.get(name);
	}


	public final Attribute getAttribute(String attrName)
	{
		return attributes.get(attrName);
	}
	
	
	protected final void addAttribute(Attribute attribute)
	{
		attributes.put(attribute.getName(), attribute);
		((PersistentNodeImpl)attribute).setParent(this);
	}
	

	public final Attribute createAttribute(String name, Serializable defaultValue,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		Attribute attribute = constructAttribute(name, defaultValue);
		
		addAttribute(attribute);
		attribute.setSizeToStandard();
		attribute.layout(layoutBound, outermostAffectedRef);
		
		return attribute;
	}
	
	
	public final Attribute createAttribute(PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Create a name for the Attribute.
		
		String name = createUniqueChildName("Attr");
		
		
		// Create the Attribute.
		
		Attribute attribute = constructAttribute(name);

		addAttribute(attribute);
		attribute.setSizeToStandard();
		attribute.layout(layoutBound, outermostAffectedRef);
		
		return attribute;
	}
	
	
	public final Attribute createAttribute(String name, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		
		// Create the Attribute.
		
		Attribute attribute = constructAttribute(name, null);

		addAttribute(attribute);
		attribute.setSizeToStandard();
		attribute.layout(layoutBound, outermostAffectedRef);
		
		return attribute;
	}


	public final void deleteAttribute(Attribute attribute)
	throws
		ParameterError
	{
		deleteChild(attribute);
	}
	
	
	public final void deleteAttribute(Attribute attribute, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(attribute, outermostAffectedRef);
	}
	
	
	public final boolean isOwnedByMotifDef()
	{
		return (this.getDomain() instanceof MotifDef);
	}
	
	
	public final Image getLocalStoredImage(String handle)
	{
		return this.getModelEngine().getLocalStoredImageIcon(handle).getImage();
	}
	
	
	public final String getPriorVersionNodeId() { return priorVersionNodeId; }
	
	
	public final void deleteFromAllNodesTable()
	{
		if (OperationMode.PrintNodeDeletions)
			GlobalConsole.println("Deleting Node " + getNodeId());
		
		allNodes.remove(this.nodeId);
	}
	
	
	public final void setHTMLDescription(String desc) { this.htmlDescription = desc; }


	public final String getHTMLDescription() { return htmlDescription; }
	
	
	public final String getFullName()
	{
		String fullName = this.getName();
		for (PersistentNode p = getParent(); p != null; p = p.getParent())
		{
			fullName = p.getName() + "." + fullName;
		}
		
		return fullName;
	}


	public final String getNameRelativeTo(PersistentNode parent)
	throws
		ParameterError
	{
		String relativeName = this.getName();
		PersistentNode p = this;
		while (true)
		{
			p = p.getParent();
			
			if (p == null) throw new ParameterError(
				parent.getFullName() + " is not a parent of " + this.getFullName());
			
			if (p == parent) break;
			
			relativeName = p.getName() + "." + relativeName;
		}
		
		return relativeName;
	}


	protected final void checkForUniqueness(String name)
	throws
		ParameterError  // if the name is already used within this Node.
	{
		try
		{
			getChild(name);
			throw new ParameterError(
				"A child with the name '" + name + "' already exists.");
		}
		catch (ElementNotFound ex) {} // ok
	}


	public final String createUniqueChildName()
	{
		return createUniqueChildName(null);
	}

	
	public final String createUniqueChildName(String baseName)
	{
		if (baseName == null)
		{
			baseName = "";
		}
		else
		{
			try { getChild(baseName); }
			catch (ElementNotFound ex) { return baseName; }
		}
		
		// Create a unique name that is based on the base name.
		
		int i = 0;
		for (;;)
		{
			i++;
			String name = baseName + i;
		
			try { getChild(name); }
			catch (ElementNotFound ex) { return name; }
		}
	}
	
	
	public final Domain getDomain() { return this.domain; }
	
	
	public final void setDomain(Domain d) { this.domain = d; }
	
	
	public final SortedSet<PersistentNode> getChildrenRecursive()
	{
		return getChildrenRecursive(PersistentNode.class);
	}
	
	
	public final SortedSet<PersistentNode> getChildrenRecursive(Class kind)
	{
		return getChildrenRecursive(kind, new TreeSetNullDisallowed<PersistentNode>());
	}
	
	
	protected final SortedSet<PersistentNode> getChildrenRecursive(Class kind,
		SortedSet<PersistentNode> allChildren)
	{
		SortedSet<PersistentNode> children = this.getChildren();
		for (PersistentNode node : children)
		{
			if (kind.isAssignableFrom(node.getClass())) allChildren.add(node);
			((PersistentNodeImpl)node).getChildrenRecursive(kind, allChildren);
		}
		
		return allChildren;
	}
	

	public final PersistentNode getParent() { return this.parent; }
	
	
	public final void setParent(PersistentNode parent)
	{
		if (this == parent) throw new RuntimeException(
			"Node's parent is being set to itself");
		
		this.parent = parent;
	}


	public final boolean isDescendantOf(PersistentNode ancestor)
	{
		if (ancestor == null) throw new RuntimeException("null argument");
		if (this == ancestor) return false;  // I cannot be my own grandfather.
		
		PersistentNode par = this;
		for (;;)
		{
			par = par.getParent();
			if (par == null) return false;
			if (par == ancestor) return true;
		}
	}
	

	public final PersistentNode findNode(String qualifiedName)
	throws
		ParameterError
	{
		if (name == null) throw new ParameterError("Null qualified name");


		// Decompose the qualifiedName String into a List of distinct simple names.

		String[] pathParts = qualifiedName.split("\\.");

		return findNode(pathParts);
	}
		

	public final PersistentNode findNode(String[] pathParts)
	throws
		ParameterError
	{
		// Given a path Path composed of a sequence of 0 or more path parts,
		// and given a starting context (method target) of this Node,
		
		if (pathParts.length == 0) return null;
		
		PersistentNode child = getChildByRelativeQualifiedName(pathParts);

		if // NOT found within this context, so go up a level
			(child == null)
		{
			PersistentNode par = (ModelElement)(this.getParent());
			if (par == null)
			{
				Domain d;
				try { d = this.getModelEngine().getDomainPersistentNode(pathParts[0]); }
				catch (ElementNotFound enf) { return null; }
				catch (ParameterError pe) { throw pe; }
				catch (Exception ex) { throw new RuntimeException(ex); }
				
				if (pathParts.length == 1) return d;
				
				String[] remainingParts = new String[pathParts.length-1];
				for (int i = 1; i < pathParts.length; i++)
					remainingParts[i-1] = pathParts[i];
				
				child = d.getChildByRelativeQualifiedName(remainingParts);
				if (child != null) return child;
				
				return null;
			}

			// Call recursively with the new Context being C's parent
			return par.findNode(pathParts);
		}
		else
			return child;
	}


	public final PersistentNode findFirstNestedNode(String name)
	throws
		ElementNotFound
	{
		if (name.equals(this.getName())) return this;
		
		Set<PersistentNode> children = getChildren();
		for (PersistentNode child : children)
		{
			try { return findFirstNestedNode(name); }
			catch (ElementNotFound ex) {} // ok
		}
		
		throw new ElementNotFound("Node named '" + name + "'");
	}


	public final PersistentNode getChildByRelativeQualifiedName(String[] pathParts)
	throws
		ParameterError
	{
		if (pathParts.length == 0) return null;
		
		Set<PersistentNode> children = this.getChildren();
		
		for (PersistentNode child : children)
		{
			if (child.getName().equals(pathParts[0]))
			{
				if (pathParts.length == 1) return child;
				
				String[] remainingParts = new String[pathParts.length-1];
				for (int i = 1; i < pathParts.length; i++)
					remainingParts[i-1] = pathParts[i];
				
				return child.getChildByRelativeQualifiedName(remainingParts);
			}
		}
		
		return null;
	}
	
	
	public final long getLastUpdated() { return lastUpdated; }
	
	
	public final int getVersionNumber() { return versionNumber; }
	
	
	public final String getClientThatLastModified() { return clientId; }
	
	
	final String getClientId() { return ServiceContext.getServiceContext().getClientId(); }
	
	
	final void setClientId()
	{
		this.clientId = ServiceContext.getServiceContext().getClientId();
	}


	public final void update()
	{
		this.versionNumber++;
		lastUpdated = System.currentTimeMillis();
		this.clientId = ServiceContext.getServiceContext().getClientId();
	}
	
	
	public final NodeSer externalize() throws ParameterError
	{
		// Construct and return a serializable copy of this Node.
		
		NodeSer nodeSer = null;
		try { nodeSer = (NodeSer)(getSerClass().newInstance()); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		return externalize(nodeSer);
	}
	
	
	/**
	 * Any children that are Hierarchies are placed first in the array that is
	 * returned.
	 */
	 
	protected final String[] getChildNodeIds()
	{
		Set<PersistentNode> children = this.getChildren();
		String[] childNodeIds = new String[children.size()];
		
		if (this instanceof PersistentListNode)  // sort children by ascending position.
		{
			int endPos = children.size()-1;
			for (PersistentNode child : children)
			{
				if (child instanceof Hierarchy)
				{
					Hierarchy chier = (Hierarchy)child;
					try { childNodeIds[chier.getPosition()] = child.getNodeId(); }
					catch (ParameterError pe) { throw new RuntimeException(pe); }
				}
				else
				{
					childNodeIds[endPos] = child.getNodeId();  // put at the end.
					endPos--;
				}
			}
		}
		else
		{
			int i = 0;
			for (PersistentNode child : children)
			{
				if (child == null) throw new RuntimeException("child is null");
				childNodeIds[i] = child.getNodeId();
				i++;
			}
		}
		
		return childNodeIds;
	}
	
	
	/**
	 * Write Attributes for any of the predefined child Nodes. Only one level
	 * is traversed. Do not write the predefined child Nodes themselves.
	 */
		
	protected final void writeXMLAttributesForPredefElements(PrintWriter writer,
		int indentation)
	throws
		IOException
	{
		Set<PersistentNode> children = getChildren();
		for (PersistentNode child : children)
		{
			if (child.isPredefined())
			{
				Set<Attribute> attrs = child.getAttributes();
				for (Attribute attr : attrs)
					attr.writeAsXML(writer, indentation, child);
			}
		}
	}
	
	
	public final String[] getAvailableHTTPFormats(String[] descriptions)
	{
		if (descriptions.length < WebFormatDescriptions.length) throw new RuntimeException(
			"'descriptions' argument is not long enough");
		
		for (int i = 0; i < descriptions.length; i++)
		{
			if (i > WebFormatDescriptions.length-1)
				descriptions[i] = null;
			else
				descriptions[i] = WebFormatDescriptions[i];
		}
		
		return WebFormatNames;
	}


	public final String getWebURLString(String format)
	throws
		ParameterError
	{
		InetSocketAddress inetSocketAddress = 
			getModelEngine().getHttpManager().getInetSocketAddress();
		
		String serverName = inetSocketAddress.getHostName();
		String portStr = Integer.toString(inetSocketAddress.getPort());
		
		return URLHelper.createWebURLString(serverName, portStr, format,
			new String[] { getNodeId() });
	}
	
	
	protected final double computePixPerModel(Graphics2D g, double wCmMax, double hCmMax)
	{
		return RenderingHelper.computePixPerModel(g, this, wCmMax, hCmMax);
	}
	
	
	public final Dimension renderHierarchyAsLeaves(Graphics2D g, int x, int y, double pixPerModel)
	{
		renderAsLeafAt(g, x, y);
		int elementHeight = DefaultLeafHeightPixels;
		return RenderingHelper.drawChildHierarchy(this, elementHeight, g, x, y, pixPerModel);
	}
	
	
	public final java.awt.Dimension render(Graphics2D g, double pixPerModel)
	{
		// Draw background.
		
		g.setColor(Color.white);
		GraphicsConfiguration gc = g.getDeviceConfiguration();
		Rectangle bounds = gc.getBounds();
		g.fillRect(0, 0, bounds.width, bounds.height);
		
		// Draw border.
		
		g.setColor(Color.black);
		Stroke stroke = new BasicStroke(4);
		g.setStroke(stroke);
		int w = (int)(this.getWidth() * pixPerModel);
		int h = (int)(this.getHeight() * pixPerModel);
		g.drawRect(0, 0, w, h);
		
		
		// Draw each child icon.
		
		SortedSet<PersistentNode> children = getChildren();
		for (PersistentNode child : children)
		{
			child.renderAsChild(g, pixPerModel);
		}
		
		return new java.awt.Dimension(w, h);
	}


	protected final Dimension renderAsLeafAt(Graphics2D g, int x, int y)
	{
		String text = getTextToRender();
		Font font = Font.getFont("Arial");
		FontMetrics fm = g.getFontMetrics(font);
		final int ElementWidth = fm.stringWidth(text) + 10;
		g.setColor(Color.black);
		Stroke stroke = new BasicStroke(4);
		g.setStroke(stroke);
		g.drawRect(x, y, ElementWidth, DefaultLeafHeightPixels);
		g.drawString(text, x + 5, y + fm.getAscent());
		return new Dimension(ElementWidth, DefaultLeafHeightPixels);
	}
	
	
	public final byte[] renderAsHTML()
	throws
		ParameterError,
		IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(baos);

		pw.println("<table>");
		
		List<Method> allChildMethods = new Vector<Method>();
		SortedSet<PersistentNode> children = getChildren();
		for (PersistentNode child : children)
		{
			Method[] methods = child.getClass().getMethods();
			for (Method method : methods)
			{
				if (method.getParameterTypes().length == 0)
				{
					allChildMethods.add(method);
				}
			}
		}
		
		pw.print("<tr>");
		for (Method method : allChildMethods)
		{
			pw.print("<td>");
			pw.print(method.getName());
			pw.print("</td>");
		}
		pw.print("</tr>");
		
		for (PersistentNode child : children)
		{
			pw.print("<tr>");
			for (Method method : allChildMethods)
			{
				pw.print("<td>");
				try
				{
					Method m = child.getClass().getMethod(method.getName());
					Object result;
					try
					{
						result = m.invoke(child);
						pw.print(result.toString());
					}
					catch (Exception ex)
					{
						pw.print("(error)");
					}
				}
				catch (NoSuchMethodException ex) {} // ok
				
				pw.print("</td>");
			}
			pw.println("</tr>");
		}
		
		pw.println("</table>");

		pw.flush();
		return baos.toByteArray();
	}
	
	
	/** ************************************************************************
	 * If the specified Object is not an instance of Class c, throw an Exception;
	 * otherwise, return the Object.
	 */
	 
	protected final Object checkInstanceCompatibility(Object object, Class c)
	throws
		ParameterError  // if the Object is not an instance of Class c.
	{
		if (object == null) throw new ParameterError(
			"Object is null.");
		
		if (! c.isInstance(object)) throw new ParameterError(
			"Object is not a " + c.getName() + "; it is a " + object.getClass().getName());
		
		return object;
	}


	/** ************************************************************************
	 * If the specified Object reference is null, return null. Otherwise, if
	 * the specified Object is not an instance of Class c, throw an Exception;
	 * otherwise, return the Object.
	 */
	 
	protected final Object checkInstanceCompatibilityOrNull(Object object, Class c)
	throws
		ParameterError  // if the Object is not an instance of Class c.
	{
		if (object == null) return null;
		
		return checkInstanceCompatibilityOrNull(object, c);
	}


	
	public final String[] createNodeIdArray(Collection<? extends PersistentNode> col)
	{
		int nodeNo = 0;
		PersistentNode[] objects = col.toArray(new PersistentNode[col.size()]);
		PersistentNode[] objectsCopy = java.util.Arrays.copyOf(objects, objects.length);
		
		int noOfNodes = objectsCopy.length;
		String[] result = new String[noOfNodes];
		
		for (PersistentNode node : objectsCopy)
		{
			//PersistentNode node = (PersistentNode)o;
			result[nodeNo++] = node.getNodeId();
		}
		
		return result;
	}
	
	
	public final String[] createNodeIdArray(Map<String, ? extends PersistentNode> map)
	{
		Collection<? extends PersistentNode> values = map.values();
		Set<PersistentNode> set = new HashSet<PersistentNode>(values);
		String[] ids = new String[set.size()];
		int i = 0;
		for (String id : ids) ids[i++] = id;
		return ids;
	}	
	
	
	public final String getNodeIdOrNull(PersistentNode node)
	{
		if (node == null) return null;
		else return node.getNodeId();
	}
	
	
	/** ************************************************************************
	 * Create an array of TableEntry that contains the values represented in the
	 * specified Map.
	 */
	
	protected final TableEntry[] convertMapToTableEntryArray(Map map)
	{
		Set keys = map.keySet();
		Set keysCopy = new TreeSet(keys);
		
		TableEntry[] tableEntries = new TableEntry[keysCopy.size()];
		
		int entryNo = 0;
		for (Object okey : keysCopy)
		{
			PersistentNode key = (PersistentNode)okey;
			TableEntry tableEntry = new TableEntry(key.getNodeId(), map.get(key));
			tableEntries[entryNo++] = tableEntry;
		}
		
		return tableEntries;
	}
	
	
	public final Display getDisplay() { return display; }


	public final void setVisible(boolean vis) { this.visible = vis; }
	
	
	public final boolean isVisible() { return this.visible; }
	
	
	public final void conditionalLayout()
	{
		if (resizable) return;
		this.layout();
	}
	
	
	public final PersistentNode layoutUpward(PersistentNode layoutBound)
	throws
		ParameterError
	{
		if (this == layoutBound) throw new ParameterError(
			"Attempt to layoutUpward from the layout bound");
		
		this.layout();
		
		PersistentNode par = this.getParent();
		if (par == null) return this;
		if (par == layoutBound) return this;
		if (par.getLayoutManager() == null) return this;
		
		return par.layoutUpward(layoutBound);
	}
	

	public final void layout(PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	{
		if (layoutBound == this) throw new RuntimeException(
			"Layout bound may not be this Node");
		
		PersistentNode outermostAffected = null;
		try { outermostAffected = layoutUpward(layoutBound); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		if (outermostAffectedRef != null) outermostAffectedRef[0] = outermostAffected;
	}
	

	public final void setPredefined(boolean pre) { this.predefined = pre; }
	
	
	public final boolean isPredefined() { return this.predefined; }


	public final void setDeletable(boolean del) { this.deletable = del; }
	
	
	public final boolean isDeletable() { return this.deletable; }
	
	
	public final void setMovable(boolean m) { this.movable = m; }
	
	
	public final boolean isMovable() { return this.movable; }
	
	
	public final void setResizable(boolean yes) { this.resizable = yes; }
	
	
	public final boolean isResizable() { return this.resizable; }
	
	
	public final double getWidth() { return width; }


	public final double getHeight() { return height; }


	public final boolean useBorderWhenIconified() { return this.iconBorder; }
	
	
	public final void setUseBorderWhenIconified(boolean yesOrNo) { this.iconBorder = yesOrNo; }
	
	
	public final Image getIconImage() { return getLocalStoredImageIcon(this.iconImageHandle).getImage(); }
	
	
	public final void setIconImage(String path)
	throws
		IOException
	{
		byte[] bytes = ServiceContext.getMotifClassLoader().getMotifResource(path);
		if (bytes == null) throw new RuntimeException("Resource not found: " + path);
		this.iconImageHandle = path;
		getModelEngine().createLocalImageIcon(bytes, path);
	}
	
	
	public final ImageIcon getImageIcon() { return getLocalStoredImageIcon(this.iconImageHandle); }
	
	
	protected final ImageIcon getLocalStoredImageIcon(String handle)
	{
		return getModelEngine().getLocalStoredImageIcon(handle);
	}
	
	
	public final String getIconImageHandle() { return iconImageHandle; }
	
	
	protected final Image getImageResource(String path)
	{
		return AWTTools.getImage(PersistentNode.class, path);
	}
	
	
	public final void setLayoutManager(LayoutManager lm) { this.layoutManager = lm; }
	
	
	public final LayoutManager getLayoutManager() { return layoutManager; }
	
	
	public final String getDefaultViewTypeName()
	{
		if (getDomain() == null) return null;
		return getDomain().getDefaultDomainViewTypeName();
	}
	
	
	public final boolean contains(Point p)
	{
		double x = p.getX();
		double y = p.getY();
		
		return contains(x, y);
	}


	public final boolean contains(double x, double y)
	{
		return (x >= 0.0) && (x <= width) && (y >= 0.0) && (y <= height);
	}
	
	
	/**
	 * Return an array of Faces representing the visual edges of this Node.
	 * The results are in the coordinate system of this Node. If it
	 * is desired to have the results in the parent's coordinate system, simply
	 * provide the location of this Node as arguments.
	 * The Faces are in the coordinate system of this Node's parent.
	 * The Faces are in the order of (bottom, right, top, left).
	 * Assumes that this Node is fully expanded (not an icon).
	 */
	 
	public final Face[] getFaces(double x, double y)
	{
		return getFaces(x, y, this.width, this.height);
	}


	/**
	 * Same as getFaces(double, double), but assuming that this Node is an icon.
	 */
	 
	public final Face[] getIconFaces(double x, double y)
	{
		double w = getIconWidth();
		if (w == 0.0) throw new RuntimeException("Icon width is zero");
		return getFaces(x, y, getIconWidth(), getIconHeight());
	}
	
	
	protected final Face[] getFaces(double x, double y, double w, double h)
	{
		if (w == 0.0) throw new RuntimeException("Icon width is zero");
		if (h == 0.0) throw new RuntimeException("Icon height is zero");
		
		Face[] faces = new Face[4];
		
		PointImpl[] corners = new PointImpl[4];
		corners[0] = new PointImpl(x, y);
		corners[1] = new PointImpl(x+w, y);
		corners[2] = new PointImpl(x+w, y+h);
		corners[3] = new PointImpl(x, y+h);
		
		final double HalfPi = Math.PI/2.0;
		
		double angle = - Math.PI;
		for (int i = 0; i < 4; i++)
		{
			try { faces[i] = new Face(corners[i], corners[(i+1) % 4], angle += HalfPi); }
			catch (Exception ex) { throw new RuntimeException(
				"Error creating a Face for " + this.getFullName(), ex); }
		}
		
		return faces;
	}


	public final Position findSideClosestToInternalPoint(double x, double y)
	throws
		ParameterError
	{
		if (! (contains(x, y))) throw new ParameterError(
			"Node " + getFullName() + " does not contain the Point " + x + "," + y);
		
		double shortestDistance = y;  // distance to bottom edge.
		Position pos = Position.bottom;
		
		if (width - x < shortestDistance)  // distance to right edge.
		{
			pos = Position.right;
			shortestDistance = width-x;
		}
		
		if (height - y < shortestDistance)  // distance to top edge.
		{
			pos = Position.top;
			shortestDistance = height-y;
		}
		
		if (x < shortestDistance)  // distance to left edge.
		{
			pos = Position.left;
			shortestDistance = x;
		}
		
		return pos;
	}
	

	public final Position findSideClosestToExternalPoint(Point location)
	{
		double distance = 0.0;
		
		Face[] faces = this.getFaces(0.0, 0.0);  // in this coordinate system.
		
		Position pos = FacePositions[0];
		int posNo = 0;
		for (Face face : faces)
		{
			double d = // distance from location to face.
				face.getAsLineSegment().distanceTo(location);
			
			if (posNo == 0)
			{
				distance = d;
			}
			else if (d < distance)
			{
				pos = FacePositions[posNo];
				distance = d;
			}
				
			posNo++;
		}
		
		return pos;
	}
	
	
	public final double getHeaderLabelHeight()
	{
		VisualGuidance guidance = getDomain().getVisualGuidance();
		double probablePixelSize = guidance.getProbablePixelSize();
		
		return probablePixelSize * 
			(double)(getNoOfHeaderRows() * this.getPreferredHeaderRowHeightInPixels());
	}
	
	
	public final double getX() { return display.getX(); }
	public final double getY() { return display.getY(); }
	public final double getScale() { return display.getScale(); }
	public final double getOrientation() { return display.getOrientation(); }

	
	public final void setLocation(double x, double y)
	{
		this.setX(x);
		this.setY(y);
	}
	
	
	public final void setScale(double scale)
	{
		display.setScale(scale);
		update();
		//notifyAllListeners();
	}
	
	
	public final void setOrientation(double angle)
	{
		display.setOrientation(angle);
		update();
		//notifyAllListeners();
	}
	

	public final String getNodeId() { return nodeId; }


	public final String[] getNodeIdPath()
	{
		List<String> nodeIdList = new Vector<String>();
		
		for (PersistentNode node = this;;)
		{
			nodeIdList.add(0, node.getNodeId());
			PersistentNode par = node.getParent();
			if (par == null) break;
			node = par;
		}
		
		return nodeIdList.toArray(new String[nodeIdList.size()]);
	}
	
	
	public final String getNodeIdPathString()
	{
		String[] pathList = this.getNodeIdPath();
		
		String pathString = pathList[0];
		
		for (int i = 1; i < pathList.length; i++)
		{
			pathString += ("." + pathList[i]);
		}
		
		return pathString;
	}
	
	public final Date getDeletionDate() { return deletionDate; }


	public final PersistentNode getPriorVersion() { return priorVersion; }


	public final void setWatch(boolean enable) {this.watchEnable = enable; }
	
	
	public final boolean getWatch() { return this.watchEnable; }
	
	
	protected final boolean watch() { return watchEnable; }  // convenience method.
	
	
	public final void notifyAllListeners()
	throws
		Exception
	{
		notifyAllListeners(new PeerNoticeBase.NodeChangedNotice(this.getNodeId()));
	}
	
	
	public final void notifyNodeDeleted()
	throws
		Exception
	{
		notifyAllListeners(new PeerNoticeBase.NodeDeletedNotice(this.nodeId));
	}
		

	public final void notifyAllListeners(PeerNotice notice)
	throws
		Exception
	{
		CallbackManager callbackManager = getModelEngine().getCallbackManager();
		callbackManager.notifyAllListeners(notice);
	}
	
	
	public final String identify()
	{
		return this.getName() + " (id=" + this.getNodeId() + ")";
	}
	
	
	public final void printChildren(int indentationLevel)
	{
		printChildren(indentationLevel, false);
	}
	
	
	public final void printChildren(int indentationLevel, boolean recursive)
	{
		Set<PersistentNode> cs = getChildren();
		String s = getIndentString(indentationLevel);
		for (PersistentNode c : cs)
		{
			GlobalConsole.println(s + c.identify());
			if (recursive) c.printChildren(indentationLevel+1, true);
		}
	}
	
	
	public final void printChildrenRecursive(int indentationLevel)
	{
		printChildren(indentationLevel, true);
	}
}

