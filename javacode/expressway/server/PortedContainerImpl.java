/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import geometry.*;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Date;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.List;
import java.util.Vector;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.awt.Dimension;


public abstract class PortedContainerImpl extends ModelContainerImpl implements PortedContainer
{
	double iconWidth = 0;
	double iconHeight = 0;
	
	
	public Class getSerClass() { return PortedContainerSer.class; }
	
	
	public String[] getPortNodeIds() { return createNodeIdArray(getPorts()); }
	public String[] getStateNodeIds() { return createNodeIdArray(getStates()); }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		List<State> states = getStates();
		List<Port> ports = getPorts();
		String[] portNodeIds = createNodeIdArray(ports);
		String[] stateNodeIds = createNodeIdArray(states);
		
		
		// Set Ser fields.
		
		PortedContainerSer ser = (PortedContainerSer)nodeSer;
		ser.portNodeIds = portNodeIds;
		ser.stateNodeIds = stateNodeIds;
		String[] ids = new String[ports.size() + states.size()];
		int i = 0;
		for (Port p : ports) ids[i++] = p.getNodeId();
		for (State s : states) ids[i++] = s.getNodeId();
		ser.setSubHierarchyNodeIds(ids);
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		return (PortedContainerImpl)(super.clone(cloneMap, cloneParent));
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public PortedContainerImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
	}


	public PortedContainerImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
	}
	
	
	public double getIconWidth()
	{
		if (iconWidth == 0.0) return super.getIconWidth();
		return iconWidth;
	}
	
	public double getIconHeight()
	{
		if (iconHeight == 0.0) return super.getIconHeight();
		return iconHeight;
	}
	
	
	public void setIconWidth(double w) { this.iconWidth = w; }
	
	public void setIconHeight(double h) { this.iconHeight = h; }
	
	
	public Set<PersistentNode> getNodesWithSpecialLayout()
	{
		Set<PersistentNode> nodesToIgnore = super.getNodesWithSpecialLayout();
		if (nodesToIgnore == null) nodesToIgnore = new TreeSetNullDisallowed<PersistentNode>();
		nodesToIgnore.addAll(this.getPorts());
		return nodesToIgnore;
	}


	public Set<ModelComponent> getSubcomponents() // must extend
	{
		Set<ModelComponent> children = super.getSubcomponents();
		return children;
	}
	
	
	public void deleteSubcomponent(ModelElement child, PersistentNode[] outermostAffectedRef) // must extend
	throws
		ParameterError
	{
		super.deleteSubcomponent(child, outermostAffectedRef);
	}
	

	public List<Port> getPorts()
	{
		List<Port> ports = new Vector<Port>();
		List<Hierarchy> shs = getSubHierarchies();
		for (Hierarchy h : shs) if (h instanceof Port) ports.add((Port)h);
		return ports;
	}
	
	
	public List<Port> getInputPorts()
	{
		List<Port> inputPorts = new Vector<Port>();
		List<Port> ports = getPorts();
		for (Port p : ports)
		{
			PortDirectionType dir = p.getDirection();
			if ((dir == PortDirectionType.input) || (dir == PortDirectionType.bi))
				inputPorts.add(p);
		}
		
		return inputPorts;
	}
	
	
	public Port getPort(String name)
	throws
		ElementNotFound
	{
		PersistentNode node = getChild(name);
		if (! (node instanceof Port)) throw new RuntimeException(
			"Element with name '" + name + "' is not a Port");
		return (Port)node;
	}
	
	
	protected void addPort(Port port)
	throws
		ParameterError
	{
		addOrderedChildAfter(port, null);
	}


	public Port createPort(String name, Position side, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createPort(name, false, PortDirectionType.bi, Position.left, layoutBound,
			outermostAffectedRef);
	}
	
	
	public Port createPort(String name, PortDirectionType direction, Position side,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createPort(name, false, direction, side, layoutBound, outermostAffectedRef);
	}
	
	
	public Port createPort(String name, boolean black, PortDirectionType direction, Position side,
		PersistentNode layoutBound, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		Port port = new PortImpl(name, this, this.getModelDomain(), direction);
		addPort(port);
		port.setBlack(black);
		port.setSizeToStandard();
		port.layout(layoutBound, outermostAffectedRef);
		port.setSide(side);

		return port;
	}


	public Port createPort(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		return createPort(name, PortDirectionType.output, Position.right,
			layoutBound, outermostAffectedRef);
	}
	

	/**
	 * Implements the Delete Pattern.
	 */
	
	public void deletePort(Port port, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError  // if the Port is not a Port for this PortedContainer.
	{
		deleteChild(port, outermostAffectedRef);
	}


	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		port.setDirection(dir);
	}


	public List<State> getStates()
	{
		List<State> states = new Vector<State>();
		List<Hierarchy> shs = getSubHierarchies();
		for (Hierarchy h : shs) if (h instanceof State) states.add((State)h);
		return states;
	}
	
	
	public int getStateNo(State state)
	{
		return getStates().indexOf(state) + 1;
	}


	public State createState(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		checkForUniqueness(name);
		
		if (getState(name) != null) throw new ParameterError(
			"A State with the name '" + name + "' already exists");

		State state = new StateImpl(name, this);
		addOrderedChildAfter(state, null);
		
		state.setSizeToStandard();
		state.layout(layoutBound, outermostAffectedRef);

		return state;
	}

	
	/**
	 * Implements the Delete Pattern.
	 */
	
	public void deleteState(State state, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(state, outermostAffectedRef);
	}
	
	
	/** Initialization data for the instantiable Element Lists. */
	private static final Object[][] instantiableElements =
	{
		{ NodeKindNames.State, "createState", "@States" },
		{ NodeKindNames.Port, "createPort", "@Ports" }
	};
	

	protected static List<String> staticInstantiableNodeKinds;
	protected static List<String> staticCreateMethodNames;
	protected static List<String> staticNodeDescriptions;
	
	
	static
	{
		staticInstantiableNodeKinds = 
			new Vector<String>(ModelContainerImpl.staticInstantiableNodeKinds);
		staticCreateMethodNames = 
			new Vector<String>(ModelContainerImpl.staticCreateMethodNames);
		staticNodeDescriptions = 
			new Vector<String>(ModelContainerImpl.staticNodeDescriptions);
		
		for (Object[] oa : instantiableElements)
			staticInstantiableNodeKinds.add((String)(oa[0]));
		
		for (Object[] oa : instantiableElements)
			staticCreateMethodNames.add((String)(oa[1]));
		
		for (Object[] oa : instantiableElements)
			staticNodeDescriptions.add((String)(oa[2]));
	}
	
	
	List<String> getStaticNodeKinds()
	{
		return staticInstantiableNodeKinds;
	}
	
	
	List<String> getStaticNodeDescriptions()
	{
		return staticNodeDescriptions;
	}
	
	
	public List<String> getInstantiableNodeKinds()
	{
		List<String> names = staticInstantiableNodeKinds;
		getDynamicInstantiableNodeKind(names);
		return names;
	}
	
	
	public List<String> getStaticCreateMethodNames()
	{
		return staticCreateMethodNames;
	}
	
	
	public List<String> getInstantiableNodeDescriptions()
	{
		return super.getInstantiableNodeDescriptions();
	}
	
	
	/*public void deleteChild(ModelElement child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
	}*/


	public State getState(String name)
	{
		List<State> states = getStates();
		for (State state : states)
		{
			if ((state != null) && state.getName().equals(name)) return state;
		}

		return null;
	}
	
	
	public List<Port> getPorts(Position pos)
	{
		List<Port> ports = this.getPorts();
		List<Port> portsOnSameSide = new Vector<Port>();
		for (Port p : ports)
		{
			Position pSide = p.getSide();
			if (pSide == null) throw new RuntimeException("Port has no side");
			
			if (pSide == pos) portsOnSameSide.add(p);
		}
		
		return portsOnSameSide;
	}
	
	
	public Face getFace(Port port, boolean asIcon)
	throws
		ParameterError  // if the Port is not found.
	{
		// Find the Port in this Container's Set of Ports.
		
		boolean found = false;
		List<Port> ports = this.getPorts();
		for (Port p : ports)
		{
			if (p == port)
			{
				found = true;
				break;
			}
		}
		
		if (! found) throw new ParameterError(
			"Port " + port.getFullName() + " not found in " + this.getFullName());
			
		
		// Determine whether the Port is geometrically between any two corners of
		// this Container.
		
		Face[] faces;
		if (asIcon) faces = this.getIconFaces(0.0, 0.0);  // in this Container's reference frame.
		else faces = this.getFaces(0.0, 0.0);
		
		
		//GlobalConsole.println("\tfaces of " + this.getName() + ":");
		
		Face portFace = null;
		for (Face face : faces)
		{
			//GlobalConsole.println("\t\t" + face);
			if (face.intersects(port.getRectangle(asIcon)))
			{
				//GlobalConsole.println("\t\t\t" + face + "\n\t\t\tintersects\n\t\t\t" +
				//	port.getRectangle());
					
				portFace = face;
				break;
			}
		}
		
		if (portFace == null)
		{
			GlobalConsole.println(port);
			GlobalConsole.println(this);
			GlobalConsole.println("width/height=" + this.getWidth() + ", " + this.getHeight());
			GlobalConsole.println("icon width/height=" + this.getIconWidth() + ", " + this.getIconHeight());
			GlobalConsole.println("asIcon=" + asIcon);
			GlobalConsole.println("Faces:");
			for (Face face : faces) GlobalConsole.println("\t" + face.toString());
			
			GlobalConsole.println("Port rectangle:");
			GlobalConsole.println("\t" + port.getRectangle(asIcon).toString());
			
			throw new ParameterError(
			"Port " + port.getFullName() + " is not located on a face of " + 
				this.getFullName());
		}
		
		return portFace;
	}
	
	
	public void setWidth(double width)
	{
		super.setWidth(width);
		
		List<Port> ports = this.getPorts();
		for (Port p : ports)
		{
			Position pSide = p.getSide();
			
			if ((pSide == Position.top) || (pSide == Position.bottom))
				p.setSide(pSide);
		}
	}
	
	
	public void setHeight(double height)
	{
		super.setHeight(height);
		
		List<Port> ports = this.getPorts();
		for (Port p : ports)
		{
			Position pSide = p.getSide();
			
			//if ((pSide == Position.left) || (pSide == Position.right))
				p.setSide(pSide);
		}
	}
	
	
	public void dump(int indentation)
	{
		super.dump(indentation);

		List<State> states = this.getStates();
		List<Port> ports = this.getPorts();
		for (Port p : ports) p.dump(indentation + 1);
		for (State s : states) s.dump(indentation + 1);
	}
}
