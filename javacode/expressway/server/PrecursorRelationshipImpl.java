/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.DecisionElement.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.SortedSet;


public class PrecursorRelationshipImpl extends DecisionElementImpl implements PrecursorRelationship
{
	private DecisionPoint precursorDecisionPoint = null;
	private DecisionPoint subordinateDecisionPoint = null;
	

	PrecursorRelationshipImpl(DecisionDomain decisionDomain,
		DecisionPoint precursorDecisionPoint, DecisionPoint SubordinateDecisionPoint)
	{
		super(decisionDomain);
		setResizable(false);
		this.precursorDecisionPoint = precursorDecisionPoint;
		this.subordinateDecisionPoint = subordinateDecisionPoint;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
	}


	public String getTagName() { return "precursor"; }

	
	public Class getSerClass() { return PrecursorRelationshipSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((PrecursorRelationshipSer)nodeSer).precursorDecisionPointNodeId = getNodeIdOrNull(precursorDecisionPoint);
		((PrecursorRelationshipSer)nodeSer).subordinateDecisionPointNodeId = getNodeIdOrNull(subordinateDecisionPoint);
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		PrecursorRelationshipImpl newInstance = (PrecursorRelationshipImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.precursorDecisionPoint = cloneMap.getClonedNode(precursorDecisionPoint);
		newInstance.subordinateDecisionPoint = cloneMap.getClonedNode(subordinateDecisionPoint);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public PersistentNode getChild(String name)
	throws
		ElementNotFound
	{
		throw new ElementNotFound("Node with name '" + name + "'");
	}


	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.PrecursorRelationshipIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public DecisionPoint getPrecursorDecisionPoint() throws ValueNotSet
	{
		if (precursorDecisionPoint == null) throw new ValueNotSet(
			"Field 'precursorDecisionPoint' for '" + getFullName() + "'");
		return precursorDecisionPoint;
	}
	
	public DecisionPoint getSubordinateDecisionPoint() throws ValueNotSet
	{
		if (subordinateDecisionPoint == null) throw new ValueNotSet(
			"Field 'subordinateDecisionPoint' for '" + getFullName() + "'");
		return subordinateDecisionPoint;
	}
}
