/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Set;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.Serializable;


public class PulseGeneratorImpl extends AbstractGenerator implements PulseGenerator
{
	/** reference only. */
	public State riseState = null;
	
	/** reference only. */
	public Port riseIn = null;
	
	/** reference only. */
	public Port riseOut = null;
	
	public transient String riseStateNodeId = null;
	public transient String riseInNodeId = null;
	public transient String riseOutNodeId = null;
	
	public String getRiseStateNodeId() { return riseStateNodeId; }
	public String getRiseInNodeId() { return riseInNodeId; }
	public String getRiseOutNodeId() { return riseOutNodeId; }


	public Class getSerClass() { return PulseGeneratorSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		
		riseStateNodeId = getNodeIdOrNull(riseState);
		riseInNodeId = getNodeIdOrNull(riseIn);
		riseOutNodeId = getNodeIdOrNull(riseOut);
		
		// Set Ser fields.
		
		//nodeSer.embeddedGeneratorNodeId = this.getEmbeddedGeneratorNodeId();
		//nodeSer.compensatorNodeId = this.getCompensatorNodeId();
		
		((PulseGeneratorSer)nodeSer).riseStateNodeId = this.getRiseStateNodeId();
		((PulseGeneratorSer)nodeSer).riseInNodeId = this.getRiseInNodeId();
		((PulseGeneratorSer)nodeSer).riseOutNodeId = this.getRiseOutNodeId();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		PulseGeneratorImpl newInstance = (PulseGeneratorImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.riseState = cloneMap.getClonedNode(riseState);
		newInstance.riseIn = cloneMap.getClonedNode(riseIn);
		newInstance.riseOut = cloneMap.getClonedNode(riseOut);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deleteState(riseState, null);
		deletePort(riseIn, null);
		deletePort(riseOut, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		riseState = null;
		riseIn = null;
		riseOut = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public PulseGeneratorImpl(String name, ModelContainer parent, ModelDomain domain,
		double timeDistShape, double timeDistScale, double valueDistShape, 
		double valueDistScale, boolean ignoreStartup)
	{
		super(name, parent, domain, timeDistShape, timeDistScale, valueDistShape,
			valueDistScale, ignoreStartup);
			
		init();
	}
	
	
	public PulseGeneratorImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init();
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.GeneratorIconImageName);
	}
	
	
	private void init()
	{
		try { this.setNativeImplementationClass(PulseGeneratorNativeImpl.class); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		try
		{
			riseIn = super.createPort("rise_in", PortDirectionType.input, 
				Position.bottom, this, null);
			riseOut = super.createPort("rise_out", PortDirectionType.output, 
				Position.bottom, this, null);
			
			riseIn.setVisible(false);
			riseOut.setVisible(false);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}
		
		try
		{
			riseState = super.createState("RiseState", this, null);
			riseState.setVisible(false);
		}
		catch (ParameterError ex)
		{
			throw new RuntimeException("Unexpected", ex);
		}


		try { riseState.bindPort(riseOut); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		riseIn.setMovable(false);
		riseOut.setMovable(false);
		riseState.setMovable(false);
		
		riseIn.setPredefined(true);
		riseOut.setPredefined(true);
		riseState.setPredefined(true);
	}
	
	
	public State getRiseState() { return riseState; }
	
	public Port getRiseIn() { return riseIn; }

	public Port getRiseOut() { return riseOut; }
	
	public void dump(int indentation)
	{
		super.dump(indentation);
		
		riseIn.dump(indentation);
		riseOut.dump(indentation);
	}
}


class PulseGeneratorNativeImpl extends AbstractGeneratorNativeImpl
{
	private String riseStateName = "RiseState";


	public synchronized void start(ActivityContext context) throws Exception
	{
		super.start(context);
	}


	public synchronized void stop()
	{
		super.stop();
	}


	public synchronized void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		respond(events, false);
	}
	
	
	protected synchronized void respond(SortedEventSet<GeneratedEvent> events, 
		boolean eventsWereDeleted)
	throws
		ModelContainsError
	{
		if ((events.size() == 0) && getGenerator().getIgnoreStartup()) return;
		
		super.respond(events, eventsWereDeleted);
		
		// Propagate generated Event to RiseState.
		
		if (newlyScheduledEvent != null)
		{
			//System.out.println("Propagating event to rise state");
			Serializable valueCopy;
			try { valueCopy = PersistentNodeImpl.copyObject(newlyScheduledEvent.getNewValue()); }
			catch (CloneNotSupportedException ex) { throw new ModelContainsError(
				"Event value must be cloneable: is a " + 
					newlyScheduledEvent.getNewValue().getClass().getName()); }
			
			try { getActivityContext().scheduleFutureStateChange(
				this.getPulseGenerator().getRiseState(),
					newlyScheduledEvent.getTimeToOccur(), valueCopy, events); }
			catch (ParameterError pe) { throw new ModelContainsError(pe); }
			
			newlyScheduledEvent = null;  // reset.
		}
		
		
		/* Generate compensating Event.
		
			When Event received at �RiseIn� port,
				If there is no other Event on State in this epoch,
					Generate new Event on State after time 0 with value <baseline>.
		*
		
		if (this.portHasNonCompensatingEvent(events, getRiseIn()))
		{
			for (GeneratedEvent event : events)
			{
				State s = event.getState();
				if (s == this.getState()) return;  // an Event on State.
			}
			
			GeneratedEvent fallEvent = 
					((ModelContextImpl)(getActivityContext())).scheduleCompensationEvent(
						getState(), 0.0, events);
					
			fallEvent.setCompensation(true);
		}*/   // This is now handled by the SimulationRunImpl.
	}
	
	
	protected synchronized void notifyOfScheduleEventOnGeneratorState(SimulationTimeEvent event)
	{
		this.newlyScheduledEvent = event;
		event.setPulse(true);
	}
	
	private SimulationTimeEvent newlyScheduledEvent = null;


	PulseGeneratorImpl getPulseGenerator() { return (PulseGeneratorImpl)(getGenerator()); }
	
	public State getRiseState() { return getPulseGenerator().getRiseState(); }
	
	public Port getRiseIn() { return getPulseGenerator().getRiseIn(); }

	public Port getRiseout() { return getPulseGenerator().getRiseOut(); }
	
}
