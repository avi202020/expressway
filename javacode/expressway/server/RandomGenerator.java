/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import java.util.Random;


public class RandomGenerator extends Random
{
	private static RandomGenerator random = new RandomGenerator();
	
	
	public RandomGenerator()
	{
		super();
	}
	
	
	public RandomGenerator(long seed)
	{
		super(seed);
	}
	
	
	public static synchronized RandomGenerator getRandomGenerator() { return random; }
	
	
	static synchronized void setRandomSeed(long seed)
	{
		random = new RandomGenerator(seed);
	}
	
	
	static synchronized void setRandom(RandomGenerator r) { random = r; }
	
	
	static synchronized void reset() { random = new RandomGenerator(); }
	
	
	/*public double nextDouble()
	{
		double d = super.nextDouble();
		GlobalConsole.println("Random number: " + d);
		return d;
	}*/
}

