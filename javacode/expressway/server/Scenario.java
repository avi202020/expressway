/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import java.util.Date;
import java.util.Set;
import java.util.Map;
import java.util.SortedSet;
import java.io.Serializable;


/**
 * The base type for any kind of model scenario or decision scenario.
 */

public interface Scenario extends PersistentListNode
{
	/**
	 * If this Scenario was created by copying another Scenario and then modifying
	 * the copy, then return that other Scenario. Otherwise return null.
	 */
	 
	Scenario derivedFrom();
	
	void setDerivedFrom(Scenario s);
	
	void setDate(Date d);
	
	Date getDate();  // Date and time of creation.

	Serializable getAttributeValue(Attribute attr)
	throws
		ParameterError;
	
	void setAttributeValue(Attribute attr, Serializable value)
	throws
		ParameterError;  // if the value is not copyable.
		
	
	/** Clear this Scenario's Attribute values and set them to the argument. */
	void setAttributeValues(Map<Attribute, Serializable> attrValues);
	
	
	/**
	 * Return a Set of the Attributes that have values defined in this Scenario.
	 */
	 
	Set<Attribute> getAttributesWithValues();
	
	
	/**
	 * Get all Attributes of the Node that have a value of the
	 * specified type. Set onlyOne to true if the Node is only permitted 
	 * to have at most one Attribute of the specified type.
	 */
	
	public Set<Attribute> getAttributesWithValuesOfType(PersistentNode elt, 
		Class type, boolean onlyOne)
	throws
		ModelContainsError;  // if there is more than one Attribute of the type.
			
		
	void setScenarioSet(ScenarioSet ss);
	
	
	ScenarioSet getScenarioSet();
	
	
	void setScenarioThatWasCopied(Scenario scen);
	
	
	Scenario getScenarioThatWasCopied();
	
	
	/**
	 * Identify all Attributes with values of the specified type within this
	 * Scenario. If an Attribute's value is of type Class, then the value itself
	 * (that class) is compared with 'type'. If 'onlyOne' is true, there may
	 * be only one such Attribute within the Model Scenario.
	 */
		
	SortedSet<Attribute> identifyTags(Class type, boolean onlyOne)
	throws
		ModelContainsError;  // if more than Attribute of type 'type' is found.
	
	
	/**
	 * Update this Scenario by applying the values from the
	 * specified ScenarioSer.
	 */
	 
	Scenario update(ScenarioSer ser)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError;


	/**
	 * Create a new independent copy of the Scenario, with a unique name.
	 * The copy will belong to the same domain as the original Scenario.
	 */
	 
	Scenario createIndependentCopy()
	throws
		CloneNotSupportedException;
		
		
	ScenarioHelper getHelper();
}
