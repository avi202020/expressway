/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.server.HierarchyElement.*;
import expressway.server.MotifElement.*;
import expressway.ser.*;
import generalpurpose.DateAndTimeUtils;
import generalpurpose.StringUtils;
import generalpurpose.XMLTools;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.Date;
import java.util.Map;
import java.text.DateFormat;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;


public class ScenarioHelper
{
	private static final double PreferredWidthInPixels = 20.0;
	private static final double PreferredHeightInPixels = 20.0;
	
	private Scenario scenario;
	private PersistentNode superScenario;
	
	private Date date = null;  // real time when this Scenario was created.

	/** Ownership (of the values). */
	private Map<Attribute, Serializable> attributeValues = new HashMap<Attribute, Serializable>();
	
	private Scenario scenarioThatWasCopied = null;
	private ScenarioSet scenarioSet = null;
	
	
	public ScenarioHelper(Scenario scenario, PersistentNode superScenario)
	{
		this.scenario = scenario;
		this.superScenario = superScenario;
	}
	
	
	protected Scenario getScenario() { return scenario; }
	
	
	protected PersistentNode getSuperScenario() { return superScenario; }
	
	
 /* From PersistentNode */
	
	
	public void renameChild(PersistentNode child, String newName)
	throws
		ParameterError
	{
		superScenario.renameChild(child, newName);
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		Scenario newInstance;
		try { newInstance = (Scenario)
			(superScenario.clone(cloneMap, cloneParent)); }
		catch (CloneNotSupportedException ce) { throw ce; }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		newInstance.setDate(new Date());
		
		newInstance.setAttributeValues(new HashMap<Attribute, Serializable>());
		Set<Attribute> attrs = new HashSet<Attribute>(attributeValues.keySet());
		for (Attribute a : attrs)
		{
			Serializable value = attributeValues.get(a);
			Serializable newValue = PersistentNodeImpl.copyObject(value);
			try { newInstance.setAttributeValue(a, newValue); }
			catch (ParameterError pe) { throw new RuntimeException(pe); }
		}
		
		newInstance.setScenarioThatWasCopied(cloneMap.getClonedNode(scenarioThatWasCopied));
		newInstance.setScenarioSet(cloneMap.getClonedNode(scenarioSet));
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = StringUtils.getIndentString(indentation);
		String indentString2 = StringUtils.getIndentString(indentation+1);
		
		DateFormat df = DateAndTimeUtils.DateTimeFormat;
		
		writer.print(indentString + "<" + scenario.getTagName() + " name=\"" + scenario.getName() + "\" "
			+ "domain=\"" + scenario.getDomain().getName() + "\" "
			+ (scenarioSet == null? "" : (scenario.getTagName() + "_set=\"" + scenarioSet.getName() + "\" "))
			+ (scenarioThatWasCopied == null? "" : ("based_on=\"" + scenarioThatWasCopied.getName() + "\" "))
			+ "x=\"" + scenario.getX() + "\" "
			+ "y=\"" + scenario.getY() + "\" "
			+ (scenario.isDeletable() ? "" : "deletable=\"false\" "));
			
		scenario.writeSpecializedXMLAttributes(writer);
	
		writer.println(">");
		
		
		scenario.writeSpecializedXMLElements(writer, indentation);
		
		
		// Write Attribute value assignments.
		
		Set<Attribute> attrsWithValues = new HashSet<Attribute>(attributeValues.keySet());
		for (Attribute attrWithValue : attrsWithValues)
		{
			Serializable value = attributeValues.get(attrWithValue);
			
			String attrWithValueRelativePath = null;
			try { attrWithValueRelativePath = 
				attrWithValue.getNameRelativeTo(scenario.getDomain()); }
			catch (ParameterError pe) { throw new IOException(pe); }
			
			writer.println(indentString2 + "<attribute_value "
				+ "path=\"" + attrWithValueRelativePath
				+ "\" value=\""
				+ XMLTools.encodeAsXMLAttributeValue(value.toString()) + "\"/>");
		}
		
		
		// Write each Attribute.
		
		Set<Attribute> attrs = scenario.getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);


		writer.println(indentString + "</" + scenario.getTagName() + ">");
	}
	
	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		Set<Attribute> values = new HashSet<Attribute>(attributeValues.keySet());
		for (Attribute a : values) attributeValues.remove(a);
		
		
		// Call super.prepareForDeletion, if any.
		superScenario.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		try { return (PersistentNode)(superScenario.getChild(name)); }
		catch (ElementNotFound enf) { throw enf; }
		catch (Exception ex) { throw new RuntimeException(ex); }
	}
	
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		return (SortedSet<PersistentNode>)(superScenario.getChildren());
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Delete (per the Delete Pattern) or disconnect the Node from any other
		// child Nodes known to this class that depend on the existence of the Node
		// that is being deleted. These are normally siblings of the Node that
		// is being deleted.
		
		// Call super.deleteChild.
		superScenario.deleteChild(child, outermostAffectedRef);
	}

	
	public int getNoOfHeaderRows() { return 2; }
	
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (scenario.getDomain())
		{
		ScenarioSer ser = (ScenarioSer)nodeSer;
		
		ser.setDate(date);
		ser.setScenarioThatWasCopiedNodeId(scenario.getNodeIdOrNull(scenarioThatWasCopied));
		ser.setScenarioSetNodeId(scenario.getNodeIdOrNull(scenarioSet));


		// Set Attribute values for the Scenario.
		
		Set<Attribute> attributesWithValues = attributeValues.keySet();
		
		Vector<String> ids = new Vector<String>();
		Vector<Serializable> values = new Vector<Serializable>();
		for (Attribute a : attributesWithValues)
		{
			ids.add(a.getNodeId());
			values.add(attributeValues.get(a));
		}

		ser.setAttributeNodeIds(ids.toArray(new String[ids.size()]));
		ser.setAttributeValues(values.toArray(new Serializable[values.size()]));
		
		return (NodeSer)(superScenario.externalize(nodeSer));
		}
	}
	
	
  /* From Scenario */
	
		
	public Scenario update(ScenarioSer scenarioSer)
	throws
		ParameterError,
		ModelContainsError,
		InternalEngineError
	{
		this.date = scenarioSer.getDate();

		scenario.getParent().renameChild(scenario, scenarioSer.getName());
		
		
		// Update Attribute values.
		
		int i = 0;
		String[] attrNodeIds = scenarioSer.getAttributeNodeIds();
		for (String attrId : attrNodeIds)
		{
			PersistentNode node = PersistentNodeImpl.getNode(attrId);
			if (node == null) throw new ParameterError(
				"Attribute with ID " + attrId + " not found");
			
			if (! (node instanceof Attribute)) throw new ParameterError(
				"Node with ID " + attrId + " is not an Attribute");
			
			scenario.setAttributeValue((Attribute)node, scenarioSer.getAttributeValues()[i]);
			i++;
		}
		
		return scenario;
	}
	
	
	public void setDate(Date d) { this.date = d; }
	
	
	public Date getDate() { return date; }
	
	
	public Scenario derivedFrom() { return scenarioThatWasCopied; }
	
	
	public void setDerivedFrom(Scenario originalScenario)
	{
		this.scenarioThatWasCopied = originalScenario;
	}

	
	public void setAttributeValue(Attribute attribute, Serializable value)
	throws
		ParameterError
	{
		if (! (attribute instanceof Attribute)) throw new RuntimeException(
			"Attribute is not a HierarchyAttribute");
		
		Serializable originalValue = attributeValues.get(attribute);
		
		if (value == null)  // remove the Attribute's value in this Scenario.
		{
			attributeValues.remove(attribute);
			return;
		}
		
		if (value.equals(originalValue)) return;
		
		attribute.validate(value);
		
		try
		{
			attributeValues.put(attribute, PersistentNodeImpl.copyObject(value));
		}
		catch (CloneNotSupportedException ex)
		{
			throw new ParameterError("Unable to clone attribute value for attribute " +
				attribute.getName() + ": value is a " + value.getClass().getName(), ex);
		}
	}
	
	
	public void setAttributeValues(Map<Attribute, Serializable> attrValues)
	{
		this.attributeValues = attrValues;
	}
	
	
	/**
	 * A lazy lookup is used for Attribute values: if no value is found in this
	 * HierarchyScenario's Attribute value table, then the Attribute's default value
	 * is retrieved and written to the table.
	 */

	public Serializable getAttributeValue(Attribute attribute)
	throws
		ParameterError
	{
		Serializable value = attributeValues.get(attribute);

		if (value == null)
		{
			Serializable defaultValue = attribute.getDefaultValue();
			
			if (defaultValue != null) setAttributeValue(attribute, defaultValue);
			value = defaultValue;
		}

		return value;
	}


	public Set<Attribute> getAttributesWithValues()
	{
		return new TreeSetNullDisallowed<Attribute>(attributeValues.keySet());
	}
	
	
	public Set<Attribute> getAttributesWithValuesOfType(PersistentNode elt, 
		Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		Set<Attribute> attrsOfType = new TreeSetNullDisallowed<Attribute>();
		Set<Attribute> attrs = attributeValues.keySet();
		for (Attribute attr : attrs)
		{
			Serializable value = attributeValues.get(attr);
			if (type.isAssignableFrom(value.getClass())) attrsOfType.add(attr);
		}
		
		if (attrsOfType.size() > 1) throw new ModelContainsError(
			"There are " + attrsOfType.size() + " Attributes of type " +
			type.getName());
		
		return attrsOfType;
	}
		
	
	public void setScenarioSet(ScenarioSet s) { this.scenarioSet = s; }
	
	
	public ScenarioSet getScenarioSet() { return scenarioSet; }
	
	
	public void setScenarioThatWasCopied(Scenario scen) { this.scenarioThatWasCopied = scen; }
	
	
	public Scenario getScenarioThatWasCopied() { return scenarioThatWasCopied; }
	

	public Scenario createIndependentCopy()
	throws
		CloneNotSupportedException
	{
		Object object = this.clone(new CloneMap(scenario), scenario.getDomain());
		
		Scenario newScenario = (Scenario)object;
		
		scenario.getDomain().addScenario(newScenario);
		
		String newName = newScenario.getParent().createUniqueChildName(scenario.getName());
		try { newScenario.getParent().renameChild(newScenario, newName); }
		catch (ParameterError pe) { throw new RuntimeException(
			"Should not happen: error setting name using an Expressway-created name"); }
		
		newScenario.setScenarioThatWasCopied(scenario);
		
		return newScenario;
	}
	
	
	public SortedSet<Attribute> identifyTags(Class type, boolean onlyOne)
	throws
		ModelContainsError
	{
		// Note: See also the method ModelEngineRemote.getElementsWithAttributeDeep.
		
		SortedSet<Attribute> attrsOfType = new TreeSetNullDisallowed<Attribute>();
		
		SortedSet<Attribute> attrs = new TreeSetNullDisallowed<Attribute>(attributeValues.keySet());
		for (Attribute attr : attrs)
		{
			Serializable value = attributeValues.get(attr);
			
			if (value instanceof Class)
			{
				if (type.isAssignableFrom((Class)value))
					attrsOfType.add(attr);
			}
			else
				if (type.isAssignableFrom(value.getClass())) // value is a 'type'
					attrsOfType.add(attr);
		}
		
		return attrsOfType;
	}
	
	
	public DateFormat getDateFormat() { return DateAndTimeUtils.DateTimeFormat; }
	
	
  /* Debug */
	
	
	public void dumpAttrValues()
	{
		// Traverse every Component of the Domain, dumping each Attribute value.
		
		GlobalConsole.println("For Scenario " + scenario.getName() + ":");
		dumpAttrValues(scenario.getDomain());
	}
	
	
	void dumpAttrValues(PersistentNode elt)
	{
		// Dump Attribute values for elt.
		
		Set<Attribute> attrs = new TreeSetNullDisallowed<Attribute>(elt.getAttributes());
		for (Attribute attr : attrs) try
		{
			Serializable value = getAttributeValue(attr);
			GlobalConsole.println(attr.getFullName() + "=" + value.toString());
		}
		catch (Exception ex)
		{
			GlobalConsole.println("***Could not obtain value for Attribute " + attr.getFullName());
		}
		
		
		// Recursively traverse each nested HierarchyElement.
		
		Set<PersistentNode> subElts = elt.getChildren();
		for (PersistentNode subElt : subElts)
		{
			dumpAttrValues(subElt);
		}
	}
}

