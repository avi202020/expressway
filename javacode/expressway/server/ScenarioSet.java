/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.util.Date;
import java.util.List;
import java.io.Serializable;


/**
 * An aggregator for Model Scenarios. The Scenarios in a Scenario Set differ
 * only in their value for a specified Attribute.
 */

public interface ScenarioSet extends PersistentListNode
{
	Scenario derivedFrom();
	
	void setVariableAttribute(Attribute attr);
	
	/** Return the Attribute that is varied across the Scenarios in this
		Scenario Set. */
	Attribute getVariableAttribute();
	
	void setScenarios(List<Scenario> scens);
	
	/** Must return the actual backed List. */
	List<Scenario> getScenarios();
	
	void addScenario(Scenario scen);
	
	void removeScenario(Scenario scen);
	
	void setScenarioThatWasCopied(Scenario scenario);
	
	Scenario getScenarioThatWasCopied();
}

