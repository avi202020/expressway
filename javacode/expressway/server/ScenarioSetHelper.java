/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.ModelAPITypes.*;
import expressway.common.*;
import expressway.server.HierarchyElement.*;
import expressway.server.MotifElement.*;
import expressway.ser.*;
import generalpurpose.XMLTools;
import generalpurpose.StringUtils;
import generalpurpose.DateAndTimeUtils;
import generalpurpose.SetVector;
import java.net.URL;
import java.util.List;
import java.util.Vector;
import java.util.SortedSet;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Set;
import java.text.DateFormat;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;


public class ScenarioSetHelper extends ListNodeHelper
{
	private PersistentNode superScenarioSet;
	
	private List<Scenario> scenarios = new SetVector<Scenario>();
	
	private Attribute attribute;
	
	private Scenario scenarioThatWasCopied;
	
	
	public ScenarioSetHelper(ScenarioSet scenarioSet, PersistentNode superScenarioSet)
	{
		super(scenarioSet);
		this.superScenarioSet = superScenarioSet;
	}
	
	
	protected ScenarioSet getScenarioSet() { return (ScenarioSet)(getListNode()); }
	
	
	protected PersistentNode getSuperScenarioSet() { return superScenarioSet; }
	
	
  /* From PersistentNode */


	public int getNoOfHeaderRows() { return 0; }
	
	
	public double getPreferredWidthInPixels() { return 10; }

	
	public double getPreferredHeightInPixels() { return 10; }


	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	throws CloneNotSupportedException
	{
		ScenarioSet newInstance = (ScenarioSet)(superScenarioSet.clone(cloneMap, cloneParent));
		newInstance.setScenarios(
			PersistentNodeImpl.cloneNodeList(scenarios, cloneMap, newInstance));
		newInstance.setVariableAttribute(cloneMap.getClonedNode(attribute));
		newInstance.setScenarioThatWasCopied(cloneMap.getClonedNode(scenarioThatWasCopied));
		
		return newInstance;
	}
	
	
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = StringUtils.getIndentString(indentation);
		String indentString2 = StringUtils.getIndentString(indentation+1);
		
		DateFormat df = DateAndTimeUtils.DateTimeFormat;
		
		writer.print(indentString + "<" + getScenarioSet().getTagName() + " name=\""
			+ getScenarioSet().getName() + "\" "
			+ "domain=\"" + getScenarioSet().getDomain().getName() + "\" "
			+ "based_on=\"" + scenarioThatWasCopied.getName() + "\" "
			+ (getScenarioSet().isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + getScenarioSet().getX() + "\" "
			+ "y=\"" + getScenarioSet().getY() + "\" ");
		
		getScenarioSet().writeSpecializedXMLAttributes(writer);
			
		writer.println("\">");
		
		getScenarioSet().writeSpecializedXMLElements(writer, indentation+1);

		// Write Attribute value assignments.
		
		for (Scenario scen : scenarios)
		{
			Serializable value = null;
			try { value = scen.getAttributeValue(attribute); }
			catch (ParameterError pe) { throw new IOException(pe); }
			
			writer.println(indentString2 + "<value value=\""
				+ XMLTools.encodeAsXMLAttributeValue(value.toString()) + "\"/>");
		}
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getScenarioSet().getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		writer.println(indentString + "</" + getScenarioSet().getTagName() + ">");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		superScenarioSet.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		//scenarioSet = null;
		scenarios = null;
		attribute = null;
		scenarioThatWasCopied = null;
	}

	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		synchronized (getScenarioSet().getDomain())
		{
		ScenarioSetSer ser = (ScenarioSetSer)(nodeSer);
		
		ser.setVariableAttributeId(attribute.getNodeId());
		ser.setScenarioThatWasCopiedNodeId(getScenarioSet().getNodeIdOrNull(scenarioThatWasCopied));
		
		String[] scenarioIds = new String[scenarios.size()];
		ser.setScenarioIds(scenarioIds);
			
		int i = 0;
		for (Scenario scen : scenarios) scenarioIds[i++] = scen.getNodeId();
		
		return (NodeSer)(superScenarioSet.externalize(nodeSer));
		}
	}
	
	
  /* From PersistentListNode */
	
	
	public List<PersistentNode> getOrderedChildren()
	{
		return new Vector<PersistentNode>(scenarios);
	}
	
	
	public int getNoOfOrderedChildren()
	{
		return scenarios.size();
	}
	
	
	public PersistentNode getOrderedChild(String name) throws ElementNotFound
	{
		List<PersistentNode> nodes = getScenarioSet().getOrderedChildren();
		for (PersistentNode node : nodes) if (node.getName().equals(name)) return node;
		throw new ElementNotFound("Node with name '" + name + "'");
	}
	
	public PersistentNode getOrderedChildAt(int index)
	throws
		ParameterError
	{
		try { return scenarios.get(index); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	public int getIndexOfOrderedChild(PersistentNode child)
	{
		return scenarios.indexOf(child);
	}
	
	public void insertOrderedChildAt(PersistentNode child, int index)
	throws
		ParameterError
	{
		if (! (child instanceof Scenario)) throw new ParameterError(
			"'" + child.getFullName() + "' is not a Scenario");
		try { scenarios.add(index, (Scenario)child); }
		catch (IndexOutOfBoundsException ex) { throw new ParameterError(ex); }
	}
	
	public boolean appendOrderedChild(PersistentNode child)
	throws
		ParameterError
	{
		if (! (child instanceof Scenario)) throw new ParameterError(
			"Node '" + child.getName() + "' is not a Scenario");
		return scenarios.add((Scenario)child);
	}
	
	public List<PersistentNode> getOrderedNodesOfKind(Class c)
	{
		List<PersistentNode> nodes = getOrderedChildren();
		for (PersistentNode node : nodes) if (c.isAssignableFrom(node.getClass()))
			nodes.add(node);
		return nodes;
	}
	
	public void removeOrderedNodesOfKind(Class c)
	{
		List<PersistentNode> nodes = getOrderedChildren();
		for (PersistentNode node : nodes)
			if (c.isAssignableFrom(node.getClass()))
				try { removeOrderedChild(node); }
				catch (ParameterError pe) { throw new RuntimeException(pe); }
	}
	
	public boolean addAllAsOrderedNodes(List<? extends PersistentNode> nodes)
	throws ParameterError
	{
		boolean changed = false;
		for (PersistentNode node : nodes)
		{
			if (! (node instanceof Scenario)) throw new RuntimeException(
				"Node '" + node.getFullName() + "' is not a Scenario");
			scenarios.add((Scenario)node);
			changed = true;
		}
		return changed;
	}
	
	
  /* From ScenarioSet */


	public Scenario derivedFrom() { return scenarioThatWasCopied; }
	
	
	public void setDerivedFrom(Scenario scen) { scenarioThatWasCopied = scen; }
	
	
	public void setVariableAttribute(Attribute attr)
	{
		this.attribute = attr;
	}
	
	
	public Attribute getVariableAttribute() { return attribute; }
	
	
	public void setScenarios(List<Scenario> scens)
	{
		this.scenarios = scens;
	}


	public List<Scenario> getScenarios() { return scenarios; }

	
	public void addScenario(Scenario scen)
	{
		if (! scenarios.contains(scen)) scenarios.add(scen);
	}
	
	
	public void removeScenario(Scenario scen)
	{
		scenarios.remove(scen);
	}


	public void setScenarioThatWasCopied(Scenario scenario)
	{
		this.scenarioThatWasCopied = scenario;
	}
	
	
	public Scenario getScenarioThatWasCopied()
	{
		return scenarioThatWasCopied;
	}
}

