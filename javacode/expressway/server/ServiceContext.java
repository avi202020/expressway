/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.io.IOException;


/**
 * A context for the main thread of execution of a user request.
 * The ServiceContext carries information about the nature of a service request.
 * This context allows activities performed by a service thread to access information
 * about the request without having to pass all of that information through
 * method arguments. This context is used primarily to contain the Set of scenarios that are
 * used by the request. Subordinate threads are also normally given access to
 * this context, but that depends on the thread implementation.
 */

public class ServiceContext
{
	private static MotifClassLoader motifClassLoader;

	private Set<Scenario> scenarios = new TreeSetNullDisallowed<Scenario>();
	private ModelEngineLocal modelEngine;
	private String clientId;
	
	
	private static ThreadLocal serviceContext = new ThreadLocal()
	{
		protected synchronized Object initialValue()
		{
			return new ServiceContext();
		}
	};
	
	
	ServiceContext()
	{
	}
	

	ServiceContext(ModelEngineLocal me, String clientId)
	{
		if (me == null) throw new RuntimeException("Model Engine is null");
		if (clientId == null) throw new RuntimeException("Client Id is null");
		
		this.modelEngine = me;
		this.clientId = clientId;
	}
	
	
	public static ServiceContext getServiceContext()
	{
		ServiceContext context = (ServiceContext)(serviceContext.get());
		if (context == null) serviceContext.set(new ServiceContext());
		return (ServiceContext)(serviceContext.get());
	}
	
	
	public static void setServiceContext(ServiceContext context)
	{
		serviceContext.set(context);
		
		ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();
		if (currentClassLoader != motifClassLoader)
			Thread.currentThread().setContextClassLoader(motifClassLoader);
	}
	
	
	public static ServiceContext create(ModelEngineLocal me, String clientId)
	{
		ServiceContext newContext = new ServiceContext(me, clientId);
		setServiceContext(newContext);
		return newContext;
	}
	
	
	public static void clear()
	{
		serviceContext.set(null);
	}
	
	
	public static MotifClassLoader getMotifClassLoader() { return motifClassLoader; }
	
	
	public static void setMotifClassLoader(MotifClassLoader loader) { motifClassLoader = loader; }


	public ModelEngineLocal getModelEngine() { return modelEngine; }
	
	
	public String getClientId() { return clientId; }
	
	
	public static String getCurrentClientId()
	{
		return getServiceContext().getClientId();
	}
	
	
	public synchronized Set<Scenario> getScenarios()
	{
		return scenarios;
	}
	
	
	public synchronized Scenario getScenario(Domain domain)
	{
		for (Scenario scenario : scenarios)
		{
			if (scenario.getDomain() == domain) return scenario;
		}
		
		return null;
	}


	public synchronized void addScenario(Scenario scenario)
	throws
		ParameterError  // If the Context already contains a Scenario for
			// the same Domain.
	{
		if (scenario == null) throw new RuntimeException(
			"scenario is null.");
		
		Domain scenarioDomain = scenario.getDomain();
		
		if (scenarios.contains(scenario))
		{
			try { scenarioDomain.setCurrentScenario(scenario); }
			catch (ElementNotFound ex) { throw new RuntimeException(
				"Should not happen."); }

			return; // already in the Set.
		}
		
		
		// Check that no other Scenario within the context represents the
		// same Domain.
		
		//if (containsDomain(scenarioDomain)) throw new ParameterError(
		//	"Context already contains a Scenario for Domain " +
		//		scenarioDomain.getName());
		
		
		// Make scenario the current Scenario for that Domain.
		
		try { scenarioDomain.setCurrentScenario(scenario); }
		catch (ElementNotFound ex) { throw new RuntimeException(
			"Should not happen."); }
			
		scenarios.add(scenario);
	}

	
	public synchronized boolean removeScenario(Scenario scenario)
	{
		return scenarios.remove(scenario);
	}
	
	
	public synchronized boolean containsDomain(Domain domain)
	{
		for (Scenario scenario : scenarios)
		{
			Domain d = scenario.getDomain();
			if (domain == d) return true;
		}
		
		return false;
	}
	
	
	//public Set<SimCallback> getSimCallbacks() ...how should this be set?
	
	
	//public Set<DecisionCallback> getDecisionCallbacks() ...
	
	
	
	/**
	 * Retrieve the Peer for the specified node, for communicating with client-side
	 * listeners. A Peer is associated with a Scenario.
	 * If no ModelEngine has been established, then return null.
	 */
	 
	public Peer getPeer(PersistentNode node)
	{
		if (this.modelEngine == null)
		{
			//(new Exception("Warning: modelEngine is null")).printStackTrace();
			return null;
		}
		
		return (Peer)node;
	}
	
	
	/**
	 * Notify the client (that is associated with this Service Context) by sending
	 * it the Notice provided.
	 */
	 
	public void notifyClient(PeerNotice notice) throws IOException
	{
		if (clientId == null) throw new RuntimeException("No client Id");
		if (notice == null) throw new RuntimeException("No notice");
		this.modelEngine.getCallbackManager().notifyClient(clientId, notice);
	}
	
	
	public static void dumpScenarios()
	{
		ServiceContext sc = ServiceContext.getServiceContext();
		Set<Scenario> scenarios = sc.getScenarios();
		GlobalConsole.println("ServiceContext has these scenarios:");
		for (Scenario s : scenarios) GlobalConsole.println("\t" + s.getName());
			
	}
}
