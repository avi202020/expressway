/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Vector;
import java.util.List;


/**
 * A thread, for performing client requests, that can be managed by the client.
 * The thread automatically
 * inherits the ServiceContext of the thread that creates it. 
 * The ServiceContext carries information about the nature of the request.
 * This context allows activities performed by the thread to access information
 * about the request without having to pass all of that information through
 * method arguments.
 */
 
public class ServiceThreadImpl extends Thread
implements ServiceThread
{
	private Integer requestHandle = null;
	private ServiceContext context = null;
	private List<String> progressMessages = new Vector<String>();
	private List<String> asyncMessages = new Vector<String>();
	
	private boolean bSetToAbort = false;
		// If true, indicates that this thread should abort gracefully.
		
	private boolean bAborted = false;
		// If true, indicates that this thread has in fact aborted.
		
	private boolean bIsCompleted = false;
		// If true, indicates that this thread has in fact completed or aborted.
		
	private Runnable runnable = null;
		// The object to execute, by calling its run() method.
	

	/**
	 * Create a ServiceThread with the specified Context.
	 */
	
	public ServiceThreadImpl(Integer handle, ServiceContext context)
	{
		this.requestHandle = handle;
		this.context = context;
	}
	
	
	/**
	 * Create a ServiceThread that shares the Context of the specified
	 * ServiceThread. This constructor is intended to be used when a
	 * ServiceThread creates additional ServiceThreads.
	 */
	
	public ServiceThreadImpl(ServiceThread thread)
	{
		ServiceContext c = thread.getServiceContext();
		if (c == null) throw new RuntimeException(
			"The context of the specified Service Thread is null.");
		
		this.requestHandle = thread.getRequestHandle();
		this.context = c;
	}
	
	
	public ServiceThreadImpl(ServiceThread thread, Runnable runnable)
	{
		this(thread);
		this.runnable = runnable;
	}
	
	
	/**
	 * Create a ServiceThread.
	 * If the current thread has a ServiceContext, use that for the new
	 * ServiceThread. Otherwise, create a new ServiceContext and apply it
	 * to both the current Thread and the new ServiceThread.
	 */
	
	public ServiceThreadImpl(Integer handle)
	{
		this.requestHandle = handle;
		this.context = ServiceContext.getServiceContext();
		// When this ServiceThread is run, it will set its ServiceContext
		// to the value of context.
	}
	
	
	/**
	 * This thread's run method. Sets the ServiceContext based on the reference
	 * captured when the constructor is run. Note that the ServiceContext is a 
	 * thread local and so it can only be set once this thread actually starts
	 * running (which it is NOT when it is constructed). The run method is
	 * final to prevent subclasses from over-riding the setting of the
	 * ServiceContext. Subclasses should over-ride the go method instead.
	 */
	 
	public final void run()
	{
		ServiceContext.setServiceContext(context);
		go();
		if (runnable != null) runnable.run();
	}
	
	
	/**
	 * Subclasses should implement this method instead of the run method
	 * to define the ServiceThread's behavior.
	 */
	
	public void go() {}

	
	public synchronized Integer getRequestHandle() { return requestHandle; }
	
	
	public synchronized ServiceContext getServiceContext()
	{
		return context;
	}


	/*
	 * Methods defined by ProgressControl:
	 */
	 
	 
	public synchronized void forceAbort()
	{
		this.interrupt();
	}


	/*
	 * Methods defined by ProgressCallback:
	 */
	 
	 
	public synchronized boolean checkAbort()
	{
		return bSetToAbort;
	}
	
	
	public synchronized void showProgress(String msg)
	{
		if (msg == null)
		{
			(new RuntimeException("msg is null")).printStackTrace();
			return;
		}
		
		progressMessages.add(msg);
		GlobalConsole.println(msg);
	}

	
	public synchronized void showMessage(String msg)
	{
		if (msg == null)
		{
			(new RuntimeException("msg is null")).printStackTrace();
			return;
		}
		
		asyncMessages.add(msg);
		GlobalConsole.println("For client " + context.getClientId() + ": " + msg);
	}

	
	public synchronized void showErrorMessage(String msg)
	{
		if (msg == null)
		{
			(new RuntimeException("msg is null")).printStackTrace();
			return;
		}
		
		asyncMessages.add(msg);
		GlobalConsole.println(msg);
		
		notifyClientOfError(msg);
	}
	

	protected void notifyClientOfError(String msg)
	{
		try
		{
			context.notifyClient(
				new PeerNoticeBase.ScenarioSimErrorMessageNotice(
					this.requestHandle,
					//id, this.simRunNodeId,
					msg));
		}
		catch (Exception ex) { GlobalConsole.printStackTrace(ex); }
	}


	public synchronized void completed()
	{
		bIsCompleted = true;
		showMessage("Service request completed.");
	}

	
	public synchronized void aborted()
	{
		bAborted = true;
		showMessage("Aborted");
	}

	
	public synchronized void aborted(String msg)
	{
		if (msg == null)
		{
			(new RuntimeException("msg is null")).printStackTrace();
			return;
		}
		
		bAborted = true;
		showMessage("Aborted: " + msg);
	}

	
	public synchronized void aborted(Exception ex)
	{
		bAborted = true;
		GlobalConsole.println("Aborted: ");
		GlobalConsole.printStackTrace(ex);
		for (Throwable t = ex; t != null; t = t.getCause())
		{
			showMessage(t.getMessage());
		}
	}

	
	public synchronized void aborted(String msg, Exception ex)
	{
		if (msg == null)
		{
			(new RuntimeException("msg is null")).printStackTrace();
			ex.printStackTrace();
			return;
		}
		
		bAborted = true;
		GlobalConsole.println("Aborted: " + msg);
		GlobalConsole.printStackTrace(ex);
		for (Throwable t = ex; t != null; t = t.getCause())
		{
			showMessage(t.getMessage());
		}
	}

	
	/*
	 * Methods defined by ServiceThread:
	 */
	 
	 
	public synchronized List<String> getProgress()
	{
		List<String> msgs = progressMessages;
		progressMessages = new Vector<String>();
		return msgs;
	}
	

	public synchronized List<String> getMessages()
	{
		List<String> msgs = asyncMessages;
		asyncMessages = new Vector<String>();
		return msgs;
	}
	

	public synchronized boolean isComplete()
	{
		return bIsCompleted;
	}
	

	public synchronized boolean isAborted()
	{
		return bAborted;
	}
	
		
	public synchronized void abort()
	{
		this.bSetToAbort = true;
	}

	
	public void waitForFinish()
	{
		try { join(); } catch (InterruptedException ie) {}
	}


	/*  Methods for subclasses. */
	
	
	protected void abortThread()
	{
		GlobalConsole.println("Aborting...");
		this.bSetToAbort = true;
	}
}
