/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import java.util.*;
import expressway.common.GlobalConsole;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement;
import expressway.server.ModelElement.*;



/**
 * Arranges child Nodes from top to bottom in one column, using the spacing
 * parameters provided to the constructor.
 */
 
public class SingleColumnLayoutManager extends LayoutManager
{
	public static final double DefaultHorizontalSpacingToContainer = 0.0;
	public static final double DefaultVerticalSpacingToContainer = 0.0;
	public static final double DefaultHorizontalSpacingBetweenNodes = 0.0;
	public static final double DefaultVerticalSpacingBetweenNodes = 0.0;
	
	
	public SingleColumnLayoutManager() {}
	
	
	public SingleColumnLayoutManager(double hContainerSpacing, double vContainerSpacing,
		double hNodeSpacing, double vNodeSpacing)
	{
		this(hContainerSpacing, hContainerSpacing, vContainerSpacing, vContainerSpacing,
			hNodeSpacing, vNodeSpacing);
	}
	
	
	public SingleColumnLayoutManager(double leftHContainerSpacing, double rightHContainerSpacing,
		double topVContainerSpacing, double bottomVContainerSpacing,
		double hNodeSpacing, double vNodeSpacing)
	{
		super();
		
		this.leftHorizontalSpacingToContainer = leftHContainerSpacing;
		this.rightHorizontalSpacingToContainer = rightHContainerSpacing;
		this.topVerticalSpacingToContainer = topVContainerSpacing;
		this.bottomVerticalSpacingToContainer = bottomVContainerSpacing;
		this.horizontalSpacingBetweenNodes = hNodeSpacing;
		this.verticalSpacingBetweenNodes = vNodeSpacing;
	}
	
	
	/** The minimum horizontal spacing between the left edge of each Node and the
		left and edge of the Container. */
	private double leftHorizontalSpacingToContainer = DefaultHorizontalSpacingToContainer;
	
	
	/** The minimum horizontal spacing between the right edge of each Node and the
		right edge of the Container. */
	private double rightHorizontalSpacingToContainer = DefaultHorizontalSpacingToContainer;
	
	
	/** The minimum vertical spacing between the top edge of each Node and the
		top edge of the Container. */
	private double topVerticalSpacingToContainer = DefaultVerticalSpacingToContainer;
	
	
	/** The minimum vertical spacing between the bottom edge of each Node and the
		bottom edge of the Container. */
	private double bottomVerticalSpacingToContainer = DefaultVerticalSpacingToContainer;
	
	
	/** The minimum horizontal spacing to insert between Nodes. */
	private double horizontalSpacingBetweenNodes = DefaultHorizontalSpacingBetweenNodes;
	
	
	/** The minimum vertical spacing to insert between Nodes. */
	private double verticalSpacingBetweenNodes = DefaultVerticalSpacingBetweenNodes;
	
	
	public void setLeftHorizontalSpacingToContainer(double s) { this.leftHorizontalSpacingToContainer = s; }
	public double getLeftHorizontalSpacingToContainer() { return leftHorizontalSpacingToContainer; }
	
	
	public void setRightHorizontalSpacingToContainer(double s) { this.rightHorizontalSpacingToContainer = s; }
	public double getRightHorizontalSpacingToContainer() { return rightHorizontalSpacingToContainer; }
	
	
	public void setTopVerticalSpacingToContainer(double s) { this.topVerticalSpacingToContainer = s; }
	public double getTopVerticalSpacingToContainer() { return this.topVerticalSpacingToContainer; }
	
	
	public void setBottomVerticalSpacingToContainer(double s) { this.bottomVerticalSpacingToContainer = s; }
	public double getBottomVerticalSpacingToContainer() { return this.bottomVerticalSpacingToContainer; }
	
	
	public void setHorizontalSpacingBetweenNodes(double s) { this.horizontalSpacingBetweenNodes = s; }
	public double getHorizontalSpacingBetweenNodes() { return this.horizontalSpacingBetweenNodes; }
	
	
	public void setVerticalSpacingBetweenNodes(double s) { this.verticalSpacingBetweenNodes = s; }
	public double getVerticalSpacingBetweenNodes() { return this.verticalSpacingBetweenNodes; }
	
	
	/**
	 * Arrange the subordinate Nodes of 'node'; and resize 'node' so that it can
	 * accommodate its subordinate Nodes. Ignore Node in 'nodesToIgnore'.
	 * If 'recursive' is true, do this for each sub-Node as well.
	 */
	 
	public void layout(PersistentNode node, boolean recursive, 
		PersistentNode[] nodesToIgnore)
	{
		double w = node.getWidth();
		double h = node.getHeight();
		
		Set<PersistentNode> subNodes = node.getChildren();
		
		if (subNodes.size() == 0)
		{
			node.setSizeToStandard();
			return;
		}
		
		double headerSpace = node.getHeaderLabelHeight();
		
		double minWidth = leftHorizontalSpacingToContainer;
		double minHeight = bottomVerticalSpacingToContainer;
		
		double x = leftHorizontalSpacingToContainer;
		double y = bottomVerticalSpacingToContainer;
		
		SortedSet<PersistentNode> predefinedNodes = node.getPredefinedNodes();
		
		
		// Position each sub-Node that is not pre-defined and that is not a Port.
		// While doing this, determine the space needed by each sub-Node.
		
		NextSubnode: for (PersistentNode subNode : subNodes)
		{
			if (! subNode.isVisible()) continue;
			if (subNode instanceof Port) continue;
			
			if (recursive)
			{
				LayoutManager lm = subNode.getLayoutManager();
				if (lm != null) lm.layout(subNode, true);
			}
			
			if (predefinedNodes.contains(subNode)) continue;
			if (nodesToIgnore != null)
				for (PersistentNode n : nodesToIgnore)
					if (n == subNode) continue NextSubnode;
			
			minWidth = Math.max(minWidth, subNode.getWidth() +
				leftHorizontalSpacingToContainer);
			minHeight += (subNode.getHeight() + verticalSpacingBetweenNodes);
			
			subNode.setX(x);
			subNode.setY(y);
			
			y += subNode.getHeight() + verticalSpacingBetweenNodes;
		}
		
		
		// Position each sub-Node that is pre-defined and that is not a Port.
		// While doing this, determine the space needed by each non-Port pre-defined sub-Node.
		
		NextPredefinedNode: for (PersistentNode predefinedSubNode : predefinedNodes)
		{
			if (! predefinedSubNode.isVisible()) continue;
			if (predefinedSubNode instanceof Port) continue;
			
			if (recursive)
			{
				LayoutManager lm = predefinedSubNode.getLayoutManager();
				if (lm != null) lm.layout(predefinedSubNode, true);
			}
			
			if (nodesToIgnore != null)
				for (PersistentNode n : nodesToIgnore)
					if (n == predefinedSubNode) continue NextPredefinedNode;
			
			minWidth = Math.max(minWidth, predefinedSubNode.getWidth() + 
				leftHorizontalSpacingToContainer);
			minHeight += (predefinedSubNode.getHeight() + verticalSpacingBetweenNodes);
			
			predefinedSubNode.setX(x);
			predefinedSubNode.setY(y);
			
			y += predefinedSubNode.getHeight() + verticalSpacingBetweenNodes;
		}			
		
		minWidth += rightHorizontalSpacingToContainer;
		minHeight += topVerticalSpacingToContainer + headerSpace;
		
		
		// Ensure that there is room for the Ports.
		
		if (node instanceof PortedContainer)
		{
			PortedContainer pc = (PortedContainer)node;

			double minIconWidth = node.getIconWidth();
			double minIconHeight = node.getIconHeight();

			int noOfVerticalPorts = Math.max(pc.getPorts(Position.left).size(),
				pc.getPorts(Position.right).size());
			double portDiam = pc.getModelDomain().getVisualGuidance().getProbablePortDiameter();
			
			double spaceRequiredForInputPorts = noOfVerticalPorts * portDiam * 1.1;
			
			minHeight = Math.max(minHeight, spaceRequiredForInputPorts +
				topVerticalSpacingToContainer + headerSpace + bottomVerticalSpacingToContainer);
			minIconHeight = Math.max(minIconHeight, spaceRequiredForInputPorts);
			

			// Set icon size based on the room needed for the Ports.
			
			pc.setIconWidth(minIconWidth);
			pc.setIconHeight(minIconHeight);
		}
		
		minWidth = Math.max(node.getMinimumWidth(), minWidth);
		minHeight = Math.max(node.getMinimumHeight(), minHeight);
		
		node.setWidth(Math.max(w, minWidth));
		node.setHeight(Math.max(h, minHeight));
		

		// Set location of each Port, for both the iconized and non-iconized configuration.
		
		if (node instanceof PortedContainer)
		{
			List<Port> ports = ((PortedContainer)node).getInputPorts();
			for (Port port : ports) port.setSide(port.getSide());
		}
	}
}

