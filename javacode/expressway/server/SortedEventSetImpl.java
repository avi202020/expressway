/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.server.Event.*;
import java.util.Collection;
import generalpurpose.TreeSetNullDisallowed;
import java.util.Comparator;
import java.util.Date;
import java.io.Serializable;


public class SortedEventSetImpl<T extends Event> extends TreeSetNullDisallowed<T>
	implements SortedEventSet<T>
{
	SortedEventSetImpl()
	{
		super(new EventComparator());
	}
	
	
	SortedEventSetImpl(Collection<T> events)
	{
		super(events);
	}
	
	
	SortedEventSetImpl(SortedEventSet<T> events)
	{
		super(events);
	}
	
	
	static class EventComparator implements Comparator<Event>, java.io.Serializable
	{
		public int compare(Event o1, Event o2)
		{
			if (o1 == o2) return 0; // same object instance.
			
			
			// Check if one of the Events is a Startup Event, but not the other.
			
			if (o1 instanceof StartupEvent)
				if (! (o2 instanceof StartupEvent))
					return -1;
			
			if (o2 instanceof StartupEvent)
				if (! (o1 instanceof StartupEvent))
					return 1;
			
			
			// At least one Event is not a Startup Event.
			
			Date date1 = o1.getTimeToOccur();
			Date date2 = o2.getTimeToOccur();

			if (date1 == null)
			{
				if (date2 == null)
					
					if (o1.hashCode() < o2.hashCode()) return -1;
					else return 1;  // two null dates are
						// not considered equal, so arbitarily order them
						// based on hashcode..

				else return -1;  // a null date is considered earlier
					// than any non-null date.
			}
			else if (date2 == null)
			{
				return 1;
			}
			else
			{
				int timeComparison = date1.compareTo(date2);
				if (timeComparison != 0) return timeComparison;

				Serializable value1 = o1.getNewValue();
				Serializable value2 = o2.getNewValue();

				if (o1.hashCode() < o2.hashCode()) return -1;
				if (o1.hashCode() > o2.hashCode()) return 1;
				return 0;
			}
		}

		public boolean equals(Object obj) { return super.equals(obj); }
	}
	
	
	public Object clone()
	{
		return super.clone();
	}
	
	
	//public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	// I don't think this is used.
	//{
	//	SortedEventSetImpl<T> setCopy = new SortedEventSetImpl();
	//	for (Event e : this)
	//	{
	//		setCopy.add((T)e.clone(cloneMap, cloneParent));
	//	}
	//	
	//	return setCopy;
	//}
	
	
	public void dump(int indentation)
	{
		String indentString = "\t";
		for (int i = 1; i <= indentation; i++) indentString = indentString + "\t";
		
		for (Event e : this) GlobalConsole.println(indentString + e.toString());
	}
}
