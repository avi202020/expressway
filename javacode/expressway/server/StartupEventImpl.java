/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.Event.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import java.util.Date;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.IOException;


public class StartupEventImpl extends SimulationTimeEventImpl implements StartupEvent, Cloneable
{
	private ModelComponent component = null;
	
	private Epoch epoch;
	
	
	public EventSer externalize() throws ModelContainsError
	{
		synchronized (this.getState().getDomain())
		{
		EventSer eventSer = super.externalize();
		
		// Set all transient externalizable Node fields.
		
		
		// Set Ser fields.
		
		((StartupEventSer)eventSer).componentNodeId = (component == null? null : component.getNodeId());
		((StartupEventSer)eventSer).epoch = epoch;
		
		return eventSer;
		}
	}
	
	
	public Class getSerClass() { return StartupEventSer.class; }
	
		
	//public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent)
	//throws
	//	CloneNotSupportedException
	//{
	//	StartupEventImpl newInstance = 
	//		(StartupEventImpl)(super.clone(cloneMap, cloneParent));
	//	
	//	newInstance.component = (ModelComponent)(cloneMap.getClonedNode(component));
	//	
	//	return newInstance;
	//}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		this.epoch = null;
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
	}
	
	
	public StartupEventImpl(ModelComponent component, SimulationRun simRun, Epoch epoch)
	throws
		ParameterError
	{
		super(null, null, null, simRun, epoch);
		
		this.component = component;
	}
	
	
	
	public State getState() { return null; }
	
	
	
	/* From SimulationTimeEvent */
	
	public Epoch getEpoch() { return epoch; }
	
	
	public void setEpoch(Epoch epoch) { this.epoch = epoch; }
		
		
	//public String encodeAsString()
	//throws
	//	IOException
	//{
	//	throw new RuntimeException("Not implemented");
	//}
	
	
	public int getIteration() { return epoch.getIteration(); }
	
	
	
	/* From Startup Event */
	
	public ModelComponent getComponent() { return this.component; }
	
	
	public String toString()
	{
		return getClass().getName() + " for " + component.getName();
	}
}
