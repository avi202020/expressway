/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.Event.*;
import expressway.server.ModelElement.*;
import java.util.Collection;
import generalpurpose.TreeSetNullDisallowed;
import java.util.List;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;
import java.util.HashSet;
import java.util.Vector;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public class StateImpl extends ModelComponentImpl implements State
{
	/** Ownership. */
	private Set<PredefinedEvent> preEvents = new HashSet<PredefinedEvent>(); // Maintained
		// in sequence of ascending time.

	private Set<AttributeStateBinding> attributeBindings = new TreeSetNullDisallowed<AttributeStateBinding>();

	private Set<Port> portBindings = new TreeSetNullDisallowed<Port>();
	
	public SortedSet<PersistentNode> getChildren() // must extend
	{
		SortedSet<PersistentNode> children = super.getChildren();
		children.addAll(preEvents);
		return children;
	}
	
	
	public void deleteChild(PersistentNode child, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		// Delete (per the Delete Pattern) or disconnect the Node from any other
		// child Nodes known to this class that depend on the existence of the Node
		// that is being deleted. These are normally siblings of the Node that
		// is being deleted.
		
		// Call super.deleteChild.
		try { super.deleteChild(child, outermostAffectedRef); }
		catch (ParameterError pe)
		{
			// Nullify references to the Node that this class knows about.
			if (! preEvents.remove(child)) throw pe;
	
			// Set outermostAffectedRef argument, if any.
			setRefArgIfNotNull(outermostAffectedRef, this);
		}
	}
	
	
	public PersistentNode getChild(String name) // must extend
	throws
		ElementNotFound
	{
		try { return super.getChild(name); }
		catch (ElementNotFound ex)
		{
			for (PredefinedEvent e : preEvents) if (e.getName().equals(name)) return e;
			throw ex;
		}
	}
	

	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<state name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		for (PredefinedEvent preEvent : preEvents)
		{
			if (! (preEvent instanceof PredefinedEvent)) throw new RuntimeException(
				"Event in State's list of predefined Event is not a PredefinedEvent");
			
			((PredefinedEvent)preEvent).writeAsXML(writer, indentation+1);
		}
		
		for (Port port : portBindings) // each bound Port port,
		{
			writer.println(indentString2 + "<port_binding name=\"" + port.getName() + "\" />");
		}
			
		
		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</state>");
	}
	

	public Class getSerClass() { return StateSer.class; }
	
	
	public String[] getPreEventNodeIds() { return createNodeIdArray(preEvents); }
	public String[] getAttributeBindingNodeIds() { return createNodeIdArray(attributeBindings); }
	public String[] getPortBindingNodeIds() { return createNodeIdArray(portBindings); }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((StateSer)nodeSer).stateNo = this.getEventProducer().getStateNo(this);
		((StateSer)nodeSer).eventProducerNodeId = this.getEventProducer().getNodeId();
		((StateSer)nodeSer).preEventNodeIds = this.getPreEventNodeIds();
		((StateSer)nodeSer).attributeBindingNodeIds = this.getAttributeBindingNodeIds();
		
		Set<ModelAttribute> boundAttrs = new TreeSetNullDisallowed<ModelAttribute>();
		for (AttributeStateBinding binding : attributeBindings) try
		{
			boundAttrs.add(binding.getAttribute());
		}
		catch (ValueNotSet w) { continue; } // to next binding.
		
		((StateSer)nodeSer).boundAttrNodeIds = createNodeIdArray(boundAttrs);
		
		((StateSer)nodeSer).portBindingNodeIds = this.getPortBindingNodeIds();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		StateImpl newInstance = (StateImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.preEvents = 
			PersistentNodeImpl.cloneNodeSet(preEvents, cloneMap, newInstance);
		
		newInstance.attributeBindings = new TreeSetNullDisallowed<AttributeStateBinding>();
		for (AttributeStateBinding b : attributeBindings)
			newInstance.attributeBindings.add((AttributeStateBinding)(cloneMap.getClonedNode(b)));
		
		newInstance.portBindings = new TreeSetNullDisallowed<Port>();
		for (Port p : portBindings)
			newInstance.portBindings.add((Port)(cloneMap.getClonedNode(p)));
				
		return newInstance;
	}

	
	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		Set<PredefinedEvent> preEvents = new HashSet<PredefinedEvent>();
		preEvents.addAll(getPredefinedEvents());
		for (PredefinedEvent preEvent : preEvents) 
			deletePredefinedEvent((PredefinedEvent)preEvent, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		Set<AttributeStateBinding> bindingsCopy = new TreeSetNullDisallowed<AttributeStateBinding>(attributeBindings);
		for (AttributeStateBinding b : bindingsCopy) try
		{
			b.getAttribute().stateDeleted(this);
		}
		catch (ValueNotSet w) { continue; } // to next binding.
		
		// Nullify all references.
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = super.setNameAndNotify(newName);
		
		Set<AttributeStateBinding> bindingsCopy = new TreeSetNullDisallowed<AttributeStateBinding>(attributeBindings);
		for (AttributeStateBinding b : bindingsCopy) try
		{
			b.getAttribute().stateNameChanged(this, oldName);
		}
		catch (ValueNotSet w) { continue; } // to next binding
		
		return oldName;
	}
	

	/**
	 * Constructor.
	 */

	public StateImpl(String name, EventProducer eventProducer)
	{
		super(name, (ModelContainer)eventProducer, eventProducer.getModelDomain());
		init();
	}


	public StateImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		init();
	}
	
	
	public String getTagName() { return "state"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.StateIconImageName);
	}
	
	
	public void init()
	{
		setResizable(false);
		setLayoutManager(DefaultLayoutManager);
		//setUseBorderWhenIconified(false);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 50.0;
	private static final double PreferredHeightInPixels = 1.0; // a very small
		// non-zero amount, to minimize the visual footprint of a State. Cannot
		// be zero because then there would be no space for client selection
		// events.
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }

	
	//public String getFullName() { return getEventProducer().getFullName() + "." + getName(); }


	public EventProducer getEventProducer() { return (EventProducer)(getParent()); }


	public Set<Port> getPortBindings() { return portBindings; }


	public void bindPort(Port p)
	throws
		ParameterError
	{
		// Verify that the Port belongs to this State's PortedContainer.
		
		PortedContainer container = (PortedContainer)(p.getParent());
		if (container != this.getParent()) throw new ParameterError(
			"Port and State must belong to the same Container");
		
		
		// Remove any existing binding for the Port.
		
		List<State> states = container.getStates();
		for (State state : states)
		{
			if (state.getPortBindings().contains(p))
			{
				state.unbindPort(p);
				break;  // because a Port is never bound to more than one State.
			}
		}
		
		this.portBindings.add(p);
	}


	public void unbindPort(Port p)
	throws
		ParameterError
	{
		if (! portBindings.remove(p)) throw new ParameterError(
			"State " + this.getFullName() + " is not bound to Port " + p.getFullName());
	}


	public void addAttributeBinding(AttributeStateBinding binding)
	{
		attributeBindings.add(binding);
	}


	public Set<AttributeStateBinding> getAttributeBindings()
	{
		return attributeBindings;
	}


	public PredefinedEvent predefineEvent(String name, String timeExprString,
		String valueExprString, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (name == null) name = createUniqueName("Event ");
		else checkForUniqueness(name);
		
		PredefinedEvent event = new PredefinedEventImpl(name, this, timeExprString,
			valueExprString);
		preEvents.add(event);
		
		event.layout(layoutBound, outermostAffectedRef);
		
		return event;
	}


	public PredefinedEvent predefineEvent(String name, ModelAttribute timeAttr,
		ModelAttribute valueAttr, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (name == null) name = createUniqueName("Event ");
		else checkForUniqueness(name);
		
		ModelDomain modelDomain = getModelDomain();
		ModelScenario modelScenario = 
			(ModelScenario)
			(ServiceContext.getServiceContext().getScenario(modelDomain));
			
		if (modelScenario == null)
		{
			modelScenario = (ModelScenario)(modelDomain.createScenario(modelDomain.getName()));
		}
			
		Serializable serT = modelScenario.getAttributeValue(timeAttr);
		Serializable serV = modelScenario.getAttributeValue(valueAttr);
		
		PredefinedEvent event = new PredefinedEventImpl(name, this, 
			serT.toString(), serV.toString(), timeAttr, valueAttr);
		preEvents.add(event);
		
		event.layout(layoutBound, outermostAffectedRef);

		return event;
	}
	
	
	public PredefinedEvent predefineEvent(String name, PersistentNode layoutBound, 
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		if (name == null) name = createUniqueName("Event ");
		else checkForUniqueness(name);
		
		PredefinedEvent event = new PredefinedEventImpl(name, this, null, null);
		
		preEvents.add(event);
		
		event.layout(layoutBound, outermostAffectedRef);

		return event;
	}
	
	
	public void deletePredefinedEvent(PredefinedEvent event, PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		deleteChild(event, outermostAffectedRef);
	}


	public Set<PredefinedEvent> getPredefinedEvents()
	{
		return preEvents;
	}
	
	
  /* ***************************************************************************
   * From ModelElement:
   */

	/** Initialization data for the instantiable Element Lists. */
	private static final Object[][] instantiableElements =
	{
		{ NodeKindNames.PredefinedEvent, "predefineEvent", "@Predefined Events" }
	};
	

	protected static List<String> staticInstantiableNodeKinds;
	protected static List<String> staticCreateMethodNames;
	protected static List<String> staticNodeDescriptions;
	
	
	static
	{
		List<String> kinds = ModelElementImpl.staticInstantiableNodeKinds;
		List<String> names = ModelElementImpl.staticCreateMethodNames;
		List<String> pageNames = ModelElementImpl.staticNodeDescriptions;
		
		staticInstantiableNodeKinds = new Vector<String>(kinds);
		for (Object[] oa : instantiableElements)
			staticInstantiableNodeKinds.add((String)(oa[0]));
		
		staticCreateMethodNames = new Vector<String>(names);
		for (Object[] oa : instantiableElements)
			staticCreateMethodNames.add((String)(oa[1]));
		
		staticNodeDescriptions = new Vector<String>(pageNames);
		for (Object[] oa : instantiableElements)
			staticNodeDescriptions.add((String)(oa[2]));
	}
	
	
	List<String> getStaticNodeKinds()
	{
		return staticInstantiableNodeKinds;
	}
	
	
	List<String> getStaticNodeDescriptions()
	{
		return staticNodeDescriptions;
	}
	
	
	public List<String> getInstantiableNodeKinds()
	{
		List<String> names = staticInstantiableNodeKinds;
		getDynamicInstantiableNodeKind(names);
		return names;
	}
	
	
	public List<String> getStaticCreateMethodNames()
	{
		return staticCreateMethodNames;
	}
	
	
	public List<String> getInstantiableNodeDescriptions()
	{
		return super.getInstantiableNodeDescriptions();
	}
	
	
	private int mostRecentPreEventNo = 0;
	
	
	synchronized String createUniqueName(String baseName)
	{
		for (;;)
		{
			mostRecentPreEventNo++;
			String newName = baseName + String.valueOf(mostRecentPreEventNo);
			try { checkForUniqueness(newName); }
			catch (ParameterError pe) { continue; }
			
			return newName;
		}
	}

	
	
  /* ***************************************************************************
   * ChangeListeners:
   */

	public void attributeStateBindingDeleted(AttributeStateBinding binding)
	{
		if (binding == null) return;
		this.attributeBindings.remove(binding);
	}
	
	
	public void attributeStateBindingNameChanged(AttributeStateBinding asBinding, String oldName)
	{
	}
	
	
	public void portDeleted(Port port)
	{
		try { unbindPort(port); }
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}


	public void portNameChanged(Port port, String oldName)
	{
	}
	
	
	public String toString()
	{
		return getFullName();
	}
	
	
	public void dump(int indentation)
	{
		super.dump(indentation);
		
		// Dump port bindings.
		
		String indentString = "\t";
		for (int i = 1; i <= indentation; i++) indentString = indentString + "\t";

		/*
		for (Port port : portBindings)
		{
			GlobalConsole.println(indentString + "->Bound to Port " + port.getName());
		}
		*/
	}


	static void dump(Set<State> states, int indentation, String title)
	{
		for (int i = 1; i <= indentation; i++) GlobalConsole.print("\t");
		GlobalConsole.println(title);
		for (State state : states)
		{
			state.dump(indentation+1);
		}
	}
}
