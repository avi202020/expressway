/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;


import java.util.Set;
import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.Event.*;
import expressway.server.ModelElement.*;
import generalpurpose.TreeSetNullDisallowed;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;


public final class TerminalImpl extends FunctionImpl implements Terminal
{
	/** reference only. */
	public Port port = null;

	/** reference only. */
	public State state = null;

	public int color = 0;
	
	
	public transient String portNodeId = null;
	public transient String stateNodeId = null;
	
	public String getPortNodeId() { return portNodeId; }
	public String getStateNodeId() { return stateNodeId; }
	public int getColor() { return color; }


	public Class getSerClass() { return TerminalSer.class; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		// Set all transient externalizable Node fields.
		
		portNodeId = getNodeIdOrNull(port);
		stateNodeId = getNodeIdOrNull(state);
		
		
		// Set Ser fields.
		
		((TerminalSer)nodeSer).portNodeId = this.getPortNodeId();
		((TerminalSer)nodeSer).stateNodeId = this.getStateNodeId();
		((TerminalSer)nodeSer).color = this.getColor();
		
		return super.externalize(nodeSer);
	}
	
		
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		TerminalImpl newInstance = (TerminalImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.port = cloneMap.getClonedNode(port);
		newInstance.state = cloneMap.getClonedNode(state);
		
		return newInstance;
	}


	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;

		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<terminal name=\"" + this.getName() + "\" "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
		
		
		writeXMLAttributesForPredefElements(writer, indentation+1);


		// Write each Attribute.
		
		Set<Attribute> attrs = getAttributes();
		for (Attribute attr : attrs) attr.writeAsXML(writer, indentation+1);

		
		writer.println(indentString + "</terminal>");
	}
	

	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		try
		{
		deletePort(port, null);
		deleteState(state, null);
		}
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		
		// Nullify all references.
		port = null;
		state = null;
	}
	
	
	/**
	 * Constructor.
	 */

	public TerminalImpl(String name, ModelContainer parent, ModelDomain domain)
	{
		super(name, parent, domain);
		init();
	}


	public TerminalImpl(ModelContainer parent, String baseName)
	{
		super(parent, baseName);
		init();
	}
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.TerminalIconImageName);
	}
	
	
	private void init()
	{
		setResizable(false);
		//setUseBorderWhenIconified(false);

		try { port = super.createPort("TerminalPort", PortDirectionType.bi, 
			Position.left, this, null); }
		catch (ParameterError ex) { throw new RuntimeException(
			"Unexpected", ex); }


		// Automatically bind the Port to the internal State.

		try { state = super.createState("TerminalState", this, null); }
		catch (ParameterError ex) { throw new RuntimeException(
			"Unexpected", ex); }

		//state.setX(this.getWidth()/2.0);
		//state.setY(this.getHeight()/2.0);
		
		state.setPredefined(true);

		try { state.bindPort(port); }
		catch (ParameterError pe) { throw new RuntimeException(pe); }
		
		state.setMovable(false);
		
		state.setVisible(false);

		this.color = 0x4b9800;
	}
	
	
	public void layout()
	{
		super.layout();
		
		port.setX(this.getWidth());
		port.setY(this.getHeight());
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public void setPortDirection(Port port, PortDirectionType dir)
	throws
		ParameterError
	{
		throw new ParameterError("Cannot change Port direction for a standard component");
	}


	public Port getPort() { return port; }

	public void setAttention() { this.color = 0xfcff00; }

	public void setUrgent() { this.color = 0xff0000; }
}


final class TerminalNativeImpl extends FunctionNativeImplBase
	implements Function.JavaFunctionImplementation
{
	State state = null;
	String stateName = "TerminalState";
	double singleUserReqRate = 0.1;


	public void start(FunctionContext context) throws Exception
	{
		super.start(context);
		state = context.getState(stateName);
	}


	/**
	 *	1. Select the Port that is currently driven (it is an error if multiple Ports
	 *		are driven with different values during the same epoch).
	 *
	 *	2. Update its internal state with the value of that Port.
	 *
	 *	3. Since all of the Terminal's Ports are bound to the internal state,
	 *		all then receive events (in the current epoch) containing the new state value.
	 */

	public void respond(SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		// Get the value on the Port.
		
		Serializable inputValue = null;
		
		boolean nonCompensationEventFound = false;
		
		SortedEventSet<GeneratedEvent> eventsOnPort = getEventsOnPort(events);
		for (GeneratedEvent event : eventsOnPort)
		{
			if (event.getState() == null) continue;  // Allow for Startup Events.
			
			if ((inputValue != null) && 
					(! ObjectValueComparator.compare(inputValue, event.getNewValue())))
					//(! inputValue.equals(event.getNewValue())))
				throw new RuntimeException(
					"Model permitted conflicting events on Port " +
						getPort().getName());
						
			if (! event.isCompensation()) nonCompensationEventFound = true;
						
			inputValue = event.getNewValue();
		}

		if (inputValue == null)
		{
			return; // do not drive output.
		}

		//System.out.println("-----> Generating Event for Terminal " +
		//	getModelComponent().getName());
		GeneratedEvent event = null;
		
		if (nonCompensationEventFound)
			getFunctionContext().setState(this.state, inputValue, events);
		else
			getFunctionContext().scheduleCompensationEvent(this.state, inputValue,
				events);
	}
	
	
	protected Port getPort()
	throws
		ModelContainsError
	{
		Port port = null;
		try
		{
			port = ((TerminalImpl)(getModelComponent())).getPort();
		}
		catch (ClassCastException ex)
		{
			throw new ModelContainsError(
				"Native implementation for a Terminal is not bound to a " +
				"TerminalImpl; it is bound to a " + 
				getModelComponent().getClass().getName()
				);
		}
		
		return port;
	}
	
	
	protected SortedEventSet<GeneratedEvent> getEventsOnPort(
		SortedEventSet<GeneratedEvent> events)
	throws
		ModelContainsError
	{
		return getEventsOnPort(getPort(), events);
	}
}
