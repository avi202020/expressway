/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.ModelElement.*;
import expressway.server.Event.*;
import expressway.ser.*;
import java.io.*;
import java.util.*;


public class ValueHistoryImpl implements ValueHistory, Serializable, Cloneable
{
	/** History of each bound State, indexed by the State's Id. Compensation
		Events are not stored. The first Event in the time period is the most
		recent Event at the start of the time period - even if that Event occurred
		before the time period. */
	private Map<String, EventSer[]> history = new HashMap<String, EventSer[]>();
	
	private long startTime;
	private long deltaTime;
	private long endTime;
	
	
	public ValueHistoryImpl(long startTime, long deltaTime, long endTime)
	{
		this.startTime = startTime;
		this.deltaTime = deltaTime;
		this.endTime = endTime;
	}
	
	
	/** For reconstructing a Value History. */
	ValueHistoryImpl() {}
	
	
	public long getStartTime() { return startTime; }
	public long getDeltaTime() { return deltaTime; }
	public long getEndTime() { return endTime; }
	
	
	void setStartTime(long startTime) { this.startTime = startTime; }
	void setDeltaTime(long deltaTime) { this.deltaTime = deltaTime; }
	void setEndTime(long endTime) { this.endTime = endTime; }
	
	
	public long getStartTimeForInterval(int interval)
	{
		return startTime + deltaTime * interval;
	}
	
	
	public Set<String> getStateIds() { return history.keySet(); }
	
	
	public EventSer[] getEventSersForStateId(String stateId) { return history.get(stateId); }
	
	
	public Serializable getMostRecentRisingValueAtTime(State state, long simulatedTime)
	throws
		ParameterError
	{
		Serializable mostRecentValue = null;
		EventSer[] eventSers = history.get(state.getNodeId());
		for (EventSer eventSer : eventSers)
		{
			long eventMs = eventSer.timeToOccur.getTime();
			
			if (eventMs > simulatedTime) break;
			
			mostRecentValue = eventSer.newValue;
			
			if (eventMs == simulatedTime) break;
		}
		
		return mostRecentValue;
	}
	
	
	public ValueHistory makeCopy() throws CloneNotSupportedException
	{
		return (ValueHistory)(clone());
	}
	
	
	protected Object clone() throws CloneNotSupportedException
	{
		ValueHistoryImpl vh = (ValueHistoryImpl)(super.clone());
		vh.history = new HashMap<String, EventSer[]>();
		Set<String> keys = history.keySet();
		for (String key : keys)
		{
			EventSer[] eventSers = history.get(key);
			EventSer[] eventSersCopy = new EventSer[eventSers.length];
			int i = 0;
			for (EventSer eventSer : eventSers) eventSersCopy[i++] = eventSer;
			vh.history.put(key, eventSersCopy);
		}
		
		return vh;
	}
	
	
	void addStateHistory(State state, EventSer[] eventSers)
	{
		history.put(state.getNodeId(), eventSers);
	}
	
	
	void addStateHistory(String stateId, EventSer[] eventSers)
	{
		history.put(stateId, eventSers);
	}
	
	
	/** For debug only */
	public void dump()
	{
		try
		{
			System.out.println("startTIme=" + (new Date(startTime)));
			System.out.println("endTIme=" + (new Date(endTime)));
			System.out.println("deltaTIme=" + (deltaTime / (24*60*60*1000)) + " days");
			
			Set<String> keys = history.keySet();
			for (String stateId : keys)
			{
				ModelElement.State state = 
					(ModelElement.State)(PersistentNodeImpl.getNode(stateId));
				
				System.out.print(state.getFullName() + ": ");
				
				EventSer[] eventSers = history.get(stateId);
				
				for (EventSer eventSer : eventSers)
				{
					System.out.print(eventSer.timeToOccur + ", ");
				}
				
				System.out.println();
				
				for (EventSer eventSer : eventSers)
				{
					System.out.print(eventSer.newValue + ", ");
				}
				
				System.out.println();
			}
		}
		catch (Exception ex) { ex.printStackTrace(); }
	}
}


