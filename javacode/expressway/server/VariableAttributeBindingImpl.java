/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.ser.*;
import expressway.server.Event.*;
import expressway.server.DecisionElement.*;
import expressway.server.ModelElement.ModelAttribute;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Set;
import generalpurpose.TreeSetNullDisallowed;
import java.util.SortedSet;


public class VariableAttributeBindingImpl extends DecisionElementImpl
	implements VariableAttributeBinding
{
	private ModelElement.ModelAttribute attribute = null;
	
	
	VariableAttributeBindingImpl(Variable variable, ModelElement.ModelAttribute attribute)
	{
		super(variable);
		setResizable(false);
		this.attribute = attribute;
	}


  /* From TemplateInstance */
	
	
	public void initialize(Class nativeImplClass, PersistentNode layoutBound,
		PersistentNode[] outermostAffectedRef)
	throws
		ParameterError
	{
		setResizable(false);
	}


	public Class getSerClass() { return VariableAttributeBindingSer.class; }
	
	
	public String getTagName() { return "attr_binding"; }
	
	
	public void setIconImageToDefault() throws IOException
	{
		setIconImage(NodeIconImageNames.VariableAttributeBindingIconImageName);
	}
	
	
	public int getNoOfHeaderRows() { return 0; }
	
	
	private static final double PreferredWidthInPixels = 30.0;
	private static final double PreferredHeightInPixels = 30.0;
	
	public double getPreferredWidthInPixels() { return PreferredWidthInPixels; }
	public double getPreferredHeightInPixels() { return PreferredHeightInPixels; }
	
	
	public NodeSer externalize(NodeSer nodeSer) throws ParameterError
	{
		((VariableAttributeBindingSer)nodeSer).attributeNodeId = getNodeIdOrNull(attribute);
		
		return super.externalize(nodeSer);
	}
	
		
	public void writeAsXML(PrintWriter writer, int indentation)
	throws
		IOException
	{
		if (this.isPredefined()) return;
		
		String indentString = getIndentString(indentation);
		String indentString2 = getIndentString(indentation+1);
		
		writer.println(indentString + "<attr_binding "
			+ (isDeletable() ? "" : "deletable=\"false\" ")
			+ (attribute == null ? "" : ("attribute=" + attribute.getFullName() + "\" "))
			+ "x=\"" + this.getX() + "\" "
			+ "y=\"" + this.getY() + "\" >");
			

		writer.println(indentString + "</attr_binding>");
	}
	
	
	public PersistentNode clone(CloneMap cloneMap, PersistentNode cloneParent) throws CloneNotSupportedException
	{
		VariableAttributeBindingImpl newInstance = (VariableAttributeBindingImpl)(super.clone(cloneMap, cloneParent));
		
		newInstance.attribute = cloneMap.getClonedNode(attribute);
		
		return newInstance;
	}


	public void prepareForDeletion()
	{
		// Delete (per the Delete Pattern) any Nodes for which the class
		// holds a primary reference. This is how a chain of owned Nodes is deleted.
		
		// Call super.prepareForDeletion, if any.
		super.prepareForDeletion();
		
		// Call <Node-type>Deleted on each ChangeListener that listens for changes
		// to the deleted Node. (See the table �Change Listeners�.) Each Change Listener
		// holds an alias (non-owning reference) to the deleted Node, and when its
		// <deleted-Node-type>Deleted method is called, it must nullify that alias.
		// Call super.prepareForDeletion, if any.
		if (attribute != null)
		{
			attribute.variableAttributeBindingDeleted(this);
			attribute.getModelDomain().variableAttributeBindingDeleted(this);
		}
		
		// Nullify all references.
	}
	
	
	public String setNameAndNotify(String newName)
	throws
		ParameterError
	{
		String oldName = super.setNameAndNotify(newName);
		
		if (attribute != null)
		{
			attribute.variableAttributeBindingNameChanged(this, oldName);
			attribute.getModelDomain().variableAttributeBindingNameChanged(this, oldName);
		}
		
		return oldName;
	}
	

	public Variable getVariable() throws ValueNotSet
	{
		if (getParent() == null) throw new ValueNotSet(
			"Field 'parent' for '" + getFullName() + "'");
		return (Variable)(getParent());
	}


	public ModelAttribute getAttribute() throws ValueNotSet
	{
		if (attribute == null) throw new ValueNotSet(
			"Field 'attribute' for '" + getFullName() + "'");
		return attribute;
	}

	
	public void attributeDeleted(Attribute attribute)
	{
		try { this.getVariable().deleteAttributeBinding(this); }
		catch (ValueNotSet w) {}  // ok
		catch (ParameterError pe) { GlobalConsole.printStackTrace(pe); }
	}
	
	
	public void attributeNameChanged(Attribute attr, String oldName)
	{
	}
}
