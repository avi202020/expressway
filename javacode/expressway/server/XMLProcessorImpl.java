/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.server;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.ClientModel.*;
import generalpurpose.ThrowableUtil;
import java.io.*;
import java.util.*;
import org.xml.sax.SAXParseException;


public class XMLProcessorImpl
	implements ModelEngineLocal.XMLProcessor
{
	private ModelEngineLocal modelEngine;
	private boolean replace;
	private PeerListener peerListener;
	private String clientId;
	private char[] charAr;
	private List<PersistentNode> rootNodesCreated;
	
	
	XMLProcessorImpl(
		ModelEngineLocal modelEngine,
		boolean replace,  // replace classes, resources, or motifs if already present.
		PeerListener peerListener,  // may be null
		String clientId,
		char[] charAr
		)
	{
		this.modelEngine = modelEngine;
		this.replace = replace;
		this.peerListener = peerListener;
		this.clientId = clientId;
		this.charAr = charAr;
	}
	
	
	public ModelEngineLocal getModelEngine() { return this.modelEngine; }
	
	public boolean getReplace() { return this.replace; }
	
	public PeerListener getPeerListener() { return this.peerListener; }

	public String getClientId() { return this.clientId; }

	public char[] getCharAr() { return this.charAr; }
	protected void setCharAr(char[] charAr) { this.charAr = charAr; }

	public List<PersistentNode> getRootNodesCreated() { return rootNodesCreated; }
	
	
	public int processXML()
	{
		XMLParser xmlParser = null;
		int result = -1;
			
		try
		{
			//ServiceContext.create(ModelEngineLocalPojoImpl.this, this.clientId);
			
			GlobalConsole.println("Parsing XML document...");
			
			xmlParser = new XMLParser();
			
			xmlParser.setModelEngineLocal(getModelEngine());
			
			
			// Set PeerListener for the parser. This will enable the parser
			// to pass PeerListener to the ModelEngine when it simulate, so
			// that simulate() can automatically subscribe the Listener to
			// the SimulationRun.
			
			xmlParser.setPeerListener(peerListener);
			
			xmlParser.setClientId(clientId);
			
			xmlParser.setCollidingDomainNameGeneratesException(this.replace);
		
			xmlParser.parse(charAr);

			GlobalConsole.println("...Interpreting XML document...");
			
			// Must be synchronized because clients can query the database
			// while this is occurring.
			synchronized (getModelEngine())
			{
				xmlParser.genDocument();
			}
			
			this.rootNodesCreated = xmlParser.getRootNodesCreated();
			
			result = 0;
			GlobalConsole.println("...Completed XML document.");
		}
		catch (SAXParseException spe)
		{
			GlobalConsole.printStackTrace(spe);

			try { ServiceContext.getServiceContext().notifyClient(
				new PeerNoticeBase.ImportErrorNotice(
					"At line " + spe.getLineNumber() +
					", column " + spe.getColumnNumber() + ",<br>" +
					ThrowableUtil.getAllMessagesAsHTML(spe))); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (GenException ge)
		{
			GlobalConsole.printStackTrace(ge);

			// Send notice to clients that the parse failed with
			// the specified error message
			
			try { ServiceContext.getServiceContext().notifyClient(
				new PeerNoticeBase.ImportErrorNotice(
					ThrowableUtil.getAllMessagesAsHTML(ge))); }
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}			
		}
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
		finally
		{
			List<String> warnings = xmlParser.getWarnings();
			if (warnings.size() > 0) try
			{
				String warningString = "";
				boolean first = true;
				for (String warning : warnings)
				{
					if (first) first = false;
					else warningString += "\n";
					warningString += warning;
				}

				ServiceContext.getServiceContext().notifyClient(
					new PeerNoticeBase.ImportWarningNotice(warningString));
			}
			catch (Exception ex)
			{
				GlobalConsole.printStackTrace(ex);
			}
		
		
			// Note: task handles can be obtained by calling
			// xmlParser.getTaskHandles().
			
			return result;
		}
	}
}



