/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package generalpurpose;

import java.text.DateFormat;


public class DateAndTimeUtils
{
	public static final long MsInASecond = 1000;
	public static final long MsInAMinute = MsInASecond * 60;
	public static final long MsInAnHour = MsInAMinute * 60;
	public static final long MsInADay = MsInAnHour * 24;
	
	public static final DateFormat DateTimeFormat =
		DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
	
	static
	{
		DateTimeFormat.setLenient(true);
	}
	
		
	/**
	 * Convert the specified number of milliseconds to days, hours, minutes,
	 * seconds, and fractions of a second.
	 */
	 
	public static String convertMsToDayHourMinSec(long ms)
	{
		long days = ms / MsInADay;
		long remainder = ms - days * MsInADay;
		
		long hours = remainder / MsInAnHour;
		remainder = remainder - hours * MsInAnHour;
		
		long minutes = remainder / MsInAMinute;
		remainder = remainder - minutes * MsInAMinute;
		
		long seconds = remainder / MsInASecond;
		remainder = remainder - seconds * MsInASecond;
		
		return (days + "D " + hours + "H " + minutes + "M " + seconds + "." + remainder + "S");
		
	}
}

