

package generalpurpose;


import java.io.*;


public class JarUtils
{
	public static File getJarFile(String jarFilePath)
	throws
		IOException
	{
		File jarFile = new File(jarFilePath);
		if (! jarFile.exists()) throw new IOException("File '" + jarFilePath + "' does not exist");
		
		if (! (jarFilePath.endsWith(".jar") || jarFilePath.endsWith(".JAR")))
			throw new IOException("motif must be a JAR file");
		
		return jarFile;
	}
	
	
	public static byte[] getBytes(File jarFile)
	throws
		IOException
	{
		long fileLength = jarFile.length();
		if (fileLength > Integer.MAX_VALUE) throw new IOException("File too large (" + fileLength + " characters); max is " 
				+ Integer.MAX_VALUE);
		
		String fileName = jarFile.getName();
				
		byte[] jarByteAr = new byte[(int)fileLength];
		
		FileInputStream fis = new FileInputStream(jarFile);
		int noOfBytes = fis.read(jarByteAr);
		if (noOfBytes < 0) throw new IOException("Error reading file " + fileName);
		if (noOfBytes != (int)fileLength) throw new IOException(
			"File is " + fileLength + " bytes but only " + noOfBytes + " were read.");
		
		fis.close();
		
		return jarByteAr;
	}
}





