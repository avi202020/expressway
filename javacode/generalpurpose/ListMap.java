/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package generalpurpose;


import java.util.Vector;
import java.util.Set;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Iterator;


/**
 * A Map that maintains an ordering of its elements, and allows the ordering
 * to be read and modified.
 * The keys in the Map should be immutable, and null is not allowed.
 */
 
public class ListMap<K,V> extends HashMap<K,V>
{
	private List<K> list = new Vector<K>();
	
	
  /* Constructors */
	

	public ListMap()
	{
		super();
	}
	
	
	public ListMap(int initialCapacity)
	{
		super(initialCapacity);
	}
	
	
	public ListMap(int initialCapacity, float loadFactor)
	{
		super(initialCapacity, loadFactor);
	}
	
	
	public ListMap(ListMap<? extends K,? extends V> m)
	{
		super(m);
		list = new Vector<K>(m.list);
	}
	
	
  /* From HashMap */
	
	
	public void clear()
	{
		super.clear();
		list.clear();
	}
	
	
	public Object clone()
	{
		ListMap<K,V> mapClone = (ListMap<K,V>)(super.clone());
		mapClone.list = new Vector<K>(list);
		return mapClone;
	}
	
	
	public V put(K key, V value)
	{
		if (key == null) throw new RuntimeException("Null keys not allowed");
		V v = super.put(key, value);
		list.add(key);
		return v;
	}
	
	
	public void putAll(Map<? extends K,? extends V> m)
	{
		Set<? extends K> mkeys = m.keySet();
		for (K k : mkeys) if (k == null) throw new RuntimeException("Null keys not allowed");
		super.putAll(m);
		for (K k : mkeys) list.add(k);
	}
	
	
	public V remove(Object key)
	{
		V v = super.remove(key);
		if (v != null) list.remove(key);
		return v;
	}
	
	
  /* List-like methods */
	
	
	public boolean add(K key, V v)
	{
		if (get(key) != null) throw new RuntimeException(
			"Key '" + key + "' is already in map");
		super.put(key, v);
		return list.add(key);
	}
	
	
	public void add(int index, K key, V v)
	{
		if (get(key) != null) throw new RuntimeException(
			"Key '" + key + "' is already in map");
		super.put(key, v);
		list.add(index, key);
	}
	
	
	public boolean containsAll(Collection<?> c)
	{
		return list.containsAll(c);
	}
	
	
	public K getKey(int index)
	{
		return list.get(index);
	}
	
	
	public int indexOfKey(K k)
	{
		return list.indexOf(k);
	}
	
	
	public Iterator<K> iterator()
	{
		return list.iterator();
	}
	
	
	public int lastIndexOfKey(K k)
	{
		return list.lastIndexOf(k);
	}
	
	
	public K removeAtIndex(int index)
	{
		remove(list.get(index));
		return list.remove(index);
	}
	
	
	public boolean removeAllKeys(Collection<K> ks)
	{
		boolean changed = false;
		for (K k : ks)
		{
			remove(k);
			list.remove(k);
			changed = true;
		}
		
		return changed;
	}
	
	
	public boolean retainAllKeys(Collection<K> ks)
	{
		boolean changed = false;
		List<K> listCopy = new Vector<K>(list);
		for (K k : listCopy)
		{
			if (! ks.contains(k))
			{
				remove(k);
				list.remove(k);
				changed = true;
			}
		}
		
		return changed;
	}
	
	
	public K set(int index, K key, V v)
	{
		if (get(key) != null) throw new RuntimeException("Key is already in map");
		put(key, v);
		return list.set(index, key);
	}
	
	
	/**
	 * Return a List of the keys, from the specified from-index (inclusive) to
	 * the specified to-index (exclusive). Same semantics as the List.subList method.
	 */
	 
	public List<K> keySubList(int fromIndex, int toIndex)
	{
		return list.subList(fromIndex, toIndex);
	}
	
	
	/**
	 * Return a List of the keys, from the specified from-index (inclusive) through
	 * the end of the list. Same semantics as the List.subList method.
	 */
	 
	public List<K> keySubList(int fromIndex)
	{
		return list.subList(fromIndex, list.size()-1);
	}
	
	
	/**
	 * Return a List of the values.
	 */
	 
	public List<V> asList()
	{
		List<V> vlist = new Vector<V>();
		for (K k : list) vlist.add(get(k));
		return vlist;
	}
	
	
	public Object[] toKeyArray()
	{
		return list.toArray();
	}
	
	
	public <T> T[] toKeyArray(T[] a)
	{
		return list.toArray(a);
	}
}

