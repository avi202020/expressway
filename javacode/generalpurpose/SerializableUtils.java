package generalpurpose;

import java.io.*;


public class SerializableUtils
{
	public static Serializable makeCopy(Serializable ser)
	throws
		Exception
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(ser);
		oos.flush();
		byte[] data = baos.toByteArray();
		
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
		ObjectInputStream ois = new ObjectInputStream(bais);
		Object obj = ois.readObject();
		return (Serializable)obj;
	}
}

