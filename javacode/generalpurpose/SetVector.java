

package generalpurpose;


import java.util.Vector;
import java.util.Set;
import java.util.Collection;


/**
 * A Vector that implements the Set interface and ignores attempts to add
 * duplicate elements.
 *
 * To do: Improve performance by adding a hashtable or tree so that 'contains'
 * will be faster.
 */
 
public class SetVector<E> extends Vector<E> implements Set<E>
{
	public SetVector() { super(); }
	
	public SetVector(Collection<? extends E> c)
	{
		for (E e : c) if (! contains(e)) add(e);
	}
	
	public SetVector(int initialCapacity) { super(initialCapacity); }
	
	public SetVector(int initialCapacity, int capacityIncrement) { super(initialCapacity,
		capacityIncrement); }

	
	public boolean add(E e)
	{
		if (contains(e)) return false;
		return super.add(e);
	}
	
	
	public boolean addAll(Collection<? extends E> c)
	{
		boolean changed = false;
		for (E e : c) changed = changed || add(e);
		return changed;
	}
}

