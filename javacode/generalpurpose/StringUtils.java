package generalpurpose;


public class StringUtils
{
	public static String safeToStringOrNull(Object o)
	{
		if (o == null) return null;
		else return o.toString();
	}
	
	
	public static String safeToString(Object o)
	{
		String s = safeToStringOrNull(o);
		if (s == null) return "";
		else return s;
	}


	/**
	 * In the String argument, find and convert all sequences of the form %ab,
	 * where a and b are numeric characters (i.e., characters in the range from
	 * '0' through '9'), to the equivalent Unicode character, and return the
	 * resultant String with all of the escaped sequences converted.
	 */
	 
	public static String decodeEscapes(String string)
	{
		int priorPos = 0;
		String newString = "";
		for (;;)
		{
			// For characters preceding the next %.
			int pos = string.indexOf("%", priorPos);
			if (pos < 0) // there are no more % signs.
			{
				newString += string.substring(priorPos);
				break;
			}
			
			if (string.length() <= pos+3) // escape sequence is incomplete.
			{
				newString += string.substring(priorPos);
				break;
			}

			// For characters up to the escape sequence.
			newString += string.substring(priorPos, pos);
				
			// For the escape sequence.
			CharSequence intChars = string.subSequence(pos+1, pos+3);
			int i = Integer.valueOf(intChars.toString());
			newString += Character.valueOf((char)i);
			
			// Advance past the %.
			priorPos = pos + 3;
		}
		
		return newString;
	}


	/**
	 * Return a String that consists of the number of tabs specified by 'indentation';
	 */
	 
	public static String getIndentString(int indentation)
	{
		if (indentation < tabs.length) return tabs[indentation];
		String s = "";
		for (int i = 0; i < indentation; i++) s += '\t';
		return s;
	}
	
	
	public static void main(String[] args)
	{
		String s = "abc%20def";
		System.out.println("'" + decodeEscapes(s) + "'");
	}
	
	
	public static final String[] tabs = 
	{
		"",
		"\t",
		"\t\t",
		"\t\t\t",
		"\t\t\t\t",
		"\t\t\t\t\t",
		"\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t",
		"\t\t\t\t\t\t\t\t\t\t"
	};
}
