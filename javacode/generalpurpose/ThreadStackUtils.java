/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */




package generalpurpose;

import java.lang.reflect.Method;


/**
 * Utilities for diagnosing thread stack conditions.
 */
 
public class ThreadStackUtils
{
	/**
	 * Return true if the current thread is nested within a prior call to the
	 * specified Method.
	 */
	 
	public static boolean stackContainsPriorCallTo(String fullMethodName)
	{
		StackTraceElement[] traceElements = Thread.currentThread().getStackTrace();
		
		for (int pos = 2; pos < traceElements.length; pos++)  // skip the top two frames
		{
			StackTraceElement traceElement = traceElements[pos];
			String traceFullMethodName = traceElement.getClassName() + "." + traceElement.getMethodName();
			
			if (traceFullMethodName.equals(fullMethodName)) return true;
		}
		
		return false;
	}
	
	
	/**
	 * Return true if the current thread is nested within a prior call to the
	 * specified Method.
	 */
	 
	public static boolean stackContainsPriorCallTo(Method method)
	{
		String fullMethodName = method.getClass().getName() + "." + method.getName();
		return stackContainsPriorCallTo(fullMethodName);
	}
}

