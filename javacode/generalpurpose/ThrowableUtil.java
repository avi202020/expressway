/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package generalpurpose;


/**
 * Utilities pertaining to the type Throwable.
 */
 
public class ThrowableUtil
{
	/**
	 * Return a String containing all nested messages of the Throwable, separated
	 * by newline characters.
	 */
	 
	public static String getAllMessages(Throwable t)
	{
		String msg = t.getClass().getName() + ": " + safeString(t.getMessage());
		
		for (Throwable cause = t.getCause(); cause != null; cause = cause.getCause())
		{
			msg += ";\n " + safeString(cause.getMessage());
		}
		
		return msg;
	}
	
	
	/**
	 * Return an HTML page containing all nested messages of the Throwable, separated
	 * by HTML breaks.
	 */
	 
	public static String getAllMessagesAsHTML(Throwable t)
	{
		String msg = t.getClass().getName() + ": " + safeString(t.getMessage());
		msg = HTMLEncoder.stringToHTMLString(msg);
		
		for (Throwable cause = t.getCause(); cause != null; cause = cause.getCause())
		{
			msg += ";<br> " + HTMLEncoder.stringToHTMLString(safeString(cause.getMessage()));
		}
		
		return "<html>" + msg + "</html>";
	}
	
	
	protected static String safeString(String s)
	{
		if (s == null) return "null";
		return s;
	}
}

