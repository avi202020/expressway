

package generalpurpose;


import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Collection;
import  java.util.Comparator;


/**
 * A TreeSet that does not permit null elements.
 */
 
public class TreeSetNullDisallowed<E> extends TreeSet<E>
{
	public TreeSetNullDisallowed() { super(); }
	
	public TreeSetNullDisallowed(Collection<? extends E> c)
	{
		super(c);
		for (E e : c) if (e == null) throwException();
	}
	
	public TreeSetNullDisallowed(Comparator<? super E> comparator)
	{
		super(comparator);
	}
	
	public TreeSetNullDisallowed(SortedSet<E> s)
	{
		super(s);
		for (E e : s) if (e == null) throwException();
	}

	
	public boolean add(E e)
	{
		if (e == null) throwException();
		return super.add(e);
	}
	
	
	public boolean addAll(Collection<? extends E> c)
	{
		for (E e : c) if (e == null) throwException();
		return super.addAll(c);
	}
	
	protected void throwException()
	{
		RuntimeException ex = new RuntimeException("Attempt to add null to Set");
		ex.printStackTrace();
		System.out.flush();
		throw ex;
	}
}

