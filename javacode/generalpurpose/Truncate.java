/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */


package generalpurpose;


public class Truncate
{
	/**
	 * Truncate all of the digits following the first digit, and round the
	 * remaining digit.
	 */
	 
	public static double roundToNearestDigit(double v)
	{
		//System.out.println("truncate " + v);						// 2.0999999999

		int ex = (int)(Math.floor(Math.log10(v)));
		//System.out.println("ex(" + v + ")=" + ex);								// 1

		
		double factor = Math.pow(10.0, (double)ex);
		//System.out.println("factor=" + factor);						// 10

		
		double result = Math.rint(v / factor) * factor;
		//System.out.println("result=" + result);						// 0

		return result;
	}
	
}

