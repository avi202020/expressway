/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package generalpurpose;


public class XMLTools
{
	/** See http://www.xmlnews.org/docs/xml-basics.html#references */
	
	public static String encodeAsXMLAttributeValue(String s)
	{
		String result = "";
		
		for (int i = 0; i < s.length(); i++)
		{
			char c = s.charAt(i);
			if (c == '&') result += "&amp;";
			else if (c == '<') result += "&lt;";
			else if (c == '>') result += "&gt;";
			else if (c == '"') result += "&quot;";
			else if (c == '\'') result += "&apos;";
			else result += c;
		}
		
		return result;
	}
}

