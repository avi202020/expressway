/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package geometry;


/** ****************************************************************************
 * Represents a geometric line segment.
 */
 
public class LineSegment
{
	public Point p1;
	public Point p2;
	
	
	public LineSegment(Point p1, Point p2)
	{
		this.p1 = p1;
		this.p2 = p2;
	}
	
	
	public boolean intersects(LineSegment otherSegment)
	throws
		Exception  // if this Segment is congruent with the specified Segment.
	{
		return findIntersection(this, otherSegment) != null;
	}
	
	
	public double findSlope()
	throws
		Exception  // if the slope is infinite.
	{
		if (p2.getX() == p1.getX()) throw new Exception("Infinite slope");
		return (p2.getY() - p1.getY()) / (p2.getX() - p1.getX());
	}
	
	
	/**
	 * Find the y-intercept of the line that is congruent with this Segment.
	 */
	 
	public double findYIntercept()
	throws
		Exception  // if there is no y intercept (because the slope is infinite).
	{
		return p1.getY() - findSlope() * p1.getX();
	}
	
	
	/**
	 * Return true if and only if this Line Segment passes through the specified
	 * Point.
	 */
	 
	public boolean passesThrough(Point point)
	{
		double m;
		
		try { m = this.findSlope(); }
		catch (Exception ex)
		{
			if (point.getX() == p1.getX()) return true;
			return false;
		}
		
		// See if the Point is on the Segment.
		
		double b;
		try { b = findYIntercept(); }
		catch (Exception ex)  // no y intercept, because segment is vertical.
		{
			if (point.getX() == p1.getX()) return true;
			return false;
		}
		
		if (point.getY() == m * point.getX() + b)  // on the line.
			if (isBetween(point.getX(), p1.getX(), p2.getX()))
				return true;  // within the segment
				
		return false;
	}


	/**
	 * Find the intersection of LineSegments seg1 and seg2. If they do not
	 * intersect, return null. If they are congruent, throw an Exception.
	 */
	 
	public static Point findIntersection(LineSegment seg1, LineSegment seg2)
	throws
		Exception  // if the Segments are congruent.
	{
		//System.out.println("Checking if " + seg1 + " intersects " + seg2);
		
		double m1 = 0.0;
		double b1 = 0.0;
		
		double m2 = 0.0;
		double b2 = 0.0;
		
		boolean m1IsInfinite = false;
		boolean m2IsInfinite = false;
		
		double xInt;
		double yInt;
		
		
		// Determine if either (or both) of the segments is vertical (infinite
		// slope).
		
		try
		{
			//System.out.println("Finding slope for seg1; p1=" + seg1.p1 + ", p2=" + seg1.p2);
			m1 = seg1.findSlope();
			b1 = seg1.findYIntercept();
		}
		catch (Exception ex)
		{
			//ex.printStackTrace();
			m1IsInfinite = true;
		}
		
		try
		{
			m2 = seg2.findSlope();
			b2 = seg2.findYIntercept();
		}
		catch (Exception ex)
		{
			//ex.printStackTrace();
			m2IsInfinite = true;
		}
		
		//System.out.println("m1=" + m1);
		//System.out.println("b1=" + b1);
		//System.out.println("m2=" + m2);
		//System.out.println("m2=" + b2);
		
		
		// Consider each case: possible combinations of infinite and finite slope.
		
		if (m1IsInfinite && (! m2IsInfinite))  // ONLY seg1 is vertical.
		{
			xInt = seg1.p1.getX();
			yInt = m2 * xInt + b2;
		}
		else if (m2IsInfinite && (! m1IsInfinite))  // ONLY seg2 is vertical.
		{
			xInt = seg2.p1.getX();
			yInt = m1 * xInt + b1;
		}
		else if (m1IsInfinite && m2IsInfinite && (seg1.p1.getX() != seg2.p1.getX()))
		{
			// if both vertical, but not at same x location
			return null;
		}
		else if  // if the lines are congruent
		(
			(m1IsInfinite && m2IsInfinite && seg1.p1.getX() == seg2.p2.getX())
				// both vertical, both same x intercept.
			||
			((m1 == m2) && (b1 == b2))  // same slope, same y intercept.
		)
		{
			// See if the two segments touch only at their endpoints:
			// if an endpoint of one Segment equals an endpoint of the other,
			// and neither Segment passes through the other Segment's other
			// endpoint.
			
			if (check(seg1.p1, seg1.p2, seg1, seg2.p1, seg2.p2, seg2)) return seg1.p1;
			if (check(seg1.p1, seg1.p2, seg1, seg2.p2, seg2.p1, seg2)) return seg1.p1;
			if (check(seg1.p2, seg1.p1, seg1, seg2.p1, seg2.p2, seg2)) return seg1.p2;
			if (check(seg1.p2, seg1.p1, seg1, seg2.p2, seg2.p1, seg2)) return seg1.p2;
			
			throw new Exception("Segments are congruent.");
		}
		else if ((m1 == 0.0) && (m2 == 0.0))  // both lines horizontal but not congruent
		{
			return null;
		}
		else
		{
			xInt =
				(
					-m2 * seg2.p1.getX() + seg2.p1.getY() +
					 m1 * seg1.p1.getX() - seg1.p1.getY()
				)
				/ ( m1 - m2 );
				
			yInt = m1 * ( xInt - seg1.p1.getX() ) + seg1.p1.getY();
		}
			
		if (! isBetween(xInt, seg1.p1.getX(), seg1.p2.getX())) return null;
		if (! isBetween(xInt, seg2.p1.getX(), seg2.p2.getX())) return null;
		if (! isBetween(yInt, seg1.p1.getY(), seg1.p2.getY())) return null;
		if (! isBetween(yInt, seg2.p1.getY(), seg2.p2.getY())) return null;
		
		return new PointImpl(xInt, yInt);
	}
	
	
	/**
	 * Compute the distance from this Line Segment to the specified Point.
	 */
	 
	public double distanceTo(Point p)
	{
		// Compute distance to each endpoint of this Line Segment.
		
		double distToP1 = PointImpl.findDistance(p, p1);
		double distToP2 = PointImpl.findDistance(p, p2);
		
		
		// Choose the closest endpoint.
		
		Point closestEndpoint = (distToP1 > distToP2) ? p2 : p1;
		Point otherEndpoint = (closestEndpoint == p1 ) ? p2 : p1;
		double distToClosestPoint = (closestEndpoint == p1) ? distToP1 : distToP2;
		
		
		// Compute distance to the line passing through this Line Segment.
		
		Ray rayEndpointToP = null;
		try { rayEndpointToP = new Ray(closestEndpoint, p); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		Ray rayEndpointToOtherEndpoint = null;
		try { rayEndpointToOtherEndpoint = new Ray(closestEndpoint, otherEndpoint); }
		catch (Exception ex) { throw new RuntimeException(ex); }
		
		// Distance from clsest endpoint to intersection:
		double dToInters = Ray.dot(rayEndpointToP, rayEndpointToOtherEndpoint)
			/ rayEndpointToOtherEndpoint.getMagnitude();
		
		if (dToInters < 0.0)
			return distToClosestPoint;

		double distToLine = Math.sqrt(distToClosestPoint * distToClosestPoint -
			dToInters * dToInters);
		
		return distToLine;
	}
	
	
	/**
	 * Find the shortest distance from this Line Segment to the specified Line
	 * Segment. If the segments intersect, the distance is 0.
	 */
	 
	public double distanceTo(LineSegment seg)
	{
		/*
		If segments intersect,
			Return intersection point.

		For each segment, find the distance from each endpoint to the other segment.

		Return the smallest of the four distances.
		*/
		
		Point inters = null;
		try { inters = findIntersection(this, seg); }
		catch (Exception ex) { return 0.0; }  // congruent.
		
		if (inters != null) return distanceTo(inters);
		
		double d1 = this.distanceTo(seg.p1);
		double d2 = this.distanceTo(seg.p2);
		double d3 = seg.distanceTo(this.p1);
		double d4 = seg.distanceTo(this.p2);
		
		double result = Math.min(d1, d2);
		result = Math.min(result, d3);
		result = Math.min(result, d4);
		
		return result;
	}
	
	
	/**
	 * Return this Line Segment's angle in radians. The value returned is within
	 * the range between 0 and PI.
	 */
	 
	public double getRadians()
	throws
		Exception
	{
		double dy = p2.getY() - p1.getY();
		double slope = 0.0;
		double radians = 0.0;
		try
		{
			slope = this.findSlope();
			radians = Math.atan(slope);
			if (radians < 0.0) radians += Math.PI;
		}
		catch (Exception ex)  // this segment is vertical
		{
			if (dy == 0.0) throw new Exception("Line Segment is a point");
				
			radians = Math.PI / 2.0;
		}
		
		return radians;
	}
	
	
	/**
	 * Return this LineSegment's angle minus the angle of
	 * the specified LineSegment, in radians. The value is normlized so that its
	 * magnitude is less than PI.
	 */
	 
	public double radiansBetween(LineSegment otherSegment)
	throws
		Exception  // if this segment or the specified segment is a point.
	{
		double segmentRadians = this.getRadians();
		double otherSegmentRadians = otherSegment.getRadians();
		
		return segmentRadians - otherSegmentRadians;
	}


	/**
	 * Check if two Segments, seg1 and seg2, share the common endpoints p1s
	 * and p2s.
	 */
	 
	private static boolean check(Point p1s, Point p1e, LineSegment seg1, 
		Point p2s, Point p2e, LineSegment seg2)
	{
		return p1s.equals(p2s) && ! ( seg1.passesThrough(p2e) || seg2.passesThrough(p1e) );
	}
	
	
	/**
	 * Return true if and only if x falls between x0 and x1, inclusive.
	 */

	public static boolean isBetween(double x, double x0, double x1)
	{
		if (x1 > x0) return (x0 <= x) && (x <= x1);
		else return (x1 <= x) && (x <= x0);
	}
	
	
	public String toString()
	{
		return "{ LineSegment: " + (p1 == null ? "null" : p1.toString()) + 
			", " + (p2 == null ? "null" : p2.toString()) + " }";
	}
}

