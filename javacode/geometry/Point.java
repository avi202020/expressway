/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package geometry;


/**
 * A two-dimensional coordinate locaton.
 */

public interface Point extends Cloneable
{
	double getX();
	double getY();
	
	void setX(double newX);
	void setY(double newY);
	
	
	//public Object clone() throws CloneNotSupportedException;


	Object clone() throws CloneNotSupportedException;
	
	//Object deepCopy();
}
