/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package geometry;

public class PointImpl implements Point, java.io.Serializable
{
	public static final double AllowedAbsError = 0.0000000001;

	public double x;
	public double y;

	
	public PointImpl(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	
	public Object clone() throws CloneNotSupportedException
	{
		Point newPoint = (PointImpl)(super.clone());
		return newPoint;
	}

	
	public static <T extends Point> T[] clonePointArray(T[] ar)
	throws
		CloneNotSupportedException
	{
		Object o = ar.clone();
		T[] copy = (T[])o;
		
		for (int i = 0; i < ar.length; i++) copy[i] = (T)(ar[i].clone());
		
		return copy;
	}
	
	
	public double getX() { return x; }
	public double getY() { return y; }
	
	
	public void setX(double newX) { this.x = newX; }
	public void setY(double newY) { this.y = newY; }
	

	public Object deepCopy()
	{
		try { return (Point)(this.clone()); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
	}
	
	
	
	/** ************************************************************************
	 * Find the Point that is midway between point1 and point2.
	 */
	 
	public static Point findMidpoint(Point point1, Point point2)
	{
		double dx = point2.getX() - point1.getX();
		double dy = point2.getY() - point1.getY();
		
		double xMid = point1.getX() + dx/2.0;
		double yMid = point1.getY() + dy/2.0;
		
		return new PointImpl(xMid, yMid);
	}
	
	
	/** ************************************************************************
	 * Determine the distance between point1 and point2.
	 */
	 
	public static double findDistance(Point point1, Point point2)
	{
		double dx = point2.getX() - point1.getX();
		double dy = point2.getY() - point1.getY();
		
		return Math.hypot(dx, dy);
	}
	
	
	/** ************************************************************************
	 * Transform an (x, y) location, specified relative to a rotated and translated
	 * Container. The transformed (absolute) location is returned.
	 * The container's origin is translated (relative to absolute) by the (dx, dy)
	 * translation containerPos, and it is rotated about its origin by the angle
	 * containerAngle.
	 */

	public static Point transformToContainer(Point pointInContainer,
		Point containerPos, double containerAngle)
	{
		double x1 = containerPos.getX();
		double y1 = containerPos.getY();

		double x2 = pointInContainer.getX();
		double y2 = pointInContainer.getY();

		double theta = containerAngle;

		Point absPosition = new PointImpl(

			x1 + (x2 * Math.cos(theta)) - (y2 * Math.sin(theta)),
			y1 + (x2 * Math.sin(theta)) + (y2 * Math.cos(theta)));

		return absPosition;
	}

	
	public static boolean areAlmostEqual(Point p1, Point p2)
	{
		return PointImpl.findDistance(p1, p2) < AllowedAbsError;
	}
	
	
	public static boolean areAlmostEqual(double value, double expectedValue)
	{
		if (Math.abs(value - expectedValue) < AllowedAbsError) return true;
		else return false;
	}


	public boolean equals(Object p)
	{
		if (! (p instanceof PointImpl)) return false;
		
		PointImpl otherPoint = (PointImpl)p;
		
		return (x == otherPoint.x) && (y == otherPoint.y);
	}

	
	public String toString()
	{
		return "{ Point: " + x + ", " + y + " }";
	}
}
