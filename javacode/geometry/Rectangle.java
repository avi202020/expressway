/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package geometry;


/** ****************************************************************************
 * Represents a geometric rectangular area.
 */
 
public class Rectangle
{
	public Point[] corners;  // Beginning with origin, going counter-clockwise.
	
	
	/**
	 * Constructor for case in which this Rectangle has a rotation angle of 0.
	 */
	 
	public Rectangle(Point origin, double width, double height)
	{
		corners = new Point[4];
		corners[0] = origin;
		
		corners[1] = new PointImpl(origin.getX() + width, origin.getY());
		corners[2] = new PointImpl(corners[1].getX(), origin.getY() + height);
		corners[3] = new PointImpl(origin.getX(), corners[2].getY());
	}
	
	
	/**
	 * Return true if this Rectangle intersects the specified Line Segment.
	 */
	 
	public boolean intersects(LineSegment segment)
	{
		for (int i = 0; i < 4; i++)
		{
			Point p1 = this.corners[i];
			Point p2 = this.corners[(i+1)%4];
			try
			{
				if (segment.intersects(new LineSegment(p1, p2))) return true;
			}
			catch (Exception ex) // Segments are congruent.
			{
				return true;
			}
		}
	
		return false;
	}
	
	
	/**
	 * Return true if the specified Point is inside of this Rectangle.
	 */
	 
	public boolean contains(Point p, boolean includeBorder)
	{
		double x = p.getX();
		double y = p.getY();
		
		double leftBound = corners[0].getX();
		double rightBound = corners[1].getX();
		double bottomBound = corners[0].getY();
		double topBound = corners[2].getY();
		
		if (includeBorder) return
			(x >=leftBound) && (x <= rightBound) && (y >= bottomBound) && (y <= topBound);
		else return
			(x > leftBound) && (x < rightBound) && (y > bottomBound) && (y < topBound);
	}
	
	
	public String toString()
	{
		Point[] c = corners;
		return "{ Rectangle: " + c[0].toString() + ", " + c[1].toString() + ", " +
			c[2].toString() + ", " + c[3].toString() + " }";
	}
}

