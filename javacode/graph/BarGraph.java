/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package graph;


import java.awt.Graphics;
import java.awt.Color;
import geometry.Point;



/**
 * Add bars to a Graph to create a bar graph.
 */

public class BarGraph extends Graph
{
	private int desiredBarWidth = 10;  // pixels
	public static final int MinBarWidth = 3;  // pixels
	private Color barColor = GraphDefaultValues.DefaultBarColor;
	
	
	public void draw(Graphics g)
	throws
		InvalidGraph
	{
		// Draw the axes and the points.
		
		super.draw(g);
		
		
		// Determine the smallest X-coordinate spacing between all points.
		// This is used to determine the maximum possible bar width.
		
		double[] xValues = getXValues();
		double[] yValues = getYValues();

		double smallestSpacing = 0.0;  // smallest X-coordinate spacing between points.
		double spacingToPriorPoint = xValues[0] - getXStartLogical();
		
		for (int i = 1; i < xValues.length; i++)
		{
			spacingToPriorPoint = Math.min(
				spacingToPriorPoint,
				Math.abs(xValues[i] - xValues[i-1]));
		}
		
		smallestSpacing = spacingToPriorPoint;
		
		
		// Determine the width of the bars to draw.
		
		int barWidth;
		try { barWidth = translateXLength(smallestSpacing); }
		catch (OverflowException ex)
		{
			ex.printStackTrace();
			throw new InvalidGraph(ex);
		}
		
		if (desiredBarWidth != 0)
		{
			barWidth = Math.min(desiredBarWidth, barWidth);
		}
		
		barWidth = Math.max(MinBarWidth, barWidth);
		
		
		// Draw bars.
		
		int halfBarWidth = barWidth/2;
		//g.setDrawMode();
		g.setColor(barColor);
		
		for (int i = 0; i < xValues.length; i++)
		{
			try
			{
				double xLogical = xValues[i];
				double yLogical = yValues[i];
				
				//System.out.println("Logical: " + xLogical + ", " + yLogical);

				
				// Draw a rectangle from the X axis to the point Y coordinate.
				// The width should be the min of desiredBarWidth and the
				// smallest X-coordinate spacing between points.
				
				int xDisplay = translateXToDisplay(xLogical);
				int yDisplay = translateYToDisplay(yLogical);
				
				//System.out.println("Display: " + xDisplay + ", " + yDisplay);
				
				
				g.fillRect(xDisplay - halfBarWidth, yDisplay, barWidth, 
					translateYLength(yLogical));
			}
			catch (OverflowException ex) { ex.printStackTrace(); }
		}
	}
	
	
	/** Set the preferred width for the bars in the graph. The actual bar used
		might be smaller if the desired width is too large to accommodate the
		X spacing between values. If 0 is specified, then the desired width
		is ignored. */
	public int getDesiredBarWidth() { return desiredBarWidth; }
	public void setDesiredBarWidth(int w) { this.desiredBarWidth = w; }
	
	/** Color to use when drawing bars on the graph. */
	public Color getBarColor() { return barColor; }
	public void setBarColor(Color c) { this.barColor = c; }
}

