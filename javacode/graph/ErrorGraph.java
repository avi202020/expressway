/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package graph;


import java.awt.Graphics;
import java.awt.Color;
import geometry.Point;



/**
 * Add error bars to a Graph.
 */

public class ErrorGraph extends DelegatingGraph
{
	/** The upper value. This is a delta from the yValue. It must be positive. */
	private double[] upperErrors;
	
	/** The lower value. This is a delta from the yValue. It must be positive. */
	private double[] lowerErrors;
	
	/** The length of the small notches at the ends of an error bar. */
	private int errorBarNotchLength = GraphDefaultValue.DefaultErrorBarNotchLength;
	
	/** The Color to use for drawing error bars. */
	private Color errorBarColor = GraphDefaultValue.DefaultErrorBarColor;
	
	
	public ErrorGraph(Graph graph)
	{
		super(graph);
	}
	
	
	public void setUpperErrors(double[] errors) { this.upperErrors = errors; }
	public double[] getUpperErrors() { return this.upperErrors; }
	
	public void setLowerErrors(double[] errors) { this.lowerErrors = errors; }
	public double[] getLowerErrors() { return this.lowerErrors; }
	
	public Color getErrorBarColor() { return errorBarColor; }
	public void setErrorBarColor(Color c) { this.errorBarColor = c; }
	
		
	public void draw(Graphics g)
	{
		graph.draw(g);
		
		
		double[] xValues = getXValues();
		double[] yValues = getYValues();
		
		
		// Draw error bars.
		
		g.setDrawMode();
		g.setColor(errorBarColor);
		int halfErrorBarNotchLength = errorBarNotchLength/2;
		
		for (int i = 0; i < xValues.length; i++)
		{
			double xLogical = xValues[i];
			double yLogical = yValues[i];
			int xDisplay = translateXToDisplay(xLogical);
			
			int yUpperDisplay = translateYToDisplay(yLogical + upperErrors[i]);
			int yLowerDisplay = translateYToDisplay(yLogical - lowerErrors[i]);
			
			// Draw vertical line.
			g.drawLine(xDisplay, yLowerDisplay, xDisplay, yUpperDisplay);
			
			// Draw notches at the end of the vertial line.
			g.drawLine(xDisplay - halfErrorBarNotchLength, yLowerDisplay,
				xDisplay + halfErrorBarNotchLength, yLowerDisplay);
			g.drawLine(xDisplay - halfErrorBarNotchLength, yUpperDisplay,
				xDisplay + halfErrorBarNotchLength, yUpperDisplay);
		}
	}
	
	
	protected void verifyLogical()
	throws
		InvalidGraph
	{
		super.verifyLogical();
		
		if (upperErrors.size() != lowerErrors.size())
			throw new InvalidGraph(
				"Number of upper errors does not match number of lower errors.");
				
		if (points == null)
			if (upperErrors.length != xValues.length)
				throw new InvalidGraph(
					"Number of errors does not match number of data values");
			else if (upperErrors.size() != points.size())
				throw new InvalidGraph(
					"Number of errors does not match numver of data points");
	}
}

