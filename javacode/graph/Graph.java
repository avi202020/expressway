/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package graph;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.util.List;
import java.text.NumberFormat;
import java.awt.FontMetrics;
import geometry.Point;
import statistics.HistogramDomainParameters;
import statistics.TimeSeriesParameters;



/**
 * The intended use is to create an instance of this class whenever a graph needs
 * to be drawn on a Graphics. It is not intended that Graph be a container for
 * a graph that was drawn or that it be used as a Component.
 *
 * The data to be graphed can be provided either as a List of Points, or as arrays
 * of X and Y values.
 */

public class Graph implements Cloneable
{
	private static final Font TitleFont = new Font("Arial", Font.BOLD, 18);
	private static final Color TitleColor = Color.blue;
	private static final FontMetrics TitleFontMetrics = 
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(TitleFont);
		
	private static final Font LabelFont = new Font("Arial", Font.PLAIN, 12);
	private static final Color LabelTextColor = Color.black;
	private static final FontMetrics LabelFontMetrics =
		java.awt.Toolkit.getDefaultToolkit().getFontMetrics(LabelFont);
	
	
	/*
	 * Logical graph attributes.
	 */
	 
	private String title;
	private String xAxisLabel;
	private String yAxisLabel;
	
	/** Scale factor for the numbers that label the X axis divisions. Only the
		labeling is affected: the curve it not. */
	private double xScaleFactor = 1.0;
	
	/** Scale factor for the numbers that label the Y axis divisions. Only the
		labeling is affected: the curve it not. */
	private double yScaleFactor = 1.0;
	
	/** The Color to use when drawing the X and Y axes and their labels. */
	private Color axisColor = GraphDefaultValues.DefaultAxisColor;
	
	/** The Color to use when drawing a Point on a graph. */
	private Color pointColor = GraphDefaultValues.DefaultPointColor;
	
	/** The data to be graphed, represented as a List. If this is non-null, then
		the 'xValues' and 'yValues' variables will be ignored. */
	private List<Point> points;
	
	/** If true, draw points of the graph. Otherwise do not. */
	private boolean drawPoints = true;
	
	/** The X values of the data to be graphed, as an array. If this form is used,
		then the 'points' variable should be null. */
	private double[] xValues;
	
	/** The Y values of the data to be graphed, as an array. If this form is used,
		then the 'points' variable should be null. */
	private double[] yValues;
	
	/** Logical starting point of the X axis. This should be the least positive
		value that should be displayable along the X axis that is to be drawn.
		The first notch along the X axis will be at this point.
		"Logical" means in the space of the abstract model that is being graphed,
		not in the space of the display. */
	private double xStartLogical = GraphDefaultValues.DefaultXStartLogical;
	
	/**  */
	private double xEndLogical;
	
	/** True if a value for xEndLogical has been set. */
	private boolean xEndLogicalSpecified = false;
	
	/** The spacing between notches of the graph along the X axis, in the logical
		space. */
	private double xDeltaLogical;
	
	/** Logical starting point of the Y axis. This should be the smallest value
		that should be displayable along the Y axis that is to be drawn. */
	private double yStartLogical = GraphDefaultValues.DefaultYStartLogical;
	
	/**  */
	private double yEndLogical;
	
	/** True if a value for yEndLogical has been set. */
	private boolean yEndLogicalSpecified = false;
	
	/** The spacing between notches of the graph along the Y axis, in the logical
		space. */
	private double yDeltaLogical;
	
	
	/*
	 * Display-dependent physical attributes.
	 */
	
	/**  */
	private double pixelXSize = GraphDefaultValues.DefaultPixelXSize;
	
	/**  */
	private double pixelYSize = GraphDefaultValues.DefaultPixelYSize;
	
	/** The maximum length available for drawing the X axis on the display. */
	private int maxXAxisLength = GraphDefaultValues.DefaultMaxXAxisLength;

	/** The maximum length available for drawing the Y axis on the display. */
	private int maxYAxisLength = GraphDefaultValues.DefaultMaxYAxisLength;
	
	/** The location in display space of the X axis starting point. The first notch
		along the X axis will be at this point. */
	private int xStartDisplay = GraphDefaultValues.DefaultXStartDisplay;
	
	/**  */
	private int yStartDisplay = GraphDefaultValues.DefaultYStartDisplay;

	/**  */
	private int xAxisNotchLength = GraphDefaultValues.DefaultXAxisNotchLength;
	
	/**  */
	private int yAxisNotchLength = GraphDefaultValues.DefaultYAxisNotchLength;
	
	/** The size of a Point to be drawn on a graph, along the X axis. */
	private int pointRectXLength = GraphDefaultValues.DefaultPointRectXLength;

	/** The size of a Point to be drawn on a graph, along the Y axis. */
	private int pointRectYLength = GraphDefaultValues.DefaultPointRectYLength;
	
	/** If not null, summary statistics (mean, sd) are printed under the title. */
	private double[] summaryStatistics = null;
	
	
	public Graph()
	{
	}
	
	
	/**
	 * Create a new instance of this Graph, but without the data to be graphed.
	 */
	 
	public Graph copyConfiguration()
	{
		Object o;
		try { o = this.clone(); }
		catch (CloneNotSupportedException ex) { throw new RuntimeException(ex); }
		
		Graph graph = (Graph)o;
		
		graph.setPoints(null);
		graph.setXValues(null);
		graph.setYValues(null);
		
		return graph;
	}
	
	
	public void copyDimensionsFrom(Graph otherGraph)
	{
		this.setPixelXSize(otherGraph.getPixelXSize());
		this.setPixelYSize(otherGraph.getPixelYSize());
		this.setXStartLogical(otherGraph.getXStartLogical());
		this.setYStartLogical(otherGraph.getYStartLogical());
		this.setXEndLogical(otherGraph.getXEndLogical());
		this.setYEndLogical(otherGraph.getYEndLogical());
		this.setXDeltaLogical(otherGraph.getXDeltaLogical());
		this.setYDeltaLogical(otherGraph.getYDeltaLogical());

		this.setMaxXAxisLength(otherGraph.getMaxXAxisLength());
		this.setMaxYAxisLength(otherGraph.getMaxYAxisLength());
		this.setXStartDisplay(otherGraph.getXStartDisplay());
		this.setYStartDisplay(otherGraph.getYStartDisplay());
	}
	
	
	static final double PiOver2 = Math.PI/2.0;
	
	/**
	 * Draw the graph on the specified Graphics context. This is the method that
	 * should be called by clients of this class to actually render a graph once
	 * the graph has been defined. Normally this method would be called by the
	 * paintComponent method of a Component.
	 */
	 
	public void draw(Graphics g)
	throws
		InvalidGraph
	{
		InvalidGraph invalidGraphException = null;
		
		try { verifyLogical(); }
		catch (InvalidGraph ig)
		{
			// Save exception; try to draw graph anyway, and re-throw afterwards.
			invalidGraphException = ig;
		}
		
		
		// Compute domain and range.
		
		int xAxisEndDisplay;  // location in display space of the end of the X axis.
			
		try { xAxisEndDisplay = translateXToDisplay(getXEndLogical()); }
		catch (OverflowException oe)
		{
			throw new InvalidGraph(oe);
		}
		
		int yAxisEndDisplay;  // location in display space of the end of the Y axis.
			
		try
		{
			//yAxisEndDisplay = translateYToDisplay(getYEndLogical());
			yAxisEndDisplay = 
				getYStartDisplay() - 
				Math.min(maxYAxisLength,
					translateYLength(getYEndLogical() - getYStartLogical()));
		}
		//try { yAxisEndDisplay = getYEndDisplay(); }
		catch (OverflowException oe)
		{
			throw new InvalidGraph(oe);
		}
		
		
		// Draw X axis.
		
		g.setFont(LabelFont);
		g.setColor(axisColor);
		g.setPaintMode();
		g.drawLine(getXStartDisplay(), getYStartDisplay(), xAxisEndDisplay, getYStartDisplay());
			
		int xNotchDisplay;
		int yNotchDisplay;
		
		NumberFormat numberFormat = NumberFormat.getInstance();
	
		numberFormat.setMaximumFractionDigits(2);
		double xLogical = getXStartLogical();
		yNotchDisplay = yStartDisplay;
		if (xDeltaLogical <= 0.0) throw new InvalidGraph(
			"Delta must be positive: is " + xDeltaLogical);
		
		int yMax = 0;  // the highest numerical value of y reached by the X value labels.
		for (;;)  // each notch
		{
			if (xLogical > getXEndLogical()) break;
			
			try { xNotchDisplay = translateXToDisplay(xLogical); }
			catch (OverflowException oe) { throw new RuntimeException(oe); }
			
			if (xNotchDisplay > xAxisEndDisplay) break;
			
			g.setColor(axisColor);
			g.drawLine(xNotchDisplay, yNotchDisplay, 
				xNotchDisplay, yNotchDisplay + getYAxisNotchLength());
				
			// Draw value text.
			
			g.setColor(LabelTextColor);
					
			String stringValue = numberFormat.format(xLogical * xScaleFactor);
			
			char[] charValue = stringValue.toCharArray();
			int stringWidth = LabelFontMetrics.charsWidth(charValue, 0, stringValue.length());
			
			if (stringWidth - 10 > xAxisNotchLength) // values won't fit.
			{
				if (g instanceof Graphics2D)  // draw the text vertically.
				{
					((Graphics2D)g).rotate(-PiOver2, xNotchDisplay, yNotchDisplay);
					
					g.drawString(stringValue, xNotchDisplay - stringWidth - 10, 
						yNotchDisplay);
					
					((Graphics2D)g).rotate(PiOver2, xNotchDisplay, yNotchDisplay);
					
					yMax = yNotchDisplay + stringWidth;
				}
				else  // can't draw vertically: draw in a stepwise manner.
				{
					int dx = LabelFontMetrics.charWidth('0');
					int dy = LabelFontMetrics.getHeight();
					int x = xNotchDisplay;
					int y = yNotchDisplay + dy;
					for (int offset = 0; offset < charValue.length; offset++)
					{
						char c = charValue[offset];
						g.drawChars(charValue, offset, 1, x, y);
						yMax = Math.max(y, yMax);
						
						if (offset+1 < charValue.length)
						{
							char nextChar = charValue[offset+1];
							
							if ((nextChar == '.') || (nextChar == ','))
							{
								x += dx;
							}
							else
							{
								if ((c != '.') && (c != ',')) x += (dx-2);
								y += (dy-2);
							}
						}
						else
						{
							if ((c != '.') && (c != ',')) x += (dx-2);
							y += (dy-2);
						}
					}
				}
			}
			else
			{
				int charPosY = yNotchDisplay + 20;
				g.drawString(stringValue, xNotchDisplay, charPosY);
				yMax = Math.max(charPosY, yMax);
			}
			
			xLogical += getXDeltaLogical();
		}
		
		if (xAxisLabel != null)  // Draw X-axis label.
		{
			g.setColor(LabelTextColor);
					
			int labelLength = LabelFontMetrics.stringWidth(xAxisLabel);
			g.drawString(xAxisLabel, (xStartDisplay + xAxisEndDisplay - labelLength)/2, 
				yMax + LabelFontMetrics.getHeight()+10);
		}
		
		 
		// Draw Y axis.
		
		g.setColor(axisColor);			
		g.drawLine(getXStartDisplay(), getYStartDisplay(), getXStartDisplay(), yAxisEndDisplay);
		
		numberFormat.setMaximumFractionDigits(2);
		double yLogical = getYStartLogical();
		xNotchDisplay = getXStartDisplay();
		for (;;)
		{
			if (yLogical > getYEndLogical()) break;
			
			try { yNotchDisplay = translateYToDisplay(yLogical); }
			catch (OverflowException oe) { throw new RuntimeException(oe); }

			if (yNotchDisplay < yAxisEndDisplay) break;
			
			g.setColor(axisColor);
			g.drawLine(xNotchDisplay, yNotchDisplay, 
				xNotchDisplay - getYAxisNotchLength(), yNotchDisplay);
			
			// Draw value text.
			
			g.setColor(LabelTextColor);
					
			//String stringValue = numberFormat.format(yLogical);
			String stringValue = numberFormat.format(yLogical * yScaleFactor);
			//System.out.println("Drawing " + stringValue);

			int stringWidth = LabelFontMetrics.charsWidth(stringValue.toCharArray(),
				0, stringValue.length());
			
			g.drawString(stringValue, getXStartDisplay() - stringWidth - 10, yNotchDisplay);

			yLogical += getYDeltaLogical();
		}
		
		if (yAxisLabel != null)  // Draw Y axis label.
		{
			g.setColor(LabelTextColor);
					
			if (g instanceof Graphics2D) // draw String from bottom to top.
			{
				Graphics2D g2D = (Graphics2D)g;
				
				int labelLength = LabelFontMetrics.stringWidth(yAxisLabel);
				int startingY = yAxisEndDisplay + (yStartDisplay - yAxisEndDisplay)/2 + labelLength/2;
				//int startingY = (yStartDisplay + yAxisEndDisplay - labelLength)/2;
				
				int x = LabelFontMetrics.getHeight()+2;
				g2D.rotate(-PiOver2, x, startingY);
				g.drawString(yAxisLabel, 5, startingY);
				
				g2D.rotate(PiOver2, x, startingY);
			}
			else // draw characters one by one from top to bottom.
			{
				int labelHeight = LabelFontMetrics.stringWidth(yAxisLabel);
					  // an approximation that assumes that font width ~ font height.
					  
				int startingY = (yStartDisplay + yAxisEndDisplay + labelHeight)/2;
				
				int noOfChars = yAxisLabel.length();
				int x = 5;
				int y = startingY;
				int dy = LabelFontMetrics.getHeight();
					
				for (int i = 0; i < noOfChars; i++)
				{
					g.drawString(yAxisLabel.substring(i, i+1), x, y);
					y += dy;
				}
			}
		}
		
		
		// Populate xValues and yValues if necessary.
		
		if (getPoints() != null)
		{
			int noOfPoints = getPoints().size();
			setXValues(new double[noOfPoints]);
			setYValues(new double[noOfPoints]);
			
			for (int i = 0; i < noOfPoints; i++)
			{
				Point p = points.get(i);
				getXValues()[i] = p.getX();
				getYValues()[i] = p.getY();
			}
		}
		
		
		// Draw points.
		
		if (drawPoints)
		{
			int pointRectOffsetX = getPointRectXLength() / 2;
			int pointRectOffsetY = getPointRectYLength() / 2;
			
			g.setColor(pointColor);
		
			for (int i = 0; i < getXValues().length; i++)
			{
				double xValueLogical = getXValues()[i];
				double yValueLogical = getYValues()[i];
				
				int xValueDisplay;
				try { xValueDisplay = translateXToDisplay(xValueLogical); }
				catch (OverflowException oe)
				{
					System.out.println("Overflow");
					continue;  // skip the value.
				}
				
				int yValueDisplay;
				try { yValueDisplay = translateYToDisplay(yValueLogical); }
				catch (OverflowException oe)
				{
					System.out.println("Overflow");
					continue;  // skip the value.
				}
				
				int xRect = xValueDisplay - pointRectOffsetX;
				int yRect = yValueDisplay - pointRectOffsetY;
				
				g.drawRect(xRect, yRect, getPointRectXLength(), getPointRectYLength());
			}
		}
		
		
		int noOfTitleLines = 0;
		
		if (title != null)  // Draw graph title.
		{
			g.setFont(TitleFont);
			g.setColor(TitleColor);
					
			int titleWidth = TitleFontMetrics.stringWidth(title);
			int spaceWidth = TitleFontMetrics.stringWidth(" ");
			//int titleHeight = TitleFontMetrics.getMaxAscent() + TitleFontMetrics.getMaxDescent();
			
			noOfTitleLines = 1;
			int[] lineWidth = { 0, 0 };
			String[] lines = { "", "" };
			if (titleWidth > this.maxXAxisLength)  // need to wrap title
			{
				String[] titleParts = title.split(" ");
				
				int[] titlePartWidths = new int[titleParts.length];
				int i = 0;
				for (String titlePart : titleParts)
				{
					if (i == titleParts.length-2)  // next to last part
						break;
					
					if (i > 0)
					{
						lineWidth[0] += spaceWidth;  // inter-word spacing
						lines[0] += " ";
					}

					int titlePartWidth = TitleFontMetrics.stringWidth(titlePart);
					lines[0] += titleParts[i];
					titlePartWidths[i] = titlePartWidth;
					lineWidth[0] += titlePartWidth;
					
					i++;
					
					if (lineWidth[0] > (titleWidth * 10) / 6)  // > 60%
						break;
				}
				
				if (i < titleParts.length)
				{
					noOfTitleLines++;
					
					for (int j = i; j < titleParts.length; j++)
					{
						String titlePart = titleParts[j];
						int titlePartWidth = TitleFontMetrics.stringWidth(titlePart);
						
						if (j > i)
						{
							lineWidth[1] += spaceWidth;  // inter-word spacing
							lines[1] += " ";
						}
						
						lines[1] += titleParts[j];
						lineWidth[1] += titlePartWidth;
					}
				}
			}
			else
			{
				lines[0] = title;
				lineWidth[0] = TitleFontMetrics.stringWidth(title);
			}
			
			for (int lineNo = 0; lineNo < noOfTitleLines; lineNo++)
			{
				int titleX = xStartDisplay + (maxXAxisLength - lineWidth[lineNo]) / 2;
				int titleY = 20 + lineNo * TitleFontMetrics.getHeight();
				
				g.drawString(lines[lineNo], titleX, titleY);
			}
		}
			
			
		if (summaryStatistics != null)
		{
			double mean = summaryStatistics[0];
			double sd = summaryStatistics[1];
			
			String summaryString = "mean=" + numberFormat.format(mean) + 
				", \u03c3=" + numberFormat.format(sd);
			
			int summaryWidth = LabelFontMetrics.stringWidth(summaryString);
			
			int summaryX = xStartDisplay + (maxXAxisLength - summaryWidth) / 2;
			int summaryY = 20 + noOfTitleLines * TitleFontMetrics.getHeight();
			
			g.setFont(LabelFont);
			g.setColor(LabelTextColor);
			g.drawString(summaryString, summaryX, summaryY);
		}


		// Verify that graph is valid.
		
		if (invalidGraphException != null) throw invalidGraphException;
	}

	
	/*
	 * Public accessors.
	 */

	public String getTitle() { return title; }
	public void setTitle(String title) { this.title = title; }
	
	public String getXAxisLabel() { return this.xAxisLabel; }
	public void setXAxisLabel(String s) { this.xAxisLabel = s; }
	
	public String getYAxisLabel() { return this.yAxisLabel; }
	public void setYAxisLabel(String s) { this.yAxisLabel = s; }
	
	public double getXScaleFactor() { return xScaleFactor; }
	public void setXScaleFactor(double f) { this.xScaleFactor = f; }

	public double getYScaleFactor() { return yScaleFactor; }
	public void setYScaleFactor(double f) { this.yScaleFactor = f; }

	public Color getAxisColor() { return this.axisColor; }
	public void setAxisColor(Color c) { this.axisColor = c; }
	
	public Color getPointColor() { return this.pointColor; }
	public void setPointColor(Color c) { this.pointColor = c; }
	
	public List<Point> getPoints() { return this.points; }
	public void setPoints(List<Point> points) { this.points = points; }
	
	public boolean getDrawPoints() { return drawPoints; }
	public void setDrawPoints(boolean drawPoints) { this.drawPoints = drawPoints; }
	
	public double[] getXValues() { return this.xValues; }
	public void setXValues(double[] xValues) { this.xValues = xValues; }
	
	public double[] getYValues() { return this.yValues; }
	public void setYValues(double[] yValues) { this.yValues = yValues; }
	
	public double getXStartLogical() { return this.xStartLogical; }
	public void setXStartLogical(double x) { this.xStartLogical = x; }
	
	public double getXEndLogical() { return this.xEndLogical; }
	public void setXEndLogical(double x) { this.xEndLogical = x; this.setXEndLogicalSpecified(true); }
	
	public boolean getXEndLogicalSpecified() { return this.xEndLogicalSpecified; }
	public void setXEndLogicalSpecified(boolean y) { this.xEndLogicalSpecified = y; }
	
	public double getXDeltaLogical() { return this.xDeltaLogical; }
	public void setXDeltaLogical(double d) { this.xDeltaLogical = d; }
	
	public double getYStartLogical() { return this.yStartLogical; }
	public void setYStartLogical(double y) { this.yStartLogical = y; }
	
	public double getYEndLogical() { return this.yEndLogical; }
	public void setYEndLogical(double y) { this.yEndLogical = y; this.setYEndLogicalSpecified(true); }
	
	public boolean getYEndLogicalSpecified() { return this.yEndLogicalSpecified; }
	public void setYEndLogicalSpecified(boolean y) { this.yEndLogicalSpecified = y; }
	
	public double getYDeltaLogical() { return this.yDeltaLogical; }
	public void setYDeltaLogical(double d) { this.yDeltaLogical = d; }
	
	public double getPixelXSize() { return this.pixelXSize; }
	public void setPixelXSize(double psize) { this.pixelXSize = psize; }
	
	public double getPixelYSize() { return this.pixelYSize; }
	public void setPixelYSize(double psize) { this.pixelYSize = psize; }
	
	public int getMaxXAxisLength() { return this.maxXAxisLength; }
	public void setMaxXAxisLength(int max) { this.maxXAxisLength = max; }

	public int getMaxYAxisLength() { return this.maxYAxisLength; }
	public void setMaxYAxisLength(int max) { this.maxYAxisLength = max; }
	
	public int getXStartDisplay() { return this.xStartDisplay; }
	public void setXStartDisplay(int xStart) { this.xStartDisplay = xStart; }
	
	public int getYStartDisplay() { return this.yStartDisplay; }
	public void setYStartDisplay(int yStart) { this.yStartDisplay = yStart; }
	
	public int getYEndDisplay()
	throws
		OverflowException { return translateYToDisplay(getYEndLogical()); }

	public int getXAxisNotchLength() { return this.xAxisNotchLength; }
	public void setXAxisNotchLength(int notchLength) { this.xAxisNotchLength = notchLength; }
	
	public int getYAxisNotchLength() { return this.yAxisNotchLength; }
	public void setYAxisNotchLength(int notchLength) { this.yAxisNotchLength = notchLength; }
	
	public int getPointRectXLength() { return this.pointRectXLength; }
	public void setPointRectXLength(int rectSize) { this.pointRectXLength = rectSize; }

	public int getPointRectYLength() { return this.pointRectYLength; }
	public void setPointRectYLength(int rectSize) { this.pointRectYLength = rectSize; }
	
	public double[] getSummaryStatistics() { return summaryStatistics; }
	public void setSummaryStatistics(double[] stats) { this.summaryStatistics = stats; }
	



	/*
	 * Protected methods.
	 */
	
	
	/**
	 * Translate the specified coordinate value, as specified in the logical model
	 * space, into the display space. (Note: I really want this to be protected,
	 * with friend access to GraphPanel.)
	 */
	 
	public int translateXToDisplay(double xLogical)
	throws
		OverflowException
	{
		return getXStartDisplay() + translateXLength(xLogical - getXStartLogical());
	}
	
	
	/**
	 * Translate the specified coordinate value, as specified in the logical model
	 * space, into the display space. (Note: I really want this to be protected,
	 * with friend access to GraphPanel.)
	 */
	 
	public int translateYToDisplay(double yLogical)
	throws
		OverflowException
	{
		return getYStartDisplay() - 
			translateYLength(yLogical - getYStartLogical());
			//Math.min(maxYAxisLength, translateYLength(yLogical - getYStartLogical()));
	}
	
	
	/**
	 * Transform the specified length, as specified in the logical model space,
	 * into the display space. The length is along the X axis.
	 */
	 
	protected int translateXLength(double lengthLogical)
	throws
		OverflowException
	{
		if (pixelXSize == 0.0) throw new OverflowException(
			"Pixel X size is zero");
		
		long result = Math.round(lengthLogical / getPixelXSize());
		if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE)
			throw new OverflowException();
			
		return (int)result;
	}
	
	
	/**
	 * Transform the specified length, as specified in the logical model space,
	 * into the display space. The length is along the Y axis.
	 */
	 
	protected int translateYLength(double lengthLogical)
	throws
		OverflowException
	{
		if (getPixelYSize() == 0.0) throw new OverflowException(
			"Pixel Y size is zero");
		
		long result = Math.round(lengthLogical / getPixelYSize());
		if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE)
				throw new OverflowException();
			
		return (int)result;
	}
	
	
	public double translateDisplayXToLogical(int x)
	{
		return xStartLogical + translateDisplayWidthToLogical(x - xStartDisplay);
	}
	
	
	public double translateDisplayYToLogical(int y)
	{
		return yStartLogical - translateDisplayHeightToLogical(y - yStartDisplay);
	}
	
	
	public double translateDisplayWidthToLogical(int w)
	{
		return getPixelXSize() * w;
	}
	
	
	public double translateDisplayHeightToLogical(int h)
	{
		return getPixelYSize() * h;
	}
	
	
	/**
	 * Verify that the graph can be drawn, based on logical analysis but not based
	 * on display characteristics. E.g., verify that there are enough data points,
	 * and that all required logical parameters are set or have default values.
	 */
	 
	protected void verifyLogical()
	throws
		InvalidGraph
	{
		if (getXValues() == null) throw new InvalidGraph("Null x values");
		if (getYValues() == null) throw new InvalidGraph("Null y values");
		
		if (getXValues().length == 0) throw new InvalidGraph("No x values");
		if (getYValues().length == 0) throw new InvalidGraph("No y values");
		
		if (points == null)
			if (getXValues().length != getYValues().length)
				throw new InvalidGraph(
					"Number of x values differs from number of y values");
			else if (getXValues().length == 0)
				throw new InvalidGraph("Zero size value set");

		if (getXAxisLabel() == null)
			throw new InvalidGraph("null x axis label");
			
		if (getYAxisLabel() == null)
			throw new InvalidGraph("null y axis label");

		if (! getXEndLogicalSpecified())
			throw new InvalidGraph("No x endpoint specified");
	
		if (getXDeltaLogical() == 0.0)
			throw new InvalidGraph("Zero increment between x values");
	
		if (! getYEndLogicalSpecified())
			throw new InvalidGraph("No y endpoint specified");
	
		if (getYDeltaLogical() == 0.0)
			throw new InvalidGraph("Zero increment between y values");
			
		if (getXStartLogical() > getXEndLogical())
			throw new InvalidGraph("X axis defined to run backwards: " +
				getXStartLogical() + " to " + getXEndLogical());
			
		if (getYStartLogical() > getYEndLogical())
			throw new InvalidGraph("Y axis defined to run backwards: " + 
				getYStartLogical() + " to " + getYEndLogical());
	}


	public static class InvalidGraph extends Exception
	{
		public InvalidGraph() { super("Invalid graph"); }
		public InvalidGraph(String msg) { super("Invalid graph: " + msg); }
		public InvalidGraph(Throwable cause) { super(cause); }
		public InvalidGraph(String msg, Throwable cause) { super(msg, cause); }
	}
	
	
	public static class OverflowException extends Exception
	{
		public OverflowException() { super("Overflow"); }
		public OverflowException(String msg) { super("Overflow: " + msg); }
		public OverflowException(Throwable cause) { super(cause); }
		public OverflowException(String msg, Throwable cause) { super(msg, cause); }
	}
}

