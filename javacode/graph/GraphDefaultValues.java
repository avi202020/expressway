/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package graph;


import java.awt.Color;


public class GraphDefaultValues
{
	/** The Color to use when drawing the X and Y axes and their labels. */
	public static final Color DefaultAxisColor = Color.BLACK;
	
	/** The Color to use when drawing a Point on a graph. */
	public static final Color DefaultPointColor = Color.RED;
	
	/** The Color to use when drawing a graph curve. */
	public static final Color DefaultShapeColor = Color.BLACK;
	
	/** The Color to use when drawing bars in a bar graph. */
	public static final Color DefaultBarColor = Color.BLUE;
	
	/** Color to use when drawing error bars. */
	public static final Color DefaultErrorBarColor = Color.BLACK;
	
	/** Logical starting point of the X axis. This should be the least positive
		value that should be displayable along the Y axis that is to be drawn.
		The first notch along the X axix will be at this point.
		"Logical" means in the space of the abstract model that is being graphed,
		not in the space of the display. */
	public static final double DefaultXStartLogical = 0.0;
	
	/** Logical starting point of the Y axis. This should be the smallest value
		that should be displayable along the Y axis that is to be drawn. */
	public static final double DefaultYStartLogical = 0.0;
	
	
	/*
	 * Display-dependent physical attributes.
	 */
	
	/**  */
	public static final double DefaultPixelXSize = 1.0;
	
	/**  */
	public static final double DefaultPixelYSize = 1.0;
	
	/** The maximum length available for drawing the X axis on the display. */
	public static final int DefaultMaxXAxisLength = 500;

	/** The maximum length available for drawing the Y axis on the display. */
	public static final int DefaultMaxYAxisLength = 500;
	
	/** The location in display space of the X axis starting point. The first notch
		along the X axis will be at this point. */
	public static final int DefaultXStartDisplay = 50;
	
	/**  */
	public static final int DefaultYStartDisplay = 50;

	/**  */
	public static final int DefaultXAxisNotchLength = 3;
	
	/**  */
	public static final int DefaultYAxisNotchLength = 3;
	
	/** The length of the small notches at the ends of an error bar. */
	public static final int DefaultErrorBarNotchLength = 4;
	
	/** The size of a Point to be drawn on a graph, along the X axis. */
	public static final int DefaultPointRectXLength = 4;

	/** The size of a Point to be drawn on a graph, along the Y axis. */
	public static final int DefaultPointRectYLength = 4;
}

