/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package graph;


import java.awt.Graphics;
import java.awt.Color;
import java.util.List;
import geometry.Point;
import statistics.HistogramDomainParameters;
import statistics.TimeSeriesParameters;


/**
 * Container for the conversion of raw histogram data (usually an array of
 * integer counts) into a format usable by the methods of the Graph class
 * (which requires an array of doubles).
 */
 
public class GraphInput implements java.io.Serializable
{
	public double[] xValues;
	public double[] yValues;
	public double minYValue = 0.0;
	public double maxYValue = 0.0;
	
	
	/**
	 * Format the input count data, and return the results in this object.
	 * The input is in the form of an array of incident counts, characterized
	 * by the specified histogram parameters.
	 */
	 
	public void formatInput(HistogramDomainParameters histoParams, int[] counts)
	{
		xValues = new double[counts.length];
		yValues = new double[counts.length];
		double xValue = histoParams.bucketStart + (histoParams.bucketSize / 2.0);
		minYValue = 0.0;
		maxYValue = 0.0;
		
		for (int i = 0; i < counts.length; i++)
		{
			double yValue = (double)(counts[i]);
			xValues[i] = xValue;
			yValues[i] = yValue;
			
			if (i == 0) { minYValue = yValue; maxYValue = yValue; }
			else
			{
				minYValue = Math.min(minYValue, yValue);
				maxYValue = Math.max(maxYValue, yValue);
			}
			
			xValue += histoParams.bucketSize;
		}
	}
	
	
	/**
	 * Format the input time series data, and return the results in this object.
	 * The format is in the form of an array of values.
	 */
	 
	public void formatInput(TimeSeriesParameters params, double[] values)
	{
		//System.out.println("GraphInput.formatInput, input values:");
		//for (double d : values) System.out.println("\t" + d);
		
		xValues = new double[params.noOfPeriods];
		yValues = new double[params.noOfPeriods];
		
		long periodStartTime = params.startTime;
		double halfPeriodDuration = (double)params.periodDuration / 2.0;
		minYValue = 0.0;
		maxYValue = 0.0;
		
		for (int period = 0; period < params.noOfPeriods; period++)
		{
			double yValue = values[period];
			long periodEndTime = periodStartTime + params.periodDuration;
			
			xValues[period] = (double)periodStartTime + halfPeriodDuration;
			yValues[period] = yValue;
			
			if (period == 0) { minYValue = yValue; maxYValue = yValue; }
			else
			{
				minYValue = Math.min(minYValue, yValue);
				maxYValue = Math.max(maxYValue, yValue);
			}
			
			periodStartTime = periodEndTime;
		}
		//System.out.println("GraphInput.formatInput, formatted y values:");
		//for (double d : yValues) System.out.println("\t" + d);
	}
	
	
	/**
	 * Determine min and max values for the Y values.
	 */
	 
	public void formatInput(double[] xValues, double[] yValues)
	{
		this.xValues = xValues;
		this.yValues = yValues;
		
		if (yValues.length == 0) throw new RuntimeException("No Y values");
		
		this.minYValue = yValues[0];
		this.maxYValue = yValues[0];
		for (int i = 1; i < yValues.length; i++)
		{
			this.minYValue = Math.min(this.minYValue, yValues[i]);
			this.maxYValue = Math.max(this.maxYValue, yValues[i]);
		}
	}
	
	
	/**
	 * Format the input time series data, and return the results in this object.
	 * The format is in the form of an array of values. 'index' selects the array
	 * of 'valuesArray' to use.
	 */
	 
	public void formatInput(TimeSeriesParameters params, double[][] valuesArray,
		int index)
	{
		//double[] values = valuesArray[index];
		
		double[] values = new double[valuesArray.length];
		int t = 0;
		for (double[] dAr : valuesArray) values[t++] = dAr[index];
		
		formatInput(params, values);
	}
	
	
	/**
	 * The input is an array of arrays: sort the indexToSortOn'th array (in
	 * ascending order), and move the elements of each other array as the
	 * elements of the indexToSortOn'th array are moved: that is, maintain
	 * the relationship across the arrays. It is required that all arrays have
	 * the same length.
	 */
	 
	public static void sortValues(double[][] valueField, int indexToSortOn)
	{
		double[] sortValues = valueField[indexToSortOn];
		
		int n = valueField[0].length;
		for (int i = 1; i < n; i++)  // elements i and higher are unsorted;
										// elements 0..(i-1) are sorted.
		{
			double value = sortValues[i];  // element to be placed among
				// the sorted elements 0..(i-1); a space will be made by shifting
				// some elements to the right by 1.
				
			
			// Find where element i goes, among elements 0..(i-1)
			
			for (int below = 0; below < i; below++)
			{
				if (value < sortValues[below])  // insert value at [below]
				{
					// Shift right by one all elements to the right of [below];
					// but do this for all value arrays, not just array 'indexToSortOn'.
					
					for (int ar = 0; ar < valueField.length; ar++)
					{
						double holdValue = valueField[ar][i];
						
						for (int r = i; r >= below+1; r--)
						{
							valueField[ar][r] = valueField[ar][r-1];
						}
						
						
						// place value at [below]
	
						valueField[ar][below] = holdValue;
					}
					
					break;
				}
			}
					
		}
	}
	
	
	/**
	 * Multiply each value in the array by the specified scale factor.
	 */
	 
	public static void scaleValues(double[] values, double scaleFactor)
	{
		for (int i = 0; i < values.length; i++) values[i] *= scaleFactor;
	}
	
	
	/** for debug only. */
	public void dump()
	{
		System.out.println("minYValue = " + minYValue);
		System.out.println("maxYValue = " + maxYValue);
		int i = 0;
		for (; i < xValues.length; i++)
		{
			System.out.print("\t" + xValues[i] + ", ");
			if (yValues.length > i) System.out.print(yValues[i]);
			else System.out.print("-");
			System.out.println();
		}
		
		for (; i < yValues.length; i++)
		{
			System.out.println("-, " + yValues[i]);
		}
	}
	
	
	/* for debug only
	public static void main(String[] args)
	{
		double[][] valueField = 
		{
			{
				3099447.0148348454,
				4614373.472952144,
				3775067.1626057946
			},
			{
				4510.0,
				9960.0,
				6380.0
			}
		};
		
		
		// Expect this:
		// (3099447.0148348454, 4510.0) 
		// (3775067.1626057946, 6380.0) 
		// (4614373.472952144, 9960.0)
		
		
		sortValues(valueField, 0);
		
		double[] xValues = valueField[0];
		double[] yValues = valueField[1];
		
		for (int i = 0; i < xValues.length; i++)
		{
			double x = xValues[i];
			double y = yValues[i];
			System.out.println("\t\t(" + x + ", " + y + ") ");
		}
	}*/
}

