/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package graph;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.CubicCurve2D.Double;
import java.awt.geom.AffineTransform;
import java.text.NumberFormat;
import org.apache.commons.math.analysis.SplineInterpolator;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.analysis.PolynomialSplineFunction;
import org.apache.commons.math.FunctionEvaluationException;
import geometry.Point;

import java.awt.Shape; // for test only.


/**
 * For creating a graph of a set of points, using smooth lines for interpolation.
 *
 * The intended use is to create an instance of this class whenever a graph needs
 * to be drawn on a Graphics. It is not intended that this class be a container for
 * a graph that was drawn or that it be used as a Component.
 *
 * The data to be graphed can be provided either as a List of Points, or as arrays
 * of X and Y values.
 *
 * Slopes is optionally displayed next to each graphed point. The slope is multiplied
 * by the Y scale factor divided by the X scale factor.
 */

public class SmoothGraph extends Graph
{
	private static final int MaxFractionDigits = 2;
	
	
	/** The Color to use when drawing the graph curve. */
	private Color shapeColor = GraphDefaultValues.DefaultShapeColor;
	
	/** If true, then the slope of the curve is displayed adjacent to each Point. */
	private boolean displaySlopes = false;
	
	
	public void draw(Graphics g)
	throws
		InvalidGraph
	{
		// Draw the axes and the points.
		
		super.draw(g);
		
		if (! (g instanceof Graphics2D))
		{
			System.out.println("Platform does not support Java 2D drawing.");
			return;
		}
		
		
		// Derive a natural spline function to fit all of the graph's points.
		
		double[] xValues = getXValues();
		double[] yValues = getYValues();
		
		SplineInterpolator interpolator = new SplineInterpolator();
		UnivariateRealFunction function = interpolator.interpolate(xValues, yValues);
		
		
		// Use the natural spline to create extra points if necessary, in order
		// to ensure that we have 3n+1 points, where n the smallest whole number
		// such that 3n+1 >= the original number of points. This is because a cubic
		// spline is composed of three-point triplets, but starting from an initial
		// point.
		
		int windingRule = GeneralPath.WIND_NON_ZERO;  // set arbitrarily: not used.
		int noOfPoints = xValues.length;
		int noOfSegments = noOfPoints-1;
		GeneralPath path = new GeneralPath(windingRule, noOfSegments);
		
		// Add the starting point of the spline.
		double priorX = xValues[0];
		double priorY = yValues[0];
		
		checkForFloatRange(priorX);
		checkForFloatRange(priorY);
		
		path.moveTo((float)priorX, (float)priorY);
		
		// Add each successive triplet of points, to form a cubic B-spline.
		for (int i = 1; i < noOfPoints; i++)  // each point
		{
			double newX = xValues[i];
			double newY = yValues[i];

			checkForFloatRange(newX);
			checkForFloatRange(newY);

			double oneThird = (newX - priorX)/3.0;
			double c1x = priorX + oneThird;
			double c2x = c1x + oneThird;
			
			double c1y;
			double c2y;
			try
			{
				c1y = function.value(c1x);
				c2y = function.value(c2x);
			}
			catch (FunctionEvaluationException ex)
			{
				System.out.println("Error evaluating natural spine function: "
					+ ex.getMessage());
					
				continue;
			}
			
			// Note that the spline will not necessarily go through c1 and c2.
			// These are obtained from the interpolated natural spline anyway.
			path.curveTo((float)c1x, (float)c1y, (float)c2x, (float)c2y, 
				(float)newX, (float)newY);
			
			priorX = newX;
			priorY = newY;
		}
		
		
		//
		// Transform the path to the coordinate space of the display.
		//
		
		AffineTransform completeTransformation = new AffineTransform();
		
		
		// Adjust for logical offset.
		
		AffineTransform translationLogical = new AffineTransform();
		translationLogical.setToTranslation(
			-(double)(getXStartLogical()), -(double)(getYStartLogical()));
		
		completeTransformation.preConcatenate(translationLogical);
		
		
		// Adjust for scale from logical to display.
		
		AffineTransform scale = new AffineTransform();
		scale.setToScale(1.0/getPixelXSize(), -1.0/getPixelYSize());
		
		completeTransformation.preConcatenate(scale);
		
		
		// Adjust for translation of display from edge of Component.
		
		AffineTransform translationDisplay = new AffineTransform();
		translationDisplay.setToTranslation(
			(double)(getXStartDisplay()), (double)(getYStartDisplay()));
			
		completeTransformation.preConcatenate(translationDisplay);
		
		//System.out.println("Path before transformation:");
		//dumpShape(path);
		
		
		// Perform complete transformation on the path.

		path.transform(completeTransformation);
		
		
		// Draw the path.
		
		g.setColor(shapeColor);
		g.setPaintMode();
		((Graphics2D)g).draw(path);
		
		
		// Draw slope values.
		
		if (displaySlopes) try
		{
			NumberFormat numberFormat = NumberFormat.getInstance();
	
			numberFormat.setMaximumFractionDigits(MaxFractionDigits);
			
			Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 8);
			g.setFont(font);
			
			PolynomialSplineFunction polyFunction = (PolynomialSplineFunction)function;
			
			UnivariateRealFunction derivative = polyFunction.derivative();
			
			
			double[] coords = new double[6];
			PathIterator iterator = path.getPathIterator(null);
			for (int segNo = 0;; segNo++)
			{
				if (iterator.isDone()) break;
				
				int segKind = iterator.currentSegment(coords);
				iterator.next();
				
				switch (segKind)
				{
					case PathIterator.SEG_MOVETO:  continue;
					
					case PathIterator.SEG_LINETO:
						throw new RuntimeException("Unexpected segment kind: SEG_LINETO");
					
					case PathIterator.SEG_QUADTO:
						throw new RuntimeException("Unexpected segment kind: SEG_QUADTO");
						
					case PathIterator.SEG_CUBICTO: break;
						
					case PathIterator.SEG_CLOSE:
						throw new RuntimeException("Unexpected segment kind: SEG_CLOSE");
						
					default:
						throw new RuntimeException("Segment kind is unrecognized");
				}
				
				double x = xValues[segNo];
				double m = derivative.value(x) * getYScaleFactor() / getXScaleFactor();
				
				String formattedValue = numberFormat.format(m);
				
				int displayX = (int)(coords[4]);
				int displayY = (int)(coords[5]);
				
				//g.drawString("slope=" + m, displayX+5, displayY+4);
				g.drawString("slope=" + formattedValue, displayX+5, displayY+4);
			}
		}
		catch (FunctionEvaluationException ex)
		{
			throw new InvalidGraph(ex);
		}
		
		
		//System.out.println("Path that was drawn:");
		//dumpShape(path);
	}
	
	
	public Color getShapeColor() { return shapeColor; }
	public void setShapeColor(Color c) { this.shapeColor = c; }
	
	
	public void setDisplaySlopes(boolean displaySlopes)
	{
		this.displaySlopes = displaySlopes;
	}
	
	public boolean getDisplaySlopes() { return displaySlopes; }
	
	
	private void checkForFloatRange(double x)
	throws
		InvalidGraph
	{
		if (x >= Float.POSITIVE_INFINITY)
			throw new InvalidGraph("Max Float value exceeded: " + x + " > " + Float.POSITIVE_INFINITY);
		
		if (x <= Float.NEGATIVE_INFINITY)
			throw new InvalidGraph("Min Float value exceeded: " + x + " < " + Float.NEGATIVE_INFINITY);
	}
	
	
	/** For test only. */
	private static void dumpShape(Shape shape)
	{
		double[] coords = new double[6];
		java.awt.geom.PathIterator it = shape.getPathIterator(null);
		for (;;)
		{
			int segkind = it.currentSegment(coords);
			switch (segkind)
			{
			case java.awt.geom.PathIterator.SEG_MOVETO:
				System.out.println("x=" + coords[0] + ", y=" + coords[1]);
				break;
			case java.awt.geom.PathIterator.SEG_CUBICTO:
				System.out.println("x1=" + coords[0] + ", y1=" + coords[1]);
				System.out.println("x2=" + coords[2] + ", y2=" + coords[3]);
				System.out.println("x3=" + coords[4] + ", y3=" + coords[5]);
				break;
			default:
				throw new RuntimeException("Unexpected segment kind");
			}
			
			it.next();
			if (it.isDone()) break;
		}
	}
}

