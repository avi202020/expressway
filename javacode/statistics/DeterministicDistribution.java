/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package statistics;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;


public class DeterministicDistribution implements ContinuousDistribution
{
	private double value;
	
	
	public DeterministicDistribution(double value)
	{
		this.value = value;
	}
	
	
	public double cumulativeProbability(double x)
	throws MathException
	{
		if (x < value) return 0; else return 1;
	}
	
	
	public double cumulativeProbability(double x0, double x1)
	throws MathException
	{
		if ((x0 <= value) && (value <= x1)) return 1; else return 0;
	}
	
	
	public double inverseCumulativeProbability(double p)
	throws MathException
	{
		return value;
	}
}

