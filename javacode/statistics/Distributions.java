/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package statistics;


import java.util.*;


public class Distributions
{
	private static Distribution.DistributionKind kind;
		
		
	public interface Distribution
	{
		enum DistributionKind { Apache, ORO };
		
		double getPD();
	}
	
	
	public interface Gamma extends Distribution
	{
	}
	
	
	public static class DistributionFactory
	{
		public static void setDistributionKind(Distribution.DistributionKind kind)
		{
			Distributions.kind = kind;
		}
		
		
		public static Gamma createGammaDistribution(double alpha, double beta)
		{
			if (kind == Distribution.DistributionKind.Apache)
			{
				return new GammaApache(alpha, beta);
			}
			else if (kind == Distribution.DistributionKind.ORO)
			{
				return new GammaORO(alpha, beta);
			}
			else
				throw new RuntimeException("Unrecognized distribution kind");
		}
	}
	
	
	public static class GammaApache implements Gamma
	{
		private static org.apache.commons.math.distribution.DistributionFactory distFactory = 
			org.apache.commons.math.distribution.DistributionFactory.newInstance();
			
		private org.apache.commons.math.distribution.GammaDistribution dist;
		
		private java.util.Random random = new Random();
		
		
		public GammaApache(double alpha, double beta)
		{
			dist = distFactory.createGammaDistribution(alpha, beta);
			System.out.println("Created " + dist.getClass().getName());
		}
		
		
		public double getPD()
		{
			try { return dist.inverseCumulativeProbability(random.nextDouble()); }
			catch (Exception ex) { throw new RuntimeException(ex); }
		}
	}
	
	
	public static class GammaORO implements Gamma
	{
		private drasys.or.prob.GammaDistribution dist;
		
		public GammaORO(double alpha, double beta)
		{
			dist = new drasys.or.prob.GammaDistribution(alpha, beta);
			System.out.println("Created " + dist.getClass().getName());
		}
		
		
		public double getPD()
		{
			return dist.getRandomScaler();
		}
	}
	
	
	public static class Constant implements Distribution
	{
		private double value;
		
		
		public Constant(double value)
		{
			this.value = value;
		}
		
		
		public double getPD()
		{
			return value;
		}
	}
}


