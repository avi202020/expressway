/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package statistics;

import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.DistributionFactory;
import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.GammaDistributionImpl;

public class FitGammaDistribution
{
	/**
	 * Fit a gamma distribution to the specified parameters. That is, find a
	 * gamma distribution such that cumulative distribution is 0.022
	 * at 'lower2sigma'.
	 *
	 * See:
	 *	http://en.wikipedia.org/wiki/Gamma_distribution#Maximum_likelihood_estimation
	 */
	 
	public static GammaDistributionImpl fit(double lower2Sigma, double mode)
	throws
		Exception
	{
		if (mode <= lower2Sigma) throw new Exception("Mode is <= lower 2 sigma");
		
		double sigma = (mode - lower2Sigma)/2.0;


		// Properties of Gamma:
		// mode = scale * (shape-1)    [if scale >= 1; otherwise mode = 0]
		// sigma^2 = scale^2 * shape
		// mean = scale * shape
		
		// By derivation:
		
		double scale = sigma * sigma / (mode + 1.0);
		double sigmaOverScale = sigma / scale;
		double shape = sigmaOverScale * sigmaOverScale;
		
		if (shape <= 0.0) throw new Exception("Shape is negative");
		
		GammaDistributionImpl dist = null;
		
		dist = new GammaDistributionImpl(shape, scale);

		return dist;
	}
	
	
	
	/*
	public static void main(String[] args)
	{
		double[][] values =  // m, 2sigma
		{
			{ 100.0, 090.0 },
			{ 250.0, 200.0 },
			{ 200.0, 180.0 },

			{ 700.0, 50.0 },
			{ 700.0, 100.0 },
			{ 700.0, 200.0 },
			{ 700.0, 300.0 },
			{ 700.0, 400.0 },
			{ 700.0, 500.0 },
			{ 700.0, 600.0 },

			{ 1000.0, 500.0 },
			{ 1500.0,  500.0 },
			{ 1500.0,  700.0 },
			{ 1500.0,  800.0 },
			{ 1500.0,  900.0 },
			{ 1500.0,  950.0 },
			{ 1500.0,  980.0 },
			{ 1500.0,  1000.0 },
			{ 1500.0,  1100.0 },
			{ 1500.0,  1300.0 }
		};
		
		
		System.out.println("Mode\t2Sigma\talpha\tbeta\tcomp mean\tcomp 2sigma\tpercent");
		
		for (int i = 0; i < values.length; i++) try
		{
			double mode = values[i][0];
			double twoSigma = values[i][1];
			
			GammaDistributionImpl dist = fit(twoSigma, mode);
			
			double shape = dist.getAlpha();
			double scale = dist.getBeta();
			
			double computedMean = dist.getAlpha() * dist.getBeta();
			//double computedTwoSigma = dist.inverseCumulativeProbability(0.022);... no
			double computedTwoSigma = computedMean - 2.0 * scale * Math.sqrt(shape);
			
			double percent = 100.0 * (computedTwoSigma - twoSigma) / (twoSigma);
			
			System.out.println(mode + "\t" + twoSigma + "\t" + dist.getAlpha()
				+ "\t" + dist.getBeta() + "\t" + computedMean + "\t" + computedTwoSigma
				+ "\t" + percent);
		}
		catch (Exception ex)
		{
			ex.printStackTrace(); 
		}
	}*/
}

