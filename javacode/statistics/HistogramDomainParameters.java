/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package statistics;

/**
 * Container for parameters that characterize the domain of a histogram.
 */
 
public class HistogramDomainParameters implements java.io.Serializable
{
	public double bucketSize;
	public double bucketStart;
	public int noOfBuckets;
	
	public String toString()
	{
		return "[ HistogramDomainParameters: bucketSize=" + bucketSize +
			", bucketStart=" + bucketStart + ", noOfBuckets=" + noOfBuckets + "]";
	}
}
	
	

