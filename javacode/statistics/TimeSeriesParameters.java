/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package statistics;

/**
 * Container for parameters that characterize a time series of values.
 */
 
public class TimeSeriesParameters implements java.io.Serializable
{
	public long periodDuration;
	public long startTime;
	public int noOfPeriods;
	
	public String toString()
	{
		return "[ TimeSeriesParameters: periodDuration=" + periodDuration +
			", startTime=" + startTime + ", noOfPeriods=" + noOfPeriods + "]";
	}
}

