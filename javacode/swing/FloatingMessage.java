/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package swing;

import javax.swing.*;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class FloatingMessage extends JDialog
{
	private static final Color TranslucentWhite = new Color(255, 255, 255, 127);
	
	public FloatingMessage(String text, int x, int y, int w, int h)
	{
		super();
		setBackground(TranslucentWhite);
		setLayout(new FlowLayout());
		getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		setBounds(x, y, w, h);
		setAlwaysOnTop(true);

		// Add a close button.
		JButton closeButton = new JButton("X");
		closeButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) { dispose(); }
		});
		getContentPane().add(closeButton);

		// Add text.
		JTextArea textArea = new JTextArea(text);
		textArea.setEditable(false);
		getContentPane().add(textArea);
		
		setVisible(true);
	}
}

