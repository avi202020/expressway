/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package swing;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.event.*;
import java.util.List;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.tree.TreePath;
import javax.swing.table.JTableHeader;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeExpansionEvent;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.*;
import org.jdesktop.swingx.decorator.*;
import org.jdesktop.swingx.renderer.*;
import org.apache.commons.math.analysis.SplineInterpolator;
import org.apache.commons.math.analysis.UnivariateRealFunction;
import org.apache.commons.math.FunctionEvaluationException;


/**
 * Multiple TreeTables, with a Panel between each adjacent pair that displays
 * the connections from the rows of adjacent Panels.
 */
 
public class MultiHierarchyPanel extends JPanel
{
	private List<TreeTableComponent> treePanels = new Vector<TreeTableComponent>();
	private List<CrossMapPanel> crossMapPanels = new Vector<CrossMapPanel>();


	public void addTreeTableComponent(int position, TreeTableComponent tt)
	{
		if (! (tt instanceof JComponent)) throw new RuntimeException(
			"A TreeTableComponent must be a JComponent");
		
		treePanels.add(position, tt);
		defineLayout();
	}
	
	
	public void addTreeTableComponent(TreeTableComponent tt)
	{
		addTreeTableComponent(treePanels.size(), tt);
	}
	
	
	public void addCrossMapPanel(int position, CrossMapPanel p)
	{
		crossMapPanels.add(position, p);
		defineLayout();
	}
	
	
	public void addCrossMapPanel(CrossMapPanel p)
	{
		addCrossMapPanel(crossMapPanels.size(), p);
	}
	
	
	/**
	 * Remove the specified TreeTableComponent from this MultiHierarchyPanel.
	 * The TreeTableComponent must be the last (right-most) one in the treePanels
	 * List. The CrossMapPanel immediately to its left (if any) is also removed.
	 */
	 
	public void removeTreeTableComponent(TreeTableComponent ttc)
	throws
		Exception
	{
		if (! isLastTreeTableComponent(ttc)) throw new Exception(
			"Cannot remove TreeTableComponent: to remove it, it must be the last one");
		
		// Remove the CrossMapPanel (if any) that is immediately to the left of 'ttc'.
		int index = treePanels.indexOf(ttc);
		if (index > 0)
		{
			CrossMapPanel cmp = crossMapPanels.get(index-1);
			crossMapPanels.remove(cmp);
		}
		
		treePanels.remove(ttc);
		defineLayout();
		repaint();
	}
	
	
	public void remove(Component comp)
	{
		if (comp instanceof TreeTableComponent) try
		{
			removeTreeTableComponent((TreeTableComponent)comp);
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
		else
			super.remove(comp);
	}
	
	
	public int getTreeTableComponentNo(TreeTableComponent ttc) { return treePanels.indexOf(ttc); }
	public int getCrossMapPanelNo(CrossMapPanel p) { return crossMapPanels.indexOf(p); }
	
	
	public boolean isFirstTreeTableComponent(TreeTableComponent ttc)
	{
		if (treePanels.size() == 0) throw new RuntimeException(
			"MultiHierarchyPanel has no TreeTableComponents");
		
		return treePanels.get(0) == ttc;
	}
	
	
	public boolean isLastTreeTableComponent(TreeTableComponent ttc)
	{
		if (treePanels.size() == 0) throw new RuntimeException(
			"MultiHierarchyPanel has no TreeTableComponents");
		
		return treePanels.get(treePanels.size()-1) == ttc;
	}
	
	
	public TreeTableComponent getTreeTableComponent(int pos) { return treePanels.get(pos); }
	public CrossMapPanel getCrossMapPanel(int pos) { return crossMapPanels.get(pos); }
	
	public List<TreeTableComponent> getTreeTableComponents() { return treePanels; }
	public List<CrossMapPanel> getCrossMapPanels() { return crossMapPanels; }
	
	public int getNoOfTreeTableComponents() { return treePanels.size(); }
	public int getNoOfCrossMapPanels() { return crossMapPanels.size(); }
	
	
	public void notifyMappingChanged(TreeTableNode fromNode)
	{
		// Identify the CrossMapPanels that map the specified node to a node in
		// another TreeTableComponent.
		
		Set<CrossMapPanel> affectedPanels = new HashSet<CrossMapPanel>();
		for (CrossMapPanel cmp : crossMapPanels)
		{
			if (cmp.mapsFrom(fromNode))
			{
				affectedPanels.add(cmp);
			}
		}
		
		// Update the connections in those CrossMapPanels by calling the
		// leftTreeStructureChanged or rightTreeStructureChanged on each
		// of those CrossMapPanels.
		
		for (CrossMapPanel cmp : crossMapPanels)
		{
			cmp.leftTreeStructureChanged(fromNode);
			cmp.rightTreeStructureChanged(fromNode);
			cmp.repaint();
		}
	}
	
	
	/**
	 * Return the TreeTableComponents that are to the left (if any) and right (if any)
	 * of the specified TreeTableComponent. Returns from 0-2 TreeTableComponents.
	 */
	 
	public List<TreeTableComponent> getAdjacentTreeTableComponents(TreeTableComponent ttc)
	{
		TreeTableComponent leftTtc = null;
		TreeTableComponent rightTtc = null;
		List<TreeTableComponent> ttcs = new Vector<TreeTableComponent>();
		
		TreeTableComponent priorTtc = null;
		for (TreeTableComponent ttc2 : treePanels)
		{
			if (leftTtc != null)
			{
				rightTtc = ttc2;
				break;
			}
			
			if (ttc2 == ttc) leftTtc = priorTtc;
			priorTtc = ttc2;
		}
		
		if (leftTtc != null) ttcs.add(leftTtc);
		if (rightTtc != null) ttcs.add(rightTtc);
		
		return ttcs;
	}


	private void defineLayout()
	{
		removeAll();
		
		setLayout(new GridLayout(1, treePanels.size() + crossMapPanels.size()));
		boolean first = true;
		int i = 0;
		for (TreeTableComponent treePanel : treePanels)
		{
			if (first) first = false;
			else add(crossMapPanels.get(i++));
			
			add((Component)treePanel);
		}
		
		validate();
	}
	
	
    public MultiHierarchyPanel(String name)
	{
		super();

		setBackground(Color.white);
		
		for (TreeTableComponent panel : treePanels)
		{
			JScrollPane scrollPane = new JScrollPane((JComponent)panel);
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		}
		
		int headerHeight = 15;
		
		for (CrossMapPanel panel : crossMapPanels) panel.setHeaderHeight(headerHeight);
		
		repaint();
	}
	
	
	public MultiHierarchyPanel postConstructionSetup()
	throws
		Exception
	{
		for (TreeTableComponent treePanel : treePanels) treePanel.postConstructionSetup();
		return this;
	}
	
	
	/**
	 * A Tree Table that can be instantiated into this MultiHierarchyPanel.
	 */
	 
	public interface TreeTableComponent
	{
		JXTreeTable getJXTreeTable();
		
		void postConstructionSetup()
		throws
			Exception;
		
		TreeTableNode getNodeById(String nodeId);
		
		void addChangeListener(TreeTableComponentChangeListener listener);
		
		/** Called when a Visual is selected. After being called, the listener
			is removed. */
		void addTemporaryRowSelectionListener(RowSelectionListener listener);
		
		void removeTemporaryRowSelectionListener();
		
		int getYPosition(TreeTableNode node);
		
		TreeTableComponent getPriorTreeTableComponent();
		
		TreeTableComponent getNextTreeTableComponent();
	}
	
	
	/**
	 * A means of being notified when another Tree Table in this MultiHierarchyPanel
	 * changes, allowing the Table that created the Listener to update itself.
	 * Note that 'node' belongs to the TreeTable
	 * that has changed - not the TreeTable that will be the target of
	 * the method call.
	 */
	 
	public interface TreeTableComponentChangeListener
	{
		void notifyNodeChanged(TreeTableNode node);
		
		void notifyNodeInserted(TreeTableNode node);
		
		void notifyNodeRemoved(TreeTableNode node);
		
		void notifyTreeStructureChanged(TreeTableNode node);
	}
	
	
	public interface RowSelectionListener
	{
		void visualSelected(TreeTableNode treeTableNode);
	}


	public static int getYPosition(JXTreeTable treeTable, TreeTableNode node)
	throws
		Exception
	{
		TreeTableNode[] nodesPath = getPathToRoot(node, treeTable);
		TreePath path = new TreePath(nodesPath);
		int row = treeTable.getRowForPath(path);
		
		if (! isVisible(path, treeTable)) throw new Exception(path.toString() + " is not visible");
		
		if (row < 0) throw new Exception(path.toString() + 
			" is not visible in table with root " +
			treeTable.getTreeTableModel().getRoot().toString());
		
		JTableHeader header = treeTable.getTableHeader();
		Rectangle headerRect = header.getHeaderRect(0);
		int titleBarHeight = headerRect.height;
		
		int column = 0;
		Rectangle rect = treeTable.getCellRect(row, column, true);
		return rect.y + rect.height/2 + titleBarHeight;
	}
	
	
	/**
	 * Use this instead of the DefaultTreeTableModel method, which seems to be broken.
	 */
	
	public static TreeTableNode[] getPathToRoot(TreeTableNode node, JXTreeTable treeTable)
	{
		List<TreeTableNode> pathList = new Vector<TreeTableNode>();
		for (;;)
		{
			if (node == null) return pathList.toArray(new TreeTableNode[pathList.size()]);

			pathList.add(0, node);
			if (node == ((TreeTableModel)(treeTable.getTreeTableModel())).getRoot())
				return pathList.toArray(new TreeTableNode[pathList.size()]);
			node = node.getParent();
		}
	}
	
	
	/**
	 * Use this instead of the JXTreeTable method, which seems to be broken.
	 */
	
	public static boolean isVisible(TreePath path, JXTreeTable treeTable)
	{
		for (;;)
		{
			if (path == null) return true;
			if (path.getPathCount() <= 1) return true;
			path = path.getParentPath();
			if (! treeTable.isExpanded(path)) return false;
		}
	}
		
		
	/**
	 * Extend this to define the center Panel that connects the two hierarchies.
	 */
	 
	public abstract static class CrossMapPanel extends JPanel
	{
		/** Rebuild and repaint this CrossMapPanel. */
		public abstract void refresh();
		
		/** Indicates that the specified Node's data changed, but its children
			were not removed or added. */
		public abstract void leftTreeTableNodeChanged(TreeTableNode node);
		
		public abstract void leftTreeTableNodeInserted(TreeTableNode node);
		
		public abstract void leftTreeTableNodeRemoved(TreeTableNode node);
		
		public abstract void leftTreeStructureChanged(TreeTableNode node);
		
		public abstract void rightTreeTableNodeChanged(TreeTableNode node);
		
		public abstract void rightTreeTableNodeInserted(TreeTableNode node);
		
		public abstract void rightTreeTableNodeRemoved(TreeTableNode node);
		
		public abstract void rightTreeStructureChanged(TreeTableNode node);
		
		public abstract void connectionSelected(final TreeTableNode leftNode,
			final TreeTableNode rightNode, final int x, final int y);
		
		public abstract void addContextMenuItemsForConnections(final JPopupMenu containerPopup,
			final TreeTableNode leftNode, final TreeTableNode rightNode, final int x, final int y);
		
		public CrossMapPanel(TreeTableComponent leftPanel, TreeTableComponent rightPanel)
		{
			init(leftPanel, rightPanel);
		}
		
		public final void setHeaderHeight(int h) { this.headerHeight = h; }
		
		public final TreeTableComponent getLeftPanel() { return leftPanel; }
		
		public final TreeTableComponent getRightPanel() { return rightPanel; }
		
		public final void setLeftToRightLineColor(Color c) { this.leftToRightColor = c; }
		
		public final void setRightToLeftLineColor(Color c) { this.rightToLeftColor = c; }
		
		/**
		 * Return true if this CrossMapPanel contains a mapping for the specified
		 * TreeTableNode.
		 */
		 
		public final boolean mapsFrom(TreeTableNode node)
		{
			return 
				leftConnections.containsKey(node) || rightConnections.containsKey(node);
		}
		
		
		/* *********************************************************************
		 * Internal implementation.
		 */
		 
		TreeTableComponent leftPanel;
		TreeTableComponent rightPanel;
		Map<TreeTableNode, Set<TreeTableNode>> leftConnections = 
			new HashMap<TreeTableNode, Set<TreeTableNode>>();
		Map<TreeTableNode, Set<TreeTableNode>> rightConnections = 
			new HashMap<TreeTableNode, Set<TreeTableNode>>();
		int headerHeight;
		Color leftToRightColor = Color.black;
		Color rightToLeftColor = Color.black;
		
		
		protected void init(TreeTableComponent leftPanel, TreeTableComponent rightPanel)
		{
			this.setBackground(Color.white);
			this.leftPanel = leftPanel;
			this.rightPanel = rightPanel;
			
			addMouseListener(new MouseAdapter()
			{
				public void mouseClicked(MouseEvent e)
				{
					if // right-mouse
						((e.getButton() == MouseEvent.BUTTON1) && (! e.isControlDown()))
					{
						final int x = e.getX();
						final int y = e.getY();
						Connection[] conns = getConnectionsAt(x, y);
						if (conns.length > 0)  // a connection was selected
						{
							if (conns.length > 1)  // ask user which one they want
							{
								JPopupMenu popup = new JPopupMenu();
								popup.add(new JMenuItem("Which connection did you intend?"));
								popup.add(new JSeparator());
								for (final Connection conn : conns)
								{
									JMenuItem mi = new JMenuItem("From " +
										conn.fromNode.toString() + " to " + conn.toNode.toString());
									mi.addActionListener(new ActionListener()
									{
										public void actionPerformed(ActionEvent e2)
										{
											connectionSelected(conn.fromNode, conn.toNode, x, y);
										}
									});
								}
							}
							else
								connectionSelected(conns[0].fromNode, conns[0].toNode, x, y);
						}
					}
				}
				
				public void mouseReleased(final MouseEvent e)
				{
					if ( // right-mouse
						(e.getButton() == MouseEvent.BUTTON2) ||
						(e.getButton() == MouseEvent.BUTTON3) ||
						((e.getButton() == MouseEvent.BUTTON1) && e.isControlDown())
					)
					{
						final int x = e.getX();
						final int y = e.getY();
						Connection[] conns = getConnectionsAt(x, y);
						if (conns.length > 0)  // a connection was selected
						{
							if (conns.length > 1)  // ask user which one they want
							{
								JPopupMenu popup = new JPopupMenu();
								popup.add(new JMenuItem("Which connection did you intend?"));
								popup.add(new JSeparator());
								for (final Connection conn : conns)
								{
									JMenuItem mi = new JMenuItem("From " +
										conn.fromNode.toString() + " to " + conn.toNode.toString());
									mi.addActionListener(new ActionListener()
									{
										public void actionPerformed(ActionEvent e2)
										{
											JPopupMenu popup2 = new JPopupMenu();
											addContextMenuItemsForConnections(popup2,
												conn.fromNode, conn.toNode, x, y);
											popup2.show(e.getComponent(), e.getX(), e.getY());
										}
									});
								}
							}
							else
							{
								JPopupMenu popup = new JPopupMenu();
								addContextMenuItemsForConnections(popup,
									conns[0].fromNode, conns[0].toNode, x, y);
								popup.show(e.getComponent(), e.getX(), e.getY());
							}
						}
					}
				}
			});
			
			TreeExpansionListener expansionListener = new TreeExpansionListener()
			{
				public void treeExpanded(TreeExpansionEvent event)
				{
					TreePath path = event.getPath();
					TreeTableNode node = (TreeTableNode)(path.getLastPathComponent());
					CrossMapPanel.this.repaint();
				}
				
				public void treeCollapsed(TreeExpansionEvent event)
				{
					TreePath path = event.getPath();
					TreeTableNode node = (TreeTableNode)(path.getLastPathComponent());
					CrossMapPanel.this.repaint();
				}
			};
			
			leftPanel.getJXTreeTable().addTreeExpansionListener(expansionListener);
			rightPanel.getJXTreeTable().addTreeExpansionListener(expansionListener);
			
			leftPanel.addChangeListener(new TreeTableComponentChangeListener()
			{
				public void notifyNodeChanged(TreeTableNode node)
				{
					leftTreeTableNodeChanged(node);
				}
				
				public void notifyNodeInserted(TreeTableNode node)
				{
					leftTreeTableNodeInserted(node);
				}
				
				public void notifyNodeRemoved(TreeTableNode node)
				{
					leftTreeTableNodeRemoved(node);
				}
				
				public void notifyTreeStructureChanged(TreeTableNode node)
				{
					leftTreeStructureChanged(node);
				}
			});
			
			rightPanel.addChangeListener(new TreeTableComponentChangeListener()
			{
				public void notifyNodeChanged(TreeTableNode node)
				{
					rightTreeTableNodeChanged(node);
				}
				
				public void notifyNodeInserted(TreeTableNode node)
				{
					rightTreeTableNodeInserted(node);
				}
				
				public void notifyNodeRemoved(TreeTableNode node)
				{
					rightTreeTableNodeRemoved(node);
				}
				
				public void notifyTreeStructureChanged(TreeTableNode node)
				{
					rightTreeStructureChanged(node);
				}
			});
		}
		
		
		protected final void connectLeftToRightNode(boolean connect,
			TreeTableNode leftNode, TreeTableNode rightNode, boolean repaintNow)
		{
			Set<TreeTableNode> rightNodes = leftConnections.get(leftNode);
			if (rightNodes == null)
			{
				rightNodes = new HashSet<TreeTableNode>();
				leftConnections.put(leftNode, rightNodes);
			}
			
			if (connect)
			{
				rightNodes.add(rightNode);
			}
			else
				rightNodes.remove(rightNode);
			
			if (repaintNow) this.repaint();
		}
		
		
		protected final void connectRightToLeftNode(boolean connect,
			TreeTableNode rightNode, TreeTableNode leftNode, boolean repaintNow)
		{
			Set<TreeTableNode> leftNodes = rightConnections.get(rightNode);
			if (leftNodes == null)
			{
				leftNodes = new HashSet<TreeTableNode>();
				rightConnections.put(rightNode, leftNodes);
			}
			
			if (connect)
				leftNodes.add(leftNode);
			else
				leftNodes.remove(leftNode);
			
			if (repaintNow) this.repaint();
		}
		
		
		protected final void leftNodeRemoved(TreeTableNode leftNode)
		{
			leftConnections.remove(leftNode);
		}
		
		
		protected final void rightNodeRemoved(TreeTableNode rightNode)
		{
			rightConnections.remove(rightNode);
		}
		
		
		protected final void removeAllConnections()
		{
			leftConnections.clear();
			rightConnections.clear();
		}
		
		
		TreeTableNode getVisibleNode(TreeTableNode node, JXTreeTable treeTable)
		{
			TreeTableNode[] nodes = MultiHierarchyPanel.getPathToRoot(node, treeTable);
			TreePath path = new TreePath(nodes);
			for (;;)
			{
				TreeTableNode visibleNode = (TreeTableNode)(path.getLastPathComponent());
				if (visibleNode == null) return null;
				if (MultiHierarchyPanel.isVisible(path, treeTable)) return visibleNode;
				path = path.getParentPath();
			}
		}
		
		
		/**
		 * Return in nestedNodes all of the nodes nested under the specified node, including
		 * the node itself. Recursive.
		 */
		 
		protected Set<TreeTableNode> getAllNestedNodes(TreeTableNode node,
			Set<TreeTableNode> nestedNodes)
		{
			nestedNodes.add(node);
			for (Enumeration<? extends TreeTableNode> ce = node.children(); ce.hasMoreElements();)
			{
				getAllNestedNodes(ce.nextElement(), nestedNodes);
			}
			
			return nestedNodes;
		}
		
		
		/**
		 * Paint an arrow from each node in the panel to each connected node in
		 * the other panel. If leftToRight, draw arrow from left to right, otherwise
		 * right to left.
		 */
		 
		protected void paintConnections(Graphics g, TreeTableComponent panel,
			TreeTableComponent otherPanel, Map<TreeTableNode,
			Set<TreeTableNode>> connections, boolean leftToRight)
		{
			Set<TreeTableNode> nodes = connections.keySet();
			for (TreeTableNode node : nodes)  // each panel node that has a connection
			{
				// The node might not be visible: it might belong to a node that is
				// collapsed. Therefore, identify the innermost visible node that
				// contains node.
				TreeTableNode visibleNode = getVisibleNode(node, panel.getJXTreeTable());
				
				// Identify all nodes that are contained within the visible node, inclusive.
				Set<TreeTableNode> nestedNodes =
					getAllNestedNodes(visibleNode, new HashSet<TreeTableNode>());

				// For each of the nested nodes, identify all connected nodes in the right Panel.
				Set<TreeTableNode> connectedNodes = new HashSet<TreeTableNode>();
				for (TreeTableNode nestedNode : nestedNodes)
				{
					Set<TreeTableNode> connectedNodesForNestedNode = connections.get(nestedNode);
					if (connectedNodesForNestedNode != null)
						connectedNodes.addAll(connectedNodesForNestedNode);
				}

				if (connectedNodes != null) for (TreeTableNode connectedNode : connectedNodes)
				{
					TreeTableNode connectedVisibleNode =
						getVisibleNode(connectedNode, otherPanel.getJXTreeTable());
					
					int yNode = panel.getYPosition(visibleNode) + headerHeight;
					int yConnectedNode = otherPanel.getYPosition(connectedVisibleNode) + headerHeight;
					int xNode = (leftToRight? 0 : this.getWidth());
					int xConnectedNode = (leftToRight? this.getWidth() : 0);
					
					// Draw spline from node to connectedVisibleNode.
					
					if (leftToRight)
					{
						drawSpline(g, xNode, yNode, xConnectedNode, yConnectedNode);
						drawRightArrow(g, xConnectedNode, yConnectedNode);
					}
					else
					{
						drawSpline(g, xConnectedNode, yConnectedNode, xNode, yNode);
						drawLeftArrow(g, xConnectedNode, yConnectedNode);
					}
				}
			}
		}
		
		
		public final void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			

			// Draw spline between each pair of connected Nodes.
			
			g.setColor(leftToRightColor);
			paintConnections(g, leftPanel, rightPanel, leftConnections, true);

			g.setColor(rightToLeftColor);
			paintConnections(g, rightPanel, leftPanel, rightConnections, false);
		}
		
		
		protected void drawSpline(Graphics g, int xFrom, int yFrom, int xTo, int yTo)
		{
			final Color shapeColor = Color.green;

			GeneralPath path = getSplinePath(xFrom, yFrom, xTo, yTo);

			g.setColor(shapeColor);
			g.setPaintMode();
			((Graphics2D)g).draw(path);
		}
		
		
		protected void drawLeftArrow(Graphics g, int tipX, int tipY)
		{
			g.drawLine(tipX, tipY, tipX+4, tipY-4);
			g.drawLine(tipX, tipY, tipX+4, tipY+4);
		}
		
		
		protected void drawRightArrow(Graphics g, int tipX, int tipY)
		{
			g.drawLine(tipX, tipY, tipX-4, tipY-4);
			g.drawLine(tipX, tipY, tipX-4, tipY+4);
		}
		
		
		protected GeneralPath getSplinePath(int xFrom, int yFrom, int xTo, int yTo)
		{
			// Derive a natural spline function to fit all of the graph's points.
			
			final double DeltaX = this.getWidth() / 6.0;
			final Color shapeColor = Color.green;
			double[] xValues = { xFrom, xFrom+DeltaX, xTo-DeltaX, xTo };
			double[] yValues = { yFrom, yFrom, yTo, yTo };
			
			SplineInterpolator interpolator = new SplineInterpolator();
			UnivariateRealFunction function = interpolator.interpolate(xValues, yValues);
			
			
			// Use the natural spline to create extra points if necessary, in order
			// to ensure that we have 3n+1 points, where n the smallest whole number
			// such that 3n+1 >= the original number of points. This is because a cubic
			// spline is composed of three-point triplets, but starting from an initial
			// point.
			
			int windingRule = GeneralPath.WIND_NON_ZERO;  // set arbitrarily: not used.
			int noOfPoints = xValues.length;
			int noOfSegments = noOfPoints-1;
			GeneralPath path = new GeneralPath(windingRule, noOfSegments);
			
			// Add the starting point of the spline.
			double priorX = xValues[0];
			double priorY = yValues[0];
			
			checkForFloatRange(priorX);
			checkForFloatRange(priorY);
			
			path.moveTo((float)priorX, (float)priorY);
			
			// Add each successive triplet of points, to form a cubic B-spline.
			for (int i = 1; i < noOfPoints; i++)  // each point
			{
				double newX = xValues[i];
				double newY = yValues[i];
	
				checkForFloatRange(newX);
				checkForFloatRange(newY);
	
				double oneThird = (newX - priorX)/3.0;
				double c1x = priorX + oneThird;
				double c2x = c1x + oneThird;
				
				double c1y;
				double c2y;
				try
				{
					c1y = function.value(c1x);
					c2y = function.value(c2x);
				}
				catch (FunctionEvaluationException ex)
				{
					System.out.println("Error evaluating natural spine function: "
						+ ex.getMessage());
						
					continue;
				}


				// Note that the spline will not necessarily go through c1 and c2.
				// These are obtained from the interpolated natural spline anyway.
				path.curveTo((float)c1x, (float)c1y, (float)c2x, (float)c2y, 
					(float)newX, (float)newY);
				
				priorX = newX;
				priorY = newY;
			}
			
			return path;
		}
		
		
		protected boolean intersectsPath(GeneralPath path, int x, int y)
		{
			// Allow a tolerance of one pixel.
			return path.intersects((double)(x-1), (double)(y-1), 3.0, 3.0);
		}
		
		
		protected Connection[] getConnectionsAt(int x, int y)
		{
			Set<Connection> intersectingConns = new HashSet<Connection>();
			
			Set<TreeTableNode> nodes = leftConnections.keySet();
			for (TreeTableNode node : nodes)
			{
				TreeTableNode visibleNode = getVisibleNode(node, leftPanel.getJXTreeTable());
				
				Set<TreeTableNode> otherNodes = leftConnections.get(visibleNode);
				if (otherNodes != null) for (TreeTableNode otherNode : otherNodes)
				{
					TreeTableNode otherVisibleNode = getVisibleNode(otherNode, rightPanel.getJXTreeTable());
					
					int yLeft = leftPanel.getYPosition(visibleNode) + headerHeight;
					int yRight = rightPanel.getYPosition(otherVisibleNode) + headerHeight;
					int xLeft = 0;
					int xRight = this.getWidth();
					
					GeneralPath path = getSplinePath(xLeft, yLeft, xRight, yRight);
					if (intersectsPath(path, x, y))
						intersectingConns.add(new Connection(node, otherNode));
				}
			}
			
			nodes = rightConnections.keySet();
			for (TreeTableNode node : nodes)
			{
				TreeTableNode visibleNode = getVisibleNode(node, rightPanel.getJXTreeTable());
				
				Set<TreeTableNode> otherNodes = rightConnections.get(visibleNode);
				if (otherNodes != null) for (TreeTableNode otherNode : otherNodes)
				{
					TreeTableNode otherVisibleNode = getVisibleNode(otherNode, leftPanel.getJXTreeTable());
					
					int yLeft = leftPanel.getYPosition(visibleNode) + headerHeight;
					int yRight = rightPanel.getYPosition(otherVisibleNode) + headerHeight;
					
					int xLeft = 0;
					int xRight = this.getWidth();
					
					GeneralPath path = getSplinePath(xRight, yRight, xLeft, yLeft);
					if (intersectsPath(path, x, y))
						intersectingConns.add(new Connection(node, otherNode));
				}
			}
			
			return intersectingConns.toArray(new Connection[intersectingConns.size()]);
		}
		

		private void checkForFloatRange(double x)
		{
			if (x >= Float.POSITIVE_INFINITY)
				throw new RuntimeException(
					"Max Float value exceeded: " + x + " > " + Float.POSITIVE_INFINITY);
			
			if (x <= Float.NEGATIVE_INFINITY)
				throw new RuntimeException(
					"Min Float value exceeded: " + x + " < " + Float.NEGATIVE_INFINITY);
		}
		
		
		class Connection
		{
			TreeTableNode fromNode;
			TreeTableNode toNode;
			
			Connection(TreeTableNode fromNode, TreeTableNode toNode)
			{
				this.fromNode = fromNode;
				this.toNode = toNode;
			}
		}
	}
}

