package trythis;

import java.util.*;
import java.text.*;

public class TryThis6
{
	public static void main(String[] args) throws Exception
	{
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
		
		Date date = new Date();
		System.out.println("Created new Date: " + date.toString());
		String dateString = df.format(date);
		
		System.out.println("Formatted date as String: " + dateString);
		
		
		Date d = df.parse(dateString);
		System.out.println("Parsed the formatted date, to obtain this Date: " + d.toString());
	}
}

