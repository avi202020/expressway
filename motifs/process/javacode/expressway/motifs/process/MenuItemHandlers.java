/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.motifs.process;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.VisualComponent.*;
import expressway.gui.*;
import awt.AWTTools;
import java.awt.BorderLayout;
import javax.swing.JDialog;


/**
 * These are enabled by adding them to the XML for the Process motif.
 */
 
public class MenuItemHandlers
{
	/* MenuItemContext has these methods:
		View getView();
		VisualComponentDisplayArea getDisplayArea();
		ScenarioVisual getCurrentScenarioVisual();
		VisualComponent getVisualComponent();
		HelpfulMenuItem getMenuItem();
		ModelEngineRMI getModelEngine();
	 */
	
	
	/**
	 * Activity->Set Role.
	 * For a selected Activity, present the user with a list of Roles and allow
	 * the user to choose a Role to assign to the Activity.
	 */
	
	public void selectNewRole(MenuItemContext context)
	{
		// Identify the selected Activity.
		
		VisualComponent visual = context.getVisualComponent();
		if (! (visual instanceof ActivityVisual))
		{
			ErrorDialog.showReportableDialog((Component)(context.getDisplayArea()),
				"The selected Visual is not an Activity Visual");
			return;
		}
		
		ActivityVisual activityVis = (ActivityVisual)visual;
		
		// Enable the user to select a Role.
		
		RoleChooser roleChooser = new RoleChooser(context);
		RoleSer roleSer = roleChooser.getSelectedRoleSer();
		if (roleSer == null) return;  // user cancelled action.
		
		// Apply the Role to the selected Activity.
		
		ModelEngineRMI modelEngine = context.getModelEngine();
		try
		{
			try { modelEngine.createCrossReference(false,
				"Performs", roleSer.getNodeId(), activityVis.getNodeId()); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog((Component)(context.getDisplayArea()),
					w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					modelEngine.createCrossReference(true,
						"Performs", roleSer.getNodeId(), activityVis.getNodeId());
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog((Component)(context.getDisplayArea()), ex);
		}
	}
	
	
	/**
	 * ModelDomain->Add Role.
	 * Present the user with a list of Roles and allow the user to choose a Role
	 * to add to the View.
	 */
	
	public void addRole(MenuItemContext context)
	{
		// Ask the user to select a Role from the ModelEngine.
		RoleExplorer roleExplorer = new RoleExplorer(context);
		RoleSer roleSer = roleExplorer.getSelectedRoleSer();
		if (roleSer == null) return;  // user cancelled action.
		
		// Get the Role Ids used by this View's Activities and row height from
		// the server.
		
		ModelEngineRMI modelEngine = context.getModelEngine();
		AttributeSer attrSer = modelEngine.getAttribute(
			context.getVisualComponent().getNodeId(), "Role Ids");
		
		Serializable obj;
		try { obj = modelEngine.getAttributeDefaultValueById(attrSer.getNodeId()); }
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog((Component)(context.getDisplayArea()), ex);
			return;
		}
		
		if (! (obj instanceof String[])) throw new ParameterError(
			"Attribute 'Roles' is of type " + obj.getClass().getName() +
			"; must be compatible with type String[].");
		
		String[] roleIds = (String[])obj;
		
		// Add the Role to the View - if it is not already in the View.
		for (String roleId : roleIds)
		{
			if (roleSer.getNodeId().equals(roleId))
			{
				ErrorDialog.showErrorDialog((Component)(context.getView()),
					"Role '" + roleSer.getFullName() "' is already in the View");
				return;
			}
		}
		
		// Append Role that the user selected
		String newRoleIds = new String[roleIds.length+1];
		int i = 0;
		for (String roleId : roleIds)
		{
			newRoleIds[i++] = roleId;
		}
		newRoleIds[i] = roleSer.getNodeId();
		
		try
		{
			try { modelEngine.setAttributeDefaultValue(false,
				context.getVisualComponent().getNodeId(), newRoleIds); }
			catch (Warning w)
			{
				// To do: Add optimistic locking to the Attribute value.
				if (JOptionPane.showConfirmDialog((Component)(context.getDisplayArea()),
					w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					modelEngine.setAttributeDefaultValue(true,
						context.getVisualComponent().getNodeId(), newRoleIds);
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog((Component)(context.getDisplayArea()), ex);
		}
	}
	
	
	/**
	 * Role->Remove From View.
	 * For a selected Role, remove that Role from the View. Does not delete the
	 * Role. If there are Activities displayed in the View that use the Role,
	 * an error message will be displayed and the View will not change.
	 */
	
	public void removeRole(MenuItemContext context)
	{
		String roleId = context.getVisualComponent().getNodeId();
		
		// Remove Role that the user selected
		String newRoleIds = new String[roleIds.length-1];
		int i = 0;
		for (String rid : roleIds)
		{
			if (roleId.equals(rid)) continue;
			newRoleIds[i++] = rid;
		}
		if (i != newRoleIds.length)
		{
			ErrorDialog.showReportableDialog((Component)(context.getDisplayArea()),
				"Appears to be more than one occurrence of Role '" +
				context.getVisualComponent().getFullName() + "'");
			return;
		}
		
		try
		{
			try { modelEngine.setAttributeDefaultValue(false,
				context.getVisualComponent().getNodeId(), newRoleIds); }
			catch (Warning w)
			{
				// To do: Add optimistic locking to the Attribute value.
				if (JOptionPane.showConfirmDialog((Component)(context.getDisplayArea()),
					w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					modelEngine.setAttributeDefaultValue(true,
						context.getVisualComponent().getNodeId(), newRoleIds);
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog((Component)(context.getDisplayArea()), ex);
		}
	}
	
	
	/**
	 * Role->Delete.
	 * For a selected Role, delete the Role from its Domain. If the Role is used
	 * by any Activities, an error message will be displayed and no change will
	 * occur.
	 */
	
	public void deleteRole(MenuItemContext context)
	{
		String roleId = context.getVisualComponent().getNodeId();
		try
		{
			try { deleteNode(false, roleId); }
			catch (Warning w)
			{
				if (JOptionPane.showConfirmDialog((Component)(context.getDisplayArea()),
					w.getMessage(),
					"Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					deleteNode(true, roleId);
			}
		}
		catch (Exception ex)
		{
			ErrorDialog.showReportableDialog((Component)(context.getDisplayArea()), ex);
		}
	}
	
	
	/**
	 * Dispaly a list of the Roles that are referenced by the Domain that the
	 * current View displays. Also, display a 'More' Button that allows the user
	 * to explore the other Role Domains that exist in the ModelEngine. If the user
	 * selects a Role, the Role is applied to the current Activity.
	 */
	 
	class RoleChooser extends JDialog
	{
		private RoleSer roleSer = null;
		
		
		RoleChooser(MenuItemContext context, RoleSer currentRoleSer)
		throws
			Exception
		{
			super(AWTTools.getContainingFrame((Component)(context.getDisplayArea())),
				"Select a Role", true);
			
			ModelEngineRMI modelEngine = context.getModelEngine();
			
			JLabel roleLabel = new JLabel();
			setSelectedRoleSer(currentRoleSer);
			
			// Display ComboBox of Roles referenced by the current Domain.
			
			VisualComponent visual = context.getVisualComponent();
			String domainId = visual.getNodeSer().getDomainId();
			
			NamedReferenceSer namedRefSer =
				modelEngine.getNamedReferenceForDomain(domainId, "Performs");
			CrossReferenceSer[] crossRefSers =
				modelEngine.getCrossReferences(namedRefSer.getNodeId());
			
			JComboBox roleList = new JComboBox();
			roleList.setSelectedIndex(0);
			
			for (CrossReferenceSer crSer : crossRefSers)
			{
				NodeSer ser = modelEngine.getNode(crSer.toNodeId);
				if (! (ser instanceof RoleSer)) throw new Exception(
					"Unexpected: the 'to' Node of a '" + namedRefSer.getFullName() +
					"' is not a Role.");
				roleList.addItem((RoleSer)ser);
			}
			
			roleList.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					JComboBox cb = (JComboBox)e.getSource();
					RoleSer roleSer = (RoleSer)cb.getSelectedItem();
					if (roleSer != null) setSelectedRoleSer(roleSer);
				}
			});
			
			// Display 'More...' Button that pops up a Role explorer.
			
			JButton moreButton = new JButton("More Roles...");
			moreButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					RoleExplorer roleExplorer = new RoleExplorer();
					roleExplorer.show();
					RoleSer roleSer = roleExplorer.getSelectedRoleSer();
					if (roleSer != null) setSelectedRoleSer(roleSer);
				}
			});
			
			JButton okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (roleSer == null)
					{
						ErrorDialog.showErrorDialog(RoleChooser.this,
							"No Role has been selected.");
					}
					else
						dispose();
				}
			});
			
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					setSelectedRoleSer(null);
					dispose();
				}
			});
			
			JPanel topPanel = new JPanel();
			JPanel bottomPanel = new JPanel();
			
			add(topPanel, BorderLayout.NORTH);
			add(roleList, BorderLayout.CENTER);
			add(bottomPanel, BorderLayout.SOUTH);
			topPanel.add(roleLabel);
			bottomPanel.add(moreButton);
			bottomPanel.add(okButton);
			bottomPanel.add(cancelButton);
			
			setSize(400, 300);
			setVisible(true);
		}
		
		
		/** May return null if no RoleSer was selected. */
		public RoleSer getSelectedRoleSer() { return roleSer; }
		
		void setSelectedRoleSer(RoleSer roleSer)
		{
			this.roleSer = roleSer;
			roleLabel.setText(roleSer == null? "" : roleSer.getFullName());
		}
	}
	
	
	/**
	 * Enable the user to peruse all of the Roles in the ModelEngine and select one.
	 */
	 
	class RoleExplorer extends JDialog
	{
		private RoleVisual selectedRoleVisual = null;
		
		
		public RoleExplorer(MenuItemContext context)
		{
			super(AWTTools.getContainingFrame((Component)(context.getDisplayArea())),
				"Select a Role", true);
			
			DomainSelector domainSelector;
			JScrollPanel domainTreeScrollPane = new JScrollPane(
				domainSelector = DomainSelector.createDomainSelector(getModelEngine(),
					getPanelManager(), this, "Domain List")));
		
			domainTreeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			domainTreeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			
			domainSelector.disableDisplayOfNodes(Object.class);
			domainSelector.enableDisplayOfNodes(DomainVisual.class);
			domainSelector.enableDisplayOfNodes(RoleVisual.class);
			domainSelector.enableDisplayOfNodes(DomainSelector.HostTreeNode.class);
			
			domainSelector.disableSelectionOfNodes(Object.class);
			domainSelector.enableSelectionOfNodes(RoleVisual.class);

			// Create a listener to respond to changes in the tree: specifically,
			// changes that hide or expose a selected node.
			
			domainSelector.setSelectionListener(new DomainSelector.NodeSelectionListener()
			{
				public void visualSelected(VisualComponent visual)
				{
					if (! (visual instanceof RoleVisual)) throw new RuntimeException(
						"Visual expected to be a RoleVisual");
					this.selectedRoleVisual = (RoleVisual)visual;
				}

				public void hostSelected(MutableTreeNode root)
				{
					throw new RuntimeException("Should not be able to select host");
				}
			});

			JPanel bottomPanel = new JPanel();
			
			JButton okButton = new JButton("OK");
			JButton cancelButton = new JButton("Cancel");
			
			okButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					dispose();
				}
			});
			
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					selectedRoleVisual = null;
					dispose();
				}
			});

			add(domainTreeScrollPane, BorderLayout.CENTER);
			add(bottomPanel, BorderLayout.SOUTH);
			bottomPanel.add(okButton);
			bottomPanel.add(cancelButton);
			
			setSize(400, 300);
			setVisible(true);
		}
		
		
		/** May return null if no RoleVisual was selected. */
		public RoleSer getSelectedRoleSer()
		{
			return (RoleSer)(selectedRoleVisual.getNodeSer());
		}
	}
}

