/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.motifs.process;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.server.*;
import expressway.server.NamedReference.*;


/**
 * Handlers for responding to changes to CrossReferences that a Process
 * participates in.
 */
 
public class ProcessHandlers
{
	public void roleRemoved(CrossReferenceDeletionContext context)
	{
		context.getCrossReferenceable().layout();
	}
	
	
	public void roleAdded(CrossReferenceCreationContext crContext)
	{
		context.getCrossReferenceable().layout();
	}
}

