/*
 * Confidential property of Expressway Solutons LLC.
 * Copyright (c) 2007 by Expressway Solutions LLC. All rights reserved.
 * This is prototype software. No warrantee or promise of correctness
 * is stated or implied.
 */

package expressway.motifs.process;


import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.common.PeerNoticeBase.*;
import expressway.server.ModelElement;
import expressway.server.ModelElement.*;
import expressway.server.NamedReference.*;
import java.util.*;


/**
 * LayoutManager for the Process Motif. See the slide "Process Motif".
 *
 * A Node's y coordinate is chosen to center it within the row that it is currently
 * placed within. If the Node's Role does not match the Role that that row represents,
 * then the Node's Role is changed to that Row for that row. Rows are ordered from
 * the top down, and the top-most Row is placed flush with the top of the ModelContainer
 * that is being layed out: this is done so that new Rows can be added to the bottom
 * as needed without shifting other rows up to make room, although the last Row will
 * be shifted down since it is the 'Unassigned' Row.
 *
 * A Node's x coordinate is not changed.
 *
 * The size of the ModelContainer that this LayoutManager lays out is not modified. It is
 * assumed that the ModelContainer will be displayed in a DisplayArea that is scrollable
 * and resizble.
 * 
 * It is assumed that Nodes are depicted as icons.
 */
 
public class ProcessLayoutManager extends LayoutManager
{
	private ModelContainer process;
	private List<Row> rows = new Vector<Row>();  // Ordered from top down.
		// The last Row may be for Activities that have no Role assigned to them.
	private double cachedRowHeight;
	
	
	public ProcessLayoutManager(ModelContainer pa)
	throws
		ParameterError
	{
		this.process = pa;
		Attribute attr = pa.getAttribute("Role Ids");
		if (attr == null) throw new RuntimeException(
			"No Role Ids Attribute for '" + pa.getFullName() + "'");
		
		Object roleIdsObj = pa.getDefaultValue();
		if (! (roleIdsObj instanceof String[])) throw new ParameterError(
			"Attribute 'Roles' is of type " + roleIdsObj.getClass().getName() +
			"; must be compatible with type String[].");
		
		String[] roleIds = (String[])roleIdsObj;
		getRowHeight();  // set the value of cachedRowHeight.

		
		rows.clear();
		for (String roleId : roleIds) try
		{
			Role role = (Role)(getModelEngine().getNode(roleId));
			appendNewRow(role);
		}
		catch (Exception ex) { continue; }
	}
	
	
	protected ModelEngineLocal getModelEngine()
	{
		return ServiceContext.getServiceContext().getModelEngine();
	}
	
	
	/**
	 * Return the Row being used for Activities that have no Role assigned to them,
	 * or null if there is no such Row.
	 */
	 
	protected Row getUnassignedRow()
	{
		if (rows.size() == 0) return null;
		Row lastRow = rows.get(rows.size()-1);
		if (lastRow.getRole() == null) return lastRow;
		return null;
	}
	
	
	/**
	 * Add a new Row to the bottom of the stack of Rows. The new Row is added at
	 * the end of the Rows List. If 'role' is null, create an "Unassigned" Row
	 * that has a null Role. There may only be one "Unassigned" Row, and it must
	 * be maintained at the bottom of the stack of Rows.
	 */
	 
	protected Row appendNewRow(Role role)
	{
		Row row = new Row();
		row.setRole(role);
		row.setHeight(rowHeight);
		
		Row unassignedRow = getUnassignedRow();
		if (unassignedRow != null)
		{
			if (role == null) throw new RuntimeException(
				"Attempt to create multiple Unassigned Rows");
			rows.add(rows.size()-1);  // insert before the Unassigned Row.
		}
		else
			rows.add(row);
		
		return row;
	}
	
	
	public ModelContainer getProcess() { return process; }
	
	
	/**
	 * Change the position of the Row for the specified Role, relative to the
	 * other Rows. Cannot be used to move the Unassigned Row.
	 */
	 
	public void moveRolePosition(Role role, int newPosition)
	throws
		ParameterError
	{
		if (role == null) throw new ParameterError(
			"May not move the position of the Unassigned Role");
		
		if (newPosition < 0) throw new ParameterError("new position < 0");
		synchronized (this)
		{
			if (newPosition > rows.size()) throw new ParameterError(
				"new position > number or roles");
			if ((newPosition == rows.size()) && (getUnassignedRow() != null))
				throw new ParameterError("Cannot move a Row below the Unassigned Row");
			int rowIndex = getRowIndex(role);
			if (rowIndex < 0) throw new ParameterError(
				"Row for Role '" + role.getName() + "' not found");
			Row row = rows.remove(rowIndex);
			if (newPosition > rowIndex) newPosition--; // compensate for row removed
			rows.add(newPosition, row);
		}
		
		layout(process, false, null)
		
		// Notify clients.
		try { notifyAllListeners(new NodeChangedNotice(process.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	/**
	 * Return the index in 'rows' of the Row with the specified Role. If 'role'
	 * is null, return the Unassigned Row. If the appropriate Row is not found,
	 * return -1.
	 */
	 
	protected int getRowIndex(Role role)
	{
		int i = -1;
		for (Row row : rows)
		{
			i++;
			if (row.getRole() == role) break;
		}
		return i;
	}
	
	
	/**
	 * Arrange the subordinate Nodes of 'node'; and resize 'node' so that it can
	 * accommodate its subordinate Nodes. Ignore Node in 'nodesToIgnore'.
	 * If 'recursive' is true, do this for each sub-Node as well.
	 */
	 
	public void layout(PersistentNode node, boolean recursive, 
		PersistentNode[] nodesToIgnore)
	{
		if (! (node instanceof ModelContainer)) return;
		ModelContainer proc = (ModelContainer)node;
		
		Set<PersistentNode> subNodes = node.getChildren();
		
		
		// Position each sub-Node that is not pre-defined and that is not a Port.
		// While doing this, determine the space needed by each sub-Node.
		
		NextSubnode: for (PersistentNode subNode : subNodes)
		{
			if (! subNode.isVisible()) continue;
			if (subNode instanceof Port) continue;
			
			if (recursive)
			{
				LayoutManager lm = subNode.getLayoutManager();
				if (lm != null) lm.layout(subNode, true);
			}
			
			if (nodesToIgnore != null)
				for (PersistentNode n : nodesToIgnore)
					if (n == subNode) continue NextSubnode;
			
			double y = subNode.getY();
			Row row = getRowForY(y);
			if (row == null)  // place subNode into a Row.
			{
				Role role = subNode.getRole();  // may be null
				
				Row row;
				synchronized(this)
				{
					int rowIndex = getRowIndex(role);
					if (rowIndex < 0) // Create a new Row for the Role and append it as a new Row.
					{
						row = appendNewRow(role)
					}
					else
						row = rows.indexOf(row);
				}
				
				// Place the subNode in that Row.
				placeNodeInRow(subNode, row)
			}
			else  // Check that subNode is in the right Row.
			{
				Row subNodeCurRow = getRowForY(subNode.getY());
				if (row != subNodeCurRow) placeNodeInRow(subNode, row)
			}
			
			subNode.setY(row.getMidpointY() - subNode.getIconHeight()/2.0);
			
			if (! (subNode instanceof ModelContainer)) continue;
			ModelContainer subProc = (ModelContainer)subNode;
			
			if (row.getRole() == null)
			{
				row.setRole(subProc.getRole());
			}
			else
			{
				Role role = getProcRole(subProc);
				if (role != row.getRole())
				{
					setProcRole(subProc, row.getRole());
					
					// Notify clients.
					try { notifyAllListeners(new NodeChangedNotice(process.getNodeId())); }
					catch (Exception ex)
					{
						GlobalConsole.printStackTrace(ex);
					}
				}
			}
		}
		

		// Set location of each Port.
		
		if (node instanceof PortedContainer)
		{
			List<Port> ports = ((PortedContainer)node).getPorts();
			for (Port port : ports) port.setSide(port.getSide());
		}
	}
	
	
	protected Role getProcRole(ModelContainer proc)
	{
		CrossReference cr = subProc.getCrossReference("Performs");
		if (cr == null) return null;
		return (Role)(cr.getFromNode());
	}
	
	
	protected void setProcRole(ModelContainer proc, Role role)
	throws
		ParameterError,
		ModelContainsError // if the 'Performs' NamedReference is not found.
	{
		NamedReference nr = process.getDomain().getNamedReference("Performs");
		if (nr == null) throw new ModelContainsError(
			"Domain '" + process.getDomain().getName() + 
			"' does not contain the Named Reference 'Performs'");
		CrossReference cr = nr.link(role, proc);
	}
	
	
	public synchronized double getRowHeight()
	{
		Attribute attr = process.getAttribute("Row Height");
		if (attr == null) throw new RuntimeException("
			"No Row Height Attribute for '" + process.getFullName() + "'");
		Object rowHeightObj = attr.getDefaultValue();
		if (! (rowHeightObj instanceof Number)) throw new ParameterError(
			"Attribute 'Role Height' is of type " + rowHeightObj.getClass().getName() +
			"; must be compatible with type Number.");
		this.cachedRowHeight = ((Number)rowHeightObj).doubleValue();
		return this.cachedRowHeight;
	}
	
	
	protected void placeNodeInRow(PersistentNode node, Row row)
	{
		node.setY(row.getMidpointY() - node.getHeight()/2.0);
	}
	
	
	protected double getCachedRowHeight() { return this.cachedRowHeight; }
	
	
	/**
	 * Set a new value for the height of each Row, and adjust the value of the
	 * 'Row Height' Attribute as well. Notify clients of the change.
	 */
	 
	public synchronized void modifyRowHeight(double h)
	{
		Attribute attr = process.getAttribute("Row Height");
		if (attr == null) throw new RuntimeException("Attribute 'Row Height' not found");
		attr.setDefaultValue(new Double(h));
		getRowHeight();  // update cached value.
		
		// Notify clients.
		try { notifyAllListeners(new NodeChangedNotice(process.getNodeId())); }
		catch (Exception ex)
		{
			GlobalConsole.printStackTrace(ex);
		}
	}
	
	
	/**
	 * Return the Row that occupies the specified y location. If none, return null.
	 */
	 
	protected Row getRowForY(double y)
	{
		double height = getCachedRowHeight();
		double rowTop = rows.size() * height;
		for (Row row : rows)
		{
			double rowBottom = rowTop - height;
			if ((rowTop <= y) && (y < rowBottom)) return row;
			rowTop = rowBottom;
		}
		
		return null;
	}
	
	
	class Row
	{
		Role role;
		
		Role getRole() { return role; }
		void setRole(Role role) { this.role = role; }
		
		double getBottomY()
		{
			return (rows.size() - rows.indexOf(this) - 1) * getCachedRowHeight()
		}
		
		double getMidpointY() { return getBottomY() + getCachedRowHeight()/2.0; }
	}
}

