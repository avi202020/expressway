!Set up development environment.

!set ozone_home=C:\tools\ozone-1.2
set ANT_HOME=C:\tools\ant\apache-ant-1.5.4
set JAVA_HOME="C:\Progra~1\Java\jdk1.6.0_01"
set APACHE_MATH=C:\tools\commons-math-1.1\commons-math-1.1.jar
set path=%path%;C:\tools\ant\apache-ant-1.5.4\bin
set path=%path%;%JAVA_HOME%\bin
set OZONE_HOME=C:\tools\ozone-1.2
set path=%path%;%OZONE_HOME%\bin
set junit=c:\tools\junit\junit4.4\junit-4.4.jar

set src_dir=..\javacode\expressway
set build_dir=..\build
set deploy_dir="C:\Program Files\OpenLaszlo Server 4.0.2\Server\lps-4.0.2\WEB-INF\classes"
set sp=..\javacode;..\test\javacode
set cp=%build_dir%;%junit%
set cp=%cp%;%APACHE_MATH%
!set cp=%cp%;%ozone_home%\lib\ozoneServer-1.2.jar;%ozone_home%\lib\ozoneODMG-0.1.jar;%ozone_home%\lib\castor.jar

set rcp=%cp%
set javac=javac -sourcepath %sp% -classpath %cp% -d ..\build -s ..\build
!set javac=javac -Xlint -sourcepath %sp% -classpath %cp% -d ..\build -s ..\build


! -----------------------------------------------------------------------------
! Compile source needed for all domains.

%javac% %src_dir%\ModelAPITypes.java
%javac% %src_dir%\Point.java
%javac% %src_dir%\InflectionPoint.java
%javac% %src_dir%\Display.java
%javac% %src_dir%\PersistentNode.java
%javac% %src_dir%\ModelElement.java %src_dir%\DecisionElement.java


! -----------------------------------------------------------------------------
! Compile source needed to demonstrate GUI, i.e., the stub implementation.

%javac% %src_dir%\ModelEngine.java
%javac% %src_dir%\ModelEngineStubImpl.java


! -----------------------------------------------------------------------------
! Deploy code needed for GUI demo.

xcopy /E %build_dir%\* %deploy_dir%


! -----------------------------------------------------------------------------
! Compile source for the model domain.

%javac% %src_dir%\PointImpl.java
%javac% %src_dir%\InflectionPointImpl.java
%javac% %src_dir%\DisplayImpl.java
%javac% %src_dir%\PersistentNodeImpl.java
%javac% %src_dir%\ModelElementImpl.java
%javac% %src_dir%\ActivityImpl.java
%javac% %src_dir%\ConduitImpl.java
%javac% %src_dir%\ConstraintImpl.java
%javac% %src_dir%\EventImpl.java
%javac% %src_dir%\FunctionImpl.java
%javac% %src_dir%\TerminalImpl.java
%javac% %src_dir%\GeneratorImpl.java
%javac% %src_dir%\ObjectiveFunctionImpl.java
%javac% %src_dir%\PortImpl.java
%javac% %src_dir%\StateImpl.java
%javac% %src_dir%\DirectedRelationImpl.java
%javac% %src_dir%\SatisfiesImpl.java
%javac% %src_dir%\DerivesImpl.java
%javac% %src_dir%\ModelDomainImpl.java
%javac% %src_dir%\ActivityBase.java
%javac% %src_dir%\FunctionBase.java


! -----------------------------------------------------------------------------
! Compile source for the decision domain.

%javac% %src_dir%\ModelContainerImpl.java
%javac% %src_dir%\AttributeImpl.java
%javac% %src_dir%\AttributeStateBindingImpl.java
%javac% %src_dir%\ChoiceImpl.java
%javac% %src_dir%\ModelComponentImpl.java
%javac% %src_dir%\ConstantValueImpl.java
%javac% %src_dir%\DecisionFunctionImpl.java
%javac% %src_dir%\DecisionElementImpl.java
%javac% %src_dir%\DecisionImpl.java
%javac% %src_dir%\DecisionModelRelationImpl.java
%javac% %src_dir%\DecisionScenarioImpl.java
%javac% %src_dir%\DependencyImpl.java
%javac% %src_dir%\ModelScenarioImpl.java
%javac% %src_dir%\ParameterAliasImpl.java
%javac% %src_dir%\ParameterImpl.java
%javac% %src_dir%\PersistentNode.java
%javac% %src_dir%\PersistentNodeImpl.java
%javac% %src_dir%\PrecursorRelationshipImpl.java
%javac% %src_dir%\PredefinedEventImpl.java
%javac% %src_dir%\SortedEventSetImpl.java
%javac% %src_dir%\VariableAttributeBindingImpl.java
%javac% %src_dir%\VariableImpl.java
%javac% %src_dir%\DecisionElementImpl.java
%javac% %src_dir%\DecisionPointImpl.java
%javac% %src_dir%\DecisionDomainImpl.java
%javac% %src_dir%\ModelContextImpl.java
%javac% %src_dir%\DecisionActivityContextImpl.java
%javac% %src_dir%\DecisionBase.java


! -----------------------------------------------------------------------------
! Compile source for the model engine implementation.

%javac% %src_dir%\ModelEnginePojoImpl.java
!%javac% %src_dir%\ModelEngineSerImpl.java
!%javac% %src_dir%\ModelEngineOzoneImpl.java


! -----------------------------------------------------------------------------
! Compile source for the test suite.

!%javac% ..\test\javacode\test\Test.java
%javac% ..\test\javacode\test\Test2.java


! -----------------------------------------------------------------------------
! Generate documentation.

javadoc -private -splitindex -d ..\javadoc -classpath %cp% -sourcepath %sp% expressway


! -----------------------------------------------------------------------------
! Deploy code needed for tests.

xcopy /E %build_dir%\* %deploy_dir%


! -----------------------------------------------------------------------------
! Execute the test suite.

!java -classpath %rcp% test.Test2
java -classpath %rcp% org.junit.runner.JUnitCore test.Test2 > output.txt
