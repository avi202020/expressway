package marriott;


import expressway.ModelElement.*;
import expressway.ModelAPITypes.*;
import expressway.ActivityBase;
import java.util.Set;


public class CumLoss extends ActivityBase
{
	private State totalLossState = null;
	
	
	public void start(ActivityContext context) throws Exception
	{
		super.start(context);
		totalLossState = getActivityContext().getState("Total Security-Related Loss");
	}
	
	
	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);
		
		if (events.size() == 0) return;
		
		
		// Get prior total loss.
		
		double totalLoss = 0.0;
		
		Object o = getActivityContext().getStateValue(totalLossState);
		if (o != null)
		{
			Number dbl = null;
			try { dbl = (Number)o; }
			catch (ClassCastException cce) { throw new ModelContainsError(
				"Total loss expected to be number, but it is not: it is a " +
					o.getClass().getName()); }
			
			totalLoss = dbl.doubleValue();
		}
		
		System.out.println("*CumLoss.respond: epoch=" + getActivityContext().getCurrentSimulationEpoch());
		System.out.println("**CumLoss.respond: prior totalLoss = " + totalLoss);
		
		
		// Accumulate the new losses.
		
		for (Event event : events)
		{		
			Number dd = null;
			try { dd = (Number)(event.getNewValue()); }
			catch (ClassCastException cce) { throw new ModelContainsError(
				"Event value expected to be number, but not."); }
				
			if (dd == null) throw new ModelContainsError(
				"Null value for a loss event: " + 
					event.getClass().getName() + ", " + event.toString());
				
			double loss = dd.doubleValue();
			
			System.out.println("***CumLoss.respond: new loss = " + loss);
			
			totalLoss += loss;
		}
		
		
		// Record the total loss.
			
		System.out.println("****CumLoss.respond: setting state '" +
			totalLossState.getName() + "' to " + totalLoss);
			
		getActivityContext().setState(totalLossState, new Double(totalLoss));
	}
}

