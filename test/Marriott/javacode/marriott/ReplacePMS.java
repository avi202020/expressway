package marriott;


import expressway.ModelElement.*;
import expressway.ModelAPITypes.*;
import expressway.ActivityBase;
import java.util.Set;


public class ReplacePMS extends ActivityBase
{
	private DistributionFactory timeDistFactory = DistributionFactory.newInstance();
	private DistributionFactory valueDistFactory = DistributionFactory.newInstance();

	private ContinuousDistribution timeDistribution = 
			timeDistFactory.createGammaDistribution(
				....shape, ....scale);

	private ContinuousDistribution valueDistribution
			valueDistFactory.createGammaDistribution(
				....shape, ....scale);
	
	private int nComponentsBuilt = 0;

	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		double timeDelaySec = 0;
		double newValue = 0;

		try
		{
			timeDelaySec = getTimeDistribution().inverseCumulativeProbability(Math.random());
			newValue = getValueDistribution().inverseCumulativeProbability(Math.random());
		}
		catch (MathException ex)
		{
			throw new ModelContainsError(
				"When computing inverse cumulative probability;", ex);
		}

		long timeDelayMs = (long)(timeDelaySec * 1000);

		GeneratedEvent event = null;
		try
		{
			event = getActivityContext().scheduleDelayedStateChange(
					this.state, timeDelayMs, new Double(newValue));
		}
		catch (ParameterError ex)
		{
			throw new ModelContainsError(
				"When scheduling delayed state change", ex);
		}
	}
}

