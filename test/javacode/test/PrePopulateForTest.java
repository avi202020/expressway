package test;


public class PrePopulateForTest
{

	/*
	 * Declare variables needed for pre-population and testing.
	 */

	public static String vStateName = "InternalThroughputState";
	public ModelDomain modelDomain = null;
	public ModelScenario currentModelScenario = null;
	public Function v = null;
	public Activity source = null;
	public Activity sink = null;
	public State vState = null;
	public static State sourceState = null;
	public Port sourcePort = null;
	public Port vSourcePort = null;
	public Port vSinkPort = null;
	public Port sinkPort = null;
	public Attribute sourceAttribute = null;
	public State sinkState = null;

	public ModelDomain designDomain = null;
	public ModelScenario currentDesignScenario = null;
	public Attribute throughputAttr = null;
	public Activity a = null;
	public Attribute aIsPresentAttr = null;
	public Activity b = null;
	public Attribute bIsPresentAttr = null;
	public AttributeStateBinding attrStateBinding = null;

	public DecisionDomain decisionDomain = null;
	public DecisionScenario currentDecisionScenario = null;
	public DecisionPoint dpA = null;
	public DecisionPoint dpB = null;
	public Variable inputVarA = null;
	public VariableAttributeBinding bThr = null;
	public Variable outputVarA = null;
	public VariableAttributeBinding bA = null;
	public Dependency dep = null;
	public DecisionPoint dpNUsers = null;
	public Variable outputVarNUsers = null;
	public VariableAttributeBinding nUsersAttrBinding = null;

	public ModelDomain eventsDomain = null;
	public ModelScenario currentEventsScenario = null;
	public Generator lobGenerator = null;
	public Generator marketplaceGenerator = null;
	public Generator productionGenerator = null;
	public Activity capabilityCreator = null;
	public Generator serviceGenerator = null;
	public Generator maintenanceGenerator = null;
	public Generator altInvGenerator = null;

	public Terminal newProductEventsTerminal = null;
	public Terminal marketplaceEventsTerminal = null;
	public Terminal productionEventsTerminal = null;
	public Terminal capabilityCreationTerminal = null;
	public Terminal serviceEventsTerminal = null;
	public Terminal maintenanceEventsTerminal = null;
	public Terminal altInvestEventsTerminal = null;
	
	public Attribute capCreDistAttr = null;
	public static String capCreAttrName = "Value Distribution Parameter";
	public static String capabilityCreatorStateName = "CapCreState";


	/**
	 * Pre-populate with in-memory models for testing and demonstration.
	 */

	public void prepopulate()
	{
		try
		{
			/* ----------------------------------------------------------------
			 *	Create a performance model that has a state variable that
			 *	defines the value of some decision variable, say "throughput".
			 */

			// Instantiate a Domain to contain the performance model.
			modelDomain =
				createModelDomain("Performance_Domain");

				modelDomain.setWidth(300);
				modelDomain.setHeight(300);
				modelDomain.setX(10);
				modelDomain.setY(10);

			// Create a ModelScenario for containing Events.
			//currentModelScenario =
			//	modelDomain.createScenario("Model_Scenario");

			//modelDomain.setCurrentModelScenario(currentModelScenario);
			//currentModelScenario.setIterationLimit(5);

			// Instantiate a value-creation Function.
			v = modelDomain.createFunction("Function_V", new FunctionV());

				v.setWidth(100);
				v.setHeight(60);
				v.setX(150);
				v.setY(100);

			// Create Ports for the Function.
			vSourcePort = v.createPort("Port_V_NUsrs");

				vSourcePort.setX(50);
				vSourcePort.setY(60);

			vSinkPort = v.createPort("Port_V_Tput");

				vSinkPort.setX(50);
				vSinkPort.setY(0);

			// Create an internal State for the Function.
			vState = v.createState(vStateName);

			// Connect the Function's internal State to its output Port.
			vState.bindPort(vSinkPort);

			// Instantiate an Activity to represent an input source.
			source = modelDomain.createActivity("Activ_Source", new Source());

				source.setWidth(100);
				source.setHeight(60);
				source.setX(150);
				source.setY(180);

			// Instantiate an Activity to represent an output sink.
			sink = modelDomain.createActivity("Activ_Sink", new Sink());

				sink.setWidth(100);
				sink.setHeight(60);
				sink.setX(150);
				sink.setY(20);

			// Create an internal State for the Source Activity.
			sourceState = source.createState("SourceState");

			// Define a 'startup' event for the Source State. The value of the
			// event does not matter.
			sourceState.predefineEvent(null, new Double(1));

			// Create ports for Activities.
			sourcePort = source.createPort("Port_Source_NUsrs");

				sourcePort.setX(50);
				sourcePort.setY(0);

			sinkPort = sink.createPort("Port_Sink_Tput");

				sinkPort.setX(50);
				sinkPort.setY(60);

			// Bind the Source State to the Source Port.
			sourceState.bindPort(sourcePort);

			// Connect the input source to the Function via a Conduit.
			Conduit sourceToFunction =
				modelDomain.createConduit("Cond_NUsrs", sourcePort, vSourcePort);

			// Connect the Function to the output sink via a Conduit.
			Conduit functionToSink =
				modelDomain.createConduit("Cond_Tput", vSinkPort, sinkPort);

			// Add a Double Attribute called "NUsers" to the input source Activity.
			sourceAttribute = source.createAttribute("Attr_NUsers", new Double(10.0));

			// Add a Double State called "State_Sink_Tput" to the output sink.
			sinkState = sink.createState("State_Sink_Tput");

			modelDomain.dump();


			/* ----------------------------------------------------------------
			 *	Create a design model that contains two components, each having an
			 *	attribute representing the number of instances of that component.
			 */

			// Instantiate a Domain to contain the design model.
			designDomain =
				createModelDomain("Design_Domain");

				designDomain.setWidth(300);
				designDomain.setHeight(300);
				designDomain.setX(10);
				designDomain.setY(10);

			// Create a ModelScenario for containing Events.
			//currentDesignScenario =
			//	designDomain.createScenario("Design_Scenario");

			//	currentDesignScenario.setWidth(100);
			//	currentDesignScenario.setHeight(60);
			//	currentDesignScenario.setX(150);
			//	currentDesignScenario.setY(180);

			//designDomain.setCurrentModelScenario(currentDesignScenario);
			//currentDesignScenario.setIterationLimit(5);

			// Add an Attribute to the domain, to represent the "throughput".
			throughputAttr = designDomain.createAttribute("Attr_DesDom_Tput", null);

				throughputAttr.setWidth(100);
				throughputAttr.setHeight(60);
				throughputAttr.setX(30);
				throughputAttr.setY(180);

			// Bind the "throughput" Attribute to the "Tput" State in
			// the performance model Domain.
			attrStateBinding = throughputAttr.bindToState(sinkState);

			// Instantiate Activity A.
			a = designDomain.createActivity("Activ_A", new ActivityA());

				a.setWidth(100);
				a.setHeight(60);
				a.setX(150);
				a.setY(100);

			// Add an isPresent attribte to Activity A, with a default
			// value of false.
			aIsPresentAttr = a.createAttribute("Attr_IsPresent",
				new Boolean(false));

				aIsPresentAttr.setWidth(80);
				aIsPresentAttr.setHeight(20);
				aIsPresentAttr.setX(160);
				aIsPresentAttr.setY(110);

			// Instantiate Activity B.
			b = designDomain.createActivity("Activ_B", new ActivityB());

				b.setWidth(100);
				b.setHeight(60);
				b.setX(150);
				b.setY(20);

			// Add an isPresent attribute to Activity B, with a default
			// value of false.
			bIsPresentAttr = b.createAttribute("Attr_IsPresent",
				new Boolean(true));

				bIsPresentAttr.setWidth(80);
				bIsPresentAttr.setHeight(20);
				bIsPresentAttr.setX(160);
				bIsPresentAttr.setY(30);

			designDomain.dump();


			/* ----------------------------------------------------------------
			 *	Create a decision tree that depends on the business model's
			 *	arbitrary attribute.
			 */

			// Instantiate a Domain to contain the decision model.
			decisionDomain =
				createDecisionDomain("Decision_Domain");

				decisionDomain.setWidth(370);
				decisionDomain.setHeight(300);
				decisionDomain.setX(10);
				decisionDomain.setY(10);

			// Create a DecisionScenario for containing Decisions.
			//currentDecisionScenario =
			//	decisionDomain.createScenario("Decision Scenario");
			//decisionDomain.setCurrentDecisionScenario(currentDecisionScenario);

			// Instantiate a DecisionPoint A to represent the decision about the
			// value of Activity A isPresent.
			dpA = decisionDomain.createDecisionPoint("DecisPt_A", new DecisionPointA());

				dpA.setWidth(210);
				dpA.setHeight(80);
				dpA.setX(150);
				dpA.setY(100);

			// Instantiate a DecisionPoint B to represent the decision about the
			// value of Activity B isPresent.
			dpB = decisionDomain.createDecisionPoint("DecisPt_B");

				dpB.setWidth(210);
				dpB.setHeight(60);
				dpB.setX(150);
				dpB.setY(20);

			// Add an input Variable to DecisionPoint A.
			inputVarA = dpA.createVariable("Var_DpA_Thr");

				inputVarA.setWidth(135);
				inputVarA.setHeight(25);
				inputVarA.setX(60);
				inputVarA.setY(10);


			// Bind the input Variable to the deisgn domain's Throughput Attribute.
			bThr = inputVarA.bindToAttribute(throughputAttr);

			if (throughputAttr.getParameterBinding() == null) throw new RuntimeException(
				"ParemeterBinding is null");
			System.out.println("******Bound Attribute " + throughputAttr.getName() +
				" to Variable " + throughputAttr.getParameterBinding().getVariable().getName());


			// Add an output Variable to DecisionPoint A.
			outputVarA = dpA.createVariable("DpA_ActivAisPresent");

				outputVarA.setWidth(135);
				outputVarA.setHeight(25);
				outputVarA.setX(60);
				outputVarA.setY(40);

			// Bind the output Variable to Activity A's isPresent Attribute.
			bA = outputVarA.bindToAttribute(aIsPresentAttr);

			// Insert a Precludes Dependency between two decision points that each
			// impact a structural model parameter.
			dep = decisionDomain.createPrecludes(dpA, dpB);

			// Instantiate a DecisionPoint to represent the decision about the
			// value of the model domain's NUsers attribute.
			dpNUsers = decisionDomain.createDecisionPoint("DecPt_NUsers");

				dpNUsers.setWidth(210);
				dpNUsers.setHeight(60);
				dpNUsers.setX(150);
				dpNUsers.setY(200);

			// Add an output Variable to the DeicisonPoint.
			outputVarNUsers = dpNUsers.createVariable("Var_DpNUsers_NUsers");

				outputVarNUsers.setWidth(135);
				outputVarNUsers.setHeight(25);
				outputVarNUsers.setX(60);
				outputVarNUsers.setY(10);

			// Bind the outputVarNUsers Variable to the Attr_NUsers Attribute.
			nUsersAttrBinding = outputVarNUsers.bindToAttribute(sourceAttribute);


			decisionDomain.dump();



			/* ----------------------------------------------------------------
			 *	Create a future events model.
			 */

			eventsDomain =
				createModelDomain("Events_Domain");

				eventsDomain.setWidth(800);
				eventsDomain.setHeight(700);
				eventsDomain.setX(10);
				eventsDomain.setY(10);
				System.out.println("A");

			// Create a ModelScenario for containing Events.
			//currentEventsScenario =
			//	eventsDomain.createScenario("Design_Scenario");
			//System.out.println("A.a");

			//eventsDomain.setCurrentModelScenario(currentEventsScenario);
			//System.out.println("Ab");
			//currentEventsScenario.setIterationLimit(5);
			//System.out.println("A.c");

			// Instantiate LOB Activity (the "launch new product" event generator).

				DistributionFactory distFactory = DistributionFactory.newInstance();
				System.out.println("A.d");
				// See http://mathworld.wolfram.com/GammaDistribution.html
				System.out.println("A.1");

				lobGenerator = eventsDomain.createGenerator("This LOB",
					1.0, 2.0, 1.0, 2.0);

				lobGenerator.setWidth(150);
				lobGenerator.setHeight(60);
				lobGenerator.setX(100);
				lobGenerator.setY(610);
				System.out.println("A.2");

				Port lobGenPort = lobGenerator.getOutputPort();
				lobGenPort.setX(150);
				lobGenPort.setY(30);

				Port lobGenMaintPort = lobGenerator.getInputPort();
				lobGenMaintPort.setX(0);
				lobGenMaintPort.setY(30);
				System.out.println("A.3");

				newProductEventsTerminal = eventsDomain.createTerminal("New Prods");

				newProductEventsTerminal.setX(420);
				newProductEventsTerminal.setY(640);
				newProductEventsTerminal.setAttention();
				System.out.println("A.4");

				Conduit evCon1 = eventsDomain.createConduit("Launch New Product",
					lobGenPort, newProductEventsTerminal.getPort());
				System.out.println("B");


			// Instantiate Marketplace Activity.

				marketplaceGenerator = eventsDomain.createGenerator("Marketplace",
					1.0, 2.0, 1.0, 2.0);

				marketplaceGenerator.setWidth(150);
				marketplaceGenerator.setHeight(60);
				marketplaceGenerator.setX(100);
				marketplaceGenerator.setY(510);

				Port mktGenPort = marketplaceGenerator.getOutputPort();
				mktGenPort.setX(150);
				mktGenPort.setY(30);

				Port mktGenSvcDmndPort = marketplaceGenerator.getInputPort();
				mktGenSvcDmndPort.setX(0);
				mktGenSvcDmndPort.setY(30);

				marketplaceEventsTerminal = eventsDomain.createTerminal("Market Dmnd");

				marketplaceEventsTerminal.setX(420);
				marketplaceEventsTerminal.setY(540);
				marketplaceEventsTerminal.setAttention();

				Conduit evCon2 = eventsDomain.createConduit("Change In Demand",
					mktGenPort, marketplaceEventsTerminal.getPort());
				System.out.println("C");


			// Instantiate Production Activity.

				//productionGenerator = eventsDomain.createGenerator("Production",
				//	ProductionGenerator.getClass());
				productionGenerator = eventsDomain.createGenerator("Production",
					1.0, 2.0, 1.0, 2.0);

				productionGenerator.setWidth(150);
				productionGenerator.setHeight(60);
				productionGenerator.setX(100);
				productionGenerator.setY(410);

				Port prodGenPort = productionGenerator.getOutputPort();
				prodGenPort.setX(150);
				prodGenPort.setY(30);

				Port prodGenCostPort = productionGenerator.getInputPort();
				prodGenCostPort.setX(0);
				prodGenCostPort.setY(30);

				//Port prodGenDmndPort = productionGenerator.getDemandPort();
				Port prodGenDmndPort = productionGenerator.createPort("Demand");
				prodGenDmndPort.setX(75);
				prodGenDmndPort.setY(60);

				productionEventsTerminal = eventsDomain.createTerminal("Prod");

				productionEventsTerminal.setX(420);
				productionEventsTerminal.setY(440);

				Conduit evCon3 = eventsDomain.createConduit("Change In Production Level",
					prodGenPort, productionEventsTerminal.getPort());

				Conduit evCon3a = eventsDomain.createConduit("Change In Demand a",
					marketplaceEventsTerminal.getPort(), prodGenDmndPort);

				InflectionPoint[] evCon3aInflPts = new InflectionPoint[] {
					new InflectionPointImpl(420, 490),
					new InflectionPointImpl(175, 490)
				};

				evCon3a.setInflectionPoints(evCon3aInflPts);
				System.out.println("D");


			// Instantiate Capability Creation Activity.

				capabilityCreator = eventsDomain.createActivity("Capability_Creation",
					new CapabilityCreator());

				capabilityCreator.setWidth(150);
				capabilityCreator.setHeight(60);
				capabilityCreator.setX(100);
				capabilityCreator.setY(310);

				Port capGenPort = 
					capabilityCreator.createPort("OutputPort");
					
				capGenPort.setX(150);
				capGenPort.setY(30);

				// Create internal state to hold the function's output value.
				
				State capCreState = 
					capabilityCreator.createState(capabilityCreatorStateName);
				
				// Bind the state to the output port.
				
				capCreState.bindPort(capGenPort);

				// Create a Terminal. This is really not needed, but is for
				// symmetry, since the example uses Terminals to contain
				// all outputs.
				
				capabilityCreationTerminal = eventsDomain.createTerminal("Capab Cre");

				capabilityCreationTerminal.setX(420);
				capabilityCreationTerminal.setY(340);
				capabilityCreationTerminal.setUrgent();
				
				Conduit evCon4 = eventsDomain.createConduit("Change In Costs",
					capGenPort, capabilityCreationTerminal.getPort());

				Conduit evCon4a = eventsDomain.createConduit("Change In Costs a",
					capabilityCreationTerminal.getPort(), prodGenCostPort);

				InflectionPoint[] evCon4aInflPts = new InflectionPoint[] {
					new InflectionPointImpl(420, 390),
					new InflectionPointImpl(70, 390),
					new InflectionPointImpl(70, 440)
				};

				evCon4a.setInflectionPoints(evCon4aInflPts);

				// Create an Attribute to represent the distribution parameter for
				// the Capability Creation Activity's value distribution.

				capCreDistAttr = capabilityCreator.createAttribute(
					capCreAttrName, Boolean.FALSE);

					capCreDistAttr.setWidth(80);
					capCreDistAttr.setHeight(20);
					capCreDistAttr.setX(10);
					capCreDistAttr.setY(10);

				// Bind the DecisionModel DecisionPointA's output Variable to
				// the InitialInvestment Attribute in the Event model.
				outputVarA.bindToAttribute(capCreDistAttr);
				System.out.println("E");
				
				// Create a Predefined Event that propagates the value of the
				// Attribute to the output Port.
				
				//capCreState.predefineEvent(null, capCreDistAttr);


			// Instantiate Service Activity.

				serviceGenerator = eventsDomain.createGenerator("Service",
					1.0, 2.0, 1.0, 2.0);

				serviceGenerator.setWidth(150);
				serviceGenerator.setHeight(60);
				serviceGenerator.setX(100);
				serviceGenerator.setY(210);

				Port svcGenPort = serviceGenerator.getOutputPort();
				svcGenPort.setX(150);
				svcGenPort.setY(30);

				serviceEventsTerminal = eventsDomain.createTerminal("Svc");

				serviceEventsTerminal.setX(420);
				serviceEventsTerminal.setY(240);
				serviceEventsTerminal.setUrgent();

				Conduit evCon5 = eventsDomain.createConduit("Change In Service Failure Level",
					svcGenPort, serviceEventsTerminal.getPort());

				Conduit evCon5a = eventsDomain.createConduit("Change In Service Failure Level a",
					serviceEventsTerminal.getPort(), mktGenSvcDmndPort);

				InflectionPoint[] evCon5aInflPts = new InflectionPoint[] {
					new InflectionPointImpl(420, 290),
					new InflectionPointImpl(50, 290),
					new InflectionPointImpl(50, 540)
				};

				evCon5a.setInflectionPoints(evCon5aInflPts);
				System.out.println("F");


			// Instantiate Maintenance Activity.

				maintenanceGenerator = eventsDomain.createGenerator("Maintenance",
					1.0, 2.0, 1.0, 2.0);

				maintenanceGenerator.setWidth(150);
				maintenanceGenerator.setHeight(60);
				maintenanceGenerator.setX(100);
				maintenanceGenerator.setY(110);

				Port maintGenPort = maintenanceGenerator.getOutputPort();
				maintGenPort.setX(150);
				maintGenPort.setY(30);

				maintenanceEventsTerminal = eventsDomain.createTerminal("Maint");

				maintenanceEventsTerminal.setX(420);
				maintenanceEventsTerminal.setY(140);
				maintenanceEventsTerminal.setUrgent();

				Conduit evCon6 = eventsDomain.createConduit("Change In Maintainability Level",
					maintGenPort, maintenanceEventsTerminal.getPort());

				Conduit evCon6a = eventsDomain.createConduit("Change In Maintainability Level a",
					maintenanceEventsTerminal.getPort(), lobGenMaintPort);

				InflectionPoint[] evCon6aInflPts = new InflectionPoint[] {
					new InflectionPointImpl(420, 100),
					new InflectionPointImpl(30, 100),
					new InflectionPointImpl(30, 640)
				};

				evCon6a.setInflectionPoints(evCon6aInflPts);
				System.out.println("G");


			// Instantiate Alternative Investments Activity.

				altInvGenerator = eventsDomain.createGenerator("Alternative Investments",
					1.0, 2.0, 1.0, 2.0);

				altInvGenerator.setWidth(150);
				altInvGenerator.setHeight(60);
				altInvGenerator.setX(100);
				altInvGenerator.setY(10);

				Port annuGenPort = altInvGenerator.getOutputPort();
				annuGenPort.setX(150);
				annuGenPort.setY(30);

				Port annuGenCostPort = altInvGenerator.getInputPort();
				annuGenCostPort.setX(75);
				annuGenCostPort.setY(60);

				altInvestEventsTerminal = eventsDomain.createTerminal("Alt Invest");

				altInvestEventsTerminal.setX(420);
				altInvestEventsTerminal.setY(40);

				Conduit evCon7 = eventsDomain.createConduit("Change In Annuities",
					annuGenPort, altInvestEventsTerminal.getPort());

				Conduit evCon4b = eventsDomain.createConduit("Change In Costs b",
					capabilityCreationTerminal.getPort(), annuGenCostPort);

				InflectionPoint[] evCon4bInflPts = new InflectionPoint[] {
					new InflectionPointImpl(480, 340),
					new InflectionPointImpl(480, 80),
					new InflectionPointImpl(175, 80)
				};

				evCon4b.setInflectionPoints(evCon4bInflPts);
				System.out.println("H");


			eventsDomain.dump();

			System.out.println("Pre-population complete.");
		}
		catch (Throwable ex)
		{
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}


class Source extends ActivityBase
{
	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		String attributeQualifiedName =
			"Performance_Domain.Activ_Source.Attr_NUsers";

		Object attrValue = null;
		try
		{
			attrValue =
				getActivityContext().getAttributeValue(attributeQualifiedName);
		}
		catch (ParameterError er)
		{
			throw new ModelContainsError(er.getMessage(), er);
		}

		if (attrValue == null) throw new ModelContainsError(
			"Unable to locate Attribute " + attributeQualifiedName);

		Double attrDoubleValue = null;

		if (attrValue instanceof Double)
			attrDoubleValue = (Double)attrValue;
		else if (attrValue instanceof Number)
			attrDoubleValue = new Double(((Number)attrValue).doubleValue());
		else
		{
			throw new ModelContainsError(
				"Attribute " + attributeQualifiedName +
				" is not a Double value: it is a " + attrValue.getClass().getName());
		}


		Double noOfUsers = attrDoubleValue;
		System.out.println("noOfUsers=" + noOfUsers);

		State sourceState = getActivityContext().getState("SourceState");
		if (sourceState == null) throw new ModelContainsError(
			"Cannot locate State 'SourceState'");

		System.out.println("SourceState found");

		GeneratedEvent event =
			getActivityContext().setState(sourceState, noOfUsers);
		System.out.println("Generated event " + event.toString());
	}
}


class FunctionV extends FunctionBase
{
	State state = null;
	String stateName = ModelEnginePojoImpl.vStateName;
	double singleUserReqRate = 0.1;


	public void start(FunctionContext context) throws Exception
	{
		super.start(context);

		state = context.getState(stateName);
	}


	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);


		// Get the value on the Port.
		
		Object inputValue = null;
		
		Port port = getFunctionContext().getPort("Port_V_NUsrs");
		
		Set<Event> eventsOnPort = getEventsOnPort(port, events);
		for (Event event : eventsOnPort)
		{
			if (event.getState() == null) continue;  // Allow for Startup Events.
			
			if ((inputValue != null) && 
					(! inputValue.equals(event.getNewValue())))
				throw new RuntimeException(
					"Model engine permitted conflicting events on Port " +
						port.getName());
						
			inputValue = event.getNewValue();
		}

		if (inputValue == null) return; // don't drive output.

		
		Double inputDouble = null;

		if (inputValue instanceof Double)
			inputDouble = (Double)inputValue;
		else if (inputValue instanceof Number)
			inputDouble = new Double(((Number)inputValue).doubleValue());
		else
			throw new ModelContainsError(
				"Input to FunctionV is not a Double value: it is a " +
					inputValue.getClass().getName());

		Double noOfUsers = inputDouble;

		// Throughput = (no. of users) X (0.1 requests/sec.)
		Double throughput = new Double(noOfUsers.doubleValue() * singleUserReqRate);

		GeneratedEvent event =
			getFunctionContext().setState(state, throughput);
	}
}


class Sink extends ActivityBase
{
	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);


		// Get the value on the Port.
		
		Object inputValue = null;
		
		Port port = getActivityContext().getPort("Port_Sink_Tput");
		
		Set<Event> eventsOnPort = getEventsOnPort(port, events);
		for (Event event : eventsOnPort)
		{
			if (event.getState() == null) continue;  // Allow for Startup Events.
			
			if ((inputValue != null) && 
					(! inputValue.equals(event.getNewValue())))
				throw new RuntimeException(
					"Model engine permitted conflicting events on Port " +
						port.getName());
						
			inputValue = event.getNewValue();
		}

		if (inputValue == null)
			throw new ModelContainsError("Input to TerminalNativeImpl is null.");


		State sinkState = getActivityContext().getState("State_Sink_Tput");
		if (sinkState == null) throw new ModelContainsError(
			"Cannot locate State 'State_Sink_Tput'");

		if (! (inputValue instanceof Double)) throw new ModelContainsError(
			"Input event for Sink should be Double.");

		Double throughput = (Double)inputValue;

		GeneratedEvent event =
			getActivityContext().setState(sinkState, throughput);
	}
}


class CapabilityCreator extends ActivityBase
{
	State state = null;
	String stateName = ModelEnginePojoImpl.capabilityCreatorStateName;


	public void start(ActivityContext context) throws Exception
	{
		super.start(context);

		state = context.getState(stateName);
	}


	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		super.respond(events);

		System.out.println("CapabilityCreator.respond: A");
		
		String attributeQualifiedName =
			"Events_Domain.Capability_Creation." + ModelEnginePojoImpl.capCreAttrName;

		Object attrValue = null;
		try
		{
			attrValue =
				getActivityContext().getAttributeValue(attributeQualifiedName);
		}
		catch (ParameterError er)
		{
			throw new ModelContainsError(er.getMessage(), er);
		}

		System.out.println("CapabilityCreator.respond: B");
		
		if (attrValue == null) throw new ModelContainsError(
			"Unable to locate Attribute " + attributeQualifiedName);

		System.out.println("CapabilityCreator.respond: C");
		

		// Define the output to be the sum of investment is A and B.
		
		Double totalInvestment = 1000000.0;
		
		if (attrValue.equals(Boolean.TRUE)) totalInvestment += 300000.0;
		
		System.out.println("CapabilityCreator.respond: totalInvestment=" +
			totalInvestment);
		
		System.out.println("ooooo> Generating Event for State " + 
			state.getName() + ": new value=" + totalInvestment.toString());
			
		GeneratedEvent event =
			getActivityContext().setState(state, totalInvestment);

		System.out.println("CapabilityCreator.respond: exiting.");
	}
}




class ActivityA extends ActivityBase
{
	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		throw new ModelContainsError(
			"Unexpected invocation of ActivityA");
	}
}


class ActivityB extends ActivityBase
{
	public void respond(Set<Event> events)
	throws
		ModelContainsError
	{
		throw new ModelContainsError(
			"Unexpected invocation of ActivityB");
	}
}


class DecisionPointA extends DecisionBase
{
	public Set<Decision> evaluate(DecisionPoint decisionPoint, DecisionScenario scenario)
	throws
		ModelContainsError
	{
		/*
		 * If Throughput > 5, create a Decision that specifies True
		 * for Variable 'DpA_ActivAisPresent' (outputVarA).
		 */

		// Retrieve throughput Variable 'Var_DpA_Thr' (inputVarA).

		Variable thrVariable = null;
		try
		{
			thrVariable = decisionPoint.getVariable("Var_DpA_Thr");
		}
		catch (ParameterError ex)
		{
			throw new ModelContainsError("Attempting to get Variable Var_DpA_Thr", ex);
		}

		Object thrValue = getDecisionContext().getBoundAttributeValue(thrVariable);
		if (thrValue == null) throw new ModelContainsError(
			"No value found for Variable 'Var_DpA_Thr'");

		if (! (thrValue instanceof Double)) throw new ModelContainsError(
			"Value of Var_DpA_Thr is not Double: it is " + thrValue.getClass().getName());

		Double throughput = (Double)thrValue;

		System.out.print(
			"------throughput=" + throughput.doubleValue() + "; ");

		if (throughput.doubleValue() <= 5.0)
		{
			System.out.println(
				"no Decision will be created, using defaults for A.IsPresent");
			return null;  // no Decision created: defaults should be used.
		}
		else
			System.out.println(
				"a Decision will be created for DecisionPointA");


		// Retrieve Variable 'DpA_ActivAisPresent'.

		Variable isPresVariable = null;

		try
		{
			isPresVariable = decisionPoint.getVariable("DpA_ActivAisPresent");
		}
		catch (ParameterError ex)
		{
			throw new ModelContainsError(
				"Error while trying to locate Variable DpA_ActivAisPresent", ex);
		}

		if (isPresVariable == null) throw new ModelContainsError(
			"Cannot locate Variable 'DpA_ActivAisPresent'");


		// Retrieve the DecisionPoint that owns this native implementation.

		if (! (getDecisionElement() instanceof DecisionPoint)) throw new RuntimeException(
			"Native implementation for a DecisionPoint is not owned by a DecisionPoint: " +
				"it is owned by a " + getDecisionElement().getClass().getName());

		DecisionPoint dp = (DecisionPoint)(getDecisionElement());


		// Create a Decision for Variable 'DpA_ActivAisPresent'.

		Decision decision = null;
		try
		{
			decision = scenario.createDecision(isPresVariable, new Boolean(true), dp);
		}
		catch (CloneNotSupportedException ex)
		{
			throw new ModelContainsError(ex);
		}


		// Construct a Set to contain the Decision and return the Set.

		Set<Decision> decisions = new TreeSet<Decision>();
		decisions.add(decision);
		return decisions;
	}
}

