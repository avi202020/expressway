package test;

import expressway.ModelAPITypes.*;
import expressway.ModelElement.*;
import expressway.DecisionElement.*;
import expressway.DecisionElement;
import expressway.ModelEngine;
import expressway.ModelEnginePojoImpl;
import java.util.Set;
import java.util.TreeSet;
import org.junit.*;
import static org.junit.Assert.*;
import expressway.EventImpl;
import java.util.Vector;
import java.util.List;
import java.util.SortedSet;


/**
 * Test the ModelEngine Pojo implementation.
 *
 * Instantiate a ModelEngine, and create a set of models.
 * Then exercise those models.
 */

public class Test2
{
	static ModelEnginePojoImpl modelEngine = null;


	@BeforeClass
	public static void setUp()
	{
		try
		{
			// Instantiate ModelEngine.

			modelEngine = new ModelEnginePojoImpl();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}


	@Test
	public void Test_getDecisionPoint()
	{
		try
		{
			String decisionPointName = "DecPt_NUsers";
			DecisionPoint decisionPoint =
					modelEngine.decisionDomain.getDecisionPoint(decisionPointName);
			assertTrue(decisionPoint != null);
		}
		catch (ParameterError ex) { ex.printStackTrace(); }
	}


	@Test
	public void Test_getVariable()
	{
		try
		{
			String variableName = "Var_DpNUsers_NUsers";
			Parameter parameter = modelEngine.dpNUsers.getVariable(variableName);
			assertTrue(parameter != null);
		}
		catch (ParameterError ex) { ex.printStackTrace(); }
	}


	@Test
	public void Test_UpdateVariable()
	{
		try
		{
			/* ----------------------------------------------------------------
			 *	Evaluate the decision domain.
			 */

			// Check that the predefined events exist.
			SortedSet<Event> sourcePredefEvents = modelEngine.sourceState.getPredefinedEvents();
			assertTrue(sourcePredefEvents.size() == 1);
			Event e = sourcePredefEvents.first();
			assertTrue(e.getNewValue().equals(new Double(1)));
			System.out.println("*****Predefined event is present: " + e.toString());
			 
			// Update the NUsers Variable, and wait for the simulation to complete.
			Vector v = new Vector();
			v.add(new Integer(101));
			Integer[] handles =
				modelEngine.updateVariable(
					"Decision_Domain", null, "DecPt_NUsers", "Var_DpNUsers_NUsers",
						v, true, false);


			// Wait until the simulation is complete, but no longer than 5 seconds.

			for (int i=0; i<25; i++)
			{
				if (modelEngine.isComplete(handles[0])) break;
				if (modelEngine.isAborted(handles[0])) break;
				Thread.currentThread().sleep(200);
			}

			if (! modelEngine.isComplete(handles[0]))
			{
				System.out.println("Time limit of " + (25.0*200.0/1000.0) +
					" seconds exceeded; aborting");
				modelEngine.abort(handles[0]);
			}


			System.out.println(">>>>Checking Variable bindings");
			for (AttributeStateBinding binding : modelEngine.sinkState.getAttributeBindings())
			{
				Attribute attribute = binding.getAttribute();

				System.out.println("\tAttribute: " + attribute.getName());

				// Identify all Variables that are linked to the changed
				// attribute.

				VariableAttributeBinding vaBinding =
					attribute.getParameterBinding();

				Variable variable = vaBinding.getVariable();

				System.out.println("\t\tVariable: " + variable.getName());
			}


			// Verify that the expected Decision node resulted.

			Decision decision =
				modelEngine.decisionDomain.getCurrentDecisionScenario().getDecision(modelEngine.outputVarA);
			assertTrue("decision is null", decision != null);


			// Veryfy that the design attributes have assumed the expected values.

			DecisionPoint decPoint = decision.getDecisionPoint();
			assertTrue("decPoint is null", decPoint != null);
			assertTrue("decPoint != dpA", decPoint == modelEngine.dpA);

			Object value = decision.getValue();
			assertTrue("decision value is not TRUE", value.equals(Boolean.TRUE));

			Variable decVar = decision.getVariable();
			assertTrue("decVar is null", decVar != null);

			Set<VariableAttributeBinding> varAttrBindings =
				decVar.getAttributeBindings();
			assertTrue("varAttrBindings is null", (varAttrBindings != null));
			assertTrue("varAttrBindings does not contain bA", varAttrBindings.contains(modelEngine.bA));


			/* ----------------------------------------------------------------
			 * Evaluate the Events Domain, since propagation will not automatically
			 * occur.
			 */
			
			System.out.println("Simulating Events domain");
			
			handles = modelEngine.simulate("Events_Domain", null, 0, false);

			// Wait until the simulation is complete, but no longer than 5 seconds.

			for (int i=0; i<25; i++)
			{
				if (modelEngine.isComplete(handles[0])) break;
				Thread.currentThread().sleep(200);
			}

			if (! modelEngine.isComplete(handles[0]))
			{
				System.out.println("Time limit of " + (25.0*200.0/1000.0) +
					" seconds exceeded; aborting");
				modelEngine.abort(handles[0]);
				return;
			}
			
			System.out.println("Simulation of Events domain complete.");
			
			
			// Check that the Capability Creation Activity's Attribute reflects
			// the value from the most recent DecisionScenario.
			
			ModelScenario eventsScenario = 
				modelEngine.eventsDomain.getCurrentModelScenario();
			Object initInvestValue = eventsScenario.getAttributeValue(
				modelEngine.capCreDistAttr);
			assertTrue("Initial investment Attribute not set properly",
				initInvestValue.equals(Boolean.TRUE));
			
			
			// Check that the Change In Costs Terminal reflects the correct value
			// as output by the Capability Creation Activity.
			
			Vector resColVec = new Vector<String>();
			resColVec.add("Events_Domain.Capab Cre.TerminalState");
			List<Number>[] values = modelEngine.getResultColumns(resColVec);
			assertTrue("Too many values in return list", values.length == 1);
			
			int valuesSize = values[0].size();
			System.out.println("Test2: values[0] size == " + valuesSize);
			
			Object valueObject = values[0].get(valuesSize-1);
			System.out.println("valueObject == " + (valueObject == null ? "<null>" : valueObject));
			
			assertTrue("Capability Creation output Terminal has a null value", valueObject != null);
			
			assertTrue("Capability Creation output Terminal has the wrong value: " +
				values[0],
				valueObject.equals(new Double(1300000.0)));


			System.out.println("Success!");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}


	@AfterClass
	public static void tearDown()
	{
	}


	// http://java.sun.com/docs/books/jls/third_edition/html/statements.html#14.10
	/*
	static void assertThat(boolean expr, String msg)
	{
		if (! expr)
		{
			throw new RuntimeException("Test failure: " + msg);
		}
	}
	*/
}
