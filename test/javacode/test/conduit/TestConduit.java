package test.conduit;

import expressway.ModelElement.Port.Position;
import expressway.ModelElement.*;
import expressway.*;
import geometry.*;


/** ****************************************************************************
 * Test that Conduits are arranged properly when placed within a Container.
 */
 
public class TestConduit
{
	public static void main(String[] args)
	throws
		Exception
	{
		/*
		 * Instantiate a ModelEngine.
		 */
		 
		ModelEngineLocal modelEngine = new ModelEngineLocalPojoImpl();
		
		
		/*
		 * Create "test bench" setup, consisting of two Components, each with one
		 * or more Ports on one or more faces, contained within an enclosing Container
		 * that contains one or more Ports.
		 */
		
		ModelDomain modelDomain = modelEngine.createModelDomainPersistentNode("Model Domain");
		modelDomain.setWidth(1000.0);
		modelDomain.setHeight(500.0);
		
		Activity enclosingActivity = modelDomain.createActivity("Enclosing Activity", null);
		enclosingActivity.setWidth(900.0);
		enclosingActivity.setHeight(400.0);
		
		modelDomain.flowComponentsHorizontally(enclosingActivity);
		modelDomain.flowComponentsVertically(enclosingActivity);
		
		System.out.println("Flowed " + enclosingActivity.getFullName() + " to " +
			enclosingActivity.getDisplay().toString());
		
		
		// Add Port A to the Enclosing Activity, to its top face.
		
		Port portA = enclosingActivity.createPort("PortA");
		//portA.setWidth(10.0);
		//portA.setHeight(10.0);
		
		System.out.println("Before setSide(Top): portA x,y = " + portA.getX() + ", " + portA.getY());
		portA.setSide(Position.Top);
		System.out.println("After setSide(Top): portA x,y = " + portA.getX() + ", " + portA.getY());
		System.out.println();
		
		
		// Add two Activities within the Enclosing Activity. Make each 200 on a side,
		// and arranged side-by-side.
		
		ActivityImpl alpha = (ActivityImpl)(enclosingActivity.createActivity("InternalActivityAlpha", null));
		alpha.setWidth(200.0);
		alpha.setHeight(200.0);
		
		Activity beta = enclosingActivity.createActivity("InternalActivityBeta", null);
		beta.setWidth(200.0);
		beta.setHeight(200.0);
		
		enclosingActivity.flowComponentsHorizontally(alpha, beta);
		enclosingActivity.flowComponentsVertically(alpha);
		enclosingActivity.flowComponentsVertically(beta);
		
		System.out.println("Flowed " + alpha.getFullName() + " to " +
			alpha.getDisplay().toString());
		System.out.println("Flowed " + beta.getFullName() + " to " +
			beta.getDisplay().toString());
		
		
		// Add Port PortAlphaA to InternalActivityAlpha and PortBetaA to InternalActivityBeta, resp.
		
		Port portAlphaA = alpha.createPort("PortAlphaA");
		//portAlphaA.setWidth(10.0);
		//portAlphaA.setHeight(10.0);
		
		Port portBetaA = beta.createPort("PortBetaA");
		//portBetaA.setWidth(10.0);
		//portBetaA.setHeight(10.0);
		
		//if (portAlphaA.getContainer() != portBetaA.getContainer()) throw new RuntimeException(
		//	"Test design error: Containers should be equal.");
		
		System.out.println("Before setSide(Right): portAlphaA x,y = " + portAlphaA.getX() + ", " + portAlphaA.getY());
		portAlphaA.setSide(Position.Right);
		
		// Check that Port AlphaA is on the Right side of alpha.
		
		if (! (portAlphaA.getSide() == Position.Right)) throw new Exception(
			"Port AlphaA is not on the Right of Alpha.");
		
		Face[] alphaFaces = alpha.getFaces(0.0, 0.0);  // in alpha's reference frame.
		assertThat(alphaFaces.length == 4);
		Rectangle portAlphaARectangle = portAlphaA.getRectangle();  // in alpha's reference frame.
		System.out.println("portAlphaARectangle=" + portAlphaARectangle.toString());
		assertThat(alphaFaces[1].intersects(portAlphaARectangle));
		
		
		System.out.println("Before setSide(Right): portBetaA x,y = " + portBetaA.getX() + ", " + portBetaA.getY());
		portBetaA.setSide(Position.Left);
		System.out.println("After setSide(Right): portBetaA x,y = " + portBetaA.getX() + ", " + portBetaA.getY());
		System.out.println();
		if (portBetaA.getSide() != Position.Left) throw new Exception(
			"PortBetaA is not on the Left side.");
		
		
		// Create a Conduit between PortAlphaA and PortBetaA.
		
		Conduit conduit = enclosingActivity.createConduit("Conduit", portAlphaA, portBetaA);  //<<<<<<<<<<<<<<<<<<
		
		
		/*
		 * Test that case 1 works.
		 */
		
		{
			int testNo = 1;
			int noOfSegments = getConduitSegmentNumber(conduit);
			if (noOfSegments != 1)
			{
				System.out.println("Test case " + testNo + " failed:");
				System.out.println(portAlphaA);
				System.out.println(portBetaA);
				System.out.println(conduit);
				throw new Exception(
					"Test " + testNo + ": Conduit has " + noOfSegments + 
					" segments; expected " + 1);				
			}
			
			// Check that segment is horizontal.
			
			try { checkSegmentSlopes(conduit); }
			catch (Exception ex)
			{
				System.out.println("Test case " + testNo + " failed: " + ex.getMessage());
				return;
			}
			
			System.out.println("Test case " + testNo + " (" + noOfSegments + 
				") passed: " + conduit.toString());
		}
		
		
		/*
		 * Test that case 4 works.
		 */
		
		{
			int testNo = 2;
			portAlphaA.setSide(Position.Bottom);
			int noOfSegments = getConduitSegmentNumber(conduit);
			if (noOfSegments != 4)
			{
				System.out.println("Test case " + testNo + " failed:");
				System.out.println(portAlphaA);
				System.out.println(portBetaA);
				System.out.println(conduit);
				throw new Exception(
					"Test " + testNo + ": Conduit has " + noOfSegments + 
					" segments; expected " + 4);				
			}
			
			try { checkSegmentSlopes(conduit); }
			catch (Exception ex)
			{
				System.out.println("Test case " + testNo + " failed: " + ex.getMessage());
				return;
			}
			
			System.out.println("Test case " + testNo + " (" + noOfSegments + 
				") passed: " + conduit.toString());
		}

		
		/*
		 * Test that case 3 works.
		 */
		
		{
			int testNo = 3;
			portBetaA.setSide(Position.Bottom);
			int noOfSegments = getConduitSegmentNumber(conduit);
			if (noOfSegments != 3)
			{
				System.out.println("Test case " + testNo + " failed:");
				System.out.println(portAlphaA);
				System.out.println(portBetaA);
				System.out.println(conduit);
				throw new Exception(
					"Test " + testNo + ": Conduit has " + noOfSegments + 
					" segments; expected " + 3);				
			}
			
			try { checkSegmentSlopes(conduit); }
			catch (Exception ex)
			{
				System.out.println("Test case " + testNo + " failed: " + ex.getMessage());
				return;
			}
			
			System.out.println("Test case " + testNo + " (" + noOfSegments + 
				") passed: " + conduit.toString());
		}
		

		/*
		Conduit after reroute: [ Conduit (simple_demo.output_port_to_input_port_9): 
		p1=[ Port: simple_demo.start.output_port: at 0.0, 0.0 ], 
		p2=[ Port: simple_demo.breach events.input_port: at 0.0, 0.0 ], 
		inflections: 
			[ InflectionPoint: x=20.000000000000004, y=-20.0 ] 
			[ InflectionPoint: x=480.0, y=60.0 ]  ]
		p1 Container: [ simple_demo.start at 20.0, 20.0]
		p2 Container: [ simple_demo.breach events at 480.0, 100.0]
		*/

		{
			int testNo = 4;

			ModelDomain modelDomain2 = modelEngine.createModelDomainPersistentNode("MD 2");
			modelDomain2.setWidth(1000.0);
			modelDomain2.setHeight(500.0);

			Activity A = modelDomain2.createActivity("A", null);
			A.setWidth(100.0);
			A.setHeight(100.0);
			A.setX(20.0);
			A.setY(20.0);
		
			Activity B = modelDomain2.createActivity("B", null);
			B.setWidth(100.0);
			B.setHeight(100.0);
			B.setX(480.0);
			B.setY(100.0);
		
			modelDomain2.flowComponentsHorizontally(enclosingActivity);
			modelDomain2.flowComponentsVertically(enclosingActivity);

			Port port1 = A.createPort("Port1");
			Port port2 = B.createPort("Port2");

			port1.setX(0.0);
			port1.setY(0.0);
			
			port2.setX(0.0);
			port2.setY(0.0);
			
			conduit = modelDomain2.createConduit("Conduit", port1, port2);
			
			int noOfSegments = getConduitSegmentNumber(conduit);
			if (noOfSegments != 3)
			{
				System.out.println("Test case " + testNo + " failed:");
				System.out.println(port1);
				System.out.println(port2);
				System.out.println(conduit);
				throw new Exception(
					"Test " + testNo + ": Conduit has " + noOfSegments + 
					" segments; expected " + 3);				
			}
			
			// Check results.
			
			try { checkSegmentSlopes(conduit); }
			catch (Exception ex)
			{
				System.out.println("Test case " + testNo + " failed: " + ex.getMessage());
				System.out.println(port1);
				System.out.println(port2);
				System.out.println(conduit);
				return;
			}
			
			System.out.println("Test case " + testNo + " (" + noOfSegments + 
				") passed: " + conduit.toString());
		}
		
		
		/*
		 * Test that case 5 works.
		 */
		
		{
		}
		
		
		/*
		 * Test that case 2 works.
		 */
		
		{
		}
		
		
		/*
		 * Test that ModelContainer.chechForConduitOverlaps() works.
		 */
		
		// Distance from { LineSegment: Display: x=150.0, y=0.0, scale=1.0, or=0.0], { Point: 250.0, 862.0 } } to { LineSegment: Display: x=150.0, y=0.0, scale=1.0, or=0.0], Display: x=90.0, y=0.0, scale=1.0, or=0.0] } is NaN

		
		
		System.out.println("All tests passed");
	}
	
	
	static void assertThat(boolean condition) throws Exception
	{
		if (! condition) throw new Exception("Assertion failure.");
	}
	
	
	/** ************************************************************************
	 * Return the number of segments in the Conduit. Throw an Exception if the
	 * Conduit does not have any segments.
	 */
	 
	static int getConduitSegmentNumber(Conduit conduit)
	throws
		Exception
	{
		InflectionPoint[] ips = conduit.getInflectionPoints();
		if (ips == null) throw new Exception(
			"Conduit does not have an Inflection Point array.");
		
		return ips.length + 1;
	}


	static void checkSegmentSlopes(Conduit conduit)
	throws
		Exception  // if slope of any segment is not vertical or horizontal.
	{
		double startX = conduit.getPort1().getDisplay().getX();
		double startY = conduit.getPort1().getDisplay().getY();
		
		double endX = conduit.getPort2().getDisplay().getX();
		double endY = conduit.getPort2().getDisplay().getY();
		
		Point startpoint = conduit.getPort1().getContainer().translateToContainer(conduit.getPort1().getDisplay());
		Point endpoint = conduit.getPort2().getContainer().translateToContainer(conduit.getPort2().getDisplay());

		System.out.println("Conduit parent is " + conduit.getParent().getFullName());
		System.out.println("Start point: " + startpoint.toString());
		
		InflectionPoint[] ips = conduit.getInflectionPoints();
		int noOfSegments = ips.length + 1;
		Point[] points = new Point[noOfSegments+1];
		
		points[0] = startpoint;
		for (int ipNo = 0; ipNo < ips.length; ipNo++) points[ipNo+1] = ips[ipNo];
		points[noOfSegments] = endpoint;

		Point point = points[0];
		for (int segNo = 0; segNo < noOfSegments; segNo++)
		{
			Point segStartpoint = points[segNo];
			Point segEndpoint = points[segNo+1];
			
			System.out.println("segEngpoint: " + segEndpoint.toString());
			
			double dx = segEndpoint.getX() - segStartpoint.getX();
			double dy = segEndpoint.getY() - segStartpoint.getY();
			
			System.out.println("dx=" + dx + ", dy=" + dy);
			
			if (PointImpl.areAlmostEqual(Math.abs(dx), 0.0)) continue;  // vertical slope.
		
			if (PointImpl.areAlmostEqual(Math.abs(dy), 0.0)) continue;  // horizontal slope.
			
			throw new Exception(
				"Slope of segment " + segNo + " is not horiz or vert: is " + (dy/dx) + " for segment " + segNo);
		}
	}
}

