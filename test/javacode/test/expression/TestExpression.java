package test.expression;

import expressway.common.*;
import expressway.common.ModelAPITypes.*;
import expressway.tools.parser.*;
import expressway.tools.lexer.*;
import expressway.tools.node.*;
import expressway.tools.analysis.*;
import java.net.URLClassLoader;
import java.util.*;
import java.io.*;


/**
 * Test bench and test suite for the ExpressionInterpreter class.
 */
 
public class TestExpression
{
	public static void main(String[] args)
	{
		try
		{
			Object result1 = interpret(test1);
			compare(result1, expectedResult1);
			GlobalConsole.println("Test 1 passed");
			
			Object result2 = interpret(test2);
			compare(result1, expectedResult1);
			GlobalConsole.println("Test 2 passed");
		}
		catch (Throwable t)
		{
			GlobalConsole.println(generalpurpose.ThrowableUtil.getAllMessages(t));
			GlobalConsole.printStackTrace(t);
		}
	}
	
	
	/* *************************************************************************
	 * Test suite. These are all of the tests for the expression language.
	 */
	
	// -------------------------------------------------------------------------
	// Test 1
	// Test that one can call a Java static method.
	static Object expectedResult1 = new Double(
		org.apache.commons.math.stat.StatUtils.geometricMean(new double[] { 123, 456 }));
	static String test1 = 
		"org.apache.commons.math.stat.StatUtils.geometricMean([123, 456])";
	
	// -------------------------------------------------------------------------
	// Test 2
	// Test that one can define a function that evaluates to a Java object instance
	// and then call a Java method on that function.
	static Object expectedResult2;
	{ try { expectedResult2 = new Double(
		(new org.apache.commons.math.distribution.GammaDistributionImpl(1, 10.0))
		.cumulativeProbability(100.0)); } catch (Exception ex) { throw new RuntimeException(ex); } }
	static String test2 = 
		"( function f() { " +
		"org.apache.commons.math.distribution.GammaDistributionImpl(1, 10.0) } " +
		"f().cumulativeProbability(100.0) )";
	
	/* *************************************************************************
	 * Test bench functions. These do not need to change if the test suite changes.
	 */
	
	static void compare(Object result, Object expectedResult) throws Exception
	{
		if (! result.equals(expectedResult)) throw new Exception(
			"result=" + result.toString() + "; expected result=" + expectedResult.toString());
	}


	static InterpreterScope.ClassScope classScope = new InterpreterScope.ClassScope()
	{
		TestClassLoader classLoader = new TestClassLoader();
		
		public Package findJavaPackage(String name)
		{
			return this.classLoader.getPackage(name);
		}
	};
	
	
	static class TestClassLoader extends ClassLoader
	{
		TestClassLoader()
		{
			super(ClassLoader.getSystemClassLoader());
		}
		
		public Package getPackage(String name) { return super.getPackage(name); }
	}
	
	
	static Object interpret(String expr) throws Exception
	{
		// Create expression lexer.
		Lexer lexer = new Lexer (new PushbackReader(new StringReader(expr)));

		// Parse expression string.
		Parser parser = new Parser(lexer);
		Start ast = parser.parse();

		ExpressionInterpreter interpreter = new ExpressionInterpreter(classScope)
		{
			protected double getPredefinedPathValue(List<String> pathParts)
			throws
				ModelContainsError,
				ElementNotFound
			{
				return super.getPredefinedPathValue(pathParts);
			}
			
			
			/**
			 * Override this method to perform function calls and return the result.
			 * Call the built-in function of the specified name, providing it with the
			 * arguments in 'argObjects'. If the function produces an error, throw
			 * a InterpretationError.
			 */
			 
			protected double callBuiltinFunction(String name, Object... argObjects)
			throws
				ModelContainsError,
				ElementNotFound
			{
				return super.callBuiltinFunction(name, argObjects);
			}
		};

		ast.apply(interpreter);
		
		return interpreter.getFinalObjectResult();
	}
}

