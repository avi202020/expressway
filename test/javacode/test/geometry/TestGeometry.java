package test.geometry;

import geometry.*;


/** ****************************************************************************
 * Test the methods in package geometry.
 */
 
public class TestGeometry
{
	public static void main(String[] args)
	throws
		Exception
	{
		PointImpl point1 = new PointImpl(1.0, 2.0);
		PointImpl point2 = new PointImpl(5.0, 0.5);


	  /* *********************************************************************
		 * Test PointImpl.
		 */
		 
		/*
		 * Test that findMidpoint works.
		 */
		
		Point midpoint1 = PointImpl.findMidpoint(point1, point2);
		if (! PointImpl.areAlmostEqual(midpoint1, new PointImpl(3.0, 1.25))) throw new RuntimeException(
			"Test failed.");
		
		
		/*
		 * Test that findDistance works.
		 */
		
		double distance1 = PointImpl.findDistance(point1, point2);
		if (! PointImpl.areAlmostEqual(distance1, 4.272001872658765))
		//if (Math.abs(distance1 - 4.272001872658765) > AllowedAbsError)
			throw new RuntimeException("Test failed.");
		
		
	  /* *********************************************************************
		 * Test LineSegment.
		 */
		
		/*
		 * Test isBetween.
		 */
		
		if (! LineSegment.isBetween(2.0, 1.0, 3.0)) throw new RuntimeException(
			"Test failed.");
		
		if (! LineSegment.isBetween(2.0, -1.0, 3.0)) throw new RuntimeException(
			"Test failed.");
		
		if (! LineSegment.isBetween(2.0, 3.0, -1.0)) throw new RuntimeException(
			"Test failed.");
		
		if (! LineSegment.isBetween(-2.0, -3.0, 1.0)) throw new RuntimeException(
			"Test failed.");
		
		if (! LineSegment.isBetween(-2.0, -3.0, -1.0)) throw new RuntimeException(
			"Test failed.");
		
		if (! LineSegment.isBetween(-2.0, -1.0, -3.0)) throw new RuntimeException(
			"Test failed.");
		
		if (LineSegment.isBetween(2.0, -1.0, -3.0)) throw new RuntimeException(
			"Test failed.");
		
		if (LineSegment.isBetween(2.0, -3.0, -1.0)) throw new RuntimeException(
			"Test failed.");
		
		if (LineSegment.isBetween(-2.0, 1.0, 3.0)) throw new RuntimeException(
			"Test failed.");
		
		
		/*
		 * Test that findSlope works.
		 */

		LineSegment segment1 = 
			new LineSegment(new PointImpl(1.0, 1.0), new PointImpl(2.0, 2.0));
			
		if (! PointImpl.areAlmostEqual(segment1.findSlope(), 1.0)) throw new RuntimeException(
			"Test failed.");
		
		LineSegment segment2 = 
			new LineSegment(new PointImpl(0.0, 0.0), new PointImpl(2.0, 4.0));
			
		if (! PointImpl.areAlmostEqual(segment1.findSlope(), 1.0)) throw new RuntimeException(
			"Test failed.");
		

		/*
		 * Test that findYIntercept works.
		 */
		
		if (! PointImpl.areAlmostEqual(segment1.findYIntercept(), 0.0)) throw new RuntimeException(
			"Test failed.");
		
		LineSegment segment3 = 
			new LineSegment(new PointImpl(2.0, 2.0), new PointImpl(3.0, 4.0));
		
		if (! PointImpl.areAlmostEqual(segment3.findYIntercept(), -2.0)) throw new RuntimeException(
			"Test failed.");
		
		
		/*
		 * Test that findIntersection works.
		 */
		 
		LineSegment segment4 = 
			new LineSegment(new PointImpl(1.0, 0.0), new PointImpl(-1.0, 4.0));
		
		if (! PointImpl.areAlmostEqual(LineSegment.findIntersection(segment1, segment3),
			new PointImpl(2.0, 2.0))) throw new RuntimeException("Test failed.");
		
		if (LineSegment.findIntersection(segment1, segment2) != null)
			throw new RuntimeException("Test failed.");
		
		if (LineSegment.findIntersection(segment2, segment3) != null)
			throw new RuntimeException("Test failed.");
		
		if (! PointImpl.areAlmostEqual(LineSegment.findIntersection(segment2, segment4),
			new PointImpl(0.5, 1.0))) throw new RuntimeException("Test failed.");
		
		
		/*
		 * Test that distanceTo() works.
		 */
		 
		LineSegment segment5 = new LineSegment(new PointImpl(1.0, 1.0), new PointImpl(3.0, 5.0));
		LineSegment segment6 = new LineSegment(new PointImpl(3.0, 2.0), new PointImpl(8.0, 5.0));
		LineSegment segment7 = new LineSegment(new PointImpl(4.0, 5.5), new PointImpl(6.0, 6.0));
		PointImpl p5 = new PointImpl(4.0, 4.0);
		PointImpl p6 = new PointImpl(5.0, 5.0);
		
		double theta = Math.atan(0.5);
		
		double d1 = 3.0 * Math.sin(theta);
		double d2 = Math.sqrt(2.0) * Math.sin(theta + Math.PI / 4.0);
		double d3 = 2.0;
		double d4 = Math.sqrt(1.25);
		
		if (! PointImpl.areAlmostEqual(segment5.distanceTo(segment6), d1))
			throw new RuntimeException("Test failed.");
		
		if (! PointImpl.areAlmostEqual(segment5.distanceTo(p5), d2))
			throw new RuntimeException("Test failed.");
		
		if (! PointImpl.areAlmostEqual(segment5.distanceTo(p6), d3))
			throw new RuntimeException("Test failed.");

		if (! PointImpl.areAlmostEqual(segment5.distanceTo(segment7), d4))
			throw new RuntimeException("Test failed.");

		

	  /* *********************************************************************
		 * Test Ray.
		 */
		 
		/*
		 * Test that contains works.
		 */
		
		Ray cRay = new Ray(new PointImpl(0.0, 30.0), Math.PI * 1.5);
		Point cp = new PointImpl(0.0, -1.83697019872103E-15);
		if (! cRay.contains(cp))
			throw new RuntimeException(
				"Test failed: contains");
				
		/*
		 * Test that findIntersection works.
		 */
		
		Ray ray1 = new Ray(new PointImpl(2.0, 4.0), Math.PI/3.0);  // 60 degrees.
		Ray ray2 = new Ray(new PointImpl(1.0, 2.0), Math.PI/6.0);  // 30 degrees.
		Point expectedIntersPoint1 = null;
		
		Point intersPoint1 = Ray.findIntersection(ray1, ray2);
		if (intersPoint1 != null) throw new RuntimeException("Test failed.");
		
		
		ray1 = new Ray(new PointImpl(1.0, 2.0), Math.PI/3.0);  // 60 degrees.
		ray2 = new Ray(new PointImpl(1.0, 4.0), Math.PI/6.0);  // 30 degrees.
		expectedIntersPoint1 = new PointImpl(2.732, 5.0);
		
		intersPoint1 = Ray.findIntersection(ray1, ray2);
		if (PointImpl.findDistance(intersPoint1, expectedIntersPoint1) > 0.01)
			throw new RuntimeException(
				"Test failed: inters computed to be " + intersPoint1);
				
				
		Ray ray11 = new Ray(new PointImpl(400.0, 60.0), Math.PI/2.0);
		Ray ray12 = new Ray(new PointImpl(533.0, 200.0), Math.PI);
		Point expectedIntersPoint11 = new PointImpl(400.0, 200.0);
		Point intersPoint11 = Ray.findIntersection(ray11, ray12);
		if ((intersPoint11 == null) || (! PointImpl.areAlmostEqual(intersPoint11, expectedIntersPoint11)))
			throw new RuntimeException(
				"Test failed: inters computed to be " + intersPoint11);
		
		ray12.setRadians(0.0);
		Point intersPoint111 = Ray.findIntersection(ray11, ray12);
		if (intersPoint111 != null)
			throw new RuntimeException(
				"Test failed: inters (should be null) computed to be " + intersPoint111);

		Ray ray13 = new Ray(new PointImpl(15.0, 0.0), -Math.PI);
		Ray ray131 = new Ray(new PointImpl(0.0, 30.0), -Math.PI * 1.5);
		Point intersPoint131 = Ray.findIntersection(ray13, ray131);
		if (intersPoint131 == null)
		{
			System.out.println("inters of:");
			System.out.println("\t" + ray13);
			System.out.println("\t" + ray131);
			System.out.println(" is null");
				
			throw new RuntimeException(
				"Test failed: inters should be (0, 0), computed to be null");
		}
				
		if (! PointImpl.areAlmostEqual(intersPoint131, new PointImpl(0.0, 0.0)))
			throw new RuntimeException(
				"Test failed: inters should be (0, 0), computed to be " + 
				intersPoint131);
				
		
		/*
		 * Test that dot works.
		 */
		
		double dotProduct1 = Ray.dot(ray1, ray2);
		double expectedDotProduct1 = Math.sqrt(3.0)/2.0;
		if (! PointImpl.areAlmostEqual(dotProduct1, expectedDotProduct1))
		//if ((Math.abs(dotProduct1 - expectedDotProduct1) > AllowedAbsError))
			throw new RuntimeException("Test failed.");
		
		
		/*
		 * Test that findClosestPoint works.
		 */
		
		Ray ray3 = new Ray(new PointImpl(1.0, 2.0), Math.PI/3.0);
		Point point3 = new PointImpl(1.0, 4.0);
		Point expectedClosestPoint1 = new PointImpl(1.732050807568877/2.0 + 1.0, 3.5);
		
		Point closestPoint1 = ray3.findClosestPoint(point3);
		if (! PointImpl.areAlmostEqual(closestPoint1, expectedClosestPoint1))
		//if (PointImpl.findDistance(closestPoint1, expectedClosestPoint1) > AllowedAbsError)
			throw new RuntimeException("Test failed.");
			
		Ray ray31 = new Ray(new PointImpl(15.0, 0.0), Math.PI);
		if (! PointImpl.areAlmostEqual
			(
				ray31.findClosestPoint(new PointImpl(0.0, 30.0)),
				new PointImpl(0.0, 0.0)
			)
		)
			throw new RuntimeException("Test failed.");
			
		Ray ray32 = new Ray(new PointImpl(15.0, 0.0), -Math.PI);
		if (! PointImpl.areAlmostEqual
			(
				ray32.findClosestPoint(new PointImpl(0.0, 30.0)),
				new PointImpl(0.0, 0.0)
			)
		)
			throw new RuntimeException("Test failed.");
		
		
		
		/*
		 * Test that Ray(Point origin, Point destination) works.
		 */
		 
		Point origin1 = new PointImpl(1.0, 2.0);
		Point destination1 = new PointImpl(1.732050807568877/2.0 + 1.0, 3.5);
		double expectedRadians1 = Math.PI/3.0;
		
		Ray ray4 = new Ray(origin1, destination1);
		if (! PointImpl.areAlmostEqual(ray4.getRadians(), expectedRadians1))
		//if (Math.abs(ray4.getRadians() - expectedRadians1) > AllowedAbsError)
			throw new RuntimeException("Test failed.");

		
	  /* *********************************************************************
		 * Test Face.
		 */
		
		Face face1 = new Face(new PointImpl(1.0, 1.0), new PointImpl(2.0, 2.0),
			new PointImpl(2.0, 1.0));
		
		 
		/*
		 * Test that createOutwardRay works.
		 */
		 
		Ray ray5 = face1.createOutwardRay();
		if (! PointImpl.areAlmostEqual(ray5.getOrigin(), new PointImpl(1.5, 1.5)))
			throw new RuntimeException("Test failed.");
		
		if (! PointImpl.areAlmostEqual(ray5.getRadians(), Math.PI * 0.75))
			throw new RuntimeException(
				"Test failed: ray5 radians = " + ray5.getRadians() +
				" (expected) " + Math.PI*0.75 + ").");
		
		
		/*
		 * Test that intersects works.
		 */
		
		Rectangle rect1 = new Rectangle(new PointImpl(1.5, 1.5), 1.0, 1.0);
		if (! face1.intersects(rect1))
			throw new RuntimeException("Test failed.");
		
		Rectangle rect2 = new Rectangle(new PointImpl(-1.5, 1.5), 1.0, 1.0);
		if (face1.intersects(rect2))
			throw new RuntimeException("Test failed.");
	}
}

