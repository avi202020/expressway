package test.graph;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.*;
import graph.*;


public class TestGraph
{
	public static void main(String[] args)
	{
		/* Test basic Graph. */
		{
			JFrame frame = new JFrame();
			frame.setSize(500, 500);
			frame.setPreferredSize(new Dimension(500, 500));
			frame.setLocation(0, 0);
	
			Container contentPane = frame.getContentPane();
			
			contentPane.setLayout(new FlowLayout());
			
			Graph g1 = new Graph();
			double[] xValues = { 1.0, 2.0, 2.5 };
			double[] yValues = { 3.1, 2.5, 2.2 };
			
			g1.setPixelXSize(0.01);
			g1.setPixelYSize(0.01);
			g1.setXAxisLabel("X axis");
			g1.setYAxisLabel("Y axis");
			g1.setXValues(xValues);
			g1.setYValues(yValues);
			g1.setXStartLogical(0.0);
			g1.setYStartLogical(0.0);
			g1.setXEndLogical(4.0);
			g1.setYEndLogical(4.0);
			g1.setXDeltaLogical(1.0);
			g1.setYDeltaLogical(1.0);
			g1.setMaxXAxisLength(400);
			g1.setMaxYAxisLength(400);
			g1.setXStartDisplay(50);
			g1.setYStartDisplay(420);
			
			G1Panel g1Panel = new G1Panel(g1);
			g1Panel.setSize(450, 450);
			g1Panel.setPreferredSize(new Dimension(450, 450));
			contentPane.add(g1Panel);
			
			frame.pack();
			frame.validate();
			frame.setVisible(true);
		}

		/* Test SmoothGraph. */
		{
			JFrame frame = new JFrame();
			frame.setSize(500, 500);
			frame.setPreferredSize(new Dimension(500, 500));
			frame.setLocation(50, 50);
	
			Container contentPane = frame.getContentPane();
			
			contentPane.setLayout(new FlowLayout());
			
			Graph g1 = new SmoothGraph();
			double[] xValues = { 1.0, 2.0, 2.5 };
			double[] yValues = { 3.1, 2.5, 3.2 };
			
			g1.setPixelXSize(0.01);
			g1.setPixelYSize(0.01);
			g1.setXAxisLabel("X axis");
			g1.setYAxisLabel("Y axis");
			g1.setXValues(xValues);
			g1.setYValues(yValues);
			g1.setXStartLogical(0.25);
			g1.setYStartLogical(0.25);
			g1.setXEndLogical(4.0);
			g1.setYEndLogical(4.0);
			g1.setXDeltaLogical(1.0);
			g1.setYDeltaLogical(1.0);
			g1.setMaxXAxisLength(400);
			g1.setMaxYAxisLength(400);
			g1.setXStartDisplay(50);
			g1.setYStartDisplay(420);
			
			G1Panel g1Panel = new G1Panel(g1);
			g1Panel.setSize(450, 450);
			g1Panel.setPreferredSize(new Dimension(450, 450));
			contentPane.add(g1Panel);
			
			frame.pack();
			frame.validate();
			frame.setVisible(true);
		}
		
		/* Test ErrorGraph. */
		{
		}
		
		
		/* Test BarGraph. */
		{
		}
	}
	
	
	static class G1Panel extends JPanel
	{
		private Graph graph;
		
		G1Panel(Graph graph)
		{
			this.graph = graph;
			setBackground(Color.BLUE);
		}
		
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			
			try { graph.draw(g); }
			catch (Graph.InvalidGraph ig)
			{
				ig.printStackTrace();
			}
		}
	}
}

