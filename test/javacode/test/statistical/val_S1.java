package test.statistical;

import java.util.*;
import statistics.Distributions.*;


/**
 * Perform a completely independent verification of the Course example result.
 * Each instance of this class is a single independent test run. The main method
 * instantiates a large number of instances of this class and runs each one.
 *
 * For 1000 runs, using the OR Objects math library, the results are as follows:
	Tally average=4020.772089512006
	Tally std dev=851.0246131608702

	With the Apache Math library, and 1000 runs,
	Tally average=4131.46338276968
	Tally std dev=1038.7934106716752
	
 * Expressway (with Apache Math) produces the following results (for 100 runs):
	mean=4297.8902
	sd=977.1462 

 */

public class val_S1
{
	/** If true, run in diagnostic mode. */
	private static boolean TEST = true;
	
	/** Set to the number of independent simulation runs that should be performed. */
	private static int NoOfTests = 1000;
	
	/** The simulation time. Simulation events will not extend past this time. */
	private static double TimeHorizon = 365.0 * 5.0;  // five years
	
	/** Safety to prevent endless loop. */
	private static int IterationLimit = 500;
	
	
	public static void main(String[] args)
	{
		// Choose an implementation of the statistics package.
		
		//DistributionFactory.setDistributionKind(Distribution.DistributionKind.Apache);
		DistributionFactory.setDistributionKind(Distribution.DistributionKind.ORO);
		
		
		double tallyTotal = 0.0;
		double[] tallies = new double[NoOfTests];
				
		for (int i = 1; i <= NoOfTests; i++)  // Perform the simulation NoOfTests times.
		{
			System.out.println("Run " + i);
			val_S1 test = new val_S1(TimeHorizon);  // simulate.
			
			double tally = test.getTally();  // get result.
			tallies[i-1] = tally;
			
			tallyTotal += tally;
		}
		
		double tallyAverage = tallyTotal / ((double)NoOfTests);
		
		
		// Compute standard deviation.
		
		double sd = 0.0;
		for (int i = 0; i < NoOfTests; i++)
		{
			double z = tallies[i] - tallyAverage;
			sd += (z * z);
		}
		
		sd = Math.sqrt(sd / ((double)NoOfTests));
		
		
		System.out.println("\tTally average=" + tallyAverage);
		System.out.println("\tTally std dev=" + sd);
	}
	
	
	public double getTally() { return tally; }
	
	
	public double getTimeHorizon() { return timeHorizon; }
	
	
	private double tally = 0.0;
	private double timeHorizon = 0.0;
	
	
	public val_S1(double timeHorizon)
	{
		this.timeHorizon = timeHorizon;
		
		Constant devCostTimeDist = new Constant(1.0);
		Gamma devCostValueDist = DistributionFactory.createGammaDistribution(225.0, 6.7);
		
		Constant deployCostTimeDist = new Constant(365.0);
		Gamma deployCostValueDist = DistributionFactory.createGammaDistribution(400.0, 0.25);
		
		
		Gamma secFailTimeDist = DistributionFactory.createGammaDistribution(1.0, 365.0);
		Gamma secFailValueDist = DistributionFactory.createGammaDistribution(100.0, 2.5);
		
		Constant oandMTimeDist = new Constant(365.0);
		Gamma oandMValueDist = DistributionFactory.createGammaDistribution(400.0, 0.5);
		
		Constant avoidCostTimeDist = new Constant(365.0);
		Gamma avoidCostValueDist = DistributionFactory.createGammaDistribution(8.0, 89.0);
		
		Constant missionTimeDist = new Constant(365.0);
		Gamma missionValueDist = DistributionFactory.createGammaDistribution(16.0, 63.0);
		
		Gamma futureAvoidCostTimeDist = DistributionFactory.createGammaDistribution(3.0, 365.0);
		Gamma futureAvoidCostValueDist = DistributionFactory.createGammaDistribution(4.0, 50.0);
		
		
		Constant futureEarlierValueTimeDist = new Constant(1.0);
		Gamma futureEarlierValueValueDist = DistributionFactory.createGammaDistribution(4.0, 125.0);
		
		String[] streamNames =
		{
			"Sec Fail",
			"O and M",
			"Avoid Cost",
			"Mission",
			"Future Avoid Cost"
		};
		
		Distribution[] timeDistributions = new Distribution[5];
		Distribution[] valueDistributions = new Distribution[5];
		
		timeDistributions[0] = secFailTimeDist;
		timeDistributions[1] = oandMTimeDist;
		timeDistributions[2] = avoidCostTimeDist;
		timeDistributions[3] = missionTimeDist;
		timeDistributions[4] = futureAvoidCostTimeDist;
		
		valueDistributions[0] = secFailValueDist;
		valueDistributions[1] = oandMValueDist;
		valueDistributions[2] = avoidCostValueDist;
		valueDistributions[3] = missionValueDist;
		valueDistributions[4] = futureAvoidCostValueDist;
		
		/** The arithmetic sign to use for each of the five iterated value types. */
		int[] signs =
		{
			-1, -1, 1, 1, 1
		};
		
		
		// Generate and tally the value of the two startup random events.
		
		double devTime = devCostTimeDist.getPD();
		double devValue = devCostValueDist.getPD();
		
		double value = 0.0;
		
		if (devTime <= timeHorizon)
		{
			value = -devValue;
			tally += value;
		}
		
		double deployTime = deployCostTimeDist.getPD();
		double deployValue = deployCostValueDist.getPD();
		
		if (deployTime <= timeHorizon)
		{
			value = -deployValue;
			tally += value;
		}
		

		// For each of the five repeating components.

		for (int i = 0; i < 5; i++) // each of the five main comps
		{
			// Set initial time to be the time at which the system was deployed.
			
			double currentTime = deployTime;
			
			double iteration = 0;
			
			
			// Create events for each of the five repeating components.
			
			for (;;)  // Loop until event is beyond the time horizon.
			{
				// Create next event for component i.
				
				double deltaTime = timeDistributions[i].getPD();
				
				double eventTime = currentTime + deltaTime;
				
				if // the next event (for that comp) is not beyond the time horizon,
					(eventTime > timeHorizon)
				
					break;
					
				if (iteration > IterationLimit) break;
				
				iteration++;
				
				
				// Tally the event.
				
				double eventValue = signs[i] * valueDistributions[i].getPD();
				
				tally += eventValue;
				
				
				// If the comp is the fifth comp, schedule the dependent event.
				
				if (i == 4)
				{
					double deltaDepFutureTime = 
						futureEarlierValueTimeDist.getPD();
					
					double depFutureTime = deltaDepFutureTime + eventTime;
					
					if (depFutureTime <= timeHorizon)
					{
						double depFutureValue = futureEarlierValueValueDist.getPD();
					
						tally += depFutureValue;
					}
				}
				
				
				currentTime = eventTime;
			}
		}
	}
}



