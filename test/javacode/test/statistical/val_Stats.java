package test.statistical;

import java.util.*;
import statistics.Distributions.*;


/**
 * Validate the gamma distribution of a statistics package.
 * The package to validate is chosen via the method 
 * 		Distributions.DistributionFactory.setDistributionKind(kind).
 */

public class val_Stats
{
	public static int NoOfIterations = 100;
	
	
	static double kThetas[][] =
	{
		{ 3.0, 10.0 },
		{ 225.0, 6.7 },
		{400.0, 0.25},
		{100.0, 2.5},
		{400.0, 0.5},
		{8.0, 89.0},
		{16.0, 63.0},
		{4.0, 50.0},
		{4.0, 125.0},
		{1.0, 365.0},
		{3.0, 365.0}
	};
	
	
	public static void main(String[] args)
	{
		double k = 3.0;
		double theta = 10.0;
		
		for (double[] kTheta : kThetas)
		{
			k = kTheta[0];
			theta = kTheta[1];
		
			double alpha = k;
			double beta = theta;
			//double beta = 1.0/theta;
			
			
			System.out.println("For a gamma(k=" + k + ", theta=" + theta + "):");
			System.out.println("\tmean=" + (k * theta));
			System.out.println("\tsd=" + Math.sqrt(k * theta * theta));
			
			System.out.println();
			
			
			DistributionFactory.setDistributionKind(Distribution.DistributionKind.Apache);
			val_Stats v = new val_Stats(alpha, beta);
			System.out.println("\tFor Apache:");
			System.out.println("\t\tmean=" + v.mean);
			System.out.println("\t\tsd=" + v.sd);
	
	
			DistributionFactory.setDistributionKind(Distribution.DistributionKind.ORO);
			v = new val_Stats(alpha, beta);
			System.out.println("\tFor ORO:");
			System.out.println("\t\tmean=" + v.mean);
			System.out.println("\t\tsd=" + v.sd);
			
			System.out.println("======");
		}
	}
	
	
	public double mean = 0.0;
	public double variance = 0.0;
	public double sd = 0.0;


	private double eSumOfSquares = 0.0;
	
	
	public val_Stats(double alpha, double beta)
	{
		// Instantiate a gamma distribution.
		
		Gamma gamma = DistributionFactory.createGammaDistribution(alpha, beta);
		
		
		// Repeatedly obtain a random variable from the distribution, computing
		// the mean and variance.
		
		for (int iteration = 1; iteration <= NoOfIterations; iteration++)
		{
			double randomValue = gamma.getPD();
			
			mean += randomValue / NoOfIterations;
			
			eSumOfSquares += ((randomValue * randomValue) / (double)NoOfIterations);
		}
		
		variance = eSumOfSquares - (mean * mean);
		sd = Math.sqrt(variance);
	}
}


