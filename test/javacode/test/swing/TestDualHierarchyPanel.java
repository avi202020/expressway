package test.swing;

import swing.*;
import swing.DualHierarchyPanel.*;
import java.util.*;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.tree.*;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.*;
import org.jdesktop.swingx.decorator.*;
import org.jdesktop.swingx.renderer.*;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;


public class TestDualHierarchyPanel
{
	public static void main(String[] args) throws Exception
	{
		TreeTableNode leftRootNode = new DefaultMutableTreeTableNode(
			"Left Root User Object", true);
		TreeTableNode rightRootNode = new DefaultMutableTreeTableNode(
			"Right Root User Object", true);
		
		List<String> leftColumnNames = new Vector<String>();
		List<String> rightColumnNames = new Vector<String>();
		
		leftColumnNames.add("1st Column");
		rightColumnNames.add("First Column");
		
		LeftModel leftModel = new LeftModel(leftRootNode, leftColumnNames);
		RightModel rightModel = new RightModel(rightRootNode, rightColumnNames);
		
		TreeTableComponent leftTreePanel = new LeftTreePanel(leftModel);
		TreeTableComponent rightTreePanel = new RightTreePanel(rightModel);
		CrossMapPanel crossMapPanel = new CenterPanel(leftTreePanel, rightTreePanel);
		
		ScrollableDualHierarchyPanel dualHierarchyPanel = new ScrollableDualHierarchyPanel(
			"Dual Hierarchy Panel",
			leftTreePanel, crossMapPanel, rightTreePanel);
		
		JFrame frame = new JFrame("Test Dual Hierarchy Panel");
		frame.add(dualHierarchyPanel, BorderLayout.CENTER);
		frame.setSize(800, 600);
		
		dualHierarchyPanel.postConstructionSetup();
		
		frame.show();
	}
}


class LeftTreePanel extends JXTreeTable implements TreeTableComponent
{
	LeftTreePanel(LeftModel model)
	{
		super(model);
		Highlighter highligher = HighlighterFactory.createSimpleStriping(
			HighlighterFactory.BEIGE);
		this.setHighlighters(highligher);
		this.setShowGrid(true);
		this.setShowsRootHandles(true);
		this.setColumnControlVisible(true);
		this.setTreeCellRenderer(new DefaultXTreeCellRenderer());
		this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		this.setHorizontalScrollEnabled(true);

		this.setDefaultRenderer(String.class, new DefaultTableRenderer(
		new StringValue()
		{
			public String getString(Object value) { return 
				value == null ? "null" : value.toString(); }            
		}));
	}
	
	
	private List<TreeTableComponentChangeListener> listeners = 
		new Vector<TreeTableComponentChangeListener>();
	
	
	public JXTreeTable getJXTreeTable() { return this; }

	
	public void postConstructionSetup()
	throws
		Exception
	{
		doLayout();
	}
	
		
	public void addChangeListener(TreeTableComponentChangeListener listener)
	{
		listeners.add(listener);
	}
	
	
	public int getYPosition(TreeTableNode node)
	{
		DefaultTreeTableModel model = (DefaultTreeTableModel)(getTreeTableModel());
		TreeTableNode[] nodesPath = DualHierarchyPanel.getPathToRoot(node, this);
		TreePath path = new TreePath(nodesPath);
		int row = getRowForPath(path);
		int column = 0;
		Rectangle rect = getCellRect(row, column, true);
		return rect.y + rect.height/2;
	}
	
	
	TreeTableNode getFirstNode() { return ((LeftModel)(getTreeTableModel())).getFirstNode(); }
}


class RightTreePanel extends JXTreeTable implements TreeTableComponent
{
	RightTreePanel(RightModel model)
	{
		super(model);
		Highlighter highligher = HighlighterFactory.createSimpleStriping(
			HighlighterFactory.BEIGE);
		this.setHighlighters(highligher);
		this.setShowGrid(true);
		this.setShowsRootHandles(true);
		this.setColumnControlVisible(true);
		this.setTreeCellRenderer(new DefaultXTreeCellRenderer());
		this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		this.setHorizontalScrollEnabled(true);

		this.setDefaultRenderer(String.class, new DefaultTableRenderer(
		new StringValue()
		{
			public String getString(Object value) { return 
				value == null ? "null" : value.toString(); }            
		}));
	}
	
	
	private List<TreeTableComponentChangeListener> listeners = 
		new Vector<TreeTableComponentChangeListener>();
	
	
	public JXTreeTable getJXTreeTable() { return this; }

	
	public void postConstructionSetup()
	throws
		Exception
	{
		doLayout();
	}
	
		
	public void addChangeListener(TreeTableComponentChangeListener listener)
	{
		listeners.add(listener);
	}
	
	
	public int getYPosition(TreeTableNode node)
	{
		return DualHierarchyPanel.getYPosition(this, node);
	}
	
	
	TreeTableNode getSecondNode() { return ((RightModel)(getTreeTableModel())).getSecondNode(); }
}


class CenterPanel extends CrossMapPanel
{
	CenterPanel(TreeTableComponent leftPanel, TreeTableComponent rightPanel)
	{
		super(leftPanel, rightPanel);
	}
	
	
	/**
	 * Build the map.
	 */
	
	public void refresh()
	{
		removeAllConnections();
		
		connectLeftToRightNode(true, 
			((LeftTreePanel)(getLeftPanel())).getFirstNode(), 
			((RightTreePanel)(getRightPanel())).getSecondNode(), false);
		
		repaint();
	}
	
	
	public void leftTreeTableChanged(TreeTableNode node)
	{
		refresh();
	}
	
	
	public void rightTreeTableChanged(TreeTableNode node)
	{
		refresh();
	}
}


class LeftModel extends DefaultTreeTableModel
{
	LeftModel(TreeTableNode root, List<String> columnNames)
	{
		super(root, columnNames);
		
		// Build the tree.
		
		node1 = new DefaultMutableTreeTableNode("Object 1", true);
		node1.setValueAt("Obj 1 Value", 0);
		insertNodeInto(node1, (MutableTreeTableNode)(getRoot()), 0);

		DefaultMutableTreeTableNode node1_1 = new DefaultMutableTreeTableNode("Object 1.1", true);
		node1_1.setValueAt("Obj 1.1 Value", 0);
		insertNodeInto(node1_1, node1, 0);
	}
	
	
	DefaultMutableTreeTableNode node1;
	
	
	TreeTableNode getFirstNode() { return node1; }
}


class RightModel extends DefaultTreeTableModel
{
	RightModel(TreeTableNode root, List<String> columnNames)
	{
		super(root, columnNames);
		
		// Build the tree.
		
		DefaultMutableTreeTableNode nodeA = new DefaultMutableTreeTableNode("Object A", true);
		nodeA.setValueAt("Obj A Value", 0);
		insertNodeInto(nodeA, (MutableTreeTableNode)(getRoot()), 0);

		nodeA_A = new DefaultMutableTreeTableNode("Object A.A", true);
		nodeA_A.setValueAt("Obj A_A Value", 0);
		insertNodeInto(nodeA_A, nodeA, 0);
	}
	
	
	DefaultMutableTreeTableNode nodeA_A;
	
	
	TreeTableNode getSecondNode() { return nodeA_A; }
}

