package test.swing;

import swing.*;
//import swing.MultiHierarchyPanel.*;
import java.util.*;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.table.JTableHeader;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.*;
import org.jdesktop.swingx.decorator.*;
import org.jdesktop.swingx.renderer.*;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;


public class TestTreeTableHeader
{
	public static void main(String[] args)
	{
		JFrame testframe = new JFrame("Test");
		
		Node root = new Node("ROOT", "root", true);
		final List<String> names = new Vector<String>();
		names.add("Name Col");
		names.add("Row No Col");
		names.add("Third Col");
		
		AbstractTreeTableModel m = new BasicTreeTableModel(root, names);
		
		final JXTreeTable treeTable = new JXTreeTable(m);
		root.setTable(treeTable);
		treeTable.setRootVisible(true);
		treeTable.setEditable(true);

		Node c1 = new Node("C1", treeTable);
		root.insert(c1, 0);
		m.setValueAt("c1", c1, 0);
		Object o11 = m.getValueAt(c1, 1);
		
		Object o10 = m.getValueAt(c1, 0);
		
		Node c2 = new Node("C2", treeTable);
		m.setValueAt("c2", c2, 0);
		root.insert(c2, 1);
		
		Node c2_1 = new Node("C2_1", treeTable);
		m.setValueAt("c2_1", c2_1, 0);
		c2.insert(c2_1, 0);
		
		Node c1_1 = new Node("C1_1", treeTable);
		m.setValueAt("c1_1", c1_1, 0);
		c1.insert(c1_1, 0);
		
		JScrollPane sp = new JScrollPane(treeTable);
		
		testframe.add(sp, BorderLayout.CENTER);
		
		testframe.setSize(800, 600);
		testframe.show();
		
		treeTable.addTreeExpansionListener(new TreeExpansionListener()
		{
			public void treeCollapsed(TreeExpansionEvent event)
			{
				TreePath path = event.getPath();
				int row = treeTable.getRowForPath(path);
				System.out.println("Row for path '" + path + "' is " + row);
				
				Node node = (Node)(path.getLastPathComponent());
				Enumeration children = node.children();
				for (; children.hasMoreElements(); )
				{
					Node cnode = (Node)(children.nextElement());
					TreePath cpath = new TreePath(cnode.getPathToRoot());
					int crow = treeTable.getRowForPath(cpath);
					System.out.println("\tRow for path '" + cpath + "' is " + crow);
				}
			}
			
			public void treeExpanded(TreeExpansionEvent event)
			{
				TreePath path = event.getPath();
				int row = treeTable.getRowForPath(path);
				System.out.println("Row for path '" + path + "' is " + row);
			}
		});
	}
}

class BasicTreeTableModel extends AbstractTreeTableModel
{
	private List<String> names;
	
	public BasicTreeTableModel(Node root, List<String> names)
	{
		super(root);
		this.names = names;
	}
	
	public int getColumnCount() { return names.size(); }
	public void setValueAt(Object value, Object node, int column)
	{
		((Node)node).setValueAt(value, column);
	}
	
	public Object getValueAt(Object node, int column) { return ((Node)node).getValueAt(column); }
	public Object getChild(Object parent, int index) { return ((Node)parent).getChildAt(index); }
	public int getChildCount(Object parent) { return ((Node)parent).getChildCount(); }
	public int getIndexOfChild(Object parent, Object child) { return ((Node)parent).getIndex((Node)child); }
	public boolean isLeaf(Object node) { return ((Node)node).isLeaf(); }
	public boolean isCellEditable(Object node, int column) { return ((Node)node).isEditable(column); }
	public String getColumnName(int column) {return names.get(column); }

};

class Node extends AbstractMutableTreeTableNode
{
	private String name;
	private JXTreeTable table;
	private Object[] values = new Object[3];
	
	public Node(String name, JXTreeTable table) { this.setName(name); this.table = table; }
	
	Node(String name, Object userObject, boolean allowsChildren)
	{
		super(userObject, allowsChildren);
		this.setName(name);
	}
	
	void setName(String name) { this.name = name; }
	
	void setTable(JXTreeTable table) { this.table = table; }
	
	public boolean isEditable(int column) { return true; }
	
	public int getColumnCount() { return 3; }
	
	public Object getValueAt(int column)
	{
		if (column == 1) return table.getRowForPath(new TreePath(getPathToRoot()));
		
		return values[column];
	}
	
	public void setValueAt(Object aValue, int column)
	{
		if (column == 1) throw new RuntimeException("Cannot set value for column 1");
		values[column] = aValue;
	}
	
	Object[] getPathToRoot()
	{
		List nodes = new Vector();
		TreeTableNode node = this;
		for (;;)
		{
			nodes.add(0, node);
			node = node.getParent();
			if (node == null) break;
		}
		
		return nodes.toArray(new Object[nodes.size()]);
	}
	
	public String toString() { return name; }
}

