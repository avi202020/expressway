/*
Clarify understanding of templates.
*/



package test.template;


import java.util.List;
import java.util.Vector;


public class TestTemplate
{
}


interface HierarchyElement
{
	interface HierarchyDomain extends Hierarchy
	{
	}
	
	
	interface Hierarchy extends HierarchyElement
	{
		Hierarchy createSubHierarchy();
	}
}


abstract class HierarchyBase
	<HE extends HierarchyElement,
		H extends HierarchyElement.Hierarchy>
	
	extends HierarchyElementBase//<HE, H>
{
	private List<H> subHierarchies = new Vector<H>();

	protected HierarchyBase(H parent)
	{
		super(parent);
	}

	public abstract H createSubHierarchy();
}


abstract class HierarchyElementBase
	//<HE extends HierarchyElement,
	//	H extends HierarchyElement.Hierarchy>
	
	implements HierarchyElement
{
	HierarchyElementBase(HierarchyElement parent)
	{
	}
}

