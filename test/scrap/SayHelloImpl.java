/*
java -Djava.security.manager -Djava.security.policy="file:/C:/Documents and Settings/Owner/.java.policy" -Djava.rmi.server.codebase="file:/Volumes/cliff_apple/Expressway/Prototype/test/" scrap.SayHelloImpl
java -Djava.rmi.server.codebase="file:/Volumes/cliff_apple/Expressway/Prototype/test/" scrap.SayHelloImpl
 */



package scrap;


import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.*;
import java.io.IOException;


public class SayHelloImpl extends UnicastRemoteObject implements HelloWorld
{
	public static void main(String[] args)
	{
		Registry registry = null;
		
		/*
		// Create and install a security manager
        if (System.getSecurityManager() == null) 
        {
            System.setSecurityManager(new RMISecurityManager());
            System.out.println("Security manager installed.");
        }
        else
            System.out.println("Security manager already exists.");
		*/
 
        try  //special exception handler for registry creation
        {
            registry = LocateRegistry.createRegistry(1099); 
            System.out.println("java RMI registry created.");
        }
        catch (RemoteException ex)
        {
            ex.printStackTrace();
			return;
        }
 
        try
        {
            //Instantiate RmiServer
            SayHelloImpl obj = new SayHelloImpl();
 
            // Bind this object instance to the name "RmiServer"
            registry.rebind("SayHello", obj);
 
            System.out.println("PeerServer bound in registry");
        } 
        catch (Exception e) 
        {
            System.err.println("RMI server exception:");
            e.printStackTrace();
        }
	}
	
	
	public SayHelloImpl() throws RemoteException
	{
		super();		
 	}
	
	
	public void sayHello() throws IOException
	{
		System.out.println("Hello");
	}
}

